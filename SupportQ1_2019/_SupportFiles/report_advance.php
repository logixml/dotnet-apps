<?php get_header(); 

	$remote_address=$_SERVER['REMOTE_ADDR'];
	$result= $this->session->all_userdata();
	$user_id=rawurlencode($result['cus_username']);   
	$id = $result['cus_id'];
	$url = $logi_url.$logi_app_name."/rdTemplate/rdGetSecureKey.aspx?Username=".$user_id."&ClientBrowserAddress=0.0.0.0&databasename=".$databasename."&hostname=".$hostname."&uname=".$username."&password=".$password;
	$currenturl=$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

	$output = curl_exec($ch); 
	curl_close($ch);
	$rep_url = $logi_url.$logi_app_name."/rdPage.aspx?rdReport=AdvancedGTE&rdAgReset=True&rdAgRefreshData=True&myLanguage=".$this->session->userdata("cus_language")."&user_id=".$id."&user_role=".$user_role."&rdSecureKey=".$output."&currentUrl=".$currenturl;
?>

<div class="cd-module">
    <?php echo cd_page_header(lang('cd3MenuCOREDashboard'), "fa icon-report fa-fw"); ?>
    <div class="cd-tabstrip" data-tab-position="top">
        <ul>
            <li><?php echo anchor(file_path(GTET_VERSION, 'report/index'), lang('reports'), 'class=""');?></li>
            <li class="k-state-active"><?php echo anchor(gtet_path_helper::generate_report_url(), lang('advanceReport'), 'class=""');?></li>
        </ul>
        <div></div>
        <div>
            <iframe src="<?php echo $rep_url?>" frameborder="0" height="1000px" width="100%"></iframe>
        </div>
    </div>
</div>

<?php get_footer(); ?>
