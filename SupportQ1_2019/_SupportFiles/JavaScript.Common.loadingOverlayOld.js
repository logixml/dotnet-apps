var rotateWaitingCircleInterval;
(function() {
    var oldOpen = XMLHttpRequest.prototype.open;
    window.openHTTPs = 0;
    XMLHttpRequest.prototype.open = function(method, url, async, user, pass) {
		window.openHTTPs++;
		this.addEventListener('readystatechange', function() {
			if(this.readyState == 4) {
				window.openHTTPs--;
			}
		}, false);
		oldOpen.call(this, method, url, async, user, pass);
    }
})();


var rotateWaitingCircle = function() {
	if($('#rdWaitImage')[0]) {
		var x = parseInt($('#rdWaitImage')[0].style.backgroundPositionX);
		$('#rdWaitImage')[0].style.backgroundPositionX = -32 + x + 'px';
	} else {
		clearInterval(rotateWaitingCircleInterval);
		$('#yui_3_1_1551182826082_5769').remove();
	}
	if(window.openHTTPs != 1) {
		if (console && console.log) {
			console.log('handle error while hiding wait panel');
		}
		clearInterval(rotateWaitingCircleInterval);
		$('#yui_3_1_1551182826082_5769').remove();
		$('.yui3-widget-mask').remove();
	}
}

var waitWindow = function() {
	var waitPanel = $('.yui3-widget-mask');
	var waitPanelFull  = '\
		<div class="yui3-widget-mask" style="position: fixed; width: 100%; height: 100%; top: 0px; left: 0px; z-index: 9300; opacity: 0.5;"></div>\
		<div id="yui_3_1_1551182826082_5769" class="yui3-widget yui3-panel yui3-widget-positioned yui3-widget-stacked yui3-widget-modal yui3-panel-focused" tabindex="0" style="width: 500px; left: 541.5px; top: 361px; z-index: 9300; position: absolute;">\
			<div id="rdWait" style="display: table; z-index: 10000; " class="rdThemeWaitPanel yui3-panel-content yui3-widget-stdmod">\
				<table>\
					<tbody>\
						<tr>\
							<td>\
								<div id="rdWaitImage" style="width: 32px; height: 32px; background-image: url(&quot;rdTemplate/rdWaitAll.gif&quot;); background-position: -1280px 0px; z-index: 10000;"></div>\
							</td>\
							<td>\
								<span class="rdThemeWaitCaption">\
									Please wait...\
								</span>\
							</td>\
						</tr>\
					</tbody>\
				</table>\
			</div>\
		</div>\
		';
	
	if (waitPanel.length == 0) {
		$(waitPanelFull).insertBefore( $('body').children().first());
	} 
	$(waitPanelFull).show();
}


var displayLoading = function() {
	waitWindow();
	var w = $(window).width();
	var h = $(window).height(); 
	
	document.getElementById('yui_3_1_1551182826082_5769').style.position = 'fixed';
	document.getElementById('yui_3_1_1551182826082_5769').style.left = Math.round(w/2 - 89.5) + 'px';
	document.getElementById('yui_3_1_1551182826082_5769').style.top = Math.round(h/2 - 27) + 'px';
	
	rotateWaitingCircleInterval = setInterval(rotateWaitingCircle, 50);
}

var addLoadingOverlay = function() {
	$('#HomeReportTable > tbody a').on('click', function() {
		// fix for freezing loading
		window.openHTTPs++;
		displayLoading();
	});
}