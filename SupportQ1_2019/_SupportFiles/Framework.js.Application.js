function ToggleExpander(id){
	// close all other expansion boxes and open the new target
	$("#"+id).slideToggle("slow");
}

function getScrollBarWidth() {
    var $outer = $('<div>').css({visibility: 'hidden', width: 100, overflow: 'scroll'}).appendTo('body'),
        widthWithScroll = $('<div>').css({width: '100%'}).appendTo($outer).outerWidth();
    $outer.remove();
    return 100 - widthWithScroll;
};

function offCanvasToggle(){
	$(".Sidebar-left").toggleClass("on-canvas");
	$(".Main-Container").toggleClass("off-canvas");	
}

function BootstrapCheckboxGroup(){
	$(".checkbox-group").css("border","none");
	$(".checkbox-group ul").addClass("btn-group");
	$("div.checkbox-group label").each(function(){
		$(this).unwrap().addClass("btn btn-default");
	});
	
	$("div.checkbox-group label").on("click", function () {
		if($("input",this).is(":checked")===true){
			$(this).addClass("active");
		}else{
			$(this).removeClass("active");
		}
	});
}

$(function(){
	$(".sidebar-toggle").click(function(){
		var pageWidth = ($(window).outerWidth()+getScrollBarWidth());
		//console.log(pageWidth);
		if(pageWidth>=769){
			$(".Sidebar-left").toggleClass("collapsed");
		}else{
			$(".Sidebar-left").toggleClass("on-canvas");
			$(".Main-Container").toggleClass("off-canvas");	
		}
	});
	
	//Remove the off-canvas, on-canvas, and collapsed classes from the sidebar and main stage elements.
	$(window).resize(function(){
		$(".Sidebar-left").removeClass("collapsed").removeClass("on-canvas");
		$(".Main-Container").removeClass("off-canvas");
	});
	
	//Check to see if the window is top if not then display button
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('#divScrollToTop').fadeIn();
		} else {
			$('#divScrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('#ScrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},250);
		return false;
	});	
	
	
	// Init the datepicker plugin
	$(".datepicker").datepicker();//.attr("readonly",true);
	
	
	// Init the daterange picker tool
	$("span.daterange").daterangepicker({
	     format: 'MM/DD/YYYY',
		//startDate: moment().subtract(1, 'quarter').startOf('quarter'),
		//endDate: moment().subtract(1, 'quarter').endOf('quarter'),
		startDate: document.getElementById('FromDate').value,
		endDate: document.getElementById('ToDate').value,
		showDropdowns: true,
		timePicker: false,
	    ranges: {
	       'Today': [moment(), moment()],
	       'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	       'This Week': [moment().startOf('week'), moment().endOf('week')],
	       'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
	       'This Month': [moment().startOf('month'), moment().endOf('month')],
	       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
	       'This Quarter': [moment().startOf('quarter'), moment().endOf('quarter')],
	       'Last Quarter': [moment().subtract(1, 'quarter').startOf('quarter'), moment().subtract(1, 'quarter').endOf('quarter')],
	       'This Year': [moment().startOf('year'), moment().endOf('year')],
	       'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')]
	    }
	}, 
	function(start, end, label) {
	    $("span#daterange span.range-label").text(label);
		$("input#FromDate").val(start.format("YYYY-M-D"));
		$("input#ToDate").val(end.format("YYYY-M-D"));
		$("input#ToDate").trigger("change");
		console.log("Hello World");
	});	
	
	BootstrapCheckboxGroup();
});