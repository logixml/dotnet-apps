Partial Class _Default
    Inherits System.Web.UI.Page
    
    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim sLocalInfoUrl As String = txtReportsURL.Text
        Dim sGetKeyUrl As String = "/rdTemplate/rdGetSecureKey.aspx?Username=" & txtUsername.Text & "&Roles=" & txtRoles.Text & "&ClientBrowserAddress=" & Request.UserHostAddress
        Dim webRequest As Net.HttpWebRequest
        Dim webResponse As Net.WebResponse
        webRequest = Net.HttpWebRequest.Create(sLocalInfoUrl & sGetKeyUrl)

        ' run the URL to get the key
        Try
            webResponse = webRequest.GetResponse()
        Catch ex As Exception
            ' if the web server has Basic or NTLM authentication, set the credentials from the current process
            If ex.Message.IndexOf("401") <> -1 Then
                webRequest.Credentials = Net.CredentialCache.DefaultCredentials
                webResponse = webRequest.GetResponse()
            Else
                Throw ex
            End If
        End Try

        Dim sr As New IO.StreamReader(webResponse.GetResponseStream())
        Dim sKey As String = sr.ReadToEnd()
        Dim sUsersInfoUrl As String = sLocalInfoUrl & "/rdPage.aspx"
        Response.Redirect(sUsersInfoUrl & "?rdSecureKey=" & sKey)
    End Sub

    Protected Sub btnLogin2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin2.Click
        Dim sLocalInfoUrl As String = txtReportsURL.Text
        Dim sGetKeyUrl As String = "/rdTemplate/rdGetSecureKey.aspx?Username=" & txtUsername.Text & "&Roles=" & txtRoles.Text & "&ClientBrowserAddress=" & Request.UserHostAddress
        'set the link in the page
        lblSecureKeyRequest.Text = sLocalInfoUrl & sGetKeyUrl
        btnLogin3.Visible = True
    End Sub


    Protected Sub btnLogin3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin3.Click
        Dim sLocalInfoUrl As String = txtReportsURL.Text
        Dim webRequest As Net.HttpWebRequest
        Dim webResponse As Net.WebResponse
        webRequest = Net.HttpWebRequest.Create(lblSecureKeyRequest.Text)

        ' run the URL to get the key
        Try
            webResponse = webRequest.GetResponse()
        Catch ex As Exception
            ' if the web server has Basic or NTLM authentication, set the credentials from the current process
            If ex.Message.IndexOf("401") <> -1 Then
                webRequest.Credentials = Net.CredentialCache.DefaultCredentials
                webResponse = webRequest.GetResponse()
            Else
                Throw ex
            End If
        End Try

        Dim sr As New IO.StreamReader(webResponse.GetResponseStream())
        Dim sKey As String = sr.ReadToEnd()
        Dim sUsersInfoUrl As String = sLocalInfoUrl & "/rdPage.aspx"
        Dim url As String = sUsersInfoUrl & "?rdSecureKey=" & sKey
        lblSecureKeyRedirect.Text = "<a href='" & url & "' target='blank'>" & url & "</a>"
    End Sub


End Class