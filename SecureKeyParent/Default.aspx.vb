﻿
Partial Class _Default
    Inherits System.Web.UI.Page

    Protected Sub btnSubmit_Get_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit_Get.Click
		clearErrors()
        Dim sPage As String = "rdPage.aspx"
        Dim sLocalInfoUrl As String = AppURL.Text  ' the URL from perspective of this web process
        If sLocalInfoUrl.Length < 1 Then
            AppURL.Text = "Enter a URL"
            Return
        Else
            If sLocalInfoUrl.EndsWith("/") Then sLocalInfoUrl.Substring(0, sLocalInfoUrl.Length - 1)
			If Not (sLocalInfoUrl.StartsWith("http://")) Then sLocalInfoUrl = String.Format("{0}{1}","http://",sLocalInfoUrl)
        End If
        If DefPage.Text.Length > 0 Then
            sPage = "/" & DefPage.Text
            If sPage.EndsWith(".aspx") Then
                sPage = sPage & "?"
            End If
        End If
        Dim sGetKeyUrl As String = "/rdTemplate/rdGetSecureKey.aspx?Username=" & inpUser.Text & "&Roles=" & _
            inpRoles.Text
        If inpClientAdd.Text.Length > 0 Then
            sGetKeyUrl = sGetKeyUrl & "&ClientBrowserAddress=" & inpClientAdd.Text
        Else
            sGetKeyUrl = sGetKeyUrl & "&ClientBrowserAddress=" & Request.UserHostAddress
        End If
        If inpRights.Text.Length > 0 Then
            sGetKeyUrl = sGetKeyUrl & "&Rights=" & inpRights.Text
        End If
        If inpSession.Text.Length > 0 Then
            sGetKeyUrl = sGetKeyUrl & "&" & inpSession.Text
        End If
        Dim webRequest As Net.HttpWebRequest
        Dim webResponse As Net.WebResponse
        webRequest = Net.HttpWebRequest.Create(sLocalInfoUrl & sGetKeyUrl)

        'Show GetSecureKey request URL for debug purposes
        lnkDebug.Text = sLocalInfoUrl & sGetKeyUrl

        ' run the URL to get the key
        Try
            webResponse = webRequest.GetResponse()
        Catch ex As Exception
            ' if the web server has Basic or NTLM authentication, set the credentials from the current process
            If ex.Message.IndexOf("401") <> -1 Then
                webRequest.Credentials = Net.CredentialCache.DefaultCredentials
                webResponse = webRequest.GetResponse()
            Else
                'Throw ex
                lblerror.Text = "<font color=""red""> Critical Error obtaining SecureKey Token.</font> <br/><br/>Stack Trace: <br/><br/>" & ex.Message & "<br/><br/>" & ex.ToString
                Return
            End If
        End Try

        Dim sr As New IO.StreamReader(webResponse.GetResponseStream())
        Dim sKey As String = sr.ReadToEnd()
        ' the URL from the perspective of the end user's browser, usually not localhost
        'Dim sUsersInfoUrl As String = "http://localhost/rdweb1/rdPage.aspx"
        'Response.Redirect(sUsersInfoUrl & "?rdSecureKey=" & sKey)
        'Dim sPage As String = "/rdProcess.aspx?rdTaskID=tskStartup&rdProcess=testProc&"
        'Dim sPage As String = "/rdPage.aspx?"
        lnkSecureKey.Target = "NewWindow"
        lnkSecureKey.Text = sLocalInfoUrl & sPage & "rdSecureKey=" & sKey
        lnkSecureKey.NavigateUrl = sLocalInfoUrl & sPage & "rdSecureKey=" & sKey
        'lnkSecureKey.Text = sLocalInfoUrl & "/rdProcess.aspx?rdProcess=testProc&rdTaskID=tskStartup&rdSecureKey=" & sKey
        'lnkSecureKey.NavigateUrl = sLocalInfoUrl & "/rdProcess.aspx?rdProcess=testProc&rdTaskID=tskStartup&rdSecureKey=" & sKey

    End Sub

    'This method submits the necessary elements of the request via a POST instead of the standard method of GET.

    Protected Sub btnSubmit_Post_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit_Post.Click
		clearErrors()
        Dim sPage As String = "rdPage.aspx"
        Dim sLocalInfoUrl As String = AppURL.Text  ' the URL from perspective of this web process
        If sLocalInfoUrl.Length < 1 Then
            AppURL.Text = "Enter a URL"
            Return
        Else
            If sLocalInfoUrl.EndsWith("/") Then sLocalInfoUrl.Substring(0, sLocalInfoUrl.Length - 1)
        End If
        If DefPage.Text.Length > 0 Then
            sPage = "/" & DefPage.Text
            If sPage.EndsWith(".aspx") Then
                sPage = sPage & "?"
            End If
        End If

        Dim sGetKeyUrl As String = "/rdTemplate/rdGetSecureKey.aspx"

        'Create the POST parameters
        Dim postdata As String
        Dim postdatabytes As Byte()
        Dim enc As UTF8Encoding
        enc = New System.Text.UTF8Encoding()

        postdata = "Username=" & inpUser.Text & "&Roles=" & inpRoles.Text

        If inpClientAdd.Text.Length > 0 Then
            postdata = postdata & "&ClientBrowserAddress=" & inpClientAdd.Text
        Else
            postdata = postdata & "&ClientBrowserAddress=" & Request.UserHostAddress
        End If
        If inpRights.Text.Length > 0 Then
            postdata = postdata & "&Rights=" & inpRights.Text
        End If
        If inpSession.Text.Length > 0 Then
            postdata = postdata & "&" & inpSession.Text
        End If
        Dim webRequest As Net.HttpWebRequest
        Dim webResponse As Net.WebResponse
        webRequest = Net.HttpWebRequest.Create(sLocalInfoUrl & sGetKeyUrl)

        'Show GetSecureKey request URL for debug purposes
        lnkDebug.Text = sLocalInfoUrl & sGetKeyUrl

        postdatabytes = enc.GetBytes(postdata)

        ' run the URL to get the key
        Try
            webRequest.Method = "POST"
            webRequest.ContentType = "application/x-www-form-urlencoded"
            webRequest.ContentLength = postdatabytes.Length

            Using stream = webRequest.GetRequestStream()
                stream.Write(postdatabytes, 0, postdatabytes.Length)
            End Using

            webResponse = webRequest.GetResponse()
        Catch ex As Exception
            ' if the web server has Basic or NTLM authentication, set the credentials from the current process
            If ex.Message.IndexOf("401") <> -1 Then
                webRequest.Credentials = Net.CredentialCache.DefaultCredentials
                webResponse = webRequest.GetResponse()
            Else
                'Throw ex
                lblerror.Text = "<font color=""red""> Critical Error obtaining SecureKey Token.</font> <br/><br/>Stack Trace: <br/><br/>" & ex.Message & "<br/><br/>" & ex.ToString
                Return
            End If
        End Try

        Dim sr As New IO.StreamReader(webResponse.GetResponseStream())
        Dim sKey As String = sr.ReadToEnd()
        
        lnkSecureKey.Target = "NewWindow"
        lnkSecureKey.Text = sLocalInfoUrl & sPage & "rdSecureKey=" & sKey
        lnkSecureKey.NavigateUrl = sLocalInfoUrl & sPage & "rdSecureKey=" & sKey
        
    End Sub
	
    Protected Sub btnSubmit_ASYNC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit_ASYNC.Click
        clearErrors()
        Dim sPage As String = "rdPage.aspx"
        Dim sLocalInfoUrl As String = AppURL.Text  ' the URL from perspective of this web process
        If sLocalInfoUrl.Length < 1 Then
            AppURL.Text = "Enter a URL"
            Return
        Else
            If sLocalInfoUrl.EndsWith("/") Then sLocalInfoUrl.Substring(0, sLocalInfoUrl.Length - 1)
        End If
        If DefPage.Text.Length > 0 Then
            sPage = "/" & DefPage.Text
            If sPage.EndsWith(".aspx") Then
                sPage = sPage & "?"
            End If
        End If

        Dim sGetKeyUrl As String = "/rdTemplate/rdGetSecureKey.aspx"



    End Sub

    Private Sub clearErrors()
        'Clear any errors that were thrown with a previous request attempt.
        lblerror.Text = ""
    End Sub

End Class
