<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TestSecureKey.aspx.vb" Inherits="_Default" %>

<%@ Import Namespace="System.Net" %>
<html>
<head><title>Logi SecureKey Authentication Sample</title></head>
<body>
<h2>Sample for Secure Key Login</h2>
<form id="form1" runat="server">
<table>
<tr>
    <td>Client IP address:</td><td><asp:Label ID="lblClientIP" runat="server"><%=Request.UserHostAddress%> (this is the User ClientIP getting passed with the request)</asp:Label></td>
</tr>
<tr>
    <td>Authentication IP address:</td><td><asp:Label ID="Label1" runat="server"><%=System.Environment.MachineName%> | <%=System.Net.Dns.GetHostEntry(System.Environment.MachineName).AddressList(0)%> (this is the Auth ClientIP to insert in _Settings)</asp:Label></td>
</tr>
<tr>
    <td>Root URL for Reports:</td><td><asp:TextBox ID="txtReportsURL" runat="server" Text="http://SERVER_NAME/YOUR_REPORTS_APP" size="50"></asp:TextBox></td>
</tr>
<tr>
    <td>Username:</td><td><asp:TextBox ID="txtUsername" runat="server" Text="admin"></asp:TextBox></td>
</tr>
<tr>
    <td>Roles List (comma-separated):</td><td><asp:TextBox ID="txtRoles" runat="server" Text="1,2,3"></asp:TextBox></td>
</tr>
<tr>
    <td colspan="2"><asp:Button ID="btnLogin" runat="server" Text="Authenticate (Automatic Re-direct)" />&nbsp;&nbsp;&nbsp;<asp:Button ID="btnLogin2" runat="server" Text="Authenticate (Get Key, Show Link and Re-direct)" /></td>
</tr>
</table>
<br />
<b><asp:Label ID="lblSecureKeyRequest" runat="server"></asp:Label></b><br />
<asp:Button Visible="false" ID="btnLogin3" runat="server" Text="Get Key" /><br />
<asp:Label ID="lblSecureKeyRedirect" runat="server"></asp:Label><br />
</form>
<p><b>Instructions for Demonstrating SecureKey Authentication</b></p>
<ul>
<li>Make sure you edit your _Settings file to have the correct Security Element values, including the proper Client address list</li>
<li>Edit the Root URL for Reports textbox so that it points to the correct URL Path for your application</li>
<li>Set the appropriate Username and comma-separated roles list for your Logi application</li>
<li>To authenticate, click on the Authenticate Button to generate a SecureKey and automatically re-direct to your Default Report or other reporting location</li>
</ul>


</body>
</html>

