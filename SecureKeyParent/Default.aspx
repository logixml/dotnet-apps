﻿<%@ Page Language="VB" AutoEventWireup="false" Debug="true" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SecureKey Parent App Test</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <span>Logi App URL: &nbsp;&nbsp;</span>
        <asp:TextBox ID="AppURL" style="width:350px;" runat="server"></asp:TextBox>&nbsp;&nbsp;(i.e. http://localhost/myapp)<br /><br />
        <span>Default Page (Optional): &nbsp;&nbsp;</span>
        <asp:TextBox ID="DefPage" style="width:175px;" runat="server">rdPage.aspx</asp:TextBox>&nbsp;&nbsp;(i.e. rdPage.aspx or Gateway.aspx)<br /><br />
        <span>Client Browser Address (Optional): &nbsp;&nbsp;</span>
        <asp:TextBox ID="inpClientAdd" style="width:175px;" runat="server"></asp:TextBox>&nbsp;&nbsp;(127.0.0.1 or ::1 for localhost)<br /><br />
        <span >User: &nbsp;&nbsp;</span>
        <asp:TextBox ID="inpUser" style="width:175px;" runat="server"></asp:TextBox><br /><br />
        <span >Roles: &nbsp;&nbsp;</span>
        <asp:TextBox ID="inpRoles" style="width:320px;" runat="server"></asp:TextBox><br /><br />
        <span >Rights (Optional): &nbsp;&nbsp;</span>
        <asp:TextBox ID="inpRights" style="width:320px;" runat="server"></asp:TextBox><br /><br />
        <span >Session Parameters (Optional): &nbsp;&nbsp;</span>
        <asp:TextBox ID="inpSession" style="width:320px;" runat="server"></asp:TextBox>&nbsp;&nbsp;Add values as name/value pairs, separating multiple values by &. (ie. var1=myvar&var2=myvar2)
    </div>
    <p>
        <asp:Button ID="btnSubmit_Get" runat="server" Text="Request SecureKey via GET" /> &nbsp;&nbsp;&nbsp;&nbsp; 
        <asp:Button ID="btnSubmit_Post" runat="server" Text="Request SecureKey via POST" /> &nbsp;&nbsp;&nbsp;&nbsp; 
        <asp:Button ID="btnSubmit_ASYNC" runat="server" Text="Request SecureKey via ASYNC" />
    </p>
    <pre>Perform a CTRL-Click to run the link in a new browser.</pre>
    <p>
        <asp:HyperLink ID="lnkSecureKey" runat="server"></asp:HyperLink>
    </p>
<br /><br /><br />
<p><font color="#990033" style="font-weight:bold">Debug Information: </font></p>
    <p>
        SecureKey link:&nbsp;&nbsp; <asp:Label ID="lnkDebug" runat="server"></asp:Label>
    </p>
    <br />
    <asp:Label ID="lblerror" runat="server"></asp:Label>
    </form>
</body>
</html>
