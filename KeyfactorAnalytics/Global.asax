<%@ Application CodeBehind="Global.asax.vb" Inherits="rdWeb.Global" Language="VB"%>
<script RunAt="server">

Sub Application_Error(sender As Object, e As EventArgs)
Dim lastError As Exception = Server.GetLastError()
If TypeOf lastError Is HttpRequestValidationException Then
Response.Redirect("~/RequestValidationError.aspx")
End If
End Sub

</script>