<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<!-- Results will be XML -->
  <xsl:output method="xml" />
  <xsl:template match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates select="node()|@*"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="@Status[. = 0]">
	  <xsl:attribute name="{name()}">
        <xsl:text>Unknown</xsl:text>
	  </xsl:attribute>
  </xsl:template>
  <xsl:template match="@Status[. = 1]">
	  <xsl:attribute name="{name()}">
        <xsl:text>Valid</xsl:text>
	  </xsl:attribute>
  </xsl:template>
  <xsl:template match="@Status[. = 2]">
	  <xsl:attribute name="{name()}">
        <xsl:text>Stale</xsl:text>
	  </xsl:attribute>
  </xsl:template>
  <xsl:template match="@Status[. = 3]">
	  <xsl:attribute name="{name()}">
        <xsl:text>Expired</xsl:text>
	  </xsl:attribute>
  </xsl:template>
  <xsl:template match="@Status[. = 4]">
	  <xsl:attribute name="{name()}">
        <xsl:text>Warning</xsl:text>
	  </xsl:attribute>
  </xsl:template>
  <xsl:template match="@Status[. = 5]">
	  <xsl:attribute name="{name()}">
        <xsl:text>Unavailable</xsl:text>
	  </xsl:attribute>
  </xsl:template>
  <xsl:template match="@endpointType[. = 1]">
    <xsl:attribute name="{name()}">
	  <xsl:text>CRL</xsl:text>
	</xsl:attribute>
  </xsl:template>
  <xsl:template match="@endpointType[. = 2]">
    <xsl:attribute name="{name()}">
	  <xsl:text>OCSP</xsl:text>
	</xsl:attribute>
  </xsl:template>
</xsl:stylesheet>