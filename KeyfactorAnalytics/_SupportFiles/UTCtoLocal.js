﻿$(document).ready(function () {
    var labArray = $('.lastSeenDateTime').toArray();
    labArray.forEach(function (lab) {
        var newLab = $(lab).text();
        if (newLab !== "") {
            var newText = UTCtoLocal(newLab);
            $(lab).text(newText);
        }
    });
});

function UTCtoLocal(datestring) {
    var date = new Date(datestring);
    var newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);
    if (newDate.getDate() !== date.getDate()) {
        newDate.setDate(date.getDate())
    }
    var offset = date.getTimezoneOffset() / 60;
    var hours = date.getHours();
    if (date.getYear() == 0) {
        return 'Never';
    }
    newDate.setHours(hours - offset);
    return newDate.toLocaleString(window.navigator.language);
}

var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
var observer = new MutationObserver(function (mutations) {
    mutations.forEach(function (mutation) {
        if (mutation.type === "attributes") {
            var labArray = $('.lastSeenDateTime').toArray();
            labArray.forEach(function (lab) {
                var newLab = $(lab).text();
                if (newLab !== "" && newLab.indexOf('T') != -1) {
                    var newText = UTCtoLocal(newLab);
                    $(lab).text(newText);
                }
            });
        }
    });
});
function addObserverIfDesiredNodeAvailable() {
    var element = document.querySelector('iframe');
    if (!element) {
        //The node we need does not exist yet.
        //Wait 500ms and try again
        window.setTimeout(addObserverIfDesiredNodeAvailable, 500);
        return;
    }
    observer.observe(element, {
        attributes: true //configure it to listen to attribute changes
    });
}
addObserverIfDesiredNodeAvailable();





