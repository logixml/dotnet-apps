var displayFilter = function (){
	var itemInFilter = $(".select2-selection__rendered > li[title]").length;
	
	if (itemInFilter > 0 && !$("#bodyFilters").is(":visible") ){
		$("#filAction").trigger("click");
	}
}

var resetFilter = function (arrObj){
	arrObj.forEach(function(elem){
		elem.val("");
		elem.trigger("change");
	});
}

var updateFilter= function (elem, valueClicked){
//	var elem = [$("#ddlMultiSelect_state");
//	var valueClicked = "CA";
	var currentSelected = elem.val();
	if (!currentSelected.includes(valueClicked)){
		currentSelected.push(valueClicked);
	}
	elem.val(currentSelected);
	elem.trigger("change");
	displayFilter();
}

var resetBordereauxFilter = function (){
	$("input[id^='Product_rdList'],[id^='State_rdList'],[id^='Year_rdList'],[id^='Month_rdList'][type='checkbox']").filter(":checked").each(function(){
		$(this).trigger("click");
	});
}

$(document).ready(function() {	
	displayFilter();
});
