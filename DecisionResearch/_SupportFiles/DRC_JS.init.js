var languageLookup = {
  "": {
    "Limit by State": "Limit by State",
	"Limit by Product": "Limit by Product",
	"Limit by Adjuster": "Limit by Adjuster",
	"for": " for ",
	"Limit by Agency": "Limit by Agency",
	"Limit by Pay Plan": "Limit by Pay Plan",
	"Limit by Month Year": "Limit by Month Year",
	"Claims Overview": "Claims Overview",
	"Claims Loss Development": "Claims Loss Development",
	"Incurred Loss - Loss Triangle (Incremental)": "Incurred Loss - Loss Triangle (Incremental)",
	"Incurred Loss - Loss Triangle (Cumulative)": "Incurred Loss - Loss Triangle (Cumulative)",
	"Coverage Type": "Coverage Type",
	"Catastrophe": "Catastrophe",
	"Months": "Months",
	"Show All": "Show All",
	"Limit by Pay Plan": "Limit by Pay Plan",
	"Accident Year": "Accident Year",
	"State": "State",
	"Product Type": "Product Type",
	"Paid Loss": "Paid Loss",
	"Claim Number": "Claim Number"
     }  ,
  "en-CA": {
    "Limit by State": "Limit by State",
	"Limit by Product": "Limit by Product",
	"Limit by Adjuster": "Limit by Adjuster",
	"for": " for ",
	"Limit by Agency": "Limit by Agency",
	"Limit by Pay Plan": "Limit by Pay Plan",
	"Limit by Month Year": "Limit by Month Year",
	"Claims Overview": "Claims Overview",
	"Claims Loss Development": "Claims Loss Development",
	"Incurred Loss - Loss Triangle (Incremental)": "Incurred Loss - Loss Triangle (Incremental)",
	"Incurred Loss - Loss Triangle (Cumulative)": "Incurred Loss - Loss Triangle (Cumulative)",
	"Coverage Type": "Coverage Type",
	"Catastrophe": "Catastrophe",
	"Months": "Months",
	"Show All": "Show All",
	"Limit by Pay Plan": "Limit by Pay Plan",
	"Accident Year": "Accident Year",
	"State": "State",
	"Product Type": "Product Type",
	"Paid Loss": "Paid Loss",
	"Claim Number": "Claim Number" 
     }  ,
  "fr-CA": {
    "Limit by State": "Limite par Etat",
	"Limit by Product": "Limite par produit",
	"Limit by Adjuster": "Par ajusteur",
	"for": " pour ",
	"Limit by Agency": "Limite par agence",
	"Limit by Pay Plan": "Plan de limite par paye",
	"Limit by Month Year": "Limite par Mois Année",
	"Claims Overview": "Aperçu des réclamations",
	"Claims Loss Development": "Développement de sinistres",
	"Incurred Loss - Loss Triangle (Incremental)": "Perte encourue - Triangle de perte (incrémental)",
	"Incurred Loss - Loss Triangle (Cumulative)": "Perte encourue - Triangle de pertes (cumulatif)",
	"Coverage Type": "Type de couverture",
	"Catastrophe": "Catastrophe",
	"Months": "Mois",
	"Show All": "Montre tout",
	"Limit by Pay Plan": "Plan de limite par paye",
	"Accident Year": "Année d'accident",
	"State": "Etat",
	"Product Type": "type de produit",
	"Paid Loss": "Perte payée",
	"Claim Number": "Numéro de réclamation" 
     },
  "zh": {
    "Limit by State": "按國家限制",
	"Limit by Product": "按產品限制",
	"Limit by Adjuster": "由調整員",
	"for": " 對於 ",
	"Limit by Agency": "由代理商限制",
	"Limit by Pay Plan": "按付款計劃限制",
	"Limit by Month Year": "按月限制",
	"Claims Overview": "索賠概述",
	"Claims Loss Development": "索賠損失發展",
	"Incurred Loss - Loss Triangle (Incremental)": "發生損失 - 損失三角形（增量）",
	"Incurred Loss - Loss Triangle (Cumulative)": "發生損失 - 損失三角形（累積）",
	"Coverage Type": "覆蓋類型",
	"Catastrophe": "災難",
	"Months": "月",
	"Show All": "顯示所有",
	"Limit by Pay Plan": "按付款計劃限制",
	"Accident Year": "意外年",
	"State": "州",
	"Product Type": "產品類別",
	"Paid Loss": "有償損失",
	"Claim Number": "索賠號" 
     }
}

var translate = function (lang, wordDescription){

	//console.log(languageLookup['' + lang + '']['' + wordDescription + '']);
	return languageLookup['' + lang + '']['' + wordDescription + ''];
}

