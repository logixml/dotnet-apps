// Init the daterange picker tool
$('span#daterange').daterangepicker({
    format: 'MM/DD/YYYY',
 		startDate: moment('@Request.PeriodStart~'),
    	endDate: moment('@Request.PeriodEnd~'),
		//startDate: moment(new Date('07/01/2017')),
    	//endDate: moment(new Date('07/31/2018')),
		showDropdowns: true,
		alwaysShowCalendars: true,
        timePicker: false,
        applyClass: 'applyBtn btn btn-small btn-sm btn-primary',
       	opens: 'left',
		minDate: new Date("1/1/2010"),
		maxDate: new Date(),
		//minYear: 2010,
		//maxYear: 2019,
		//maxYear: parseInt(moment().format('YYYY')),
    	ranges: {
			// You can create your own date ranges here. Look at the moment.js documentation for more information.
			'Today': [moment(), moment()],
			'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'This Week': [moment().startOf('week'), moment().endOf('week')],
			'Last Week': [moment().subtract(1, 'week').startOf('week'), moment().subtract(1, 'week').endOf('week')],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
			'This Quarter': [moment().startOf('quarter'), moment().endOf('quarter')],
			'Last Quarter': [moment().subtract(1, 'quarter').startOf('quarter'), moment().subtract(1, 'quarter').endOf('quarter')],
			'This Year': [moment().startOf('year'), moment().endOf('year')],
			'Last Year': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
			 
		}
}, 
function(start, end, label) {
    // Set the start and end variables for the refresh command to use.
    var start = start.format('MM/DD/YYYY');
    var end = end.format('MM/DD/YYYY');
           
    // Set the value of the hidden input fields
    $('#PeriodStart').val(start);
    $('#PeriodEnd').val(end);
     
    // Set the label text on the range picker widget
    $("span#daterange").text(start +' - '+ end);      
    //$('#btnApplyGlobalFilters').click();
});

$(document).ready(function(){
    if($('#PeriodStart').val() && $('#PeriodEnd').val()){
    	var start = moment(new Date($('#PeriodStart').val())).format("MM/DD/YYYY");
       	var end = moment(new Date($('#PeriodEnd').val())).format("MM/DD/YYYY");
        $("span#daterange").text(start +' - '+ end);
    }
});