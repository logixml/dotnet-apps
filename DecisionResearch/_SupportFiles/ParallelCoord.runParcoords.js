var resizeTimer;
$(window).resize(function() {
	clearTimeout(resizeTimer);
	resizeTimer = setTimeout(function() {
		$('#example').html('');
		runParcoords()
	}, 250);
});
$( document ).ready(function() {
	debugger;
	runParcoords();
});
function runParcoords() {
	// linear color scale
	d3ChartWdth = $('#d3ParacoordChart').width();
	
	//$("#example").height('310').width(d3ChartWdth);
	
	var blue_to_brown = d3.scale.linear()
    .domain([1,15])
	.range(["#99E4EE","#181818" ])
	//.domain([1,5,10,14])
	//.range(["#dbdbdb","#00bcd4","#9575cd","#757575"])
	.interpolate(d3.interpolateLab);
	
	// interact with this variable from a javascript console
	var pc1;
	
	// load csv file and create the chart
debugger;
	pc1 = d3.parcoords()("#example")
		.data(ParallelData)
		.bundlingStrength(0.1)
		.smoothness(0.2)
		.hideAxis(["Product"])
		//.hideAxis(["Paid Loss", "State"])
		.composite(["darken"])
		.color(function(d) { return blue_to_brown(d['Total']); })  // quantitative color scale
		.alpha(0.5)
		.render()
		.brushMode("1D-axes")  // enable brushing
		.interactive() // command line mode
		
	/*var explore_count = 0;
	var exploring = {};
	var explore_start = false;
	pc1.svg
		.selectAll(".dimension")
		.style("cursor", "pointer")
		.on("click", function(d) {
		exploring[d] = d in exploring ? false : true;
		event.preventDefault();
		if (exploring[d]) d3.timer(explore(d,explore_count));
		});*/
}
/*	
function explore(dimension,count) {
	if (!explore_start) {
	explore_start = true;
	d3.timer(pc1.brush);
	}
	var speed = (Math.round(Math.random()) ? 1 : -1) * (Math.random()+0.5);
	return function(t) {
	if (!exploring[dimension]) return true;
	var domain = pc1.yscale[dimension].domain();
	var width = (domain[1] - domain[0])/4;

	var center = width*1.5*(1+Math.sin(speed*t/1200)) + domain[0];

	pc1.yscale[dimension].brush.extent([
		d3.max([center-width*0.01, domain[0]-width/400]),  
		d3.min([center+width*1.01, domain[1]+width/100])  
	])(pc1.g()
		.filter(function(d) {
			return d == dimension;
		})
	);
	};
};*/