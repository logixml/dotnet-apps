﻿<?xml version="1.0" encoding="utf-8"?>
<Report
	Caption="Position | @Constant.TitleCaption~"
	ID="Utilities.Position"
	>
	<MasterReport
		Report="MasterReport"
	/>
	<Body>
		<Division
			Class="container-fluid"
			HtmlDiv="True"
			>
			<Division
				Class="display-1 border-bottom pb-3"
				HtmlDiv="True"
				ID="dvHeader"
				>
				<Label
					Caption="Position"
					Format="HTML"
					HtmlTag="H1"
				/>
				<Label
					Caption="Use these shorthand utilities for quickly configuring the position of an element."
					Class="lead mt-2"
					Format="HTML"
					HtmlTag="P"
				/>
			</Division>
			<IncludeSharedElement
				DefinitionFile="Shared.Elements"
				SharedElementID="_shrdRequirements"
				>
				<PassedSharedElement
					ID="pseRequirements"
					>
					<DataLayer
						ID="dlRequirement"
						Type="Static"
						>
						<IncludeSharedElement
							DefinitionFile="Shared.Elements"
							SharedElementID="_shrdFramework"
						/>
						<SequenceColumn
							ID="sqCol"
						/>
					</DataLayer>
				</PassedSharedElement>
			</IncludeSharedElement>
			<Division
				Class="dvExample"
				HtmlDiv="True"
				>
				<Division
					Class="card card-default mb-3"
					HtmlDiv="True"
					>
					<Division
						Class="card-header"
						HtmlDiv="True"
						>
						<Label
							Caption="Common Values"
							Class="h5"
						/>
					</Division>
					<Division
						Class="card-body"
						HtmlDiv="True"
						>
						<Label
							Caption="Quick positioning classes are available, though they are not responsive."
							Class="form-text text-muted"
							Format="HTML"
							HtmlTag="P"
						/>
						<Label
							Caption="&lt;div class=&quot;position-static&quot;&gt;...&lt;/div&gt;
&lt;div class=&quot;position-relative&quot;&gt;...&lt;/div&gt;
&lt;div class=&quot;position-absolute&quot;&gt;...&lt;/div&gt;
&lt;div class=&quot;position-fixed&quot;&gt;...&lt;/div&gt;
&lt;div class=&quot;position-sticky&quot;&gt;...&lt;/div&gt;"
							Class="html bg-white"
							HtmlTag="pre"
						/>
					</Division>
					<Division
						Class="card-footer p-0"
						HtmlDiv="True"
						>
						<Division
							HtmlDiv="True"
							ID="dvCopy"
							>
							<Division
								Class="copy-wrapper"
								HtmlDiv="True"
								>
								<Label
									Class="btnCopy ti-files"
									ID="btnCopy"
									>
									<Action
										Javascript="void(0)"
										Type="Javascript"
									/>
								</Label>
							</Division>
							<Label
								Caption="&lt;Division Class=&quot;position-static&quot; HtmlDiv=&quot;True&quot; /&gt;
&lt;Division Class=&quot;position-relative&quot; HtmlDiv=&quot;True&quot; /&gt;
&lt;Division Class=&quot;position-absolute&quot; HtmlDiv=&quot;True&quot; /&gt;
&lt;Division Class=&quot;position-fixed&quot; HtmlDiv=&quot;True&quot; /&gt;
&lt;Division Class=&quot;position-sticky&quot; HtmlDiv=&quot;True&quot; /&gt;"
								Class="html"
								HtmlTag="pre"
							/>
							<Label
								Caption="&lt;Division Class=&quot;position-static&quot; HtmlDiv=&quot;True&quot; /&gt;
&lt;Division Class=&quot;position-relative&quot; HtmlDiv=&quot;True&quot; /&gt;
&lt;Division Class=&quot;position-absolute&quot; HtmlDiv=&quot;True&quot; /&gt;
&lt;Division Class=&quot;position-fixed&quot; HtmlDiv=&quot;True&quot; /&gt;
&lt;Division Class=&quot;position-sticky&quot; HtmlDiv=&quot;True&quot; /&gt;"
								Class="ThemeHidden"
								ID="copyXMLElement"
							/>
						</Division>
					</Division>
				</Division>
				<Division
					Class="card card-default mb-3"
					HtmlDiv="True"
					>
					<Division
						Class="card-header"
						HtmlDiv="True"
						>
						<Label
							Caption="Fixed top"
							Class="h5"
						/>
					</Division>
					<Division
						Class="card-body"
						HtmlDiv="True"
						>
						<Label
							Caption="Position an element at the top of the viewport, from edge to edge. Be sure you understand the ramifications of fixed position in your project; you may need to add aditional CSS."
							Class="form-text text-muted"
							Format="HTML"
							HtmlTag="P"
						/>
						<Label
							Caption="&lt;div class=&quot;fixed-top&quot;&gt;...&lt;/div&gt;"
							Class="html bg-white"
							HtmlTag="pre"
						/>
					</Division>
					<Division
						Class="card-footer p-0"
						HtmlDiv="True"
						>
						<Division
							HtmlDiv="True"
							ID="dvCopy"
							>
							<Division
								Class="copy-wrapper"
								HtmlDiv="True"
								>
								<Label
									Class="btnCopy ti-files"
									ID="btnCopy"
									>
									<Action
										Javascript="void(0)"
										Type="Javascript"
									/>
								</Label>
							</Division>
							<Label
								Caption="&lt;Division Class=&quot;fixed-top&quot; HtmlDiv=&quot;True&quot; /&gt;"
								Class="html"
								HtmlTag="pre"
							/>
							<Label
								Caption="&lt;Division Class=&quot;fixed-top&quot; HtmlDiv=&quot;True&quot; /&gt;"
								Class="ThemeHidden"
								ID="copyXMLElement"
							/>
						</Division>
					</Division>
				</Division>
				<Division
					Class="card card-default mb-3"
					HtmlDiv="True"
					>
					<Division
						Class="card-header"
						HtmlDiv="True"
						>
						<Label
							Caption="Fixed bottom"
							Class="h5"
						/>
					</Division>
					<Division
						Class="card-body"
						HtmlDiv="True"
						>
						<Label
							Caption="Position an element at the bottom of the viewport, from edge to edge. Be sure you understand the ramifications of fixed position in your project; you may need to add aditional CSS."
							Class="form-text text-muted"
							Format="HTML"
							HtmlTag="P"
						/>
						<Label
							Caption="&lt;div class=&quot;fixed-bottom&quot;&gt;...&lt;/div&gt;"
							Class="html bg-white"
							HtmlTag="pre"
						/>
					</Division>
					<Division
						Class="card-footer p-0"
						HtmlDiv="True"
						>
						<Division
							HtmlDiv="True"
							ID="dvCopy"
							>
							<Division
								Class="copy-wrapper"
								HtmlDiv="True"
								>
								<Label
									Class="btnCopy ti-files"
									ID="btnCopy"
									>
									<Action
										Javascript="void(0)"
										Type="Javascript"
									/>
								</Label>
							</Division>
							<Label
								Caption="&lt;Division Class=&quot;fixed-bottom&quot; HtmlDiv=&quot;True&quot; /&gt;"
								Class="html"
								HtmlTag="pre"
							/>
							<Label
								Caption="&lt;Division Class=&quot;fixed-bottom&quot; HtmlDiv=&quot;True&quot; /&gt;"
								Class="ThemeHidden"
								ID="copyXMLElement"
							/>
						</Division>
					</Division>
				</Division>
				<Division
					Class="card card-default mb-3"
					HtmlDiv="True"
					>
					<Division
						Class="card-header"
						HtmlDiv="True"
						>
						<Label
							Caption="Sticky top"
							Class="h5"
						/>
					</Division>
					<Division
						Class="card-body"
						HtmlDiv="True"
						>
						<Label
							Class="form-text text-muted"
							Format="HTML"
							HtmlTag="P"
						/>
						<Label
							Caption="&lt;strong&gt;Microsoft Edge and IE11 will render &lt;code&gt;position: sticky&lt;/code&gt; as &lt;code&gt;position: relative&lt;/code&gt;.&lt;/strong&gt; As such, we wrap the styles in a &lt;code&gt;@supports&lt;/code&gt; query, limiting the stickiness to only browsers that properly can render it."
							Class="form-text text-muted"
							Format="HTML"
							HtmlTag="P"
						/>
						<Label
							Caption="&lt;div class=&quot;sticky-top&quot;&gt;...&lt;/div&gt;"
							Class="html bg-white"
							HtmlTag="pre"
						/>
					</Division>
					<Division
						Class="card-footer p-0"
						HtmlDiv="True"
						>
						<Division
							HtmlDiv="True"
							ID="dvCopy"
							>
							<Division
								Class="copy-wrapper"
								HtmlDiv="True"
								>
								<Label
									Class="btnCopy ti-files"
									ID="btnCopy"
									>
									<Action
										Javascript="void(0)"
										Type="Javascript"
									/>
								</Label>
							</Division>
							<Label
								Caption="&lt;Division Class=&quot;sticky-top&quot; HtmlDiv=&quot;True&quot; /&gt;"
								Class="html"
								HtmlTag="pre"
							/>
							<Label
								Caption="&lt;Division Class=&quot;sticky-top&quot; /&gt;"
								Class="ThemeHidden"
								ID="copyXMLElement"
							/>
						</Division>
					</Division>
				</Division>
			</Division>
		</Division>
	</Body>
	<ideTestParams/>
</Report>
