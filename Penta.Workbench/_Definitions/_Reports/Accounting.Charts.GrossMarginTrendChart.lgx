﻿<?xml version="1.0" encoding="utf-8"?>
<Report
	ID="Accounting.Charts.GrossMarginTrendChart"
	SecurityReportRightID="MENU_3151"
	>
	<DefaultRequestParams
		reportTargetFrameId="logixml-frame"
		workbenchName="Penta Workbench"
		workbenchReportDefinition="Default"
	/>
	<LocalData>
		<DataLayer
			ConnectionID="Penta"
			ID="GrossMarginTrendSql"
			Source="with pentaOptions as
   (select /*+ materialize */ pk_options.f_charValue(313) as enforceWOSecurity
      from dual),
   periods as
   (select ou_id,
           short_descr,
           d_prd_begin_date,
           d_prd_end_date,
           dense_rank( )
              over(partition by ou_id
                   order by d_prd_end_date desc
                  ) as rank_num
      from acctg_prd a
     where d_prd_begin_date &lt;= coalesce( to_date( &apos;@Request.throughDate~&apos;, &apos;yyyy/mm/dd&apos; ), trunc(sysdate) )
       and exists
           (select &apos;x&apos;
              from acctg_prd
             where ou_id = a.ou_id
               and d_prd_end_date &gt;= coalesce( to_date( &apos;@Request.throughDate~&apos;, &apos;yyyy/mm/dd&apos; ), trunc(sysdate) ))),
   ouPeriods as
   (select distinct ou_sel_subt.ou_id,
           periods.short_descr,
           periods.d_prd_begin_date,
           periods.d_prd_end_date,
           coalesce( to_number(&apos;@Request.NumberOfPeriods~&apos;), 12 ) - periods.rank_num + 1 as period_num
      from periods
           join ou_sel_subt
             on ou_sel_subt.par_ou_id = periods.ou_id
         join organization_unit
             on organization_unit.ou_id = ou_sel_subt.ou_id
     where periods.rank_num &lt;= coalesce( to_number(&apos;@Request.NumberOfPeriods~&apos;), 12 )
      and (REGEXP_LIKE( &apos;@Request.leOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.le_ou_id in (@SingleQuote.Request.leOuId~))
       and (REGEXP_LIKE( &apos;@Request.balSheetOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.bal_sheet_ou_id in (@SingleQuote.Request.balSheetOuId~))
       and (REGEXP_LIKE( &apos;@Request.ouId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.ou_id in (@SingleQuote.Request.ouId~))),
   jobData as
   (select &apos;Job&apos;                                as work_type,
           gross_margin_summary.job_id,
           null as maint_contract_id,
           null as wo_id,
           null as wo_ou_id,
           organization_unit.ou_id,
           organization_unit.bal_sheet_ou_id,
           ouPeriods.d_prd_end_date,
           ouPeriods.short_descr,
           sum( gross_margin_summary.cost_amt ) as cost_amt,
           sum( gross_margin_summary.earn_amt ) as earn_amt
      from job
           join organization_unit
             on organization_unit.ou_id = job.ou_id
           join ouPeriods
             on ouPeriods.ou_id = job.ou_id
           join gross_margin_summary
             on gross_margin_summary.job_id = job.job_id and
                gross_margin_summary.d_end_date between ouPeriods.d_prd_begin_date and
                                                        ouPeriods.d_prd_end_date
     where ((job.pmgr_emp_id = &apos;@Request.projectManagerId~&apos; or &apos;@Request.projectManagerId~&apos; is null)
	       or (INSTR(&apos;@Request.projectManagerId~&apos;,&apos;-1&apos;) &gt; 0 and job.pmgr_emp_id = &apos;@Session.EmpId~&apos;))
      and (REGEXP_LIKE( &apos;@Request.serviceTypeCd~&apos;, &apos;(^|,)all($|,)&apos; ))
       and ((job.salesperson_emp_id = &apos;@Request.salespersonId~&apos; or &apos;@Request.salespersonId~&apos; is null)
	   	       or (INSTR(&apos;@Request.salespersonId~&apos;,&apos;-1&apos;) &gt; 0 and job.salesperson_emp_id = &apos;@Session.EmpId~&apos;))
       and (REGEXP_LIKE( &apos;@Request.leOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.le_ou_id in (@SingleQuote.Request.leOuId~))
       and (REGEXP_LIKE( &apos;@Request.balSheetOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.bal_sheet_ou_id in (@SingleQuote.Request.balSheetOuId~))
       and (REGEXP_LIKE( &apos;@Request.ouId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.ou_id in (@SingleQuote.Request.ouId~))
       and (job.cus_id in (@SingleQuote.Request.cusId~) or &apos;@Request.cusId~&apos; is null)
       and (REGEXP_LIKE( &apos;@Request.jobTypeCd~&apos;, &apos;(^|,)all($|,)&apos; ) or job.jt_cd in (@SingleQuote.Request.jobTypeCd~))
       and (job.job_id in (@SingleQuote.Request.jobId~) or &apos;@Request.jobId~&apos; is null)
     group by gross_margin_summary.job_id,
           organization_unit.ou_id,
           organization_unit.bal_sheet_ou_id,
           ouPeriods.d_prd_end_date,
           ouPeriods.short_descr
    having sum( gross_margin_summary.cost_amt ) &lt;&gt; 0 or
           sum( gross_margin_summary.earn_amt ) &lt;&gt; 0),
   mcData as
   (select &apos;Maintenance Contract&apos;               as work_type,
           null as job_id,
           gross_margin_summary.maint_contract_id,
           null as wo_id,
           null as wo_ou_id,
           organization_unit.ou_id,
           organization_unit.bal_sheet_ou_id,
           ouPeriods.d_prd_end_date,
           ouPeriods.short_descr,
           sum( gross_margin_summary.cost_amt - coalesce( gross_margin_summary.non_mc_cost_amt, 0 )) as cost_amt,
           sum( gross_margin_summary.earn_amt ) as earn_amt
      from maint_contract
           join organization_unit
             on organization_unit.ou_id = maint_contract.ou_id
           join ouPeriods
             on ouPeriods.ou_id = maint_contract.ou_id
           join gross_margin_summary
             on gross_margin_summary.maint_contract_id = maint_contract.maint_contract_id and
                gross_margin_summary.d_end_date between ouPeriods.d_prd_begin_date and
                                                        ouPeriods.d_prd_end_date
     where ((maint_contract.salesperson_emp_id = &apos;@Request.salespersonId~&apos; or &apos;@Request.salespersonId~&apos; is null)
	       or (INSTR(&apos;@Request.salespersonId~&apos;,&apos;-1&apos;) &gt; 0 and maint_contract.salesperson_emp_id = &apos;@Session.EmpId~&apos;))
      and (REGEXP_LIKE( &apos;@Request.jobTypeCd~&apos;, &apos;(^|,)all($|,)&apos; ))
      and &apos;@Request.projectManagerId~&apos; is null
       and (REGEXP_LIKE( &apos;@Request.leOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.le_ou_id in (@SingleQuote.Request.leOuId~))
       and (REGEXP_LIKE( &apos;@Request.balSheetOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.bal_sheet_ou_id in (@SingleQuote.Request.balSheetOuId~))
       and (REGEXP_LIKE( &apos;@Request.ouId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.ou_id in (@SingleQuote.Request.ouId~))
       and (maint_contract.cus_id in (@SingleQuote.Request.cusId~) or &apos;@Request.cusId~&apos; is null)
       and (REGEXP_LIKE( &apos;@Request.serviceTypeCd~&apos;, &apos;(^|,)all($|,)&apos; ) or maint_contract.wo_service_type_cd in (@SingleQuote.Request.serviceTypeCd~))
       and (REGEXP_LIKE( &apos;@Request.maintContractId~&apos;, &apos;(^|,)all($|,)&apos; ) or maint_contract.maint_contract_id in (@SingleQuote.Request.maintContractId~))
     group by gross_margin_summary.maint_contract_id,
           organization_unit.ou_id,
           organization_unit.bal_sheet_ou_id,
           ouPeriods.d_prd_end_date,
           ouPeriods.short_descr
    having sum( gross_margin_summary.cost_amt - coalesce( gross_margin_summary.non_mc_cost_amt, 0 ) ) &lt;&gt; 0 or
           sum( gross_margin_summary.earn_amt ) &lt;&gt; 0),
   woData as
   (select case
              when maint_contract.maint_contract_id is not null then
                 &apos;Maintenance Contract&apos;
              else
                 &apos;Work Order&apos;
           end                                  as work_type,
           null as job_id,
           maint_contract.maint_contract_id,
           work_order.wo_id,
           work_order.ou_id                     as wo_ou_id,
           organization_unit.ou_id,
           organization_unit.bal_sheet_ou_id,
           ouPeriods.d_prd_end_date,
           ouPeriods.short_descr,
           sum(case when maint_contract.maint_contract_id is null 
                then gross_margin_summary.cost_amt
                else gross_margin_summary.cost_amt - coalesce( gross_margin_summary.non_mc_cost_amt, 0 )
           end) as cost_amt,
           sum( gross_margin_summary.earn_amt ) as earn_amt
      from work_order
           join wo_service_type
             on wo_service_type.wo_service_type_cd = work_order.wo_service_type_cd
           left outer join maint_contract
             on maint_contract.maint_contract_id = work_order.maint_contract_id
           join organization_unit
             on organization_unit.ou_id =
                   case
                      when maint_contract.maint_contract_id is not null and
                           ( maint_contract.fixed_price_cd = &apos;Y&apos; or
                             wo_service_type.always_use_mc_rev_rec_cd = &apos;Y&apos; ) then
                         maint_contract.ou_id
                      else
                         work_order.ou_id
                   end
           join ouPeriods
             on ouPeriods.ou_id = organization_unit.ou_id
           join gross_margin_summary
             on gross_margin_summary.wo_sa_num = work_order.wo_sa_num and
                gross_margin_summary.d_end_date between ouPeriods.d_prd_begin_date and
                                                        ouPeriods.d_prd_end_date
     where ((work_order.pmgr_emp_id = &apos;@Request.projectManagerId~&apos; or &apos;@Request.projectManagerId~&apos; is null)
	       or (INSTR(&apos;@Request.projectManagerId~&apos;,&apos;-1&apos;) &gt; 0 and work_order.pmgr_emp_id = &apos;@Session.EmpId~&apos;))
	   and (REGEXP_LIKE( &apos;@Request.jobTypeCd~&apos;, &apos;(^|,)all($|,)&apos; ))
       and ((work_order.salesperson_emp_id = &apos;@Request.salespersonId~&apos; or &apos;@Request.salespersonId~&apos; is null)
	   	   or (INSTR(&apos;@Request.salespersonId~&apos;,&apos;-1&apos;) &gt; 0 and work_order.salesperson_emp_id = &apos;@Session.EmpId~&apos;))
       and (REGEXP_LIKE( &apos;@Request.leOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.le_ou_id in (@SingleQuote.Request.leOuId~))
       and (REGEXP_LIKE( &apos;@Request.balSheetOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.bal_sheet_ou_id in (@SingleQuote.Request.balSheetOuId~))
       and (REGEXP_LIKE( &apos;@Request.ouId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.ou_id in (@SingleQuote.Request.ouId~))
       and (work_order.invoice_cus_id in (@SingleQuote.Request.cusId~) or &apos;@Request.cusId~&apos; is null)
       and (REGEXP_LIKE( &apos;@Request.serviceTypeCd~&apos;, &apos;(^|,)all($|,)&apos; ) or work_order.wo_service_type_cd in (@SingleQuote.Request.serviceTypeCd~))
       and (REGEXP_LIKE( &apos;@Request.maintContractId~&apos;, &apos;(^|,)all($|,)&apos; ) or work_order.maint_contract_id in (@SingleQuote.Request.maintContractId~))
       and (REGEXP_LIKE( &apos;@Request.woId~&apos;, &apos;(^|,)all($|,)&apos; ) or work_order.wo_sa_num in (@SingleQuote.Request.woId~))
     group by maint_contract.maint_contract_id,
           work_order.wo_id,
           work_order.ou_id,
           organization_unit.ou_id,
           organization_unit.bal_sheet_ou_id,
           ouPeriods.d_prd_end_date,
           ouPeriods.short_descr
    having sum( case when maint_contract.maint_contract_id is null 
                     then gross_margin_summary.cost_amt
                     else gross_margin_summary.cost_amt - coalesce( gross_margin_summary.non_mc_cost_amt, 0 )
                end ) &lt;&gt; 0 or
           sum( gross_margin_summary.earn_amt ) &lt;&gt; 0
    union
    select &apos;Work Order&apos; as work_type,
           null as job_id,
           maint_contract.maint_contract_id,
           work_order.wo_id,
           work_order.ou_id                     as wo_ou_id,
           organization_unit.ou_id,
           organization_unit.bal_sheet_ou_id,
           ouPeriods.d_prd_end_date,
           ouPeriods.short_descr,
           sum(coalesce( gross_margin_summary.non_mc_cost_amt, 0 )) as cost_amt,
           0 as earn_amt
      from work_order
           join wo_service_type
             on wo_service_type.wo_service_type_cd = work_order.wo_service_type_cd
           left outer join maint_contract
             on maint_contract.maint_contract_id = work_order.maint_contract_id
           join organization_unit
             on organization_unit.ou_id =
                   case
                      when maint_contract.maint_contract_id is not null and
                           ( maint_contract.fixed_price_cd = &apos;Y&apos; or
                             wo_service_type.always_use_mc_rev_rec_cd = &apos;Y&apos; ) then
                         maint_contract.ou_id
                      else
                         work_order.ou_id
                   end
           join ouPeriods
             on ouPeriods.ou_id = organization_unit.ou_id
           join gross_margin_summary
             on gross_margin_summary.wo_sa_num = work_order.wo_sa_num and
                gross_margin_summary.d_end_date between ouPeriods.d_prd_begin_date and
                                                        ouPeriods.d_prd_end_date
     where ((work_order.pmgr_emp_id = &apos;@Request.projectManagerId~&apos; or &apos;@Request.projectManagerId~&apos; is null)
	       or (INSTR(&apos;@Request.projectManagerId~&apos;,&apos;-1&apos;) &gt; 0 and work_order.pmgr_emp_id = &apos;@Session.EmpId~&apos;))	       
      and (REGEXP_LIKE( &apos;@Request.jobTypeCd~&apos;, &apos;(^|,)all($|,)&apos; ))
       and ((work_order.salesperson_emp_id = &apos;@Request.salespersonId~&apos; or &apos;@Request.salespersonId~&apos; is null)
	   	   or (INSTR(&apos;@Request.salespersonId~&apos;,&apos;-1&apos;) &gt; 0 and work_order.salesperson_emp_id = &apos;@Session.EmpId~&apos;))	   
       and (REGEXP_LIKE( &apos;@Request.leOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.le_ou_id in (@SingleQuote.Request.leOuId~))
       and (REGEXP_LIKE( &apos;@Request.balSheetOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.bal_sheet_ou_id in (@SingleQuote.Request.balSheetOuId~))
       and (REGEXP_LIKE( &apos;@Request.ouId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.ou_id in (@SingleQuote.Request.ouId~))
       and (work_order.invoice_cus_id in (@SingleQuote.Request.cusId~) or &apos;@Request.cusId~&apos; is null)
       and (REGEXP_LIKE( &apos;@Request.serviceTypeCd~&apos;, &apos;(^|,)all($|,)&apos; ) or work_order.wo_service_type_cd in (@SingleQuote.Request.serviceTypeCd~))
       and (REGEXP_LIKE( &apos;@Request.maintContractId~&apos;, &apos;(^|,)all($|,)&apos; ) or work_order.maint_contract_id in (@SingleQuote.Request.maintContractId~))
       and (REGEXP_LIKE( &apos;@Request.woId~&apos;, &apos;(^|,)all($|,)&apos; ) or work_order.wo_sa_num in (@SingleQuote.Request.woId~))
       and maint_contract.maint_contract_id is not null
       and coalesce( gross_margin_summary.non_mc_cost_amt, 0 ) &lt;&gt; 0
     group by maint_contract.maint_contract_id,
           work_order.wo_id,
           work_order.ou_id,
           organization_unit.ou_id,
           organization_unit.bal_sheet_ou_id,
           ouPeriods.d_prd_end_date,
           ouPeriods.short_descr           
    having sum(coalesce( gross_margin_summary.non_mc_cost_amt, 0 )) &lt;&gt; 0
    ),
   combinedDetails as
   (select mcData.*,
           &apos;Y&apos; as entity_allowed,
           pk_ouSecurity.f_userAuthorized( &apos;EXT&apos;,&apos;3151&apos;, mcData.ou_id, &apos;@Function.UserName~&apos; ) as ou_allowed
      from mcData
     where (REGEXP_LIKE( &apos;@Request.workType~&apos;, &apos;(^|,)all($|,)&apos; ) or mcData.work_type in (@SingleQuote.Request.workType~))
     union all
    select jobData.*,
           pk_jobSecurity.f_userAuthorized( jobData.job_id, &apos;@Function.UserName~&apos; ) as entity_allowed,
           pk_ouSecurity.f_userAuthorized( &apos;EXT&apos;,&apos;3151&apos;, jobData.ou_id, &apos;@Function.UserName~&apos; ) as ou_allowed
      from jobData
     where (REGEXP_LIKE( &apos;@Request.workType~&apos;, &apos;(^|,)all($|,)&apos; ) or jobData.work_type in (@SingleQuote.Request.workType~))
     union all
    select woData.*,
           case pentaOptions.enforceWOSecurity
              when &apos;Y&apos; then
                 pk_woSecurity.f_userAuthorized( &apos;EXT&apos;,&apos;3151&apos;, woData.wo_id, woData.wo_ou_id, &apos;@Function.UserName~&apos; )
              else &apos;Y&apos;
           end as entity_allowed,
           pk_ouSecurity.f_userAuthorized( &apos;EXT&apos;,&apos;3151&apos;, woData.ou_id, &apos;@Function.UserName~&apos; ) as ou_allowed
      from woData
           cross join PentaOptions
     where (REGEXP_LIKE( &apos;@Request.workType~&apos;, &apos;(^|,)all($|,)&apos; ) or woData.work_type in (@SingleQuote.Request.workType~))),
   details as
   (select /*+ materialize */ *  -- forces Oracle to run the security routines not in predicate
      from combinedDetails),
   currencyCount as
   (select count( distinct organization_unit.currency_id ) as currency_count
      from details
           join organization_unit
             on organization_unit.bal_sheet_ou_id = details.bal_sheet_ou_id
     where organization_unit.currency_id is not null)
select upper(ouPeriods.short_descr) as short_descr,
       currencyCount.currency_count,
       coalesce( sum( details.earn_amt ), 0 ) -
          coalesce( sum( details.cost_amt ), 0 ) as gross_margin,
 	   case when coalesce( sum( details.earn_amt ), 0 ) &lt;&gt; 0 then
 		   round( ( coalesce( sum( details.earn_amt ), 0 ) -
 		   coalesce( sum( details.cost_amt ), 0 ) ) /
 		   sum( details.earn_amt ),4 )
 		 else 0
 		 end as gross_margin_pct,
       coalesce( sum( case when details.work_type = &apos;Maintenance Contract&apos;
                         then details.earn_amt
                         else 0
                      end), 0 ) -
          coalesce( sum( case when details.work_type = &apos;Maintenance Contract&apos;
                            then details.cost_amt
                            else 0
                         end ), 0 ) as maint_margin,
       coalesce( sum( case when details.work_type = &apos;Work Order&apos;
                         then details.earn_amt
                         else 0
                      end), 0 ) -
          coalesce( sum( case when details.work_type = &apos;Work Order&apos;
                            then details.cost_amt
                            else 0
                         end ), 0 ) as wo_margin,
       coalesce( sum( case when details.work_type = &apos;Job&apos;
                         then details.earn_amt
                         else 0
                      end), 0 ) -
          coalesce( sum( case when details.work_type = &apos;Job&apos;
                            then details.cost_amt
                            else 0
                          end ), 0 ) as job_margin
  from ouPeriods
       cross join currencyCount
       left outer join details
         on details.ou_id = ouPeriods.ou_id and
            details.d_prd_end_date = ouPeriods.d_prd_end_date and
            details.entity_allowed = &apos;Y&apos; and
            details.ou_allowed = &apos;Y&apos;
 group by upper(ouPeriods.short_descr),
       currencyCount.currency_count
 order by min( ouPeriods.d_prd_begin_date )"
			Type="SQL"
			>
			<DataLayerLink
				ID="dlnkChartDataTrend@Request.uniqueID~"
			/>
		</DataLayer>
	</LocalData>
	<ReportHeader/>
	<StyleSheet
		StyleSheet="styling.quicktip.css"
	/>
	<Body>
		<Label
			Caption="Attention: No data will be displayed until one or more Legal Entity OUs have been selected.  Use the Edit button to set the Legal Entity OU filter."
			Class="ThemeTextNegative,ThemeBold"
			>
			<ConditionalClass
				Class="ThemeHidden"
				Condition="&apos;@Request.leOuId~&apos; &lt;&gt; &apos;&apos;"
			/>
		</Label>
		<Label
			Caption="Attention: The values in your chart include amounts for more than one base currency.  Use the Edit button to limit to Legal Entity OUs with the same base currency."
			Class="ThemeHidden"
			>
			<ConditionalClass
				Class="ThemeBold ThemeTextNegative"
				Condition="@Local.CURRENCY_COUNT~ &gt; 1 "
			/>
		</Label>
		<Rows
			Width="95"
			WidthScale="%"
			>
			<Row>
				<Column
					Width="99"
					WidthScale="%"
					>
					<ChartCanvas
						ChartHeight="350"
						ChartOrientation="Default"
						ID="ChartCanvas1"
						NoDataCaption="No data was found using the selected filters."
						>
						<DataLayer
							ID="dlCombinedMarginTrendChart"
							LinkedDataLayerID="dlnkChartDataTrend@Request.uniqueID~"
							Type="Linked"
						/>
						<NoDataCaptionStyle
							FontColor="#0815C5"
							FontFamily="Arial"
							FontSize="12"
						/>
						<ChartXAxis
							AxisLineColor="#6c6d71"
							AxisLineColorTransparency="0"
							>
							<AxisLabelStyle
								FontAngle="45"
							/>
						</ChartXAxis>
						<Remark>
							<Resizer
								MinHeight="200"
								MinWidth="200"
							/>
						</Remark>
						<Series
							ChartXDataColumn="SHORT_DESCR"
							ChartXDataColumnType="Text"
							ChartYDataColumn="GROSS_MARGIN"
							ID="Series.Line.GrossMargin"
							LegendLabel="Gross Margin"
							Type="Line"
							>
							<InputSelection
								ID="ispGrossMargin"
								PointValueColumn="GROSS_MARGIN"
								SelectionType="ClickSinglePoint"
								Type="Point"
								>
								<InputSelectionEvent
									ID="iseGrossMargin"
									SelectionEvent="PointsSelected"
									Type="Point"
									>
									<Action
										FrameID="@Request.reportTargetFrameId~"
										ID="actGrossMarginReport"
										Process="OpenReport"
										TaskID="openACReport"
										Type="Process"
										>
										<WaitPage/>
										<LinkParams
											balSheetOuId="@Request.balSheetOuId~"
											cusId="@Request.cusId~"
											jobId="@Request.jobId~"
											jobTypeCd="@Request.jobTypeCd~"
											leOuId="@Request.leOuId~"
											maintContractId="@Request.maintContractId~"
											menuItem="3151"
											myReport="Accounting.Reports.GrossMarginTrendAnalysis"
											ouId="@Request.ouId~"
											projectManagerId="@Request.projectManagerId~"
											rdAgRefreshData="True"
											rdAgReset="@Request.rdAgReset~"
											reportName="Gross Margin Trend Details Report"
											reportTargetFrameId="@Request.reportTargetFrameId~"
											salespersonId="@Request.salespersonId~"
											serviceTypeCd="@Request.serviceTypeCd~"
											throughDate="@Request.throughDate~"
											woId="@Request.woId~"
											workType="@Request.workType~"
										/>
									</Action>
								</InputSelectionEvent>
							</InputSelection>
							<Quicktip
								ID="qtGrossMargin"
								>
								<QuicktipRow
									Caption="@Chart.SHORT_DESCR~"
									ID="qtRow1"
								/>
								<QuicktipRow
									Caption="Jobs: "
									Format="###,##0.00"
									ID="qtRow2"
									Value="@Chart.JOB_MARGIN~"
								/>
								<QuicktipRow
									Caption="Work Orders:"
									Format="###,##0.00"
									ID="qtRow3"
									Value="@Chart.WO_MARGIN~"
								/>
								<QuicktipRow
									Caption="Maintenance Contracts:"
									Format="###,##0.00"
									ID="qtRow4"
									Value="@Chart.MAINT_MARGIN~"
								/>
								<QuicktipRow
									ID="qtRow5"
									Value="-------------"
								/>
								<QuicktipRow
									Caption="Total Gross Margin:"
									Format="###,##0.00"
									ID="qtRow6"
									Value="@Chart.GROSS_MARGIN~"
								/>
								<QuicktipRow
									Caption="Total Gross Margin %:"
									Format="Percent"
									ID="qtRow7"
									Value="@Chart.GROSS_MARGIN_PCT~"
								/>
							</Quicktip>
						</Series>
						<ChartYAxis
							AxisCaption="Gross Margin"
							ID="yGrossMargin"
							OppositeSide="False"
						/>
						<Series
							BarBorderColor="#6c6d71"
							BarBorderColorTransparency="0"
							BarBorderRadius="0"
							BarBorderThickness="1"
							BarThickness="20"
							ChartXDataColumn="SHORT_DESCR"
							ChartXDataColumnType="Text"
							ChartYDataColumn="GROSS_MARGIN_PCT"
							Color="#91c653"
							ID="Series.Bar.GrossMarginPct"
							LegendLabel="Gross Margin %"
							LinkedToYAxisID="yGrossMarginPct"
							Transparency="7"
							Type="Bar"
							>
							<InputSelection
								ID="ispGrossMarginPCT"
								PointValueColumn="GROSS_MARGIN_PCT"
								SelectionType="ClickSinglePoint"
								Type="Point"
								>
								<InputSelectionEvent
									ID="iseGrossMargin"
									SelectionEvent="PointsSelected"
									Type="Point"
									>
									<Action
										FrameID="@Request.reportTargetFrameId~"
										ID="actGrossMarginReport"
										Process="OpenReport"
										TaskID="openACReport"
										Type="Process"
										>
										<WaitPage/>
										<LinkParams
											balSheetOuId="@Request.balSheetOuId~"
											cusId="@Request.cusId~"
											jobId="@Request.jobId~"
											jobTypeCd="@Request.jobTypeCd~"
											leOuId="@Request.leOuId~"
											maintContractId="@Request.maintContractId~"
											menuItem="3151"
											myReport="Accounting.Reports.GrossMarginTrendAnalysis"
											ouId="@Request.ouId~"
											projectManagerId="@Request.projectManagerId~"
											rdAgRefreshData="True"
											rdAgReset="@Request.rdAgReset~"
											reportName="Gross Margin Trend Details Report"
											reportTargetFrameId="@Request.reportTargetFrameId~"
											salespersonId="@Request.salespersonId~"
											serviceTypeCd="@Request.serviceTypeCd~"
											throughDate="@Request.throughDate~"
											woId="@Request.woId~"
											workType="@Request.workType~"
										/>
									</Action>
								</InputSelectionEvent>
							</InputSelection>
							<Quicktip
								ID="qtGrossMarginPct"
								>
								<QuicktipRow
									Caption="@Chart.SHORT_DESCR~"
									ID="qtRow1"
								/>
								<QuicktipRow
									Caption="Jobs: "
									Format="###,##0.00"
									ID="qtRow2"
									Value="@Chart.JOB_MARGIN~"
								/>
								<QuicktipRow
									Caption="Work Orders:"
									Format="###,##0.00"
									ID="qtRow3"
									Value="@Chart.WO_MARGIN~"
								/>
								<QuicktipRow
									Caption="Maintenance Contracts:"
									Format="###,##0.00"
									ID="qtRow4"
									Value="@Chart.MAINT_MARGIN~"
								/>
								<QuicktipRow
									ID="qtRow5"
									Value="-------------"
								/>
								<QuicktipRow
									Caption="Total Gross Margin:"
									Format="###,##0.00"
									ID="qtRow6"
									Value="@Chart.GROSS_MARGIN~"
								/>
								<QuicktipRow
									Caption="Total Gross Margin %:"
									Format="Percent"
									ID="qtRow7"
									Value="@Chart.GROSS_MARGIN_PCT~"
								/>
							</Quicktip>
						</Series>
						<ChartYAxis
							AxisCaption="Gross Margin %"
							ID="yGrossMarginPct"
							OppositeSide="True"
							>
							<AxisCaptionStyle
								CaptionSpacing="75"
								FontAngle="90"
							/>
							<AxisLabelStyle
								Format="###,##0%"
							/>
						</ChartYAxis>
						<ChartCanvasLegend
							LegendOrientation="Horizontal"
							ShowAllThreshold="0"
						/>
					</ChartCanvas>
				</Column>
			</Row>
		</Rows>
	</Body>
	<ReportFooter/>
	<ideTestParams
		balSheetOuId=""
		cusId=""
		jobId=""
		jobTypeCd=""
		leOuId=""
		maintContractId=""
		NumberOfPeriods=""
		ouId=""
		projectManagerId=""
		rdAgReset=""
		reportTargetFrameId=""
		salespersonId=""
		serviceTypeCd=""
		throughDate=""
		uniqueID=""
		woId=""
		workType=""
	/>
</Report>
