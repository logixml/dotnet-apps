﻿<?xml version="1.0" encoding="utf-8"?>
<Report
	Caption="@Request.reportName~"
	ID="Accounting.Reports.CurrentAssetsAnalysis"
	>
	<MasterReport
		Report="DetailMasterReport"
	/>
	<IncludeScriptFile
		ID="loadJqueryEngine"
		IncludedScriptFile="Javascript.jquery-3.3.1.min.js"
	/>
	<IncludeScriptFile
		ID="loadJqueryUI"
		IncludedScriptFile="Javascript.jquery-ui.min.js"
	/>
	<IncludeScriptFile
		ID="loadjalert"
		IncludedScriptFile="Javascript.jquery.alert.js"
	/>
	<IncludeScriptFile
		ID="modifyAgTabBackground"
		IncludedScriptFile="Javascript.ModifyAgTabBackground.js"
	/>
	<LocalData>
		<DataLayer
			ID="dlDomains"
			LinkedDataLayerID="dlnkDomains"
			Type="Linked"
		/>
	</LocalData>
	<DefaultRequestParams
		factCols="0,colACCOUNT_BALANCE_BASE,colACCOUNT_BALANCE_SOURCE,colEXCHANGE_RATE"
		reportName="=iif( &quot;@Request.reportName~&quot; &lt;&gt; &quot;&quot;, &quot;@Request.reportName~&quot;, &quot;Combined Gross Margin Report&quot; )"
	/>
	<StyleSheet
		StyleSheet="styling.jquery.alerts.css"
	/>
	<StyleSheet
		StyleSheet="styling.analysisgrid.css"
	/>
	<IncludeSharedElement
		DefinitionFile="SharedElements.seReportHeaderExport"
		SharedElementID="seReportHeaderExport"
	/>
	<Body>
		<IncludeSharedElement
			DefinitionFile="SharedElements.seSavedBookmarks"
			SharedElementID="seSavedBookmarks"
		/>
		<IncludeSharedElement
			DefinitionFile="Accounting.SharedElements"
			SharedElementID="CommonHiddenElements"
		/>
		<InputHidden
			DefaultValue="@Request.assetAcctNum~"
			ID="assetAcctNum"
		/>
		<InputHidden
			DefaultValue="@Request.cashAcctNum~"
			ID="cashAcctNum"
		/>
		<InputHidden
			DefaultValue="@Request.cashActivityDays~"
			ID="cashActivityDays"
		/>
		<InputHidden
			DefaultValue="@Request.currentAPDays~"
			ID="currentAPDays"
		/>
		<InputHidden
			DefaultValue="@Request.currentARDays~"
			ID="currentARDays"
		/>
		<InputHidden
			DefaultValue="@Request.liabilityAcctNum~"
			ID="liabilityAcctNum"
		/>
		<InputHidden
			DefaultValue="@Request.lineOfCreditDrawAcctNum~"
			ID="lineOfCreditDrawAcctNum"
		/>
		<InputHidden
			DefaultValue="@Request.lineOfCreditLimit~"
			ID="lineOfCreditLimit"
		/>
		<InputHidden
			DefaultValue="@Request.templateBookmarkID~"
			ID="templateBookmarkID"
		/>
		<InputHidden
			DefaultValue="Accounting.Reports.CurrentAssetsAnalysis"
			ID="myReport"
		/>
		<AnalysisGrid
			AjaxPaging="True"
			BatchSelection="True"
			ID="agCurrentAssets"
			MaxRowsExport="-1"
			ShowPageNumber="True"
			SortArrows="True"
			TemplateModifierFile="TMFs._PentaAnalysisGrid.xml"
			>
			<DataLayer
				ConnectionID="Penta"
				ID="agDataLayer"
				Source="with systemOptions as
(
   select /*+ materialize */
          pk_options.f_charValue( 44 ) as multi_curr
     from dual
),
exchangeRates as
(
   select /*+ materialize */
          source_currency.currency_id source_currency_id,
          base_currency.currency_id base_currency_id,
          pk_currency.f_spotExchRate(
             source_currency.currency_id,
             base_currency.currency_id,
             coalesce( to_date( &apos;@Request.throughDate~&apos;, &apos;yyyy/mm/dd&apos; ),
                       trunc( sysdate ) ) ) as exchange_rate
     from currency base_currency
          cross join currency source_currency
),
selectedOus as
(
   select ou.ou_id,
          ou.name,
          ou.currency_id,
          bs_ou.ou_id     as balance_sheet_ou_id,
          bs_ou.name      as balance_sheet_ou_name,
          le_ou.ou_id     as legal_entity_ou_id,
          le_ou.name      as legal_entity_ou_name
     from organization_unit ou
          join organization_unit bs_ou
            on bs_ou.ou_id = ou.bal_sheet_ou_id
          join organization_unit le_ou
            on le_ou.ou_id = ou.le_ou_id
    where (REGEXP_LIKE( &apos;@Request.leOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or le_ou.ou_id in (@SingleQuote.Request.leOuId~))
      and (REGEXP_LIKE( &apos;@Request.balSheetOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or bs_ou.ou_id in (@SingleQuote.Request.balSheetOuId~))
      and pk_ouSecurity.f_userAuthorized( &apos;EXT&apos;,&apos;@Request.menuItem~&apos;, ou.ou_id, &apos;@Function.UserID~&apos; ) = &apos;Y&apos;
)
select selectedOus.legal_entity_ou_id,
       selectedOus.legal_entity_ou_name,
       selectedOus.balance_sheet_ou_id,
       selectedOus.balance_sheet_ou_name,
       account.acct_num                        as account_number,
       account.name                            as account_description,
       base_currency.currency_id               as base_currency_id,
       base_currency.descr                     as base_currency_description,
       source_currency.currency_id             as source_currency_id,
       source_currency.descr                   as source_currency_description,
       coalesce(exchangeRates.exchange_rate,
                1.000000)                      as exchange_rate,
       coalesce(sum(
            ar_bh_base.amt - pk_arDoc.f_received(
                               ar_bh_base.doc_num,
                               coalesce(to_date(&apos;@Request.throughDate~&apos;, &apos;yyyy/mm/dd&apos;), trunc(sysdate)),
                               ar_bh_base.distr_num,
                               ar_bh_base.seq_num)), 0.00) as account_balance_source,
       round(
          coalesce(sum(
            ar_bh_base.amt - pk_arDoc.f_received(
                               ar_bh_base.doc_num,
                               coalesce(to_date(&apos;@Request.throughDate~&apos;, &apos;yyyy/mm/dd&apos;), trunc(sysdate)),
                               ar_bh_base.distr_num,
                               ar_bh_base.seq_num)), 0.00) *
          coalesce(exchangeRates.exchange_rate,
                   1.000000), 2)                 as account_balance_base
  from selectedOus
       cross join systemOptions
       left outer join currency base_currency
         on base_currency.currency_id = selectedOus.currency_id and
            systemOptions.multi_curr = &apos;Y&apos;
       join ar_bh_base
         on ar_bh_base.dr_ou_id = selectedOus.ou_id
       join account
         on account.acct_num = ar_bh_base.dr_acct_num||&apos;&apos; and
            account.edit_rule_cd = &apos;AR&apos;
       left outer join currency source_currency
         on source_currency.currency_id = ar_bh_base.source_currency_id and
            systemOptions.multi_curr = &apos;Y&apos;
       left outer join exchangeRates
         on systemOptions.multi_curr = &apos;Y&apos; and
            exchangeRates.base_currency_id = selectedOus.currency_id and
            exchangeRates.source_currency_id = ar_bh_base.source_currency_id and
            exchangeRates.exchange_rate &lt;&gt; 0
  where (ar_bh_base.ar_stat_cd &lt;&gt; &apos;C&apos; or
        ar_bh_base.d_paid_date &gt; coalesce(to_date(&apos;@Request.throughDate~&apos;, &apos;yyyy/mm/dd&apos;), trunc(sysdate)))
   and (REGEXP_LIKE( &apos;@Request.assetAcctNum~&apos;, &apos;(^|,)all($|,)&apos; ) or ar_bh_base.dr_acct_num||&apos;&apos; in (@SingleQuote.Request.assetAcctNum~))
   and ar_bh_base.d_distr_date &lt;= coalesce(to_date(&apos;@Request.throughDate~&apos;, &apos;yyyy/mm/dd&apos;), trunc(sysdate))
	  and  pk_arInvoice.f_dueDate( ar_bh_base.cus_id, ar_bh_base.cus_invoice_num ) between  
																	 coalesce(to_date(&apos;@Request.throughDate~&apos;, &apos;yyyy/mm/dd&apos;), trunc(sysdate)) -
                                                                     coalesce(to_number(&apos;@Request.invoicesPastDueBy~&apos;), 120)
																	 and 
																	 coalesce(to_date(&apos;@Request.throughDate~&apos;, &apos;yyyy/mm/dd&apos;), trunc(sysdate)) +
                                                                     coalesce(to_number(&apos;@Request.currentARDays~&apos;), 30)
 group by selectedOus.legal_entity_ou_id,
          selectedOus.legal_entity_ou_name,
          selectedOus.balance_sheet_ou_id,
          selectedOus.balance_sheet_ou_name,
          account.acct_num,
          account.name,
          base_currency.currency_id,
          base_currency.descr,
          source_currency.currency_id,
          source_currency.descr,
          coalesce(exchangeRates.exchange_rate, 1.000000)
union all
select selectedOus.legal_entity_ou_id,
       selectedOus.legal_entity_ou_name,
       selectedOus.balance_sheet_ou_id,
       selectedOus.balance_sheet_ou_name,
       account.acct_num                        as account_number,
       account.name                            as account_description,
       base_currency.currency_id               as base_currency_id,
       base_currency.descr                     as base_currency_description,
       source_currency.currency_id             as source_currency_id,
       source_currency.descr                   as source_currency_description,
       coalesce(exchangeRates.exchange_rate,
                1.000000)                      as exchange_rate,
       coalesce(sum(auditor_ledger.amt), 0.00) as account_balance_source,
       round(
          coalesce(sum(auditor_ledger.amt), 0.00) *
          coalesce(exchangeRates.exchange_rate,
                   1.000000), 2)               as account_balance_base
  from selectedOus
       cross join systemOptions
       left outer join currency base_currency
         on base_currency.currency_id = selectedOus.currency_id and
            systemOptions.multi_curr = &apos;Y&apos;
       join auditor_ledger
         on auditor_ledger.ou_id = selectedOus.ou_id
       join account
         on account.acct_num = auditor_ledger.acct_num||&apos;&apos; and
            ( account.edit_rule_cd is null or account.edit_rule_cd &lt;&gt; &apos;AR&apos; )and 
			 account.acct_class_cd = &apos;A&apos;
       left outer join currency source_currency
         on source_currency.currency_id = auditor_ledger.currency_id and
            systemOptions.multi_curr = &apos;Y&apos;
       left outer join exchangeRates
         on systemOptions.multi_curr = &apos;Y&apos; and
            exchangeRates.base_currency_id = base_currency.currency_id and
            exchangeRates.source_currency_id = source_currency.currency_id and
            exchangeRates.exchange_rate &lt;&gt; 0
  where (REGEXP_LIKE( &apos;@Request.assetAcctNum~&apos;, &apos;(^|,)all($|,)&apos; ) or auditor_ledger.acct_num||&apos;&apos; in (@SingleQuote.Request.assetAcctNum~))
   and auditor_ledger.d_distr_date &lt;= coalesce(to_date(&apos;@Request.throughDate~&apos;, &apos;yyyy/mm/dd&apos;), trunc(sysdate))
 group by selectedOus.legal_entity_ou_id,
          selectedOus.legal_entity_ou_name,
          selectedOus.balance_sheet_ou_id,
          selectedOus.balance_sheet_ou_name,
          account.acct_num,
          account.name,
          base_currency.currency_id,
          base_currency.descr,
          source_currency.currency_id,
          source_currency.descr,
          coalesce(exchangeRates.exchange_rate, 1.000000)
order by 1, 3, 5





"
				Type="SQL"
			/>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="BASE_CURRENCY_DESCRIPTION"
				DataType="Text"
				Header="Base Currency Description"
				ID="colBASE_CURRENCY_DESCRIPTION"
				NoAgAggregates="True"
				NoAgCrosstabAggregate="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="LEGAL_ENTITY_OU_ID"
				DataType="@Local.OU_DATA_TYPE~"
				Header="Legal Entity OU ID"
				ID="colLEGAL_ENTITY_OU_ID"
				NoAgAggregates="True"
				NoAgCrosstabAggregate="True"
				>
				<ExcelColumnFormat
					DataType="@Local.OU_DATA_TYPE~"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="@Local.OU_EXCEL_FORMAT~"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="LEGAL_ENTITY_OU_NAME"
				DataType="Text"
				Header="Legal Entity OU Name"
				ID="colLEGAL_ENTITY_OU_NAME"
				NoAgAggregates="True"
				NoAgCrosstabAggregate="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="SOURCE_CURRENCY_DESCRIPTION"
				DataType="Text"
				Header="Source Currency Description"
				ID="colSOURCE_CURRENCY_DESCRIPTION"
				NoAgAggregates="True"
				NoAgCrosstabAggregate="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<Note
				Note="Start of Default Columns"
			/>
			<AnalysisGridColumn
				ColumnVisible="True"
				DataColumn="BALANCE_SHEET_OU_ID"
				DataType="@Local.OU_DATA_TYPE~"
				Header="Balance Sheet OU ID"
				ID="colBALANCE_SHEET_OU_ID"
				NoAgAggregates="True"
				NoAgCrosstabAggregate="True"
				>
				<ExcelColumnFormat
					DataType="@Local.OU_DATA_TYPE~"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="@Local.OU_EXCEL_FORMAT~"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="True"
				DataColumn="BALANCE_SHEET_OU_NAME"
				DataType="Text"
				Header="Balance Sheet OU Name"
				ID="colBALANCE_SHEET_OU_NAME"
				NoAgAggregates="True"
				NoAgCrosstabAggregate="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="True"
				DataColumn="ACCOUNT_NUMBER"
				DataType="@Local.ACCT_DATA_TYPE~"
				Header="Account Number"
				ID="colACCOUNT_NUMBER"
				NoAgAggregates="True"
				NoAgCrosstabAggregate="True"
				>
				<ExcelColumnFormat
					DataType="@Local.ACCT_DATA_TYPE~"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="@Local.ACCT_EXCEL_FORMAT~"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="True"
				DataColumn="ACCOUNT_DESCRIPTION"
				DataType="Text"
				Header="Account Description"
				ID="colACCOUNT_DESCRIPTION"
				NoAgAggregates="True"
				NoAgCrosstabAggregate="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="True"
				DataColumn="BASE_CURRENCY_ID"
				DataType="Text"
				Header="Base Currency ID"
				ID="colBASE_CURRENCY_ID"
				NoAgAggregates="True"
				NoAgCrosstabAggregate="True"
				>
				<ExcelColumnFormat
					DataType="Number"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="###0"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="True"
				DataColumn="SOURCE_CURRENCY_ID"
				DataType="Text"
				Header="Source Currency ID"
				ID="colSOURCE_CURRENCY_ID"
				NoAgAggregates="True"
				NoAgCrosstabAggregate="True"
				>
				<ExcelColumnFormat
					DataType="Number"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="###0"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				Class="ThemeAlignRight"
				ColumnVisible="True"
				DataColumn="EXCHANGE_RATE"
				DataType="Number"
				Format="##,##0.00####"
				Header="Exchange Rate - Spot Rate"
				ID="colEXCHANGE_RATE"
				NoAgAggregates="False"
				>
				<ExcelColumnFormat
					DataType="Number"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="##,##0.00####"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
				<ConditionalClass
					Class="ThemeTextNegative ThemeAlignRight"
					Condition="@Data.EXCHANGE_RATE~ &lt; 0"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				Class="ThemeAlignRight"
				ColumnVisible="True"
				DataColumn="ACCOUNT_BALANCE_SOURCE"
				DataType="Number"
				Format="##,##0.00"
				Header="Account Balance - Source"
				ID="colACCOUNT_BALANCE_SOURCE"
				NoAgAggregates="False"
				>
				<ExcelColumnFormat
					DataType="Number"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="##,##0.00"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
				<ConditionalClass
					Class="ThemeTextNegative ThemeAlignRight"
					Condition="@Data.ACCOUNT_BALANCE_SOURCE~ &lt; 0"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				Class="ThemeAlignRight"
				ColumnVisible="True"
				DataColumn="ACCOUNT_BALANCE_BASE"
				DataType="Number"
				Format="##,##0.00"
				Header="Account Balance - Base"
				ID="colACCOUNT_BALANCE_BASE"
				NoAgAggregates="False"
				>
				<ExcelColumnFormat
					DataType="Number"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="##,##0.00"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
				<ConditionalClass
					Class="ThemeTextNegative ThemeAlignRight"
					Condition="@Data.ACCOUNT_BALANCE_BASE~ &lt; 0"
				/>
			</AnalysisGridColumn>
			<Note
				Note="Start of non-Default Fact Columns"
			/>
		</AnalysisGrid>
		<IncludeSharedElement
			DefinitionFile="SharedElements.seReportButtons"
			SharedElementID="seReportButtons"
			>
			<SharedElementParams
				restoreGrid="pnlSavedBookmarks"
			/>
			<PassedSharedElement
				ID="pseRefreshAfterClearParams"
				>
				<Action
					ID="actPrSaveTemplate"
					Process="OpenReport"
					TaskID="openACReport"
					Type="Process"
					>
					<LinkParams
						assetAcctNum="@Request.assetAcctNum~"
						balSheetOuId="@Request.balSheetOuId~"
						cashAcctNum="@Request.cashAcctNum~"
						cashActivityDays="@Request.cashActivityDays~"
						currentAPDays="@Request.currentAPDays~"
						currentARDays="@Request.currentARDays~"
						cusId="@Request.cusId~"
						fromPersonalReport="@Request.fromPersonalReport~"
						invoicesPastDueBy="@Request.invoicesPastDueBy~"
						jobId="@Request.jobId~"
						jobTypeCd="@Request.jobTypeCd~"
						leOuId="@Request.leOuId~"
						liabilityAcctNum="@Request.liabilityAcctNum~"
						lineOfCreditDrawAcctNum="@Request.lineOfCreditDrawAcctNum~"
						lineOfCreditLimit="@Request.lineOfCreditLimit~"
						maintContractId="@Request.maintContractId~"
						menuItem="@Request.menuItem~"
						myReport="@Request.myReport~"
						ouId="@Request.ouId~"
						projectManagerId="@Request.projectManagerId~"
						rdAgAggrRowPosition="@Request.rdAgAggrRowPosition~"
						rdAgHideFunctionNamesCheckbox="@Request.rdAgHideFunctionNamesCheckbox~"
						rdAgRefreshData="True"
						reportName="@Request.reportName~"
						reportTargetFrameId="@Request.reportTargetFrameId~"
						salespersonId="@Request.salespersonId~"
						serviceTypeCd="@Request.serviceTypeCd~"
						throughDate="@Request.throughDate~"
						woId="@Request.woId~"
						workType="@Request.workType~"
					/>
				</Action>
			</PassedSharedElement>
			<PassedSharedElement
				ID="pseSaveParams"
				>
				<Action
					ID="apSave"
					Process="CheckSaveName"
					TaskID="CheckNameAssetAnalysis"
					Type="Process"
					>
					<LinkParams
						assetAcctNum="@Request.assetAcctNum~"
						balSheetOuId="@Request.balSheetOuId~"
						cashAcctNum="@Request.cashAcctNum~"
						cashActivityDays="@Request.cashActivityDays~"
						currentAPDays="@Request.currentAPDays~"
						currentARDays="@Request.currentARDays~"
						cusId="@Request.cusId~"
						fromPersonalReport="@Request.fromPersonalReport~"
						invoicesPastDueBy="@Request.invoicesPastDueBy~"
						jobId="@Request.jobId~"
						jobTypeCd="@Request.jobTypeCd~"
						leOuId="@Request.leOuId~"
						liabilityAcctNum="@Request.liabilityAcctNum~"
						lineOfCreditDrawAcctNum="@Request.lineOfCreditDrawAcctNum~"
						lineOfCreditLimit="@Request.lineOfCreditLimit~"
						maintContractId="@Request.maintContractId~"
						menuItem="@Request.menuItem~"
						myReport="@Request.myReport~"
						ouId="@Request.ouId~"
						projectManagerId="@Request.projectManagerId~"
						rdAgAggrRowPosition="@Request.rdAgAggrRowPosition~"
						rdAgHideFunctionNamesCheckbox="@Request.rdAgHideFunctionNamesCheckbox~"
						rdAgRefreshData="True"
						reportName="@Request.reportName~"
						reportTargetFrameId="@Request.reportTargetFrameId~"
						salespersonId="@Request.salespersonId~"
						serviceTypeCd="@Request.serviceTypeCd~"
						templateBookmarkID="@Request.templateBookmarkID~"
						throughDate="@Request.throughDate~"
						woId="@Request.woId~"
						workType="@Request.workType~"
					/>
					<Action
						ID="validateInputSave"
						Javascript="var inputText = encodeURIComponent(document.getElementById(&quot;itSave&quot;).value.trim());


if (inputText == &apos;&apos;)
{ 
	jAlert(&apos;Please enter a report name.&apos;, &apos;Warning&apos;);
	return false;
}
else
{	
	return true;
}"
						Type="PreActionJavascript"
					/>
				</Action>
			</PassedSharedElement>
			<PassedSharedElement
				ID="pseRefreshParams"
				>
				<Action
					ID="actPrSaveTemplate"
					Process="OpenReport"
					TaskID="ACTemplate"
					Type="Process"
					>
					<LinkParams
						assetAcctNum="@Request.assetAcctNum~"
						balSheetOuId="@Request.balSheetOuId~"
						cashAcctNum="@Request.cashAcctNum~"
						cashActivityDays="@Request.cashActivityDays~"
						currentAPDays="@Request.currentAPDays~"
						currentARDays="@Request.currentARDays~"
						cusId="@Request.cusId~"
						fromPersonalReport="@Request.fromPersonalReport~"
						invoicesPastDueBy="@Request.invoicesPastDueBy~"
						jobId="@Request.jobId~"
						jobTypeCd="@Request.jobTypeCd~"
						leOuId="@Request.leOuId~"
						liabilityAcctNum="@Request.liabilityAcctNum~"
						lineOfCreditDrawAcctNum="@Request.lineOfCreditDrawAcctNum~"
						lineOfCreditLimit="@Request.lineOfCreditLimit~"
						maintContractId="@Request.maintContractId~"
						menuItem="@Request.menuItem~"
						myReport="@Request.myReport~"
						ouId="@Request.ouId~"
						projectManagerId="@Request.projectManagerId~"
						rdAgAggrRowPosition="@Request.rdAgAggrRowPosition~"
						rdAgHideFunctionNamesCheckbox="@Request.rdAgHideFunctionNamesCheckbox~"
						rdAgRefreshData="True"
						reportName="@Request.reportName~"
						reportTargetFrameId="@Request.reportTargetFrameId~"
						salespersonId="@Request.salespersonId~"
						serviceTypeCd="@Request.serviceTypeCd~"
						throughDate="@Request.throughDate~"
						woId="@Request.woId~"
						workType="@Request.workType~"
					/>
				</Action>
			</PassedSharedElement>
			<PassedSharedElement
				ID="pseHelpLink"
				>
				<Action
					ID="actHelpLink"
					Type="Link"
					>
					<Target
						ID="tgtHelpLink"
						Link="javascript:url = &quot;http://www.penta.com/clientresources/PENTAWorkbench/Default.htm#PENTA Workbenches/Panel Descriptions/Finance and Accounting/Cash and Working Capital.htm&quot;;
window.open(url);"
						Type="Link"
					/>
				</Action>
			</PassedSharedElement>
			<PassedSharedElement
				ID="pseSaveCoParams"
			/>
		</IncludeSharedElement>
	</Body>
	<ReportFooter>
		<IncludeSharedElement
			DefinitionFile="SharedElements.seReportFooter"
			SharedElementID="seReportFooter"
			>
			<SharedElementParams
				bookmarkRequestParams="balSheetOuId,cusId,jobId,jobTypeCd,leOuId,maintContractId,ouId,projectManagerId,salespersonId,serviceTypeCd,woId,workType,menuItem,NumberOfPeriods,reportName,myReport,assetAcctNum,cashAcctNum,cashActivityDays,currentAPDays,currentARDays,invoicesPastDueBy,liabilityAcctNum,lineOfCreditDrawAcctNum,lineOfCreditLimit,rdAgAggrRowPosition,rdAgHideFunctionNamesCheckbox"
				restoreGrid="pnlSavedBookmarks"
			/>
		</IncludeSharedElement>
	</ReportFooter>
	<IncludeSharedElement
		DefinitionFile="SharedElements.sePrintablePaging"
		SharedElementID="sePrintablePaging"
	/>
	<ideTestParams
		assetAcctNum=""
		balSheetOuId=""
		cashAcctNum=""
		cashActivityDays=""
		currentAPDays=""
		currentARDays=""
		cusId=""
		fromPersonalReport=""
		invoicesPastDueBy=""
		jobId=""
		jobTypeCd=""
		leOuId=""
		liabilityAcctNum=""
		lineOfCreditDrawAcctNum=""
		lineOfCreditLimit=""
		maintContractId=""
		menuItem=""
		myReport=""
		ouId=""
		projectManagerId=""
		rdAgAggrRowPosition=""
		rdAgHideFunctionNamesCheckbox=""
		reportName=""
		reportTargetFrameId=""
		salespersonId=""
		serviceTypeCd=""
		templateBookmarkID=""
		throughDate=""
		woId=""
		workType=""
	/>
</Report>
