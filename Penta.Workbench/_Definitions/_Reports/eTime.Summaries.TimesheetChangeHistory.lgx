﻿<?xml version="1.0" encoding="utf-8"?>
<Report
	ID="eTime.Summaries.TimesheetChangeHistory"
	SecurityReportRightID="MENU_3200"
	>
	<DefaultRequestParams
		menuItem="3200"
		reportTargetFrameId="Self"
		workbenchName="Penta Workbench"
		workbenchReportDefinition="Default"
	/>
	<StyleSheet
		StyleSheet="styling.datatable.css"
	/>
	<Body>
		<DataTable
			AjaxPaging="True"
			Class="penta-table"
			HideWhenZeroRows="False"
			ID="dtTimesheets"
			SortArrows="True"
			Width="100"
			WidthScale="%"
			>
			<DataLayer
				ConnectionID="Penta"
				HandleQuotesInTokens="True"
				ID="dlTimesheets"
				Source="with
emp_info as
	(select		activity_log_rid,
				max(emp_id) as emp_id
   	 from		activity_log_emp_time
   	 group by	activity_log_rid
	),

stat_hist as
	(select		stat_hist.activity_log_rid			as act_log_rid,
				emp_info.emp_id						as emp_id,
				empname.formatted_name 				as emp_name,
				emp.ou_id							as emp_ou_id,
				stat_hist.user_id					as user_id,
				appname.formatted_name				as user_name,
				act_log.d_end_date					as week_end_date,
				stat_hist.activity_log_status_cd	as status_cd,
				stats.descr							as status_descr,
				stat_hist.creation_date				as date_changed,
				act_log.creation_user_id   			as created_by_id,
				crname.formatted_name				as created_by_name,
				case
					when stat_hist.activity_log_status_cd = &apos;APP&apos;
						then stat_hist.user_id
					else &apos;&apos;
				end									as approved_by_id,
				case
					when stat_hist.activity_log_status_cd = &apos;APP&apos;
						then appname.formatted_name
					else &apos;&apos;
				end									as approved_by_name,
				prime_appr.emp_id					as primary_appr_id,
				prime_name.formatted_name			as primary_appr_name
	 from		activity_log_status_hist stat_hist
	 			join activity_log_status stats
	 			 	on stats.activity_log_status_cd = stat_hist.activity_log_status_cd
	 			join activity_log act_log
	 			 	on stat_hist.activity_log_rid = act_log.id
	 			join emp_info
	 			 	on emp_info.activity_log_rid = act_log.id
	 			join employee emp
				 	on emp.emp_id = emp_info.emp_id
				join bi30_employee_name empname
				  	on empname.emp_id = emp_info.emp_id
	 			left outer join users
				 	on stat_hist.user_id = users.user_id
				left outer join etimesheet_appr_group_emp prime_appr
				 	on prime_appr.approval_group_num = emp.approval_group_num and prime_appr.primary_approver = &apos;Y&apos;
				join bi30_employee_name prime_name
				 	on prime_name.emp_id = prime_appr.emp_id
				left outer join users users_cr
				 	on act_log.creation_user_id = users_cr.user_id
      			left outer join employee cr_emp
          		 	on (cr_emp.emp_id = users_cr.emp_id or cr_emp.emp_user_id = users_cr.user_id) and
                   		coalesce(users_cr.emp_id, cr_emp.emp_id) = cr_emp.emp_id and
                   		coalesce(cr_emp.emp_user_id, users_cr.user_id) = users_cr.user_id
  				left outer join bi30_employee_name crname
 				 	on crname.emp_id = cr_emp.emp_id
      			left outer join employee app_emp
          		 	on (app_emp.emp_id = users.emp_id or app_emp.emp_user_id = users.user_id) and
                   		coalesce(users.emp_id, app_emp.emp_id) = app_emp.emp_id and
                   		coalesce(app_emp.emp_user_id, users.user_id) = users.user_id
  				left outer join bi30_employee_name appname
  				 	on appname.emp_id = app_emp.emp_id
	 where		activity_log_type_cd = &apos;ETS&apos;
	  and 		(emp_info.emp_id in (@SingleQuote.Request.empId~) or &apos;@Request.empId~&apos; is null)
	  and 		d_end_date between to_date(&apos;@Request.beginDate~&apos;, &apos;yyyy/mm/dd&apos;) and to_date(&apos;@Request.endDate~&apos;, &apos;yyyy/mm/dd&apos;)
	  and 		(pk_ouSecurity.f_userAuthorized( &apos;EXT&apos;, &apos;@Request.menuItem~&apos;, emp.ou_id, &apos;@Function.UserName~&apos; ) = &apos;Y&apos;)
	  and   	(pk_payGroup.f_userAuthorized( emp.pay_group_num, &apos;@Function.UserName~&apos; ) = &apos;Y&apos;)
	),

approvedStatus as
	(select activity_log_rid		as act_rid,
 			activity_log_status_cd,
 			status_user_id,
 			dense_rank() over( partition by activity_log_rid
 	 	   			           order by seq_num desc ) as rank_num
 	 from activity_log_status_hist
 	 where activity_log_status_cd = &apos;APP&apos;),

log_hist as
	(select	activity_log_rid			as act_log_rid,
			aud.id						as emp_time_rid,
			audit_timestamptz			as audit_tmsp,
			aud.modification_date		as change_tmsp,
			reg_hrs						as reg_hrs,
			prem1_hrs					as prem1_hrs,
			prem2_hrs					as prem2_hrs,
			job_id						as job_id,
			cc							as costcode,
			item_num					as work_code,
			aud.ou_id					as charge_ou,
			hr_class_num				as hr_class,
			wo.wo_id					as work_order,
			wo_task_id					as task_id,
			d_end_date					as week_end_date,
			empname.formatted_name 		as emp_name,
			aud.emp_id					as emp_id,
			emp.ou_id					as emp_ou_id,
			aud.user_id					as changed_by_id,
			users.name					as changed_by_user_name,
			chname.formatted_name		as changed_by_name,
			aud.d_work_date				as work_date,
			aud.creation_user_id  		as created_by_id,
			users_cr.name				as created_by_user_name,
			crname.formatted_name	  	as created_by_name,
			appStatus.status_user_id	as approver_id,
			users_app.name				as approver_user_name,
			appname.formatted_name  	as approver_name,
			appr_emp.emp_id				as primary_appr_id,
			apprname.formatted_name 	as primary_appr_name
	from	activity_log_emp_time_aud aud
			join activity_log
				on activity_log.id = aud.activity_log_rid
			join bi30_employee_name empname
				on empname.emp_id = aud.emp_id
			join employee emp
				on emp.emp_id = empname.emp_id
			left outer join work_order wo
				on aud.wo_sa_num = wo.wo_sa_num
			left outer join users
				on aud.user_id = users.user_id
      		left outer join employee ch_emp
          		on (ch_emp.emp_id = users.emp_id or ch_emp.emp_user_id = users.user_id) and
                    coalesce(users.emp_id, ch_emp.emp_id) = ch_emp.emp_id and
                    coalesce(ch_emp.emp_user_id, users.user_id) = users.user_id
  			left outer join bi30_employee_name chname
 				on chname.emp_id = ch_emp.emp_id
			left outer join users users_cr
				on aud.creation_user_id = users_cr.user_id
      		left outer join employee cr_emp
          		on (cr_emp.emp_id = users_cr.emp_id or cr_emp.emp_user_id = users_cr.user_id) and
                    coalesce(users_cr.emp_id, cr_emp.emp_id) = cr_emp.emp_id and
                    coalesce(cr_emp.emp_user_id, users_cr.user_id) = users_cr.user_id
  			left outer join bi30_employee_name crname
 				on crname.emp_id = cr_emp.emp_id
			left outer join approvedStatus appStatus
				on activity_log.id = appStatus.act_rid
			left outer join users users_app
				on appStatus.status_user_id = users_app.user_id
			left outer join etimesheet_appr_group_emp appr_emp
				on appr_emp.approval_group_num = emp.approval_group_num and appr_emp.primary_approver = &apos;Y&apos;
			join bi30_employee_name apprname
				on apprname.emp_id = appr_emp.emp_id
      		left outer join employee app_emp
          		on (app_emp.emp_id = users_app.emp_id or app_emp.emp_user_id = users_app.user_id) and
                    coalesce(users_app.emp_id, app_emp.emp_id) = app_emp.emp_id and
                    coalesce(app_emp.emp_user_id, users_app.user_id) = users_app.user_id
  			left outer join bi30_employee_name appname
  				on appname.emp_id = app_emp.emp_id
	where 	activity_log_type_cd = &apos;ETS&apos;
	and 	(aud.emp_id in (@SingleQuote.Request.empId~) or &apos;@Request.empId~&apos; is null)
	and 	d_end_date between to_date(&apos;@Request.beginDate~&apos;, &apos;yyyy/mm/dd&apos;) and to_date(&apos;@Request.endDate~&apos;, &apos;yyyy/mm/dd&apos;)
	and 	(pk_ouSecurity.f_userAuthorized( &apos;EXT&apos;, &apos;@Request.menuItem~&apos;, emp.ou_id, &apos;@Function.UserName~&apos; ) = &apos;Y&apos;)
	and     (pk_payGroup.f_userAuthorized( emp.pay_group_num, &apos;@Function.UserName~&apos; ) = &apos;Y&apos;)  
UNION ALL
	select	activity_log_rid			as act_log_rid,
			log_time.id					as emp_time_rid,
			sysdate						as audit_tmsp,
			log_time.modification_date	as change_tmsp,
			reg_hrs						as reg_hrs,
			prem1_hrs					as prem1_hrs,
			prem2_hrs					as prem2_hrs,
			job_id						as job_id,
			cc							as costcode,
			item_num					as work_code,
			log_time.ou_id				as charge_ou,
			hr_class_num				as hr_class,
			wo.wo_id					as work_order,
			wo_task_id					as task_id,
			d_end_date					as week_end_date,
			empname.formatted_name 		as emp_name,
			log_time.emp_id				as emp_id,
			emp.ou_id					as emp_ou_id,
			log_time.user_id			as changed_by_id,
			users.name					as changed_by_user_name,
			chname.formatted_name		as changed_by_name,
			log_time.d_work_date		as work_date,
			log_time.creation_user_id   as created_by_id,
			users_cr.name				as created_by_user_name,
			crname.formatted_name	  	as created_by_name,
			appStatus.status_user_id	as approver_id,
			users_app.name				as approver_user_name,
			appname.formatted_name  	as approver_name,
			appr_emp.emp_id				as primary_appr_id,
			apprname.formatted_name 	as primary_appr_name
	from	activity_log_emp_time log_time
			join activity_log
				on activity_log.id = log_time.activity_log_rid
			join bi30_employee_name empname
				on empname.emp_id = log_time.emp_id
			join employee emp
				on emp.emp_id = empname.emp_id
			left outer join work_order wo
				on log_time.wo_sa_num = wo.wo_sa_num
			left outer join users
				on log_time.user_id = users.user_id
      		left outer join employee ch_emp
          		on (ch_emp.emp_id = users.emp_id or ch_emp.emp_user_id = users.user_id) and
                    coalesce(users.emp_id, ch_emp.emp_id) = ch_emp.emp_id and
                    coalesce(ch_emp.emp_user_id, users.user_id) = users.user_id
  			left outer join bi30_employee_name chname
 				on chname.emp_id = ch_emp.emp_id
			left outer join users users_cr
				on log_time.creation_user_id = users_cr.user_id
      		left outer join employee cr_emp
          		on (cr_emp.emp_id = users_cr.emp_id or cr_emp.emp_user_id = users_cr.user_id) and
                    coalesce(users_cr.emp_id, cr_emp.emp_id) = cr_emp.emp_id and
                    coalesce(cr_emp.emp_user_id, users_cr.user_id) = users_cr.user_id
  			left outer join bi30_employee_name crname
 				on crname.emp_id = cr_emp.emp_id
			left outer join approvedStatus appStatus
				on activity_log.id = appStatus.act_rid
			left outer join users users_app
				on appStatus.status_user_id = users_app.user_id
			left outer join etimesheet_appr_group_emp appr_emp
				on appr_emp.approval_group_num = emp.approval_group_num and appr_emp.primary_approver = &apos;Y&apos;
			join bi30_employee_name apprname
				on apprname.emp_id = appr_emp.emp_id
      		left outer join employee app_emp
          		on (app_emp.emp_id = users_app.emp_id or app_emp.emp_user_id = users_app.user_id) and
                    coalesce(users_app.emp_id, app_emp.emp_id) = app_emp.emp_id and
                    coalesce(app_emp.emp_user_id, users_app.user_id) = users_app.user_id
  			left outer join bi30_employee_name appname
  				on appname.emp_id = app_emp.emp_id
	where 	activity_log_type_cd = &apos;ETS&apos;
	and 	(log_time.emp_id in (@SingleQuote.Request.passed_empId~) or &apos;@Request.passed_empId~&apos; is null)
	and 	(log_time.emp_id in (@SingleQuote.Request.empId~) or &apos;@Request.empId~&apos; is null)
	and 	d_end_date between to_date(&apos;@Request.beginDate~&apos;, &apos;yyyy/mm/dd&apos;) and to_date(&apos;@Request.endDate~&apos;, &apos;yyyy/mm/dd&apos;)
	and 	(pk_ouSecurity.f_userAuthorized( &apos;EXT&apos;, &apos;@Request.menuItem~&apos;, emp.ou_id, &apos;@Function.UserName~&apos; ) = &apos;Y&apos;)
	and     (pk_payGroup.f_userAuthorized( emp.pay_group_num, &apos;@Function.UserName~&apos; ) = &apos;Y&apos;)    
	)

, the_changes as
	(select act_log_rid,
			emp_time_rid,
			change_tmsp 										as date_changed,
			reg_hrs,
			lag(reg_hrs)
				over(partition by act_log_rid, emp_time_rid
					 order by audit_tmsp)						as prev_reg_hrs,
			prem1_hrs,
			lag(prem1_hrs)
				over(partition by act_log_rid, emp_time_rid
					 order by audit_tmsp)						as prev_prem1_hrs,
			prem2_hrs,
			lag(prem2_hrs)
				over(partition by act_log_rid, emp_time_rid
					 order by audit_tmsp)						as prev_prem2_hrs,
			job_id,
			lag(job_id)
				over(partition by act_log_rid, emp_time_rid
					 order by audit_tmsp)						as prev_job_id,
			costcode,
			lag(costcode)
				over(partition by act_log_rid, emp_time_rid
					 order by audit_tmsp)						as prev_costcode,
			work_code,
			lag(work_code)
				over(partition by act_log_rid, emp_time_rid
					 order by audit_tmsp)						as prev_work_code,
			charge_ou,
			lag(charge_ou)
				over(partition by act_log_rid, emp_time_rid
					 order by audit_tmsp)						as prev_charge_ou,
			hr_class,
			lag(hr_class)
				over(partition by act_log_rid, emp_time_rid
					 order by audit_tmsp)						as prev_hr_class,
			work_order,
			lag(work_order)
				over(partition by act_log_rid, emp_time_rid
					 order by audit_tmsp)						as prev_work_order,
			task_id,
			lag(task_id)
				over(partition by act_log_rid, emp_time_rid
					 order by audit_tmsp)						as prev_task_id,
			to_char(week_end_date, &apos;yyyy/mm/dd&apos;) 				as week_end_date,
			emp_name,
			emp_ou_id,
			emp_id,
			changed_by_id,
			changed_by_name,
			work_date,
			created_by_id,
			created_by_name,
			approver_id,
			approver_name,
			primary_appr_id,
			primary_appr_name
	 from	log_hist
	 ),


 change_detail as (
	(select act_log_rid,
			emp_time_rid,
			date_changed,
			week_end_date,
			emp_id,
			emp_name,
			emp_ou_id,
			changed_by_id,
			changed_by_name,
			created_by_id,
			created_by_name,
			approver_id,
			approver_name,
			primary_appr_id,
			primary_appr_name,
			work_date,
			&apos;&apos; as changed_distribution,
			&apos;&apos; as new_distribution_value,
			&apos;&apos; as old_distribution_value,
			&apos;Regular&apos; as hours_changed,
			to_char(reg_hrs) as new_hours_value,
			to_char(prev_reg_hrs) as old_hours_value,
			&apos;&apos; as new_status_descr,
			&apos;&apos; as old_status_descr
	from	the_changes
	where	(reg_hrs &lt;&gt; prev_reg_hrs
			 or (reg_hrs is null and prev_reg_hrs is not null)
			 or (reg_hrs is not null and prev_reg_hrs is null)))

  	UNION ALL

	(select	act_log_rid,
			emp_time_rid,
			date_changed,
			week_end_date,
			emp_id,
			emp_name,
			emp_ou_id,
			changed_by_id,
			changed_by_name,
			created_by_id,
			created_by_name,
			approver_id,
			approver_name,
			primary_appr_id,
			primary_appr_name,
			work_date,
			&apos;&apos; as changed_distribution,
			&apos;&apos; as new_distribution_value,
			&apos;&apos; as old_distribution_value,
			&apos;Premium 1&apos; as hours_changed,
			to_char(prem1_hrs) as new_hours_value,
			to_char(prev_prem1_hrs) as old_hours_value,
			&apos;&apos; as new_status_descr,
			&apos;&apos; as old_status_descr
	from	the_changes
	where	(prem1_hrs &lt;&gt; prev_prem1_hrs
			 or (prem1_hrs is null and prev_prem1_hrs is not null)
			 or (prem1_hrs is not null and prev_prem1_hrs is null)))

	UNION ALL

	(select	act_log_rid,
			emp_time_rid,
			date_changed,
			week_end_date,
			emp_id,
			emp_name,
			emp_ou_id,
			changed_by_id,
			changed_by_name,
			created_by_id,
			created_by_name,
			approver_id,
			approver_name,
			primary_appr_id,
			primary_appr_name,
			work_date,
			&apos;&apos; as changed_distribution,
			&apos;&apos; as new_distribution_value,
			&apos;&apos; as old_distribution_value,
			&apos;Premium 2&apos; as hours_changed,
			to_char(prem1_hrs) as new_hours_value,
			to_char(prev_prem1_hrs) as old_hours_value,
			&apos;&apos; as new_status_descr,
			&apos;&apos; as old_status_descr
	from	the_changes
	where	(prem2_hrs &lt;&gt; prev_prem2_hrs
			 or (prem2_hrs is null and prev_prem2_hrs is not null)
			 or (prem2_hrs is not null and prev_prem2_hrs is null)))

	UNION ALL

	(select	act_log_rid,
			emp_time_rid,
			date_changed,
			week_end_date,
			emp_id,
			emp_name,
			emp_ou_id,
			changed_by_id,
			changed_by_name,
			created_by_id,
			created_by_name,
			approver_id,
			approver_name,
			primary_appr_id,
			primary_appr_name,
			work_date,
			&apos;Job ID&apos; as changed_distribution,
			to_char(job_id) as new_distribution_value,
			to_char(prev_job_id) as old_distribution_value,
			&apos;&apos; as hours_changed,
			&apos;&apos; as new_hours_value,
			&apos;&apos; as old_hours_value,
			&apos;&apos; as new_status_descr,
			&apos;&apos; as old_status_descr
	from	the_changes
	where	(job_id &lt;&gt; prev_job_id
			 or (job_id is null and prev_job_id is not null)
			 or (job_id is not null and prev_job_id is null)))

	UNION ALL

	(select	act_log_rid,
			emp_time_rid,
			date_changed,
			week_end_date,
			emp_id,
			emp_name,
			emp_ou_id,
			changed_by_id,
			changed_by_name,
			created_by_id,
			created_by_name,
			approver_id,
			approver_name,
			primary_appr_id,
			primary_appr_name,
			work_date,
			&apos;Costcode&apos; as changed_distribution,
			to_char(costcode) as new_distribution_value,
			to_char(prev_costcode) as old_distribution_value,
			&apos;&apos; as hours_changed,
			&apos;&apos; as new_hours_value,
			&apos;&apos; as old_hours_value,
			&apos;&apos; as new_status_descr,
			&apos;&apos; as old_status_descr
	from	the_changes
	where	(costcode &lt;&gt; prev_costcode
			 or (costcode is null and prev_costcode is not null)
			 or (costcode is not null and prev_costcode is null)))

	UNION ALL

	(select	act_log_rid,
			emp_time_rid,
			date_changed,
			week_end_date,
			emp_id,
			emp_name,
			emp_ou_id,
			changed_by_id,
			changed_by_name,
			created_by_id,
			created_by_name,
			approver_id,
			approver_name,
			primary_appr_id,
			primary_appr_name,
			work_date,
			&apos;Work Code&apos; as changed_distribution,
			to_char(work_code) as new_distribution_value,
			to_char(prev_work_code) as old_distribution_value,
			&apos;&apos; as hours_changed,
			&apos;&apos; as new_hours_value,
			&apos;&apos; as old_hours_value,
			&apos;&apos; as new_status_descr,
			&apos;&apos; as old_status_descr
	from	the_changes
	where	(work_code &lt;&gt; prev_work_code
			 or (work_code is null and prev_work_code is not null)
			 or (work_code is not null and prev_work_code is null)))

	UNION ALL

	(select	act_log_rid,
			emp_time_rid,
			date_changed,
			week_end_date,
			emp_id,
			emp_name,
			emp_ou_id,
			changed_by_id,
			changed_by_name,
			created_by_id,
			created_by_name,
			approver_id,
			approver_name,
			primary_appr_id,
			primary_appr_name,
			work_date,
			&apos;Hours Class&apos; as changed_distribution,
			to_char(hr_class) as new_distribution_value,
			to_char(prev_hr_class) as old_distribution_value,
			&apos;&apos; as hours_changed,
			&apos;&apos; as new_hours_value,
			&apos;&apos; as old_hours_value,
			&apos;&apos; as new_status_descr,
			&apos;&apos; as old_status_descr
	from	the_changes
	where	(hr_class &lt;&gt; prev_hr_class
			 or (hr_class is null and prev_hr_class is not null)
			 or (hr_class is not null and prev_hr_class is null)))

	UNION ALL

	(select	act_log_rid,
			emp_time_rid,
			date_changed,
			week_end_date,
			emp_id,
			emp_name,
			emp_ou_id,
			changed_by_id,
			changed_by_name,
			created_by_id,
			created_by_name,
			approver_id,
			approver_name,
			primary_appr_id,
			primary_appr_name,
			work_date,
			&apos;Charge OU&apos; as changed_distribution,
			to_char(charge_ou) as new_distribution_value,
			to_char(prev_charge_ou) as old_distribution_value,
			&apos;&apos; as hours_changed,
			&apos;&apos; as new_hours_value,
			&apos;&apos; as old_hours_value,
			&apos;&apos; as new_status_descr,
			&apos;&apos; as old_status_descr
	from	the_changes
	where	(charge_ou &lt;&gt; prev_charge_ou
			 or (charge_ou is null and prev_charge_ou is not null)
			 or (charge_ou is not null and prev_charge_ou is null)))

	UNION ALL

	(select	act_log_rid,
			emp_time_rid,
			date_changed,
			week_end_date,
			emp_id,
			emp_name,
			emp_ou_id,
			changed_by_id,
			changed_by_name,
			created_by_id,
			created_by_name,
			approver_id,
			approver_name,
			primary_appr_id,
			primary_appr_name,
			work_date,
			&apos;Work Order&apos; as changed_distribution,
			to_char(work_order) as new_distribution_value,
			to_char(prev_work_order) as old_distribution_value,
			&apos;&apos; as hours_changed,
			&apos;&apos; as new_hours_value,
			&apos;&apos; as old_hours_value,
			&apos;&apos; as new_status_descr,
			&apos;&apos; as old_status_descr
	from	the_changes
	where	(work_order &lt;&gt; prev_work_order
			 or (work_order is null and prev_work_order is not null)
			 or (work_order is not null and prev_work_order is null)))

	UNION ALL

	(select	act_log_rid,
			emp_time_rid,
			date_changed,
			week_end_date,
			emp_id,
			emp_name,
			emp_ou_id,
			changed_by_id,
			changed_by_name,
			created_by_id,
			created_by_name,
			approver_id,
			approver_name,
			primary_appr_id,
			primary_appr_name,
			work_date,
			&apos;Task&apos; as changed_distribution,
			to_char(task_id) as new_distribution_value,
			to_char(prev_task_id) as old_distribution_value,
			&apos;&apos; as hours_changed,
			&apos;&apos; as new_hours_value,
			&apos;&apos; as old_hours_value,
			&apos;&apos; as new_status_descr,
			&apos;&apos; as old_status_descr
	from	the_changes
	where	(task_id &lt;&gt; prev_task_id
			 or (task_id is null and prev_task_id is not null)
			 or (task_id is not null and prev_task_id is null)))

	UNION ALL

	(select	act_log_rid,
			null as emp_time_rid,
			date_changed,
			to_char(week_end_date, &apos;yyyy/mm/dd&apos;) as week_end_date,
			emp_id,
			emp_name,
			emp_ou_id,
			user_id as changed_by_id,
			user_name as changed_by_name,
			created_by_id,
			created_by_name,
			approved_by_id as approver_id,
			approved_by_name as approver_name,
			primary_appr_id,
			primary_appr_name,
			null as work_date,
			&apos;&apos; as changed_distribution,
			&apos;&apos; as new_distribution_value,
			&apos;&apos; as old_distribution_value,
			&apos;&apos; as hours_changed,
			&apos;&apos; as new_hours_value,
			&apos;&apos; as old_hours_value,
			status_descr as new_status_descr,
			lag(status_descr)
				over(partition by act_log_rid
			   	     order by date_changed)			as old_status_descr
	 from	stat_hist)
	 )

select	emp_id			as emp_id,
		emp_name		as formatted_name,
		emp_ou_id		as emp_ou_id,
		week_end_date	as d_end_date,
		count(*)		as change_count
from	change_detail
group by emp_id, emp_name, emp_ou_id, week_end_date
having count(*) &gt; 0
"
				Type="SQL"
			/>
			<InteractivePaging
				CaptionType="Text"
				HideShowPrevNextCaptions="True"
				HideWhenOnePage="True"
				Location="Bottom"
				PageRowCount="10"
				ShowPageNumber="Numbered"
			/>
			<DataTableColumn
				Header="Employee ID"
				ID="colEMP_ID"
				>
				<Label
					Caption="@Data.EMP_ID~"
					ID="lblEMP_ID"
				/>
				<DataColumnSort
					DataColumn="EMP_ID"
					DataType="Number"
				/>
			</DataTableColumn>
			<DataTableColumn
				Header="Employee Name"
				ID="colFORMATTED_NAME"
				>
				<Label
					Caption="@Data.FORMATTED_NAME~"
					ID="lblFORMATTED_NAME"
				/>
				<DataColumnSort
					DataColumn="FORMATTED_NAME"
				/>
			</DataTableColumn>
			<DataTableColumn
				Class="ThemeAlignCenter"
				Header="Week Ending Date"
				ID="colD_END_DATE"
				>
				<Label
					Caption="@Data.D_END_DATE~"
					Format="MM/dd/yyyy"
					ID="lblD_END_DATE"
					>
					<Action
						FrameID="Parent"
						ID="actTimesheetSummaryDrillDown"
						Process="OpenReport"
						TaskID="openeTimeReport"
						Type="Process"
						>
						<WaitPage/>
						<LinkParams
							approvalGroup="@Request.approvalGroup~"
							BeginDate="@Request.BeginDate~"
							chargeOuId="all"
							EndDate="@Request.EndDate~"
							hourClass="all"
							leOuId="all"
							menuItem="@Request.menuItem~"
							myReport="eTime.Reports.TimesheetReport"
							ouId="@Data.OU_ID~"
							OUManager="@Request.OUManager~"
							passed_empId="@Data.EMP_ID~"
							payGroupNumber="all"
							rdAgRefreshData="True"
							reportName="Timesheets"
							throughDate="@Request.idThroughDate~"
							timesheetStatus="all"
							useWeekEndDate="Y"
							weekEndDate="@Data.D_END_DATE~"
						/>
					</Action>
				</Label>
				<DataColumnSort
					DataColumn="D_END_DATE"
					DataType="Date"
				/>
			</DataTableColumn>
			<DataTableColumn
				Class="ThemeAlignRight"
				Header="Modifications"
				ID="colCHANGE_COUNT"
				>
				<Label
					Caption="@Data.CHANGE_COUNT~"
					ID="lblCHANGE_COUNT"
					>
					<Action
						FrameID="Parent"
						ID="actTimesheetChangeHistoryDrillDown"
						Process="OpenReport"
						TaskID="openeTimeReport"
						Type="Process"
						>
						<WaitPage/>
						<LinkParams
							menuItem="@Request.menuItem~"
							myReport="eTime.Reports.TimesheetChangeHistoryReport"
							passed_empId="@Data.EMP_ID~"
							rdAgRefreshData="True"
							reportName="Timesheet Change History"
							throughDate="@Request.idThroughDate~"
							weekEndDate="@Data.D_END_DATE~"
						/>
					</Action>
				</Label>
				<DataColumnSort
					DataColumn="CHANGE_COUNT"
					DataType="Number"
				/>
			</DataTableColumn>
		</DataTable>
	</Body>
	<ideTestParams
		approvalGroup=""
		beginDate=""
		BeginDate=""
		empId=""
		endDate=""
		EndDate=""
		idThroughDate=""
		menuItem=""
		OUManager=""
		passed_empId=""
	/>
</Report>
