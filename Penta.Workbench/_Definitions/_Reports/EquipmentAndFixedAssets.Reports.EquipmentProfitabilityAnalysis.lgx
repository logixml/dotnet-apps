﻿<?xml version="1.0" encoding="utf-8"?>
<Report
	Caption="@Request.reportName~"
	ID="EquipmentAndFixedAssets.Reports.EquipmentProfitabilityAnalysis"
	>
	<MasterReport
		Report="DetailMasterReport"
	/>
	<IncludeScriptFile
		ID="loadJqueryEngine"
		IncludedScriptFile="Javascript.jquery-3.3.1.min.js"
	/>
	<IncludeScriptFile
		ID="loadJqueryUI"
		IncludedScriptFile="Javascript.jquery-ui.min.js"
	/>
	<IncludeScriptFile
		ID="loadjalert"
		IncludedScriptFile="Javascript.jquery.alert.js"
	/>
	<IncludeScriptFile
		ID="modifyAgTabBackground"
		IncludedScriptFile="Javascript.ModifyAgTabBackground.js"
	/>
	<DefaultRequestParams
		factCols="colPRD_UNITS_CHRGD,colYTD_UNITS_CHRGD,colPRD_REVENUE_AMT,colPRD_EXPENSE_AMT,colPRD_PROFIT_AMT,colYTD_REVENUE_AMT,colYTD_EXPENSE_AMT,colYTD_PROFIT_AMT,"
		reportTargetFrameId="logixml-frame"
		workbenchName="Penta Workbench"
		workbenchReportDefinition="Default"
	/>
	<LocalData>
		<DataLayer
			ID="dlDomains"
			LinkedDataLayerID="dlnkDomains"
			Type="Linked"
		/>
	</LocalData>
	<StyleSheet
		StyleSheet="styling.jquery.alerts.css"
	/>
	<StyleSheet
		StyleSheet="styling.analysisgrid.css"
	/>
	<IncludeSharedElement
		DefinitionFile="SharedElements.seReportHeaderExport"
		SharedElementID="seReportHeaderExport"
	/>
	<Body>
		<IncludeSharedElement
			DefinitionFile="SharedElements.seSavedBookmarks"
			SharedElementID="seSavedBookmarks"
		/>
		<IncludeSharedElement
			DefinitionFile="ProjectManagement.SharedElements"
			SharedElementID="CommonHiddenElements"
		/>
		<InputHidden
			DefaultValue="@Request.parOuId~"
			ID="parOuId"
		/>
		<InputHidden
			DefaultValue="@Request.faClassNum~"
			ID="faClassNum"
		/>
		<InputHidden
			DefaultValue="@Request.faId~"
			ID="faId"
		/>
		<InputHidden
			DefaultValue="@Request.periodEndDate~"
			ID="periodEndDate"
		/>
		<InputHidden
			DefaultValue="@Request.templateBookmarkID~"
			ID="templateBookmarkID"
		/>
		<InputHidden
			DefaultValue="EquipmentAndFixedAssets.Reports.EquipmentProfitabilityAnalysis"
			ID="myReport"
		/>
		<AnalysisGrid
			AjaxPaging="True"
			BatchSelection="True"
			DraggableColumns="True"
			ID="agEquipmentProfitabilityAnalysis"
			MaxRowsExport="-1"
			ShowPageNumber="True"
			SortArrows="True"
			TemplateModifierFile="TMFs._PentaAnalysisGrid.xml"
			>
			<DataLayer
				ConnectionID="Penta"
				ID="agDataLayer"
				Source="with dates as
   (select /*+ materialize */ou_id,
           pk_acctgPrd.f_fiscalYearBeginDate(
              ou_id,
              coalesce( to_date( &apos;@Request.periodEndDate~&apos;, &apos;YYYY-MM-DD&quot;T&quot;HH24:MI:SS&apos; ), trunc( sysdate ) )
           ) as d_fy_begin_date,
           pk_acctgPrd.f_periodBeginDate(
              ou_id,
              coalesce( to_date( &apos;@Request.periodEndDate~&apos;, &apos;YYYY-MM-DD&quot;T&quot;HH24:MI:SS&apos; ), trunc( sysdate ) )
           ) as d_prd_begin_date,
           pk_acctgPrd.f_periodEndDate(
              ou_id,
              coalesce( to_date( &apos;@Request.periodEndDate~&apos;, &apos;YYYY-MM-DD&quot;T&quot;HH24:MI:SS&apos; ), trunc( sysdate ) )
           ) as d_prd_end_date
      from organization_unit),
   authorizedOus as
   (select distinct ou_sel_subt.ou_id
      from ou_sel_subt
     where pk_ouSecurity.f_userAuthorized( &apos;EXT&apos;, &apos;@Request.menuItem~&apos;, ou_sel_subt.ou_id, &apos;@Function.UserName~&apos; ) = &apos;Y&apos;
       and (REGEXP_LIKE( &apos;@Request.ouId~&apos;, &apos;(^|,)all($|,)&apos; ) or ou_sel_subt.ou_id in (@SingleQuote.Request.ouId~))
       and (REGEXP_LIKE( &apos;@Request.parOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or ou_sel_subt.par_ou_id in (@SingleQuote.Request.parOuId~))),
   eqIncomeExpense as
   (select fixed_asset.fa_id,
           fixed_asset.descr as fa_descr,
           fixed_asset.fa_class_num,
           coalesce( fixed_asset.par_fa_id, fixed_asset.fa_id ) as par_fa_id,
           organization_unit.ou_id,
           organization_unit.name as ou_name,
           organization_unit.le_ou_id,
           organization_unit.bal_sheet_ou_id,
           case when eq_usage_hist.d_distr_date
                 between dates.d_prd_begin_date and dates.d_prd_end_date
              then &apos;Y&apos;
              else &apos;N&apos;
           end as in_current_period,
		   case eq_usage_hist.rental_cd
              when &apos;Y&apos; then &apos;Rental&apos;
              when &apos;N&apos; then &apos;Usage&apos;
           end as transaction_type,
           eq_usage_hist.um,
           unit_of_measure.descr as um_descr,
           eq_usage_hist.eq_work_stat_cd,
           eq_work_stat.descr as eq_work_stat_descr,
           null as fa_exp_cat_num,
           null as fa_exp_cat_descr,
           eq_usage_hist.amt as income_amt,
           0 as expense_amt,
           eq_usage_hist.amt as profit_amt,
		   eq_usage_hist.units_chrgd
      from dates
           join eq_usage_hist
             on eq_usage_hist.d_distr_date between dates.d_fy_begin_date and dates.d_prd_end_date and
                eq_usage_hist.cr_ou_id = dates.ou_id
           join fixed_asset
             on fixed_asset.fa_id = eq_usage_hist.fa_id
           join organization_unit
             on organization_unit.ou_id = fixed_asset.ou_id
		   join authorizedOus
		     on authorizedOus.ou_id = organization_unit.ou_id
		   join eq_work_stat
             on eq_work_stat.eq_work_stat_cd = eq_usage_hist.eq_work_stat_cd
           left outer join unit_of_measure
             on unit_of_measure.um = eq_usage_hist.um
     where (&apos;@Request.faClassNum~&apos; is null or fixed_asset.fa_class_num = &apos;@Request.faClassNum~&apos;)
       and (fixed_asset.fa_id in (@SingleQuote.Request.faId~) or &apos;@Request.faId~&apos; is null)
	   and (REGEXP_LIKE( &apos;@Request.balSheetOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.bal_sheet_ou_id in (@SingleQuote.Request.balSheetOuId~))
	   and (REGEXP_LIKE( &apos;@Request.leOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.le_ou_id in (@SingleQuote.Request.leOuId~))
     union all
    select fixed_asset.fa_id,
           fixed_asset.descr as fa_descr,
           fixed_asset.fa_class_num,
           coalesce( fixed_asset.par_fa_id, fixed_asset.fa_id ) as par_fa_id,
           organization_unit.ou_id,
           organization_unit.name as ou_name,
           organization_unit.le_ou_id,
           organization_unit.bal_sheet_ou_id,
           case when fa_exp_hist.d_distr_date
                 between dates.d_prd_begin_date and dates.d_prd_end_date
              then &apos;Y&apos;
              else &apos;N&apos;
           end as in_current_period,
		   &apos;Expense&apos; as transaction_type,
           null as um,
           null as um_descr,
           null as eq_work_stat_cd,
           null as eq_work_stat_descr,
           fa_exp_hist.fa_exp_cat_num,
           fa_exp_cat.descr as fa_exp_cat_descr,
           0 as income_amt,
           fa_exp_hist.amt as expense_amt,
           fa_exp_hist.amt * -1 as profit_amt,
		   0 as units_chrgd
      from dates
           join fa_exp_hist
             on fa_exp_hist.ou_id = dates.ou_id and
                fa_exp_hist.d_distr_date between dates.d_fy_begin_date and dates.d_prd_end_date
           join fa_exp_cat
             on fa_exp_cat.fa_exp_cat_num = fa_exp_hist.fa_exp_cat_num and
                fa_exp_cat.incl_in_cost_analysis = &apos;Y&apos;
           join fixed_asset
             on fixed_asset.fa_id = fa_exp_hist.fa_id
           join organization_unit
             on organization_unit.ou_id = fixed_asset.ou_id
		   join authorizedOus
		     on authorizedOus.ou_id = organization_unit.ou_id
     where (&apos;@Request.faClassNum~&apos; is null or fixed_asset.fa_class_num = &apos;@Request.faClassNum~&apos;)
       and (fixed_asset.fa_id in (@SingleQuote.Request.faId~) or &apos;@Request.faId~&apos; is null)
	   and (REGEXP_LIKE( &apos;@Request.balSheetOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.bal_sheet_ou_id in (@SingleQuote.Request.balSheetOuId~))
	   and (REGEXP_LIKE( &apos;@Request.leOuId~&apos;, &apos;(^|,)all($|,)&apos; ) or organization_unit.le_ou_id in (@SingleQuote.Request.leOuId~))),
   summarizedIncomeExpense as
   (select fa_id,
           fa_descr,
           fa_class_num,
           par_fa_id,
           ou_id,
           ou_name,
           le_ou_id,
           bal_sheet_ou_id,
		   transaction_type,
           um,
           um_descr,
           eq_work_stat_cd,
           eq_work_stat_descr,
           fa_exp_cat_num,
           fa_exp_cat_descr,
           sum( case in_current_period
                   when &apos;Y&apos; then income_amt
                   else 0
                end ) as prd_income_amt,
           sum( case in_current_period
                   when &apos;Y&apos; then expense_amt
                   else 0
                end ) as prd_expense_amt,
           sum( case in_current_period
                   when &apos;Y&apos; then profit_amt
                   else 0
                end ) as prd_profit_amt,
           sum( case in_current_period
                   when &apos;Y&apos; then units_chrgd
                   else 0
                end ) as prd_units_chrgd,
           sum( income_amt ) as ytd_income_amt,
           sum( expense_amt ) as ytd_expense_amt,
           sum( profit_amt ) as ytd_profit_amt,
		   sum( units_chrgd ) as ytd_units_chrgd
      from eqIncomeExpense
     group by fa_id,
           fa_descr,
           fa_class_num,
           par_fa_id,
           ou_id,
           ou_name,
           le_ou_id,
           bal_sheet_ou_id,
		   transaction_type,
           um,
           um_descr,
           eq_work_stat_cd,
           eq_work_stat_descr,
           fa_exp_cat_num,
           fa_exp_cat_descr)
select fa_class.descr as fa_class_descr,
       par_fixed_asset.descr as par_fa_descr,
       leou.name as le_ou_name,
       bsou.name as bal_sheet_ou_name,
       summarizedIncomeExpense.*
  from summarizedIncomeExpense
       join fa_class
         on fa_class.fa_class_num = summarizedIncomeExpense.fa_class_num
       join fixed_asset par_fixed_asset
         on par_fixed_asset.fa_id = summarizedIncomeExpense.par_fa_id
       join organization_unit leou
         on leou.ou_id = summarizedIncomeExpense.le_ou_id
       join organization_unit bsou
         on bsou.ou_id = summarizedIncomeExpense.bal_sheet_ou_id"
				Type="SQL"
			/>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="FA_CLASS_NUM"
				DataType="Number"
				Header="Asset Class ID"
				ID="colASSET_CLASS_ID"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="Number"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="FA_CLASS_DESCR"
				DataType="Text"
				Header="Asset Class Name"
				ID="colASSET_CLASS_NAME"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="BAL_SHEET_OU_ID"
				DataType="@Local.OU_DATA_TYPE~"
				Header="Balance Sheet OU ID"
				ID="colBAL_SHEET_OU_ID"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="@Local.OU_DATA_TYPE~"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="@Local.OU_EXCEL_FORMAT~"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="BAL_SHEET_OU_NAME"
				DataType="Text"
				Header="Balance Sheet OU Name"
				ID="colBAL_SHEET_OU_NAME"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="FA_EXP_CAT_NUM"
				DataType="Number"
				Header="Expense Category #"
				ID="colEXP_CAT_ID"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="Number"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="FA_EXP_CAT_DESCR"
				DataType="Text"
				Header="Expense Category Description"
				ID="colEXP_CAT_NAME"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="LE_OU_ID"
				DataType="@Local.OU_DATA_TYPE~"
				Header="Legal Entity OU ID"
				ID="colLE_OU_ID"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="@Local.OU_DATA_TYPE~"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="@Local.OU_EXCEL_FORMAT~"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="LE_OU_NAME"
				DataType="Text"
				Header="Legal Entity OU Name"
				ID="colLE_OU_NAME"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="PAR_FA_ID"
				DataType="@Local.FA_DATA_TYPE~"
				Header="Parent Asset ID"
				ID="colPAR_FA_ID"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="@Local.JOB_DATA_TYPE~"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="@Local.JOB_EXCEL_FORMAT~"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="PAR_FA_DESCR"
				DataType="Text"
				Header="Parent Asset Description"
				ID="colPAR_FA_NAME"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="UM"
				DataType="Text"
				Header="Unit of Measure"
				ID="colUM_ID"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="EQ_WORK_STAT_CD"
				DataType="Text"
				Header="Work Status Code"
				ID="colWORK_STAT_CD"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="False"
				DataColumn="EQ_WORK_STAT_DESCR"
				DataType="Text"
				Header="Work Status Description"
				ID="colWORK_STAT_DESCR"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<Note
				Note="Start of Default Fields"
			/>
			<AnalysisGridColumn
				ColumnVisible="True"
				DataColumn="OU_ID"
				DataType="@Local.OU_DATA_TYPE~"
				Header="OU ID"
				ID="colOU_ID"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="@Local.OU_DATA_TYPE~"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="@Local.OU_EXCEL_FORMAT~"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="True"
				DataColumn="OU_NAME"
				DataType="Text"
				Header="OU Name"
				ID="colOU_NAME"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="True"
				DataColumn="FA_ID"
				DataType="@Local.FA_DATA_TYPE~"
				Header="Asset ID"
				ID="colASSET_ID"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="@Local.FA_DATA_TYPE~"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="@Local.FA_EXCEL_FORMAT~"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="True"
				DataColumn="FA_DESCR"
				DataType="Text"
				Header="Asset Description"
				ID="colASSET_NAME"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="True"
				DataColumn="TRANSACTION_TYPE"
				DataType="Text"
				Header="Transaction Type"
				ID="colTRANSACTION_TYPE"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				ColumnVisible="True"
				DataColumn="UM_DESCR"
				DataType="Text"
				Header="Unit of Measure Description"
				ID="colUM_NAME"
				NoAgAggregates="True"
				>
				<ExcelColumnFormat
					DataType="Text"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="Text"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				Class="ThemeAlignRight"
				ColumnVisible="True"
				DataColumn="PRD_INCOME_AMT"
				DataType="Number"
				Format="##,##0.00"
				Header="Period Revenue"
				ID="colPRD_REVENUE_AMT"
				NoAgAggregates="False"
				>
				<ExcelColumnFormat
					DataType="Number"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="#,##0.00"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
				<ConditionalClass
					Class="ThemeAlignRight ThemeTextNegative"
					Condition="@Data.PRD_INCOME_AMT~ &lt; 0"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				Class="ThemeAlignRight"
				ColumnVisible="True"
				DataColumn="PRD_EXPENSE_AMT"
				DataType="Number"
				Format="##,##0.00"
				Header="Period Expenses"
				ID="colPRD_EXPENSE_AMT"
				NoAgAggregates="False"
				>
				<ExcelColumnFormat
					DataType="Number"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="#,##0.00"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
				<ConditionalClass
					Class="ThemeAlignRight ThemeTextNegative"
					Condition="@Data.PRD_EXPENSE_AMT~ &lt; 0"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				Class="ThemeAlignRight"
				ColumnVisible="True"
				DataColumn="PRD_PROFIT_AMT"
				DataType="Number"
				Format="##,##0.00"
				Header="Period Profit/Loss"
				ID="colPRD_PROFIT_AMT"
				NoAgAggregates="False"
				>
				<ExcelColumnFormat
					DataType="Number"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="#,##0.00"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
				<ConditionalClass
					Class="ThemeAlignRight ThemeTextNegative"
					Condition="@Data.PRD_PROFIT_AMT~ &lt; 0"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				Class="ThemeAlignRight"
				ColumnVisible="True"
				DataColumn="YTD_INCOME_AMT"
				DataType="Number"
				Format="##,##0.00"
				Header="YTD Revenue"
				ID="colYTD_REVENUE_AMT"
				NoAgAggregates="False"
				>
				<ExcelColumnFormat
					DataType="Number"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="#,##0.00"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
				<ConditionalClass
					Class="ThemeAlignRight ThemeTextNegative"
					Condition="@Data.YTD_INCOME_AMT~ &lt; 0"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				Class="ThemeAlignRight"
				ColumnVisible="True"
				DataColumn="YTD_EXPENSE_AMT"
				DataType="Number"
				Format="##,##0.00"
				Header="YTD Expenses"
				ID="colYTD_EXPENSE_AMT"
				NoAgAggregates="False"
				>
				<ExcelColumnFormat
					DataType="Number"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="#,##0.00"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
				<ConditionalClass
					Class="ThemeAlignRight ThemeTextNegative"
					Condition="@Data.YTD_EXPENSE_AMT~ &lt; 0"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				Class="ThemeAlignRight"
				ColumnVisible="True"
				DataColumn="YTD_PROFIT_AMT"
				DataType="Number"
				Format="##,##0.00"
				Header="YTD Profit/Loss"
				ID="colYTD_PROFIT_AMT"
				NoAgAggregates="False"
				>
				<ExcelColumnFormat
					DataType="Number"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="#,##0.00"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
				<ConditionalClass
					Class="ThemeAlignRight ThemeTextNegative"
					Condition="@Data.YTD_PROFIT_AMT~ &lt; 0"
				/>
			</AnalysisGridColumn>
			<Note
				Note="End of Default Fields"
			/>
			<AnalysisGridColumn
				Class="ThemeAlignRight"
				ColumnVisible="False"
				DataColumn="PRD_UNITS_CHRGD"
				DataType="Number"
				Format="##,##0.00"
				Header="Period Revenue Units"
				ID="colPRD_UNITS_CHRGD"
				NoAgAggregates="False"
				>
				<ExcelColumnFormat
					DataType="Number"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="#,##0.00"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
				<ConditionalClass
					Class="ThemeAlignRight ThemeTextNegative"
					Condition="@Data.PRD_UNITS_CHRGD~ &lt; 0"
				/>
			</AnalysisGridColumn>
			<AnalysisGridColumn
				Class="ThemeAlignRight"
				ColumnVisible="False"
				DataColumn="YTD_UNITS_CHRGD"
				DataType="Number"
				Format="##,##0.00"
				Header="YTD Revenue Units"
				ID="colYTD_UNITS_CHRGD"
				NoAgAggregates="False"
				>
				<ExcelColumnFormat
					DataType="Number"
					ExcelAutoFitRow="True"
					ExcelColumnWidth="Auto"
					ExcelFormat="#,##0.00"
					FontBold="False"
					FontItalic="False"
					FontName="Calibri"
					FontSize="11"
				/>
				<ConditionalClass
					Class="ThemeAlignRight ThemeTextNegative"
					Condition="@Data.YTD_UNITS_CHRGD~ &lt; 0"
				/>
			</AnalysisGridColumn>
		</AnalysisGrid>
		<IncludeSharedElement
			DefinitionFile="SharedElements.seReportButtons"
			SharedElementID="seReportButtons"
			>
			<SharedElementParams
				restoreGrid="pnlSavedBookmarks"
			/>
			<PassedSharedElement
				ID="pseRefreshAfterClearParams"
				>
				<Action
					ID="actPrSaveTemplate"
					Process="OpenReport"
					TaskID="openEQReport"
					Type="Process"
					>
					<LinkParams
						balSheetOuId="@Request.balSheetOuId~"
						cusId="@Request.cusId~"
						faClassNum="@Request.faClassNum~"
						faId="@Request.faId~"
						fromPersonalReport="@Request.fromPersonalReport~"
						jobId="@Request.jobId~"
						jobStatCd="@Request.jobStatCd~"
						jtCd="@Request.jtCd~"
						leOuId="@Request.leOuId~"
						menuItem="@Request.menuItem~"
						myReport="@Request.myReport~"
						ouId="@Request.ouId~"
						parJobId="@Request.parJobId~"
						parOuId="@Request.parOuId~"
						periodEndDate="@Request.periodEndDate~"
						projectManagerId="@Request.projectManagerId~"
						rdAgAggrRowPosition="@Request.rdAgAggrRowPosition~"
						rdAgHideFunctionNamesCheckbox="@Request.rdAgHideFunctionNamesCheckbox~"
						rdAgRefreshData="True"
						reportName="@Request.reportName~"
						throughDate="@Request.throughDate~"
						venId="@Request.venId~"
					/>
				</Action>
			</PassedSharedElement>
			<PassedSharedElement
				ID="pseSaveParams"
				>
				<Action
					ID="apSave"
					Process="CheckSaveName"
					TaskID="CheckNameEquipmentProfitability"
					Type="Process"
					>
					<LinkParams
						balSheetOuId="@Request.balSheetOuId~"
						cusId="@Request.cusId~"
						faClassNum="@Request.faClassNum~"
						faId="@Request.faId~"
						fromPersonalReport="@Request.fromPersonalReport~"
						jobId="@Request.jobId~"
						jobStatCd="@Request.jobStatCd~"
						jtCd="@Request.jtCd~"
						leOuId="@Request.leOuId~"
						menuItem="@Request.menuItem~"
						myReport="@Request.myReport~"
						ouId="@Request.ouId~"
						parJobId="@Request.parJobId~"
						parOuId="@Request.parOuId~"
						periodEndDate="@Request.periodEndDate~"
						projectManagerId="@Request.projectManagerId~"
						rdAgAggrRowPosition="@Request.rdAgAggrRowPosition~"
						rdAgHideFunctionNamesCheckbox="@Request.rdAgHideFunctionNamesCheckbox~"
						rdAgRefreshData="True"
						reportName="@Request.reportName~"
						throughDate="@Request.throughDate~"
						venId="@Request.venId~"
					/>
					<Action
						ID="validateInputSave"
						Javascript="var inputText = encodeURIComponent(document.getElementById(&quot;itSave&quot;).value.trim());


if (inputText == &apos;&apos;)
{ 
	jAlert(&apos;Please enter a report name.&apos;, &apos;Warning&apos;);
	return false;
}
else
{	
	return true;
}"
						Type="PreActionJavascript"
					/>
				</Action>
			</PassedSharedElement>
			<PassedSharedElement
				ID="pseRefreshParams"
				>
				<Action
					ID="actPrSaveTemplate"
					Process="OpenReport"
					TaskID="EQTemplate"
					Type="Process"
					>
					<LinkParams
						balSheetOuId="@Request.balSheetOuId~"
						cusId="@Request.cusId~"
						faClassNum="@Request.faClassNum~"
						faId="@Request.faId~"
						fromPersonalReport="@Request.fromPersonalReport~"
						jobId="@Request.jobId~"
						jobStatCd="@Request.jobStatCd~"
						jtCd="@Request.jtCd~"
						leOuId="@Request.leOuId~"
						menuItem="@Request.menuItem~"
						myReport="@Request.myReport~"
						ouId="@Request.ouId~"
						parJobId="@Request.parJobId~"
						parOuId="@Request.parOuId~"
						periodEndDate="@Request.periodEndDate~"
						projectManagerId="@Request.projectManagerId~"
						rdAgAggrRowPosition="@Request.rdAgAggrRowPosition~"
						rdAgHideFunctioNamesCheckbox="@Request.rdAgHideFunctionNamesCheckbox~"
						rdAgRefreshData="True"
						reportName="@Request.reportName~"
						throughDate="@Request.throughDate~"
						venId="@Request.venId~"
					/>
				</Action>
			</PassedSharedElement>
			<PassedSharedElement
				ID="pseHelpLink"
				>
				<Action
					ID="actHelpLink"
					Type="Link"
					>
					<Target
						ID="tgtHelpLink"
						Link="javascript:url = &quot;http://www.penta.com/clientresources/PENTAWorkbench/Default.htm#PENTA Workbenches/Panel Descriptions/Equipment and Fixed Assets/Equipment Profitability Trend.htm&quot;;
window.open(url);"
						Type="Link"
					/>
				</Action>
			</PassedSharedElement>
			<PassedSharedElement
				ID="pseSaveCoParams"
			/>
		</IncludeSharedElement>
	</Body>
	<ReportFooter>
		<IncludeSharedElement
			DefinitionFile="SharedElements.seReportFooter"
			SharedElementID="seReportFooter"
			>
			<SharedElementParams
				bookmarkRequestParams="balSheetOuId,cusId,faClassNum,faId,jobId,jobStatCd,jtCd,leOuId,menuItem,myReport,ouId,parJobId,parOuId,periodEndDate,projectManagerId,rdAgRefreshData,reportName,throughDate,venId,rdAgAggrRowPosition,rdAgHideFunctionNamesCheckbox"
				restoreGrid="pnlSavedBookmarks"
			/>
		</IncludeSharedElement>
	</ReportFooter>
	<IncludeSharedElement
		DefinitionFile="SharedElements.sePrintablePaging"
		SharedElementID="sePrintablePaging"
	/>
	<ideTestParams
		balSheetOuId=""
		cusId=""
		faClassNum=""
		faId=""
		fromPersonalReport=""
		jobId=""
		jobStatCd=""
		jtCd=""
		leOuId=""
		menuItem="2000"
		myReport=""
		ouId="all"
		parJobId=""
		parOuId="all"
		periodEndDate=""
		projectManagerId=""
		rdAgAggrRowPosition=""
		rdAgHideFunctionNamesCheckbox=""
		reportName=""
		templateBookmarkID=""
		throughDate=""
		venId=""
	/>
</Report>
