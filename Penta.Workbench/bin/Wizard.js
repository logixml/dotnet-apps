var applicationPath = "";

function SetFocus()
{			    
	if (document.getElementById('ReturnText') != null)
	{
		document.getElementById('ReturnText').focus();
	}
}

function setFocusFirstTextBox()
{
	var pageInputs =  document.getElementsByTagName('INPUT'); 
	
	if (pageInputs.length > 0)
	{ 
		for (var i = 0; i < pageInputs.length; i++)
		{
			if (pageInputs[i].type == 'text' && pageInputs[i].id != 'editField')
			{			                
				pageInputs[i].focus();
				break;
			}
		}			        
	}
}

function btnCancel_Click()
{
	document.getElementById('editField').value = '';
	document.getElementById('overlay').style.display = 'none';
	document.getElementById('modalEdit').style.display = 'none';			    
}

function checkAll()
{
	var currInput;
	var formInputs = document.getElementsByTagName('Input');
	
	for (var i = 0; i < formInputs.length; i++)
	{
		currInput = formInputs[i];			        
		if (currInput.type == 'checkbox')
			currInput.checked = true;
	}
}

function unCheckAll()
{
	var currInput;
	var formInputs = document.getElementsByTagName('Input');
	
	for (var i = 0; i < formInputs.length; i++)
	{
		currInput = formInputs[i];			        
		if (currInput.type == 'checkbox')
			currInput.checked = false;
	}
}

function showThemePreview(path, cbo)
{
    applicationPath = path;
	var previewImage = document.getElementById('previewImage');
	var noTheme = document.getElementById('noThemeSelected');
	
	if (cbo.value != '')
	{
		noTheme.style.display = 'none';
		previewImage.style.display = '';
		previewImage.src = path + cbo.value;
	}
	else
	{			        
		previewImage.style.display = 'none';
		noTheme.style.display = '';
	}
}

function selectRadio(radioName)
{
	document.getElementById('rad' + radioName).checked = true;
	
	//Additional highlighting for size selection
	if (radioName == 'Small' || radioName == 'Medium' || radioName == 'Large')
	{
		var sizeDivs = Y.all('.WizSizeSelect');
		
		sizeDivs.each( function(currSizeDiv) {					
			if (currSizeDiv.one('[type=radio]').get('id') == 'rad' + radioName)
				currSizeDiv.addClass('WizDashedContainterSelected');
			else
				currSizeDiv.removeClass('WizDashedContainterSelected');
		});
	}	
}

function initSlider(maxValue, initValue, updateID)
{
	var slider = new Y.Slider({
		min: 1,
		max: maxValue,
		value: initValue,
		length: '215px'
	});
	
	slider.after('valueChange', function(e) {
		document.getElementById(updateID).innerText = e.newVal;
		
		var imgDashColumnSample = document.getElementById('imgDashColumnSample');					
		
		if (imgDashColumnSample != null)
		    imgDashColumnSample.src = applicationPath + 'Dashboard' + e.newVal + 'Column.png';
	});
	slider.render('#sldWizard');
	document.getElementById(updateID).innerText = initValue;
}			

//Etl Create Table Wizard functions
//-------------------------------------------------------------------------
function addColumn()
{			    				
	var table = document.getElementById('tblColumnSetup');
	var rowID = table.rows.length - 1;
	
	//create the row
	var row =  table.insertRow(rowID);
	
	//create the column name cell
	var cellColumnName = row.insertCell(0);
	var txtColumnName = document.createElement('input');
	txtColumnName.type = 'text';	            
	txtColumnName.size = 30;
	cellColumnName.appendChild(txtColumnName);
					
	//create the type cell
	var cellType = row.insertCell(1);	            
	var cboType = document.createElement('select');
	cboType.id = 'cboType' + rowID;	            
	cboType.options[0] = new Option('Varchar', 'varchar');
	cboType.options[1] = new Option('nVarchar', 'nvarchar');
	cboType.options[2] = new Option('Date and Time', 'datetime');
	cboType.options[3] = new Option('Integer', 'int');
	cboType.options[4] = new Option('Decimal', 'decimal');
	cboType.options[5] = new Option('Text', 'text');
	cboType.options[6] = new Option('Boolean', 'boolean');
	var txtSize = document.createElement('input');
	cboType.onchange = function(){cboType_Changed(this, 'txtSize' + rowID)};
	cellType.appendChild(cboType);
	
	//create the size cell
	var cellSize = row.insertCell(2);
	txtSize.id = 'txtSize' + rowID;	            
	txtSize.type = 'text';	            
	txtSize.size = 10;
	txtSize.value = '50';
	cellSize.appendChild(txtSize);
	
	//create the delete button for the row
	var cellDelete = row.insertCell(3);
	var imgDelete = document.createElement('img');
	imgDelete.id = 'imgDelete' + rowID;
	imgDelete.src = applicationPath + 'delete.png';
	imgDelete.style.cursor = 'pointer';
	imgDelete.onclick = function(){deleteColumn(rowID)};
	cellDelete.appendChild(imgDelete);
					
	row.appendChild(cellColumnName);	            
	row.appendChild(cellType);
	row.appendChild(cellSize);
	row.appendChild(cellDelete);
	txtColumnName.focus();                       
}

function deleteColumn(rowID)
{   
	var table = document.getElementById('tblColumnSetup');
	
	//Update rows following
	for (var i = rowID + 1; i < table.rows.length - 1; i++)
	{  
		(function(i)
		{      
			var imgDelete;
			var txtSize;
			var cboType;                     
			var adjRowID;
			
			adjRowID = i - 1;
			imgDelete = document.getElementById('imgDelete' + i);                        
			imgDelete.id = 'imgDelete' + adjRowID;                    
			imgDelete.onclick = function(){deleteColumn(adjRowID)};                        
			
			txtSize = document.getElementById('txtSize' + i);
			txtSize.id = 'txtSize' + adjRowID;
			
			cboType = document.getElementById('cboType' + i);
			cboType.id = 'cboType' + adjRowID;
			cboType.onchange = function(){cboType_Changed(this, txtSize.id)};
		})(i);
	}                              
	
	//Delete the row                
	table.deleteRow(rowID);
}

function cboType_Changed(cboType, sizeID)
{    
	var txtSize = document.getElementById(sizeID);
	txtSize.style.visibility = 'visible';
	txtSize.disabled = false;
	
	if (cboType.value == 'varchar' || cboType.value == 'nvarchar')
		txtSize.value = '50';
	else if (cboType.value == 'int')
	{
		txtSize.value = '4'
		txtSize.disabled = true;
	}
	else if (cboType.value == 'decimal')
		txtSize.value = '22,10';
	else if (cboType.value == 'boolean')
	{
		txtSize.value = '1';
		txtSize.disabled = true;
	}                    
	else
		txtSize.style.visibility = 'hidden';
}

//XOLAP Wizard functions
//-------------------------------------------------------------------------

var lastSelectedDimensionID = -1;			
var dimensionIDGen = 0;			
var dimensionLevels = new Array();

function lstDimensions_onChange(sender)
{
	if (sender.selectedIndex != -1)
	{
		var i;
		var tblLevels = document.getElementById('tblLevels');
		var lstAvailableLevels = document.getElementById('lstAvailableLevels');
		var lstSelectedLevels = document.getElementById('lstSelectedLevels');
		var selectedDimension = sender.options[sender.selectedIndex];
		
		tblLevels.style.display = '';
		
		//Save selected levels from last selection			        			        
		if (lastSelectedDimensionID != -1)
		{
			dimensionLevels[lastSelectedDimensionID].length = 0;
			for (i = 0; i < lstSelectedLevels.length; i++)
				dimensionLevels[lastSelectedDimensionID][dimensionLevels[lastSelectedDimensionID].length] = lstSelectedLevels[i].value;
		}
		lastSelectedDimensionID = selectedDimension.value;
		
		//Clear both listboxes			        
		lstAvailableLevels.options.length = 0;			        
		lstSelectedLevels.options.length = 0;    
		
		//Load levels for the selected dimension
		var arrAvailableLevels = document.getElementById('originalColumns').value.split('|');			       			        
		var newLevel;
			
		if(dimensionLevels[selectedDimension.value].length > 0)
		{
			var arrSelectedValues = dimensionLevels[selectedDimension.value];
			//load selected values
			for (i = 0; i < arrSelectedValues.length; i++)
			{
				var strPadding = '';
				for (var i = 0; i < lstSelectedLevels.options.length; i++)
					strPadding += '   ';
				
				newLevel = new Option(strPadding + arrSelectedValues[i], arrSelectedValues[i], false, false);
				lstSelectedLevels.options[lstSelectedLevels.options.length] = newLevel;
			}		             
			
			var levelSelected;
			var j;
			//load remaining values into the available list
			for (i = 0; i < arrAvailableLevels.length; i++)
			{
				levelSelected = false;			                
				for (j = 0; j < arrSelectedValues.length; j++)
				{
					if (arrAvailableLevels[i] == arrSelectedValues[j])
					{
						levelSelected = true;
						break;
					}
				}
				
				if (!levelSelected)
				{
					newLevel = new Option(arrAvailableLevels[i], arrAvailableLevels[i], false, false);
					lstAvailableLevels.options[lstAvailableLevels.options.length] = newLevel;
				}			                
			}
			
			//Show lstSelectedLevels if it is hidden
			if (lstSelectedLevels.style.display == 'none')
			{
				lstSelectedLevels.style.display = '';
				document.getElementById('emptySelection').style.display = 'none';
			}
			
			if (lstAvailableLevels.options.length > 0)
			{
				//show lstAvailableLevels
				lstAvailableLevels.style.display = ''
				document.getElementById('emptyLevels').style.display = 'none';
			}
			else
			{
				//hide lstAvailableLevels
				lstAvailableLevels.style.display = 'none'
				document.getElementById('emptyLevels').style.display = '';			                
			}
		}
		else
		{
			//load all available values
			for (i = 0; i < arrAvailableLevels.length; i++)
			{
				newLevel = new Option(arrAvailableLevels[i], arrAvailableLevels[i], false, false);			                
				lstAvailableLevels.options[lstAvailableLevels.options.length] = newLevel;			                             
			}
			//Hide lstSelectedLevels if needed
			if (lstSelectedLevels.style.display == '')
			{
				lstSelectedLevels.style.display = 'none';
				document.getElementById('emptySelection').style.display = '';
			}
			//show lstAvailableLevels
			lstAvailableLevels.style.display = ''
			document.getElementById('emptyLevels').style.display = 'none';
		}
	}
	
}

function newDimensionKeyDown(event)			
{			    
	var e = event || window.event;			    
	
	if (e.keyCode == 13)
		addDimension()
}

function addDimension()
{
	var txtNewDimension = document.getElementById('newDimensionName');
	var dimensionName = txtNewDimension.value;			    
	var lstDimensions = document.getElementById('lstDimensions');
	var dimensionExists = false;
	var i;
	
	//Check to see if the dimension already exists
	for (i = 0; i < lstDimensions.options.length; i++)
	{			        
		if (lstDimensions.options[i].value == dimensionName)
			dimensionExists = true;
	}			    
   
	//If not then add the new dimension
	if (dimensionName != '')
	{
		if (!dimensionExists)
		{			        
			var newDimension = new Option(dimensionName, dimensionIDGen, false, false);
			var newDimIndex;
			dimensionIDGen++;
			
			newDimIndex = lstDimensions.options.length;
			
			lstDimensions.options[newDimIndex] = newDimension;
			var arrSelection = new Array();
			dimensionLevels[newDimension.value] = arrSelection;
			txtNewDimension.value = '';			            
			 
			//If we are hiding this list because it is empty we need to show it.
			if (lstDimensions.style.display == 'none')
			{			        
				lstDimensions.style.display = '';
				var emptyDim = document.getElementById('emptyDimensions');
				emptyDim.style.display = 'none';
			}
			
			lstDimensions.focus();
			lstDimensions.selectedIndex = newDimIndex;
			lstDimensions_onChange(lstDimensions)
			
			saveDimensions();
		}
		else
			alert('Dimension already exists.');
	 }
}

function renameDimension()
{
	var lstDimensions = document.getElementById('lstDimensions');
	var selectedIndex = -1;
	var i;			    
	
	//Make sure a dimension is selected
	for (i = 0; i < lstDimensions.options.length; i++)			    
		if (lstDimensions.options[i].selected)
			selectedIndex = i;
	
	if (selectedIndex != -1)
	{        
		var overlay = document.getElementById('overlay');
		var modalEdit = document.getElementById('modalEdit');			    
		overlay.style.width = document.body.clientWidth;
		overlay.style.height = document.body.clientHeight;
		overlay.style.display = '';
		modalEdit.style.display = '';
		
		var editField = document.getElementById('editField');
		editField.value = lstDimensions.options[selectedIndex].text;
		editField.focus();
		editField.select();
		document.getElementById('btnOK').onclick = function() { renameDimensionOK(); };
	}
}

function renameDimensionOK()
{			
	var editField = document.getElementById('editField');
	var newValue = editField.value;
	editField.value = '';
	
	if (newValue != '')
	{    			
		document.getElementById('overlay').style.display = 'none';
		document.getElementById('modalEdit').style.display = 'none';
		
		var lstDimensions = document.getElementById('lstDimensions');
		var selectedIndex = -1;
		var i;			    
		
		//Make sure a dimension is selected
		for (i = 0; i < lstDimensions.options.length; i++)			    
			if (lstDimensions.options[i].selected)
				selectedIndex = i;
		
		//Rename Dimension
		lstDimensions.options[selectedIndex].text = newValue;
		
		saveDimensions();
	}
}

function removeDimension()
{
	var lstDimensions = document.getElementById('lstDimensions');
	var lstSelectedLevels = document.getElementById('lstSelectedLevels');			    
	var selectedIndex = -1;
	var i;			    
	
	//Make sure a dimension is selected
	for (i = 0; i < lstDimensions.options.length; i++)			    
		if (lstDimensions.options[i].selected)
			selectedIndex = i;
	
	if (selectedIndex != -1)
	{
		//Remove the levels for the dimension
		dimensionLevels[lstDimensions.options[selectedIndex].value].length = 0
	
		//Remove the dimension from the dimension list
		var tmpIndex = lstDimensions.selectedIndex;
		lstDimensions[selectedIndex] = null;
		if (tmpIndex == 0)
			lstDimensions.selectedIndex = tmpIndex;
		else
			lstDimensions.selectedIndex = tmpIndex - 1; 
			
		//If we are hiding this list because it is empty we need to show it.
		if (lstDimensions.options.length == 0)
		{			        
			lstDimensions.style.display = 'none';
			var emptyDim = document.getElementById('emptyDimensions');
			emptyDim.style.display = '';
			document.getElementById('tblLevels').style.display = 'none';		                
		}		            
							
		//Hide lstSelectedLevels if needed
		if (lstSelectedLevels.style.display == '')
		{
			lstSelectedLevels.style.display = 'none';
			document.getElementById('emptySelection').style.display = '';
		}
		
		lstDimensions_onChange(lstDimensions);
		
		saveDimensions();
	}
}

function selectLevel()
{
	var lstAvailableLevels = document.getElementById('lstAvailableLevels');
	var lstSelectedLevels = document.getElementById('lstSelectedLevels');
					
	if (lstAvailableLevels.selectedIndex != -1)
	{		    
		var selectedItem = lstAvailableLevels[lstAvailableLevels.selectedIndex];
		var arrDateColumns = document.getElementById('dateColumns').value.split('|');
		var isDate = false;
		
		for (var i = 0; i < arrDateColumns.length; i++)			        
			if (arrDateColumns[i] == selectedItem.value)
				isDate = true;			        
		
		if (isDate)
		{
			//Show overlay
			var overlay = document.getElementById('overlay');			            
			overlay.style.width = document.body.clientWidth;
			overlay.style.height = document.body.clientHeight;
			overlay.style.display = '';
			
			//Show dialog
			var btnDateNo = document.getElementById('btnDateNo');
			btnDateNo.value = 'No thanks. Just add ' + selectedItem.value + '.';
			document.getElementById('dateColumn').value = selectedItem.value;
			document.getElementById('divAutoTPColumns').style.display = '';			            
		}
		else
			addSelectedLevel(selectedItem.value);			       		        
		
		saveDimensions();
	}			    
}

function addSelectedLevel(selectedValue)
{
	var lstAvailableLevels = document.getElementById('lstAvailableLevels');
	var lstSelectedLevels = document.getElementById('lstSelectedLevels');
	var strPadding = '';
	
	//Add selected level			    
	for (var i = 0; i < lstSelectedLevels.options.length; i++)
		strPadding += '   ';
		
	var newSelected = new Option(strPadding + selectedValue, selectedValue, false, false);			        
	lstSelectedLevels.options[lstSelectedLevels.options.length] = newSelected;
	
	//Remove level from available list
	var tmpIndex;
	if (lstAvailableLevels.selectedIndex != -1 && lstAvailableLevels.options[lstAvailableLevels.selectedIndex].value == selectedValue)
		tmpIndex = lstAvailableLevels.selectedIndex;
	else
	{
		//Find the index
		for (var i = 0; i < lstAvailableLevels.options.length; i++)
		{
			if (lstAvailableLevels.options[i].value == selectedValue)
			{
				tmpIndex = i;
				break;   
			}
		}
	}
	
	lstAvailableLevels.options[tmpIndex] = null;
	//if (tmpIndex == 0)
		lstAvailableLevels.selectedIndex = tmpIndex;
	//else
	   // lstAvailableLevels.selectedIndex = tmpIndex - 1;        
	
	//Show lstSelectedLevels if it is hidden
	if (lstSelectedLevels.style.display == 'none')
	{
		lstSelectedLevels.style.display = '';
		document.getElementById('emptySelection').style.display = 'none';
	}
	//Hide lstAvailableLevels if it is empty
	if (lstAvailableLevels.options.length == 0)
	{
		lstAvailableLevels.style.display = 'none'
		document.getElementById('emptyLevels').style.display = '';
	}
}

function btnDateYesClick()
{
	var chkYear = document.getElementById('chkYear');
	var chkQuarter = document.getElementById('chkQuarter');
	var chkMonth = document.getElementById('chkMonth');
	var chkDay = document.getElementById('chkDay');
	var originalColumns = document.getElementById('originalColumns');
	var dateColumn = document.getElementById('dateColumn');
	var lstSelectedLevels = document.getElementById('lstSelectedLevels');
	var strPadding;
	
	//check the users selection.  skip time period columns that have already been added
	if (chkYear.checked)
	{   
		if (!levelExists(dateColumn.value + 'Year'))
		{
			//Add the new column
			strPadding = '';
			for (var i = 0; i < lstSelectedLevels.options.length; i++)
				strPadding += '   ';
			
			var newSelected = new Option(strPadding + dateColumn.value + 'Year', dateColumn.value + 'Year', false, false);
			lstSelectedLevels.options[lstSelectedLevels.options.length] = newSelected;
		}
		else if(!levelSelected(dateColumn.value + 'Year'))
			addSelectedLevel(dateColumn.value + 'Year');
		
	}
	//check the users selection.  skip time period columns that have already been added
	if (chkQuarter.checked)
	{   
		if (!levelExists(dateColumn.value + 'Quarter'))
		{
			//Add the new column
			strPadding = '';
			for (var i = 0; i < lstSelectedLevels.options.length; i++)
				strPadding += '   ';
				
			var newSelected = new Option(strPadding + dateColumn.value + 'Quarter', dateColumn.value + 'Quarter', false, false);
			lstSelectedLevels.options[lstSelectedLevels.options.length] = newSelected;
		}
		else if(!levelSelected(dateColumn.value + 'Quarter'))
			addSelectedLevel(dateColumn.value + 'Quarter');                    
	}
	//check the users selection.  skip time period columns that have already been added
	if (chkMonth.checked)
	{   
		if (!levelExists(dateColumn.value + 'Month'))
		{
			//Add the new column
			strPadding = '';
			for (var i = 0; i < lstSelectedLevels.options.length; i++)
				strPadding += '   ';
				
			var newSelected = new Option(strPadding + dateColumn.value + 'Month', dateColumn.value + 'Month', false, false);
			lstSelectedLevels.options[lstSelectedLevels.options.length] = newSelected;
		}
		else if(!levelSelected(dateColumn.value + 'Month'))
			addSelectedLevel(dateColumn.value + 'Month');
		
	}
	//check the users selection.  skip time period columns that have already been added
	if (chkDay.checked)
	{   
		if (!levelExists(dateColumn.value + 'Day'))
		{
		   //Add the new column
			strPadding = '';
			for (var i = 0; i < lstSelectedLevels.options.length; i++)
				strPadding += '   ';
				
			var newSelected = new Option(strPadding + dateColumn.value + 'Day', dateColumn.value + 'Day', false, false);
			lstSelectedLevels.options[lstSelectedLevels.options.length] = newSelected; 
		}
		else if(!levelSelected(dateColumn.value + 'Day'))
			addSelectedLevel(dateColumn.value + 'Day');
		
	}
	
	//Make sure list is visible.
	lstSelectedLevels.style.display = '';
	document.getElementById('emptySelection').style.display = 'none';
	
	//Hide overlay			    
	document.getElementById('overlay').style.display = 'none';
	document.getElementById('divAutoTPColumns').style.display = 'none';
	
	saveDimensions();
}

function levelExists(levelName)
{
	var lstAvailableLevels = document.getElementById('lstAvailableLevels');
	var lstSelectedLevels = document.getElementById('lstSelectedLevels');			    
	var i;
	
	for (i = 0; i < lstAvailableLevels.options.length; i++)
		if (lstAvailableLevels.options[i].value == levelName)
			return true;
			
	for (i = 0; i < lstSelectedLevels.options.length; i++)
		if (lstSelectedLevels.options[i].value == levelName)
			return true;			    
			
	return false;
}

function levelSelected(levelName)
{
	var lstSelectedLevels = document.getElementById('lstSelectedLevels');
	
	 for (var i = 0; i < lstSelectedLevels.options.length; i++)
		if (lstSelectedLevels.options[i].value == levelName)
			return true;
			
	 return false;
}

function btnDateNoClick()
{
	addSelectedLevel(document.getElementById('dateColumn').value);
	
	saveDimensions();
	
	//Hide overlay			    
	document.getElementById('overlay').style.display = 'none';
	document.getElementById('divAutoTPColumns').style.display = 'none';
}

function removeLevel()
{
	var lstAvailableLevels = document.getElementById('lstAvailableLevels');
	var lstSelectedLevels = document.getElementById('lstSelectedLevels');
	
	if (lstSelectedLevels.selectedIndex != -1)
	{
		var selectedItem = lstSelectedLevels[lstSelectedLevels.selectedIndex];
		//Add selected level
		var newSelected = new Option(selectedItem.value, selectedItem.value, false, false);
		
		 //insert alphabetically        
		for (var i = 0; i < lstAvailableLevels.options.length; i++)
		{			           
			if (selectedItem.value.toUpperCase() < lstAvailableLevels.options[i].value.toUpperCase())
			{
				lstAvailableLevels.add(newSelected, i);
				break;
			}
			else if (i == lstAvailableLevels.options.length - 1)
			{
				lstAvailableLevels.options[lstAvailableLevels.options.length] = newSelected;
				break;
			}
		}			        
		if (lstAvailableLevels.options.length == 0)			            
				lstAvailableLevels.options[lstAvailableLevels.options.length] = newSelected;	    			        
		
		//Remove level from available list
		var tmpIndex = lstSelectedLevels.selectedIndex;
		lstSelectedLevels.options[lstSelectedLevels.selectedIndex] = null;
		if (tmpIndex == 0)
			lstSelectedLevels.selectedIndex = tmpIndex;
		else
			lstSelectedLevels.selectedIndex = tmpIndex - 1; 
			
		//Show lstAvailableLevels if it is hidden
		if (lstAvailableLevels.style.display == 'none')
		{
			lstAvailableLevels.style.display = '';
			document.getElementById('emptyLevels').style.display = 'none';
		}
		//Hide lstSelectedLevels if it is empty
		if (lstSelectedLevels.options.length == 0)
		{
			lstSelectedLevels.style.display = 'none'
			document.getElementById('emptySelection').style.display = '';
		}
		
		reformatSelectedLevels()
		
		saveDimensions();
	}
}

function reformatSelectedLevels()
{
	var lstSelectedLevels = document.getElementById('lstSelectedLevels');
	var currLevel, strPadding;
	
	for (var i = 0; i < lstSelectedLevels.options.length; i++)
	{
		strPadding = '';
		currLevel = lstSelectedLevels.options[i];
		
		//create padding
		for (var j = 0; j < i; j++)
			strPadding += '   ';
			
		currLevel.text = strPadding + currLevel.value;
	}
}

function saveDimensions()
{
	var savedDimensions = document.getElementById('savedDimensions');
	var lstDimensions = document.getElementById('lstDimensions');			    
	var lstSelectedLevels = document.getElementById('lstSelectedLevels');
	var selectedDimensionID = -1;
	var dimensionName;
	var i, j;
	
	//clear the old value
	savedDimensions.value = '<XOLAPDimensions>'
	
	//Save the ID of the selected dimension
	if (lstDimensions.selectedIndex != -1)
		selectedDimensionID = lstDimensions[lstDimensions.selectedIndex].value;
	
	var currDimensionID;
	
	//Go through dimensionLevels and save them.  Skip the selected item and emtpy items
	for (i = 0; i < lstDimensions.options.length; i++)
	{
		currDimensionID = lstDimensions.options[i].value
		
		if (currDimensionID == selectedDimensionID && lstSelectedLevels.options.length > 0)
		{   
			dimensionName = lstDimensions[lstDimensions.selectedIndex];		                
	
			if (lstSelectedLevels.options.length > 0)
			{    			        
				savedDimensions.value += '<dim' + dimensionName.text.replace(' ', '') + ' DimensionName="' + dimensionName.text.replace(' ', '') + '" >';
				
				for (j = 0; j < lstSelectedLevels.options.length; j++)
					savedDimensions.value += '<Level name="' + lstSelectedLevels.options[j].value + '" />';
			
				savedDimensions.value += '</dim' + dimensionName.text.replace(' ', '') + '>';
			}
		}
		else if (dimensionLevels[currDimensionID].length > 0)
		{			                		                
			dimensionName = lstDimensions.options[i].text.replace(' ', '');
									
			savedDimensions.value += '<dim' + dimensionName + ' DimensionName="' + dimensionName + '" >';
			
			for (j = 0; j < dimensionLevels[currDimensionID].length; j++)
				savedDimensions.value += '<Level name="' + dimensionLevels[currDimensionID][j] + '" />';
		
			savedDimensions.value += '</dim' + dimensionName + '>';
		}			        
	}
	savedDimensions.value += '</XOLAPDimensions>'
}

function selectMeasure(path)
{
    applicationPath = path
	var table = document.getElementById('tblSelectedMeasures');
	var lstAvailableMeasures = document.getElementById('lstAvailableMeasures');
	
	if (lstAvailableMeasures.selectedIndex != -1)
	{
		var selectedItem = lstAvailableMeasures[lstAvailableMeasures.selectedIndex];
		
		var rowID = table.rows.length;
		
		//create the row
		var row =  table.insertRow(rowID);
		
		//create the column ID cell
		var cellColumnID = row.insertCell(0);
		var txtColumnID = document.createElement('input');
		txtColumnID.id = 'txtColumnID' + rowID;
		txtColumnID.type = 'text';	            
		txtColumnID.size = 15;
		txtColumnID.value = selectedItem.text;
		txtColumnID.disabled = true;
		cellColumnID.appendChild(txtColumnID);
		
		//create the Measure Name cell
		var cellMeasureName = row.insertCell(1);
		var txtMeasureName = document.createElement('input');
		txtMeasureName.type = 'text';	                
		txtMeasureName.size = 15;
		cellMeasureName.appendChild(txtMeasureName);
		
		//create the function cell
		var cellFunction = row.insertCell(2);
		var cboFunction = document.createElement('select');	                
		//add function values provided from the rules
		var arrFunctionValues = document.getElementById('measureFunctions').value.split('|');
		var i;
		for (i = 0; i < arrFunctionValues.length; i++)
		{
			var functionItem = new Option(arrFunctionValues[i], arrFunctionValues[i], false, false);
			cboFunction.options[cboFunction.options.length] = functionItem;
		}
		cellFunction.appendChild(cboFunction);	           
		
		//create the delete button for the row
		var cellDelete = row.insertCell(3);
		var imgDelete = document.createElement('img');
		imgDelete.id = 'imgDelete' + rowID;
		imgDelete.src = applicationPath + 'delete.png';
		imgDelete.style.cursor = 'pointer';
		imgDelete.onclick = function(){removeMeasure(rowID)};
		cellDelete.appendChild(imgDelete);
						
		row.appendChild(cellColumnID);	            
		row.appendChild(cellMeasureName);	                
		row.appendChild(cellFunction);
		row.appendChild(cellDelete);	                
		
		//Remove level from available list
		//Commented for #12202
		/*var tmpIndex = lstAvailableMeasures.selectedIndex;
		lstAvailableMeasures[lstAvailableMeasures.selectedIndex] = null;
		if (tmpIndex == 0)
			lstAvailableMeasures.selectedIndex = tmpIndex;
		else
			lstAvailableMeasures.selectedIndex = tmpIndex - 1;*/
		
		//Show table if needed
		if (table.style.display == 'none')
		{
			table.style.display = '';
			document.getElementById('emptySelection').style.display = 'none';
		}
		//Hide lstAvailableLevels if it is empty
		/*if (lstAvailableMeasures.options.length == 0)
		{
			lstAvailableMeasures.style.display = 'none'
			document.getElementById('emptyMeasures').style.display = '';
		}*/
		txtMeasureName.focus();
	}
}

var lastDataLayerTypeOption = null;
function OnDataLayerFocus(dropdown) {
    lastDataLayerTypeOption = dropdown.options[dropdown.selectedIndex].value
}
function OnDataLayerTypeChange(dropdown) {
    setDescriptionDisplay('none');
    lastDataLayerTypeOption = dropdown.options[dropdown.selectedIndex].value;
    setDescriptionDisplay('block');
}
function setDescriptionDisplay(display) {
    var divBlock = document.getElementById(lastDataLayerTypeOption + '_description');
    if (divBlock != null) {
        divBlock.style.display = display;
    }
}

function removeMeasure(rowID)
{                   
	var lstAvailableMeasures = document.getElementById('lstAvailableMeasures');
	var table = document.getElementById('tblSelectedMeasures');
	var txtColumnID = document.getElementById('txtColumnID' + rowID);
									
	//Add the removed measure back into the available measures
	//Commented for issue #12202
	/*var putbackMeasure = new Option(txtColumnID.value, txtColumnID.value, false, false);			        
					
	//insert alphabetically        
	for (var i = 0; i < lstAvailableMeasures.options.length; i++)
	{			           
	   if (putbackMeasure.value.toUpperCase() < lstAvailableMeasures.options[i].value.toUpperCase())
	   {
		   lstAvailableMeasures.add(putbackMeasure, i);
		   break;
	   }
	   else if (i == lstAvailableMeasures.options.length - 1)
	   {
		   lstAvailableMeasures.options[lstAvailableMeasures.options.length] = putbackMeasure;
		   break;
	   }
	}			        
	if (lstAvailableMeasures.options.length == 0)			            
	   lstAvailableMeasures.options[lstAvailableMeasures.options.length] = putbackMeasure;*/	                
	
	//Update rows following
	for (var i = rowID + 1; i < table.rows.length; i++)
	{  
		(function(i)
		{      
			var imgDelete;                        
			var adjRowID;                        
			
			adjRowID = i - 1;
			imgDelete = document.getElementById('imgDelete' + i);                        
			imgDelete.id = 'imgDelete' + adjRowID;                    
			imgDelete.onclick = function(){removeMeasure(adjRowID)};
			
			txtColumnID = document.getElementById('txtColumnID' + i);                        
			txtColumnID.id = 'txtColumnID' + adjRowID;
		})(i);
	}                              
	
	//Delete the row                
	table.deleteRow(rowID);
	
	//Hide table if needed
	if (table.rows.length == 1)
	{
		table.style.display = 'none';
		document.getElementById('emptySelection').style.display = '';
	}
	//Show table if needed
	if (lstAvailableMeasures.style.display == 'none')
	{
		lstAvailableMeasures.style.display = '';
		document.getElementById('emptyMeasures').style.display = 'none';
	}
}

//ChartGrid wizard functions
			
function dateColumnCheckChanged(chk, colName)
{
	var disable;
	
	if (chk.checked)
		disable = false;                
	else
		disable = true;
		
	var checkboxes = document.getElementsByTagName('input');
	
	for (var i = 0; i < checkboxes.length; i++)
	{                    
		if (checkboxes(i).type == 'checkbox' && checkboxes(i).id.indexOf(colName) != -1)
		{
			if (disable)
				checkboxes(i).disabled = true;
			else
				checkboxes(i).disabled = false;   
		}
	}
}

function wizExecute(cmd)
{
	window.location = 'DynaContent.html?//wizExecute=' + cmd;
}

