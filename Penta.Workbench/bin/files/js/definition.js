$(document).ready(function() { 

/*ToolTips*/
	$('#one_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_one.png" width="75" height="75" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:17px;padding-right:40px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">Tabs at the bottom of the Workspace panel allow you to see your work in three ways. The Definition tab is the report definition editor, with its Element Tree.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'top'
	});
	$('#two_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_two.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:4px;padding-right:40px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">The Source tab shows you the XML source code Studio creates when you add elements to the definition.  It’s possible to write code here by hand, but we don’t recommend it because you won’t necessarily know which elements can be the children of other elements.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'top'
	});

	$('#three_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_three.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:4px;padding-right:40px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">The Preview tab shows you a fully-functioning preview of your report right in Studio.<br>The current definition in the Workspace editor is saved when you click the Preview tab.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'top'
	});	
	// $('#four_tooltip').tooltipster({
	// 	content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_four.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:12px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">An alternative way of adding a child element is to right-click the Body tag and select and element from its context menu. Some developers think this is faster.</p></div></div>'),
	// 	maxWidth: 675,
	// 	minWidth: 675,
	// 	position: 'top'
	// });
	// $('#five_tooltip').tooltipster({
	// 	content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_five.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:12px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">5.	The Information panel is your source for quick help. Select an element or an attribute and information about it is shown in this panel. There’s also a link to more information about this item or topic on our DevNet website.</p></div></div>'),
	// 	maxWidth: 675,
	// 	minWidth: 675,
	// 	position: 'top'
	// });	
	// $('#six_tooltip').tooltipster({
	// 	content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_six.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:12px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">6.	The Attributes panel allows you to configure element properties, called “Attributes” in Logi-speak. Values can be typed-in or, in some cases, can be selected from option lists or pop-up tools. Attributes names can be double-clicked to open a larger window for easier data entry.<br>The Test Parameters panel lets you insert test values for parameters that are usually passed to the page. This panel is only visible if the report expects parameters and is not displayed until a report has been run or previewed once.</p></div></div>'),
	// 	maxWidth: 675,
	// 	minWidth: 675,
	// 	position: 'top'
	// });		
});