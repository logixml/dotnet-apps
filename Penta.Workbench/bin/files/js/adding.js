$(document).ready(function() {
 
/*ToolTips*/
	$('#one_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_one.png" width="75" height="75" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:16px;padding-right:40px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">Add a new report by clicking New File → Report in the Ribbon Menu (or open an existing report definition by clicking it in the Application panel).</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'bottom'
	});
	$('#two_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_two.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:16px;padding-right:40px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">In the Workspace editor, select an element by clicking it. New elements you add will be inserted below this one.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'bottom'
	});

	$('#three_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_three.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:15px;padding-right:40px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">In the Element Toolbox, double-click an element and it will be inserted as a child element of the selected element.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'bottom'
	});	
	$('#four_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_four.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:16px;padding-right:40px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">An alternative way of adding a child element is to right-click the selected element and select an element from its context menu. Some developers think this is faster.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'bottom'
	});
	// $('#five_tooltip').tooltipster({
	// 	content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_five.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:12px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">5.	The Information panel is your source for quick help. Select an element or an attribute and information about it is shown in this panel. There’s also a link to more information about this item or topic on our DevNet website.</p></div></div>'),
	// 	maxWidth: 675,
	// 	minWidth: 675,
	// 	position: 'top'
	// });	
	// $('#six_tooltip').tooltipster({
	// 	content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_six.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:12px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">6.	The Attributes panel allows you to configure element properties, called “Attributes” in Logi-speak. Values can be typed-in or, in some cases, can be selected from option lists or pop-up tools. Attributes names can be double-clicked to open a larger window for easier data entry.<br>The Test Parameters panel lets you insert test values for parameters that are usually passed to the page. This panel is only visible if the report expects parameters and is not displayed until a report has been run or previewed once.</p></div></div>'),
	// 	maxWidth: 675,
	// 	minWidth: 675,
	// 	position: 'top'
	// });		
});