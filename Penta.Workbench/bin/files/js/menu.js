$(document).ready(function() {
 

    $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
		var Id = $(this).attr('id');
		if ((Id) && (Id!="vv")){
			$('#contVideo').empty();
			$("#contVideo").load('files/videoyt.html');
		}    	
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
        $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");

    });

/*ToolTips*/
	$('#one_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="files/images/circ_one.png" width="75" height="75" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:8px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">Links here allow you to open existing applications, create new ones, and open applications registered with your local web server. <br>A link to DevNet, our community web site, takes you to documentation, samples, and forums.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'top'
	});
	$('#two_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="files/images/circ_two.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:12px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">Interesting information about new and highlighted <br>Logi Info features appears in a scrolling panel.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'top'
	});

	$('#three_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="files/images/circ_three.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:6px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">Recent applications can be opened from this list, including a special<br>“Getting Started” sample application installed with Logi Info.<br>Click this link to open this sample.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'bottom'
	});	
	$('#four_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="files/images/circ_two.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:12px;"><p style="text-align:left;font-size:14px;line-height: 1.3;">The Getting Started dialog box you’re reading now can be<br>displayed at any time using the green button.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'bottom'
	});

});