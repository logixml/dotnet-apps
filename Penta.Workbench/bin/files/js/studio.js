$(document).ready(function() {
	$('#one_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_one.png" width="75" height="75" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:8px;padding-right:40px;"><p style="text-align: justify;font-size:14px;line-height: 1.3;">The context-sensitive Ribbon Menu presents the options and tools you’ll need to create and deploy your Logi application. It also provides design assistance, database and collaboration tools, and wizards that simplify complex tasks.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'bottom'
	});

	$('#two_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_two.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:12px;padding-right:40px;"><p style="text-align: justify;font-size:14px;line-height: 1.3;">The Application panel shows you the source files used in your application. Click to open them in the editor and right-click them to manage them, set their properties, or browse them individually.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'bottom'
	});

	$('#three_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_three.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:6px;padding-right:40px;"><p style="text-align: justify;font-size:14px;line-height: 1.3;">The Workspace panel lets you edit your open source files, each in its own tab. Content-aware editors are provided depending on the file type (report definition, style sheet, XML data, etc.). Report definitions (shown) display the “elements” that make up a report, in a tree arrangement. Tabs at the bottom let you switch between visual design, raw source code, and preview modes.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'bottom'
	});	

	$('#four_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_four.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:10px;padding-right:40px;"><p style="text-align: justify;font-size:14px;line-height: 1.3;">The Element Toolbox panel presents collections of “elements”, the building blocks of a Logi app.  Select an element here to add it your report definition. The collections are dynamic, so you can’t combine the wrong elements.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'bottom'
	});

	$('#five_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_five.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:10px;padding-right:40px;"><p style="text-align: justify;font-size:14px;line-height: 1.3;">The Information panel is your source for quick help. Select an element or an attribute and information about it is shown in this panel. There’s also a link to more information about this item or topic on our DevNet website.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'top'
	});	

	$('#six_tooltip').tooltipster({
		content: $('<div class="row"><div class="col-xs-2 col-sm-2 col-md-2 col-lg-2"><img src="images/circ_six.png" width="75" height="75" class="center-block img-responsive" /></div><div class="col-xs-10 col-sm-10 col-md-10 col-lg-10" style="padding-top:3px;padding-right:40px;"><p style="text-align: justify;font-size:14px;line-height: 1.3;">The Attributes panel allows you to configure element properties, called “Attributes” in Logi-speak. Values can be typed-in or, in some cases, can be selected from option lists or pop-up tools. Attributes names can be double-clicked to open a larger window for easier data entry.</p><p style="text-align:justify;font-size:14px;line-height: 1.3;">The Test Parameters panel lets you insert test values for parameters that are usually passed to the page. This panel is only visible if the report expects parameters and is not displayed until a report has been run or previewed once.</p></div></div>'),
		maxWidth: 675,
		minWidth: 675,
		position: 'top'
	});		
});