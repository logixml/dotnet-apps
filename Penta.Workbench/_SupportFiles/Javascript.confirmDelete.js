function confirmDeleteBookmark (report, collection, bookmarkID, refreshID)
{
	jConfirm('Do you really want to delete this saved report?', 'Warning', function(resp) {
		if (resp)
		{
			NavigateLink2("javascript:rdRemoveBookmark(\'\',\'" + report + "\',\'" + collection + "\',\'\',\'" + bookmarkID + "\',\'\',\'\',\'\',\'1\')");
		    rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=' + refreshID + '&rdReport=' + report,'false','',null,null,null,null,true);
		}
	});
}   

function confirmRemovePanel (panelInstanceId)
{
	jConfirm('Do you really want to remove this panel?', 'Warning', function(resp) {
		if (resp)
		{
		    NavigateLink2("javascript: LogiXML.Dashboard.pageDashboard.rdRemoveDashboardPanel(\'rdDashboardPanel-" + panelInstanceId + "\',\'\',\'\',\'false\',\'\',\'\',null,null)");
		}
	});
}  

function confirmRemoveTab (dashboard, tabId)
{
	jConfirm('Do you really want to remove this tab?', 'Warning', function(resp) {
		if (resp)
		{
            SubmitForm('rdPage.aspx?rdReport=' + dashboard + '&rdDashboardRemoveTab=' + tabId + '&rdRequestForwarding=Form','','false','',null,['Please wait ... ','','']);
		}
	});
}