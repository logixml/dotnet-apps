//Note: This requires a licensing local data layer that brings back...
//LICENSE_SERVER, LICENSE_KEY, LICENSE_DATABASE, AND DATABASE_NANE
function logReportUsage(server, key, user, report, curr_database) {

	$( document ).ready(function() {
		var jsonObj = new Object();
		jsonObj.user = user;
		jsonObj.reportCode = "Workbench";
		jsonObj.reportName = report;
		jsonObj.db_name = curr_database;
		var stringified = JSON.stringify(jsonObj);
		
		$.ajax({
			url: 'https://' + server + '/product-use/json?license_key=' + key + '&app=workbench',
			type: 'post',
			data: stringified,
			processData: false,
			success: function(data){}
		});
	});
}
	
function logPulse(server, key, lic_database, curr_database, user){

	$( document ).ready(function() {
		function getPulse() {
			$.ajax({
				url: 'https://' + server + '/webapp-checkin/concurrent?license_key=' + key + '&app=Workbench&user=' + user + '&username=' + user,
				type: 'get',
				processData: false,
				success: function(data){}
			});
		}
				
		if(lic_database == curr_database) {
			getPulse();
			setInterval(getPulse, 120*1000);
		}
	});
}