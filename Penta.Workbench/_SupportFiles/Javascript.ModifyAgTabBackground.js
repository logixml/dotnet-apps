$(document).ready(evaluateBackground);


function evaluateBackground() {

				
	//If at least one filter, change filter tab's background-image
	if ((document.getElementById("colFilterName_rdAgAnalysisFilter_Row1") !== null) || (document.getElementById("colFilterDisabled_rdAgAnalysisFilter_Row1") !== null)) {
		document.getElementById("colFilter").style.backgroundImage = "url(./_Themes/PentaTheme/rdButtonOutOn.png)";
	}
	
	//If at least one formula, change formula tab's background-image
	if (document.getElementById("colCalcColumnFormula_Row1") !== null) {
		document.getElementById("colCalc").style.backgroundImage = "url(./_Themes/PentaTheme/rdButtonOutOn.png)";
	}
	
	//If at least one chart, change chart tab's background-image
	if (document.getElementById("rdDivAgPanelWrap_rowAnalChart_1") !== null) {
		document.getElementById("colChart").style.backgroundImage = "url(./_Themes/PentaTheme/rdButtonOutOn.png)";
	}
					
	//If at least one crosstab, change crosstab tab's background-image
	if (document.getElementById("rdDivAgPanelWrap_rowAnalCrosstab_1") !== null) {
		document.getElementById("colCrosstab").style.backgroundImage = "url(./_Themes/PentaTheme/rdButtonOutOn.png)";
	}
                
}
