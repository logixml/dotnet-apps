function submitPanelParams(pnlId, pnlInstanceId, getFormInputs)
{
	var frmInputs = getFormInputs();
	$.each(frmInputs, function( i, frmInput ) {
		if (frmInput.type == "Select" || frmInput.type == "Text")
		{
			$("#" + frmInput.id + "_" + pnlInstanceId, window.parent.document).val($("#" + frmInput.id).val());
		}
		else if (frmInput.type == "RadioButton")
		{
			$("#" + frmInput.id + "_" + pnlInstanceId, window.parent.document).val($("input:radio[name=\"" + frmInput.id + "\"]:checked").val());
		}
		else if (frmInput.type == "Checkbox")
		{
			$("#" + frmInput.id + "_" + pnlInstanceId, window.parent.document).val($("input:checkbox[name=\"" + frmInput.id + "\"]:checked").val());
		}
		else if (frmInput.type == "MultiSelect")
		{
			$("#" + frmInput.id + "_" + pnlInstanceId, window.parent.document).val($("#" + frmInput.id).val());
		}
	});
	
	// Hide the popup panel overlay.
	window.parent.ShowElement(null, "popPanel_PanelParamsOverlay_" + pnlInstanceId, "Hide", "");
	
	// Show Reloading message while refreshing panel parameters
	window.parent.ShowElement(null, "waitDiv1_" + pnlInstanceId, "Show", "");
	$("#waitDiv1_" + pnlInstanceId, window.parent.document).siblings().css({"display": "none"});
	
	// Perform an Ajax refresh on the contents of the panel. 
	window.parent.LogiXML.Dashboard.pageDashboard.rdSaveDashboardParams("rdDashboardPanel-" + pnlId + "_" + pnlInstanceId);
			
	// Hide the controls on the panel params pull-down (11.1 only).
	//window.parent.ShowElement(this.id, "rdDashboardEdit-" + pnlId + "_" + pnlInstanceId + ",rdDashboardCancel-" + pnlId + "_" + pnlInstanceId + ",rdDashboard2PanelParams-" + pnlId + "_" + pnlInstanceId + ",rdDashboardRemove-" + pnlId + "_" + pnlInstanceId + ",rdDashboardChangePanel-" + pnlId + "_" + pnlInstanceId, "Toggle", "");
	
	//Modified to keep rdDashboardChangePanel (cog button) from disappearing on Mobile Dashboard
	window.parent.ShowElement(this.id, "rdDashboardEdit-" + pnlId + "_" + pnlInstanceId + ",rdDashboardCancel-" + pnlId + "_" + pnlInstanceId + ",rdDashboard2PanelParams-" + pnlId + "_" + pnlInstanceId + ",rdDashboardRemove-" + pnlId + "_" + pnlInstanceId, "Toggle", "");
	
}

function loadPanelParams(pnlId, pnlInstanceId, getFormInputs)
{
	var frmInputs = getFormInputs();
	$.each(frmInputs, function( i, frmInput ) {
		if (frmInput.type == "Select" || frmInput.type == "Text")
		{
			$("#" + frmInput.id).val($("#" + frmInput.id + "_" + pnlInstanceId, window.parent.document).val());
		}
		else if (frmInput.type == "RadioButton")
		{
			$("input:radio[name=\"" + frmInput.id + "\"][value=\"" + $("#" + frmInput.id + "_" + pnlInstanceId, window.parent.document).val() + "\"]").prop("checked", true);
		}
		else if (frmInput.type == "Checkbox")
		{
			$("input:checkbox[name=\"" + frmInput.id + "\"][value=\"" + $("#" + frmInput.id + "_" + pnlInstanceId, window.parent.document).val() + "\"]").prop("checked", true);
		}
		else if (frmInput.type == "MultiSelect")
		{
			var data=$("#" + frmInput.id + "_" + pnlInstanceId, window.parent.document).val();
			$.each(data.split(","), function(a,b){
				$("#" + frmInput.id + " option[value='"+b+"']").prop("selected", true);
			});
		}
	});
}

function convertDatePickerSingle(dateFilter)
{
    var dateFilterValue = $(dateFilter).val();
	var dateFormat = $("#dateFormat").val();

	if(!dateFilterValue)
	{
		$(dateFilter).val('');
		return;
	}
	
    var begindate = $(dateFilter).val();
	var splitChar = "";
	if (begindate.search("/") == -1) 
		{
		splitChar = "-";
		}
	else 
	{
		splitChar = "/";
	};
	
    var beginparts = begindate.split(splitChar);
						
		if(dateFormat == "dd/MM/yyyy"){
			$(dateFilter).val(beginparts[2] + '/' + beginparts[1] + '/' + beginparts[0]);
		} else {
			$(dateFilter).val(beginparts[2] + '/' + beginparts[0] + '/' + beginparts[1]);
		}
}

function revertDatePickerSingle(dateFilter)
{
    var dateFilterValue = $(dateFilter).val();
	var dateFormat = $("#dateFormat").val();
	
	if(!dateFilterValue)
	{
		$(dateFilter).val('');
		return;
	}
    var begindate = $(dateFilter).val();
	var splitChar = "";
	if (begindate.search("/") == -1) 
		{
		splitChar = "-";
		}
	else 
	{
		splitChar = "/";
	};
	
    var beginparts = begindate.split(splitChar);

		if(dateFormat == "dd/MM/yyyy"){
			$(dateFilter).val(beginparts[2] + '/' + beginparts[1] + '/' + beginparts[0]);
		} else {
			$(dateFilter).val(beginparts[1] + '/' + beginparts[2] + '/' + beginparts[0]);
		}
}

function convertDatePicker(beginFilter, endFilter)
{
   var begindate = $(beginFilter).val();
	var enddate = $(endFilter).val();
	var splitChar = "";
	var dateFormat = $("#dateFormat").val();
	

	if(!begindate)
	{
		$(beginFilter).val('');
	}
	else
	{
		if (begindate.search("/") == -1) 
		{
		splitChar = "-";
		}
		else 
		{
			splitChar = "/";
		};
		
		var beginparts = begindate.split(splitChar);
		
		if(dateFormat == "dd/MM/yyyy"){
			$(beginFilter).val(beginparts[2] + '/' + beginparts[1] + '/' + beginparts[0]);
		} else {
			$(beginFilter).val(beginparts[2] + '/' + beginparts[0] + '/' + beginparts[1]);
		}
		

	}
	
	if(!enddate)
	{
		$(endFilter).val('');
	}
	else
	{
		if (enddate.search("/") == -1) 
		{
		splitChar = "-";
		}
		else 
		{
			splitChar = "/";
		};
		
		var endparts = enddate.split(splitChar);
		
				
		if(dateFormat == "dd/MM/yyyy"){
			$(endFilter).val(endparts[2] + '/' + endparts[1] + '/' + endparts[0]);
		} else {
			$(endFilter).val(endparts[2] + '/' + endparts[0] + '/' + endparts[1]);
		}
	}
	
	 
	
}

function revertDatePicker(beginFilter, endFilter)
{
    var begindate = $(beginFilter).val();
	var enddate = $(endFilter).val();
	var splitChar = "";
	var dateFormat = $("#dateFormat").val();

	if(!begindate)
	{
		$(beginFilter).val('');
	}
	else
	{
		if (begindate.search("/") == -1) 
		{
		splitChar = "-";
		}
		else 
		{
			splitChar = "/";
		};
		
		var beginparts = begindate.split(splitChar);
		
		if(dateFormat == "dd/MM/yyyy"){
			$(beginFilter).val(beginparts[2] + '/' + beginparts[1] + '/' + beginparts[0]);
		} else {
			$(beginFilter).val(beginparts[1] + '/' + beginparts[2] + '/' + beginparts[0]);
		}

	}
	
	if(!enddate)
	{
		$(endFilter).val('');
	}
	else
	{
		if (enddate.search("/") == -1) 
		{
		splitChar = "-";
		}
		else 
		{
			splitChar = "/";
		};
		
		var endparts = enddate.split(splitChar);
		
		if(dateFormat == "dd/MM/yyyy"){
			$(endFilter).val(endparts[2] + '/' + endparts[1] + '/' + endparts[0]);
		} else {
			$(endFilter).val(endparts[1] + '/' + endparts[2] + '/' + endparts[0]);
		}
	}
	


}