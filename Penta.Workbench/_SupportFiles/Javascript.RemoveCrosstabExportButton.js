$(document).ready(evaluateBackground);


function evaluateBackground() {
	
	//If at least one crosstab, remove standard export button
	if (document.querySelector('[id^="rdDivAgPanelWrap_rowAnalCrosstab_"]') != null) {
	    var elements = document.querySelectorAll('[id^="colTableExportMenu"]');
		for (var i = 0, len = elements.length; i < len; i++) {
			var element = elements[i];
				if (element.parentNode.parentNode.parentNode.attributes.id.value.indexOf("rowPanelHeading_AnalCrosstab_") > -1) {
					element.parentNode.removeChild(element);
				}	
	    }

	}	
                
}
