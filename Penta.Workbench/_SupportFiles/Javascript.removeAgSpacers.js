$(document).ready(hideSpacers);

function hideSpacers() {
  
  if (!document.body.contains(document.getElementById('colChart'))) {
	$("#colSpacerChart").remove();
  }
  
  if (!document.body.contains(document.getElementById('colCrosstab'))) {
    $("#colSpacerCrosstab").remove();
  }
}