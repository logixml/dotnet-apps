$(document).ready(function() {

	if (document.body.contains(document.getElementById("ppoRemovethisTab_rdPopupOptionItem"))) 
	{
		if ($("#DashboardIdentifier").val() == "PENTAWorkbench") 
		{
			document.getElementById("ppoRemovethisTab_rdPopupOptionItem").setAttribute("href", "javascript:confirmRemoveTab('Default', $('#ActiveTabIdentifier').val());");
		}
		else
		{
	        document.getElementById("ppoRemovethisTab_rdPopupOptionItem").setAttribute("href", "javascript:confirmRemoveTab('ProjectManagement.JobWorkbench', $('#ActiveTabIdentifier').val());");
		}
		
    } 

});