function clickAccordion(optionNum, accSize, accReport) {

	var reportNum = accReport + optionNum;
	
	if($('#divTitle' + reportNum).hasClass( "accordion-selected" )) {
		$('#divTitle' + reportNum).removeClass( "accordion-selected" );
		$('#divCaret' + reportNum).removeClass( "rot90" );
		ShowElement('','divContent' + reportNum,'Hide','');
	} else {
		$('#divTitle' + reportNum).addClass( "accordion-selected" );
		$('#divCaret' + reportNum).addClass( "rot90" );
		ShowElement('','divContent' + reportNum,'Show','');
	}
}

function clickAccordions(optionNum, accSize) {

	if($('#divTitle' + optionNum).hasClass( "accordion-selected" )) {
		$('#divTitle' + optionNum).removeClass( "accordion-selected" );
		$('#divCaret' + optionNum).removeClass( "rot90" );
		ShowElement('','divContent' + optionNum,'Hide','');
	} else {
		var i;
		for (i = 1; i < accSize + 1; i++) {
			if(i == optionNum) {
				$('#divTitle' + i).addClass( "accordion-selected" );
				$('#divCaret' + i).addClass( "rot90" );
				ShowElement('','divContent' + i,'Show','');
			} else {
				$('#divTitle' + i).removeClass( "accordion-selected" );
				$('#divCaret' + i).removeClass( "rot90" );
				ShowElement('','divContent' + i,'Hide','');
			}
		}
	}
}