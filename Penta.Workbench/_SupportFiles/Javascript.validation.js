function validateNumRowsDisplayed(eleInput, min, max)
{
	if ((Number(eleInput.value) < min) || (Number(eleInput.value) > max))
	{
		throw('Please enter a number <= ' + max)
	}
	
}

function validateDayOfWeek(eleDate,eleDay)
{
	var inputDate = new Date(eleDate.value);
	if (inputDate.getDay() != eleDay.value - 1)
	{
		jAlert('Please enter a date that matches the Default Week Ending Day', 'Warning');
		return false;
	}
	else
	{
	    return true;
	}
}

function isNumberKey(event)
{
 var charCode = (event.which) ? event.which : event.keyCode
 if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

 return true;
}
   