<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:param name="SessionEmpId"></xsl:param>
 <xsl:param name="SessionUserName"></xsl:param>
 <xsl:output method="xml" indent="yes"/>
      
    <xsl:template match="@* | node()">
     <xsl:copy>
       <xsl:apply-templates select="@* | node()"/>
     </xsl:copy>     
    </xsl:template>

    <xsl:template match="//@*[contains(., '-1') and name() != 'rdAgLoadSaved']">
        <xsl:choose>
			<xsl:when test="name() = 'projectControllerId' or name() = 'projectManagerId' or name() = 'salesperson' or name() = 'salespersonId'
			                or name() = 'projAdminEmpId' or name() = 'projectAdministrator' or name() = 'projectManagerId' or name() = 'requester' 
							or name() = 'technician' or name() = 'foreman' or name() = 'OUManager' or name() = 'primaryApprover' "> 
				<xsl:attribute name="{name()}">
    				<xsl:call-template name="string-replace-all">
      					<xsl:with-param name="text" select="." />
      					<xsl:with-param name="replace" select="-1" />
      					<xsl:with-param name="by" select="$SessionEmpId" />
    				</xsl:call-template>					
				</xsl:attribute>
			</xsl:when>
			
			<xsl:when test="name() = 'primaryAdminId' or name() = 'passed_primaryAdminId'">
				<xsl:attribute name="{name()}">
    				<xsl:call-template name="string-replace-all">
      					<xsl:with-param name="text" select="." />
      					<xsl:with-param name="replace" select="-1" />
      					<xsl:with-param name="by" select="$SessionUserName" />
    				</xsl:call-template>
				</xsl:attribute>					
			</xsl:when>
			
			<xsl:otherwise>
				<xsl:attribute name="{name()}">
					<xsl:value-of select="."></xsl:value-of>
				</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>				
	</xsl:template>
	
	
	<xsl:template match="//@*[. = '']">
        <xsl:choose>
			<xsl:when test="name() = 'calcForcasts' or name() = 'calculateForecasts' or name() = 'summarizeToParentJob' or name() = 'isDistributed'
			   or name() = 'isOver' or name() = 'isUnder' or name() = 'showOverdue' or name() = 'showUpcoming' or name() = 'excludeClosingEntries'
			   or name() = 'excludeNoFYActivity' or name() = 'includeWO'"> 
				<xsl:attribute name="{name()}">
					<xsl:text>N</xsl:text>
				</xsl:attribute>
        	</xsl:when>
			
			<xsl:otherwise>
				<xsl:attribute name="{name()}">
					<xsl:text>All</xsl:text>
				</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>		
	</xsl:template>
	
	
	
	<xsl:template match="//@*[name() = 'CCAndOr']">
        <xsl:choose>
			<xsl:when test=". = 'A'"> 
				<xsl:attribute name="{name()}">
					<xsl:text>And</xsl:text>
				</xsl:attribute>
        	</xsl:when>
	
			<xsl:otherwise>
				<xsl:attribute name="{name()}">
					<xsl:text>Or</xsl:text>
				</xsl:attribute>
			</xsl:otherwise>
		</xsl:choose>		
	</xsl:template>
	
    <xsl:template match="//@*[. = 'all']">
        <xsl:attribute name="{name()}">
            <xsl:text>All</xsl:text>
        </xsl:attribute>
    </xsl:template> 
	
	<xsl:template match="//@*[name() = 'periodEndDate']">
        <xsl:attribute name="{name()}">
            <xsl:value-of select="substring-before(., 'T')"></xsl:value-of>
		</xsl:attribute>
    </xsl:template> 
	
	<xsl:template match="//@*[name() = 'safetyIndicator2']">
		<xsl:choose>
			<xsl:when test=". != ''">
				<xsl:attribute name="safetyIndicator">
					<xsl:value-of select="."></xsl:value-of>
				</xsl:attribute>
			</xsl:when>
		</xsl:choose>
    </xsl:template> 
	
	<xsl:template match="//@*[name() = 'trainingClassNum']">
		<xsl:choose>
			<xsl:when test=". != 'all'">
				<xsl:attribute name="trainingClass">
					<xsl:value-of select="."></xsl:value-of>
				</xsl:attribute>
			</xsl:when>
		</xsl:choose>
    </xsl:template> 
	
    <xsl:template match="//@*[. = 'DISTR_DATE']">
        <xsl:attribute name="{name()}">
            <xsl:text>Distribution Date</xsl:text>
        </xsl:attribute>
    </xsl:template> 
	
	<xsl:template match="//@*[. = 'WORK_DATE']">
        <xsl:attribute name="{name()}">
            <xsl:text>Work Date</xsl:text>
        </xsl:attribute>
    </xsl:template> 
	
	
	<xsl:template match="//@*[name() = 'beginDate' or name() = 'endDate' or name() = 'EndDate' or
	                          name() = 'BeginDate' or name() = 'periodEndDate' or name() = 'workDate' or
						      name() = 'weekEndDate' or name() = 'endDateWeek']">
	    <xsl:choose>
			<xsl:when test=". = ''">
				<xsl:attribute name="{name()}">
					<xsl:text>None</xsl:text>
				</xsl:attribute>
			</xsl:when>
			
			<xsl:when test=". != ''">
				<xsl:attribute name="{name()}">
					<xsl:value-of select="concat(substring(.,6,2),'/',substring(.,9,2),'/',substring(.,1,4))"/>
				</xsl:attribute>
			</xsl:when>			
		</xsl:choose>

	</xsl:template>
	
 <xsl:template name="string-replace-all">
    <xsl:param name="text" />
    <xsl:param name="replace" />
    <xsl:param name="by" />

    <xsl:choose>
 
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)" />
        <xsl:value-of select="$by" />
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text"
          select="substring-after($text,$replace)" />
          <xsl:with-param name="replace" select="$replace" />
          <xsl:with-param name="by" select="$by" />
        </xsl:call-template>
	  </xsl:when>
	  
      <xsl:otherwise>
	    <xsl:choose>
	    	<xsl:when test="not($text)">
				<xsl:value-of select="' '" />
			</xsl:when>
        
			<xsl:otherwise>
				<xsl:value-of select="$text" />
			</xsl:otherwise>
		</xsl:choose>
      </xsl:otherwise>

    </xsl:choose>
  </xsl:template>


</xsl:stylesheet>
