var numAutocompletes = 0;
var maxItemsShown = 3;


function addAutocomplete(objElement,ajaxReport, KeyFieldName, ValFieldName, pnlMenuID)
{
      var elementID = objElement.getAttribute("id");
      var parentElement = document.getElementById(elementID);
      
      if(parentElement.getAttribute("hasAutocomplete") == "Y")
      {
            //function is allready attached, jump out
            return;
      }
      else 
      {
            //
			// find the next text box after the autocomplete (+1) this will contain our key's when the user selects them
			// find the next text box after that (autocomplete +2) this will contain the descriptions when the user selects them
			//
		    var destElementID = $(":input").get($(":input").index(parentElement)+1).id;
			var storeNameElementID = $(":input").get($(":input").index(parentElement)+2).id;

            parentElement.setAttribute("id",elementID + "_" + numAutocompletes);
            parentElement.setAttribute("ID",elementID + "_" + numAutocompletes);
            parentElement.setAttribute("destElementID",destElementID);
			parentElement.setAttribute("storeNameElementID",storeNameElementID);
            parentElement.setAttribute("hasAutocomplete","Y");
            parentElement.setAttribute("numItems","0");
            elementID = elementID + "_" + numAutocompletes;
      }
      
	  //
	  // clear button is a link with a class that makes it look like 
	  // a button
	  // When clicked it clears the msg div and the key ID
	  //
	  var clearBTN = document.createElement("a");
	  clearBTN.innerHTML ="<span class='ThemeLinkButtonSmall'>Clear</span>";
	  clearBTN.href= "javascript:clearAutoComplete('hdnAutoCompMsg_" + numAutocompletes + "', '" + destElementID + "', '" + storeNameElementID + "', '" + elementID + "');void(0);";
	  
	  parentElement.parentNode.appendChild(clearBTN);
	  
	  //
	  // Holds the message/values the user has selected
	  //
      var elementMsg = document.createElement("div");
      elementMsg.id = "hdnAutoCompMsg_" + numAutocompletes;
      elementMsg.innerHTML = "All";
      
      parentElement.parentNode.appendChild(elementMsg);
      
	  //
	  // Prefill values if there is already something selected from earlier
	  //	  
	  var numItems = parseInt(parentElement.getAttribute("numItems"));
      var elementHiddenIds = document.getElementById(parentElement.getAttribute("destElementID"));
	  var elementHiddenNames = document.getElementById(parentElement.getAttribute("storeNameElementID"));
	  var selectionCnt = 0;
						
		// This is here because the names and number of items attribute isn't always properly cleared out by the clear button.
		// The real solution should be to figure out why the clear isn't working correctly.
		if (elementHiddenIds.value == '')
		{
			parentElement.setAttribute("numItems",0);
			elementHiddenNames.value = '';
		}
        
		// figure out how many items were previously selected
		if(elementHiddenIds.value != '')
        {
			var substrings = elementHiddenIds.value.split(/,/g);
			selectionCnt = substrings.length;
		}

		// display the previously selected items and update the count
        if(numItems == 0 && elementHiddenIds.value != '')
        {
			if(selectionCnt > maxItemsShown)
            {
                elementMsg.innerHTML = selectionCnt + " selected";
            } else {
                elementMsg.innerHTML = elementHiddenNames.value;
			}
			parentElement.setAttribute("numItems",selectionCnt);
        }
						

		//
		// jQuery Autocomplete functionality
		//
      $("#" + elementID).autocomplete({
            minLength: 2,                       
            delimiter: /(,|;)\s*/, // regex or character 
        source:  function(request, response) 
        {
            var matches = null;
            var data = null;
            if(data != null )
            {
                        matches = $.map(data, function(tag) 
						{                                         
                                    if ( tag.value.match(new RegExp("^" +request.term, "i")))
									{
                                    	return tag;
                                    }
                  		});   
                        response(matches);
            }

            if (matches == null || matches.length == 0)
            {
                $.ajax({
					url: 'rdPage.aspx',
					data: {rdReport:"" + ajaxReport + "", rdReportFormat:"DataLayerXml", q:request.term, menuID:pnlMenuID },
					dataType: 'xml',
					success: function( xmlResponse ) 
					{
						data = $( "dtSearchResults", xmlResponse ).map(
							function() {
								return {
									value: $(this).attr(ValFieldName),
									id: $(this).attr(KeyFieldName)
								};
							});
														
						response(data.slice(0,10));
					}
                })
            }
        },
                  close: function(event, ui)
                  { 
                        parentElement.value = "";
                  },                                          
                  select: function(event, ui)
                  { 
				  		//
						// Once selected itesm are added to message box
						// and hidden key value text box
						// 
						// Only a certain number of items are shown before it 
						// just shows the count
						//
                        var numItems = parseInt(parentElement.getAttribute("numItems"));
                        var elementHidden = document.getElementById(parentElement.getAttribute("destElementID"));
						var elementHiddenNames = document.getElementById(parentElement.getAttribute("storeNameElementID"));
                        

						// This is here because the names and number of items attribute isn't always properly cleared out by the clear button.
						// The real solution should be to figure out why the clear isn't working correctly.
						if (elementHidden.value == '')
						{
							numItems = 0;
							elementHiddenNames.value = '';
						}
						
						
                        if(numItems == 0)
                        {
                              elementMsg.innerHTML = ui.item.value + "";
                              elementHidden.value = ui.item.id + "";
							  elementHiddenNames.value = ui.item.value + "";
                        
							  parentElement.setAttribute("numItems",numItems+1);
						}

                        if(numItems > 0 && numItems < maxItemsShown && elementHidden.value.indexOf(ui.item.id) == -1)
               	         {
                              elementMsg.innerHTML = elementMsg.innerHTML + "," + ui.item.value;
                              elementHidden.value = elementHidden.value + "," + ui.item.id + "";
							  elementHiddenNames.value = elementHiddenNames.value + "," + ui.item.value;
                        
							  parentElement.setAttribute("numItems",numItems+1);
						}
                        
                        if(numItems >= maxItemsShown && elementHidden.value.indexOf(ui.item.id) == -1)
                        {
                              elementMsg.innerHTML = (numItems+1) + " selected";
							  elementHidden.value = elementHidden.value + "," + ui.item.id + "";
							  elementHiddenNames.value = elementHiddenNames.value + "," + ui.item.value;
							  
							  parentElement.setAttribute("numItems",numItems+1);
                        }
                        
                        
                        
                        
                        
                  }
            });

            numAutocompletes++;
}

function clearAutoComplete (valuesDivId, hiddenKeyId, hiddenNameId, parentId)
{
    document.getElementById(valuesDivId).innerHTML = "All";
	document.getElementById(hiddenKeyId).value = "";
	document.getElementById(parentId).setAttribute("numItems",0);
	document.getElementById(hiddenNameId).value = "";
}