function filterAddPanelsList ()
{	
	var str = $('#rdInputAddPanelFind').val(); 
	var spart = str.split(" ");	
	for ( var i = 0; i < spart.length; i++ ) 
	{
		var j = spart[i].charAt(0).toUpperCase();
		spart[i] = j + spart[i].substr(1).toLowerCase();
	}
    $('#rdInputAddPanelFind').val(spart.join(" "));
					
	switch ($('#rdInputAddPanelFind').val()) 
	{
		case 'All': $('#rdInputAddPanelFind').val(''); break;
		case 'Accounting': $('#rdInputAddPanelFind').val('AC:'); break;
		case 'Accounts Payable': $('#rdInputAddPanelFind').val('AP:'); break;
		case 'Accounts Receivable': $('#rdInputAddPanelFind').val('AR:'); break;
		case 'eTime': $('#rdInputAddPanelFind').val('eTime:'); break;	
		case 'General': $('#rdInputAddPanelFind').val('GEN:'); break;
		case 'Human Resources': $('#rdInputAddPanelFind').val('HR:'); break;
		case 'Invoicing': $('#rdInputAddPanelFind').val('IN:'); break;
		case 'My Personal Reports': $('#rdInputAddPanelFind').val('My Personal Reports'); break;
		case 'Payroll': $('#rdInputAddPanelFind').val('PR:'); break;
		case 'Project Management': $('#rdInputAddPanelFind').val('PM:'); break;
		case 'Purchasing': $('#rdInputAddPanelFind').val('PO:'); break;
		case 'Reporting Center': $('#rdInputAddPanelFind').val('Reporting Center'); break;
		case 'Service Management': $('#rdInputAddPanelFind').val('SM:'); break;
		case 'System': $('#rdInputAddPanelFind').val('SY:'); break;		
	}

	rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID=dtPanelList&rdRefreshDashboard=True&rdRefreshAddPanelsList=True','false','',false,null,null,false,true);
    setTimeout(function(){$('#rdAddPanelsList').scrollTop(0);},1500);
}