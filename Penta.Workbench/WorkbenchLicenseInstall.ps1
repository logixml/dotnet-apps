$currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
if (-Not($currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator))) {
  Write-Host "You need to run the shell as Administrator"
  exit
} 

$licFile = "lgx120102.lic"
$url = "http://penta-workbench.s3.amazonaws.com/license/$licFile"

$progamFilesDirectory = [Environment]::GetFolderPath([System.Environment+SpecialFolder]::CommonApplicationData)
$licInstallDirectory = Join-Path -Path $progamFilesDirectory -ChildPath "Penta Technologies, Inc" | Join-Path -ChildPath "Penta Workbench" 

if (!(Test-Path -Path $licInstallDirectory)) {
	Write-Host "Cannot find a PENTA Workbench installation.  Exiting"
	#exit
}
$folders = Get-ChildItem -Path $licInstallDirectory  | ?{ $_.PSIsContainer }

$output = "$PSScriptRoot\$licFile"
$start_time = Get-Date

#Invoke-WebRequest -Uri $url -OutFile $output
(New-Object System.Net.WebClient).DownloadFile($url, $output)


Write-Output "`nDownloading $licFile..."

if (-Not(Test-Path $output)) {
  Write-Error "$licFile failed to download."
  exit
}

Write-Output "Downloaded $licFile. Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"

foreach ($folder in $folders){

  $folderName = $folder.Name	
  $newDest = $licInstallDirectory |Join-Path -ChildPath $folderName
  Write-Host "Removing old .lic files from $folderName"
  del $newDest\*.lic 
  Write-Host "Copying new $licFile to $folderName"
  Copy-Item $output -Destination $newDest -Force
  
   if(-Not(Test-Path $newDest | Join-Path -ChildPath $licFile)) {
    Write-Warning "File failed to copy"
  } 
}

del $output

$skele = @"
      .-.
     (o.o)
      |=|  - Thanks for updating the licenses!
     __|__
   //.=|=.\\
  // .=|=. \\
  \\ .=|=. //
   \\(_=_)//
    (:| |:)
     || ||
     () ()
     || ||
     || ||
    ==' '==
"@

Write-Output $skele