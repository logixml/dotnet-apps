transferBookmarks.ps1 README
----------------------------

1. Make a backup of the SavedBookmarks Folder.
2. Make sure transferBookmarks.ps1 and users.txt (or whatever you are calling your user batch file) are both in the workbenches SavedBookmarks Folder
3. Make sure your users batch file has 1 line per user with the first line being just "users"

Example of what would be in a user.txt batch file...

users
ABC
DEF
GHI

NOTE - Everything is CASE SENSITIVE (on purpose).

To run the script, open Powershell as Administrator:
1. cd to your workbench's SavedBookmarks folder
ex. cd C:\programdata\Penta Technologies, Inc\Penta Workbench\PentaWorkbenchTEST\SavedBookmarks
2. to run the script type .\transferBookmarks.ps1, then pass in a -to parameter and a -from parameter, with an optional -batch parameter if using a batch users file.

Examples
User to User:
Transfer bookmarks from user ABC to DEF
".\transferBookmarks.ps1 -from ABC -to DEF"

User to Role:
Transfer bookmarks from user ABC to the role PMGR
".\transferBookmarks.ps1 -from ABC -to PMGR"

Role to Multiple Users:
Batch Transfering bookmarks from Role PMGR to the list of users in users.txt
".\transferBookmarks.ps1 -from PMGR -to users.txt -batch"

NOTE - If role has a space or special character in the name, such as "Project Manager", be sure to put " " around the role name
".\transferBookmarks.ps1 -from "Project Manager" -to users.txt -batch"


