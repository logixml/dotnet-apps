﻿param(
    [string] $from,
    [string] $to,
    [switch] $batch
)

$currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
if (-Not($currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator))) {
  Write-Host "Error: You need to run the shell as Administrator"
  exit
} 

$Src = (Get-Item -Path ".\").FullName

#logging function
Function Write-Log
{
   Param ([string]$logstring)

   $Logfile = "$src\$(gc env:computername).log"
   $content = [DateTime]::Now.ToString("MM/dd/yyyy-HH:mm:ss") + ": " + $logstring
   Add-content $Logfile -value $content
   Write-Host $content
}

if (!$from -Or !$to) {
    Write-Log "Error: -from and -to parameters must be set."
    exit
}

function backupOldBookmarks {
    Param ([string]$toUser, [string]$fromUser)

    #Check that the files exist
    if (-not (Test-Path $Src\$toUser.xml)){
        return Write-Log "$Src\$toUser.xml not found, nothing to backup"
    } elseif(-not (Test-Path $Src\$fromUser.xml)) {
        Write-Log "$Src\$fromUser.xml not found, cancelling backup and transfer"
        return "error"
    }

    #Set backup folder path/create backup folder
    $Dst = "$Src\Backup_$toUser"

    if (-not (Test-Path $Dst)) { 
        New-Item -ItemType directory -Path $Dst
    } else {
        Remove-Item $Dst -recurse
        New-Item -ItemType directory -Path $Dst
    }

    #transfer files to backup folder
    Copy-Item "$toUser.xml" -Destination $Dst
    $temp = $toUser + "_*.xml"
    Get-ChildItem $Src -Filter $temp | Foreach-Object `
    {
        Copy-Item $_ -Destination $Dst
    }

    #if backup folder exists, delete files
    if (Test-Path $Dst) { 
        Remove-Item $Src\$temp
        Remove-Item $Src\$toUser.xml 
    }

    #archive backup folder
    $archive = "$Dst" + [DateTime]::Now.ToString("MM_dd_yyyy-HH_mm_ss") + ".zip"
    Add-Type -assembly "system.io.compression.filesystem"

    [io.compression.zipfile]::CreateFromDirectory($Dst, $archive)

    #if archive exists, delete backup folder
    if (Test-Path $archive) { 
        Remove-Item $Dst -recurse
    }

    Write-Log "Backup complete"
}

function transferNewBookmarks {
    Param ([string]$toUser, [string]$fromUser)

    #check that the file exists
    if (-not (Test-Path $Src\$fromUser.xml)){
        return Write-Log "$Src\$fromUser.xml not found, can not transfer files"
    }

    #copy fromUser.xml to toUser.xml
    $toXML = "$Src\$toUser.xml"
    Copy-Item $Src\$fromUser.xml -Destination $toXML
    #copy all associated files to toUser
    $temp = $fromUser + "_*.xml"
    Get-ChildItem $Src -Filter $temp | Foreach-Object `
        {
            $newFile = $_ -replace "$fromUser","$toUser"
            Copy-Item $Src\$_ -Destination $Src\$newFile 
        }

    #Edit xml nodes to reference new toUser files
    [xml]$XmlDocument = Get-Content -Path $toXML 

    ForEach ($node in $XmlDocument.rdBookmarks.Bookmark)
    {
        $node.ExtraFile = $node.ExtraFile -replace "$fromUser","$toUser"
        $node.RequestParameters.rdAgLoadSaved = $node.RequestParameters.rdAgLoadSaved -replace "$fromUser","$toUser"
    }

    #save toUser.xml
    $XmlDocument.save($toXML)
    Write-Log "Transfer complete"
}

if($batch) {
    Write-Log "-------------------------------------"
    Write-Log "Begin Batch Copy"

    if (-not (Test-Path $Src\$to)){
        return Write-Log "$Src\$to not found, can not transfer files"
    }

    $Users = Import-CSV "$Src\$to"

    ForEach ($Line In $Users)
    {
        if(!$Line.users) {
            Write-Log "Error: user file not setup correcly"
            exit
        }
        
        $user = $Line.users
        Write-Log "Copying bookmarks from $from to $user"
        if ((backupOldBookmarks -to $user -from $from) -ne "error"){
            transferNewBookmarks -to $user -from $from
        }
    }

} else {
    Write-Log "-------------------------------------"
    Write-Log "Copying bookmarks from $from to $to"
    if ((backupOldBookmarks -to $to -from $from) -ne "error"){
        transferNewBookmarks -to $to -from $from
    }
}