﻿$currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
if (-Not($currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator))) {
  Write-Host "Error: You need to run the shell as Administrator"
  exit
} 

$Src = (Get-Item -Path ".\").FullName

#logging function
function Write-Log
{
   Param ([string]$logstring)

   $Logfile = "$src\$(gc env:computername).log"
   $content = [DateTime]::Now.ToString("MM/dd/yyyy-HH:mm:ss") + ": " + $logstring
   Add-content $Logfile -value $content
   Write-Host $content
}

function Get-PSVersion {
    if (test-path variable:psversiontable) {$psversiontable.psversion} else {[version]"1.0.0.0"}
}

function backupOldBookmarks {

    #Set backup folder path/create backup folder
    $Dst = "$Src\ColumnFixBackup_" + [DateTime]::Now.ToString("MM_dd_yyyy-HH_mm_ss")

    if (-not (Test-Path $Dst)) { 
        New-Item -ItemType directory -Path $Dst
        Write-Log "$Dst folder created"
    }

    #transfer files to backup folder
    $folder = Get-ChildItem -path $Src -Filter *.xml

    foreach ($bookmark in $folder)
    {
        Copy-Item $bookmark -Destination $Dst
    }

    #archive backup folder
    $version = Get-PSVersion
    [version]$4 = "4.0"
    $archive = "$Dst" + ".zip"

    if($version -ge $4) {
    Add-Type -assembly "system.io.compression.filesystem"

    [io.compression.zipfile]::CreateFromDirectory($Dst, $archive)
    }

    #if archive exists, delete backup folder
    if (Test-Path $archive) { 
        Remove-Item $Dst -recurse
    }

    Write-Log "Backup complete"
}

function adjustXml {

    $temp1 = $Src +'\*'
    $folder = Get-ChildItem -Path $temp1 -Include "*.xml" -Exclude "*_*.xml"

    #Loop through each USER.xml
    ForEach($userFile in $folder) {

        Write-Log "SEARCHING: $userFile..."
        [xml]$XmlDocument = Get-Content -Path $userFile 

        #Loop through saved reports in USER.xml
        ForEach ($node in $XmlDocument.rdBookmarks.Bookmark)
        {
            $report = $node.Report
            $file = $node.RequestParameters.rdAgLoadSaved
            $description = $node.Description
            $editFile = $Src + "\" + $file

            if (-not (Test-Path $editFile)){
                Write-Log "WARNING: $editFile does not exist, skipping..."
            }
            else {
                #Replace correct columns based on report
                switch($report)
                {
                    {"AccountsPayable.Reports.AgingPeriodAnalysis", "AccountsPayable.Reports.InvoicesOnHoldAnalysis", "AccountsPayable.Reports.InvoicesOnHoldDetailsAnalysis" -contains $report}
                    {
                        replaceString -edit $editFile -old "ccRetPayablesBase" -new "RETPAYABLESBASE"
                        replaceString -edit $editFile -old "ccRetPayables" -new "RETPAYABLES"
                        replaceString -edit $editFile -old "ccPayablesBase" -new "PAYABLESBASE"
                        replaceString -edit $editFile -old "ccPayables" -new "PAYABLES"
                        replaceString -edit $editFile -old "ccTotalPayablesBase" -new "TOTALPAYABLESBASE"
                        replaceString -edit $editFile -old "ccTotalPayables" -new "TOTALPAYABLES"

                        Write-Log "FIXED: $report ($description)";break
                    }
                    "AccountsReceivable.Reports.AgingPeriodAnalysis" 
                    {
                        replaceString -edit $editFile -old "ccReceivablesBase" -new "RECEIVABLESBASE"
                        replaceString -edit $editFile -old "ccReceivables" -new "RECEIVABLES"
                        replaceString -edit $editFile -old "ccRetReceivablesBase" -new "RETRECEIVABLESBASE"
                        replaceString -edit $editFile -old "ccRetReceivables" -new "RETRECEIVABLES"
                        replaceString -edit $editFile -old "ccTotalReceivablesBase" -new "TOTALRECEIVABLESBASE" 
                        replaceString -edit $editFile -old "ccTotalReceivables" -new "TOTALRECEIVABLES"
                        Write-Log "FIXED: $report ($description)"; break
                    }
                    "Payroll.Reports.LaborOverheadDistrReport" 
                    {
                        replaceString -edit $editFile -old "colChargeRate" -new "CHARGERATE"
                        replaceString -edit $editFile -old "colTotalCharge" -new "TOTALCHARGE"
                        replaceString -edit $editFile -old "colTotalHours" -new "TOTALHOURS"
                        Write-Log "FIXED: $report ($description)"; break
                    }
                    "Payroll.Reports.ManHoursWorkedAnalysis" 
                    {
                        replaceString -edit $editFile -old "ccTOTAL_HOURS" -new "TOTAL_HOURS"
                        Write-Log "FIXED: $report ($description)"; break
                    }
                    "ProjectManagement.Reports.CashDisbursementAnalysis" 
                    {
                        if($node.RequestParameters.projectManagerId -eq "") 
                        {
                            $node.RequestParameters.projectManagerId = "all"
                        }
                        replaceString -edit $editFile -old "ccGrossPaid" -new "GROSSPAID"
                        Write-Log "FIXED: $report ($description)"; break
                    }
                    "ProjectManagement.Reports.CashReceivedAnalysis" 
                    {
                        if($node.RequestParameters.projectManagerId -eq "") 
                        {
                            $node.RequestParameters.projectManagerId = "all"
                        }
                        replaceString -edit $editFile -old "ccGrossCash" -new "GROSSCASH"
                        Write-Log "FIXED: $report ($description)"; break
                    }
                    "ProjectManagement.Reports.CashCostToDateAnalysis" 
                    {
                        if($node.RequestParameters.projectManagerId -eq "") 
                        {
                            $node.RequestParameters.projectManagerId = "all"
                        }
                        Write-Log "FIXED: $report ($description)"; break
                    }
                    "ProjectManagement.Reports.CashPositionAnalysis" 
                    {
                        if($node.RequestParameters.projectManagerId -eq "") 
                        {
                            $node.RequestParameters.projectManagerId = "all"
                        }
                        Write-Log "FIXED: $report ($description)"; break
                    }
                    "ProjectManagement.Reports.ChangeRequestAnalysis" 
                    {
                        replaceString -edit $editFile -old "ccMarginPercent" -new "MARGINPERCENT"
                        replaceString -edit $editFile -old "ccMargin" -new "MARGIN"
                        Write-Log "FIXED: $report ($description)"; break
                    }
                    "ProjectManagement.Reports.JobCostAnalysis" 
                    {
                        replaceString -edit $editFile -old "ccProjectedVarianceUnits" -new "PROJECTED_VARIANCE_UNITS"
                        replaceString -edit $editFile -old "ccProjectedVarianceHours" -new "PROJECTED_VARIANCE_HOURS"
                        replaceString -edit $editFile -old "ccProjectedVarianceCost" -new "PROJECTED_VARIANCE_COST" 
                        Write-Log "FIXED: $report ($description)"; break
                    }
                    "ProjectManagement.Reports.JobCostByCCSegmentSummaryAnalysis" 
                    {
                        replaceString -edit $editFile -old "ccProjectedVarianceUnits" -new "PROJ_VAR_UNITS"
                        replaceString -edit $editFile -old "ccProjectedVarianceHours" -new "PROJ_VAR_HOURS"
                        replaceString -edit $editFile -old "ccProjectedVarianceCost" -new "PROJ_VAR_COST" 
                        Write-Log "FIXED: $report ($description)"; break
                    }
                    "ProjectManagement.Reports.LaborCostAnalysis" 
                    {
                        replaceString -edit $editFile -old "ccActualAverageRate" -new "ACTUAL_AVG_RATE"
                        replaceString -edit $editFile -old "ccForcastToCompleteRate" -new "FORECAST_TO_COMP_RATE"
                        replaceString -edit $editFile -old "PROJECTED_VARIANCE_HOURS" -new "PROJ_VAR_HOURS" 
                        replaceString -edit $editFile -old "ccPROJECTED_VARIANCE_COST" -new "PROJ_VAR_COST" 
                        Write-Log "FIXED: $report ($description)"; break
                    }
                    "ProjectManagement.Reports.LaborHoursAnalysis" 
                    {
                        replaceString -edit $editFile -old "ccActualVariance" -new "ACTUAL_VARIANCE"
                        replaceString -edit $editFile -old "ccActualProductivity" -new "ACTUAL_PRODUCTIVITY"
                        replaceString -edit $editFile -old "ccForcastProductivity" -new "FORECAST_PRODUCTIVITY" 
                        replaceString -edit $editFile -old "ccProjectedVariance" -new "PROJ_VARIANCE" 
                        Write-Log "FIXED: $report ($description)"; break
                    }
                    "ProjectManagement.Reports.ProjectMarginAnalysis" 
                    {
                        replaceString -edit $editFile -old "GrossMarginActualPercent" -new "GM_ACTUAL_PCT"
                        replaceString -edit $editFile -old "GrossMarginActual" -new "GM_ACTUAL"
                        replaceString -edit $editFile -old "GrossMarginBilledPercent" -new "GM_BILLED_PCT" 
                        replaceString -edit $editFile -old "GrossMarginBilled" -new "GM_BILLED" 
                        replaceString -edit $editFile -old "GrossMarginCurrentPercent" -new "GM_CURRENT_PCT"
                        replaceString -edit $editFile -old "GrossMarginCurrent" -new "GM_CURRENT"
                        replaceString -edit $editFile -old "GrossMarginOriginalPercent" -new "GM_ORIG_PCT" 
                        replaceString -edit $editFile -old "GrossMarginOriginal" -new "GM_ORIGINAL" 
                        replaceString -edit $editFile -old "IndicatedOutcomePercent" -new "IND_OUTCOME_PCT" 
                        replaceString -edit $editFile -old "ProjectedGMVariancePercent" -new "PROJVAR_GM_PCT" 
                        Write-Log "FIXED: $report ($description)"; break
                    }
                    Default {Write-Log "IGNORE: $report ($description)" break;}
                }

            }

        }

        $XmlDocument.save($userFile)
    }

    Write-Log "Adjustment complete"
}

function replaceString {
    Param ([string]$editFile, [string]$old, [string]$new)

    (Get-Content $editFile) -replace $old, $new | Set-Content $editFile
    
}

Write-Log "-------------------------------------"
Write-Log "Backing up bookmarks"
if ((backupOldBookmarks) -ne "error"){
    Write-Log "Backed up without errors"
    adjustXml
}

