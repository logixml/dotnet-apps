set def off

exec pk_pentaLogin.p_setUserId( 'ATNEP' );
exec pk_pentaLogin.p_setChangeAuth( 'IFP0002841' );
-----------------------------------------------------------
-- Remove Combined Gross Margin (deprecated) menu item from customer's database
-- Menu_item_num = 3105
-- security_item_num = 1072
-----------------------------------------------------------
delete from gui_menu_item
where menu_item_num in (3262)
  and menu_item_descr like 'Income and Expense by Period Chart';

delete from gui_security
where menu_item_num in
	(select menu_item_num
	   from gui_menu_item
	  where menu_item_num = 3262
	    and menu_item_descr like 'Income and Expense by Period Chart');

delete from gui_menu_item
where menu_item_num in (3263)
  and menu_item_descr like 'Net Worth by Period Chart';

delete from gui_security
where menu_item_num in
	(select menu_item_num
	   from gui_menu_item
	  where menu_item_num = 3263
	    and menu_item_descr like 'Net Worth by Period Chart');

  delete from gui_menu_item
where menu_item_num in (3264)
  and menu_item_descr like 'Vendor Information';

delete from gui_security
where menu_item_num in
	(select menu_item_num
	   from gui_menu_item
	  where menu_item_num = 3264
	    and menu_item_descr like 'Vendor Information');

delete from gui_security
where menu_item_num in
	(select menu_item_num
	   from gui_menu_item
	  where menu_item_num = 3105
	    and menu_item_descr like 'Combined Gross Margin');

delete from gui_menu_item
where menu_item_num in (3105)
  and menu_item_descr like 'Combined Gross Margin';

delete from security_item
where security_item_num in
	(select security_item_num
	   from gui_menu_item
	  where menu_item_num = 3105
	    and menu_item_descr like 'Combined Gross Margin');
-----------------------------------------------------------
-- Remove Job Location Map menu item from customer's database
-- Menu_item_num = 3079
-- security_item_num = 1078
-----------------------------------------------------------
delete from gui_security
where menu_item_num = 3079;

delete from gui_menu_item
where menu_item_num in (3079);

delete from security_item
where security_item_num in
	(select security_item_num
	   from gui_menu_item
	  where menu_item_num = 3079
	    and menu_item_descr like 'Job Location Map');

-----------------------------------------------------------
-- Merge workbench module folders into gui_menu_item
-----------------------------------------------------------
merge into gui_menu_item
using (
select 3254 as menu_item_num,
       3078 as par_menu_item_num,
       75 as sort_seq_num,
       'M' as menu_item_type_cd,
       'Purchasing' as menu_item_descr,
       'N' as cust_cd,
       NULL as security_item_num
  from dual
) data
on (gui_menu_item.menu_item_num = data.menu_item_num)
when matched then update
   set gui_menu_item.par_menu_item_num = data.par_menu_item_num,
       gui_menu_item.sort_seq_num = data.sort_seq_num,
       gui_menu_item.menu_item_type_cd = data.menu_item_type_cd,
       gui_menu_item.menu_item_descr = data.menu_item_descr,
       gui_menu_item.cust_cd = data.cust_cd,
       gui_menu_item.security_item_num = data.security_item_num
when not matched then insert
   (gui_menu_item.menu_item_num,
    gui_menu_item.par_menu_item_num,
    gui_menu_item.sort_seq_num,
    gui_menu_item.menu_item_type_cd,
    gui_menu_item.menu_item_descr,
    gui_menu_item.cust_cd,
    gui_menu_item.security_item_num)
   values (data.menu_item_num,
           data.par_menu_item_num,
           data.sort_seq_num,
           data.menu_item_type_cd,
           data.menu_item_descr,
           data.cust_cd,
           data.security_item_num);
-----------------------------------------------------------
-- Merge reporting sub-folders into gui_menu_item
-----------------------------------------------------------
merge into gui_menu_item
using (
select 3255 as menu_item_num,
       3254 as par_menu_item_num,
       10 as sort_seq_num,
       'M' as menu_item_type_cd,
       'Reporting' as menu_item_descr,
       'N' as cust_cd,
       NULL as security_item_num
  from dual
) data
on (gui_menu_item.menu_item_num = data.menu_item_num)
when matched then update
   set gui_menu_item.par_menu_item_num = data.par_menu_item_num,
       gui_menu_item.sort_seq_num = data.sort_seq_num,
       gui_menu_item.menu_item_type_cd = data.menu_item_type_cd,
       gui_menu_item.menu_item_descr = data.menu_item_descr,
       gui_menu_item.cust_cd = data.cust_cd,
       gui_menu_item.security_item_num = data.security_item_num
when not matched then insert
   (gui_menu_item.menu_item_num,
    gui_menu_item.par_menu_item_num,
    gui_menu_item.sort_seq_num,
    gui_menu_item.menu_item_type_cd,
    gui_menu_item.menu_item_descr,
    gui_menu_item.cust_cd,
    gui_menu_item.security_item_num)
   values (data.menu_item_num,
           data.par_menu_item_num,
           data.sort_seq_num,
           data.menu_item_type_cd,
           data.menu_item_descr,
           data.cust_cd,
           data.security_item_num);
-----------------------------------------------------------
-- Merge reports/charts into gui_menu_item
-----------------------------------------------------------
merge into gui_menu_item
using (
select 3258 as menu_item_num,
       3255 as par_menu_item_num,
       30 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Purchase Order Summary' as menu_item_descr,
       'N' as cust_cd,
       1339 as security_item_num
  from dual
union all
select 3012 as menu_item_num,
       3113 as par_menu_item_num,
       15 as sort_seq_num,
       'I' as menu_item_type_cd,
       'PTO Balances - All Employees' as menu_item_descr,
       'N' as cust_cd,
       1336 as security_item_num
  from dual
union all
select 3261 as menu_item_num,
       3157 as par_menu_item_num,
       20 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Employee Information' as menu_item_descr,
       'N' as cust_cd,
       1342 as security_item_num
  from dual
union all
select 3268 as menu_item_num,
       3104 as par_menu_item_num,
       30 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Income and Expense by Period Chart' as menu_item_descr,
       'N' as cust_cd,
       1354 as security_item_num
  from dual
union all
select 3269 as menu_item_num,
       3104 as par_menu_item_num,
       35 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Net Worth by Period Chart' as menu_item_descr,
       'N' as cust_cd,
       1355 as security_item_num
  from dual
union all
select 3057 as menu_item_num,
       3104 as par_menu_item_num,
       15 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Expense Account Balances' as menu_item_descr,
       'N' as cust_cd,
       1346 as security_item_num
  from dual
union all
select 3270 as menu_item_num,
       3166 as par_menu_item_num,
       30 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Vendor Information' as menu_item_descr,
       'N' as cust_cd,
       1356 as security_item_num
  from dual
union all
select 3265 as menu_item_num,
       3171 as par_menu_item_num,
       55 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Customer Information' as menu_item_descr,
       'N' as cust_cd,
       1350 as security_item_num
  from dual
union all
select 3266 as menu_item_num,
       3113 as par_menu_item_num,
       12 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Employee Safety Incidents' as menu_item_descr,
       'N' as cust_cd,
       1352 as security_item_num
  from dual
union all
select 3115 as menu_item_num,
       3113 as par_menu_item_num,
       40 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Upcoming Training Classes' as menu_item_descr,
       'N' as cust_cd,
       1084 as security_item_num
  from dual
union all
select 3137 as menu_item_num,
       3136 as par_menu_item_num,
       20 as sort_seq_num,
       'M' as menu_item_type_cd,
       'Reporting' as menu_item_descr,
       'N' as cust_cd,
       Null as security_item_num
  from dual
union all
select 3116 as menu_item_num,
       3106 as par_menu_item_num,
       10 as sort_seq_num,
       'I' as menu_item_type_cd,
       '< 40 Regular Hours with Overtime Hours' as menu_item_descr,
       'N' as cust_cd,
       1085 as security_item_num
  from dual
union all
select 3112 as menu_item_num,
       3106 as par_menu_item_num,
       20 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Employees Missing Timecards' as menu_item_descr,
       'N' as cust_cd,
       1082 as security_item_num
  from dual
union all
select 3159 as menu_item_num,
       3106 as par_menu_item_num,
       30 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Labor Cost & Hours Trend' as menu_item_descr,
       'N' as cust_cd,
       1247 as security_item_num
  from dual
union all
select 3107 as menu_item_num,
       3106 as par_menu_item_num,
       40 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Labor Hours' as menu_item_descr,
       'N' as cust_cd,
       1233 as security_item_num
  from dual
union all
select 3260 as menu_item_num,
       3106 as par_menu_item_num,
       50 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Missing Time by Work Date' as menu_item_descr,
       'N' as cust_cd,
       1341 as security_item_num
  from dual
union all
select 3155 as menu_item_num,
       3106 as par_menu_item_num,
       60 as sort_seq_num,
       'I' as menu_item_type_cd,
       'My Paid Time Off Summary' as menu_item_descr,
       'N' as cust_cd,
       1239 as security_item_num
  from dual
union all
select 3117 as menu_item_num,
       3106 as par_menu_item_num,
       70 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Regular Hours > 40' as menu_item_descr,
       'N' as cust_cd,
       1086 as security_item_num
  from dual
union all
select 3009 as menu_item_num,
       3106 as par_menu_item_num,
       80 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Undistributed Timecards' as menu_item_descr,
       'N' as cust_cd,
       1093 as security_item_num
  from dual
union all
select 3157 as menu_item_num,
       3106 as par_menu_item_num,
       90 as sort_seq_num,
       'M' as menu_item_type_cd,
       'Reporting' as menu_item_descr,
       'N' as cust_cd,
       Null as security_item_num
  from dual
union all
select 3176 as menu_item_num,
       3119 as par_menu_item_num,
       10 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Customer Profitability Analysis' as menu_item_descr,
       'N' as cust_cd,
       1273 as security_item_num
  from dual
union all
select 3175 as menu_item_num,
       3119 as par_menu_item_num,
       20 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Customer Profitability List' as menu_item_descr,
       'N' as cust_cd,
       1272 as security_item_num
  from dual
union all
select 3123 as menu_item_num,
       3119 as par_menu_item_num,
       30 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Expiring Maintenance Contracts' as menu_item_descr,
       'N' as cust_cd,
       1092 as security_item_num
  from dual
union all
select 3141 as menu_item_num,
       3119 as par_menu_item_num,
       40 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Fixed Price Maintenance Contract Margin Performance' as menu_item_descr,
       'N' as cust_cd,
       1235 as security_item_num
  from dual
union all
select 3178 as menu_item_num,
       3119 as par_menu_item_num,
       50 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Maintenance Contract Analysis' as menu_item_descr,
       'N' as cust_cd,
       1277 as security_item_num
  from dual
union all
select 3242 as menu_item_num,
       3119 as par_menu_item_num,
       60 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Service Opportunities' as menu_item_descr,
       'N' as cust_cd,
       1323 as security_item_num
  from dual
union all
select 3010 as menu_item_num,
       3119 as par_menu_item_num,
       70 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Unassigned Work Orders' as menu_item_descr,
       'N' as cust_cd,
       1094 as security_item_num
  from dual
union all
select 3120 as menu_item_num,
       3119 as par_menu_item_num,
       80 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Unbilled Work Order Cost' as menu_item_descr,
       'N' as cust_cd,
       1088 as security_item_num
  from dual
union all
select 3233 as menu_item_num,
       3119 as par_menu_item_num,
       90 as sort_seq_num,
       'M' as menu_item_type_cd,
       'Reporting' as menu_item_descr,
       'N' as cust_cd,
       Null as security_item_num
  from dual
union all
select 3247 as menu_item_num,
       3233 as par_menu_item_num,
       30 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Service Work Order Summary Report' as menu_item_descr,
       'N' as cust_cd,
       1328 as security_item_num
  from dual union all
select 3245 as menu_item_num,
       3233 as par_menu_item_num,
       40 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Technician Status Change Analytic Report' as menu_item_descr,
       'N' as cust_cd,
       1326 as security_item_num
  from dual
union all
select  3253 as menu_item_num,
        3090 as par_menu_item_num,
        115 as sort_seq_num,
        'I' as menu_item_type_cd,
        'Work Package Schedule Reliability Measure' as menu_item_descr,
        'N' as cust_cd,
        1334 as security_item_num
  from dual
union all
select 3168 as menu_item_num,
       3090 as par_menu_item_num,
       60 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Job Workbench' as menu_item_descr,
       'N' as cust_cd,
       1264 as security_item_num
  from dual
union all
select 3088 as menu_item_num,
       3090 as par_menu_item_num,
       55 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Job Purchase Orders' as menu_item_descr,
       'N' as cust_cd,
       1058 as security_item_num
  from dual
union all
select 3267 as menu_item_num,
       3137 as par_menu_item_num,
       30 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Fixed Assets/Equipment By Disposal Date' as menu_item_descr,
       'N' as cust_cd,
       1353 as security_item_num
  from dual
union all
select 3271 as menu_item_num,
       3137 as par_menu_item_num,
       25 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Fixed Assets/Equipment By Current Location' as menu_item_descr,
       'N' as cust_cd,
       1358 as security_item_num
  from dual
union all
select 3273 as menu_item_num,
       3090 as par_menu_item_num,
       41 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Incomplete Activity Logs' as menu_item_descr,
       'N' as cust_cd,
       1360 as security_item_num
  from dual
union all
select 3257 as menu_item_num,
       3255 as par_menu_item_num,
       10 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Purchase Order Change Order' as menu_item_descr,
       'N' as cust_cd,
       1338 as security_item_num
  from dual
union all
select 3256 as menu_item_num,
       3255 as par_menu_item_num,
       20 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Purchase Order Detail' as menu_item_descr,
       'N' as cust_cd,
       1337 as security_item_num
  from dual
union all
select 3274 as menu_item_num,
       3157 as par_menu_item_num,
       60 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Labor & Overhead - Hours Only' as menu_item_descr,
       'N' as cust_cd,
       1363 as security_item_num
  from dual
union all
select 3275 as menu_item_num,
       3137 as par_menu_item_num,
       22 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Fixed Assets/Equipment By Acquisition Date' as menu_item_descr,
       'N' as cust_cd,
       1364 as security_item_num
  from dual
union all
select 3277 as menu_item_num,
       3014 as par_menu_item_num,
       20 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Job Information' as menu_item_descr,
       'N' as cust_cd,
       1365 as security_item_num
  from dual
union all
select 3281 as menu_item_num,
       3157 as par_menu_item_num,
       70 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Labor, Overhead & Actual Burden',
       'N' as cust_cd,
       1368 as security_item_num
  from dual
union all
select 3282 as menu_item_num,
       3137 as par_menu_item_num,
       21 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Equipment Cost History',
       'N' as cust_cd,
       1369 as security_item_num
  from dual    
union all
select 3287 as menu_item_num,
       3147 as par_menu_item_num,
       5 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Invoice Register',
       'N' as cust_cd,
       1374 as security_item_num
  from dual
union all
select 3290 as menu_item_num,
       3090 as par_menu_item_num,
       140 as sort_seq_num,
       'M' as menu_item_type_cd,
       'TEP Reporting',
       'N' as cust_cd,
       null as security_item_num
  from dual 
union all
select 3291 as menu_item_num,
       3290 as par_menu_item_num,
       10 as sort_seq_num,
       'I' as menu_item_type_cd,
       'Clock Hours vs. Activity Log Hours',
       'N' as cust_cd,
       1377 as security_item_num
  from dual   
) data
on (gui_menu_item.menu_item_num = data.menu_item_num)
when matched then update
   set gui_menu_item.par_menu_item_num = data.par_menu_item_num,
       gui_menu_item.sort_seq_num = data.sort_seq_num,
       gui_menu_item.menu_item_type_cd = data.menu_item_type_cd,
       gui_menu_item.menu_item_descr = data.menu_item_descr,
       gui_menu_item.cust_cd = data.cust_cd,
       gui_menu_item.security_item_num = data.security_item_num
when not matched then insert
   (gui_menu_item.menu_item_num,
    gui_menu_item.par_menu_item_num,
    gui_menu_item.sort_seq_num,
    gui_menu_item.menu_item_type_cd,
    gui_menu_item.menu_item_descr,
    gui_menu_item.cust_cd,
    gui_menu_item.security_item_num)
   values (data.menu_item_num,
           data.par_menu_item_num,
           data.sort_seq_num,
           data.menu_item_type_cd,
           data.menu_item_descr,
           data.cust_cd,
           data.security_item_num);

-----------------------------------------------------------
-- Insert Security Item Num
-----------------------------------------------------------
insert into security_item( security_item_num, security_item_descr )
select security_item_num, menu_item_descr
  from gui_menu_item
where security_item_num in (1336, 1339, 1341, 1342, 1346, 1350, 1352, 1354, 1355, 1356, 1358, 1360, 1337, 1338, 1363, 1364, 1368, 1374, 1377)
   and not exists
       (select 'x'
          from security_item
         where security_item.security_item_num = gui_menu_item.security_item_num);

-----------------------------------------------------------
-- Add TEP Url Parameters if they don't exist.
-----------------------------------------------------------
merge into gui_menu_item
using (
select 2984 as menu_item_num,
       2000 as par_menu_item_num,
       5 as sort_seq_num,
       'I' as menu_item_type_cd,
       'PENTA Workbench' as menu_item_descr,
       'http://%c(workbench:server)%/%c(workbench:NavigatorName)%/Navigation/%c(workbench:WorkbenchName)%/%u/%t' as menu_item_command,
       'N' as cust_cd,
       1076 as security_item_num
  from dual
) data
on (gui_menu_item.menu_item_num = data.menu_item_num)
when matched then update
   set gui_menu_item.par_menu_item_num = data.par_menu_item_num,
       gui_menu_item.sort_seq_num = data.sort_seq_num,
       gui_menu_item.menu_item_type_cd = data.menu_item_type_cd,
       gui_menu_item.menu_item_descr = data.menu_item_descr,
       gui_menu_item.menu_item_command = data.menu_item_command,
       gui_menu_item.cust_cd = data.cust_cd,
       gui_menu_item.security_item_num = data.security_item_num
when not matched then insert
   (gui_menu_item.menu_item_num,
    gui_menu_item.par_menu_item_num,
    gui_menu_item.sort_seq_num,
    gui_menu_item.menu_item_type_cd,
    gui_menu_item.menu_item_descr,
    gui_menu_item.menu_item_command,
    gui_menu_item.cust_cd,
    gui_menu_item.security_item_num)
   values (data.menu_item_num,
           data.par_menu_item_num,
           data.sort_seq_num,
           data.menu_item_type_cd,
           data.menu_item_descr,
           data.menu_item_command,
           data.cust_cd,
           data.security_item_num);
