set def off

exec pk_pentaLogin.p_setUserId( 'ATNEP' );
exec pk_pentaLogin.p_setChangeAuth( 'IFP0002945' );
-----------------------------------------------------------
-- Remove unneeded Urls
-----------------------------------------------------------

delete from workbench_config
where config_key in
	(select config_key
	   from workbench_config
	  where config_key = 'CustomerConsoleUrl' );

delete from workbench_config
where config_key in
	(select config_key
	   from workbench_config
	  where config_key = 'eTimeUrl' );

delete from workbench_config
where config_key in
	(select config_key
	   from workbench_config
	  where config_key = 'eTimeURL' );

delete from workbench_config
where config_key in
	(select config_key
	   from workbench_config
	  where config_key = 'eTimeRedirectURL' );

delete from workbench_config
where config_key in
	(select config_key
	   from workbench_config
	  where config_key = 'TEPUrl' );

-----------------------------------------------------------
-- Merge static config keys
-----------------------------------------------------------
merge into workbench_config
using (
select 'CustomerConsoleRedirectUrl' as config_key,
       '/calltaking/app/calltaking' as config_value
  from dual
 union all
 select 'CustomerConsoleWORedirectUrl' as config_key,
       '/calltaking/app/calltaking/customerlocation/$cusLoc/workorder/$woId' as config_value
  from dual
 union all
 select 'eTimeRedirectUrl' as config_key,
       '/etime/timesheets/mine' as config_value
  from dual
 union all
 select 'PentaWebLanding' as config_key,
       '/landing' as config_value
  from dual
 union all
 select 'ProtocolHandler' as config_key,
       'y' as config_value
  from dual
 union all
 select 'TEPRedirectUrl' as config_key,
       '/tep/app/timeEquipmentProduction/activityLogs' as config_value
  from dual
 union all
 select 'WPMRedirectUrl' as config_key,
       '/projectmanagement/job/$jobId/workPackagePlanning/$wpmId' as config_value
  from dual
) data
on (workbench_config.config_key = data.config_key)
when matched then update
   set workbench_config.config_value = data.config_value
when not matched then insert
   (workbench_config.config_key,
    workbench_config.config_value)
   values (data.config_key,
           data.config_value);

-----------------------------------------------------------
-- Insert viarable Keys if not exist
-----------------------------------------------------------
merge into workbench_config
using (
select 'NavigatorName' as config_key,
       '' as config_value
  from dual
 union all
 select 'PentaWebUrl' as config_key,
       '' as config_value
  from dual
 union all
 select 'server' as config_key,
       '' as config_value
  from dual
 union all
 select 'WorkbenchName' as config_key,
       '' as config_value
  from dual
) data
on (workbench_config.config_key = data.config_key)
when not matched then insert
   (workbench_config.config_key,
    workbench_config.config_value)
   values (data.config_key,
           data.config_value);
