NOTE: It should (almost) always be safe to just do the full install, at least
until someone makes other bug fixes in the installed packages and views (this
would be rare).


Full Install:
   1 - copy the files to a location where you can access SQL*Plus
   2 - connect to the target schema via SQL*Plus
   3 - type "@install" at the SQL*Plus command line (SQL> @install)


Merge Menu Items
   If you just want to merge the menu items, only run the @loadMenuItems script
   instead of the @install script.