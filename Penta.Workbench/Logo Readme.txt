How to store logos to be used when exporting Workbench reports to PDF or Excel:

1. Create a "SavedLogos" directory under Workbench application directory
(ex: C:\Applications\PentaWorkbench\Penta.Workbench\SavedLogos

2. Copy a logo file (.jpg, .png or .emf) into the SavedLogos directory, using the OU ID and the file extension of the image type.
(ex: If the OU ID is 10000, the filename should be 10000.jpg, 10000.png or 10000.emf)

Please note: 
1. The directory should only contain one logo file for each Legal Entity OU ID. 
(ex: Either 10000.jpg or 10000.png, not both)

2. The original logo file can be of any height/width. A resized copy (ex: 10000_sm.jpg) is automatically generated and stored in the SavedLogos directory. This smaller copy 
is then displayed in the exported reports.





How to implement Report Headers when exporting Workbench reports to PDF or Excel:

1. From the Workbench hamburger menu in the upper left corner, click 'Report Export Options'.

2. For 'Include Report Header', select 'Yes', and close the 'Report Export Options' popup window.

3. From any report, Click the export button and choose either PDF or Excel.

4. If the current Workbench user has been assigned an employee Id in Penta For Windows (PfW) and that employee Id has been assigned to an OU and 
a logo file for the OU's respective Legal Entity OU ID exists in the SavedLogos directory, then that logo file will appear in the exported reports.

(ex: OU 10000 is set up as a Legal Entity OU, then the logo file should be named 10000.jpg.)
(ex: OU 10001 is not set up as an Legal Entity OU, but is a child of OU 10000, then the logo file should be named 10000.jpg).


