<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc382291596"></a><a name="_Toc297114512">The Dashboard Builder</a></h2>

<p class=MsoNormal>The <i>Dashboard Builder</i> is an interface designed to
build dashboards. </p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>The Dashboard Builder is <i>not</i> designed to build
  or modify reports.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>Click <b>New Dashboard</b> to access the Dashboard Builder<i>.</i></p>



<p class=MsoNormal><img   id="Picture 30"
src="Report_Design_Guide_files/image038.jpg"></p>


<p class=MsoNormal><i><br>
Navigating the Dashboard Builder</i></p>


<p class=MsoNormal>The core steps in building a dashboard are:</p>

<p class=MsoNormal> </p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Specifying a dashboard name.</li>
 <li class=MsoNormal>Configuring one or more dashboard panels.</li>
</ul>



<p class=MsoNormal>These steps will be presented in this section to show the
navigation options.</p>



<p class=MsoNormal>Across the top of the page is the  breadcrumb trail  - a
series of links to the pages and folders recently visited:<br>
<br>
</p>


<p class=MsoNormal><img   id="Picture 31"
src="Report_Design_Guide_files/image039.jpg"></p>


<p class=MsoNormal><br>
For example, clicking the Personal Reports link in the breadcrumb trail will
display the Reports page. If changes to the dashboard have not been saved, the
following confirmation dialog box will be displayed:</p>


<p class=MsoNormal><img   id="Picture 305"
src="Report_Design_Guide_files/image040.jpg"><br>
<br>
</p>


<p class=MsoNormal>Below the breadcrumb trail is dashboard management bar that
allows you to specify a <i>Dashboard Name</i>, save the dashboard, review the dashboard
using the <b>Preview Dashboard</b> button, and navigate back to the <i>Reports</i>
page with the <b>Back to Reports List </b>button.</p>


<p class=MsoNormal>Clicking the  <img   id="Picture 34"
src="Report_Design_Guide_files/image034.jpg">  or  <img  
id="Picture 35" src="Report_Design_Guide_files/image035.jpg">  buttons will
collapse or expand the Dashboard Settings panel.</p>


<p class=MsoNormal><img   id="Picture 36"
src="Report_Design_Guide_files/image041.jpg"></p>



<p class=MsoNormal>Below the dashboard management bar is the list of dashboard
panels. Initially only the <b>Add a Panel</b> button is displayed. When one or
more panels have been configured, the dashboard list will appear as:</p>


<p class=MsoNormal><img   id="Picture 37"
src="Report_Design_Guide_files/image042.jpg"></p>



<p class=MsoNormal>Click <b>Delete Panels</b> to remove any selected panels.
Panels are selected by checking its checkbox. All panels can be selected or
deselected by checking the checkbox in the list header.</p>


<p class=MsoNormal>Actions can be performed on each panel by hovering the mouse
over the <img   id="Picture 38"
src="Report_Design_Guide_files/image043.jpg">  icon to display the drop-down
list of available actions and then selecting the action. The available actions
are <i>Modify Dashboard Panel,</i> <i>Move Up,</i> and <i>Move Down</i>.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>Clicking either the <b>Add Panel</b> button or the <i>Modify
Dashboard Panel</i> action will display the <i>Panel Settings</i> dialog box.</p>



<p class=MsoNormal><img   id="Picture 306"
src="Report_Design_Guide_files/image044.jpg"></p>



<p class=MsoNormal>After entering the panel settings, click <b>Save Panel</b>
to temporarily save the panel definition and return to the list of panels.</p>


<p class=MsoNormal>The dashboard definition is permanently saved by clicking <b>Save
</b>or <b>Save As</b> in the <i>Dashboard Builder</i>. </p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>



</body>
</html>
