<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h1><a name="_Toc456945123">Export Options</a></h1>




<p class=MsoFooter>The <i>Export Options</i> element allows links to be added
to the bottom of a report to give users the ability to export reports. Reports can
be printed from a browser, exported to popular formats such as Excel and PDF, or
added to an archive for other users to view.</p>


<p class=MsoFooter>Click the Exports element in the ADD frame to add the <i>Export
Options</i> tab to the report definition.</p>



<p class=MsoFooter><img border=0   id="Picture 218"
src="Report_Design_Guide_files/image173.jpg"></p>


<p class=MsoFooter>Export capability is added to the report by checking the
individual export options.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoFooter>The export options shown below are selectivelu available to end-users.
None, some, or all of the icons may be visible to the end-user based on availability
and user permissions.</p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse'>
 <tr>
  <td width=285 style='width:170.85pt;padding:0in 5.4pt 0in 5.4pt'>
  </td>
  <td width=85 style='width:51.25pt;padding:0in 5.4pt 0in 5.4pt'>
  </td>
 </tr>
 <tr>
  <td width=285 style='width:170.85pt;padding:0in 5.4pt 0in 5.4pt'>
  </td>
  <td width=85 style='width:51.25pt;padding:0in 5.4pt 0in 5.4pt'>
  </td>
 </tr>
 <tr>
  <td width=285 style='width:170.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>Export to Excel</li>
  </ul>
  </td>
  <td width=85 style='width:51.25pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><img border=0   id="Picture 219"
  src="Report_Design_Guide_files/image174.gif" alt=expExcel></p>
  </td>
 </tr>
 <tr>
  <td width=285 style='width:170.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>Export to Word</li>
  </ul>
  </td>
  <td width=85 style='width:51.25pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><img border=0   id="Picture 220"
  src="Report_Design_Guide_files/image175.gif" alt=expWord></p>
  </td>
 </tr>
 <tr>
  <td width=285 style='width:170.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>Export to PDF</li>
  </ul>
  </td>
  <td width=85 style='width:51.25pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><img border=0   id="Picture 221"
  src="Report_Design_Guide_files/image176.gif" alt=expPDF></p>
  </td>
 </tr>
 <tr>
  <td width=285 style='width:170.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>Export to CSV</li>
  </ul>
  </td>
  <td width=85 style='width:51.25pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><img border=0   id="Picture 222"
  src="Report_Design_Guide_files/image177.gif" alt=csvIcon></p>
  </td>
 </tr>
 <tr>
  <td width=285 style='width:170.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>Export to XML</li>
  </ul>
  </td>
  <td width=85 style='width:51.25pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><img border=0   id="Picture 223"
  src="Report_Design_Guide_files/image178.gif" alt=XMLLink></p>
  </td>
 </tr>
 <tr>
  <td width=285 style='width:170.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>Add to Archive</li>
  </ul>
  </td>
  <td width=85 style='width:51.25pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><img border=0   id="Picture 224"
  src="Report_Design_Guide_files/image179.gif" alt=archExport></p>
  </td>
 </tr>
 <tr>
  <td width=285 style='width:170.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>Send Email</li>
  </ul>
  </td>
  <td width=85 style='width:51.25pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><img border=0  
  src="Report_Design_Guide_files/image180.gif"></p>
  </td>
 </tr>
</table>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst>Notes:</p>
  <p class=NotesCxSpMiddle>Microsoft Office is integrated with Internet
  Explorer. Exporting the reports in an Office format opens a new browser
  window to edit and save the report. Microsoft Office is required to edit the
  reports from a browser window.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>The Export Options are available in the rendered
  report to users with the appropriate Right to add the export option(s) to the
  report from the Report Builder.</p>
  </div>
  </td>
 </tr>
</table>

</div>



<p class=MsoNormal><i>Export to Excel</i> saves the report in Microsoft Excel
format. If Microsoft Excel is installed, the report can be edited as an Excel
spreadsheet from the browser window.</p>


<p class=MsoNormal><i>Export to Word</i> saves the report in Microsoft Word
format. If Microsoft Word is installed, the report can be edited as a Word
document from the browser window.</p>


<p class=MsoNormal><i>Export to PDF</i> saves the report as a PDF (Portable
Document Format), which can be viewed with Adobe Reader or in directly in most
modern browsers, and edited with Adobe Acrobat.</p>


<p class=MsoNormal><i>Export to CSV</i> saves the report to a comma-separated
format. The file can be viewed with Microsoft Excel.</p>


<p class=MsoNormal><i>Export to XML</i> saves the report data in XML format. The
XML dataset can be viewed in most browsers.</p>

<p class=MsoNormal><i>Add to Archive </i>saves a copy of the current report to
the archive. Archiving is a great way to back-up a report before modifying it.</p>


<p class=MsoNormal><i>Send Report by Email </i>exports the report in the format
selected and attaches it to emails sent to selected recipients.</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Archiving and XML exports are not always available
  when creating a new report. Contact the system administrator for assistance
  with archiving and XML exports.</p>
  </div>
  </td>
 </tr>
</table>

</div>



</body>
</html>
