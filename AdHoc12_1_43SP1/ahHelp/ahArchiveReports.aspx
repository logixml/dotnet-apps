<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945139">Archiving Reports</a></h2>


<p class=MsoNormal>Archiving reports gives users the ability to retain copies
of a single report as the report is changed over time. Reports created with Ad
Hoc are interactive and dynamic - the report data may change depending on when
the report is executed or modified. A report archive stores copies of the
report to retain important data.</p>

<p class=MsoNormal><br>
<br>
</p>

<p class=MsoNormal><b>To add a report to the archive at runtime for report
built with the <i>Add to Archive</i> option:</b></p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Ensure
that the <i>Add to Archive </i>checkbox is checked in the Export Options tab.</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Run the
report and click the <i>Add to Archive </i>icon at the bottom of the report.</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>When the
Reports list is refreshed, the <i>View Archives</i> action will be available
for the report.</p>


<p class=MsoNormal> </p>

<p class=MsoNormal><b>To view and manage an archived report:</b></p>


<p class=MsoNormal>To view archived reports, hover your mouse cursor over the
report s <b>More</b> button and select <i>View Archives</i> from its drop-down
list. </p>


<p class=MsoNormal><img border=0   id="Picture 273"
src="Report_Design_Guide_files/image214.jpg"></p>


<p class=MsoNormal><br>
Click the <i>Archive Date</i> link of the desired archive in the list to view
the report.</p>


<p class=MsoNormal>Click the <img border=0   id="Picture 274"
src="Report_Design_Guide_files/image215.gif" alt=sendEmail>  icon to email the
archived report.</p>


<p class=MsoNormal>Click the <i>Archived Date</i> column header to sort the
list.</p>


<p class=MsoNormal>Archived reports can be selected for deletion by checking
their checkboxes. Click <b>Delete Archives</b> to remove the selected reports
from the archives. Confirm the deletion by clicking <b>OK</b> when prompted for
confirmation.</p>



</body>
</html>
