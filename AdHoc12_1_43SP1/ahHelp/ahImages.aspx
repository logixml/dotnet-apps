<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945121"></a><a name="_Toc382291617">Adding Images</a></h2>


<p class=MsoNormal>The Image element offers the ability to add an image to the
report layout. An unlimited number of images can be added above or below other
report elements. Use images to display a company logo, disclaimer or warning or
to bring emphasis to a new or updated section of a report. </p>


<p class=MsoNormal>The image can be added to the report from a file or referenced
with a URL. Image files must be 1 megabyte or smaller in size and must be one
of the following file types: JPG, JPEG, GIF, BMP. Animated images are
supported.</p>




<p class=MsoNormal><img border=0   id="Picture 212"
src="Report_Design_Guide_files/image170.jpg" alt=9></p>

<p class=MsoNormal><span style='font-size:10.0pt'>A report with an image
appearing before the tabular report.</span></p>



<p class=MsoNormal>Click the Image icon in the ADD frame to add an Image
element to the report definition. An Image Information tab will be created.<br>
<br>
</p>


<p class=MsoNormal><img border=0   id="Picture 214"
src="Report_Design_Guide_files/image171.jpg"></p>

<p class=MsoNormal>Select a Source type for the image file. If <i>File</i> is
selected, click one of the location options: click <b>From My Computer </b>and
then <b>Browse</b> to navigate to and select a file from your computer. To
select an image from a library of uploaded images, click <b>Previously Uploaded
</b>and select an image. Click <b>OK</b> when ready.</p>


<p class=MsoNormal>If <i>URL</i> is selected, specify the URL to access the
image and then click <b>OK</b>.</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst>Notes:</p>
  <p class=NotesCxSpLast>Images selected from your computer are automatically
  uploaded to the application server for later use by all users.</p>
  </div>
  </td>
 </tr>
</table>

</div>



</body>
</html>
