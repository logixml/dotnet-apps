<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945122"></a><a name="_Toc382291618">Setting Report Style</a></h2>


<p class=MsoNormal>To change the style sheet associated with the report, click <b>Select
Report Style</b>. The <i>Report Style</i> dialog box will be displayed:<br>
<br>
</p>


<p class=MsoNormal><img border=0   id="Picture 225"
src="Report_Design_Guide_files/image172.jpg"></p>


<p class=MsoNormal>Select a style from the drop-down list and click <b>OK</b>.</p>

<h3 align=right style='text-align:right'><a name="_Toc382291619"><span
style='font-size:18.0pt'>&nbsp;</span></a></h3>

</body>
</html>
