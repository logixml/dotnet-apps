<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945110">Grouping Data</a></h2>


<p class=MsoNormal>Ad Hoc provides two styles of grouped reports: <i>Flat-Table
</i>and <i>Drill-Down</i>. </p>


<p class=MsoNormal>The grouped Flat-Table style organizes records into groups
and hides duplicate entries to make the grouped report more presentable. Use this
style to give other users the ability to export the complete report to
different formats such as PDF and Excel. </p>


<p class=MsoNormal>The grouped Drill-Down style organizes records into groups
and any remaining columns are included in a sub-report. Use this style to give
other users the ability to hide and show sub-reports.</p>


<p class=MsoNormal>The feasibility of grouping data is highly dependent on the
data source chosen for the report. For example, a grouped report is not
necessarily appropriate when the data is simply a list of customers. But, if the
data source contains information about customers and orders, a grouped report
will definitely improve the presentation of the report. The application
provides two ways to group data: <i>multiple grouping columns </i>and <i>multiple
layers</i>. Users can combine both methods when creating grouped reports.<br>
<br>
</p>


<p class=MsoNormal><i>Multiple Grouping Layers</i></p>


<p class=MsoNormal>Creating grouped reports with multiple layers is useful in
scenarios where the report requires more than one grouping to organize all the
data.</p>


<p class=MsoNormal>For example, all the customers can be grouped together, and
then group by the shipping company and then group by the order date. This
scenario is illustrated as a grouped flat-table report and as a grouped
drill-down report in the following examples:</p>


<p class=MsoNormal><img border=0   id="Picture 114"
src="Report_Design_Guide_files/image104.jpg"></p>

<p class=MsoFooter><span style='font-size:10.0pt'>   A grouped flat-table
report with three layers.</span></p>




<p class=MsoNormal><img border=0   id="Picture 115"
src="Report_Design_Guide_files/image105.jpg"></p>

<p class=MsoNormal><span style='font-size:10.0pt'><br>
   A grouped drill-down report with three grouping layers.</span></p>



<b><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></b>


<p class=MsoNormal><i>Multi-Column Grouping</i></p>


<p class=MsoNormal>Multi-column grouping is useful for displaying more
information on a single grouping layer. For example, users can group by
customer and then by shipping company, showing both the customer and shipping
company on the same layer.</p>


<p class=MsoNormal>Multiple grouping columns work well with flat-table style
reports, since duplicate entries are removed from the final report.<br>
<br>
</p>



<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse'>
 <tr>
  <td width=603 valign=top style='width:361.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><img border=0
    id="Picture 116"
  src="Report_Design_Guide_files/image106.jpg"></p>
  </td>
 </tr>
 <tr>
  <td width=603 valign=top style='width:361.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><img border=0
    id="Picture 117"
  src="Report_Design_Guide_files/image107.jpg" alt="down_arrow"></p>
  </td>
 </tr>
 <tr>
  <td width=603 valign=top style='width:361.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><img border=0
    id="Picture 118"
  src="Report_Design_Guide_files/image108.jpg"></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><span style='font-size:10.0pt'>   <br>
    A grouped flat-table report with two columns in the initial grouping layer</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><b>Grouped Flat-Table</b></p>


<p class=MsoNormal>The grouping flat-table report style is useful for displaying
the entire report in an organized, presentable way.</p>


<p class=MsoNormal>In the following figure, the <i>Customer Name </i>column is
the grouping column and any row that contains a group is highlighted. All
associated rows for a specific group are displayed beneath the grouping row. Aggregated
values for each group are computed and displayed in a separate row.<br>
<br>
</p>


<p class=MsoNormal><img border=0   id="Picture 119"
src="Report_Design_Guide_files/image109.jpg"></p>

<p class=MsoNormal><span style='font-size:10.0pt'>A grouped flat-table report
of customer invoices.</span></p>




<p class=MsoNormal><b>Grouped Drill-Down</b></p>


<p class=MsoNormal>The grouped drill-down report offers viewers the ability to
control the appearance of the report by hiding and showing sub-reports for each
record in the <i>grouping column</i>.</p>


<p class=MsoNormal>In the following figure, the <i>Customer Name </i>column is
the grouping column (always the first column of the main report) and all other
columns appear in the sub-report. Users can show/hide sub-reports for each
group of records by clicking the hyperlink provided in the <i>Details </i>column.
Any additional columns included in the main report are aggregations of a
particular column from the sub-report.</p>

<p class=MsoNormal><br>
<br>
</p>

<p class=MsoNormal><img border=0   id="Picture 120"
src="Report_Design_Guide_files/image110.jpg"></p>

<p class=MsoNormal><span style='font-size:10.0pt'>A drill-down style report of
customer invoices.</span></p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Users can disable drill-down functionality for the
  Details column or hide the column completely.</p>
  </div>
  </td>
 </tr>
</table>

</div>




</body>
</html>
