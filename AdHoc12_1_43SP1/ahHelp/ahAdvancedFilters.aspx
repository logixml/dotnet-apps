<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291601" class="title">Advanced Concepts in Data Filtering</a></h4>


<p class=MsoNormal>Advanced data filtering<i> </i>makes it possible to define
groups of parameters that work together to filter undesirable data out of the
report. Users can define multiple parameters and control the order of
evaluation in order to filter report data to control what end-users see at
runtime.</p>


<p class=MsoNormal>Data filtering gives users the ability to control the
content of the report. Filtering extraneous data from the report is done by
defining one or more parameters that are evaluated at runtime. The directional
pad control (<img   id="Picture 54"
src="Report_Design_Guide_files/image055.gif" alt=paramControlPad>) enables
users to control the order of evaluation.</p>


<p class=MsoNormal>The individual arrows of the control perform the following
functions:</p>


<p class=MsoNormal style='text-indent:.5in'><img  
id="Picture 55" src="Report_Design_Guide_files/image058.gif" alt=paramUp> Shifts
a parameter one position higher in the list (retains indentation)</p>

<p class=MsoNormal style='text-indent:.5in'><img  
id="Picture 56" src="Report_Design_Guide_files/image059.gif" alt=paramDown> Shifts
a parameter one position lower in the list (retains indentation)</p>

<p class=MsoNormal style='text-indent:.5in'><img  
id="Picture 57" src="Report_Design_Guide_files/image060.gif" alt=paramLeft> Indents
a parameter one position left</p>

<p class=MsoNormal style='text-indent:.5in'><img  
id="Picture 58" src="Report_Design_Guide_files/image061.gif" alt=paramRight> Indents
a parameter one position right</p>


<p class=MsoNormal>As parameters are indented to the right, enclosing
parentheses appear to indicate the order of evaluation.</p>


<p class=MsoFooter>Users can also perform a row-level comparison with fields in
two different columns. In this case, the parameter takes the form of an
equation similar to:<br>
<br>
</p>


<p class=MsoFooter align=center style='text-align:center'><i>Label1 </i>is <i>Compared
</i>to <i>Label2</i></p>


<p class=MsoFooter><br>
where <i>Label1</i> represents the first column name, <i>Compared </i>represents
a comparison operator, and <i>Label2</i> represents the second column name.</p>



<p class=MsoNormal>To compare values from two different columns:</p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>From the <i>Column</i> drop-down list, select the desired column.
     </li>
 <li class=MsoNormal>From the <i>Operator</i> drop-down list, select the
     desired comparison operator.</li>
 <li class=MsoNormal>From the <i>Value</i> type drop-down list, select <i>Other
     Data Column</i>.</li>
 <li class=MsoNormal>From the <i>Value</i> field drop-down list, select the
     desired column the comparison will be performed against.</li>
 <li class=MsoNormal>Click <b>OK </b>to add the parameter to the report.</li>
 <li class=MsoNormal>Click the <img   id="Picture 59"
     src="Report_Design_Guide_files/image049.jpg"> icon adjacent to any
     parameter to delete it from the report. Click the <img  
     id="Picture 60" src="Report_Design_Guide_files/image054.gif" alt=mod-icon> icon
     to modify a parameter.</li>
</ol>

<p class=MsoNormal style='margin-left:.25in'>&nbsp;</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>When comparing two different columns, the <i>Ask in
  Report</i> checkbox is disabled.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal style='margin-left:.25in'>&nbsp;</p>

<p class=MsoNormal style='margin-left:.25in'>&nbsp;</p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><b>Using Session Parameters</b></p>


<p class=MsoNormal>If session parameters have been defined within the Ad Hoc
instance, the layout of the Parameters Detail dialog box may be slightly
different:<br>
 </p>


<p class=MsoNormal><img   id="Picture 317"
src="Report_Design_Guide_files/image062.jpg"></p>


<p class=MsoNormal>In the example above, there is a <i>Value</i> source control.
The <i>Value</i> source is typically a  Specific Value ; however, if session
parameters have been defined,  Session Parameter  may be selected as the <i>Value</i>
source. When  Session Parameter  is selected, a drop-down list of relevant
session parameters will be displayed.</p>


<p class=MsoNormal>There are five types of session parameters: date, number,
numeric list, text, and  textual list. The drop-down list of session parameters
will contain the session parameters that match the data type of the <i>Column</i>.
The list is also restricted by the <i>Operator</i> selected.</p>


<p class=MsoNormal>For Date columns, the date session parameters will be shown
in the list of available session parameters.</p>


<p class=MsoNormal>For Numeric columns, either the number or numeric list
session parameters will be shown in the list of available session parameters.
If the <i>Operator</i> is set to  In List  or  Not In List , the numeric list
session parameters will be shown; otherwise the number session parameters will
be shown.</p>


<p class=MsoNormal>For Text columns, either the text or textual list session
parameters will be shown in the list of available session parameters. If the <i>Operator</i>
is set to  In List  or  Not In List , the textual list session parameters will
be shown; otherwise the text session parameters will be shown.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>The ability to select session parameters from a list
  is dependent on whether the session parameter was defined within the Ad Hoc
  interface (see the System Administration Guide for setting session parameters
  within Ad Hoc) or defined outside of the Ad Hoc user interface. An example of
  the latter would be session parameters passed into Ad Hoc from a parent
  application.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>If the session parameter is defined outside of Ad
  Hoc, Ad Hoc would have no knowledge of the data type of the parameter. You
  may still refer to the session parameter by using the @Session token. For
  example, @Session.Country~ would refer to the externally defined session
  parameter.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpLast>Usage of session parameters is discussed in the <a
  href="http://devnet.logianalytics.com/rdPage.aspx?rdReport=Article&amp;dnDocID=1366">Session
  Parameters Guide</a>.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpLast></p>
  </div>
  </td>
 </tr>
</table>

</div>




<p class=MsoNormal><b>Using Cascading Filters</b></p>


<p class=MsoNormal>Cascading filters are a set of related drop-down lists,
where the contents of one list is automatically filtered by the user s
selection in another list. </p>


<p class=MsoNormal>In our example, we ll be using a Country   Region - City set
of cascading filters. Once the user selects the initial Country, the Region drop-down
list will only present options from the selected Country. Similarly, once the
user has selected Country and Region, the City drop-down list will only present
cities found in the Region.</p>


<p class=MsoNormal>A cascading filter must be pre-defined by the System
Administrator before it may be incorporated into a report definition.</p>


<p class=MsoNormal>To incorporate a cascading filter into a report, navigate to
the Filter tab of the <b>Select Data</b> dialog box: </p>

<p class=MsoNormal><img border=0   id="Picture 318"
src="Report_Design_Guide_files/image063.jpg"></p>



<p class=MsoNormal>Click <b>Add Parameter</b> and the following dialog box will
be displayed:</p>




<p class=MsoNormal><img border=0   id="Picture 319"
src="Report_Design_Guide_files/image064.jpg"></p>


<p class=MsoNormal>The starting point for incorporating a cascading filter into
the report is to identify a column that matches the expected final result of a
pre-defined cascading filter. In our example, the ultimate target column is
City.</p>


<p class=MsoNormal>If we select the City column then the list of operators will
be extended to include <i>In cascading list</i> and <i>Not in cascading list</i>:</p>



<p class=MsoNormal><img border=0   id="Picture 79"
src="Report_Design_Guide_files/image065.jpg"></p>



<p class=MsoNormal>If the <i>In cascading list</i> operator is selected, the dialog
box will changed to facilitate specification of which cascading filter to use
and which other columns may be impacted when the filter is displayed in the
report:</p>



<p class=MsoNormal><img border=0   id="Picture 320"
src="Report_Design_Guide_files/image066.jpg"></p>


<p class=MsoNormal>The <i>Default Values</i> attribute shows a list of
pre-defined cascading filters that might be applied to the selected target
column, in this case  City Cascade .</p>


<p class=MsoNormal>The <i>Caption</i> attribute will be the prompt used for the
City column when the report is rendered.</p>


<p class=MsoNormal>The bottom of the dialog box is used to specify whether the
values selected by the customer should be applied to the final filter for the
report. Associate the Data Object and Value Columns with a column in the
recordset by selecting a column from the drop-down list of available columns.
Optionally, provide a caption for the cascading filter level.</p>


<p class=MsoNormal style='margin-left:4.5pt'><span style='font-size:20.0pt;
Wingdings;color:red'>I</span><span style='font-size:20.0pt;
color:red'> </span>In prior versions of Ad Hoc, the only column that was used
to filter the recordset was the last value of the cascading filter. In our
example, had the City selected been  Vienna , the resultant WHERE clause would
have been<span style=''> </span><span
style='font-size:11.0pt;'>WHERE City =  Vienna </span><span
style='font-size:11.0pt'> </span>and the other elements of the cascading filter
would have been ignored. They were only used in the filtering levels of the cascading
filter itself. With v12 of Ad Hoc, all levels of a cascading filter may be
applied to the recordset.</p>


<p class=MsoNormal>Our completed filter dialog box to filter the report on all cascading
filter level values would look like:</p>



<p class=MsoNormal><img border=0   id="Picture 321"
src="Report_Design_Guide_files/image067.jpg"></p>


<p class=MsoNormal>Click <b>OK</b> to save this filter parameter.<br clear=all
style='page-break-before:always'>
</p>


<h1><a name="_Toc456945105"></a><a name="_Toc382291602">Creating Data Columns</a></h1>



<p class=MsoNormal>Ad Hoc gives users the ability to create custom data columns.
Custom data columns can either be a calculation or a computed statistic from
data in other columns. Both types of columns are considered data sources for
the report and may be created through the <i>Select or Modify Data Sources</i> dialog
box.</p>


<p class=MsoNormal>To create either a Calculated Column or Statistical Column, click
the <b>Select Data</b> button to display the <i>Select Data</i> dialog box.
Click either the Calculated Columns or Statistical Columns tabs.</p>



<h2><a name="_Toc456945106"></a><a name="_Toc382291603">Calculated Columns</a></h2>


<p class=MsoNormal><i>Calculated Columns </i>offer the ability to create new
columns for the report based on a specified formula applied to data from
existing columns.</p>


<p class=MsoNormal>In addition to the availability of data source columns,
customized columns can be created that consist of calculations performed on data
from other columns in the report. Calculations are performed with date, numeric
and non-numeric data types. </p>


<p class=MsoNormal>The operands of the formula are either constants or the
names of existing data columns included in the report. Users can create
formulas from the six provided operators, use the predefined functions or
utilize any SQL function supported by the selected report database.</p>


<p class=MsoNormal><img border=0   id="Picture 322"
src="Report_Design_Guide_files/image068.jpg"></p>


<p class=MsoNormal><br>
The goal of the calculated column process is to create and name a <i>Definition</i>
that the reporting database can interpret and return data. Consequently, the <i>Definition</i>
must conform to the reporting DBMS SQL syntax rules for a column.</p>


<p class=MsoNormal>Though they make life easier, you aren t required to use the
helpful controls to create the <i>Definition</i>. Knowledgeable users can
simply enter the definition in the text box. We highly recommend, however, that
columns be inserted into the <i>Definition</i> by clicking on the column from
the data object/column tree. The column reference will be placed at the last cursor
position in the <i>Definition</i> textbox.</p>


<p class=MsoNormal>In the upper left corner of the <i>Calculated Columns</i>
tab are the most common functions that are used in the definition of a
calculated column.</p>


<p class=MsoNormal><img border=0   id="Picture 68"
src="Report_Design_Guide_files/image069.jpg"></p>


<p class=MsoNormal>The functions are categorized according to the generic data
types of columns; date, text, and numeric data. The drop-down lists may be
viewed by hovering the mouse over the buttons. Click the function to insert the
reference into the <i>Definition</i> textbox. For the functions requiring
additional arguments or information, a dialog box will be displayed to complete
the function. For example, the <i>Text/Concatenate</i> function will display
the following dialog box:</p>



<p class=MsoNormal><img border=0   id="Picture 323"
src="Report_Design_Guide_files/image070.jpg"></p>


<p class=MsoNormal>To complete this particular dialog box, click the <i>String1</i>
text box and then click on a column. The column reference will be placed into
the text box. Only the columns relevant to the function type will be displayed
for selection. Repeat the process for <i>String2</i> and click on <b>OK</b> to
post the function into the <i>Definition</i> text box. The other function dialog
boxes behave similarly.</p>


<p class=MsoNormal>On the left side of the <i>Calculated Columns</i> tab of the
<i>Select or Modify Data Source</i> dialog box is the data objects/columns
tree. To place the column reference into the <i>Definition</i>, click the
column.</p>


<p class=MsoNormal>After a calculated column has been defined, the data
objects/columns tree will be refreshed and the calculated column will be added
to the tree along with two management actions: </p>


<p class=MsoNormal>Click the <img border=0   id="Picture 70"
src="Report_Design_Guide_files/image071.gif" alt=iconAction> icon to edit the
calculated column definition. Click the <img border=0  
id="Picture 71" src="Report_Design_Guide_files/image072.gif" alt=TinyRemove> icon
to remove the calculated column.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>Beneath the <i>Definition</i> area of the <i>Calculated
Columns</i> tab are operators that may be added to the <i>Definition</i>:</p>


<p class=MsoNormal><img border=0   id="Picture 72"
src="Report_Design_Guide_files/image073.jpg"></p>


<p class=MsoNormal>Clicking on any of the operators will place the symbol in
the <i>Definition</i> at the last cursor position. </p>


<p class=MsoNormal>The <i>Definition</i> text box is the actual work area. It can
be populated from the functions, columns, and operators available or you can
enter the calculated column definition directly by typing it into the textbox.</p>


<p class=MsoNormal>The <b>Test</b> button will verify that the calculated
column definition meets the syntax rules for the reporting DBMS. If so, a
mini-report displaying the calculated column and the underlying data from the
reporting database will be shown.</p>


<p class=MsoNormal>Ad Hoc must know the data type of the resultant calculated
column. In most cases the data type can be accurately determined from the data
type of the underlying columns or calculation. The <i>Automatic</i> option
allows Ad Hoc to use the implied data type. To provide the specific data type
or override the implied data type, either select the data type from the <i>Type</i>
drop-down list or click the <b>Determine Type</b> button.</p>


<p class=MsoNormal>Enter the <i>Name</i> of the calculated column in the text box
provided.</p>


<p class=MsoNormal>The <b>Preview Selected Data</b> button will display a
report of all of the columns, including the defined calculated column in the <i>Selected
Data Preview</i> dialog box. From this report dialog box, clicking <b>Select
and Continue</b> will save the current calculated column definition and dismiss
all of the dialog boxes.</p>


<p class=MsoNormal>The <b>Save</b> button verifies the <i>Definition</i>,
stores the <i>Name</i> and <i>Definition</i> temporarily, clears the <i>Definition</i>
textbox and updates the data object/column tree.</p>


<p class=MsoNormal>The <b>Clear</b> button erases the contents of the <i>Definition</i>
textbox.</p>


<p class=MsoNormal>The <b>New</b> button clears the contents of the <i>Definition</i>
textbox and resets the Name. If the existing <i>Definition</i> has not been
saved, a confirmation dialog box will be shown.</p>


<p class=MsoNormal>The <b>Cancel</b> button will discard any changes made in
the <i>Select or Modify Data Source</i> dialog box and dismiss the dialog box.</p>


<p class=MsoNormal>Click <b>OK</b> to save all of the changes made in the <i>Select
or Modify Data Source</i> dialog box. The newly defined calculated columns are
then available for selection in the Report Builder.</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>A calculated column must be used in the report for the
  column to be saved in the report definition. Unused calculated columns are
  automatically removed from the report definition when the report is saved.
  This includes intermediate saves of the report definition.</p>
  </div>
  </td>
 </tr>
</table>

</div>




<h2><a name="_Toc456945107">Statistical Columns</a></h2>


<p class=MsoNormal><i>Statistical Columns </i>give users the ability to create
new columns for the report based on a particular statistic from data in other
columns. The following statistical column types are supported with the
application:</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Difference from Previous</li>
 <li class=MsoNormal>Percentile</li>
 <li class=MsoNormal>Rank</li>
 <li class=MsoNormal>Reverse Ran</li>
 <li class=MsoNormal>Running Total</li>
</ul>



<p class=MsoNormal>The <i>Rank</i> function ranks data from the lowest value to
the highest value. For example, a total of <i>$15.00 </i>would receive a higher
rank than a total of <i>$25.00</i>. When one or more data rows have equal
values, the rank value is the same for each row. In the following example, the
first five orders listed all have a rank of 1. Since five orders share a rank
of 1, the next available rank value is 6.</p>


<p class=MsoNormal><img border=0   id="Picture 74"
src="Report_Design_Guide_files/image074.jpg" alt=3></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The <i>Quantity - Rank </i>column
ranks the Quantity column's value from lowest to highest.</span></p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Rank values are never higher than the actual number of
  rows in the report.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<p class=MsoNormal>The <i>Reverse Rank</i> function classifies data from the
highest value to the lowest value; a larger value receives a higher rank. For
example, a total of <i>$25.00 </i>would receive a higher rank than a total of <i>$15.00</i>.
When one or more data rows have equal values, the rank value is the same for
each row. In the following example, the eighth order and subsequent four orders
listed all have a rank of 8. Since five orders share a rank of 8, the next
available rank value is 13.</p>


<p class=MsoNormal><img border=0   id="Picture 75"
src="Report_Design_Guide_files/image075.jpg" alt=3></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The <i>Quantity - Reverse
Rank </i>column ranks data in the <i>Quantity </i>column from highest to
lowest.</span></p>




<p class=MsoNormal>The <i>Percentile</i> function classifies data based on a
percentage of the value distribution. In the following example, a value equal
to or greater than <i>70</i><b> </b>but less than <i>100</i><b> </b>is reported
as the <i>98<sup>th</sup></i><b> </b>percentile.</p>


<p class=MsoNormal><img border=0   id="Picture 76"
src="Report_Design_Guide_files/image076.gif" alt="perc_rank"></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The <i>Quantity Percentile </i>column
ranks data in the <i>Quantity </i>column based on a percentage of the value
distribution.</span></p>


<p class=MsoNormal>The <i>Running Total</i> function maintains a current total
of values provided in the specified column as illustrated in the following
example:</p>


<p class=MsoNormal><img border=0   id="Picture 77"
src="Report_Design_Guide_files/image077.jpg" alt=3></p>

<p class=MsoNormal><span style='font-size:10.0pt'>Example use of Running Total<i>
</i>based on the Freight column</span></p>



<p class=MsoNormal>The <i>Difference from Previous</i> function displays the
difference from the current value and the previous value of a specified column.
In the following figure, a difference of <i>31 </i>is reported from rows one to
two indicating a gain. A difference of <i>-34 </i>is reported from rows two to
three indicating a loss.</p>


<p class=MsoNormal><img border=0   id="Picture 78"
src="Report_Design_Guide_files/image078.gif" alt=diff></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The <i>Quantity Difference </i>column
maintains the difference from the current value and the previous value of the <i>Quantity
</i>column.</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><img border=0   id="Picture 325"
src="Report_Design_Guide_files/image079.jpg"></p>


<p class=MsoNormal>To create a statistical column:</p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Choose a
function from the <b>Function Type</b> drop-down menu.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Select a
column from the <i>Columns</i> list - the function is applied to the column's
value.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>OPTIONAL:
Modify the default <b>Name</b> in the field provided.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>OK</b>
to make the newly created statistical column available for selection in the
Report Builder.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>When the OK button is clicked the column's name is
  validated for SQL syntax accuracy. If the name is invalid, then a message
  will appear.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>If a statistical column is not used in the report,
  then it shall be deleted when exiting the Report Builder. Basically, use it
  or lose it.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<h1><a name="_Toc382291605"><br>
</a><a name="_Toc456945108">Tables</a></h1>



<p class=MsoNormal>A <i>table </i>(also referred to as a  display table , to
avoid confusion) presents data in a tabular style report. A column is created
for every data column that is included in the report, and a row is created for
every value in that data column. The Report Builder provides configuration
pages to customize the table's select columns, appearance, group data and
paging options.</p>


<p class=MsoNormal>Four configuration tabs are displayed in the Report Builder
for each display table; <i>Table Columns,</i> <i>Column Configuration</i>, <i>Grouping,</i>
and <i>Table Settings</i>.</p>


<p class=MsoNormal>The <i>Table Columns</i> tab allows you to determine which
columns are included in the final report.</p>


<p class=MsoNormal><i>The Column Configuration</i> tab allows you to determine
the display characteristics for each column and define column aggregations.</p>


<p class=MsoNormal>The<i> Grouping</i> tab allows you to defined data grouping
levels, the display style (flat or drill), and group level aggregations.</p>


<p class=MsoNormal>The <i>Table Settings</i> tab allows you to configure the display
table presentation characteristics such as a title, rows per page and the
location of summary information.</p>



<h2><a name="_Toc456945109"></a><a name="_Toc382291606">Configuring Table Columns</a></h2>


<p class=MsoNormal>The first step of table configuration is to select the
columns to be used in the display table. If the report contains more than one
display table, the configuration area will reflect the currently selected
display table.</p>


<p class=MsoNormal>To select columns for the table:</p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>Click the <i>Table Columns </i>tab.</li>
</ol>


<p class=MsoNormal><img border=0   id="Picture 80"
src="Report_Design_Guide_files/image080.jpg"></p>


<ol style='margin-top:0in' start=2 type=1>
 <li class=MsoNormal>Select one or more<i> Available Columns</i> and then click
     the <img border=0   id="Picture 81"
     src="Report_Design_Guide_files/image081.jpg"> icon to add the column(s) to
     the<i> Assigned Columns</i> list. Hold the Ctrl key down to select
     multiple columns.<br>
     <br>
 </li>
 <li class=MsoNormal>From the <i>Assigned Columns</i> list, change a column's
     initial display order by clicking either the <img border=0 
      id="Picture 82" src="Report_Design_Guide_files/image082.jpg"> or
     <img border=0   id="Picture 83"
     src="Report_Design_Guide_files/image083.jpg"> icon to move the row up or
     down. Hold the Ctrl key down to select multiple columns.<br>
     <br>
 </li>
 <li class=MsoNormal>To remove a column from the <i>Assigned Columns</i> list,
     select it and then click the <img border=0  
     id="Picture 84" src="Report_Design_Guide_files/image084.jpg"> icon. Hold
     the Ctrl key down to select multiple columns.</li>
</ol>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>At least one data column should be selected before
  continuing.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>A column description will only be available if one has
  been specified by the System Administrator.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<span style='font-size:6.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>To configure and delete columns in the table:</p>


<p class=MsoNormal>After adding them, each column's order of appearance,
display characteristics, and summary information may be defined on the <i>Column
Configuration</i> tab:</p>



<p class=MsoNormal><img border=0   id="Picture 85"
src="Report_Design_Guide_files/image085.jpg"></p>


<p class=MsoNormal>When first displayed, only the Column, Header and Sortable
columns are shown in the configuration grid. Click the <i>Show All Attributes</i>
icon to expand the grid so that it looks like the example above. The grid can
be collapsed again by clicking the <i>Show Minimum Attributes</i> icon.</p>


<p class=MsoNormal>Data columns can be selected (or deselected) by checking their
checkbox. All data columns can be toggled at once by clicking the checkbox in
the upper-left corner of the grid. Some of the following functions apply to
selected columns.</p>


<p class=MsoNormal>Grid rows can be rearranged by selecting the row and then
clicking the <img border=0   id="Picture 86"
src="Report_Design_Guide_files/image086.jpg"> or <img border=0 
height=24 id="Picture 87" src="Report_Design_Guide_files/image087.jpg"> icon to
move the row up or down. To move a group of rows, select the desired columns by
checking their respective checkboxes and then clicking either the <img
border=0   id="Picture 88"
src="Report_Design_Guide_files/image086.jpg"> or <img border=0 
height=24 id="Picture 89" src="Report_Design_Guide_files/image087.jpg"> icon to
move the set of rows up or down.</p>


<p class=MsoNormal>Data columns can be removed from the display table by
selecting them and clicking the <img border=0   id="Picture 90"
src="Report_Design_Guide_files/image088.gif" alt=remove>icon.</p>


<b><span style='font-size:10.0pt;'><br
clear=all style='page-break-before:always'>
</span></b>


<p class=MsoNormal>There are nine configurable options available in Column
Configuration:</p>


<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=280 valign=top style='width:167.8pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Header</p>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Linkable</p>
  <p class=MsoListParagraphCxSpLast style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Sortable</p>
  </td>
  <td width=280 valign=top style='width:167.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Summary</p>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Format</p>
  <p class=MsoListParagraphCxSpLast style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Width</p>
  </td>
  <td width=280 valign=top style='width:167.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Alignment</p>
  <p class=MsoListParagraphCxSpLast style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Style</p>
  </td>
 </tr>
</table>


<p class=MsoNormal>The <i>Header</i> determines the column header displayed
when the report is rendered.</p>


<p class=MsoNormal>The <i>Linkable</i> option toggles pre-defined hyperlinks
for records in the column. Each record in the <i>Linkable</i> column contains a
hyperlink to an address specified by the System Administrator. Make column
records linkable from the report by placing a check in the corresponding <i>Linkable</i>
checkbox.</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>The Linkable column will only be shown if the System
  Administrator has configured at least one of the columns as  hyperlink
  capable .</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>The <i>Sortable</i> option determines whether the column in
the rendered report can be sorted by the end-user. Sorting can be enabled or
disabled for all columns by checking the checkbox in the header of the <i>Sortable</i>
column.</p>


<p class=MsoNormal>The <i>Summary</i> option offers the ability to create table
footers containing aggregates of values for each column of data. An unlimited
number of aggregates can be created for each table column. The following
aggregate functions are supported:</p>


<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=280 valign=top style='width:167.8pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Average</p>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Calculation</p>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Count</p>
  </td>
  <td width=280 valign=top style='width:167.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Count Distinct</p>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Maximum</p>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Minimum</p>
  </td>
  <td width=280 valign=top style='width:167.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Standard Deviation</p>
  <p class=MsoListParagraphCxSpLast style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Sum</p>
  </td>
 </tr>
</table>




<p class=MsoNormal>To manage summary values:</p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <img
border=0 width=15 height=14 id="Picture 91"
src="Report_Design_Guide_files/image089.gif" alt=sumIcon1> to display the <i>Aggregates</i>
dialog box for a specific column. <br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>Add
an Aggregate</b> and the following dialog box will be displayed:</p>

<p class=MsoNormal><br>
<br>
</p>


<p class=MsoNormal><img border=0   id="Picture 328"
src="Report_Design_Guide_files/image090.jpg"><br>
<br>
</p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Type a
name used as the internal value for the aggregate in the <i>Name</i> field.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Type a displayed
name for the new value in the <i>Label</i> field.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Choose an <i>Aggregate</i>
function from the drop-down list.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>6.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Choose a <i>Format</i>
from the drop-down list.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>7.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>OPTIONAL:
If more than one aggregate has been specified, click the <img border=0
  id="Picture 93" src="Report_Design_Guide_files/image091.gif"
alt=arrowUp> or <img border=0   id="Picture 94"
src="Report_Design_Guide_files/image092.gif" alt=arrowDown> icon to arrange the
order in which the aggregate will appear in the column. <br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>8.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Continue
adding additional aggregates or click <b>OK</b> to add the summary value(s) and
return to the <i>Column Configuration</i> interface.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>9.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Aggregates
can be removed by clicking the <img border=0   id="Picture 95"
src="Report_Design_Guide_files/image088.gif" alt=remove>icon.</p>




<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>An <img border=0   id="Picture 96"
  src="Report_Design_Guide_files/image093.gif" alt=sumIcon2> icon is displayed
  to indicate that a summary value exists for a particular column.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>One of the aggregation options is  Calculation . This option
allows the user to create a new aggregation from previously defined summary
information. When  Calculation  is selected, the following dialog box is
displayed: </p>


<p class=MsoNormal><img border=0   id="Picture 332"
src="Report_Design_Guide_files/image094.jpg"></p>


<p class=MsoNormal><br>
In the Modify Calculation dialog box, any of the previously defined summaries
may be used in the calculation as well as aggregate functions on columns and
direct constants in the definition. The default internal name for an
aggregation is AGGR<i>n</i>, where <i>n</i> is a unique number.</p>


<p class=MsoNormal><br>
The <i>Format</i> option provides data formatting options for values in each
column. The following formatting options are supported:</p>



<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=420 valign=top style='width:251.75pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>(none)</li>
   <li class=MsoNormal>General Number</li>
   <li class=MsoNormal>Currency</li>
   <li class=MsoNormal>Integer</li>
   <li class=MsoNormal>Fixed</li>
   <li class=MsoNormal>Standard</li>
   <li class=MsoNormal>Percent</li>
   <li class=MsoNormal>Scientific</li>
   <li class=MsoNormal>2- or 3-digit place holder</li>
  </ul>
  </td>
  <td width=420 valign=top style='width:251.75pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>General Date</li>
   <li class=MsoNormal>Long/Medium/Short Date</li>
   <li class=MsoNormal>Long/Medium/Short Time</li>
   <li class=MsoNormal>Yes/No</li>
   <li class=MsoNormal>True/False</li>
   <li class=MsoNormal>On/Off</li>
   <li class=MsoNormal>HTML<sup>1</sup></li>
   <li class=MsoNormal>Preserve line feed<sup>2</sup></li>
  </ul>
  </td>
 </tr>
</table>






<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>The application chooses the default format type for
  each column. Changing the format type may yield undesirable results.<br>
  <br>
  If a format is needed that isn t offered, contact your System Administrator.
  Additional formats may be provided by the System Administrator<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>1. The Format &quot;HTML&quot; can only be specified
  by the System Administrator and may not be changed to something different
  from the Column Configuration panel.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>2. The Format  Preserve line feed  allows text in a
  memo type field to display as it is stored with line feeds (carriage returns)
  observed.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>The <i>Width</i> option offers the ability to customize the
width of each column, improving the appearance of the report when it is
rendered in the web page and when exported. </p>


<p class=MsoNormal>By default, a column's <i>Width</i> value is blank to allow the
application to  automatically determine the appropriate width based on:</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>All columns in the report</li>
 <li class=MsoNormal>The context in each column</li>
 <li class=MsoNormal>The available webpage space</li>
 <li class=MsoNormal>The page size and orientation</li>
</ul>


<p class=MsoNormal>When customizing a column's width, it s important to
determine the width scale. The scale of measured can be either <i>pixels</i> or
<i>percent</i>, where: <br>
<br>
</p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><i>Pixel</i> is a single point of graphic data displayed
     on the monitor. Hundreds of pixels can be used to display a very small
     image. </li>
 <li class=MsoNormal><i>Percentage</i> is a fraction of the screen space allocated
     for each column.<br>
     <br>
 </li>
</ul>


<p class=MsoNormal>To modify a column's width:</p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>OPTIONAL: Determine the scale Type to use for the entire
     tabular report. To toggle the scale of a column, click the label to the
     right on the width field and select <i>px</i> for pixels and <i>%</i> for percentage.</li>
 <li class=MsoNormal>Enter or modify the column's Width value.</li>
</ol>







<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>When specifying a tabular report's column's widths
  in pixels, keep in mind the average monitor resolution settings of the
  end-users viewing the report.<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>When specifying a tabular report's column's widths
  in percentages, keep in mind that:</p>
  <p class=NotesCxSpMiddle>a. The sum total of the percentage values must not
  exceed 100%.</p>
  <p class=NotesCxSpLast>b. The application will use whatever percentage has
  not been allocated to columns for the columns without a value.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>




<p class=MsoNormal>The <i>Alignment</i> option adjusts the position of values
in columns. In the following example, the alignment of numerical values is
changed from Left to Center in the <i>Quantity</i> column<br>
.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse'>
 <tr style='height:28.35pt'>
  <td width=246 style='width:2.05in;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoNormal align=center style='text-align:center'><img border=0
    id="Picture 98"
  src="Report_Design_Guide_files/image095.jpg" alt="alignment_before"></p>
  </td>
  <td width=246 style='width:2.05in;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoNormal align=center style='text-align:center'><img border=0
    id="Picture 99"
  src="Report_Design_Guide_files/image096.gif" alt=arrow></p>
  </td>
  <td width=246 style='width:2.05in;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoNormal align=center style='text-align:center'><img border=0
    id="Picture 100"
  src="Report_Design_Guide_files/image097.jpg" alt="alignment_after"></p>
  </td>
 </tr>
</table>


<p class=MsoNormal>The <i>Style</i> option<i> </i>offers the ability to apply
conditional formatting to a column's cells based on a specific value or another
column's value. Users must create the condition and specify the formatting
style. When more than one condition is specified, the application will apply
the style associated to the first condition that is satisfied. Conditions are
evaluated when the report is run.</p>


<p class=MsoNormal>Users can optionally apply the specified conditional
formatting to all data columns by checking the appropriate checkbox.</p>


<p class=MsoNormal><img border=0   id="Picture 101"
src="Report_Design_Guide_files/image098.gif" alt=condFormat></p>


<p class=MsoNormal>A conditional style takes the form of an equation similar
to:</p>


<p class=MsoFooter align=center style='text-align:center'><i>Label </i>is <i>Compared
</i>to <i>Value<br>
 </i></p>

<p class=MsoFooter align=center style='text-align:center'>Or<br>
<br>
</p>

<p class=MsoFooter align=center style='text-align:center'><i>Label</i> is <i>Compared</i>
to <i>Column</i></p>


<p class=MsoFooter>where <i>Label</i> represents a column name, <i>Compared</i>
represents a comparison operator, <i>Value</i> represents a threshold, and <i>Column</i>
represents another data column.</p>


<p class=MsoFooter>The available comparison operators are:</p>



<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=420 valign=top style='width:251.75pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>Equal to</li>
   <li class=MsoNormal>Not equal to</li>
   <li class=MsoNormal>Less than</li>
   <li class=MsoNormal>Greater than</li>
   <li class=MsoNormal>Less than or equal to</li>
   <li class=MsoNormal>Greater than or equal to</li>
   <li class=MsoNormal>Starts with <sup>1</sup></li>
  </ul>
  </td>
  <td width=420 valign=top style='width:251.75pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>Does not start with <sup>1</sup></li>
   <li class=MsoNormal>Ends with <sup>1</sup></li>
   <li class=MsoNormal>Does not end with <sup>1</sup></li>
   <li class=MsoNormal>Contains <sup>1</sup></li>
   <li class=MsoNormal>Does not end with <sup>1</sup></li>
   <li class=MsoNormal>Contains <sup>1</sup></li>
   <li class=MsoNormal>Not between</li>
  </ul>
  </td>
 </tr>
</table>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>1. These operators are only available for data type
  of type String or Text.</p>
  <p class=NotesCxSpLast>2. The operators available are dependent upon the
  column's data type.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>To add a conditional Style:</p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <img
border=0 width=15 height=15 id="Picture 102"
src="Report_Design_Guide_files/image099.gif" alt="style_icon"> or <i><img
border=0 width=15 height=15 id="Picture 103"
src="Report_Design_Guide_files/image100.gif" alt="style_icon2"> </i>to access
the Condition Styles dialog box. In the example below, two styles have been
added and the Add a Condition panel opened to show the various options in the dialog
box.</p>


<p class=MsoNormal><img border=0   id="Picture 337"
src="Report_Design_Guide_files/image101.jpg"><br>
<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>Add
a Condition</b>. In the panel are the <i>Column</i>, <i>Operator</i>, <i>Value </i>and
<i>Style </i>attributes.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Choose a <i>Column</i>
to base the conditional styling on from the drop-down list.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Choose a
comparison <i>Operator</i> from the drop-down list.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>From the <i>Value</i>
type drop-down list, choose either <i>Specific Value,</i> <i>Pre-defined Date</i>,
or <i>Other Data Column</i>.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>6.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Specify a threshold
Value. If a Value type of:<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span><i>Specific
Value </i>was selected, then type in a value or click the <img border=0
  id="Picture 105" src="Report_Design_Guide_files/image053.gif"
alt=iconFind> icon to view and select a valid value from the database.</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span><i>Pre-defined
Date </i>was selected, select a token (i.e., Today).</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>c.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span><i>Other
Data Column</i> was selected, select a column from the drop-down list.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>7.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Choose a <i>Style</i>
from the drop-down list.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>8.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>OK</b>
to add the styling condition.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>9.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Add more
parameters by clicking <b>Add a Condition</b> and repeating the steps above.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>10.<span
style='font:7.0pt "Times New Roman"'> </span>OPTIONAL: Because styles are
applied based on the first condition that is satisfied, the order may be
significant. You can move a condition up or down in the list by clicking the <img
border=0   id="Picture 106"
src="Report_Design_Guide_files/image091.gif" alt=arrowUp> or <img border=0
width=15 height=9 id="Picture 107" src="Report_Design_Guide_files/image092.gif"
alt=arrowDown> icon.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>11.<span
style='font:7.0pt "Times New Roman"'> </span>OPTIONAL: Check <b><i>Add this
style to all columns</i></b> to apply the Style to all columns of a row in the
table.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>12.<span
style='font:7.0pt "Times New Roman"'> </span>Click <b>OK</b> to save the styling
condition(s) and return to the Column Configuration interface.<br>
<br>
</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>Pre-defined dates get evaluated at the time the
  report runs.<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>If the value is a number, the value field must
  contain a valid number to complete the comparison.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpLast>The <i><img border=0  
  id="Picture 108" src="Report_Design_Guide_files/image100.gif"
  alt="style_icon2"></i>  icon indicates that a least one conditional style
  exists for that particular column.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>




<p class=MsoNormal>To modify or remove a conditional Style:</p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click the <img
border=0 width=15 height=15 id="Picture 109"
src="Report_Design_Guide_files/image100.gif" alt="style_icon2"> icon to access
the Style Details panel for a specific column.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>From the
Style Details panel:<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>If modifying
a Style, click the <img border=0   id="Picture 110"
src="Report_Design_Guide_files/image054.gif" alt=mod-icon> icon associated to a
specific Style's condition. Modify the conditions as desired and then click <b>OK</b><span
style='font-size:10.0pt'> </span>to save the condition.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>If
removing a Style, click the <img border=0   id="Picture 111"
src="Report_Design_Guide_files/image088.gif" alt=remove> icon associated to a
specific Style's condition.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>c.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>OPTIONAL: Check
or uncheck <b><i>Add this style to all columns</i></b> to apply/remove the
Style to/from all columns in the table.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>OK</b>
to save the modifications and return to the Column Configuration interface.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>

<p class=MsoFooter><b>Adding a new custom column to the data table</b></p>


<p class=MsoFooter>From the Column Configuration page, a new column can be
added to the data table by clicking <b>Add Custom Column</b>. Columns created
in this manner can reference previously defined summary information. </p>


<p class=MsoFooter>In the example below, a total column (OrderTotal) has been
created using the<i> Quantity</i> column and the <i>Saleprice</i> calculated
column.</p>

<p class=MsoFooter><br>
<br>
</p>

<p class=MsoFooter><img border=0   id="Picture 339"
src="Report_Design_Guide_files/image102.jpg"></p>


<p class=MsoFooter>The sequence of events for this example was:</p>


<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click the <b>Add
Calculated Column</b> button on the <i>Column Configuration</i> page.<br>
<br>
</p>

<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click the <i>Saleprice</i>
column from the <i>Calculate Columns</i> data object.</p>

<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click the multiplication
symbol from the list of operators.</p>

<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click the <i>Quantity</i>
column from the OrderDetails object.</p>

<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>OK</b>
to save the result</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b> <br>
  This type of calculated column may use reporting summary information in the
  calculated column definition.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>If a data table contains a calculated column, the
  configuration grid will display an <b>Actions</b> column. Clicking on the <img
  border=0   id="Picture 113"
  src="Report_Design_Guide_files/image103.jpg"> icon allows the column to be
  edited.<br clear=all style='page-break-before:always'>
  </p>
  </div>
  </td>
 </tr>
</table>

</div>

</body>
</html>
