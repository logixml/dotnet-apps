<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221792"></a><a name="_Toc457221156">Scheduled Reports</a></h2>


<p class=MsoNormal><span style=''>The <i>Scheduled
Reports</i> page allows System Administrators to view, modify and delete any schedule(s)
created for an individual report. Additionally, the administrator can create
additional schedules against any already scheduled report, subscribe users to
receive the scheduled reports via email, and archive scheduled reports.</span></p>


<p class=MsoNormal><span style=''>The
scheduling process is flexible and easy to use, offering the ability to deliver
reports as  an email in HTML format, as an email attachment in PDF, Word,
Excel, or CSV format, or as a link to the report in the archive directory on
the server.</span></p>


<p class=MsoNormal><span style=''>The
scheduling process is described in detail in the <i>Report Builder Guide</i>.
The primary purpose of the <i>Configuration </i></span><i><span
style='Wingdings'> </span></i><i><span style=''>
Report Configuration </span></i><i><span style='Wingdings'> </span></i><i><span
style=''> Scheduled Reports</span></i><span
style=''> page is to centrally manage existing
schedules. Creating a new schedule is typically implemented using the Report
Builder.</span></p>


<p class=MsoNormal><span style=''>Select <b>Scheduled
Reports</b> from the <i>Report Configuration</i> drop-down list<b> </b>to
display the <i>Scheduled Reports</i> configuration page:</span></p>


<p class=MsoNormal><span style=''><img
border=0   id="Picture 136"
src="System_Admin_Guide_files/image092.jpg"></span></p>



<p class=MsoNormal><span style=''>The <b>Delete</b>
button will remove the schedules for the selected report. Reports are selected
by clicking the applicable checkbox.</span></p>

<p class=MsoNormal><span style=''> </span></p>

<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Reports themselves are <i>not</i> deleted. Only the
  schedules for the report will be deleted.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<p class=MsoNormal><span style=''>A search
control may be displayed for the <i>Scheduled Reports</i> page. Enter the
search criteria in the textbox and click <b>Find Scheduled Reports</b>. The
search will find reports based on a  contains  test. To clear the search
criteria click the </span><img border=0   id="Picture 363"
src="System_Admin_Guide_files/image088.gif"><span style=''> icon</span><span
style=''> in the text box. The availability of
the search feature is configurable.</span></p>


<p class=MsoNormal><span style=''>The list of
scheduled reports can be sorted by clicking on either the <i>Name</i> or <i>Folder</i>
column header.</span></p>


<p class=MsoNormal><span style=''>Click the
report <i>Name</i> link to run the report.</span></p>


<p class=MsoNormal><span style=''>Click the <i>Folder</i>
link to navigate to the folder containing the report.</span></p>


<p class=MsoNormal><span style=''>Click the <i>Schedules</i>
(count) link to view the schedules for the report. From that page, the administrator
may create new schedules for the report, remove schedules, and modify schedule
information.</span></p>




</body>
</html>
