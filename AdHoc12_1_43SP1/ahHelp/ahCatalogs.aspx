<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221770"></a><a name="_Toc457221134">Catalogs</a></h2>


<p class=MsoNormal><span style=''>A <i>catalog</i>
is a predefined collection of related data objects and columns. Catalogs are
used to simplify data object selection for the end-user.</span></p>


<p class=MsoNormal><span style=''>Essentially
the System Administrator builds a catalog the same way an end-user would select
the data objects and columns for a report. A primary data object is selected
along with all of the related data objects. This identifies a pool of columns
that can be used in the report.</span></p>


<p class=MsoNormal><span style=''>When the
System Administrator has identified all of the data objects and columns, the
result can be saved as a catalog. A catalog is displayed to the end-user in the
list of data objects they may use for reporting.</span></p>


<p class=MsoNormal><span style=''>The catalog
definition has two additional features. The System Administrator can eliminate
duplicate or ambiguous column references (e.g. columns linking two data
objects) and control the column name and friendly name.</span></p>


<p class=MsoNormal><span style=''>Select <b>Catalogs</b>
from the <i>Database Configuration</i> drop-down list<b> </b>to display the <i>Catalogs</i>
configuration page:</span></p>



<p class=MsoNormal><img  
src="System_Admin_Guide_files/image068.jpg" align=left hspace=12><br clear=all>
</p>


<p class=MsoNormal><span style=''>The <b>Database</b>
drop-down list acts as a filter for the Catalog list. Only Catalogs related to
the selected database will be displayed. If only one reporting database has
been configured for the Ad Hoc instance, the <b>Database</b> filter will not be
shown.</span></p>


<p class=MsoNormal><span style=''>Click <b>Add</b>
to display an empty <i>Catalog</i> page.</span></p>


<p class=MsoNormal><span style=''>Click <b>Delete</b>
to remove the selected catalogs. Catalogs are selected by checking their checkboxes.</span></p>


<p class=MsoNormal><span style=''>Two actions
are available for a Catalog: <b>Modify Catalog </b>and<b> Delete Catalog. </b>Hover
your mouse cursor over the </span><span style=''><img
border=0   id="Picture 94"
src="System_Admin_Guide_files/image026.jpg"></span><span style='
"Arial","sans-serif"'> icon to show them.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>A catalog is a data object and additional
  configuration options, similar to those of other data objects, are available
  for existing catalogs through the Database Configuration <span
  style='Wingdings'> </span> Data Objects pages. However, an
  important difference is that relationships cannot be created between a
  catalog and other data objects.</p>
  </div>
  </td>
 </tr>
</table>

</div>



<h3><a name="_Toc457221771"></a><a name="_Toc457221135">Adding a Catalog</a></h3>

<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<p class=MsoNormal><span style=''>Creating a
catalog requires selecting a catalog name, identifying the related data
objects, and selecting the columns displayed to the end-user. Optionally, the
column names and friendly names can be specified as well as the friendly name
of the catalog.</span></p>


<p class=MsoNormal><span style=''>To create a
catalog, click <b>Add</b> to display the following page:</span></p>

<p class=MsoNormal><span style=''><br>
<br>
</span></p>

<p class=MsoNormal><img border=0   id="Picture 63"
src="System_Admin_Guide_files/image069.jpg"></p>

<p class=MsoNormal><span style=''>Click the  </span><span
style=''><img border=0  
id="Picture 96" src="System_Admin_Guide_files/image070.gif" alt=Help></span><span
style=''> icon to display brief help for the <i>Catalog</i>
page.</span></p>


<p class=MsoNormal><span style=''>The <i>Database
Connection</i> identifies the currently selected reporting database. The list
of data objects is restricted to this reporting database schema.</span></p>


<p class=MsoNormal><span style=''>Enter a
unique <i>Catalog Name</i> and <i>Friendly Name</i> for the catalog.</span></p>


<p class=MsoNormal><span style=''>If Categories
of data objects have been defined, a drop-down list of the categories will be
shown in the <i>Data Objects in</i> list. Selecting a category will act as
filter on the initial list of data objects displayed.</span></p>


<p class=MsoNormal><span style=''>Identify the
main data object for the catalog by clicking on the checkbox adjacent to the data
object. The data object tree can be expanded to show columns for each data
object by clicking  the  +  icon.</span></p>


<p class=MsoNormal><span style=''>Once a data
object has been selected, the list of <i>Available Objects</i> will be
populated and visible. Continue selecting the related data objects until the
basic catalog content has been satisfied. The <i>Catalog</i> page will appear
as:</span></p>


<p class=MsoNormal><span style=''><br>
</span><span style=''><img border=0 
 id="Picture 98" src="System_Admin_Guide_files/image071.jpg"></span></p>


<p class=MsoNormal><span style=''>From the <i>Available
Column</i> list, select the columns to be included in the catalog by cvhecking
their checkboxes clicking the <b>right-arrow</b> icon. All columns can be selected
by checking the checkbox in the header of the <i>Available Columns</i> list.</span></p>


<p class=MsoNormal><span style=''>The System
Administrator now has the opportunity to manually adjust the <i>Column Name</i>
and <i>Friendly Name</i> for each column in the <i>Catalog Columns</i> grid.
Columns can be removed from the catalog by selecting the column with the
adjacent checkbox and clicking the <b>left-arrow</b> icon.</span></p>


<p class=MsoNormal><i><span style=''>Column
Names</span></i><span style=''> must be unique.</span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the catalog information.</span></p>



<h3><a name="_Toc457221772"></a><a name="_Toc457221136">Adding a Catalog Using
 Save As </a></h3>


<p class=MsoNormal><span style=''>The
administrator can create a new catalog by modifying an existing catalog and
then saving it. Click <b>Save As</b> and specify the new <i>Catalog Name</i>
and <i>Friendly Name</i>.</span></p>




</body>
</html>
