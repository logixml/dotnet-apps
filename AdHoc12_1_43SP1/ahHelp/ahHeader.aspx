<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945119"></a><a name="_Toc382291615">Report Header</a></h2>


<p class=MsoNormal>By default, a report header is included in the standard
report template with the Date and Time options enabled:</p>



<p class=MsoNormal><img border=0   id="Picture 206"
src="Report_Design_Guide_files/image166.jpg"></p>

<p class=MsoNormal><span style='font-size:10.0pt'>Default report header</span></p>



<p class=MsoNormal>Click the Header icon from the ADD frame to add a Header to
a report. A Header tab will be created. A report can only have one Header
element.</p>



<p class=MsoNormal><b><span style='font-size:10.0pt'><img border=0 
 id="Picture 208" src="Report_Design_Guide_files/image167.jpg"></span></b></p>

<p class=MsoNormal><span style='font-size:10.0pt'>Header Information tab with
Date and Time options enabled.</span></p>




<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst>Notes:</p>
  <p class=NotesCxSpLast>If a Header is added without the Date and Time options
  enabled, then only a report name will appear in the header.</p>
  </div>
  </td>
 </tr>
</table>

</div>



<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
