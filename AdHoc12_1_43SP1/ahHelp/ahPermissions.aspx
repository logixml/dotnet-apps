<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221755"></a><a name="_Toc457221119">Permissions</a></h2>


<p class=MsoNormal><span style=''>Permissions
are collections of related  rights . Each right identifies a feature or function
in Ad Hoc. There are over 75 rights that can be bundled into a permission, also
known as a  permission package .</span></p>


<p class=MsoNormal><span style=''>One or more
permissions are assigned to roles. Roles are then assigned to users.</span></p>


<p class=MsoNormal><span style=''>The Permissions
page allows the System Administrator to create and manage permissions in the
application.</span></p>


<p class=MsoNormal><span style=''>Select <b>Permissions</b>
from the <i>User Configuration</i> drop-down list<b> </b>to display the <i>Permissions</i>
configuration page:</span></p>



<p class=MsoNormal><span style=''><img
border=0   id="Picture 45"
src="System_Admin_Guide_files/image040.jpg"></span></p>


<p class=MsoNormal><span style=''><br>
Ad Hoc is distributed with 11 of the most common permission packages
pre-defined. All but the System Administration permission package can be edited
and deleted.</span></p>


<p class=MsoNormal><span style=''>Click <b>Add</b>
to display an empty <i>Permissions</i> page.</span></p>


<p class=MsoNormal><span style=''>Click <b>Delete</b>
to remove the selected permission. Permissions are selected by checking their checkboxes.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>The  System Administration  permission package
  contains all rights and may not be altered or deleted.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpLast>If the permission is assigned to any roles, that
  permission is removed from those roles when it is deleted.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal><span style=''>Two actions
are available for a Permission: <b>Modify Permission </b>and<b> Delete Permission.
</b>Hover your mouse cursor over the </span><span style=''><img
border=0   id="Picture 46"
src="System_Admin_Guide_files/image026.jpg"></span><span style='
"Arial","sans-serif"'> icon to show the available actions.</span></p>



<p class=MsoNormal><b><span style=''>Adding a
Permission</span></b></p>


<p class=MsoNormal><span style=''>To add a
permission, click the <b>Add</b> button and the permission page will be displayed:</span></p>


<p class=MsoNormal><span style=''><img
border=0   id="Picture 47"
src="System_Admin_Guide_files/image041.jpg"></span></p>


<p class=MsoNormal><span style=''><br>
A <i>Permission Name</i> value is required and must be unique. The <i>Description
</i>value is optional. Assign rights to the permission from the list of
Available Rights.</span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the new Permission information. </span></p>

<p class=MsoNormal><span style=''><br>
<br>
</span></p>

<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<p class=MsoNormal><b><span style=''>Adding a
Permission Using  Save As </span></b></p>


<p class=MsoNormal><span style=''>The System Administrator
can also create a permission by modifying an existing permission and saving it
as a new permission by clicking the <b>Save As</b> button. </span></p>


<h5 align=left style='text-align:left'><i><span style='font-weight:normal'><span
 style='text-decoration:none'>&nbsp;</span></span></i></h5>

<h5 align=left style='text-align:left'><span style='text-decoration:none'>Modifying
Users</span></h5>


<p class=MsoNormal><span style=''>To change
permission information, click the <b>Modify Permission</b> action for the permission
package.  The <i>Modify</i> <i>Permissions</i> page will be displayed:</span></p>


<p class=MsoNormal><span style=''><img
border=0   id="Picture 48"
src="System_Admin_Guide_files/image042.jpg"></span></p>



<p class=MsoNormal><span style=''>Modify the appropriate
information and click <b>Save</b> to save the information.</span></p>


<p class=MsoNormal><span style=''>To save the
edited information using a new <i>Permission Name</i>, click <b>Save As</b>,
enter the new name, and click <b>OK</b> to save the information.</span></p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>The Selected Permission drop-down list can be used to
  navigate through permission packages and avoid using the Back to Permissions
  button and selecting a different permission.</p>
  </div>
  </td>
 </tr>
</table>

</div>




</body>
</html>
