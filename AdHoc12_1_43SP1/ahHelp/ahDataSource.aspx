<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945102"></a><a name="_Toc382291598">Selecting a Data Source</a></h2>


<p class=MsoNormal>Generally the first step in building a report is the
selection of the data source. The <i>Report Builder</i> automatically takes you
to the <i>Select Data </i>dialog box as part of creating a new report. The same
dialog box is displayed when <b>Select Data</b> is clicked:</p>



<p class=MsoNormal><img   id="Picture 308"
src="Report_Design_Guide_files/image045.jpg"></p>


<p class=MsoNormal><br>
If the data objects have been categorized, a <i>Data Object in</i> drop-down
list will be displayed and acts as a filter for the list of data objects. The
list of data objects can be changed by selecting a different category from the
list. All data objects can be displayed by selecting the  (All)  option in the
list.</p>

<p class=MsoNormal>If you have access to multiple reporting databases and the
 (All)  option was selected on the Database filter, the <b>Select Data</b> dialog
box allows you to select data objects from any of the reporting databases.</p>



<p class=MsoNormal><img   id="Picture 309"
src="Report_Design_Guide_files/image046.jpg"></p>


<p class=MsoNormal>Notice in the above image that the Database filter is set to
 (All) . As a result, the tree of data objects includes two databases;  Northwind 
and  Reporting Metadata . </p>

<p class=MsoNormal><br>
<br>
</p>

<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Presentation of all of the objects for all databases
  does not imply any relationships across databases. If the user selects a data
  object from the list, the tree will be refreshed with the related data
  objects.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>To select a data object to be used in the report, check the
checkbox adjacent to the data object. Every time a data object is selected, the
tree of data objects is refreshed to display all of the related data objects.
Continue selecting data objects and click <b>OK</b> when ready to save the
selected items as a data source for the report.</p>


<p class=MsoNormal>The <b>Exclude duplicate rows</b> checkbox indicates that
only distinct rows should be returned from the database when the report is
executed. Rows having identical values for all selected columns will be
excluded.</p>


<p class=MsoNormal>If the data source for the report needs to be adjusted, click
the <b>Select Data</b> button, make the required changes in the <i>Select Data</i>
dialog box, and click the <b>OK</b> button to save the changes.</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>Modifying a data source for the report may require
  reconfiguration of the attributes for a display element.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>Most reports are based on a single data source. All
  of the display elements share the same data source. By default, Ad Hoc is
  configured to allow only a single data source per report; however, the
  administrator can configure Ad Hoc to allow the specification of multiple
  data sources for a report. See the chapter about using multiple data sources
  for details.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>Calculated Columns and Statistical columns are also
  data sources for a report and are reflected in the Data Source panel. Refer to
  the next chapter for information about Calculated Columns and Statistical
  Columns.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpLast>If only one data object is selected, column references
  in the Report Builder will not identify the data object. If multiple data
  objects are selected, column references will be shown using  Data
  Object.Column Name  notation.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<h4><a name="_Toc382291599"><br>
<br>
</a></h4>

</body>
</html>
