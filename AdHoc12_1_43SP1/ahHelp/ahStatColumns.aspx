<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945107">Statistical Columns</a></h2>


<p class=MsoNormal><i>Statistical Columns </i>give users the ability to create
new columns for the report based on a particular statistic from data in other
columns. The following statistical column types are supported with the
application:</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Difference from Previous</li>
 <li class=MsoNormal>Percentile</li>
 <li class=MsoNormal>Rank</li>
 <li class=MsoNormal>Reverse Ran</li>
 <li class=MsoNormal>Running Total</li>
</ul>



<p class=MsoNormal>The <i>Rank</i> function ranks data from the lowest value to
the highest value. For example, a total of <i>$15.00 </i>would receive a higher
rank than a total of <i>$25.00</i>. When one or more data rows have equal
values, the rank value is the same for each row. In the following example, the
first five orders listed all have a rank of 1. Since five orders share a rank
of 1, the next available rank value is 6.</p>


<p class=MsoNormal><img border=0   id="Picture 74"
src="Report_Design_Guide_files/image074.jpg" alt=3></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The <i>Quantity - Rank </i>column
ranks the Quantity column's value from lowest to highest.</span></p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Rank values are never higher than the actual number of
  rows in the report.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<p class=MsoNormal>The <i>Reverse Rank</i> function classifies data from the
highest value to the lowest value; a larger value receives a higher rank. For
example, a total of <i>$25.00 </i>would receive a higher rank than a total of <i>$15.00</i>.
When one or more data rows have equal values, the rank value is the same for
each row. In the following example, the eighth order and subsequent four orders
listed all have a rank of 8. Since five orders share a rank of 8, the next
available rank value is 13.</p>


<p class=MsoNormal><img border=0   id="Picture 75"
src="Report_Design_Guide_files/image075.jpg" alt=3></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The <i>Quantity - Reverse
Rank </i>column ranks data in the <i>Quantity </i>column from highest to
lowest.</span></p>




<p class=MsoNormal>The <i>Percentile</i> function classifies data based on a
percentage of the value distribution. In the following example, a value equal
to or greater than <i>70</i><b> </b>but less than <i>100</i><b> </b>is reported
as the <i>98<sup>th</sup></i><b> </b>percentile.</p>


<p class=MsoNormal><img border=0   id="Picture 76"
src="Report_Design_Guide_files/image076.gif" alt="perc_rank"></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The <i>Quantity Percentile </i>column
ranks data in the <i>Quantity </i>column based on a percentage of the value
distribution.</span></p>


<p class=MsoNormal>The <i>Running Total</i> function maintains a current total
of values provided in the specified column as illustrated in the following
example:</p>


<p class=MsoNormal><img border=0   id="Picture 77"
src="Report_Design_Guide_files/image077.jpg" alt=3></p>

<p class=MsoNormal><span style='font-size:10.0pt'>Example use of Running Total<i>
</i>based on the Freight column</span></p>



<p class=MsoNormal>The <i>Difference from Previous</i> function displays the
difference from the current value and the previous value of a specified column.
In the following figure, a difference of <i>31 </i>is reported from rows one to
two indicating a gain. A difference of <i>-34 </i>is reported from rows two to
three indicating a loss.</p>


<p class=MsoNormal><img border=0   id="Picture 78"
src="Report_Design_Guide_files/image078.gif" alt=diff></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The <i>Quantity Difference </i>column
maintains the difference from the current value and the previous value of the <i>Quantity
</i>column.</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><img border=0   id="Picture 325"
src="Report_Design_Guide_files/image079.jpg"></p>


<p class=MsoNormal>To create a statistical column:</p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Choose a
function from the <b>Function Type</b> drop-down menu.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Select a
column from the <i>Columns</i> list - the function is applied to the column's
value.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>OPTIONAL:
Modify the default <b>Name</b> in the field provided.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>OK</b>
to make the newly created statistical column available for selection in the
Report Builder.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>When the OK button is clicked the column's name is
  validated for SQL syntax accuracy. If the name is invalid, then a message
  will appear.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>If a statistical column is not used in the report,
  then it shall be deleted when exiting the Report Builder. Basically, use it
  or lose it.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



</body>
</html>
