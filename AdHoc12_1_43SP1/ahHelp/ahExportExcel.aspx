<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945124"></a><a name="_Toc382291620">Export to Excel</a></h2>


<p class=MsoNormal><i>Export to Excel</i> opens a new browser window and
displays the report in Microsoft Excel (spreadsheet) format.</p>



<p class=MsoNormal><img border=0   id="Picture 227"
src="Report_Design_Guide_files/image181.jpg" alt=e></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The report is exported to Microsoft
Excel as a spreadsheet.</span></p>


<p class=MsoNormal>If Microsoft Excel is installed, the report opens as an
Excel spreadsheet. Select rows or columns for sorting, copying, pasting, etc.,
and perform many other typical Excel functions.</p>




<p class=MsoNormal>Save the report in Excel format by clicking the File menu
and choosing <b>Save as</b>. Choose a name and location for the file and click <b>Save</b>.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Reports containing drill-down groupings will not be
  expanded when exported.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #FFFFCC 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#FFFFCC'>
  <p class=HintCxSpFirst><b>Hint:</b></p>
  <p class=HintCxSpLast>Click View &gt; Toolbars to add standard Excel toolbars
  to the browser window.</p>
  </div>
  </td>
 </tr>
</table>

</div>




</body>
</html>
