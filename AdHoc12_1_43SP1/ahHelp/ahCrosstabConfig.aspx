<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945113">Crosstab Configuration</a></h2>



<p class=MsoNormal>Cross tabulation ( crosstab ) style reports give users the
ability to display joint distributions of data from three separate columns. The
results of the crosstab are displayed in table format. Every crosstab table
consists of three columns: </p>



<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=831
 style='width:498.8pt;border-collapse:collapse;border:none'>
 <tr style='height:14.9pt'>
  <td width=155 valign=top style='width:92.95pt;border:solid windowtext 1.0pt;
  background:#E0E0E0;padding:0in 5.4pt 0in 5.4pt;height:14.9pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='font-size:11.0pt'>Column Type</span></b></p>
  </td>
  <td width=676 valign=top style='width:405.85pt;border:solid windowtext 1.0pt;
  border-left:none;background:#E0E0E0;padding:0in 5.4pt 0in 5.4pt;height:14.9pt'>
  <p class=MsoNormal align=center style='text-align:center'><b><span
  style='font-size:11.0pt'>Use in Crosstab Table</span></b></p>
  </td>
 </tr>
 <tr style='height:31.55pt'>
  <td width=155 valign=top style='width:92.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:31.55pt'>
  <p class=MsoNormal>Header</p>
  </td>
  <td width=676 valign=top style='width:405.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:31.55pt'>
  <p class=MsoNormal>Populates the first row of the table. Each new value
  encountered in the crosstab column produces a new column in the crosstab
  table.</p>
  </td>
 </tr>
 <tr style='height:15.75pt'>
  <td width=155 valign=top style='width:92.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:15.75pt'>
  <p class=MsoNormal>Label</p>
  </td>
  <td width=676 valign=top style='width:405.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:15.75pt'>
  <p class=MsoNormal>Populates the first column of the table.</p>
  </td>
 </tr>
 <tr style='height:48.2pt'>
  <td width=155 valign=top style='width:92.95pt;border:solid windowtext 1.0pt;
  border-top:none;padding:0in 5.4pt 0in 5.4pt;height:48.2pt'>
  <p class=MsoNormal>Values</p>
  </td>
  <td width=676 valign=top style='width:405.85pt;border-top:none;border-left:
  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  padding:0in 5.4pt 0in 5.4pt;height:48.2pt'>
  <p class=MsoNormal>Populates the aggregate data. The aggregate values are a <i>sum</i>,
  <i>count</i>,<i> standard deviation</i>,<i> </i>or <i>average </i>of the
  fields in the Value column.</p>
  </td>
 </tr>
</table>

<p class=MsoNormal><br>
<br>
<br>
</p>

<p class=MsoNormal><img border=0   id="Picture 147"
src="Report_Design_Guide_files/image122.jpg" alt=5></p>

<p class=MsoNormal><span style='font-size:10.0pt'>A crosstab report displaying
sales totals per employee across years.</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>Users can also include crosstab summary data by adding an
optional summary row or summary column. </p>


<p class=MsoNormal>In order to build a crosstab, you only need to pick three
columns, <i>Header</i>, <i>Label</i> and <i>Value</i>, and decide how you want
the value column be aggregated. These selections can be made in the <i>Crosstab
Configuration</i> tab, with minimum attributes displayed.</p>


<p class=MsoNormal>You also have many other crosstab options at your
fingertips. All of these options can be accessed either on <i>Crosstab
Configuration</i> page, with all attributes displayed, or on the <i>Crosstab
Settings</i> tab.</p>



<p class=MsoNormal>To add a simple Crosstab Table:</p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>From the
Report Builder interface, click the Crosstab icon in the ADD frame at the left
of the interface to add the Crosstab component to the report. Use drag-and-drop
methods to place the crosstab into the report in a specific location in the MODIFY
panel.<br>
<br>
</p>


<p class=MsoNormal><img border=0   id="Picture 149"
src="Report_Design_Guide_files/image123.jpg"></p>

<p class=MsoNormal><br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>From the
Crosstab Configuration panel's <i>Header Values Column</i> section, choose a header
<i>Column</i> from the drop-down list.</p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>From the <i>Label
Values Column</i> section<i>, </i>choose a label <i>Column</i> from the
drop-down list.</p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>From the <i>Values
Column</i> section:<br>
<br>
</p>

<p class=MsoNormal style='margin-left:1.0in;text-indent:-.25in'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Choose a
value <i>Column</i> from the drop-down list.</p>

<p class=MsoNormal style='margin-left:1.0in;text-indent:-.25in'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Choose an <i>Aggregate
Function</i><sup>1</sup> from the drop-down list.</p>

<p class=MsoNormal style='margin-left:.75in'>&nbsp;</p>

<p class=MsoNormal>Your crosstab table is ready at this point.</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>The available aggregate functions are Sum, Average,
  Standard Deviation and Count.</p>
  </div>
  </td>
 </tr>
</table>

</div>



</body>
</html>
