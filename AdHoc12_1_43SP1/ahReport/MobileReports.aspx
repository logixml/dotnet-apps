<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MobileReports.aspx.vb" Inherits="LogiAdHoc.MobileReports" %>
<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenuMobile.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/MobileSearch.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControlCustomizable.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Report Management</title>
    <link href="../AdHocMobile.css" type="text/css" rel="stylesheet" />
</head>
<body>
    
    <form id="form1" runat="server">
    
    
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true" EnablePartialRendering="true" ScriptMode="Release" />
        <asp:UpdatePanel ID="upPage" runat="server" RenderMode="Inline">
            <ContentTemplate>
        
        <div class="MobileMenu">
            <AdHoc:MainMenu ID="menu" runat="server" EnableViewState="false" />
            <div id="divLogo" class="MobileLogo">
                <img alt="Logo" src="../ahImages/mobilelogo.png" />
            </div>
            <div id="submenu"><AdHoc:NavMenu id="subnav" runat="server" UseSeparator="False" /></div>
        </div>
        <div id="divSearch" class="MobileSearch">
            <div id="divBtnBack" runat="server" class="divBtnBack">
                <asp:HyperLink ID="btnBack" runat="server" >
                    <span class="btnBack">Back</span>
                </asp:HyperLink>
            </div>
            <div id="divlblCurFolder" runat="server" class="divCurFolder">
                <asp:Label ID="lblCurrFolder" runat="Server" Text="Mobile Reports" />
            </div>
            <AdHoc:Search ID="srch" runat="server" Title="Search" meta:resourcekey="AdHocSearch" />
        </div>
            
            <asp:GridView OnRowCreated="grdMain_OnRowCreated" runat="server" ID="grdMain" AllowSorting="True" CssClass="grid" AutoGenerateColumns="False" 
                PagerSettings-Position="Bottom" AllowPaging="True" DataKeyNames="ID" meta:resourcekey="grdMainResource1" OnSorting="OnSortCommandHandler"
                 GridLines="Horizontal">
                <HeaderStyle CssClass="gridheader" HorizontalAlign="left"></HeaderStyle>
                <PagerTemplate>
                    <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" Align="Left" ShowCount="false" ShowFirstLast="false" />
                </PagerTemplate>
                <PagerStyle HorizontalAlign="Left"></PagerStyle>
                <RowStyle CssClass="gridrow" />
                <AlternatingRowStyle CssClass="gridalternaterow"></AlternatingRowStyle>
                <Columns>
                
                    <asp:TemplateField SortExpression="ObjectTypeID,ObjectName"  HeaderText="<%$ Resources:LogiAdHoc, Name %>" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Image runat="server" ID="ObjectType" meta:resourcekey="ObjectTypeResource1"></asp:Image>
                            <asp:HyperLink runat="server" Target="_self" ID="ObjectName" meta:resourcekey="ObjectNameResource1">
                                <asp:Label ID="lblObjectName" runat="server" CssClass="ReportLink"/>
                            </asp:HyperLink>
                            <asp:LinkButton runat="server" ID="FolderLink" meta:resourcekey="FolderLinkResource1" OnCommand="DoModifications">
                                <asp:Label ID="lblFolderLink" runat="server" CssClass="ReportLink" />
                            </asp:LinkButton>
                            
                            <div id="divDescription" runat="server" class="mobiledescription" >
                                <asp:Label runat="server" ID="Description" meta:resourcekey="DescriptionResource1"></asp:Label>
                            </div>
                            
                            <input runat="server" id="MainID" type="hidden" />
                            <input runat="server" id="ObjectTypeID" type="hidden" />
                            <input runat="server" id="FolderID" type="hidden" />
                            <input runat="server" id="ReportID" type="hidden" />
                            <input runat="server" id="PhysicalName" type="hidden" />
                            <input runat="server" id="ArchiveExists" type="hidden" />
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="ObjectTypeID,LastModified" HeaderText="Last Modified" ItemStyle-CssClass="ModifiedItemStyle" HeaderStyle-CssClass="ModifiedHeaderStyle" meta:resourcekey="TemplateFieldResource5">
                        <ItemTemplate>
                            <asp:Label runat="server" ID="Modified" meta:resourcekey="ModifiedResource1"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" Target="_self" CssClass="ReportLink" ID="ReportActionLink" style="font-size:x-large;" meta:resourcekey="ObjectNameResource1"><img src="../ahImages/arrowbutton.png" style="border:0px;"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            
            <p runat="server" id="NoReports" class="info">
                <asp:Localize ID="locNoReports" runat="server" EnableViewState="false" Text="<%$ Resources:Errors, Err_NoReports %>"></asp:Localize>
            </p>
            </ContentTemplate>
        </asp:UpdatePanel>
        
    </form>
</body>
</html>
