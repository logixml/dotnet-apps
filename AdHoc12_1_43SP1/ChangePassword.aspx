<%@ Page AutoEventWireup="false" Codebehind="ChangePassword.aspx.vb" Inherits="LogiAdHoc.ChangePassword"
    Language="vb" %>

<%@ Register Src="~/ahControls/PasswordControl.ascx" TagName="PasswordControl" TagPrefix="AdHoc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head id="Head1" runat="server">
        <title>Change Password</title>
        <link rel="shortcut icon" href="ahImages/flav.ico" />
        <%--<link href="Login.css" rel="stylesheet" type="text/css" />--%>
    </head>
    <body id="bod">
        <form id="form1" runat="server">
        
            <div id="menu"><div id="top_nav" style="padding-top:4px;">
                <table id="tbMenu" runat="server">
                    <tr id="trMenu" runat="server">
                        <td class="tdMnuLogo">
                            <asp:HyperLink ID="SiteLogo" runat="server" meta:resourcekey="SiteLogoResource1" SkinID="SiteLogo"/>
                        </td>
                    </tr>
                </table>
            </div></div>
            
            <div class="divForm">
                <input id="UserID" runat="server" type="hidden">
                <p class="info">
                    <asp:Literal ID="infoText" runat="server" Text="<%$ Resources:LogiAdHoc, ChangePasswordInfoText %>">
                    </asp:Literal>
                </p>
                <br />
                <%--<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                    <tr align="center" valign="middle">
                        <td>
                            <table align="center" border="0" cellpadding="3" cellspacing="0" width="304">
                                <tr align="center" valign="middle">
                                    <td>
                                        <asp:Panel ID="pnlPasswordDiv" runat="server" CssClass="modalPopup" >
                                            <div class="modalDiv" style="width: 300px">--%>
                <table class="tblBody" style="width: 100%;">
                    <tr class="trBody">
                        <td>
                            <AdHoc:PasswordControl ID="UserPassword" runat="server" />
                        </td>
                    </tr>
                </table>
                <%--        </div>
                                        </asp:Panel>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>    
                </table>--%>
            </div>
        </form>
    </body>
</html>
