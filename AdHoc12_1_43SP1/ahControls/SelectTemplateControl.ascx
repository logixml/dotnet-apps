<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SelectTemplateControl.ascx.vb" Inherits="LogiAdHoc.SelectTemplateControl" %>
<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>

<asp:Panel ID="pnlTemplateControl" runat="server" meta:resourcekey="pnlTemplateControlResource1">
    <table cellpadding="0" cellspacing="10px" width="100%">
        <tr width="100%">            
            <td style="background-color: White; border: solid 1px DarkGray;" width="60%" valign="top">
                <div ID="divMid" runat="server" style="height:500px; overflow: auto; vertical-align: top;">
                    <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        
                            <div id="divBlankAndDefault" class="groupHeader" style="margin-bottom:0">
                                <asp:Localize ID="Localize2" runat="server" Text="Blank" meta:resourcekey="Localize2Resource1"></asp:Localize>
                            </div>
                            
                            <asp:DataList ID="dlDefTemplates" runat="server" OnItemCommand="dlDefTemplates_ItemCommand" 
                                OnItemDataBound="dlDefTemplates_ItemDataBound" RepeatColumns="3" CellPadding="2" CellSpacing="15" 
                                RepeatDirection="Horizontal" meta:resourcekey="dlDefTemplatesResource1" >
                                
                                <ItemStyle Width="100px" HorizontalAlign="Center" VerticalAlign="Top" />
                                <ItemTemplate>
                                    <div id="divDefTemplates" runat="server" style="padding-top: 8px;" >
                                        <asp:ImageButton ID="imgControl" Width="70px" runat="server" CommandName="Select" meta:resourcekey="imgControlResource1" /></div>
                                    <div style="width: 90px;">
                                        <asp:Label ID="lblTemplateName" runat="server" meta:resourcekey="lblTemplateNameResource1"></asp:Label></div>
                                </ItemTemplate>
                            </asp:DataList>
                            
                            <div id="divOtherTemplates" class="groupHeader" style="margin-bottom:0">
                                <asp:Localize ID="Localize1" runat="server" Text="Other Templates" meta:resourcekey="Localize1Resource1"></asp:Localize>
                            </div>
                            
                            <asp:DataList ID="dlOtherTemplates" runat="server" OnItemCommand="dlOtherTemplates_ItemCommand" 
                                OnItemDataBound="dlOtherTemplates_ItemDataBound" RepeatColumns="3" CellPadding="2" CellSpacing="15" 
                                RepeatDirection="Horizontal" meta:resourcekey="dlOtherTemplatesResource1" >
                                
                                <ItemStyle Width="100px" HorizontalAlign="Center" VerticalAlign="Top"/>
                                <ItemTemplate>
                                    <div id="divOtherTemplates" runat="server" style="padding-top: 8px;" ondblclick="javascript: SelectTemplate('2');" >
                                        <asp:ImageButton ID="imgControl" Width="70px" runat="server" CommandName="Select" meta:resourcekey="imgControlResource2" /></div>
                                    <div style="width: 90px;">
                                        <asp:Label ID="lblTemplateName" runat="server" meta:resourcekey="lblTemplateNameResource2"></asp:Label></div>
                                </ItemTemplate>
                            </asp:DataList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </td>
            <td style="vertical-align: top; background-color: White; border: solid 1px DarkGray;" >
                <asp:Panel ID="pnlRight" runat="server" Width="100%" meta:resourcekey="pnlRightResource1">
                    <asp:UpdatePanel ID="UPTemplateDesc" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table style="margin:5px;">
                                <tr>
                                    <td>
                                        <b><asp:Label ID="lblTemplateName" runat="server" meta:resourcekey="lblTemplateNameResource3"></asp:Label></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="50px" valign="top">
                                        <br />
                                        <asp:Label ID="lblTemplateDescription" runat="server" meta:resourcekey="lblTemplateDescriptionResource1"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Image ID="imgThumbnail" runat="server" AlternateText="Template Thumbnail Image" meta:resourcekey="imgThumbnailResource1" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>