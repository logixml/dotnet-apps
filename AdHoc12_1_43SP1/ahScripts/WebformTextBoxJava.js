function WebForm_TextBoxKeyHandler(event) {
	var target = event.target;
	if ((target == null) || (typeof(target) == "undefined")) target = event.srcElement;
	if (event.keyCode == 13) {
		if ((typeof(target) != "undefined") && (target != null)) {
			if (typeof(target.onchange) != "undefined") {
				target.onchange();
				event.cancelBubble = true;
				if (event.stopPropagation) event.stopPropagation();
				return false;
			}
		}
	}
	return true;
}
