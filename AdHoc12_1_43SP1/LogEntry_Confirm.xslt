<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html" indent="yes" />

<xsl:template match="LogEntry">
<html>
<head>
		<title>Log Entry</title>
</head>
<body>

	<h1>Proposed <xsl:value-of select="@Product"/> Changes</h1>
	<h2>Date: <xsl:value-of select="@Date"/></h2>
	
	<xsl:if test="not(File|Schema[@Type='Change'])">
	<p>No changes will be made to  <xsl:value-of select="@Product"/> .</p>
	</xsl:if>
	
	<xsl:apply-templates select="File[@Type='Change']" />
	<xsl:apply-templates select="Schema[@Type='Change']" />
	<xsl:apply-templates select="Schema[@Type='Warning']" />

</body>
</html>
</xsl:template>

<xsl:template match="File[@Type='Change']">
<h3>File Changes</h3>
<xsl:if test="Report[@Action='Remove']">
<p>The following reports will be marked as broken:</p>
<ul>
<xsl:for-each select="Report[@Action='Remove']">
<li><em><xsl:value-of select="@ReportName" /></em> (File: <xsl:value-of select="@Filename" />)</li>
</xsl:for-each>
</ul>
</xsl:if>
</xsl:template>

<xsl:template match="Schema[@Type='Change']">
<h3>Schema Changes</h3>
<p>You have chosen to make the following changes to  <xsl:value-of select="../@Product"/> :</p>

<xsl:if test="Object[@Action='Add']">
	<h4>Added objects</h4>
	<ul>
		<xsl:for-each select="Object[@Action='Add']">
			<li><em><xsl:value-of select="@ObjectName" /></em> (Type: <xsl:value-of select="@ObjectType" />)</li>
		</xsl:for-each>
	</ul>
</xsl:if>

<xsl:if test="Object/Column[@Action='Add']">
	<h4>Added columns</h4>
	<ul>
		<xsl:for-each select="Object/Column[@Action='Add']">
			<li><xsl:value-of select="../@ObjectName" />.<em><xsl:value-of select="@ColumnName" /></em></li>
		</xsl:for-each>
	</ul>
</xsl:if>

<xsl:if test="Object[@Action='Remove']">
	<h4>Removed objects</h4>
	<ul>
		<xsl:for-each select="Object[@Action='Remove']">
			<li><em><xsl:value-of select="@ObjectName" /></em> (Type: <xsl:value-of select="@ObjectType" />)</li>
		</xsl:for-each>
	</ul>
</xsl:if>

<xsl:if test="Object/Column[@Action='Remove']">
	<h4>Removed columns</h4>
	<ul>
		<xsl:for-each select="Object/Column[@Action='Remove']">
			<li><xsl:value-of select="../@ObjectName" />.<em><xsl:value-of select="@ColumnName" /></em></li>
		</xsl:for-each>
	</ul>
</xsl:if>

<xsl:if test="Object/Column[@Action='Update']">
	<h4>Repaired columns</h4>
	<ul>
		<xsl:for-each select="Object/Column[@Action='Update']">
			<li><xsl:value-of select="../@ObjectName" />.<em><xsl:value-of select="@ColumnName" /></em></li>
		</xsl:for-each>
	</ul>
</xsl:if>

<xsl:if test="Object/Column[@Action='SetDataType']">
	<h4>Columns with changed Data Types</h4>
	<ul>
		<xsl:for-each select="Object/Column[@Action='SetDataType']">
			<li><xsl:value-of select="../@ObjectName" />.<em><xsl:value-of select="@ColumnName" /></em></li>
		</xsl:for-each>
	</ul>
</xsl:if>

<xsl:if test="Relationship[@Action='Add']">
	<h4>Added relationships</h4>
	<ul>
		<xsl:for-each select="Relationship[@Action='Add']">
			<li><em><xsl:value-of select="@RelationName" /></em> (<xsl:value-of select="@Relation" /> between: <xsl:value-of select="@PrimaryObject" />.<xsl:value-of select="@PrimaryColumn" /> and <xsl:value-of select="@ForeignObject" />.<xsl:value-of select="@ForeignColumn" />)</li>
		</xsl:for-each>
	</ul>
</xsl:if>

<xsl:if test="Relationship[@Action='Remove']">
	<h4>Removed relationships</h4>
	<ul>
		<xsl:for-each select="Relationship[@Action='Remove']">
			<li><em><xsl:value-of select="@RelationName" /></em> (<xsl:value-of select="@Relation" /> between: <xsl:value-of select="@PrimaryObject" />.<xsl:value-of select="@PrimaryColumn" /> and <xsl:value-of select="@ForeignObject" />.<xsl:value-of select="@ForeignColumn" />)</li>
		</xsl:for-each>
	</ul>
</xsl:if>

</xsl:template>

<xsl:template match="Schema[@Type='Warning']">
<h3>Warnings</h3>
<p>The schema that  <xsl:value-of select="../@Product"/> is using is different from the schema
being used by your database. This can cause report errors or  
unpredictable behavior in the report creation wizard.</p>

<xsl:if test="Object/Column[@ColumnLocation='2']">
	<h4>Invalid columns</h4>
	<p>The following columns were found in <xsl:value-of select="../@Product"/>, but do
	not correspond with any columns in the source database. These columns
	should be removed.</p>
	<ul>
		<xsl:for-each select="Object/Column[@ColumnLocation='2']">
			<li><xsl:value-of select="../@ObjectName" />.<em><xsl:value-of select="@ColumnName" /></em></li>
		</xsl:for-each>
	</ul>
</xsl:if>

<xsl:if test="Object/Column[@ColumnDifferences!='0']">
	<h4>Columns with Different DataTypes</h4>
	<p>The following columns were found in <xsl:value-of select="../@Product"/>, but are
	different from their corresponding columns in the source database.</p>
	<ul>
		<xsl:for-each select="Object/Column[@ColumnDifferences!='0']">
			<li><xsl:value-of select="../@ObjectName" />.<em><xsl:value-of select="@ColumnName" /></em></li>
		</xsl:for-each>
	</ul>
</xsl:if>

</xsl:template>

</xsl:stylesheet>

  