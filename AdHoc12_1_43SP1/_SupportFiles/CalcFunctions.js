
function DateTimeNow()
{
    var d = new Date(); 
    return d.toString();
}

function Year(dt)
{
    if (dt == "")
        return "";
	dt = dt.replace("T", " ");
    //var d = new Date(Date.parse(dt));
	var d = parseDate(dt);
    return d.getFullYear();
}

function Month(dt)
{
    if (dt == "")
        return "";
	dt = dt.replace("T", " ");
    //var d = new Date(dt);
	var d = parseDate(dt);
    return (d.getMonth() + 1);
}

function Day(dt)
{
    if (dt == "")
        return "";
	dt = dt.replace("T", " ");
    //var d = new Date(dt);
	var d = parseDate(dt);
    return d.getDate();
}

function Left(str, len) 
{
    if (len > str.length)
        return str;
	return str.substr(0, len);
}

function Right(str, len) 
{
    if (len < str.length) 
        return str.substr((str.length - len), len);
    else
        return str;
}

function parseDate(xmlDate)
{
      if (!/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}/.test(xmlDate)) {
           throw new RangeError("xmlDate must be in ISO-8601 format YYYY-MM-DD.");
      }
      return new Date(xmlDate.substring(0,4), xmlDate.substring(5,7)-1, xmlDate.substring(8,10));
}

function Concat(str1, str2)
{
    return str1 + str2;
}

function Length(str)
{
    return str.length;
}

function Middle(str, len1, len2)
{
    return str.substr(len1, len2)
}

function Upper(str)
{
    return str.toUpperCase();
}

function Lower(str)
{
    return str.toLowerCase();
}