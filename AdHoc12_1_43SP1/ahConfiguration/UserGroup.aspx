<%@ Page Language="vb" AutoEventWireup="false" Codebehind="UserGroup.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_UserGroup"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Organization</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>

</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <asp:UpdatePanel ID="UPBct" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="Organization" ParentLevels="1" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlParentID" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
            RestorePopupPosition('ahModalPopupBehavior');
        }
        function BeginRequestHandler(sender, args) {
            SavePopupPosition('ahModalPopupBehavior');
        }
        </script>

        <div class="divForm">
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
            <input type="hidden" id="ahParentID" name="ahParentID" runat="server" />
            <asp:UpdatePanel ID="UP1" runat="Server" RenderMode="Inline">
                <ContentTemplate>
                    <table id="tbParent" runat="server" class="tbTB">
                        <tr>
                            <td width="125px">
                                <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource1" Text="Selected Organization:"></asp:Localize></td>
                            <td>
                                <asp:DropDownList ID="ddlParentID" AutoPostBack="True" runat="server" meta:resourcekey="ddlParentIDResource1" />
                            </td>
                        </tr>
                    </table>
                    <table class="tbTB">
                        <tr>
                            <td width="125">
                                <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2" Text="Organization Name:"></asp:Localize></td>
                            <td>
                                <input id="Groupname" type="text" maxlength="100" size="50" runat="server" />
                                <asp:RequiredFieldValidator ID="rtvGroupName" runat="server" ErrorMessage="Organization Name is required."
                                    ControlToValidate="Groupname" ValidationGroup="UserGroup" meta:resourcekey="rtvGroupNameResource1">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvValidName" runat="server" ErrorMessage="This name is not valid."
                                    ControlToValidate="Groupname" OnServerValidate="IsNameValid" EnableClientScript="False"
                                    meta:resourcekey="cvValidNameResource1" ValidationGroup="UserGroup">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="125">
                                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:LogiAdHoc, Description %>"></asp:Localize>:</td>
                            <td>
                                <asp:TextBox ID="GroupDescription" TextMode="MultiLine" Columns="50" Rows="3" runat="server" />
                                <asp:CustomValidator ID="cvMaxLength" runat="server" ErrorMessage="<%$ Resources:Errors, Err_Description255 %>"
                                    ControlToValidate="GroupDescription" ValidationGroup="UserGroup" OnServerValidate="ReachedMaxLength">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="125">
                                <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource4" Text="Theme"></asp:Localize>:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlApplicationTheme" runat="Server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                    <div id="UserInfo" runat="server">
                        <h2>
                            <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3" Text="Users In This Organization"></asp:Localize></h2>
                        <table><tr><td>
                            <div id="data_main">
                                <asp:GridView ID="grdMain" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                    OnSorting="OnSortCommandHandler" AllowSorting="True" CssClass="gridDetail" DataKeyNames="UserID"
                                    OnRowDataBound="OnItemDataBoundHandler" meta:resourcekey="grdMainResource1">
                                    <Columns>
                                        <asp:BoundField DataField="Username" HeaderText="Username" ReadOnly="True" SortExpression="Username"
                                            meta:resourcekey="BoundFieldResource1">
                                            <HeaderStyle Width="200px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="RoleName" HeaderText="User Roles" ReadOnly="True" meta:resourcekey="BoundFieldResource2">
                                            <HeaderStyle Width="300px" />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerTemplate>
                                        <AdHoc:PagingControl ID="PageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                                    </PagerTemplate>
                                    <PagerStyle HorizontalAlign="Center" />
                                    <HeaderStyle CssClass="gridheader" />
                                    <RowStyle CssClass="gridrow" />
                                    <AlternatingRowStyle CssClass="gridalternaterow" />
                                </asp:GridView>
                            </div>
                        </td></tr></table>
                    </div>
                    <br />
                    <div id="divButtons" class="divButtons">
                        <asp:UpdatePanel ID="UPSaveAS" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <AdHoc:LogiButton ID="btnSave" runat="server" OnClick="SaveGroup" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>"
                                    Text="<%$ Resources:LogiAdHoc, Save %>" UseSubmitBehavior="false" ValidationGroup="UserGroup" />
                                <AdHoc:LogiButton ID="btnSaveAs" runat="server" OnClick="SaveGroupAs" ValidationGroup="UserGroup"
                                    ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" Text="<%$ Resources:LogiAdHoc, SaveAs %>" />
                                <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="CancelGroup"
                                    ToolTip="Click to cancel your unsaved changes and return to the previous page."
                                    Text="Back to Organizations" CausesValidation="False" meta:resourcekey="btnCancelResource1" />
                                    
                                <asp:Button runat="server" ID="Button1" Style="display: none" />
                                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                                    TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                                    DropShadow="False" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                                </ajaxToolkit:ModalPopupExtender>
                                <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 400;">
                                    <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                                    <div class="modalPopupHandle">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                                <asp:Localize ID="PopupHeader" runat="server" Text="Save As New Organization"></asp:Localize>
                                            </td>
                                            <td style="width: 20px;">
                                                <asp:ImageButton ID="imgClosePopup" runat="server" 
                                                    OnClick="imgClosePopup_Click" CausesValidation="false"
                                                    SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                                    AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                        </td></tr></table>
                                    </div>
                                    </asp:Panel>
                                    <div class="modalDiv">
                                    <asp:UpdatePanel ID="upPopup" runat="server">
                                        <ContentTemplate>
                                            <%--<asp:Panel ID="pnlSaveAs" runat="server" CssClass="detailpanel">--%>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Localize ID="Localize7" runat="server" meta:resourcekey="LiteralResource2" Text="Organization Name:"></asp:Localize>
                                                    </td>
                                                    <td>
                                                        <input id="txtGroupName" type="text" maxlength="100" size="50" runat="server" />
                                                        <asp:RequiredFieldValidator ID="rfvtxtGroupName" runat="server" ErrorMessage="Organization Name is required."
                                                            ControlToValidate="txtGroupName" ValidationGroup="SaveAs" meta:resourcekey="rtvGroupNameResource1">*</asp:RequiredFieldValidator>
                                                        <asp:CustomValidator ID="cvtxtGroupName" runat="server" ErrorMessage="This organization name already exists in the database. Please select another name."
                                                            ControlToValidate="txtGroupName" ValidationGroup="SaveAs" OnServerValidate="IsSaveAsGroupNameValid"
                                                            EnableClientScript="false" meta:resourcekey="cvGroupNameResource1">*</asp:CustomValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <table>
                                                <tr>
                                                    <td>
                                                        <AdHoc:LogiButton ID="btnSPA_OK" runat="server" OnClick="OK_SPA" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>"
                                                            Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" ValidationGroup="SaveAs" />
                                                        <AdHoc:LogiButton ID="btnSPA_Cancel" runat="server" OnClick="Cancel_SPA"
                                                            ToolTip="<%$ Resources:LogiAdHoc, CancelTooltip1 %>" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                            CausesValidation="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="SaveAs" runat="server"
                                                meta:resourcekey="vsummaryResource1" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    </div>
                                </asp:Panel>
                                <asp:ValidationSummary ID="vsummary" runat="server" ValidationGroup="UserGroup" meta:resourcekey="vsummaryResource1" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnCancel" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
<br />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlParentID" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
        </div>
    </form>
</body>
</html>
