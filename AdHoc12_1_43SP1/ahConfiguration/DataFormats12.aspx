<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DataFormats12.aspx.vb" EnableViewState="false" Inherits="LogiAdHoc.DataFormats12" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>

<%@ Register TagPrefix="AdHoc" TagName="Grid" Src="~/ahControls/Grid.ascx" %>


<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>


<%@ Import Namespace="ahService" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Data Formats</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    
    <script src="../ahScripts/jQuery/lib/jquery-1.4.3.min.js" type="text/javascript"></script>
	<script src="../ahScripts/jQuery/js/DataFormats.js" type="text/javascript"></script>

    <link rel="stylesheet" href="../ahScripts/jQuery/slick/slick.grid.css" type="text/css" media="screen" charset="utf-8" />
	<link rel="stylesheet" href="../ahScripts/jQuery/css/smoothness/jquery-ui-1.8.5.custom.css" type="text/css" media="screen" charset="utf-8" />
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <div id="submenu">
            <AdHoc:NavMenu ID="subnav" runat="server" />
        </div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" ParentLevels="0" Key="DataFormats" />
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        
        <input type="hidden" id="EditRowNumber" name="EditRowNumber" value="0" /> 
        <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />                               
        <input type="hidden" id="ihDisableReorder" runat="server" value="0" />
        <input type="hidden" id="MaxSort" value="0" />
        <input type="hidden" id="ErrorMessageOverflow" value="<%= HttpContext.GetGlobalResourceObject("Errors","Err_Explanation255") %>" />
        <input type="hidden" id="ConfirmMessageDelete" value="<%= HttpContext.GetGlobalResourceObject("LogiAdHoc","DataFormats_DeleteConfirm") %>" />
        <input type="hidden" id="ConfirmMessageSelectDelete" value="<%= HttpContext.GetGlobalResourceObject("LogiAdHoc","DataFormats_DeleteSelect") %>" />
                        
        <table class="limiting">
            <tr>
                <td>
                    <table class="gridForm">
                        <tr>
                            <td>

<div id="data_main">

                                <div id="activities">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr width="100%">
                                            <td align="left" valign="top">
                                            
                                                <span class="outerbox">
                                                    <input id="deleteButton" type="button" value="<%= HttpContext.GetGlobalResourceObject("LogiAdHoc","Delete") %>" onclick="RemoveRow();" class="innerbutton" />
                                                </span>
                                                
                                                <span class="outerbox">
                                                    <input type="button" value="Add" onclick="AddRow();" class="innerbutton" />
                                                </span>
                                                
                                            </td>
                                            <td align="right" valign="top">
                                                <AdHoc:Search ID="srch" runat="server" Title="Find Data Formats" meta:resourcekey="AdHocSearch" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                
                                <div id="divDisabledReorder" runat="server">
                                    <p class="info">
                                        <asp:Localize ID="Localize14" runat="server" meta:resourcekey="LiteralResource14"
                                            Text="Reordering has been disabled due to some rows being hidden as a result of search." />
                                    </p>
                                </div>
                                <br />
                                
                                <a href="#" onclick="handleHeaderSort(); return false;">sort</a>
                                <br /><br />
                                <%--<AdHoc:Grid runat="server" ID="jqGrid"  Width="750" Height="450" />--%>
                                <AdHoc:AHGrid runat="server" ID="grdMain" WidthStr="750" HeightStr="450" />
</div>

                                <div id="dialog" title="Data Format Details">
                                    <br />
                                    <input type="hidden" id="NewFormatID" />
                                    <table class="tbTB">
                                        <tr>
                                            <td>
                                                <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2" Text="Format Name:"></asp:Localize>
                                            </td>
                                            <td>
                                                <input type="text" id="txtFormatName" name="txtFormatName" maxlength="50" />
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3" Text="Format:"></asp:Localize>
                                            </td>
                                            <td>
                                                <input type="text" id="txtFormat" name="txtFormat" maxlength="255" />
                                              
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource4" Text="Applies to:"></asp:Localize>
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" id="chkNumeric" name="chkNumeric" /> <asp:literal Text="<%$ Resources:LogiAdHoc, DataFormat_Numeric %>" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" id="chkDateTime" name="chkDateTime" /> <asp:literal Text="<%$ Resources:LogiAdHoc, DataFormat_DateTime %>" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" id="chkBoolean" name="chkBoolean" /> <asp:literal Text="<%$ Resources:LogiAdHoc, DataFormat_Boolean %>" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" id="chkText" name="chkText" /> <asp:literal Text="<%$ Resources:LogiAdHoc, DataFormat_Text %>" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource12" Text="Is Available:"></asp:Localize>
                                            </td>
                                            <td>
                                                <input type="checkbox" id="chkIsAvailable" name="chkIsAvailable" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource5" Text="Explanation:"></asp:Localize>
                                            </td>
                                            <td>
                                                <textarea rows="3" cols="45" id="txtExplanation" name="txtExplanation" maxlength="255"></textarea>
                                                
                                            </td>
                                        </tr>
                                    </table>
                                    <br /><br />
                                    <!-- Buttons -->
                                    <table>
                                        <tr>
                                            <td colspan="2">
                                                <span class="outerbox">
                                                    <input type="button" id="btnSaveFormat" onclick="SaveFormat()" value="Save" class="innerbutton" />
                                                </span>
                                                
                                                <span class="outerbox">
                                                    <input type="button" id="btnCancelFormat" onclick="Close()" value="Cancel" class="innerbutton" />
                                                </span>
                                                    
                                                <asp:ValidationSummary ID="vsummary" ValidationGroup="DataFormat" runat="server"/>
                                            </td>
                                        </tr>
                                    </table>
                                    
                                </div>

                                <div id="divLegend" class="legend" runat="server">
                                    <span>
                                        <asp:Localize ID="locLegend" runat="server" Text="<%$ Resources:LogiAdHoc, LegendLabel %>"></asp:Localize>
                                    </span>
                                    <dl id="dlApplicationFormat" runat="server">
                                        <dt>
                                            <img id="Img2" runat="server" alt="Application Format" border="0" height="13" meta:resourcekey="LegendImage2"
                                                src="../ahImages/spacer.gif" style="background-color: #008800;" title="Application Format"
                                                width="13" />
                                        </dt>
                                        <dd style="color: #008800;">
                                            <asp:Localize ID="Localize10" runat="server" meta:resourcekey="LiteralResource10"
                                                Text="Application Format"></asp:Localize>&nbsp;
                                        </dd>
                                    </dl>
                                    <dl id="dlUserDefinedFormat" runat="server">
                                        <dt>
                                            <img id="Img1" runat="server" alt="User-defined Format" border="0" height="13" meta:resourcekey="LegendImage3"
                                                src="../ahImages/spacer.gif" style="background-color: Black;" title="User-defined Format"
                                                width="13" />
                                        </dt>
                                        <dd style="color: Black;">
                                            <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource11" Text="User-defined Format"></asp:Localize>&nbsp;
                                        </dd>
                                    </dl>
                                </div>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        
        <div id="hovermenu" style="display: none; position: absolute; width: 150px; height: 50px; background-color:#f9f9f9; z-index:9999;">
            
            <div style="text-align: left; white-space: nowrap; background-color:#f9f9f9; border:1px solid #BCBCBC; color:#484848;" id="pnlActionsMenu">
                <div id="divModify" class="hoverMenuActionLink">
                    <a href="#" id="hovermodify" onclick="Open(''); return false;" class="hoverMenuActionLink">Modify Data Format</a>
                </div>
                <div id="divDependencies" class="hoverMenuActionLink">
                    <a href="" id="hoverlink">View Dependencies</a>
                </div>
            </div>
            
        </div>
        
    </form>
</body>
</html>
