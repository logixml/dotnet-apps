/* Checkbox Scripting on Info Schedule Manager Page */
function AddScheduleToArray(ele) {
	if (ele)
        rdSelectSchedule(ele.value, ele.checked);
	
	var cnt = rdGetSelectedSchedule().length;
	var DelBtn = document.getElementById('btnDeleteBatch_Row1');
	var BtnCnt = document.getElementById('btnArrayCnt_Row1');
	var AllChk = document.getElementById('bmCheckAll_Row1');
	
	AllChk.checked = false;
 
	if(cnt >= 1) {
		if (BtnCnt.style.display != 'inline') {
			BtnCnt.style.display = 'inline';
			
			if (DelBtn)
				DelBtn.style.display = 'inline';
			
			ClearBatchAllRows(true);
		}
		
        BtnCnt.innerHTML = ' (' + cnt + ')';

        var items = document.getElementsByClassName('go-list-item');
        if (cnt == items.length) {
            AllChk.checked = true;
        }
	}
	else if (BtnCnt.style.display != 'none') {
		if (DelBtn)
			DelBtn.style.display = 'none';
		
		BtnCnt.style.display = 'none';
		
		ClearBatchAllRows(false);
	}
	
    if (ele) {
		HideShowRowElements(ele);
    }
}

function HideShowRowElements(ele)
{
	var chkBox = ele;
	var r = chkBox.id.split("_");
	var rowSuffix = r[1];
	var n = chkBox.checked;

	//Add Bold Title
	var bTitle = document.getElementById('divGoListItemTitle_' + rowSuffix);

	if(bTitle)
	{
		if(n)
		{
			bTitle.classList.add("go-bold");
		}
		else
		{
			bTitle.classList.remove("go-bold");
		}
	}
}

function ClearBatchAllRows(bHide)
{
	var getRow = document.getElementsByClassName('go-list-item');
    var visibility = bHide ? 'hidden' : 'visible';
    for (var i = 0; i < getRow.length; i++) {
		var rowSuffix = 'Row' + (i+1);

       //Hide Row Buttons
		var sched = document.getElementById('colSchedule_' + rowSuffix);
		if (sched)
			sched = sched.firstElementChild;
		
        var del = document.getElementById('colRemoveSchedule_' + rowSuffix);
		if (del)
			del = del.firstElementChild;
        (sched) ? sched.style.visibility = visibility: false;
        (del) ? del.style.visibility = visibility: false;
	}
}

function goSchedulesSelectionCleared() {
    AddScheduleToArray(null);
}

function CheckAllSchedules(ele)
{
	var AllChk = document.getElementById('bmCheckAll_Row1');
	
	var isChecked = ele.checked;
	var getElem = document.getElementsByClassName('bkSelectElem');
    for (var i = 0; i < getElem.length; i++) {
		if(isChecked)
		{
			if(!getElem[i].checked)
			{
				getElem[i].click();
			}
		}
		else
		{
			if(getElem[i].checked)
			{
				getElem[i].click();
			}
		}
	}
	if(isChecked)
	{
		ele.checked = true;
	}
}

function UnCheckAllSchedules()
{
	var getElem = document.getElementsByClassName('bkSelectElem');
    for (var i = 0; i < getElem.length; i++) {
		if(getElem[i].checked)
		{
			getElem[i].click();
		}
    }
}

function rdSelectSchedule(sTaskID, bSelect) {
    if (!sTaskID)
        return;

    if (!bSelect || bSelect.toString().toUpperCase() === "FALSE")
        return rdDeselectSchedule(sTaskID);

    var selected = rdGetSelectedSchedule();
    if (selected.indexOf(sTaskID) >= 0)
        return;

    selected.push(sTaskID);

    document.getElementById("rdScheduleBatch").value = JSON.stringify(selected);
}

function rdGetSelectedSchedule() {
    var hdnBatch = document.getElementById("rdScheduleBatch");

    if (!hdnBatch.value)
        return [];
    var list = JSON.parse(hdnBatch.value);
    return list;
}

function rdDeselectSchedule(sTaskID) {
    if (!sTaskID)
        return;

    var selected = rdGetSelectedSchedule();
    var i = selected.indexOf(sTaskID);
    if (i < 0)
        return;

    selected.splice(i, 1);

    document.getElementById("rdScheduleBatch").value = JSON.stringify(selected);
}

function rdBatchRemoveSchedules() {
    var selected = rdGetSelectedSchedule();
    if (selected && selected.length > 0) {
        var rdSchedulerTaskID = [];
        var rdBookmarkCollection = [];
        var rdBookmarkID = [];
        var rdBookmarkUserName = [];
        var temp;
        for (var i = 0, len = selected.length; i < len; i++) {
            temp = selected[i].split(',');
            rdSchedulerTaskID.push(temp[0]);
            rdBookmarkCollection.push(temp[1]);
            rdBookmarkID.push(temp[2]);
            rdBookmarkUserName.push(temp[3]);
        }
        var url = "rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=RcRemoveSchedule";
        url += "&rdSchedulerTaskID=" + rdSchedulerTaskID.join(",");
        url += "&rdBookmarkCollection=" + rdBookmarkCollection.join(",");
        url += "&rdBookmarkID=" + rdBookmarkID.join(",");
        url += "&rdBookmarkUserName=" + rdBookmarkUserName.join(",");
        rdAjaxRequestWithFormVars(url);
        rdAjaxRequest("rdAjaxCommand=RefreshElement&rdReport=InfoGo.goScheduleManager&rdRefreshElementID=divMainContent")
    }
}