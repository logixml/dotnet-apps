if (!exists("rdRequire")) {
  dir = getwd()
  library_dir = paste(dir, ifelse(endsWith(dir, "/"), "../", "/../"), "library", sep = "")
  rm(dir)

  # print(library_dir)
  if (!dir.exists(library_dir)) {
    dir.create(library_dir)
  }

  .libPaths(library_dir)

  rdRequire = function(package = stop("Missing package"), version = NULL, allowDownload = FALSE) {
    Messages = c()
    success = FALSE

    repos = c("https://cloud.r-project.org", "https://cran.r-project.org")
    minor = substr(R.version$minor, 1, as.integer(regexpr("\\.", R.version$minor)) - 1)

    lib.name = package
    lib.cran.url = ""
    lib.archive.url = ""
    local.url = ""
    
    if (length(version) == 1 && typeof(version) == "character" && nchar(version) > 0) {

      if (.Platform$OS.type == "windows") {
        # check for binary version
        use.binary = TRUE
        pkgType = "win.binary"

        local.url = paste0("../packages/win.binary/", lib.name, "_", version, ".zip")
        use.version = FALSE
      } else {
        use.binary = FALSE
        pkgType = "source"

        local.url = paste0("../packages/source/", lib.name, "_", version, ".tar.gz")
        use.version = TRUE
      }

      if (file.exists(local.url)) {
        lib.url = local.url
        isUrl = TRUE
      } else if (.Platform$OS.type == "windows") {
        isUrl = FALSE
      } else {
        lib.url = paste0(repos, "/src/contrib/", lib.name, "_", version, ".tar.gz")
        lib.archive.url = paste0(repos, "/src/contrib/Archive/", lib.name, "/", lib.name, "_", version, ".tar.gz")
        isUrl = TRUE
      }
    } else {
      use.version = FALSE
      isUrl = FALSE
    }

    reqTc = withCallingHandlers(tryCatch({

      lib.added = FALSE

      requireError = tryCatch({
        library(package = lib.name, lib.loc = library_dir, character.only = TRUE)
        lib.added = TRUE
      }, error = function(err) {
        return(err$message)
      })

      if (lib.added != TRUE) {
        if (isUrl == TRUE) {
          print(paste0("Installing ", lib.name, " from ", lib.url, " (", pkgType, ")"))
          install.success = FALSE

          instResult = withCallingHandlers(tryCatch({
            install.packages(pkgs = lib.url, lib = library_dir, type = pkgType, repos = NULL)
            library(package = lib.name, lib.loc = library_dir, character.only = TRUE)
            install.success = TRUE
          }, error = function(err) {
            print(err$message)
            return(err$message)
          }), warning = function(war) {
            print(war$message)
            listName = "Messages"
            envir = parent.env(environment())
            assign(listName, c(envir[[listName]], war$message), envir = envir)
            invokeRestart("muffleWarning")
          })

          if (install.success == FALSE && nchar(lib.archive.url) > 0) {
            lib.url = lib.archive.url
            print(paste0("Installing ", lib.name, " from ", lib.url, " (", pkgType, ")"))
            instResult = withCallingHandlers(tryCatch({
              install.packages(pkgs = lib.url, lib = library_dir, type = pkgType, repos = NULL)
              library(package = lib.name, lib.loc = library_dir, character.only = TRUE)
            }, error = function(err) {
              print(err$message)
              return(err$message)
            }), warning = function(war) {
              print(war$message)
              listName = "Messages"
              envir = parent.env(environment())
              assign(listName, c(envir[[listName]], war$message), envir = envir)
              invokeRestart("muffleWarning")
            })
          }

          if (typeof(instResult) == "character") {
            Messages = c(Messages, instResult)
          }
        } else if (allowDownload == TRUE) {
          print(paste("Local package not found. Installing latest version of", lib.name, " from", repos))
          install.packages(pkgs = lib.name, lib = library_dir, repos = repos, quiet = TRUE, dependencies = TRUE, type = "win.binary")
        } else {
          stop(paste0("Package ", lib.name, " not found."))
        }
      }

      library(package = lib.name, lib.loc = library_dir, character.only = TRUE)
      success = TRUE
    }, error = function(err) {
      # print(paste("Error:", err$message))
      return(err$message)
    }), warning = function(war) {
      # print(paste("Warning:", war$message))
      listName = "Messages"
      envir = parent.env(environment())
      assign(listName, c(envir[[listName]], war$message), envir = envir)
      invokeRestart("muffleWarning")
    })

    if (typeof(reqTc) == "character") {
      Messages = c(Messages, reqTc)
    }

    results = list()
    results$success = success
    results$Messages = Messages

    if (success != TRUE) {
      # It is OK for rJava to fail on Windows - as long as we are not using JDBC
      # This will happen if the machine does not have a working Java installation
      # Java is not required for Windows, but if Java exists then even Windows can use it - so we try
      absorb.error = FALSE

      # if the package is rJava, and this is windows, and we are not using JDBC, then absorb the error and move on
      if (sum(regexpr("rJava", Messages) > 0) > 0) {
        # error installing rJava - this is expected on Windows as long as we are not using JDBC
        if (.Platform$OS.type == "windows" && (!exists("SourceType") || SourceType != "JdbcConnectionString")) {
          absorb.error = TRUE
        } else {
          # figure out why this failed - it could be a missing or incorrect JAVA_HOME environment variable
          java.home = Sys.getenv("JAVA_HOME")
          if (length(java.home) == 0 || typeof(java.home) != "character" || nchar(java.home) < 1) {
            # the JAVA_HOME environment variable is missing
            Messages = c(Messages, "The JAVA_HOME environment variable was not found.")
          } else if (!file.exists(java.home)) {
            # the JAVA_HOME environment variable is pointing to an invalid or inaccessible path
            Messages = c(Messages, paste0("The path pointed to by the JAVA_HOME environment variable is invalid or inaccessible: ", java.home))
          }
        }
      }

      if (absorb.error != TRUE) {
        stop(Messages)
      }
    }

    return(results)
  }

  rdRequirePackages = function(packages = stop("Missing packages"), allowDownload = FALSE) {
    pkgs = names(packages)
    if (length(pkgs) == 0 || typeof(pkgs[[1]]) != "character" || nchar(pkgs[[1]]) == 0) {
      pkgs = packages
      use.versions = FALSE
    } else {
      use.versions = TRUE
    }

    version = NULL

    for (reqLib in pkgs) {
      package = reqLib

      if (use.versions == TRUE) {
        version = packages[[package]]
      }

      suppressWarnings(suppressMessages({
        rdRequire(package = package, version = version, allowDownload = allowDownload)
      }))
    }
  }
}
