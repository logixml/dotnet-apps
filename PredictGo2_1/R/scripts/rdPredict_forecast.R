# WARNING
# running this script directly will create a new table in QASQL2K8 > GarciaDev named Prediction_Results_{UUID}
# to create the data for this test, run training_data_setup.sql
# if you get tons of Prediction_Results_xxxx tables, clean up code is at the bottom

# Start clean
if (!exists('profilingOutputFilePath')) {
  rm(list = setdiff(ls(), 'commandArgs'))
}

# this conditional block is for testing without command line arguments
testing = (length(commandArgs(trailingOnly = TRUE)) == 0)
if (testing) {
  setwd("/PredictGo/Web/R/workspace")
  commandArgs = function(...) {
    lastFileNames <- lapply(list.files(getwd(), full.names = TRUE, pattern = ".*rdParams.*xml"), function(x) file.info(x))
    lastFileName <- lastFileNames[order(sapply(lastFileNames, `[[`, i = "ctime"))][length(lastFileNames)]
    return(c(methods::slot((lastFileName[[1]]), "row.names")))
  }
}

source("../scripts/rdProfilingScript.R")
if (rdProfile("../scripts/rdPredict_forecast.R")) {
  rm(list = c('profilingOutputFilePath'))
  write("\ndone\n", stdout())
  return()
}

source("../scripts/rdSetInputParams.R")
rdCheckInputParams(requiredParams = c("TargetConnection",
                                    "SaveMethod",
                                    "TargetTable"),
                   optionalParams = c("TargetType",
                                    "QuotePrefix",
                                    "QuoteSuffix",
                                    "TargetKeyColumns",
                                    "PredictedTimeColumn",
                                    "TimestampLiteral"))
param_node = toString.XMLNode(input_param_node)

source("../scripts/rdRequire.R")
rdRequirePackages(c("caret"))

source("../scripts/rdModelManager.R")
source("../scripts/rdDataFrameToXml.R")
source("../scripts/rdListToXml.R")
source("../scripts/rdUtility.R")
source("../scripts/rdPredict.R")

predictNumber = function() {
  training_model = NULL
  training_data = NULL

  rdLoadModel(PredictionModelFile)

  if (!exists("PredictionType") || typeof(PredictionType) != "character") {
    if (AlgorithmName == "forecastDecisionTree") {
      PredictionType = c("vector", "prob", "class", "matrix")
    } else {
      PredictionType = "response"
    }
  }

  # Load pre-existing training model if specified and it exists
  if (is.null(PredictionModelFile) | !file.exists(PredictionModelFile)) {
    stop(paste("Missing PredictionModelFile", PredictionModelFile))
  }

  if (is.null(training_model)) {
    stop(paste("Invalid training model file", PredictionModelFile))
  }

  # AA-130 - Predictions should use cleanup options from training
  # override clean config with the one used from training if available
  if (typeof(training_model$clean.config) == "list") {
    CleanConfig = training_model$clean.config
  }

  results = rdGetData(SourceType = SourceType,
                      SourceConnection = SourceConnection,
                      SourceQuery = SourceQuery,
                      ColumnMap = ColumnMap,
                      DateColumns = DateColumns,
                      CategoricalColumns = CategoricalColumns,
                      clean.config = CleanConfig,
                      is.training = FALSE,
                      PredictedColumn = PredictedColumn)

  clean.results = results$clean.results
  predictds = results$dat
  CategoricalColumns = results$CategoricalColumns
  OriginalDateData = results$OriginalDateData

  PredictedRowCount = nrow(predictds)

  Messages = c()

  predictSuccess = FALSE
  rowsSaved = 0
  predicted.min = NA
  predicted.max = NA
  predicted.mean = NA

  if (PredictedRowCount < 1) {
    predictSuccess = TRUE
  } else {
    clmdl = training_model$clmdl
    dependent_names = training_model$dependent_names
    PredictableColumn = training_model$PredictableColumn

    if (exists("training.center", where = training_model)) {
      training.center = training_model$training.center
      training.scale = training_model$training.scale
    } else {
      training.center = TRUE
      training.scale = TRUE
    }

    # save copy of unscaled prediction data set  
    if (typeof(clean.results) == "list") {
      predictds_unscaled = clean.results$orig.ds
    } else {
      predictds_unscaled = cbind(predictds)
    }

    factorResp = rdFactorToNumeric(dat = predictds, cat.columns = CategoricalColumns)

    predictds = factorResp$FactoredData
    factor_names = factorResp$FactorNames
    numeric_dependent_names = intersect(factorResp$NumericNames, dependent_names)

    # Scale numeric dependent columns
    if (length(numeric_dependent_names) > 0) {
      results = rdScale(df = predictds[, colnames(predictds) %in% numeric_dependent_names, drop = FALSE], center = training.center, scale = training.scale)
      predictds[, colnames(predictds) %in% numeric_dependent_names] = results$df
    }

    # AA-1000
    missing.columns = setdiff(attr(clmdl$terms, "term.labels"), colnames(predictds))
    if (length(missing.columns) > 0) {
      predictds[, missing.columns] = 0
    }

    # Predict New Results
    predResp = rdPredict(clmdl,
                         newdata = predictds,
                         factor_names = factor_names,
                         PredictableColumn = PredictableColumn,
                         AsEnum = FALSE,
                         PredictionType = PredictionType)

    Messages = c(Messages, predResp$Messages)
    predictSuccess = predResp$Success

    if (predictSuccess == TRUE) {
      y_prednew = predResp$PredictedValues

      predicted.min = min(y_prednew, na.rm = TRUE)
      predicted.max = max(y_prednew, na.rm = TRUE)
      predicted.mean = mean(y_prednew)

      # restore unscaled values
      predictds = predictds_unscaled

      # restore original date data
      for (DateCol in DateColumns) {
        predictds[[DateCol]] = OriginalDateData[[DateCol]]
      }

      # apply predictions
      predictds[[PredictedColumn]] = y_prednew

      if (!exists("QuotePrefix")) {
        QuotePrefix = "["
      }

      if (!exists("QuoteSuffix")) {
        QuoteSuffix = "]"
      }

      if (!exists("TargetType")) {
        TargetType = NULL
      }

      if (!exists("TargetKeyColumns")) {
        TargetKeyColumns = NULL
      }

      results = rdSavePrediction(predictds = predictds,
                                   TargetType = TargetType,
                                   TargetConnection = TargetConnection,
                                   QuotePrefix = QuotePrefix,
                                   QuoteSuffix = QuoteSuffix,
                                   SaveMethod = SaveMethod,
                                   TargetKeyColumns = TargetKeyColumns,
                                   TargetSchema = TargetSchema,
                                   TargetTable = TargetTable,
                                   PredictedColumn = PredictedColumn,
                                   PredictedTimeColumn = PredictedTimeColumn,
                                   TimestampLiteral = TimestampLiteral,
                                   TargetSyntax = TargetSyntax,
                                   DateColumns = DateColumns)

      rowsSaved = results$rowsSaved
      Messages = c(Messages, results$Messages)
      predictSuccess = results$success
    }
  }

  if (predictSuccess != TRUE) {
    PredictedRowCount = 0
  } else {
    PredictedRowCount = rowsSaved
  }

  output_node = newXMLNode("OutputParams")
  addAttributes(output_node,
                "PredictionSuccess" = predictSuccess,
                "PredictedRowCount" = as.character(PredictedRowCount),
                "PredictionResult" = paste(unique(Messages), collapse = "\n"))

  if (!is.na(predicted.min)) {
    addAttributes(output_node, "Min" = predicted.min)
  }

  if (!is.na(predicted.max)) {
    addAttributes(output_node, "Max" = predicted.max)
  }

  if (!is.na(predicted.mean)) {
    addAttributes(output_node, "Mean" = predicted.mean)
  }

  rdSaveModel(file = PredictionResultsFile,
              param_node = param_node,
              output_node = output_node,
              sDocumentElementName = "PredictionRun",
              clean.results = clean.results)
}

predictNumber()

if (!exists('profilingOutputFilePath')) {
  write("\ndone\n", stdout())
}

################################################
# SQL Clean up code                            #
################################################
# DECLARE @cmd varchar(4000)                   #
# DECLARE cmds CURSOR FOR                      #
# SELECT 'DROP TABLE [' + Table_Name + ']'     #
# FROM INFORMATION_SCHEMA.TABLES               #
# WHERE Table_Name LIKE 'Prediction_Results_%' #
#                                              #
# OPEN cmds                                    #
# WHILE 1 = 1                                  #
# BEGIN                                        #
#   FETCH cmds INTO @cmd                       #
#   IF @@fetch_status != 0 BREAK               #
#   EXEC(@cmd)                                 #
# END                                          #
# CLOSE cmds;                                  #
# DEALLOCATE cmds                              #
################################################
