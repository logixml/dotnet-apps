source("../scripts/rdRequire.R")
rdRequirePackages(c("XML", "uuid"))

rdSaveModel = function(..., list = character(),
                        file = stop("'file' must be specified"),
                        envir = parent.frame(),
                        param_node = NULL,
                        output_node = NULL,
                        sDocumentElementName = "PredictionModel",
                        clean.results = NA) {

  rDataFile = paste0(file, ".RData")

  glmTryCatch = withCallingHandlers(tryCatch({
    if (length(...) > 0) {
      save(..., list = list, file = rDataFile, envir = envir)
    }
  }, error = function(err) {
    return(err$message)
  }))


  #sz = file.info(tmpFile)[1, "size"]

  #bObj = readBin(tmpFile, "raw", sz)
  #file.remove(tmpFile)
  #sObj = base64encode(bObj, size=sz)

  doc = newXMLDoc()
  node = newXMLNode(sDocumentElementName)
  addChildren(doc, node)

  if (exists("PredictGoVersion") && typeof(PredictGoVersion) == "character" && length(PredictGoVersion) == 1 && nchar(PredictGoVersion) > 0) {
    addAttributes(node, "Version" = PredictGoVersion)
  }

  if (exists("ServerEngineVersion") && typeof(ServerEngineVersion) == "character" && length(ServerEngineVersion) == 1 && nchar(ServerEngineVersion) > 0) {
    addAttributes(node, "ServerEngineVersion" = ServerEngineVersion)
  }

  node2 = newXMLNode("RData")
  addChildren(node, node2)
  addAttributes(node2, "Filename" = rDataFile)

  if (typeof(param_node) == "character") {
    param_node = xmlParseString(xml(param_node), doc = doc)
  }

  ColumnMap = NULL
  AlgorithmName = NULL

  if (typeof(param_node) == "externalptr") {
    # Remove ODBC connection string
    SourceType = xmlGetAttr(param_node, "SourceType")
    if (length(SourceType) > 0 && (SourceType == "OdbcConnectionString" || SourceType == "JdbcConnectionString")) {
      removeAttributes(param_node, .attrs = c("SourceConnection"))
    }

    TargetType = xmlGetAttr(param_node, "TargetType")
    if (length(TargetType) > 0 && (TargetType == "OdbcConnectionString" || TargetType == "JdbcConnectionString")) {
      removeAttributes(param_node, .attrs = c("TargetConnection"))
    }

    #Save ColumnMap for Output
    ColumnMap = getNodeSet(param_node, "List[@Name='ColumnMap' and @IsNamedList='True']")
    if (length(ColumnMap) == 1) {
      ColumnMap = ColumnMap[[1]]
    } else {
      ColumnMap = NULL
    }

    addChildren(node, param_node)

    #Save algorithm name
    AlgorithmName = xmlGetAttr(param_node, "AlgorithmName")
  }

  if (typeof(output_node) == "character") {
    output_node = xmlParseString(xml(output_node), doc = doc)
  }

  if (typeof(output_node) == "externalptr") {
    if (!is.null(ColumnMap)) {
      addChildren(output_node, ColumnMap)
    }

    if (exists("PredictedColumn") && length(PredictedColumn) == 1 && typeof(PredictedColumn) == "character" && nchar(PredictedColumn) > 0) {
      addAttributes(output_node, "PredictedColumn" = PredictedColumn)
    }

    if (!is.null(AlgorithmName)) {
      addAttributes(output_node, "AlgorithmName" = AlgorithmName)
    }

    addChildren(node, output_node)

    if (typeof(clean.results) == "list") {
      # NonCategoricalText
      # PredictionModel/OutputParams/NonCategoricalText/Column
      parent_node = newXMLNode("NonCategoricalText")
      addChildren(output_node, parent_node)

      for (column in clean.results$NonCategoricalText) {
        caption = rdMadeNameToCaption(column)
        originalId = rdMadeNameToOriginalID(column)
        node = newXMLNode("Column")
        addChildren(parent_node, node)
        addAttributes(node, "Caption" = enc2utf8(caption),
                        "OriginalID" = enc2utf8(originalId))
      }

      # ExceededFactorLevelCutoff
      # PredictionModel/OutputParams/ExceededFactorLevelCutoff/Column
      parent_node = newXMLNode("ExceededFactorLevelCutoff")
      addChildren(output_node, parent_node)
      addAttributes(parent_node, "FactorLevelCutoff" = clean.results$clean.config$factorlevelcutoff)

      for (column in clean.results$ExceededFactorLevelCutoff) {
        caption = rdMadeNameToCaption(column)
        originalId = rdMadeNameToOriginalID(column)
        node = newXMLNode("Column")
        addChildren(parent_node, node)
        addAttributes(node, "Caption" = enc2utf8(caption),
                        "OriginalID" = enc2utf8(originalId))
      }

      # ExceededNumericNACutoff
      # PredictionModel/OutputParams/ExceededNumericNACutoff/Column
      parent_node = newXMLNode("ExceededNumericNACutoff")
      addChildren(output_node, parent_node)
      addAttributes(parent_node, "NumericNACutoff" = clean.results$clean.config$numericnacutoffpercentage)

      for (column in clean.results$ExceededNumericNACutoff) {
        caption = rdMadeNameToCaption(column)
        originalId = rdMadeNameToOriginalID(column)
        node = newXMLNode("Column")
        addChildren(parent_node, node)
        addAttributes(node, "Caption" = enc2utf8(caption),
                        "OriginalID" = enc2utf8(originalId))
      }

      # NearZeroVarianceColumns
      # PredictionModel/OutputParams/NearZeroVarianceColumns/Column
      parent_node = newXMLNode("NearZeroVarianceColumns")
      addChildren(output_node, parent_node)
      for (column in clean.results$NearZeroVarianceColumns) {
        caption = rdMadeNameToCaption(column)
        originalId = rdMadeNameToOriginalID(column)
        node = newXMLNode("Column")
        addChildren(parent_node, node)
        addAttributes(node, "Caption" = enc2utf8(caption),
                        "OriginalID" = enc2utf8(originalId))
      }

      # HighCorrelationColumns
      # PredictionModel/OutputParams/HighCorrelationColumns/Column
      parent_node = newXMLNode("HighCorrelationColumns")
      addAttributes(parent_node, "CorrelationCutoff" = clean.results$clean.config$corcutoffvalue)
      addChildren(output_node, parent_node)

      for (column in clean.results$HighCorrelationColumns) {
        caption = rdMadeNameToCaption(column)
        originalId = rdMadeNameToOriginalID(column)
        node = newXMLNode("Column")
        addChildren(parent_node, node)
        addAttributes(node, "Caption" = enc2utf8(caption),
                        "OriginalID" = enc2utf8(originalId))
      }

      # DuplicateRowCnt
      addAttributes(output_node, "DuplicateRowCnt" = clean.results$DuplicateRowCnt)

      # PredictableColumnNullCount
      addAttributes(output_node, "PredictableColumnNullCount" = clean.results$PredictableColumnNullCount)
    }
  }

  saveXML(doc, file = file, encoding = "UTF-8")
}

rdLoadModel = function(file = stop("'file' must be specified"), envir = parent.frame()) {
  if (!file.exists(file)) {
    return(NULL)
  }

  rDataFile = paste0(file, ".RData")

  if (!file.exists(rDataFile)) {
    doc = xmlParse(file)
    node = xmlChildren(xmlRoot(doc))$RData

    if (is.null(node)) {
      return(NULL)
    }

    rDataFile = xmlAttrs(node)["Filename"]

    if (is.null(rDataFile) | is.na(rDataFile) | rDataFile == "" | !file.exists(rDataFile)) {
      sObj = xmlAttrs(node)["Base64"]
      if (is.na(sObj)) {
        stop("RData workspace information is not found.")
        return(NULL)
      }
      bObj = base64decode(sObj, "raw")
      tmpFile = paste0("rd", UUIDgenerate())
      writeBin(bObj, con = tmpFile, useBytes = TRUE)
      load(tmpFile, envir = envir)
      file.remove(tmpFile)

      return(NULL)
    }
  }

  load(rDataFile, envir = envir)
}
