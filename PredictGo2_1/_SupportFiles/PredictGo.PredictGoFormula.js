// to prevent rdSuperScript from inserting var lines, declare function arguments as dummy variables like this with a dummy one at the end
var sStart,sEnd,s,sUpdateTime,sNow,sMap,sOrigId,targetColumn,stringlist,sep,charLimit,index,str,predValCol,predCaption,takenNames,mainProbCol,cat,dummy;

function goGetDurationMinutes(sStart, sEnd) {
	var dtStart,dtEnd,diff,mins,dummy;
	
	dtStart = new Date(sStart);
	dtEnd = new Date(sEnd);
	diff = dtEnd.getTime() - dtStart.getTime();

	mins = Math.floor(diff / (1000 * 60));
	
	return mins;
}

function goDate(s) {
	if (s.indexOf("-") != 4)
		return new Date(s);
	
	var year,month,day,time,dummy;
	
	year = s.substr(0,4);
	month = s.substr(5,2);
	day = s.substr(8,2);
	time = s.substr(11,8);
	
	return new Date(month + "/" + day + "/" + year + " " + time);
}

function isOld(sUpdateTime) {
	var updateTime,s,now,dummy;
	
	updateTime = goDate(sUpdateTime);
	s = updateTime.getSeconds();
	updateTime.setSeconds(s + 30);
	now = new Date();
	now = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours(), now.getMinutes(), now.getSeconds());
	
	if (updateTime > now)
		return "False";
	
	return "True";
}

function goGetDurationCaption(sStart, sEnd) {
	var dtStart,dtEnd,diff,nSeconds,sCaption,dummy;
	
	dtStart = new Date(sStart);
	dtEnd = new Date(sEnd);
	diff = dtEnd.getTime() - dtStart.getTime();
	
	if (!diff)
		return "";
	
	nSeconds = Math.round(diff / 1000);

	if (nSeconds < 180) {
		if (nSeconds == 1)
			sCaption = "1 second"
		else
			sCaption = nSeconds + " seconds"
	} else {
		sCaption =  Math.round(nSeconds / 60)
		sCaption = sCaption + " minutes"
	}
	
	return sCaption;
}

function goGetColumnCaption(sOrigId, sMap) {
	var i,iEnd,iStart,j,dummy;
	
	i = sMap.indexOf("OriginalID=\"" + sOrigId + "\"");
	
	if (i < 0)
		return sOrigId;
	
	iEnd = sMap.indexOf("/", i);
	
	if (iEnd < 1)
		return sOrigId;
	
	iStart = sMap.substr(0, iEnd).lastIndexOf("<Column ");
	
	if (iStart < 0)
		return sOrigId;
	
	sMap = sMap.substr(iStart, iEnd - iStart);
	
	i = sMap.indexOf("Caption=\"");
	
	if (i < 0)
		return sOrigId;
	
	i += 9;
	
	j = sMap.indexOf("\"", i);
	
	if (j < 0)
		return sOrigId;
	
	return sMap.substr(i, j - i);
}

function goOriginalIDToCaption(sOrigId, sMap) {
	var i,iEnd,iStart,j,dummy;
	
	i = sMap.indexOf("OriginalID=\"" + sOrigId + "\"");
	
	if (i < 0)
		return sOrigId;
	
	iEnd = sMap.indexOf("/", i);
	
	if (iEnd < 1)
		return sOrigId;
	
	iStart = sMap.substr(0, iEnd).lastIndexOf("<Column ");
	
	if (iStart < 0)
		return sOrigId;
	
	sMap = sMap.substr(iStart, iEnd - iStart);
	
	i = sMap.indexOf("Caption=\"");
	
	if (i < 0)
		return sOrigId;
	
	i += 9;
	
	j = sMap.indexOf("\"", i);
	
	if (j < 0)
		return sOrigId;
	
	return sMap.substr(i, j - i);
}

function goBuildProbabilityName(targetColumn){
	var i,dummy;
	
	i = targetColumn.lastIndexOf('_Predicted');
	
	if (targetColumn.length > 10 && (i == (targetColumn.length - 10))) {
		targetColumn = targetColumn.substr(0, i);
	}
	
	return targetColumn + '_Probability';
}

function goBuildProbabilityCaption(targetColumn){
	var i,dummy;
	
	targetColumn = targetColumn.replace(/_/g, ' ');
	
	i = targetColumn.lastIndexOf(' Predicted');
	
	if (targetColumn.length > 10 && (i == (targetColumn.length - 10))) {
		targetColumn = targetColumn.substr(0, i);
	}
	
	return targetColumn + ' Probability';
}

function goBuildCaption(targetColumn){
	var i,dummy;
	
	targetColumn = targetColumn.replace(/_/g, ' ');
	
	return targetColumn;
}

function goUniquify(stringlist, sep, charLimit) {
	// to prevent rdSuperScript from inserting var lines, declare variables like this with a dummy one at the end
	var strings,uniqueStrings,str,uniquifier,keepGoing,uniqStr,i,j,dummy;
	
	strings = stringlist.split(",");
	uniqueStrings = [];
	
	for (i = 0; i < strings.length; i++) {
		str = strings[i];
		
		if (charLimit > 0 && str.length > charLimit)
			str = str.substr(0,charLimit);
		
		uniquifier = 2;
		keepGoing = true;
		
		while (keepGoing) {
			keepGoing = false;
			
			for (j = 0; j < uniqueStrings.length; j++) {
				if (uniqueStrings[j]==str) {
					keepGoing = true;
					str = strings[i];
					uniqStr = uniquifier.toString();
					
					if (charLimit > 0
					&& str.length > 0
					&& sep.length + uniqStr.length < charLimit
					&& str.length + sep.length + uniqStr.length > charLimit) {
						str = str.substr(0, charLimit - sep.length - uniqStr.length) + sep + uniqStr;
					} else {
						str = str + sep + uniqStr;
					}
					
					uniquifier++;
					break;
				}
			}
		}
		
		uniqueStrings.push(str);
	}
	
	return uniqueStrings.join();
}

function goGetItemAtIndex(stringlist, index) {
	var strings,dummy;
	
	strings = stringlist.split(",");
	if (strings.length > index)
		return strings[index];
	
	return "";
}

function goGetIndex(stringlist, str) {
	var strings,i,dummy;
	
	strings = stringlist.split(",");
	
	for (i = 0; i < strings.length; i++) {
		if (strings[i] == str)
			return i;
	}
	
	return -1;
}

function goGenProbColName(predValCol, charLimit, takenNames, outliers) {
	var probCol,idx,dummy;
	
	if (outliers) {
		probCol = predValCol + "_OutlierProbability";
	} else {
		probCol = predValCol + "_Probability";
	
		if (predValCol.length > 10) {
			idx = predValCol.lastIndexOf("_Predicted");
			if (idx == predValCol.length - 10)
				probCol = predValCol.substr(0, idx) + "_Probability";
		}
	}
	
	if (takenNames)
		takenNames += "," + probCol;
	else
		takenNames = probCol;
	
	takenNames = goUniquify(takenNames, "_", charLimit);
	
	idx = takenNames.lastIndexOf(",");
	
	if (idx < 0)
		probCol = takenNames;
	else
		probCol = takenNames.substr(idx + 1);
	
	return probCol;
}

function goGenProbCaption(predCaption, takenNames, outliers) {
	var probCol,idx,dummy;
	
	predCaption = predCaption.replace(/_/g, " ");

	if (outliers) {
		probCol = predCaption + " Outlier Probability";
	} else {
		probCol = predCaption + " Probability";
	
		if (predCaption.length > 10) {
			idx = predCaption.lastIndexOf(" Predicted");
			if (idx == predCaption.length - 10)
				probCol = predCaption.substr(0, idx) + " Probability";
		}
	}
	
	if (takenNames)
		takenNames += "," + probCol;
	else
		takenNames = probCol;
	
	takenNames = goUniquify(takenNames, " ", 0);
	
	idx = takenNames.lastIndexOf(",");
	
	if (idx < 0)
		probCol = takenNames;
	else
		probCol = takenNames.substr(idx + 1);
	
	return probCol;
}

function goGenCatProbColName(mainProbCol, cat, charLimit, takenNames) {
	var probCol,idx,dummy;
	
	probCol = mainProbCol + "_" + cat;
	
	if (takenNames)
		takenNames += "," + probCol;
	else
		takenNames = probCol;
	
	takenNames = goUniquify(takenNames, "_", charLimit);
	
	idx = takenNames.lastIndexOf(",");
	
	if (idx < 0)
		probCol = takenNames;
	else
		probCol = takenNames.substr(idx + 1);
	
	return probCol;
}

function goGenCatProbCaption(mainProbCol, cat, takenNames) {
	var probCol,idx,dummy;
	
	mainProbCol = mainProbCol.replace(/_/g, " ");
	
	probCol = mainProbCol + " " + cat;
	
	if (takenNames)
		takenNames += "," + probCol;
	else
		takenNames = probCol;
	
	takenNames = goUniquify(takenNames, " ", 0);
	
	idx = takenNames.lastIndexOf(",");
	
	if (idx < 0)
		probCol = takenNames;
	else
		probCol = takenNames.substr(idx + 1);
	
	return probCol;
}
