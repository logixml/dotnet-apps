//This sample displays an alert box

function goHighlightModelType(sModelType)
{
	goUnHighlightModelType("Classification")
	goUnHighlightModelType("Forecast")
	goUnHighlightModelType("Clustering")
	goUnHighlightModelType("Outliers")
	
	var eleCaption = document.getElementById("cell" + sModelType)
	var eleArrow = document.getElementById("arrow" + sModelType)
	var elePanel = document.getElementById("panel" + sModelType)
	var eleCreate = document.getElementById("create" + sModelType)

	eleCaption.style.backgroundColor = "#CCCCCC"
	eleArrow.className = ""
	elePanel.className = ""
	eleCreate.className = ""
}

function goUnHighlightModelType(sModelType)
{
	var eleCaption = document.getElementById("cell" + sModelType)
	var eleArrow = document.getElementById("arrow" + sModelType)
	var elePanel = document.getElementById("panel" + sModelType)
	var eleCreate = document.getElementById("create" + sModelType)

	eleCaption.style.backgroundColor = "white"
	eleArrow.className = "ThemeHidden"
	elePanel.className = "ThemeHidden"
	eleCreate.className = "ThemeHidden"

}

function goHoverModelType(sModelType)
{
	goUnHoverModelType("Classification")
	goUnHoverModelType("Forecast")
	goUnHoverModelType("Clustering")
	goUnHoverModelType("Outliers")
	
	var eleCaption = document.getElementById("cell" + sModelType)

	eleCaption.style.border = "1px solid #777777"
}
function goUnHoverModelType(sModelType)
{
	var eleCaption = document.getElementById("cell" + sModelType)

	eleCaption.style.borderRadius = "10px"
	eleCaption.style.border = "1px solid white"
}