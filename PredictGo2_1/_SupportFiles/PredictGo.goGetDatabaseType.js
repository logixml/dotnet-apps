function goGetDatabaseType(sMetadataType, sMetadataSize, sDatabaseType) {
	
switch(sMetadataType) {
	case "Number":
				                return "FLOAT";
    
    case "DateTime":
        switch(sDatabaseType) {
            case "SqlServer":
                                return "DATETIME2";
            case "Oracle":
                                return "TIMESTAMP";
			case "PostgreSQL":
			case "Redshift":
				                return "TIMESTAMP";
			case "DataHub":
				                return "TIMESTAMP WITHOUT TIME ZONE";
			case "MySql":
				                return "DATETIME";
        }

    case "Boolean":
        switch(sDatabaseType) {
            case "SqlServer":
                                return "NVARCHAR(5)";
            case "Oracle":
                                return "NVARCHAR2(5)";
			case "PostgreSQL":
			case "Redshift":
				                return "VARCHAR(5)";
			case "DataHub":
				                return "VARCHAR(5)";
			case "MySql":
				                return "VARCHAR(5)";
        }

    case "Text":
        if (sMetadataSize == '') {
            switch(sDatabaseType) {
                case "SqlServer":
                                    return "NVARCHAR(MAX)";
                case "Oracle":
                                    return "NCLOB";
                case "PostgreSQL":
                                    return "VARCHAR";
				case "Redshift":
                                    return "VARCHAR(MAX)";
                case "DataHub":
                                    return "VARCHAR";
                case "MySql":
                                    return "TEXT";
            }
        } else {
            switch(sDatabaseType) {
                case "SqlServer":
                                    return "NVARCHAR(" + sMetadataSize+ ")";
                case "Oracle":
                                    return "NVARCHAR2(" + sMetadataSize + ")";
                case "PostgreSQL":
				case "Redshift":
                                    return "VARCHAR(" + sMetadataSize + ")";
                case "DataHub":
                                    return "VARCHAR(" + sMetadataSize + ")";
                case "MySql":
                                    return "VARCHAR(" + sMetadataSize + ")";
            } 
        }

	}	

}