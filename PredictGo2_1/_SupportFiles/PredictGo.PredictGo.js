var PG_POLL_TIMEOUT_MS = 5000;

function goAbortTraining(modelTrainingID, modelID, btn) {
	var process = "PredictGo.goTrain";
	var taskID = "TrainAbort";
	var linkParams = {
		ModelID: modelID,
		ModelTrainingID: modelTrainingID
	};
	
	var refreshUrl = goGetRefreshUrl("divPrevRuns", "PredictGo.goModel", linkParams);
	
	var callback = function (res) {
		// response should have the RowsAffected
		//var returnValue = res.target ? res.target.responseText : res.srcElement.responseText;
		// Refresh table to show new status
		rdAjaxRequestWithFormVars(refreshUrl,false,'',null,null,null,null,true);
	};
	
	goProcessAsync(process, taskID, linkParams, callback, false);
}

function goAbortPrediction(planRunID, btn) {
	var process = "PredictGo.goPredictToSql";
	var taskID = "PredictAbort";
	var rdReport = "PredictGo.goPlan";
	var linkParams = {
		PlanRunID: planRunID
	};

	var refreshElementID = "divPrevRuns";
	var refreshUrl = goGetRefreshUrl(refreshElementID, rdReport);

	var callback = function (res) {
		// response should have the RowsAffected
		//var returnValue = res.target ? res.target.responseText : res.srcElement.responseText;
		// Refresh table to show new status
		rdAjaxRequestWithFormVars(refreshUrl, false, '', null, null, null, null, true);
	};

	goProcessAsync(process, taskID, linkParams, callback, false);
}

function goCreateResultTable(modelID, trainingID, planID) {
	if (!goValidateCreateTable())
		return;
	
	var sProcessID = "PredictGo.goCreateDatabaseTable";
	var sTaskID = "CreateTable";
	
	var linkParams = {
		ModelID: modelID,
		TrainingID: trainingID,
		PlanID: planID
	}
	
	var refreshUrl = goGetRefreshUrl("PlanDetailsInputs,divNewTable,divTableCreated,keepMessages", "PredictGo.goPlan", linkParams);
	
	var rdStartTable = document.getElementById("rdStartTable");
	if (rdStartTable && rdStartTable.value)
		linkParams.SourceTableName = rdStartTable.value;
	
	var btn = document.getElementById('divCreateTable');
	btn.className = "ThemeHidden";
	
	ShowElement(null, 'divCreatingTable', 'Show');
	
	var callback = function (res) {
		rdAjaxRequestWithFormVars(refreshUrl, false, '', null, null, null, null, true);
	};
	
	goProcessAsync(sProcessID, sTaskID, linkParams, callback, false);
}

function goSaveModelAgSql() {
	if (window.location.href.indexOf("rdAgCommand=")!=-1 
	  || window.location.href.indexOf("rdAfCommand=")!=-1) {
	  
		//Save SQL
		var sRequest = "rdProcess=PredictGo.goDbUpdate"
		sRequest += "&rdTaskID=ModelUpdateSql"
		sRequest += "&rdIgnoreMissingResponse=True"
		rdAjaxRequest(sRequest,null,null,"true")
	
		//Save Filter Caption
		var sRequest = "rdProcess=PredictGo.goDbUpdate"
		sRequest += "&rdTaskID=ModelUpdateFilter"
		sRequest += "&rdIgnoreMissingResponse=True"
		rdAjaxRequest(sRequest,null,null,"true")
	}
}

function goSavePlanAgSql() {
	if (window.location.href.indexOf("rdAgCommand=")!=-1 
	  || window.location.href.indexOf("rdAfCommand=")!=-1) {
		
		var planID = document.getElementById("PlanID").value;
		
		//Save SQL
		var sRequest = "rdProcess=PredictGo.goDbUpdate"
		sRequest += "&rdTaskID=PlanUpdateSql"
		sRequest += "&rdIgnoreMissingResponse=True"
		sRequest += "&PlanID=" + planID
		rdAjaxRequest(sRequest,null,null,"true")
	
		//Save Filter Caption
		var sRequest = "rdProcess=PredictGo.goDbUpdate"
		sRequest += "&rdTaskID=PlanUpdateFilter"
		sRequest += "&rdIgnoreMissingResponse=True"
		sRequest += "&PlanID=" + planID
		rdAjaxRequest(sRequest,null,null,"true")
	}
}

function goHideDetermineColumnImportance() {
	var elePredictedColumnDropdown = document.getElementById("PredictableColumn")
	if (elePredictedColumnDropdown) {
		if (elePredictedColumnDropdown.options.selectedIndex < 1) {
			//No PredictableColumn selected, hide the option. This is necessary if the PredictedColumn is removed from the AG.
			var eleColumnImportance = document.getElementById("rowColumnImportance")
			eleColumnImportance.parentNode.removeChild(eleColumnImportance)
		}
	}
}

var goTextInputTimeouts = [];
function goGetTimeout(id) {
	var pgTo = null;
	
	for (var i = 0; i < goTextInputTimeouts.length; i++) {
		pgTo = goTextInputTimeouts[i];
		
		if (pgTo.id == id) {
			return pgTo;
		}
	}
	
	pgTo = {
		id : id
	};
	
	goTextInputTimeouts.push(pgTo);
	
	return pgTo;
}

// sets focus and places cursor at the end, ready for more input
var goSetFocus = function (id) {
	if (!id)
		return;

	var ele = document.getElementById(id);
	if (ele) {
		ele.focus();
		ele.setSelectionRange(ele.value.length + 1, ele.value.length + 1);
	}
};

function goGetRefreshUrl(refreshElementID, rdReport, linkParams) {
	var refreshUrl = "rdAjaxCommand=RefreshElement&rdRefreshElementID=" + refreshElementID;
	
	if (rdReport)
		refreshUrl += "&rdReport=" + encodeURIComponent(rdReport);
	
	for (var key in linkParams) {
		if (key == "rdAjaxCommand" || key == "rdRefreshElementID")
			continue;
		
		if (rdReport && key == "rdReport")
			continue;
		
		refreshUrl += "&" + encodeURIComponent(key) + "=" + encodeURIComponent(linkParams[key]);
	}
	
	refreshUrl += "&pgRefresh=" + Math.random();
	
	return refreshUrl;
}

function goCallProcessTask(sProcessID, sTaskID, sParams, refreshElementID, refreshReportID) {
	var linkParams = {};
	var splitParams = sParams.split("&");
	var i;
	for (i = 0; i < splitParams.length; i++) {
		var oneParm = splitParams[i];
		
		if (!oneParm)
			continue;
		
		var parmPair = oneParm.split("=");
		
		if (parmPair.length == 2) {
			if(!refreshReportID && parmPair[0] == "rdReport") {
				refreshReportID = parmPair[1]
			}else{
				linkParams[parmPair[0]] = parmPair[1];
			}
		}
	}
	
	var callback;
	if (refreshElementID) {
		var refreshUrl = goGetRefreshUrl(refreshElementID, refreshReportID, linkParams);
		
		callback = function(res) {
			// Refresh table so that the new pgModelTraining record will show up as "Launching"
			rdAjaxRequestWithFormVars(refreshUrl,false,'',null,null,null,null,true);
		}
	}
	else
		callback = null;

	goProcessAsync(sProcessID, sTaskID, linkParams, callback, false);
}

function goProcessAsync(process, taskID, linkParams, callback, bValidate) {
	var sActionsXml = '<rdProcessAction><Action Async="True" ID="PredictGoAsyncProcess" Process="' + process + '" TaskID="'+taskID+'" Type="Process" Validate="False"><LinkParams ';
	
	if (!linkParams)
		linkParams = {};

	linkParams.rdIgnoreMissingResponse = "True";
	
	var hdnParams = [];
	var hdn;
	
	for (var prop in linkParams) {
		sActionsXml += prop + '="' + linkParams[prop] + '" ';
		
		if (!document.getElementById(prop)) {
			hdnParams.push(prop);
			hdn = document.createElement("INPUT");
			hdn.type = "hidden";
			hdn.id = prop;
			hdn.name = prop;
			hdn.value = linkParams[prop];
			document.rdForm.appendChild(hdn);
		}
	}
	
	sActionsXml += '/></Action></rdProcessAction>';
	
	var waitCfg = {
		async: true,
		onSuccess: callback,
		onFail: callback,
		waitMessage: false
	};
	
	if (bValidate === false)
		bValidate = "false";
	else
		bValidate = "true";
	
	// function RunProcess(sActionsXml, bValidate, sConfirm, sTarget, waitCfg) {
	RunProcess(sActionsXml, bValidate, null, "", waitCfg);
	
	for (var i = 0; i < hdnParams.length; i++) {
		hdn = document.getElementById(hdnParams[i]);
		if (hdn)
			hdn.parentNode.removeChild(hdn);
	}
}

function goGetModelAgRowCount(modelID) {
	return goGetAgRowCount({
		ModelID: modelID,
		rdReport: "PredictGo.goModel"
	});
}
function goGetPlanAgRowCount(modelID, planID) {
	return goGetAgRowCount({
		ModelID: modelID,
		PlanID: planID,
		rdReport: "PredictGo.goPlan"
	});
}
function goGetAgRowCount(linkParams) {
	var process = "PredictGo.goGetAgRowCount";
	var taskID = "tskGetAgRowCount";
	
	var callback = function (res) {
		// response should have the row count formatted
		var rowCount = res.target ? res.target.responseText.trim() : res.srcElement.responseText.trim();
		
		var tr = document.getElementById("rowViewDataRowCount");
		if (tr) {
			if (rowCount == "" || rowCount == "NA")
				tr.style.display = "none";
			else {
				tr.style.display = "";
				document.getElementById("lblRowCount").innerHTML = rowCount;
			}
		}
	}

	return goProcessAsync(process, taskID, linkParams, callback, false);
}

function goDeleteTraining(modelTrainingID, modelID, btn) {
	var process = "PredictGo.goDbUpdate";
	var taskID = "ModelTrainingDelete";
	var rdReport = "PredictGo.goModel";
	var linkParams = {
		ModelID: modelID,
		ModelTrainingID: modelTrainingID
	};
	
	var refreshElementID = "divPrevRuns";
	var refreshUrl = goGetRefreshUrl(refreshElementID, rdReport, linkParams);
	
	var table = document.getElementById("dtTrainingRuns");
	var eleRowRemove = Y.one(btn).ancestor('tr');
	while (eleRowRemove.ancestor('table')._node != table) {
		eleRowRemove = eleRowRemove.ancestor('tr');
	}
	var goDeletedLast = false;
		
	eleRowRemove._node.parentNode.removeChild(eleRowRemove._node);

	if (table && table.rows) {
		if (table.rows.length == 1) {
			var lastRow = table.rows[0];
			if (!lastRow.cells || !lastRow.cells.length || lastRow.cells[0].tagName.toLowerCase() == "th")
				goDeletedLast = true;
		}
		else if (table.rows.length == 0)
			goDeletedLast = true;
	}
	
	var callback;
	
	if (goDeletedLast) {
		callback = function (res) {
			// Table is now empty - Refresh table so that it obeys the report definition attribute HideWhenZeroRows
			rdAjaxRequestWithFormVars(refreshUrl,false,'',null,null,null,null,true);
		};
	}
	else
		callback = null;
	
	// Delete pgModelTraining
	return goProcessAsync(process, taskID, linkParams, callback, false);
}

function goDeletePrediction(planRunID, modelID, btn) {
	var process = "PredictGo.goDbUpdate";
	var taskID = "PredictionRunDelete";
	var rdReport = "PredictGo.goPlan";
	var linkParams = {
		ModelID: modelID,
		PredictionRunID: planRunID
	};
	
	var refreshElementID = "divPrevRuns";
	var refreshUrl = goGetRefreshUrl(refreshElementID, rdReport, linkParams);
	
	var table = document.getElementById("dtPredictionRuns");
	var eleRowRemove = Y.one(btn).ancestor('tr');
	while (eleRowRemove.ancestor('table')._node != table) {
		eleRowRemove = eleRowRemove.ancestor('tr');
	}
	var goDeletedLast = false;
		
	eleRowRemove._node.parentNode.removeChild(eleRowRemove._node);

	if (table && table.rows) {
		if (table.rows.length == 1) {
			var lastRow = table.rows[0];
			if (!lastRow.cells || !lastRow.cells.length || lastRow.cells[0].tagName.toLowerCase() == "th")
				goDeletedLast = true;
		}
		else if (table.rows.length == 0)
			goDeletedLast = true;
	}
	
	var callback;
	
	if (goDeletedLast) {
		callback = function (res) {
			// Table is now empty - Refresh table so that it obeys the report definition attribute HideWhenZeroRows
			rdAjaxRequestWithFormVars(refreshUrl,false,'',null,null,null,null,true);
		};
	}
	else
		callback = null;
	
	// Delete pgModelTraining
	return goProcessAsync(process, taskID, linkParams, callback, false);
}

function goPredictEnable(enable) {
	var btnId = document.getElementById("PlanID") ? "procPredictNow" : "procTrainNow";
	var btn = document.getElementById(btnId);
	
	if (enable) {
		if (btn.className != "ThemeHidden")
			return; // already enabled
		
		if (btn.originalClassName)
			btn.className = btn.originalClassName;
		
		ShowElement(null, 'divLaunching', 'Hide');
		goShowModelSteps();
	} else {
		if (btn.className == "ThemeHidden")
			return; // already disabled
		
		btn.originalClassName = btn.className;
		btn.className = "ThemeHidden";
		ShowElement(null, 'divLaunching', 'Show');
	}
}

var goPredictPending = [];
function goPredictWatch(id) {
	goPredictPending.push(id);
	goPredictEnable(false);
}
/// Returns true if the run was Pending
function goPredictFound(id) {
	var first = false;
	
	for (var i = 0; i < goPredictPending.length; i++) {
		if (goPredictPending[i] == id) {
			goPredictPending.splice(i, 1);
			first = true;
			break;
		}
	}
	
	if (goPredictPending.length == 0) {
		goPredictEnable(true);
	}
	
	return first;
}

/// Returns true if the run is Pending
function goPredictIsPending(id) {
	for (var i = 0; i < goPredictPending.length; i++) {
		if (goPredictPending[i] == id) {
			return true;
		}
	}
	
	return false;
}

function goTrain(modelID) {
	var process = "PredictGo.goTrain";
	var taskID = "Train";
	var modelTrainingID = guid();
	var linkParams = {
		ModelID: modelID,
		ModelTrainingID: modelTrainingID
	};
	
	goPredictWatch(modelTrainingID);
	
	var callback = function (res) {
		if (goPredictIsPending(modelTrainingID)) {
			// Still not shown in the table, but now it should show up if we refresh it
			goPredictPollOne(modelTrainingID, "");
		}
	};

	// Insert new pgModelTraining and start training
	goProcessAsync(process, taskID, linkParams, callback);
	goPredictPoll();
}

function goPredict(planID) {
	var process = "PredictGo.goPredictToSql";
	var taskID = "Predict";
	var planRunID = guid();
	var linkParams = {
		PlanID: planID,
		PlanRunID: planRunID
	};
	
	goPredictWatch(planRunID);
	
	var callback = function (res) {
		if (goPredictIsPending(planRunID)) {
			goPredictPollOne(planRunID, "");
		}
	};

	goProcessAsync(process, taskID, linkParams, callback);
	goPredictPoll();
}

function goGetStatuses(tableID, statusCssClass, idPrefix) {
	var statuses = [];
	
	var table = document.getElementById(tableID);
	if (!table)
		return statuses;
	
	var spans = table.getElementsByClassName(statusCssClass);
	if (!spans || !spans.length)
		return statuses;
	
	for (var i = 0; i < spans.length; i++) {
		var span = spans[i];
		var id = span.id;
		var classes = span.className.split(" ");
		for (var j = 0; j < classes.length; j++) {
			var css = classes[j];
			
			if (css.length <= idPrefix.length || css.indexOf(idPrefix) != 0)
				continue;
			
			// trim prefix
			id = css.substr(idPrefix.length);
			goPredictFound(id);
			break;
		}
		
		var status = span.innerText;
		
		statuses.push({
			id: id,
			status: status
		});
	}
	
	return statuses;
}

var goPoll = [];
function goGetPoll(id, pollType) {
	for (var i = 0; i < goPoll.length; i++) {
		var item = goPoll[i];
		
		if (pollType && pollType != item.pollType)
			continue;
		
		if (item.id == id) {
			return item;
		}
	}
	
	return null;
}

function goRemovePoll(pollObj) {
	if (pollObj.timeout)
		clearTimeout(pollObj.timeout);
	
	pollObj.timeout = null;

	var i = goPoll.indexOf(pollObj);
	
	if (i < 0)
		return false;
	
	return goPoll.splice(i, 1);
}

function goPredictPollOne(id, currentStatus) {
	var isTraining = !document.getElementById("PlanID");
	var pollType = isTraining ? "Training" : "Prediction";
	var pollObj = goGetPoll(id, pollType);

	if (pollObj != null) {
		if (pollObj.timeout)
			clearTimeout(pollObj.timeout);
	}
	else {
		pollObj = {
			id: id,
			pollType: pollType,
			status: currentStatus,
			timeout: null
		}
		goPoll.push(pollObj);
	}

	var process = isTraining ? "PredictGo.goTrain" : "PredictGo.goPredictToSql";
	var taskID = isTraining ? "TrainPoll" : "PredictPoll";
	var refreshElementID = "divPrevRuns";
	var rdReport = isTraining ? "PredictGo.goModel" : "PredictGo.goPlan";
	var linkParams = isTraining ? {
		ModelID: document.getElementById("ModelID").value,
		ModelTrainingID: id
	} : {
		PlanRunID: id
	};

	var refreshUrl = goGetRefreshUrl(refreshElementID, rdReport, linkParams);

	var callback = function (res) {
		var newStatus = res.target ? res.target.responseText : res.srcElement.responseText;

		if (newStatus == pollObj.status) {
			// no change yet - wait and poll again
			if (newStatus == "") {
				// still waiting for table to include the new record - poll now
				goPredictPollOne(pollObj.id, pollObj.status);
			} else {
				// record is already in the table, waiting for status completion
				pollObj.timeout = setTimeout(function () {
					clearTimeout(pollObj.timeout);
					pollObj.timeout = null;
					goPredictPollOne(pollObj.id, pollObj.status);
				}, PG_POLL_TIMEOUT_MS);
			}
		}
		else {
			// status changed!
			goRemovePoll(pollObj);
			// refresh the table and re-start polling afterwards
			rdAjaxRequestWithFormVars(refreshUrl, false, '', null, null, goPredictPoll, null, true);
		}
	};

	// ping the server to see if this is still running
	return goProcessAsync(process, taskID, linkParams, callback, false);
}

var goPredictPollTimeout = null;
function goPredictPoll() {
	if (goPredictPollTimeout) {
		clearTimeout(goPredictPollTimeout);
		goPredictPollTimeout = null;
	}

	var refreshElementID = "divPrevRuns";
	var isTraining = !document.getElementById("PlanID");
	var statusCssClass = isTraining ? "pgTrainingStatus" : "pgPredictionStatus";
	var idPrefix = "pgStatus_";
	var statuses = goGetStatuses(refreshElementID, statusCssClass, idPrefix);
	var pollCnt = goPredictPending.length;
	var i;
	for (i = 0; i < goPredictPending.length; i++) {
		goPredictPollOne(goPredictPending[i], "");
	}

	for (i = 0; i < statuses.length; i++) {
		var statusObj = statuses[i];

		if (statusObj.status == "Running" || statusObj.status == "Launching") {
			goPredictPollOne(statusObj.id, statusObj.status);
			pollCnt++;
		}
	}

	if (pollCnt) {
		goPredictPollTimeout = setTimeout(function () {
			clearTimeout(goPredictPollTimeout);
			goPredictPollTimeout = null;
			goPredictPoll();
		}, PG_POLL_TIMEOUT_MS);
	} else if (statuses.length) {
		goShowModelSteps();
	}
}

function goRefreshPlanAg(planID) {
	var refreshUrl = goGetRefreshUrl("ag", "PredictGo.goPlan", {PlanID: planID});
	rdAjaxRequestWithFormVars(refreshUrl,false,'',null,null,null,null,true);
}

function goShowPredictedValuesAg(id, planID) {
	ShowElement(id,'popupViewPredictedRows','','');
	var closeBtns = document.getElementsByClassName("goPredictedValuesClose");
	for(var i = 0; i < closeBtns.length;i++) {
		var closeBtn = closeBtns[i];
		if (closeBtn.hasRefresh)
			continue;
		
		var planID2 = planID;
		closeBtn.addEventListener("click", function() {
			goRefreshPlanAg(planID2);
		});
		closeBtn.hasRefresh = true;
	}
}

//https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
function guid() {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}
	return (s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4()).toString();
}