function goEncodeXmlName(name) {
	// Element names can contain letters, digits, hyphens, underscores, and periods
	// \w = word character = a-z, A-Z, 0-9, and _ (underscore)
	// the following regex encodes all characters that are not valid XML element names
	// example: spaces become _x0020_, question marks become _x003F_, etc
	return name.replace(/[^\w\-\.]/g, function(x) {
		// for each character in the string that is not a letter, number, digit, hyphen, underscore, or period
		// encode as _x0000_ with appropriate 4 digit hex code
		var fourdig = "000" + Number(x.charCodeAt(0)).toString(16).toUpperCase();
		fourdig = fourdig.substr(fourdig.length - 4);
		return "_x" + fourdig + "_";
	})
}

function goSetSelectedColumn(eleCheckbox, sColumnID) {
	var sAgCheckboxName = goEncodeXmlName("agCol_" + sColumnID)
	var nodAgCheckbox = window.parent.Y.one("input[value='" + sAgCheckboxName + "']")
	if (!nodAgCheckbox) {  //Not found, it's a formula column, use a differeint ID.
		nodAgCheckbox = window.parent.Y.one("input[value='" + sColumnID + "']")
	}
	if (nodAgCheckbox) {
		nodAgCheckbox._node.checked = eleCheckbox.checked
		//Populate the checkbox group's hidden input.  This code is lifted from Info.
			var sInputListID = "iclLayout"
		    var inputListElement = window.parent.Y.one("#" + sInputListID);
		    var hiddenCaptionField = window.parent.Y.one("#" + "rdICL-" + sInputListID );

		    var tempSelectedValues = "";
		    var selectedVals = [];

		    var checkAllCaptureId = "#" + sInputListID + "_check_all";
		    var checkboxSelector = "input[type=\"checkbox\"]:not(" + checkAllCaptureId + ")";

		    var items = inputListElement.all(checkboxSelector);
		    if (items && items.size() > 0) {
		        for (var i = 0; i < items.size() ; i++) {
		            if (items.item(i).get("checked")) {	
		                if (!(items.item(i).hasAttribute("rdExpanded"))) {
		                    var sVal = items.item(i).get("value");
		                    if (selectedVals.indexOf(sVal) == -1) {
		                        selectedVals.push(sVal)
		                    }
		                }		                
		            }
		        }		        
		        //var inpDelimiter = this._container.getAttribute("rdinputvaluedelimiter"); 
				var inpDelimiter = ","; 
		        tempSelectedValues = selectedVals.join(inpDelimiter);
		    }
		hiddenCaptionField.setAttribute("value", tempSelectedValues);

		//Hide the Calculate button.
		var eleCalculate = document.getElementById("rowCalculate")
		eleCalculate.className = "ThemeHidden"

		//Show the OK button
		var eleOk = document.getElementById("rowSaveSelections")
		eleOk.className = ""
	} else {
		alert ("Sorry, this column is not in the Data.")
	}
}

function goSaveSelectedColumns() {
	//Fire the AG link for saving column selections.
	var eleOkLink = window.parent.document.getElementById("actSetLayout")
	var sScript = eleOkLink.href.replace("javascript:","")
	//Add a parameters so the ColumnImportance dialog reappears.
	var nInsertPos = sScript.indexOf("?") + 1
	sScript = sScript.substring(0,nInsertPos) + "ShowColumnImportance=True&" + sScript.substring(nInsertPos)
	parent.eval(sScript)
}

function goCalcColumnImportance() {
	var modelID = document.getElementById("ModelID").value;
	var process = "PredictGo.goCalcColumnImportance";
	var taskID = "CalcColumnImportance";
	var rdReport = "PredictGo.goColumnImportance";
	var linkParams = {
		ModelID: modelID
	};
	
	document.getElementById('divRunStatus').className = "ThemeHidden";
	var classes = document.getElementById('divCalculating').className.split(" ");
	var css = "";
	for (var i = 0; i < classes.length; i++) {
		if (classes[i] != "ThemeHidden")
			css = classes[i] + " ";
	}
	document.getElementById("divCalculating").className = css;
    
	// Trigger Column Importance
	goProcessAsync(process, taskID, linkParams);
	
	setTimeout(goPollColumnImportance, PG_POLL_TIMEOUT_MS);
}

function goPollColumnImportance() {
	var modelID = document.getElementById("ModelID").value;
	var process = "PredictGo.goCalcColumnImportance";
	var taskID = "PollColumnImportance";
	var linkParams = {
		ModelID: modelID
	};
	
	var refreshUrl = goGetRefreshUrl("rowsBody", "PredictGo.goColumnImportance", linkParams);
	
	linkParams.SendResponse = "True";
	
	var callback = function (res) {
		var newStatus = res.target ? res.target.responseText : res.srcElement.responseText;
		
		if (newStatus == "True") // still running
			setTimeout(goPollColumnImportance, PG_POLL_TIMEOUT_MS);
		else // done running - reload page
			rdAjaxRequestWithFormVars(refreshUrl,false,'',null,null,null,null,true);
	};
	
	// ping the server to see if this is still running
	return goProcessAsync(process, taskID, linkParams, callback, false);
}

function goAbortColumnImportance() {
	var modelID = document.getElementById("ModelID").value;
	var process = "PredictGo.goCalcColumnImportance";
	var taskID = "AbortColumnImportance";
	var linkParams = {
		ModelID: modelID
	};
	
	var refreshUrl = goGetRefreshUrl("rowsBody", "PredictGo.goColumnImportance", linkParams);
	
	var callback = function() {
		rdAjaxRequestWithFormVars(refreshUrl,false,'',null,null,null,null,true);
	}
	
	return goProcessAsync(process, taskID, linkParams, callback, false);
}
