
function goValidateShowError(sErrorMessageID, sErrorMessage) {
	var eleErrorMessage = document.getElementById(sErrorMessageID)
	eleErrorMessage.innerHTML = sErrorMessage
}

function goValidateSafeDbCharacters(sName, sErrorMessage) {
	//First character a-z or _ or -. No numbers.
	if (/[^a-zA-Z_\-]/.test(sName.substring(0,1))) {
		goValidateShowError("lblErrorMessage", sErrorMessage)
		return false
	}
	//All characters a-z or 0-9 or _ or - or space
	if (/[^a-zA-Z0-9 _\-]/.test(sName)) {
		goValidateShowError("lblErrorMessage", sErrorMessage)
		return false
	}
	return true
}

function goValidateCreateTable() {

	document.getElementById("NewTableName").value = document.getElementById("NewTableName").value.trim()
	
	var sTableName = document.getElementById("NewTableName").value
	var sPredictedValuesColumn = document.getElementById("NewPredictedValuesColumn").value.trim()
	var sPredictedTimeColumn = document.getElementById("NewPredictedTimeColumn").value.trim()

	//TableName
	if(sTableName == "") {
		goValidateShowError("lblErrorMessage", "Table Name cannot be blank.")
		return false
	}
	
	if (!goValidateSafeDbCharacters(sTableName, "Invalid characters in Table Name.")) {
		return false
	}
	
	//PredictedColumn
	if(sPredictedValuesColumn == "") {
		goValidateShowError("lblErrorMessage", "Predicted Values Column Name cannot be blank.")
		return false
	}
	
	if (!goValidateSafeDbCharacters(sPredictedValuesColumn, "Invalid characters in Predicted Values Column Name.")) {
		return false
	}
	
	//PredictedTimeColumn
	if(sPredictedTimeColumn == "") {
		goValidateShowError("lblErrorMessage", "Time Stamp Column Name cannot be blank.")
		return false
	}

	if (!goValidateSafeDbCharacters(sPredictedTimeColumn, "Invalid characters in Time Predicted Column Name.")) {
		return false
	}
	
	if (sPredictedValuesColumn != "" & sPredictedValuesColumn == sPredictedTimeColumn) {
		goValidateShowError("lblErrorMessage", "The column names should not be the same.")
		return false
	}

	return true;

}

