function goHighlightPredictedColumns() {
    
    var sTable = document.getElementById("TargetTable").innerText   
	sTable = sTable.split(" ").join("_")  //Replace blanks with _
	sTable = sTable.split(".").join("_")  //Replace . with _            These replacements match what is done by the AG.

    //Predicted column
    var sPredictedColumn = document.getElementById("PredictedColumn").innerText
	sPredictedColumn = sPredictedColumn.split(" ").join("_x0020_")
    var sHeaderId = "agCol_" + sTable + "_" + sPredictedColumn + "-TH"
    var eleHeader = document.getElementById(sHeaderId)
    if (!eleHeader) { //Try again with Schema.
		var sSchema = document.getElementById("TargetSchema").innerText
		if (sSchema != "") {
			sTable = sSchema + "_" + sTable
			sHeaderId = "agCol_" + sTable + "_" + sPredictedColumn + "-TH"
			eleHeader = document.getElementById(sHeaderId)
		}
    }
    if (eleHeader) {
        eleHeader.style.borderTopStyle = "dashed"  // Important was removed from the Theme to make this work.  See rdAgQbColor1 - 6
    }

    //Time Column
    sTimeColumn = document.getElementById("TimeColumn").innerText
	sTimeColumn = sTimeColumn.split(" ").join("_x0020_")
    sHeaderId = "agCol_" + sTable + "_" + sTimeColumn + "-TH"
    eleHeader = document.getElementById(sHeaderId)
    if (eleHeader) {
        eleHeader.style.borderTopStyle = "dotted" 
    }

    //Probability columns
    var nodesTableHeader = Y.all("table[id='dtAnalysisGrid'] TH")._nodes;  //Get all THs under the AG's DataTable.
    for (var i = 0; i < nodesTableHeader.length ; i++) {
		var isProbCol = false;
		
        eleHeader = nodesTableHeader[i];
        if (eleHeader.id.indexOf("_Probability") != -1) {
            //This is a probability column.
			isProbCol = true;
        } else {
			var spans = eleHeader.getElementsByTagName("SPAN");
			for (var j = 0; j < spans.length; j++) {
				var span = spans[j];
				if (span.innerText.length > 12) {
					var idx = span.innerText.lastIndexOf(" Probability");
					if (idx == span.innerText.length - 12) {
						isProbCol = true;
						break;
					}
				}
			}
		}
		
		if (isProbCol)
            eleHeader.style.borderTopStyle = "dotted";
    }
	
	setTimeout(function(){goHighlightPredictedColumns()},2000)
	//setTimeout ensures that columns are highlighted after paging and sorting.

}
