
function goShowModelSteps() {
	var eleModelDetails = document.getElementById("divModelDetails");
	if (!eleModelDetails) {
		return;
	}
	
	var eleTraining = document.getElementById("divTraining");
	if (!eleTraining) {
		return;
	}
	
	if (goIsAgTableSelected()) {  //Is there a table of data selected?
		eleModelDetails.style.display = ""
	}else{		
		eleModelDetails.style.display = "none"
	}
	
	if (document.getElementById("dtTrainingRuns")) {  //Are there any training runs?
		eleTraining.style.display = ""
	}else{
		eleTraining.style.display = "none"
	}	
	
}

function goShowPlanSteps() {
	var elePredict = document.getElementById("divPredict")
	if (goIsAgTableSelected()) {  //Is there a table of data selected?
		elePredict.style.display = ""
	}else{		
		elePredict.style.display = "none"
	}
}


function goIsAgTableSelected() {
	//First Table
	var eleTableInput = Y.one("#rdStartTable");
	if (!eleTableInput) {
		return false;
	}
	
	var eleSelectedTable = eleTableInput.one("option[selected='true']");
	if (eleSelectedTable) {
		return true
	}else{
		return false
	}

}

