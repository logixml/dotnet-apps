function RunProcess(sActionsXml, bValidate, sConfirm, sTarget, waitCfg) {

	if (bValidate == "true") {
		var sErrorMsg = rdValidateForm()
		if (sErrorMsg) {
			alert(sErrorMsg)
			return;
		}
	}
	
	if (sConfirm) {
		if (sConfirm.length != 0) {
			if (!confirm(sConfirm)) {
                return;
			}
		}
	}
		
	if (typeof rdSaveInputCookies != 'undefined'){rdSaveInputCookies()}
	if (typeof rdSaveInputsToLocalStorage != 'undefined'){rdSaveInputsToLocalStorage()}
	
	if (sTarget) {
		sOldTarget = document.rdForm.target
		if (sTarget != '') {
			document.rdForm.target=sTarget
		} else {
			document.rdForm.target='_self'
		}
	}


	//Add the actions to a new Hidden form field.  First, make sure that the hidden element doesn't already exist.  (Happens with Opera Back button.)
	var hiddenProcessAction=document.getElementById("rdProcessAction")
	if (hiddenProcessAction) { 
        hiddenProcessAction.parentNode.removeChild(hiddenProcessAction);//#4217
	}
	
    //Remove hidden forwarding elements that have other input elements.
	var eleInputs = document.getElementsByTagName("INPUT")
    var eleTextAreas = document.getElementsByTagName("TEXTAREA")
    for (var i=eleInputs.length-1; i > -1; i--) {
        var eleInput = eleInputs[i]
        if (eleInput.id=="rdHiddenRequestForwarding") {
            //Is there another input element with the same id?  (Can't use getElementById())
            for (var k=eleInputs.length-1; k > -1; k--) {
                if (k != i) {
                    if (eleInputs[k].name == eleInput.name && eleInput.parentNode) {
                        eleInput.parentNode.removeChild(eleInput)  //Remove the hidden forwarding element.
                        break
                    }
                }
            }
            if (eleInput.parentNode) {
                for (var k = eleTextAreas.length - 1; k > -1; k--) {
                    if (eleTextAreas[k].name == eleInput.name && eleInput.parentNode) {
                        eleInput.parentNode.removeChild(eleInput)  //Remove the hidden forwarding element.
                        break;
                    }
                }
            }
        }

        // checkbox unchecked value ...
        rdPrepCheckbox(eleInput, false);

        if ((eleInput.type == 'text')) {
            rdFixupInputs(eleInput);
        }
    }

    // Already removed this element if already existed prior, so no need to check for dupes
    hiddenProcessAction = document.createElement("INPUT");
    hiddenProcessAction.type = "HIDDEN";
    hiddenProcessAction.id = "rdProcessAction";
    hiddenProcessAction.name = "rdProcessAction";
    hiddenProcessAction.value = encodeURIComponent(sActionsXml);
	document.rdForm.appendChild(hiddenProcessAction);

    // Get or create rdRnd
    var hiddenRnd = document.getElementById("rdRnd");
    if (!hiddenRnd) {
        hiddenRnd = document.createElement("INPUT");
        hiddenRnd.type = "HIDDEN"
        hiddenRnd.id = "rdRnd"
        hiddenRnd.name = "rdRnd"
        document.rdForm.appendChild(hiddenRnd);
    }
    hiddenRnd.value = Math.floor(Math.random() * 100000);

    // Get or create rdScrollX
    var hiddenScrollX = document.getElementById("rdScrollX");
    if (!hiddenScrollX) {
        hiddenScrollX = document.createElement("INPUT");
        hiddenScrollX.type = "HIDDEN";
        hiddenScrollX.id = "rdScrollX";
        hiddenScrollX.name = "rdScrollX";
        document.rdForm.appendChild(hiddenScrollX);
    }
    hiddenScrollX.value = rdGetScroll('x');

    // Get or create rdScrollY
    var hiddenScrollY = document.getElementById("rdScrollY");
    if (!hiddenScrollY) {
        hiddenScrollY = document.createElement("INPUT");
        hiddenScrollY.type = "HIDDEN";
        hiddenScrollY.id = "rdScrollY";
        hiddenScrollY.name = "rdScrollY";
        document.rdForm.appendChild(hiddenScrollY);
    }
    hiddenScrollY.value = rdGetScroll('y');
			
	//document.rdForm.action="rdProcess.aspx?&rdRnd=" + Math.floor(Math.random() * 100000)
    var showPanelTimeout = 0;
    if (waitCfg && waitCfg.async) {
        document.rdForm.action = "rdProcessAsync.aspx?"

        var onSuccess, onFail;

        if (waitCfg.onSuccess) {
            onSuccess = function () {
                if (showPanelTimeout)
                    clearTimeout(showPanelTimeout);
                waitCfg.onSuccess.apply(this, arguments)
            };
        }
        else {
            onSuccess = function () {
                if (showPanelTimeout)
                    clearTimeout(showPanelTimeout);
            };
        }

        if (waitCfg.onFail) {
            onFail = function () {
                if (showPanelTimeout)
                    clearTimeout(showPanelTimeout);
                waitCfg.onFail.apply(this, arguments)
            };
        }
        else {
            onFail = function () {
                if (showPanelTimeout)
                    clearTimeout(showPanelTimeout);
            };
        }

        LogiXML.submitAsync(document.rdForm, onSuccess, onFail);

        rdUnPrepCheckboxes();
    }
    else {
        document.rdForm.action = "rdProcess.aspx?"
        document.rdForm.submit();
    }

	document.rdForm.action= ""; //#4434
	
	//Show wait panel
    if (waitCfg != null && waitCfg.waitMessage !== false && LogiXML.WaitPanel.pageWaitPanel) {
		LogiXML.WaitPanel.pageWaitPanel.set('cancel', false);
        showPanelTimeout = setTimeout(function () {
            LogiXML.WaitPanel.pageWaitPanel.showWaitPanel(waitCfg);
        }, 1000);
	}
	
	if (sTarget) {
		document.rdForm.target = sOldTarget
    }
}

function rdUnPrepCheckboxes() {
    var inputs = document.getElementsByTagName("INPUT");
    for (var i = 0; i < inputs.length; i++) {
        rdPrepCheckbox(inputs[i], true);
    }
}

function rdPrepCheckbox(eleInput, restore) {
    if (eleInput.type.toLowerCase() != "checkbox")
        return;

    // checkbox unchecked value ...
    var uncheckedValue = eleInput.getAttribute("rdUncheckedValue");
    if (uncheckedValue && uncheckedValue != "rdNotSent") {
        // when unchecked the checkbox value will be handled by a hidden input with the same name
        // remove the name from the checkbox to avoid duplicate request variables
        // conversely when checked restore the checkbox name and remove the hidden input

        var hiddenID = eleInput.id + "_rdHandleUnchecked";
        var hiddenCBV = document.getElementById(hiddenID);

        if (eleInput.checked || restore) {
            // restore checkbox name and remove hidden input
            if (hiddenCBV) {
                eleInput.name = hiddenCBV.name;
                hiddenCBV.parentNode.removeChild(hiddenCBV);
            }
        } else {
            // remove checkbox name and set hidden input
            if (!hiddenCBV) {
                hiddenCBV = document.createElement("INPUT");
                hiddenCBV.type = "HIDDEN";
                hiddenCBV.id = hiddenID;
                hiddenCBV.value = uncheckedValue;

                // move name from checkbox to hidden input
                hiddenCBV.name = eleInput.name;
                eleInput.name = "";

                document.rdForm.appendChild(hiddenCBV);
            }
        }
    }
}
