﻿<?xml version="1.0" encoding="utf-8"?>
<Report ID="PredictGo.goHome" PredictGoVersion="2.1.350">
  <MasterReport Report="PredictGo.goMaster" />
  <DefinitionModifierFile DefinitionModifierFile="goCustomizations.dmfHome.xml" ID="CustomDefinitionModifierFile" />
  <IncludeSharedElement DefinitionFile="PredictGo.goShared" SharedElementID="sharedStyles" />
  <StartupProcess Condition="&quot;@Session.goDbValidated~&quot; != &quot;True&quot;" ID="startupValidatePredictGoDatabase" Process="PredictGo.goAlterPredictGoDatabase" TaskID="tskAlterPredictGoDatabase" />
  <StartupProcess ID="startupValidateModelFiles" Process="PredictGo.goAlterPredictGoDatabase" TaskID="tskUpdateModelFiles" />
  <StartupProcess ID="startupValidateRLibrary" Process="PredictGo.goInitRLibrary" TaskID="tskPollLibrary" />
  <SetSessionVariables ID="clearShowLibraryStatus">
    <SessionParams goShowLibraryError="" goShowLibrarySuccess="" />
  </SetSessionVariables>
  <Body Class="go-home">
    <Division Class="ThemeContent" Condition="'@Session.goPredictGoConnectionType~' == ''" HtmlDiv="True" ID="divDbErrorConnectionMissing">
      <Label Caption="&#xD;&#xA;Note: Application configuration is required. Please create and configure a PredictGo Connection in the settings definition file.&#xD;&#xA;Correct the problem, then try again with a new browser session." Class="ThemeErrorText" Format="Preserve Line Feeds" />
    </Division>
    <Division Class="ThemeContent" Condition="'@Session.goPredictGoConnectionHasError~' == 'True' &amp;&amp; '@Session.goPredictGoConnectionType~' != ''" HtmlDiv="True" ID="divDbErrorConnectionBroken">
      <Label Caption="&#xD;&#xA;Note: There is a problem with the PredictGo database. &#xD;&#xA;Correct the problem, then try again with a new browser session.&#xD;&#xA;&#xD;&#xA;The error is: @Session.goPredictGoConnectionError~&#xD;&#xA;" Class="ThemeErrorText" Format="Preserve Line Feeds" />
    </Division>
    <Division Class="ThemeContent" Condition="'@Session.goRScriptLocation~' == ''" HtmlDiv="True" ID="divRErrorRScriptMissing">
      <Label Caption="&#xD;&#xA;Note: R installation not found on the Logi Predict web server. Please install R 3.4.1 to " Class="ThemeErrorText" Format="Preserve Line Feeds" />
      <Label Caption="@Function.AppPhysicalPath~\R\R-3.4.1" Class="ThemeErrorText" Format="Preserve Line Feeds">
        <ConditionalClass Class="ThemeHidden" Condition="&quot;@Function.OperatingSystem~&quot;==&quot;Unix&quot;" />
      </Label>
      <Label Caption="/usr/bin" Class="ThemeErrorText" Format="Preserve Line Feeds">
        <ConditionalClass Class="ThemeHidden" Condition="&quot;@Function.OperatingSystem~&quot;!=&quot;Unix&quot;" />
      </Label>
      <Label Caption=" or set the constant rdRScriptLocation in the settings definition file to the path and filename of the Rscript executible within your existing installation.&#xD;&#xA;R can be downloaded from " Class="ThemeErrorText" Format="Preserve Line Feeds" />
      <Label Caption="https://cran.r-project.org/">
        <Action ID="actLinkToCran" Type="Link">
          <Target FrameID="NewWindow" ID="trgLinkToCran" Link="https://cran.r-project.org/" Type="Link" />
        </Action>
      </Label>
      <Label Caption="&#xD;&#xA;Correct the problem, then try again with a new browser session." Class="ThemeErrorText" Format="Preserve Line Feeds" />
    </Division>
    <Division Class="ThemeContent" Condition="'@Session.goLibraryLoading~'=='True'" HtmlDiv="True" ID="divLoadingLibrary">
      <Label Caption="&#xD;&#xA;Note: The server's PredictOpen library files are currently being upgraded. Certain features may be unavailable during this time." Class="ThemeErrorText" Format="Preserve Line Feeds" />
    </Division>
    <Division Class="ThemeContent" Condition="('@Session.goRScriptLocation~'!='')&amp;&amp;('@Session.goLibraryLoading~'!='True')&amp;&amp;('@Session.goRLibraryReady~'=='')&amp;&amp;('@Request.ShowLibraryError~'!='True')" HtmlDiv="True" ID="divRLibraryMissing">
      <Label Caption="&#xD;&#xA;Note: The server’s PredictOpen library files need to be upgraded. To continue, click " Class="ThemeErrorText" Format="Preserve Line Feeds" />
      <Label Caption="here">
        <Action ID="actInitRLibrary" Process="PredictGo.goInitRLibrary" TaskID="tskInitRLibrary" Type="Process">
          <WaitPage Caption="Initializing R Library. Please wait..." />
        </Action>
      </Label>
      <Label Caption=" to start the process." Class="ThemeErrorText" Format="Preserve Line Feeds" />
      <Division />
    </Division>
    <Division Class="ThemeContent" Condition="('@Request.ShowLibraryError~'=='True')&amp;&amp;('@Session.goLibraryLoading~'!='True')" HtmlDiv="True" ID="divRLibraryError">
      <Label Caption="&#xD;&#xA;Note: The server’s PredictOpen library files need to be upgraded. The previous attempt failed. Click " Class="ThemeErrorText" Format="Preserve Line Feeds" />
      <Label Caption="here">
        <Action ID="actInitRLibrary" Process="PredictGo.goInitRLibrary" TaskID="tskInitRLibrary" Type="Process">
          <WaitPage Caption="Initializing R Library. Please wait..." />
        </Action>
      </Label>
      <Label Caption=" to try again." Class="ThemeErrorText" Format="Preserve Line Feeds" />
    </Division>
    <Division Class="ThemeContent" Condition="('@Request.ShowLibrarySuccess~'=='True')&amp;&amp;('@Session.goLibraryLoading~'!='True')" HtmlDiv="True" ID="divRLibrarySuccess">
      <Label Caption="&#xD;&#xA;Note: The PredictGo R library has been successfully initialized on the Logi Predict web server!" Class="ThemeTextPositive" Format="Preserve Line Feeds" />
    </Division>
    <Division Class="ThemeContent ThemeContentCentered" HtmlDiv="True" ID="divGoContainer">
      <Division Class="go-content" HtmlDiv="True" ID="divMainContent">
        <Division Class="go-welcome " ID="divWelcome">
          <ResponsiveRow CollisionBehavior="Wrap">
            <ResponsiveColumn Class=" go-welcome-container go-welcome-responsive">
              <Label Caption="Welcome to Logi Predict" Class="go-welcome-title" />
              <LineBreak LineCount="1" />
              <Label Caption="Easily harness your data to uncover patterns and make valuable predictions. " Class="ThemeHeaderLarge" Format="Preserve Line Feeds" />
              <LineBreak LineCount="3" />
              <Rows>
                <Row>
                  <Column ColSpan="2">
                    <Label Caption="Getting started is easy." Class="go-welcome-header" Format="Preserve Line Feeds" />
                    <LineBreak LineCount="2" />
                  </Column>
                </Row>
                <Row Condition="&quot;@Application.rdDisableMetadataBuilder~&quot; &lt;&gt; &quot;True&quot;" SecurityRightID="goDataManager">
                  <Column>
                    <Remark>
                      <Image Caption="PredictGo.iconManageData.png" Height="40px" Width="40px">
                        <Action Type="Report">
                          <Target Report="PredictGo.goMetadata" Type="Report" />
                        </Action>
                      </Image>
                    </Remark>
                  </Column>
                  <Column>
                    <Label Caption="Begin by using the Data Manager to set up your data set." Class="ThemeHeaderLarge" Format="Preserve Line Feeds" />
                  </Column>
                </Row>
                <Row>
                  <Column />
                  <Column>
                    <Label Caption="• Click the Data Manager link on the bottom of left-side menu bar.&#xD;&#xA;&#xD;&#xA;" Class="go-welcome-text" Format="Preserve Line Feeds" />
                  </Column>
                </Row>
                <Row>
                  <Column>
                    <Remark>
                      <Image Caption="PredictGo.iconModels.png" Height="40px" Width="40px">
                        <Action Type="Report">
                          <Target Report="PredictGo.goModels" Type="Report" />
                        </Action>
                      </Image>
                    </Remark>
                  </Column>
                  <Column>
                    <Label Caption="Go to Prediction Models to create and train a model." Class="ThemeHeaderLarge" Format="Preserve Line Feeds" />
                  </Column>
                </Row>
                <Row>
                  <Column />
                  <Column>
                    <Label Caption="• Choose your historical data set.&#xD;&#xA;• Pick the type of predictive analysis you want to do.&#xD;&#xA;• Pick an algorithm and the column with the historical outcome.&#xD;&#xA;• Run training as often as needed.&#xD;&#xA;• Promote the preferred training run to production.&#xD;&#xA;&#xD;&#xA;" Class="go-welcome-text" Format="Preserve Line Feeds" />
                  </Column>
                </Row>
                <Row>
                  <Column>
                    <Remark>
                      <Image Caption="PredictGo.iconPlan.png" Height="40px" Width="40px">
                        <Action Type="Report">
                          <Target Report="PredictGo.goPlans" Type="Report" />
                        </Action>
                      </Image>
                    </Remark>
                  </Column>
                  <Column>
                    <Label Caption="Create a new Prediction Plan.&#xD;&#xA;" Class="ThemeHeaderLarge" Format="Preserve Line Feeds" />
                  </Column>
                </Row>
                <Row>
                  <Column />
                  <Column>
                    <Label Caption="• Choose your production data set.&#xD;&#xA;• Choose how you want to output the data.&#xD;&#xA;• Run the prediction.&#xD;&#xA;" Class="go-welcome-text" Format="Preserve Line Feeds" />
                  </Column>
                </Row>
              </Rows>
              <LineBreak LineCount="2" />
            </ResponsiveColumn>
            <ResponsiveColumn>
              <Image Caption="PredictGo.CrystalBall.png" Width="600" />
            </ResponsiveColumn>
          </ResponsiveRow>
        </Division>
      </Division>
    </Division>
    <Remark>
      <IncludeScript ID="scriptShowIntro" IncludedScript="if (0@Local.cntUserBookmarks~ == 0) {                document.getElementById('divGoIntroContainer').style.display = ''}" />
    </Remark>
  </Body>
  <IncludeSharedElement DefinitionFile="PredictGo.goShared" SharedElementID="sharedScripts" />
  <ideTestParams ShowLibraryError="" ShowLibrarySuccess="" />
</Report>