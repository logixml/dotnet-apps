<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" indent="yes" />

  <xsl:template match="LogEntry">
    <html>
      <head>
        <title>Meta Data Synchronization Package Log</title>
      </head>
      <body>

        <h1>
          <xsl:value-of select="@Product"/> Changes
        </h1>
        <h2>
          Date: <xsl:value-of select="@Date"/>
        </h2>
        <b> Destination Application Path: </b>
        <xsl:value-of select="@DestinationApplicationPath"/>
        <BR />
        <b> Destination Database Connection: </b>
        <xsl:value-of select="@DestinationDatabaseConnection"/>
        <BR />
        <b> Metadata Database Backup Folder: </b><xsl:value-of select="@BackupPath"/>
        <BR />
        <b> Metadata Database Synchronization Package: </b><xsl:value-of select="@PackageName"/>
        <BR />
        <b> Application URL: </b><xsl:value-of select="@ApplicationURL"/>


        <xsl:if test="not(Schema|RelationSchema|ParameterSchema|CascadeFilterSchema|ReportSchema[@Type='Change'])">
          <xsl:if test="not(ErrorLog)">
            <p>
              No changes were made to <xsl:value-of select="@Product"/>.
            </p>
          </xsl:if>
        </xsl:if>

        <xsl:apply-templates select="ErrorLog" />
        <xsl:apply-templates select="Schema[@Type='Change']" />
        <xsl:apply-templates select="RelationSchema[@Type='Change']" />
        <xsl:apply-templates select="ParameterSchema[@Type='Change']" />
        <xsl:apply-templates select="CascadeFilterSchema[@Type='Change']" />
        <xsl:apply-templates select="ReportSchema[@Type='Change']" />

      </body>
    </html>
  </xsl:template>

  <xsl:template match="ErrorLog">

    <h3>Errors</h3>
    <ul>
      <xsl:for-each select="Error">
        <li>
          <xsl:value-of select="@ErrorMessage" />
        </li>
      </xsl:for-each>
    </ul>

  </xsl:template>
  
  <xsl:template match="Schema[@Type='Change']">
    <h3>Schema Changes</h3>
    <p>
      The following changes were made to <xsl:value-of select="../@Product"/>:
    </p>

    <xsl:if test="Object[@Action='Add']">
      <h4>Added objects</h4>
      <ul>
        <xsl:for-each select="Object[@Action='Add']">
          <li>
            <em>
              <xsl:value-of select="@ObjectName" />
            </em> (Type: <xsl:value-of select="@ObjectType" />) - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>

    <xsl:if test="Object/Column[@Action='Add']">
      <h4>Added columns</h4>
      <ul>
        <xsl:for-each select="Object/Column[@Action='Add']">
          <li>
            <xsl:value-of select="../@ObjectName" />.<em>
              <xsl:value-of select="@ColumnName" />
            </em> - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>

    <xsl:if test="Object[@Action='Remove']">
      <h4>Removed objects</h4>
      <ul>
        <xsl:for-each select="Object[@Action='Remove']">
          <li>
            <em>
              <xsl:value-of select="@ObjectName" />
            </em> (Type: <xsl:value-of select="@ObjectType" />) - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>

    <xsl:if test="Object/Column[@Action='Remove']">
      <h4>Removed columns</h4>
      <ul>
        <xsl:for-each select="Object/Column[@Action='Remove']">
          <li>
            <xsl:value-of select="../@ObjectName" />.<em>
              <xsl:value-of select="@ColumnName" />
            </em> - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>

    <xsl:if test="Object/Column[@Action='SetDataType']">
      <h4>Columns with changed Data Types</h4>
      <ul>
        <xsl:for-each select="Object/Column[@Action='SetDataType']">
          <li>
            <xsl:value-of select="../@ObjectName" />.<em>
              <xsl:value-of select="@ColumnName" />
            </em> - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>

    <xsl:if test="Object[@Action='Update']">
      <h4>Repaired objects</h4>
      <ul>
        <xsl:for-each select="Object[@Action='Update']">
          <li>
            <em>
              <xsl:value-of select="@ObjectName" />
            </em> (Type: <xsl:value-of select="@ObjectType" />) - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>
    
    <xsl:if test="Object/Column[@Action='Update']">
      <h4>Repaired columns</h4>
      <ul>
        <xsl:for-each select="Object/Column[@Action='Update']">
          <li>
            <xsl:value-of select="../@ObjectName" />.<em>
              <xsl:value-of select="@ColumnName" />
            </em> - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>
    
  </xsl:template>

  <xsl:template match="RelationSchema[@Type='Change']">
    <h3>Relationship Changes</h3>

    <xsl:if test="Relation[@Action='Add']">
      <h4>Added Relationships</h4>
      <ul>
        <xsl:for-each select="Relation[@Action='Add']">
          <li>
            <em>
              <xsl:value-of select="@RelationName" />
            </em> (<xsl:value-of select="@Relation" /> between: <xsl:value-of select="@ObjectName1" />.<xsl:value-of select="@ColumnName1" /> and <xsl:value-of select="@ObjectName2" />.<xsl:value-of select="@ColumnName2" />) - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>

    <xsl:if test="Relation[@Action='Remove']">
      <h4>Removed Relationships</h4>
      <ul>
        <xsl:for-each select="Relation[@Action='Remove']">
          <li>
            <em>
              <xsl:value-of select="@RelationName" />
            </em> (<xsl:value-of select="@Relation" /> between: <xsl:value-of select="@ObjectName1" />.<xsl:value-of select="@ColumnName1" /> and <xsl:value-of select="@ObjectName2" />.<xsl:value-of select="@ColumnName2" />) - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>

    <xsl:if test="Relation[@Action='Update']">
      <h4>Repaired Relationships</h4>
      <ul>
        <xsl:for-each select="Relation[@Action='Update']">
          <li>
            <em>
              <xsl:value-of select="@RelationName" />
            </em>(<xsl:value-of select="@Relation" /> between: <xsl:value-of select="@ObjectName1" />.<xsl:value-of select="@ColumnName1" /> and <xsl:value-of select="@ObjectName2" />.<xsl:value-of select="@ColumnName2" />) - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>

  </xsl:template>

  <xsl:template match="ParameterSchema[@Type='Change']">
    <h3>Object Parameter Changes</h3>

    <xsl:if test="ObjectParameter[@Action='Add']">
      <h4>Added Parameters</h4>
      <ul>
        <xsl:for-each select="ObjectParameter[@Action='Add']">
          <li>
            <em>
              <xsl:value-of select="@ObjectName" />.<xsl:value-of select="@ColumnName" />
            </em> - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>

    <xsl:if test="ObjectParameter[@Action='Remove']">
      <h4>Removed Parameters</h4>
      <ul>
        <xsl:for-each select="ObjectParameter[@Action='Remove']">
          <li>
            <em>
              <xsl:value-of select="@ObjectName" />.<xsl:value-of select="@ColumnName" />
            </em> - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>

    <xsl:if test="ObjectParameter[@Action='Update']">
      <h4>Repaired Parameters</h4>
      <ul>
        <xsl:for-each select="ObjectParameter[@Action='Update']">
          <li>
            <em>
              <xsl:value-of select="@ObjectName" />.<xsl:value-of select="@ColumnName" />
            </em> - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>

  </xsl:template>

  <xsl:template match="CascadeFilterSchema[@Type='Change']">
    <h3>Cascading Filter Changes</h3>

    <xsl:if test="CascadeFilter[@Action='Add']">
      <h4>Added Cascading Filters</h4>
      <ul>
        <xsl:for-each select="CascadeFilter[@Action='Add']">
          <li>
            <xsl:value-of select="@FilterName" /> (<em> <xsl:value-of select="@ObjectName" />.<xsl:value-of select="@ColumnName" />
            </em>) - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>

    <xsl:if test="CascadeFilter[@Action='Remove']">
      <h4>Removed Cascading Filters</h4>
      <ul>
        <xsl:for-each select="CascadeFilter[@Action='Remove']">
          <li>
              <xsl:value-of select="@FilterName" /> (<em> <xsl:value-of select="@ObjectName" />.<xsl:value-of select="@ColumnName" />
            </em>) - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>

    <xsl:if test="CascadeFilter[@Action='Update']">
      <h4>Repaired Cascading Filters</h4>
      <ul>
        <xsl:for-each select="CascadeFilter[@Action='Update']">
          <li>
            <xsl:value-of select="@FilterName" /> (<em> <xsl:value-of select="@ObjectName" />.<xsl:value-of select="@ColumnName" />
            </em>) - <xsl:value-of select="@Status" />
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>

  </xsl:template>

  <xsl:template match="ReportSchema[@Type='Change']">
    <h3>Report Changes</h3>

    <xsl:if test="Report[@Action='Add']">
      <h4>Added Reports and Schedules</h4>
      <ul>
        <xsl:for-each select="Report[@Action='Add']">
          <li>
            <em>
              <xsl:value-of select="@ReportName" />
            </em> - <xsl:value-of select="@Status" />
            <xsl:if test="../Report[@ReportName = current()/@ReportName]/Schedule[@Action='Add']">
              <ul>
                <xsl:for-each select="../Report[@ReportName = current()/@ReportName]/Schedule[@Action='Add']">
                  <li>
                    Scheduled Task GUID: 
                    <em>
                      <xsl:value-of select="@ScheduleName" />
                    </em> - <xsl:value-of select="@Status" />
                  </li>
                </xsl:for-each>
              </ul>
            </xsl:if>
          </li>
        </xsl:for-each>
      </ul>
    </xsl:if>

  </xsl:template>
  
</xsl:stylesheet>

