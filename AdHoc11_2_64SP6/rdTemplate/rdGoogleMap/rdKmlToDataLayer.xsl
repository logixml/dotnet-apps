
<xsl:stylesheet version="2.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:str="http://exslt.org/strings" exclude-result-prefixes="str"
>
  <xsl:template match="/">
    <rdData>
     <xsl:for-each select="//*[local-name()='Placemark']" >
	    <rdPoints >

        <xsl:for-each select="*[local-name()='name'] | *[local-name()='description'] | *[local-name()='address'] | *[local-name()='phoneNumber']" >
          <xsl:attribute name="{name()}">
            <xsl:value-of select="."/>
          </xsl:attribute>
        </xsl:for-each >

        <xsl:choose>

          <xsl:when test="*[local-name()='Point']" >
            <xsl:attribute name="rdCoordinates">
              <xsl:value-of select="*[local-name()='Point']/*[local-name()='coordinates']"/>
            </xsl:attribute>
          </xsl:when>

          <xsl:otherwise>
            <xsl:attribute name="rdCoordinates">
              <xsl:for-each select="*[local-name()='Polygon']/*[local-name()='outerBoundaryIs']//*[local-name()='coordinates']">
                <xsl:value-of select="."/>
              </xsl:for-each>
			  <xsl:for-each select="*[local-name()='LineString']//*[local-name()='coordinates']">
                <xsl:value-of select="."/>
              </xsl:for-each>
            </xsl:attribute>
          </xsl:otherwise >
          
        </xsl:choose>
          
          
	    </rdPoints>
     </xsl:for-each >
   </rdData>
  </xsl:template>

</xsl:stylesheet>