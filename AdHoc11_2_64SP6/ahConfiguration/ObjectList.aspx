<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ObjectList.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_ObjectList" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="DatabaseControl" Src="~/ahControls/DatabaseControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Data Objects</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="DataObjects" ParentLevels="0" />
        
        <asp:ScriptManager ID="ScriptManager1" runat="server" />

        <%--<script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            if (args.get_error() != undefined && args.get_error().httpStatusCode == '500') {
                var errorMessage = args.get_error().message;
                args.set_errorHandled(true);
            }
        }
        </script>--%>
        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
        }
        </script>

<table class="limiting"><tr><td>
        <table class="gridForm"><tr><td>
         
            <asp:UpdatePanel ID="UP1" runat="Server" UpdateMode="Conditional">
                <ContentTemplate>
                <Adhoc:DatabaseControl ID="ucDatabaseControl" runat="server" ShowAllDatabasesOption="False" />
                <br /><br />
                <div id="data_main">
                    <div id="activities">
                        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr width="100%">
                        <td align="right" valign="top">
                            <AdHoc:Search ID="srch" runat="server" Title="Find Data Objects" meta:resourcekey="AdHocSearch" />
                        </td>
                        </tr>
                        </table>
                    </div>
                    <asp:GridView ID="grdMain" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" CssClass="grid" DataKeyNames="ObjectID"
                        OnRowDataBound="OnItemDataBoundHandler" OnSorting="OnSortCommandHandler" meta:resourcekey="grdMainResource1" >
                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                        <PagerTemplate>
                            <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex"/>
                        </PagerTemplate>
                        <PagerStyle HorizontalAlign="Center"></PagerStyle>
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow"></AlternatingRowStyle>
                            <Columns>
                                <asp:TemplateField HeaderText="Schema" SortExpression="ObjectSchema" meta:resourcekey="TemplateFieldResource4">
                                    <ItemTemplate>
                                        <asp:Label ID="ObjectSchema" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Data Object" SortExpression="ObjectName" meta:resourcekey="TemplateFieldResource1">
                                    <ItemTemplate>
                                        <input type="hidden" id="ObjectID" runat="server" />
                                        <asp:Label ID="ObjectName" runat="server" meta:resourcekey="ObjectNameResource1" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Friendly Name" SortExpression="Description" meta:resourcekey="TemplateFieldResource6">
                                    <ItemTemplate>
                                        <asp:Label ID="Description" runat="server" meta:resourcekey="DescriptionResource1" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type" SortExpression="Type" meta:resourcekey="TemplateFieldResource2">
                                    <ItemTemplate>
                                        <asp:Label ID="Type" runat="server" meta:resourcekey="TypeResource1" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Categories" meta:resourcekey="TemplateFieldResource5">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCategories" runat="server"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Hidden" meta:resourcekey="TemplateFieldResource3">
                                    <ItemTemplate>
                                        <asp:Label ID="Hidden" runat="server" meta:resourcekey="HiddenResource1" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" >
                                    <HeaderStyle Width="50px"></HeaderStyle>
                                    <ItemTemplate>
                                        <asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" 
                                            ToolTip="<%$ Resources:LogiAdHoc, Actions %>" ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                        <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                            HorizontalAlign="Left" Wrap="false" style="display:none;">
                                            <div id="divModify" runat="server" class="hoverMenuActionLink" >
                                                <asp:LinkButton ID="lnkModify" runat="server" OnCommand="ModifyObject" 
                                                    Text="Modify Data Object" meta:resourcekey="ModifyResource1"></asp:LinkButton>
                                            </div>
                                            <div id="divRelations" runat="server" class="hoverMenuActionLink" >
                                                <asp:LinkButton ID="lnkRelations" runat="server" OnCommand="RelationObject" 
                                                    Text="Set Relations" meta:resourcekey="RelationsResource1"></asp:LinkButton>
                                            </div>
                                            <div id="divPermissions" runat="server" class="hoverMenuActionLink" >
                                                <asp:LinkButton ID="lnkPermissions" runat="server" OnCommand="PermissionObject" 
                                                    Text="Set Data Object Access Rights" meta:resourcekey="PermissionsResource1"></asp:LinkButton>
                                            </div>
                                            <div id="divParameters" runat="server" class="hoverMenuActionLink" >
                                                <asp:LinkButton ID="lnkParameters" runat="server" OnCommand="ParameterObject" 
                                                    Text="Set Parameters" meta:resourcekey="ParametersResource1"></asp:LinkButton>
                                            </div>
                                            <div id="divLinks" runat="server" class="hoverMenuActionLink" >
                                                <asp:LinkButton ID="lnkLinks" runat="server" OnCommand="ManageLink" 
                                                    Text="Set Links" meta:resourcekey="LinksResource1"></asp:LinkButton>
                                            </div>
                                            <div id="divDependencies" runat="server" class="hoverMenuActionLink" >
                                                <asp:LinkButton ID="lnkDependencies" runat="server" OnCommand="Dependency" 
                                                    Text="<%$ Resources:LogiAdHoc, ViewDependencyInfo %>" ></asp:LinkButton>
                                            </div>
 				                        </asp:Panel>
                                        <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                            PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                            TargetControlID="imgActions" PopDelay="25" />
                                       
                                    </ItemTemplate><ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                </div>
                </ContentTemplate>
                <Triggers>
                <asp:AsyncPostBackTrigger ControlID="srch" EventName="DoSearch" />
            </Triggers>
            </asp:UpdatePanel>
        </td></tr></table>
</td></tr></table>
        <div id="divSaveAnimation">
            <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                DropShadow="False">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                <table><tr><td>
                <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                </td>
                <td valign="middle">
                <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                </td></tr></table>
            </asp:Panel>
        </div>
<br /><br /><br /><br /><br /><br />
    </form>
</body>
</html>
