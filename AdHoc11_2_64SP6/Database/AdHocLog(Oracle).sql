CREATE TABLE "ADHOCLOG" 
("ACTIONNAME" VARCHAR2(25) NOT NULL, 
    	"ACTIONTIMESTAMP" DATE NOT NULL, 
"ACTIONDURATION" NUMBER(10),
    	"HOSTADDRESS" VARCHAR2(40), 
"SESSIONGUID" VARCHAR2(36), 
    	"USERNAME" VARCHAR2(50), 
"GROUPNAME" VARCHAR2(100), 
	"AFFECTEDUSERNAME" VARCHAR2(50),
"AFFECTED" VARCHAR2(500),
	"APPCODE" VARCHAR2(500),
"CONNECTIONLABEL" VARCHAR2(255), 
	"REPORTID" VARCHAR2(255), 
"REPORTNAME" VARCHAR2(200), 
	"REPORTTYPE" VARCHAR2(1), 
"QUERY" CLOB, 
	"SESSIONDURATION" NUMBER(10));
