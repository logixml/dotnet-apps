<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="HomePage.aspx.vb" Inherits="LogiAdHoc.ahHome_HomePage" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Home Page</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
        function calcHeight()
        {
            try
            {
              //find the height of the internal page
              var the_height = document.getElementById('frm').contentWindow.document.body.scrollHeight;

              //change the height of the iframe
              document.getElementById('frm').height = the_height;
              //document.getElementById('frm').height = window.screen.availHeight;
            }
            catch(err)
            {
                //document.getElementById('frm').height = 5000;
                document.getElementById('frm').height = window.screen.availHeight
            }
        }
    </script> 
        
</head>
<body>
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <span>
            <iframe id="frm" runat="server" frameborder="0" width="100%"></iframe>
        </span>
    </form>
</body>
</html>
