<%@ Page AutoEventWireup="false" Codebehind="Folder.aspx.vb" Culture="auto" Inherits="LogiAdHoc.ahReport_Folder"
    Language="vb" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Folder</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />

    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>

</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server" />
    	<div id="submenu"><AdHoc:NavMenu id="subnav" runat="server" /></div>	
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="Folder" />

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
        }
        </script>

        <div class="divForm">
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" value="0" />
            <input id="FolderID" type="hidden" runat="server" name="RoleID">
            <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="conditional">
                <ContentTemplate>
                    <div id="folderinfo">
                        <table class="tbTB">
                            <tr>
                                <td width="125">
                                    <asp:Label ID="lblFolder" runat="server" CssClass="requiredfield" Text="<%$ Resources:LogiAdHoc, Folder %>" AssociatedControlID="foldername"/>:
                                </td>
                                <td>
                                    <input id="foldername" type="text" maxlength="100" runat="server" name="foldername">
                                    <asp:RequiredFieldValidator ID="rtvFolderName" runat="server" ErrorMessage="Folder is required."
                                        ControlToValidate="foldername" meta:resourcekey="rtvFolderNameResource1">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="cvFolderName" runat="server" ErrorMessage="<%$ Resources:Errors, Err_FolderName %>"
                                        ControlToValidate="foldername" OnServerValidate="IsFolderNameValid">*</asp:CustomValidator>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblDescription" runat="server" Text="<%$ Resources:LogiAdHoc, Description %>" AssociatedControlID="Description" />:
                                </td>
                                <td>
                                    <asp:TextBox ID="Description" TextMode="MultiLine" Columns="45" Rows="3" runat="server" />
                                    <asp:CustomValidator ID="cvMaxLength" runat="server" ErrorMessage="<%$ Resources:Errors, Err_Description255 %>"
                                        ControlToValidate="Description" OnServerValidate="ReachedMaxLength">*</asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="folderroles1" runat="server">
                        <h2>
                            <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource1" Text="Folder Roles"></asp:Localize>
                        </h2>
                        <table style="width: 310px;">
                            <tr id="AllUsersRow" runat="server">
                                <td>
                                    <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2" Text="Access to Folder:"></asp:Localize>
                                </td>
                                <td>
                                    <asp:RadioButton ID="AllRolesAccess" Text="All Roles" GroupName="RoleAccess" runat="server"
                                        OnCheckedChanged="AllRolesAccess_Changed" AutoPostBack="True"
                                        meta:resourcekey="AllRolesAccessResource1" />
                                    <asp:RadioButton ID="SpecificRolesAccess" Text="Specific Roles" GroupName="RoleAccess"
                                        runat="server" OnCheckedChanged="AllRolesAccess_Changed" AutoPostBack="True"
                                        meta:resourcekey="SpecificRolesAccessResource1" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <asp:UpdatePanel ID="UP3" UpdateMode="Conditional" runat="server">
                        <ContentTemplate>
                            <div id="folderroles2" runat="server">
                                <table id="ColumnMSL">
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label3" runat="server" AssociatedControlID="available" meta:resourcekey="LiteralResource3" Text="Available Roles"></asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:Label ID="Label4" runat="server" AssociatedControlID="assigned" meta:resourcekey="LiteralResource4" Text="Roles With Access"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <select id="available" class="MSL" multiple size="15" runat="server" ondblclick="document.getElementById('ahDirty').value=1; __doPostBack('moveright','');" />
                                            <%--<AdHoc:AHListControl ID="available" runat="server" ControlWidth="200" ControlHeight="100"  MultipleSelection="false" />--%>
                                        </td>
                                        <td>
                                            <asp:ImageButton CssClass="mslbutton" ID="moveright" OnClick="MoveRight_Click" ImageUrl="../ahImages/arrowRight.gif"
                                                CausesValidation="False" runat="server" AlternateText="Move Right" meta:resourcekey="moverightResource1" />
                                            <asp:ImageButton CssClass="mslbutton" ID="moveleft" OnClick="MoveLeft_Click" ImageUrl="../ahImages/arrowLeft.gif"
                                                CausesValidation="False" runat="server" AlternateText="Move Left" meta:resourcekey="moveleftResource1" />
                                        </td>
                                        <td>
                                            <select id="assigned" class="MSL" multiple size="15" runat="server" ondblclick="document.getElementById('ahDirty').value=1; __doPostBack('moveleft','');" />
                                            <%--<AdHoc:AHListControl ID="assigned" runat="server" ControlWidth="200" ControlHeight="100" MultipleSelection="true" />--%>
                                            <input id="txtProxy" class="hidden" type="text" size="3" value="0" runat="server" />
                                            <asp:CustomValidator ID="cvRoleValidator" runat="server" ControlToValidate="txtProxy"
                                                ErrorMessage="You have to add at least one of your own roles to this folder."
                                                EnableClientScript="False" OnServerValidate="IsRoleValid" meta:resourcekey="cvRoleValidatorResource1">*</asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="AllRolesAccess" EventName="CheckedChanged" />
                            <asp:AsyncPostBackTrigger ControlID="SpecificRolesAccess" EventName="CheckedChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div id="divButtons" class="divButtons">
                        <AdHoc:LogiButton ID="btnSave" runat="server" OnClick="SaveFolder" ToolTip="<%$ Resources:LogiAdHoc, SaveReportListTooltip %>"
                            Text="<%$ Resources:LogiAdHoc, Save %>" UseSubmitBehavior="false" />
                        <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="CancelFolder"
                            ToolTip="<%$ Resources:LogiAdHoc, BackToReportsListTooltip %>" Text="<%$ Resources:LogiAdHoc, BackToReportsList %>"
                            CausesValidation="False" />
                        <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1" />
                    </div>
<br />
                    <div id="divSaveAnimation">
                        <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                            TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                            DropShadow="False">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                            <table><tr><td>
                            <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>"/>
                            </td>
                            <td valign="middle">
                            <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                            </td></tr></table>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnCancel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
