<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291599" class="title">Sorting Data</a></h4>


<p class=MsoNormal>The data returned from the database may have a default sort
order specified. This will determine the initial display order of the data for
tabular reports and the initial population of crosstab reports. The sort order
may be overridden when the report is executed if the column sort option is
enabled.</p>


<p class=MsoNormal>To set the initial sort order of the data, click on the <b>Select
Data</b> button to display the <i>Select Data </i>dialog. Click on the <i>Sort</i>
tab to display:</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image049.jpg"></p>


<p class=MsoNormal>The sort order must be set column by column. </p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>Click on the <b>Add a Column</b> button to create a sorting
level.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image050.jpg"></p>


<p class=MsoNormal>Select a column from the <i>Column</i> dropdown list and the
<i>Direction</i> (Ascending or Descending). Repeat the process for all of the
columns that must be used to set the sort sequence of the data.</p>


<p class=MsoNormal>To remove a column from the sort sequence, click on the <img
border=0   src="Report_Design_Guide_files/image051.jpg"> for
the column.</p>


<p class=MsoNormal>If the <i>Column</i> order in the list needs to be adjusted,
use the drag-and-drop method by clicking on the <img border=0 
 src="Report_Design_Guide_files/image052.gif"> drag handle and
dropping the column where necessary.</p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>




</body>
</html>
