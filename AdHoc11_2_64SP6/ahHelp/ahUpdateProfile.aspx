<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291593" class="title">Updating Your Profile</a></h4>


<p class=MsoNormal>Click <b>Profile </b>to change your profile information. Profile
information is divided into User Profile and Preferences and accessed by
clicking on the respective tabs. </p>


<p class=MsoNormal><i>User Profile</i></p>


<p class=MsoNormal>The <i>User Profile</i> covers the user specific
information. From this screen, a user's <i>Username, Password, First Name, Last
Name</i> and <i>Email Address</i> may be changed.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image020.jpg"></p>


<p class=MsoNormal><b>To update your profile:</b></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>Type the new information into the <i>User Profile</i>
     fields provided.</li>
 <li class=MsoNormal>If the Security Authentication method is not NT, then</li>
 <ol style='margin-top:0in' start=1 type=a>
  <li class=MsoNormal>Click the <b>Set Password </b>button.</li>
  <li class=MsoNormal>Type your old password and new password twice in the
      fields provided.</li>
  <li class=MsoNormal>From the <i>Password</i> panel, click <b>OK </b>to apply
      the changes.</li>
 </ol>
 <li class=MsoNormal>Modify other fields as desired.</li>
 <li class=MsoNormal>Click <b>Save </b>to commit the changes.</li>
</ol>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><a name="OLE_LINK3"></a><a name="OLE_LINK2"></a><a
  name="OLE_LINK1"><span style='font-style:normal'>Notes:</span></a></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span><i>Username</i>
  cannot be changed.</p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>The ability to
  modify a user password from the <i>User Profile</i> screen may be disabled by
  the administrator.</p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>The content of
  the <i>Email Address</i> field cannot be deleted if the user has subscribed
  to a report.</p>
  </td>
 </tr>
</table>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#FFFF99;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial>Hint:</p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i>Enter an email address in order to be able to subscribe
  to scheduled reports.</i></p>
  </td>
 </tr>
</table>




</body>
</html>
