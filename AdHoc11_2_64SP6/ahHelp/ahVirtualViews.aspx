<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc263162785">Virtual Views</a></h2>


<p class=MsoNormal><span style=''>The Virtual
Views page allows the system administrator to create customized views from
objects in the source database. End users can include virtual views as a data
source when building reports. Virtual views are saved to the application metadata
database; the source database is unmodified.</span></p>


<p class=MsoNormal><span style=''>Select <b>Virtual
Views</b> from the <i>Database Object Configuration</i> drop-down list<b> </b>to
display the <i>Virtual Views</i> configuration page.</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image078.jpg"></span></p>


<p class=MsoNormal><span style=''>The <b>Database</b>
dropdown list acts as a filter for the Virtual View list. Only Virtual Views
related to the selected database will be displayed. If only one reporting
database has been configured for the Ad Hoc instance, the <b>Database</b>
filter will not be shown.</span></p>


<p class=MsoNormal><span style=''>The <b>Add</b>
button will display an empty <i>Virtual View</i> page.</span></p>


<p class=MsoNormal><span style=''>The <b>Delete</b>
button will remove the selected virtual views. Virtual views are selected by
clicking on the applicable checkbox.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>Removing a
  virtual view marks any reports that depended on that virtual view as broken.
  Delete virtual views with caution.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>Two actions
are available for a Virtual View; <b>Modify Virtual View, </b>and<b> Delete
Virtual View. </b>Hover the mousepointer over the <img border=0 
 src="System_Admin_Guide_files/image025.jpg"> icon to show the
available actions.</span></p>


<b><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></b>


<p class=MsoNormal><i><u><span style=''>Adding
a Virtual View</span></u></i></p>


<p class=MsoNormal><span style=''>A virtual
view is essentially a pre-defined SQL Select statement. Typically virtual views
are used to relate multiple tables, perform data conversions, and reduce the
number of data objects that the end user sees.</span></p>


<p class=MsoNormal><span style=''>To create a
virtual view, click on the <b>Add</b> button. The following page will be
displayed:</span></p>


<p class=MsoNormal><b><span style=''><img
border=0   src="System_Admin_Guide_files/image079.jpg"></span></b></p>


<p class=MsoNormal><span style=''>The <i>Database
Connection</i> identifies the currently selected reporting database. </span></p>


<p class=MsoNormal><span style=''>Enter a
unique <i>Virtual View Name</i>, <i>Friendly Name</i>, and Definition. The <i>Definition</i>
is a SQL Select statement that conforms to the syntax rules for a SQL
sub-query.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>Virtual
  views are implemented in the report definitions as  SELECT * FROM (<i>Definition)</i> .</span></p>
  </td>
 </tr>
</table>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>Click on the <b>Test
Definition</b> button to verify the <i>Definition</i> syntactically using the
reporting database. If the <i>Definition</i> is tested successfully, the page
will display the schema information returned by the query. For example:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image080.jpg"></span></p>


<p class=MsoNormal><span style=''>Click on the <b>Save</b>
button to store the virtual view definition in the metadata database.</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<h5 align=left style='text-align:left'><i><span style='font-weight:normal'>Creating
Virtual Views Using 'Save As'</span></i></h5>


<p class=MsoNormal><span style=''>The
administrator can create a virtual view by modifying an existing virtual view
and then clicking on the <b>Save As</b> button, entering a new <i>Virtual View
Name</i> and <i>Friendly Name</i>, and clicking on the <b>OK</b> button.</span></p>


<p class=MsoHeader>&nbsp;</p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#FFFF99;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><i><span style=''>Hint:</span></i></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i><span style=''>Always
  remember to test the virtual view definition to ensure that the correct data
  columns are returned. Modifying the sub-query later may break reports that
  depend on the virtual view for data.</span></i></p>
  </td>
 </tr>
</table>



<h5 align=left style='text-align:left'><i><span style='font-weight:normal'>Modifying
Virtual Views</span></i></h5>


<p class=MsoNormal><span style=''>A virtual
view may be modified by clicking on the <img border=0  
src="System_Admin_Guide_files/image007.gif"> icon in the list of virtual views.
The <i>Friendly Name</i> and the <i>Definition</i> may be modified.</span></p>


<p class=MsoNormal><span style=''>Click on the <b>Test
Definition</b> button to verify the new information. The <i>Virtual View
Columns </i>grid will be updated and identify the differences between the
original <i>Definition</i> and the new <i>Definition</i>. For example:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image081.jpg"></span></p>



<p class=MsoNormal><span style=''>Click on the <b>Save</b>
button to store the modified information in the metadata database.</span></p>





<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Notes:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
  style=''>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span><span style=''>Modifying a
  virtual view's sub-query may break reports that depend on the virtual view
  for data. Modify virtual views with caution.</span></p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
  style=''>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span><span style=''>Use only SQL
  sub-query statements when creating virtual views; statements such as <b>ORDER
  BY</b></span><b><span style=''> </span></b><span
  style=''>are not supported. For security
  purposes, the following SQL statements are always prohibited: <b>EXECUTE</b>,
  <b>INSERT</b>, <b>UPDATE</b> and <b>DELETE</b>.</span></p>
  </td>
 </tr>
</table>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
