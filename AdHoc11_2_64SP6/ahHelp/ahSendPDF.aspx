<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291626" class="title">Send Report By Email</a></h4>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image187.gif"></p>


<p class=MsoNormal><i>Send Report by Email </i>opens a form where the email can
be composed and recipients specified.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image197.jpg"></p>

<p class=MsoNormal>Manually enter an email recipient(s) or click t<span
style='color:black'>he respective <img border=0  
src="Report_Design_Guide_files/image055.gif"> icon to view and select from a
list of application users. Specify a Subject and message.</span> When done, click
<b>Send</b> to send the email the report attached. Click <b>Cancel</b> to
cancel this export action.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Notes:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>The<i> 'From'</i>
  email address is defaulted to the email address specified in the logged in
  user's Profile.</p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>All fields
  except for Cc and Bcc are required.</p>
  </td>
 </tr>
</table>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
