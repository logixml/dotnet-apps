<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291629" class="title">Organizing Reports</a></h4>


<p class=MsoNormal>Users can create and store reports in the root folder or within
an existing folder. Administrators can create folders in the Shared Reports area,
which can be accessed by all users or limited to users based on the role(s)
assigned to the user.</p>


<p class=MsoNormal><b>To create a new folder to store reports:</b></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>Navigate to a report area (e.g., Personal Reports, Shared
     Reports) or folder in which the new folder is to be created.</li>
 <li class=MsoNormal>Click on the New Folder button.</li>
</ol>

<p class=MsoNormal style='margin-left:.25in'>&nbsp;</p>

<p class=MsoNormal style='margin-left:.25in'> <img border=0 
 src="Report_Design_Guide_files/image205.jpg"></p>


<ol style='margin-top:0in' start=3 type=1>
 <li class=MsoNormal>Type the name of the new folder into the Folder field</li>
 <li class=MsoNormal><u>OPTIONAL</u>: Type a description into the Description
     textbox.</li>
 <li class=MsoNormal><u>OPTIONAL</u>: As a designated administrator of a User
     Group and from the Shared Reports area, specify restricted access to this
     folder based on a user's role. By default, all roles have access to the
     folder. To limit access:</li>
 <ol style='margin-top:0in' start=1 type=a>
  <li class=MsoNormal>Select the <b>Specific Roles</b> option.</li>
  <li class=MsoNormal>Move roles between the <i>Available Roles </i>and the <i>Roles
      With Access</i> list-boxes by double-clicking on the listed role or by
      selecting the role and then clicking the <img border=0  
      src="Report_Design_Guide_files/image111.gif"> (right) or <img border=0
      width=9 height=15 src="Report_Design_Guide_files/image112.gif"> (left)
      icon. At a minimum, the roles currently assigned to the user creating the
      folder must have access.</li>
 </ol>
 <li class=MsoNormal>Click <b>Save </b>to create the new folder.</li>
</ol>



<p class=MsoNormal><b>To modify a report folder:</b></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>Click the <b>Edit</b> button to modify the corresponding
     report folder.</li>
</ol>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image206.jpg"></p>


<ol style='margin-top:0in' start=2 type=1>
 <li class=MsoNormal>As desired, modify the folder name and description in the
     provided fields.</li>
 <li class=MsoNormal><u>OPTIONAL</u>: As a designated administrator of a User
     Group and from the Shared Reports area, specify restricted access to this
     folder based on a user's role. By default, all roles have access to the
     folder. To limit access:</li>
</ol>

<ol style='margin-top:0in' start=6 type=1>
 <ol style='margin-top:0in' start=1 type=a>
  <li class=MsoNormal>Select the <b>Specific Roles</b> option.</li>
  <li class=MsoNormal>Move roles between the <i>Available Roles </i>and the <i>Roles
      With Access</i> list-boxes by double-clicking on the listed role or by
      selecting the role and then clicking the <img border=0  
      src="Report_Design_Guide_files/image111.gif"> (right) or <img border=0
      width=9 height=15 src="Report_Design_Guide_files/image112.gif"> (left)
      icon. At a minimum, the roles currently assigned to the user creating the
      folder must have access.</li>
 </ol>
</ol>

<ol style='margin-top:0in' start=4 type=1>
 <li class=MsoNormal>Click <b>Save </b>to commit the changes.</li>
</ol>


<p class=MsoNormal>Changes are reflected in the Name column and the Last
Modified column is updated to the current timestamp.</p>

<span style='font-size:10.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><b>To delete one or more report folders:</b></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>Select the desired folder(s) by enabling its respective
     checkbox(es).</li>
 <li class=MsoNormal>Click the <b>Delete </b>button.</li>
 <li class=MsoNormal>Click <b>OK </b>to confirm the removal.</li>
 <li class=MsoNormal>Alternatively, a single folder may be removed by hovering
     over the <b>More</b> button, selecting the <b>Delete</b> option and
     confirming the action.</li>
</ol>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>Deleting a report folder removes all reports stored within
  that folder. A backup copy of deleted reports is stored in the <b><i>..\_Definitions\_Reports\_Backup</i></b>
  directory before they are removed from the application. Imported reports will
  be deleted from being visible in the application interface but will not be
  deleted from the <i>_Reports</i> directory.</p>
  </td>
 </tr>
</table>



<p class=MsoNormal>After creating a new report folder, move reports out of the
root folder to make the workspace more manageable.</p>


<p class=MsoNormal><b>To move reports into a folder:</b></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>To move a single report, hover the mouse over the <b>More</b>
     button and select <b>Move</b> from the list of actions or select the
     report with the checkbox adjacent to the report name and click on the <b>Move</b>
     button. Multiple reports may be moved by selecting the reports and
     clicking on the <b>Move</b> button.</li>
</ol>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image207.jpg"></p>



<ol style='margin-top:0in' start=2 type=1>
 <li class=MsoNormal><u> OPTIONAL</u>: From the<b> </b><i>Organization</i>
     drop-down menu, select a user group. Organizations are not available by
     default. The administrator must have specifically enabled the Organization
     capability and created multiple organizations.</li>
 <li class=MsoNormal>From the <i>Destination Folder Type</i><b> </b>drop-down
     menu, select a folder type.</li>
 <li class=MsoNormal>From the <b>Folder</b> tree, locate and select a folder to
     store the report in.</li>
 <li class=MsoNormal>Click <b>Save </b>to move the report.</li>
</ol>



<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
