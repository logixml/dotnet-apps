<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc263162776">Organizations</a></h2>


<p class=MsoNormal><span style=''>Ad Hoc gives
the system administrator the ability to establish multiple organizations which
are collections of user communities. Every Ad Hoc user must be a member of one
organization, typically the  Default  organization.</span></p>


<p class=MsoNormal><span style=''>Organizations
generally do not share user communities, reports, or data.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>The ability
  to define multiple organizations must be enabled via the Management Console.
  Refer to the Management Console Usage Guide for details.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>Select <b>Organizations</b>
from the <i>User Configuration</i> drop-down list<b> </b>to display the <i>Organizations</i>
configuration page.</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image024.jpg"></span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>The <i>Default</i>
  organization is created with every new installation of the application.
  System administrators can rename the organization, but it may not be deleted.
  When attempting to delete the <i>Default</i> organization, all members are
  deleted, except for the System Administrator.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>The <b>Add</b>
button will display an empty <i>Organization</i> page.</span></p>


<p class=MsoNormal><span style=''>The <b>Delete</b>
button will remove the selected organizations. Organizations are selected by
clicking on the applicable checkbox.</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>The  <img
border=0   src="System_Admin_Guide_files/image025.jpg"> icon
indicates that more than one action can be performed on the organization. Hover
the mouse over the  <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon to display the available
actions and click on the appropriate one.</span></p>


<p class=MsoNormal><span style=''>The available
actions on the <i>Organizations</i> page are <b>Modify Organization, Delete
Organization</b> and <b>Set Session Parameters</b>. The <b>Set Session
Parameters</b> option will only be displayed if a session parameter has
previously been created for the application.</span></p>

<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<p class=MsoNormal><i><u><span style=''>Adding
an Organization</span></u></i></p>


<p class=MsoNormal><span style=''>To add an
organization, click on the <b>Add</b> button and the <i>Organization</i> page
will be displayed:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image026.jpg"></span></p>


<p class=MsoNormal><span style=''>The <i>Organization
Name</i> is required and must be unique. The <i>Description</i> of the
organization is optional. </span></p>


<p class=MsoNormal><span style=''>Click on the <b>Save</b>
button to store the new Organization information. </span></p>


<p class=MsoNormal><span style=''>The <i>Modify
Organization</i> page will be displayed (see below).</span></p>



<i><u><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></u></i>

<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<p class=MsoNormal><i><u><span style=''>Adding
an Organization Using  Save As </span></u></i></p>


<p class=MsoNormal><span style=''>The administrator
can create an organization by modifying an existing organization and then
saving it with a new name by clicking on the <b>Save As</b> button. The <b>Save
As</b> option also copies the session parameters and values, if any, from the
existing organization to the new organization.</span></p>


<p class=MsoHeader>&nbsp;</p>

<h5 align=left style='text-align:left'><i><span style='font-weight:normal'>Modifying
Organizations</span></i></h5>


<p class=MsoNormal><span style=''>System administrators
can change the name, description, and theme of an organization at any time. Each
organization must have a unique name. </span></p>


<p class=MsoNormal><span style=''>To change organization
information, click the <b>Modify Organization</b> action for the organization.  The
<i>Modify</i> <i>Organization</i> page will be displayed:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image027.jpg"></span></p>


<p class=MsoNormal><span style=''>Modify the <i>Organization
Name</i> or <i>Description </i>and click the <b>Save</b> button to store the
information.</span></p>


<p class=MsoNormal><span style=''>To save the
edited information by a new <i>Organization Name</i>, click on the <b>Save As</b>
button, enter the new name and click on the <b>OK</b> button to save the
information.</span></p>

<p class=MsoHeader>&nbsp;</p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>The <i>Selected
  Organization</i> drop-down list may be used to navigate through organizations
  and avoid using the <b>Back to Organizations</b> button and selecting a
  different organization.</span></p>
  </td>
 </tr>
</table>

<p class=MsoHeader>&nbsp;</p>

<p class=MsoHeader>&nbsp;</p>

<h5 align=left style='text-align:left'><a name="_Ref169938361"><i><span
style='font-weight:normal'>Setting an Organization s Session Parameters</span></i></a></h5>


<p class=MsoNormal><span style=''>System
administrators can create multiple session variables called <i>session
parameters</i> that may be used for display purposes or to filter data within a
report or data object. Session parameters and values are initially defined with
application scope. The session parameter values may be over-ridden for each
organization. </span></p>


<p class=MsoNormal><span style=''>To change the
session parameter values for an organization, click on the <b>Set Session
Parameters</b> action for the organization. The <i>Organization Session
Parameters</i> page will be displayed:</span></p>


<p class=MsoNormal><img border=0  
src="System_Admin_Guide_files/image028.jpg"></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>If Session
  Parameters have not been defined, the option to over-ride the session
  parameter values for organizations will not be available.</span></p>
  </td>
 </tr>
</table>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#FFFF99;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Hint:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>Refer to
  the <b>Session Parameters</b> chapter for help creating parameters and
  setting the default values.</span></p>
  </td>
 </tr>
</table>

<p class=MsoHeader>&nbsp;</p>

<p class=MsoNormal><span style=''>The <i>Selected
Organization</i> identifies the organization that was the action target from
the previous page. The dropdown list allows the Administrator to select other
organizations and review or adjust their session parameters without having to
return to the Organizations page.</span></p>


<p class=MsoNormal><span style=''>The grid presents
the session parameters that may be set for the organization. Click on the <i>Parameter
Name </i>or the <i>Follow Default</i> column header to sort the contents of the
grid.</span></p>


<p class=MsoNormal><span style=''>The <b>Restore
Defaults</b> button provides a mechanism to set all of the highlighted
(checked) session parameters back to the values shown in the <i>Default Value</i>
column.</span></p>


<p class=MsoNormal><span style=''>The <i>Follow
Default</i> checkbox indicates whether the parameter value should adopt the <i>Default
Value</i>. This also allows the parameter value to be set  permanently ,
meaning that changes to the <i>Default Value</i> will have no impact on the
parameter value for the organization, if the <i>Follow Default</i> checkbox is
unchecked.</span></p>


<p class=MsoNormal><span style=''>The grayed <i>Parameter
Value</i> text boxes are not disabled. The gray is a visual cue that the value
is the same as the default value and is expected to follow the default value.
That mean that if the default value changes, the organization will
automatically pick up the new value.</span></p>


<p class=MsoNormal><span style=''>The <i>Parameter
Value</i> may be changed by either typing directly into the text box or
selecting the<b> Modify</b> action and providing a new value. The parameter
value may also be changed by selecting the <b>Restore Default</b> action.</span></p>


<p class=MsoNormal><span style=''>If any <i>Parameter
Value</i> is set it will be considered an override value and the  <i>Follow
Default</i>  checkbox will automatically be unchecked. </span></p>


<p class=MsoNormal><span style=''>Click on the <b>Save</b>
button to store the session parameter values for the organization.</span></p>


<p class=MsoNormal><span style=''>An action
that is available for each session parameter is <b>Set By User.</b> Clicking on
this action will present a page that allows the session parameter values to be
set for all users in the organization.</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image029.jpg"></span></p>


<p class=MsoNormal><span style=''>The <i>Selected
Session Parameter</i> initially identifies the parameter that was specified
when linking to this page. The dropdown list allows the Administrator to review
and modify other session parameter values for the group of users.</span></p>


<p class=MsoNormal><span style=''>The <i>Default
Value</i> is the value that will be used for all of the users in the list
unless the value is specifically over-ridden.</span></p>


<p class=MsoNormal><span style=''>The <i>Type</i>
indicates one of the five session parameter types; date, number, numeric list,
text and textual list.</span></p>


<p class=MsoNormal><span style=''>The <i>Role</i>
dropdown list may be used to filter the list of users. Initially the dropdown
indicates  All  and the list of user are all users in the Organization.</span></p>


<p class=MsoNormal><span style=''>The <b>Restore
Default</b> button will set the session parameter value for all selected users
back to the displayed Default Value. Users may be selected by clicking on the
checkbox next to the user. All users may be selected by clicking on the
checkbox in the list header.</span></p>


<p class=MsoNormal><span style=''>The <b>Set
Value</b> button will open a dialog to acquire a new value and will apply the
value to all of the selected users.</span></p>


<p class=MsoNormal><span style=''>The list of
users may be sorted by clicking on the <i>User</i> or <i>Follow Default</i>
column header.</span></p>


<p class=MsoNormal><span style=''>The <i>Follow
Default</i> checkbox indicates whether the parameter value should adopt the <i>Default
Value</i>. This also allows the parameter value to be set  permanently ,
meaning that changes to the <i>Default Value</i> will have no impact on the
parameter value for the user, if the <i>Follow Default</i> checkbox is
unchecked.</span></p>


<p class=MsoNormal><span style=''>The grayed <i>Parameter
Value</i> text boxes are not disabled. The gray is a visual cue that the value
is the same as the default value and is expected to follow the default value.
That mean that if the default value changes, the user will automatically pick
up the new value.</span></p>


<p class=MsoNormal><span style=''>The <i>Parameter
Value</i> may be changed by either typing directly into the text box or
selecting the<b> Modify</b> action and providing a new value. The parameter
value may also be changed by selecting the <b>Restore Default</b> action.</span></p>


<p class=MsoNormal><span style=''>If any <i>Parameter
Value</i> is set it will be considered an override value and the  <i>Follow
Default</i>  checkbox will automatically be unchecked. </span></p>


<p class=MsoNormal><span style=''>To change the
parameter value, either enter the new value in the <i>Parameter Value</i> text
box or hover the mousepointer over the <i>Actions</i> <img border=0 
 src="System_Admin_Guide_files/image025.jpg"> icon and select <b>Modify</b>
from the list of actions. </span></p>


<p class=MsoNormal><span style=''>The <b>Modify</b>
action will present a dialog relevant to the type of the session parameter.
 Date  session parameters will have a date picker control and  List  session
parameters will present a list of values in the dialog.</span></p>


<p class=MsoNormal><span style=''>From the<i>
Actions</i> icon, the session parameter value for the related user may be reset
to the default value for the Organization by clicking on the <b>Restore Default</b>
option.</span></p>


<p class=MsoNormal><span style=''>Click on the <b>Save</b>
button to store the session parameter values for the User.</span></p>


<p class=MsoHeader>&nbsp;</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>

<p class=MsoHeader>&nbsp;</p>


</body>
</html>
