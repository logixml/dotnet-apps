<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291633" class="title">Modifying Reports</a></h4>


<p class=MsoNormal><b>To modify a report:</b></p>


<p class=MsoNormal>To modify a report, click on the <b>Edit</b> button
associated with the report. The Report Builder interface will be invoked.</p>



<p class=MsoNormal><b>To rename a report:</b></p>


<p class=MsoNormal>To rename a report, hover the More button and select <i>Rename</i>
from the list. </p>


<p class=MsoNormal><b><img border=0  
src="Report_Design_Guide_files/image221.jpg"></b></p>


<p class=MsoNormal>Modify the <i>Report Name, Description</i> and/or <i>Expiration
Date</i>.</p>


<p class=MsoNormal>Click on the <b>Save </b>button<b> </b>to save the changes.</p>


<b><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></b>


<p class=MsoNormal><b>To delete one or more reports and the associated
archives:</b></p>


<p class=MsoNormal>Select the desired report(s) by enabling its respective
checkbox(es). Click on the <b>Delete</b> button to remove the selected reports.</p>


<p class=MsoNormal>Click the <b>OK </b>button<b> </b>to confirm the removal.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>Deleting a
  report removes the report from the Reports list.</p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>A copy of
  deleted reports and dashboards are placed in the <i>..\_Definitions\_Reports\_Backup</i>
  directory. Imported reports will be deleted from being visible in the
  application interface but will not be deleted from the <i>_Reports</i>
  directory.</p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>Archives
  associated to the deleted report(s) will be deleted according to the setting
  specified by the System Administrator in the <i>Application Settings</i>
  webpage.</p>
  </td>
 </tr>
</table>

</body>
</html>
