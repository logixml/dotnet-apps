<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291606" class="title">Configuring Table Columns</a></h4>


<p class=MsoNormal>The first step of table configuration is to select the
columns to be used in the display table. If the report contains more than one
display table, the configuration area will reflect the currently selected
display table.</p>


<p class=MsoNormal><b>To select columns for the table:</b></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>Click on the <i>Table Columns </i>tab.</li>
</ol>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image078.jpg"></p>


<ol style='margin-top:0in' start=2 type=1>
 <li class=MsoNormal> Select one or more<i> Available Columns</i> and then
     click <img border=0  
     src="Report_Design_Guide_files/image079.jpg"> to add the column(s) to the<i>
     Assigned Columns</i> list-box. Hold the CTRL key down to select multiple
     columns.</li>
 <li class=MsoNormal>From the <i>Assigned Columns</i> list-box, change a
     column's initial display order by clicking on either the <img border=0
       src="Report_Design_Guide_files/image080.jpg"> or <img
     border=0 width=24 height=24 src="Report_Design_Guide_files/image081.jpg"> icon
     to move the row up or down. Hold the CTRL key down to select multiple
     columns.</li>
 <li class=MsoNormal>If a column(s) in the <i>Assigned Columns</i> list-box is
     not desired, select one or more <i>Assigned Columns </i>and then click <img
     border=0   src="Report_Design_Guide_files/image082.jpg"> to
     remove the column(s) from the <i>Assigned Columns</i> list-box. Hold the
     CTRL key down to select multiple columns.</li>
</ol>



<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Notes:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>At least one
  data column should be selected before continuing.</p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>A column
  description will only be available if one has been specified by the System
  Administrator.</p>
  </td>
 </tr>
</table>

<span style='font-size:6.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><b>To configure and delete columns in the table:</b></p>


<p class=MsoNormal>After having added columns, a column's order of appearance,
display characteristics and summary information may be defined on the <i>Column
Configuration</i> tab. </p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image083.jpg"></p>


<p class=MsoNormal>Initially only the Column, Header and Sortable columns are
shown in the configuration grid. By clicking on the Show All Attributes icon,
the grid is expanded as shown above. The grid may be collapsed by clicking on
the Show Minimum Attributes icon.</p>


<p class=MsoNormal>Columns may be selected (or deselected) by clicking on the
checkbox adjacent to the column. All columns may be selected or deselected by
clicking on the checkbox in the upper left corner of the grid. Some of the
following functions apply to the selected columns.</p>


<p class=MsoNormal>Columns may be rearranged by selecting the row and then
click either the <img border=0  
src="Report_Design_Guide_files/image084.jpg"> or <img border=0 
height=24 src="Report_Design_Guide_files/image085.jpg"> icon to move the row up
or down. To move a group of columns, select the desired columns by enabling
their respective checkboxes and then click either the <img border=0 
 src="Report_Design_Guide_files/image084.jpg"> or <img border=0
width=25 height=24 src="Report_Design_Guide_files/image085.jpg"> icon to move
the rows up or down.</p>


<p class=MsoNormal>Columns may be removed from the display table by selecting
the column(s) and clicking the <img border=0  
src="Report_Design_Guide_files/image086.gif">icon.</p>


<b><span style='font-size:10.0pt;'><br
clear=all style='page-break-before:always'>
</span></b>


<p class=MsoNormal>There are 9 configurable options available in Column
Configuration:</p>


</div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection6>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Header</li>
 <li class=MsoNormal>Linkable</li>
 <li class=MsoNormal>Sortable</li>
 <li class=MsoNormal>Summary</li>
 <li class=MsoNormal>Format</li>
 <li class=MsoNormal>Width</li>
 <li class=MsoNormal>Alignment</li>
 <li class=MsoNormal>Style</li>
</ul>

</div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection7>



<p class=MsoNormal>The <i>Header</i> determines the column header displayed
when the report is rendered.</p>


<p class=MsoNormal>The <i>Linkable</i> option toggles predefined hyperlinks for
records in the column. Each record in the <i>Linkable</i> column contains a
hyperlink to an address specified by the System Administrator. Make column
records linkable from the report by placing a check in the corresponding <i>Linkable</i>
checkbox.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>The <i>Linkable</i> column will only be shown if the
  System Administrator has configured at least one of the columns as  hyperlink
  capable .</p>
  </td>
 </tr>
</table>



<p class=MsoNormal>The <i>Sortable</i> option determines whether the column in
the rendered report may be sorted by the end user. The <i>Sortable</i> checkbox
determines the column s sort capability.  Sort capability may be enabled or
disabled for all columns by clicking on the checkbox in the header of the <i>Sortable</i>
column.</p>


<p class=MsoNormal>The <i>Summary</i> option offers the ability to create table
footers containing aggregates of values for each column of data. An unlimited
number of aggregates can be created for each table column. The following
aggregate functions are supported:</p>


</div>

<span style='font-size:6.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection8>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Sum</li>
 <li class=MsoNormal>Average</li>
 <li class=MsoNormal>Standard Deviation</li>
 <li class=MsoNormal>Count</li>
 <li class=MsoNormal>Count Distinct</li>
 <li class=MsoNormal>Maximum</li>
 <li class=MsoNormal>Minimum</li>
 <li class=MsoNormal>Calculation</li>
</ul>


</div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection9><b><br clear=all style='page-break-before:always'>
</b>


<p class=MsoNormal><b>To manage summary values:</b></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>Click <img border=0  
     src="Report_Design_Guide_files/image087.gif"> to add a summary value for a
     specific column.</li>
</ol>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image088.jpg"></p>


<ol style='margin-top:0in' start=2 type=1>
 <li class=MsoNormal>Click the <b>Add an Aggregate</b> button on the <i>Aggregates</i>
     dialog.</li>
 <li class=MsoNormal>Type a name used as the internal value for the aggregate
     in the <i>Name</i> field.</li>
 <li class=MsoNormal>Type a displayed name for the new value in the <i>Label</i>
     field.</li>
 <li class=MsoNormal>Choose an <i>Aggregate</i> function from the drop-down
     menu.</li>
 <li class=MsoNormal>Choose a <i>Format</i> from the drop-down menu.</li>
 <li class=MsoNormal><u>OPTIONAL</u>: If more than one aggregate has been
     specified, click the <img border=0  
     src="Report_Design_Guide_files/image089.gif"> or <img border=0 
     height=9 src="Report_Design_Guide_files/image090.gif"> icon to arrange the
     order in which the aggregate will appear in the column. Continue adding
     additional aggregates or click <b>OK</b> to add the summary value(s) and
     return to the <i>Column Configuration</i> interface.</li>
 <li class=MsoNormal>Aggregates may be removed by clicking the <img border=0
       src="Report_Design_Guide_files/image086.gif">icon</li>
</ol>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>The <img border=0  
  src="Report_Design_Guide_files/image091.gif"> icon indicates that a summary
  value exists for that particular column.</p>
  </td>
 </tr>
</table>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>One of the aggregation options is  Calculation . This option
allows the user to create a new aggregation from previously defined summary
information. When  calculation  is selected, the following dialog is displayed:
</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image092.jpg"></p>


<p class=MsoNormal>The  Avg Price  and  Sum Qty  aggregates have been used in
the calculation of the  Average Line Item  in this example. </p>


<p class=MsoNormal>In the Modify Calculation dialog, any of the previously
defined summaries may be used in the calculation as well as aggregate functions
on columns and direct constants in the definition. The default internal name
for an aggregation is AGGRn, where  n  is a unique number.</p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>The <i>Format</i> option provides data formatting options
for values in each column. The following formatting options are supported:</p>


</div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection10>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>(none)</li>
 <li class=MsoNormal>General Number</li>
 <li class=MsoNormal>Currency</li>
 <li class=MsoNormal>Integer</li>
 <li class=MsoNormal>Fixed</li>
 <li class=MsoNormal>Standard</li>
 <li class=MsoNormal>Percent</li>
 <li class=MsoNormal>Scientific</li>
 <li class=MsoNormal>2 or 3-digit place holder</li>
 <li class=MsoNormal>General Date</li>
 <li class=MsoNormal>Long/Medium/Short Date</li>
 <li class=MsoNormal>Long/Medium/Short Time</li>
 <li class=MsoNormal>Yes/No</li>
 <li class=MsoNormal>True/False</li>
 <li class=MsoNormal>On/Off</li>
 <li class=MsoNormal>HTML<sup>2</sup></li>
 <li class=MsoNormal>Preserve line feed<sup>3</sup></li>
</ul>

</div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection11>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial>Notes:</p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><i>1.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></i><i>The
  application chooses the default Format type for each column. Changing the
  format type may yield undesirable results.</i></p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><i>2.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></i><i>The
  Format of &quot;HTML&quot; may only be specified by the System Administrator
  and may not be changed to something different from the Column Configuration
  panel.</i></p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><i>3.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></i><i>The
  Format of 'Preserve line feed' allows text in a memo type field to display as
  it is stored with line feeds (a.k.a., carriage returns) observed.</i></p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><i>4.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></i><i>If a
  Format is needed that is not offered, contact your System Administrator.</i></p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><i>5.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></i><i>Additional
  formats may be provided by the System Administrator</i></p>
  </td>
 </tr>
</table>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>



<p class=MsoNormal>The <i>Width</i> option offer the ability to customize the
width of each column, thereby improve the appearance of the report when it is
rendered in the webpage and when exported (e.g., Word, PDF). By default, a
column's <i>Width</i> value is left blank to allow the application the option
to automatically determine the appropriate width based on:</p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>All columns in the report</li>
 <li class=MsoNormal>The context in each column</li>
 <li class=MsoNormal>The available webpage space</li>
 <li class=MsoNormal>The page size and orientation</li>
</ul>


<p class=MsoNormal>When customizing a column's width, it is important to
determine the scale by which an entered value will be measured. The scale of
measured can be either <b>pixels</b> or <b>percent</b> where: </p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><i>Pixel</i> is a single point of picture data displayed
     on the monitor. Hundreds of pixels can be used to display a very small
     image. </li>
 <li class=MsoNormal><i>Percentage</i> is a fraction of the screen space
     allocated for each column.</li>
</ul>


<p class=MsoNormal><b>To modify a column's width:</b></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal><u>OPTIONAL</u>: Determine the scale type to use for the
     entire tabular report. To toggle the scale of a column, click on the label
     to the right on the width field where <b>px</b> equates to pixel and <b>%</b>
     equates to percentage.</li>
 <li class=MsoNormal>Input or modify the column's Width value.</li>
</ol>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial>Notes:</p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><i>1.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></i><i>When
  specifying a tabular report's column's widths in pixels, keep in mind the
  average monitor resolution settings of the end-users viewing the report.</i></p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><i>2.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></i><i>When
  specifying a tabular report's column's widths in percentages, keep in mind
  that:</i></p>
  <p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><i>a.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></i><i>The sum
  total of the percentage values must not exceed 100%.</i></p>
  <p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><i>b.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></i><i>The application
  will use whatever percentage has not been allocated to columns for the
  columns without a value.</i></p>
  </td>
 </tr>
</table>

<span style='font-size:10.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>The <i>Alignment</i> option adjusts the position of values
in columns. In the following figure, numerical values are now centered in the <i>Quantity</i>
column.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse'>
 <tr style='height:28.35pt'>
  <td width=197 style='width:2.05in;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoNormal align=center style='text-align:center'><img border=0
    src="Report_Design_Guide_files/image093.jpg"></p>
  </td>
  <td width=197 style='width:2.05in;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoNormal align=center style='text-align:center'><img border=0
    src="Report_Design_Guide_files/image094.gif"></p>
  </td>
  <td width=197 style='width:2.05in;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoNormal align=center style='text-align:center'><img border=0
    src="Report_Design_Guide_files/image095.jpg"></p>
  </td>
 </tr>
</table>


<p class=MsoNormal>The <i>Style</i> option<i> </i>offers the ability to apply
conditional formatting to a column's cell based on a specific value or another
column's value. Users must create the condition and specify the formatting
style. When more than one condition is specified, the application will apply
the style associated to the first condition that is satisfied. Conditions are
evaluated when the report is run.</p>


<p class=MsoNormal>Users can optionally apply the specified conditional
formatting to all data columns by checking the appropriate checkbox.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image096.gif"></p>


<p class=MsoNormal>A conditional style takes the form of an equation similar
to:</p>


<p class=MsoFooter align=center style='text-align:center'><i>Label </i>is <i>Compared
</i>to <i>Value </i></p>

<p class=MsoFooter align=center style='text-align:center'>or</p>

<p class=MsoFooter align=center style='text-align:center'><i>Label</i> is <i>Compared</i>
to <i>Column</i></p>


<p class=MsoFooter>where L<i>abel</i> represents a column name, <i>Compared to</i>
represents a comparison operator, <i>Value</i> represents a threshold, and <i>Column</i>
represents another data column.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoFooter>The available comparison operators are:</p>


</div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection12>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Equal to</li>
 <li class=MsoNormal>Not equal to</li>
 <li class=MsoNormal>Less than</li>
 <li class=MsoNormal>Greater than</li>
 <li class=MsoNormal>Less than or equal to</li>
 <li class=MsoNormal>Greater than or equal to</li>
 <li class=MsoNormal>Starts with <sup>1</sup></li>
 <li class=MsoNormal>Does not start with <sup>1</sup></li>
 <li class=MsoNormal>Ends with <sup>1</sup></li>
 <li class=MsoNormal>Does not end with <sup>1</sup></li>
 <li class=MsoNormal>Contains <sup>1</sup></li>
 <li class=MsoNormal>Does not contain <sup>1</sup></li>
 <li class=MsoNormal>Between</li>
 <li class=MsoNormal>Not between</li>
</ul>

</div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection13>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Notes:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>These
  operators are only available for data type of type String or Text.</p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>The operators
  available are dependent upon the column's data type.</p>
  </td>
 </tr>
</table>


<p class=MsoNormal><b>To add a conditional Style:</b></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>Click <img border=0  
     src="Report_Design_Guide_files/image097.gif"> or <i><img border=0
     width=15 height=15 src="Report_Design_Guide_files/image098.gif"> </i>to access
     the Condition Styles dialog. In the figure below two styles have been
     added and the Add a Condition panel opened to show the various options in
     the dialog.</li>
</ol>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image099.jpg"></p>


<ol style='margin-top:0in' start=2 type=1>
 <li class=MsoNormal>Click the <b>Add a Condition </b>button. In the panel are
     the <i>Column</i>, <i>Operator</i>, <i>Value </i>and <i>Style </i>attributes.</li>
 <li class=MsoNormal>Choose a <i>Column</i> from the drop-down menu to base the
     conditional styling on.</li>
 <li class=MsoNormal>Choose a comparison <i>Operator</i> from the drop-down
     menu.</li>
 <li class=MsoNormal>From the <i>Value</i> type drop-down menu, choose either <i>Specific
     Value,</i> <i>Pre-defined Date</i>, or <i>Other Data Column</i>.</li>
 <li class=MsoNormal>Specify a threshold Value. If a Value type of:</li>
 <ol style='margin-top:0in' start=1 type=a>
  <li class=MsoNormal><i>Specific Value </i>was selected, then type in a value
      or click the <img border=0  
      src="Report_Design_Guide_files/image055.gif"> icon to view and select a
      valid value from the database.</li>
  <li class=MsoNormal><i>Pre-defined Date </i>was selected, select a token (i.e.,
      Today).</li>
  <li class=MsoNormal><i>Other Data Column</i> was selected, select a column
      from the dropdown list.</li>
 </ol>
 <li class=MsoNormal>Choose a <i>Style</i> from the dropdown list.</li>
 <li class=MsoNormal>Click <b>OK</b> to add the styling condition.</li>
 <li class=MsoNormal>Add more parameters by clicking <b>Add a Condition</b> and
     repeating the steps above.</li>
 <li class=MsoNormal><u>OPTIONAL</u>: As styles are applied based on the first
     condition that is satisfied, move a condition up or down in the list by
     clicking the <img border=0  
     src="Report_Design_Guide_files/image089.gif"> or <img border=0 
     height=9 src="Report_Design_Guide_files/image090.gif"> icon respectively.</li>
 <li class=MsoNormal><u>OPTIONAL</u>: Enable <b><i>Add this style to all
     columns</i></b> to apply the Style to all columns of a row in the table.</li>
 <li class=MsoNormal>Click <b>OK</b> to save the styling condition(s) and
     return to the Column Configuration interface.</li>
</ol>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial>Notes:</p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><i>1.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></i><i>Pre-defined
  dates get evaluated at the time the report runs.</i></p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><i>2.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></i><i>If the
  value is a number, the <b>value </b>field must contain a valid number to complete
  the comparison.</i></p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><i>3.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></i><i>The <img
  border=0 width=15 height=15 src="Report_Design_Guide_files/image098.gif"> icon
  indicates that a least one conditional style exists for that particular
  column.</i></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><b>To modify or remove a conditional Style:</b></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>Click the <img border=0  
     src="Report_Design_Guide_files/image098.gif"> icon to access the Style
     Details panel for a specific column.</li>
 <li class=MsoNormal>From the Style Details panel:</li>
 <ol style='margin-top:0in' start=1 type=a>
  <li class=MsoNormal>If modifying a Style, click the <img border=0 
       src="Report_Design_Guide_files/image056.gif"> icon associated
      to a specific Style's condition. Modify the conditions as desired and
      then click <b>OK</b><span style='font-size:10.0pt'> </span>to save the
      condition.</li>
  <li class=MsoNormal>If removing a Style, click the <img border=0 
       src="Report_Design_Guide_files/image086.gif"> icon associated to
      a specific Style's condition.</li>
  <li class=MsoNormal><u>OPTIONAL</u>: Enable/Disable <b><i>Add this style to
      all columns</i></b> to apply/remove the Style to/from all columns in the
      table.</li>
 </ol>
 <li class=MsoNormal>Click <b>OK</b> to save the modifications and return to
     the Column Configuration interface.</li>
</ol>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoFooter><i>Adding a new custom column to the data table</i></p>


<p class=MsoFooter>From the Column Configuration page, a new column may be
added to the data table by clicking on the <b>Add Custom Column</b> button.
Columns created in this manner may reference previously defined summary
information. </p>


<p class=MsoFooter>In the example below, a percentage column (<i>Pct Qty</i>)
has been created using the<i> Quantity</i> column and the <i>SumQty</i>
summary.</p>


<p class=MsoFooter><img border=0  
src="Report_Design_Guide_files/image100.jpg"></p>


<p class=MsoFooter>The sequence of events for this example was:</p>


<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>Click on the <b>Add
Calculated Column</b> button on the <i>Column Configuration</i> page</p>

<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>Click on the <i>Quantity</i>
column from the <i>Order Details</i> data object</p>

<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>Click on the
division symbol from the list of operators</p>

<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>Click on the <i>SumQty</i>
summary from the A<i>vailable Summaries </i>(previously created as a simple
column summary and named <i>SumQty</i>)</p>

<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>Click on the
multiplication symbol from the list of operators</p>

<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>6.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>Type 100.0 in
the <i>Definition</i> text area</p>

<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>7.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>Type <i>Pct Qty</i>
in the <i>Name</i> text box</p>

<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>8.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>Click on the <b>OK</b>
button to save the result</p>


<p class=MsoFooter><b>Note:</b> This type of calculated column may use
reporting summary information in the calculated column definition.</p>

<p class=MsoFooter><b>Note:</b> If a data table contains a calculated column,
the configuration grid will display an <b>Actions</b> column. Clicking on the <img
border=0   src="Report_Design_Guide_files/image101.jpg"> icon
allows the column to be edited.<br clear=all style='page-break-before:always'>
</p>

</body>
</html>
