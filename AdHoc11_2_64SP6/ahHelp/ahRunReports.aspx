<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h3 class="title"></h3>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image198.jpg"></p>


<p class=MsoNormal>The <b>Report</b><b>s</b> button displays the main page of the
application.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image199.jpg"></p>


<p class=MsoNormal>A list of reports is available under the <i>Personal Reports
</i>and <i>Shared Reports </i>tabs. If no reports have been created, this list
is empty.</p>


<p class=MsoNormal>Click the name of the report to launch it in a new browser
window.</p>



<p class=MsoHeading7>Interacting with a Report</p>


<p class=MsoFooter><img border=0  
src="Report_Design_Guide_files/image200.jpg"></p>

<p class=MsoFooter><span style='font-size:10.0pt'>Any reporting components that
reference the <i>Category </i>data column will show only orders related to
 Beverages .</span></p>


<p class=MsoNormal>The figure above shows a field requesting a parameter value.
Runtime parameters are defined on the <i>Modify Data Source</i> dialog under
the <i>Filter</i> tab. Click on the <b>Modify Data Source</b> button to access
the dialog. A default value is chosen during the report-creation process and
can be changed by the user.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>Date values less than 01/01/1753 may not be specified.</p>
  </td>
 </tr>
</table>


<p class=MsoNormal>There are two options for running the report.</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><b>Run and Hide Parameters</b> - Run the report and hide
     parameter input box</li>
 <li class=MsoNormal><b>Run</b> - Run the report and leave parameter input box
     visible</li>
</ul>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#FFFF99;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial>Hint:</p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i>If the report contains numerous parameters, click <b>Run
  and Hide Parameters</b> to provide more screen space for the report. Run the
  report again to input additional parameters. The <img border=0 
   src="Report_Design_Guide_files/image201.jpg"> icon will hide the
  parameter area.</i></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><i>Run</i> and <i>Run and Hide Parameters</i> affect the
parameters for the report. Any additional settings specified in the Report
Builder are left intact.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>Run and Run and Hide Parameters are only available by
  placing a check in the 'Ask' checkbox when defining parameters.</p>
  </td>
 </tr>
</table>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoHeading7>Cascade Filters</p>

<p class=MsoFooter>Cascading filter parameters produce one or more drop-down
menus at runtime. The value selected from the first drop-down menu is used to
filter the second drop-down menu and so on. The last parameter is selected from
a list and passed to the report when the user clicks <b>Run</b>.</p>


<p class=MsoFooter><img border=0  
src="Report_Design_Guide_files/image202.jpg"></p>

<p class=MsoFooter><span style='font-size:10.0pt'>Cascading filter parameters
produce multiple, dependent drop-down menus.</span></p>



<p class=MsoHeading7>Interactive Sorting/Column Management in a Report</p>


<p class=MsoFooter>When a tabular report is run, the user may interact with the
resultant columns. They have the ability to sort the table based on one column,
adjust the column width, and rearrange the columns.</p>


<p class=MsoFooter><img border=0  
src="Report_Design_Guide_files/image203.jpg"></p>

<p class=MsoFooter><span style='font-size:10.0pt'>Interactions via the column header</span></p>


<p class=MsoFooter>The column header, <i>Customer ID</i> is displayed as a link.
The heading link allows the user to sort that column. The <i>sort</i> option
was configured in the Column Configuration panel of the Table component in the
Report Builder.</p>

<p class=MsoFooter>To the left of the header is a drag area. Mousedown on the
drag area, move the mouse to the target location, and release the mouse to move
the column .</p>


<p class=MsoFooter>To the right of the header is the column resize control.
Mousedown on the control and drag the control to the left or right to reduce or
expand the column width, respectively.</p>



<p class=MsoHeading7>Export Options</p>


<p class=MsoFooter><img border=0  
src="Report_Design_Guide_files/image204.jpg"></p>

<p class=MsoFooter><span style='font-size:10.0pt'>Export options always appear
at the bottom of a report.</span></p>


<p class=MsoFooter>Export options give users the ability to export reports to
popular formats such as Microsoft Excel and Adobe PDF. Users can also search
the report, print from the browser and add the report to the archive. Add or
remove export options through the Report Builder.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>The various export options will appear if the user viewing
  the report has Rights to add the respective export option(s) to the report in
  the Report Builder.</p>
  </td>
 </tr>
</table>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
