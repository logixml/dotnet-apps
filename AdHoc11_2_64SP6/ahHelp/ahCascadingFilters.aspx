<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc263162788">Cascading Filters</a></h2>


<p class=MsoNormal><span style=''>The Cascading
Filters page allows the system administrator to create and configure cascading
filters for use when building reports with the Report Builder. The term <i>cascading
filter </i>refers to a series of user input drop-down menus where the value
selected in the first drop-down affects the values displayed in the second
drop-down and so forth. Cascading filters have no default values but instead
rely on user input at runtime.</span></p>


<p class=MsoNormal><span style=''>Select <b>Cascading
Filters</b> from the <i>Report Configuration</i> drop-down list<b> </b>to
display the <i>Cascading Filters</i> configuration page.</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image084.jpg"></span></p>


<p class=MsoNormal><span style=''>The <b>Database</b>
dropdown list acts as a filter for the Cascade Filter list. Only Cascade
Filters related to the selected database will be displayed. If only one
reporting database has been configured for the Ad Hoc instance, the <b>Database</b>
filter will not be shown.</span></p>


<p class=MsoNormal><span style=''>The <b>Add</b>
button will display an empty <i>Cascade Filter</i> page.</span></p>


<p class=MsoNormal><span style=''>The <b>Delete</b>
button will remove the selected cascading filters. Cascading filters are
selected by clicking on the applicable checkbox.</span></p>


<p class=MsoNormal><span style=''>Two actions
are available for a Cascade Filter; <b>Modify Filter, </b>and<b> Delete Filter.
</b>Hover the mousepointer over the <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon to show the available
actions.</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>Each
cascading filter is comprised of one or more filter items. One <i>filter item </i>translates
to one drop-down menu. Administrators must configure the following attributes
for each filter item:</span></p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><i><span style=''>Data
     Object</span></i><span style=''> - the
     data object used to populate the drop-down menu</span></li>
 <li class=MsoNormal><i><span style=''>Filter
     Column</span></i><b><span style=''> </span></b><span
     style=''>- the column that is filtered by
     the value selected from the previous filter item</span></li>
 <li class=MsoNormal><i><span style=''>Value
     Column</span></i><b><span style=''> </span></b><span
     style=''>- the column that provides the
     value for the next filter item</span></li>
 <li class=MsoNormal><i><span style=''>Display
     Column</span></i><b><span style=''> </span></b><span
     style=''>- the column that provides the
     values for the drop-down menu</span></li>
</ul>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>The first
  filter item always contains a blank value for the Filter Column.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>Cascading
filters are used as parameters in reports. From the Report Builder, end users
must select a column to filter and then choose <b>In cascading list </b>as the
operator. A list of all the cascading filters present is displayed and the end
user must choose one.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>The  <i>In
  cascading list</i>  and  <i>Not in cascading list</i>  options are only
  presented to the end user creating a report when the column selected to be
  filtered matches the <u>last</u> <i>Value Column</i> in the cascading filter
  definition.</span></p>
  </td>
 </tr>
</table>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><i><u><span style=''>Adding
a new Cascading Filter</span></u></i></p>


<p class=MsoNormal><span style=''>Click on the <b>Add</b>
button to define a new cascading filter. A blank <i>Cascading Filter</i> page
will be displayed.</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image085.jpg"></span></p>


<p class=MsoNormal><span style=''>The <i>Database
Connection</i> identifies the currently selected reporting database. </span></p>


<p class=MsoNormal><span style=''>Enter a <i>Name</i>
and <i>Description</i> (optional) for the cascading filter.</span></p>


<p class=MsoNormal><span style=''>Click on the <b>Add</b>
button to create a filter item. If this is the first filter item, the following
dialog will be presented:</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image086.jpg"></span></p>



<p class=MsoNormal><span style=''>If this is an
additional filter item, the following dialog will be presented:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image087.jpg"></span></p>


<p class=MsoNormal><span style=''>The
difference between the two dialogs is the <i>Filter Column</i>. Since there are
no filters applied to the first list of a cascading filter, this option is not
presented for the first cascading filter list.</span></p>


<p class=MsoNormal><span style=''>Repeat the
process of adding filter items until the cascading filter definition is
complete.</span></p>


<p class=MsoNormal><span style=''>Filter items
may be removed by selecting the filter item with the checkboxes and clicking on
the <b>Delete</b> button. The definition of the filter item may be modified by
clicking on the <img border=0  
src="System_Admin_Guide_files/image007.gif"> icon.</span></p>


<p class=MsoNormal><span style=''>Click on the <b>Test
Filter</b> button to exercise the filter items and verify that the lists are
properly populated. A preview page will be displayed similar to the following:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image088.jpg"></span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>The
  cascading filter test will only exercise the filter. It does not accurately
  reflect the presentation of the cascading filter in a report.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>Click on the <b>Save</b>
button to store the cascading filter definition in the metadata database.</span></p>



<p class=MsoNormal><span style=''>As an
example, the following cascading filter definition was the basis for the <b>Test
Filter</b> description above:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image089.jpg"></span></p>


<p class=MsoNormal><span style=''>The first
filter list is based on the  Country  column of the  Customers  data object.
When the cascading filter is presented, the first drop-down list will be
populated with a list of the distinct countries found in the  Customers  data
object. The list is unfiltered. The caption for the list will be  Country , the
friendly name of that column. When the user selects one of the countries, the
data in the  Country  <i>Value Column</i> will be passed to the next filter
item.</span></p>


<p class=MsoNormal><span style=''>The second
filter list is based on the  Region  column of the  Customers  data object.
When this list is presented, the contents will be all of the distinct regions
in the  Customers  table, filtered by the  Country  value selected from the previous
list. The caption for the list will be  Region , the friendly name for that
column. When the user selects a  Region , the data in the  Region  <i>Value Column</i>
will be passed to the next filter item.</span></p>


<p class=MsoNormal><span style=''>The last
filter item is based on the  City  of the  Customers  data object. When this
list is presented, the contents will be a list of  Cities  filtered by the  Region 
value selected from the previous list. When the user selects a  City , the corresponding
 City  value will be used to filter the data in the report.</span></p>


<p class=MsoNormal><span style=''>The  In
Cascade List  option will be presented to the user building the report when the
column that the parameter is based on is the  City  column.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Notes:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.5in;text-indent:-.25in'><span
  style=''>1)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span><span style=''>The <i>Value
  Column </i>and <i>Display Column</i> may be different</span></p>
  <p class=MsoNormal style='margin-left:.5in;text-indent:-.25in'><span
  style=''>2)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span><span style=''>The cascading
  filter may be based on multiple, related data objects</span></p>
  <p class=MsoNormal style='margin-left:.5in;text-indent:-.25in'><span
  style=''>3)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span><span style=''>Only the last
  filter item is used to filter the report. If the last filter <i>Value Column</i>
  was  City  and the user selected  Springfield , all records with a  City  of
   Springfield  will be selected. The previous filter items are not considered.</span></p>
  </td>
 </tr>
</table>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
