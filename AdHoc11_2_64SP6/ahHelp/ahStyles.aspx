<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc263162789">Presentation Styles</a></h2>


<p class=MsoNormal><span style=''>The
Presentation Styles page allows administrators to manage Cascading Style Sheet
(CSS) classes available for use when building reports. Presentation styles
registered on this page are used for highlighting specific report labels and
captions and data points based on a given criteria.</span></p>


<p class=MsoNormal><span style=''>Before
registering a CSS class with the Ad Hoc instance, administrators must create
the class and add it to every style sheet in the <i>_StyleSheets </i>folder.
Adding the class to every style sheet ensures that all classes are available
regardless of the report template selected from the Report Builder.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b><span
  style=''> While creating and managing <i>Presentation
  Styles</i>, the default stylesheet will be used to present the class options.
  The default stylesheet is set in the <i>Configuration / Report Configuration
  / Report Settings</i> page <i>Default Template </i>attribute.</span></p>
  </td>
 </tr>
</table>



<p class=MsoNormal><span style=''>Select <b>Presentation
Styles</b> from the <i>Report Configuration</i> drop-down list<b> </b>to
display the <i>Presentation Styles</i> configuration page.</span></p>


<p class=MsoNormal><b><span style='font-size:10.0pt;'><img
border=0   src="System_Admin_Guide_files/image090.jpg"></span></b></p>



<p class=MsoNormal><span style=''>The <b>Add</b>
button will display an empty <i>Presentation Style dialog.</i></span></p>


<p class=MsoNormal><span style=''>The <b>Delete</b>
button will remove the selected presentation styles. Presentation Styles are
selected by clicking on the applicable checkbox.</span></p>


<p class=MsoNormal><span style=''>The  <img
border=0   src="System_Admin_Guide_files/image025.jpg"> icon
indicates that more than one action can be performed on the presentation style.
Hover the mouse over the  <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon to display the available
actions and click on the appropriate one.</span></p>


<p class=MsoNormal><span style=''>The available
actions for a presentation style are <b>Modify Presentation Style</b> and <b>View
Dependencies</b>.</span></p>



<p class=MsoNormal><i><u><span style=''>Adding
a Presentation Style</span></u></i></p>


<p class=MsoNormal><span style=''>To make a
presentation style available to the end user, click on the <b>Add</b> button.
The following dialog will be presented (magnifying glass used to present the
class list in the image).</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image091.jpg"></span></p>


<p class=MsoNormal><span style=''>Select the
class by clicking on the <img border=0  
src="System_Admin_Guide_files/image092.gif">icon. Enter a Friendly Name and
click on the <b>OK</b> button to store the presentation style reference in the
metadata database.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>Friendly
  names must be unique.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><i><u><span style=''>Modifying
a Presentation Style</span></u></i></p>


<p class=MsoNormal><span style=''>Modifying a
style is necessary when the name of the class changes within the style sheet,
or when administrators want to change what users see from the Report Builder.
Modifying a style only changes its registration status in the Ad Hoc instance -
the name of the class within the style sheet is not altered.</span></p>


<p class=MsoNormal><span style=''>Hover the
mouse over the  <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon for a presentation style and
select the <b>Modify Presentation Style</b> option from the list. Select the<i>
Class</i> by clicking on the <img border=0  
src="System_Admin_Guide_files/image093.gif">icon. Enter a <i>Friendly Name</i>
and click on the <b>OK</b> button to store the revised presentation style
reference in the metadata database.</span></p>


<p class=MsoNormal><i><u><span style=''>View
Dependencies</span></u></i></p>


<p class=MsoNormal><span style=''>Hover the
mouse over the  <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon for a presentation style and
select the <b>View Dependencies</b> option from the list.</span></p>


<p class=MsoNormal><span style=''>A report page
identifying the scope and usage of the presentation style will be presented.
Administrators should exercise this option before modifying or deleting a
presentation style to determine the impact of the change.</span></p>



<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
