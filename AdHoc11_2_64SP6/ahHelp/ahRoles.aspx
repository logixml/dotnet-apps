<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc263162777">Roles</a></h2>


<p class=MsoNormal><span style=''>A user s
roles determine what the user is authorized to do within Ad Hoc. Access to
features within Ad Hoc, databases, objects and columns are all controlled by
the rights afforded to the user through all of their assigned roles.</span></p>


<p class=MsoNormal><span style=''>A <i>role</i><b>
</b>is comprised of one or more packages of rights, called <i>permissions</i>
or <i>permission packages</i>. Roles are associated with organizations and then
assigned to individual users within the organization. </span></p>


<p class=MsoNormal><span style=''>Select <b>Roles</b>
from the <i>User Configuration</i> drop-down list<b> </b>to display the <i>Roles</i>
configuration page.</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image030.jpg"></span></p>


<p class=MsoNormal><span style=''>The <b>Add</b>
button will display an empty <i>Role</i> page.</span></p>


<p class=MsoNormal><span style=''>The <b>Delete</b>
button will remove the selected roles. Roles are selected by clicking on the
applicable checkbox.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Notes</span></b><span
  style=''>:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><a
  name="OLE_LINK16"></a><a name="OLE_LINK15"><span style=''>1.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span></span><span
  style=''>If a user has multiple roles
  assigned, that user will lose the role that the system administrator deletes.
  If a user has only one role assigned and it is removed, that user no longer
  has access to </span></a><span style=''>the
  Ad Hoc instance..</span></p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
  style=''>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span><span style=''>A user may not
  delete a role that is currently assigned to their User account.</span></p>
  </td>
 </tr>
</table>



<p class=MsoNormal><span style=''>The  <img
border=0   src="System_Admin_Guide_files/image025.jpg"> icon
indicates that more than one action can be performed on the role. Hover the
mouse over the  <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon to display the available
actions and click on the appropriate one.</span></p>


<p class=MsoNormal><span style=''>The available
actions on the <i>Roles</i> page are <b>Modify Role, Delete Role</b> and <b>Set
Data Object Access Rights</b>. </span></p>



<p class=MsoNormal><i><u><span style=''>Adding
a Role</span></u></i></p>


<p class=MsoNormal><span style=''>To add a role,
click on the <b>Add</b> button and the <i>Role</i> page will be displayed:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image031.jpg"><img
border=0 width=417 height=406 src="System_Admin_Guide_files/image032.jpg"></span></p>

<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image033.jpg"></span></p>



<p class=MsoNormal><span style=''>The <i>Role</i>
is required and must be unique. The <i>Description</i> of the role is optional.
</span></p>

<p class=MsoNormal><span style=''>Assign <i>Permissions</i>,
<i>Organizations</i>, <i>Databases</i> and <i>Users</i> from the respective <i>Available</i>
list boxes.</span></p>


<p class=MsoNormal><span style=''>Click on the <b>Save</b>
button to store the new Role information. </span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>Every role
  must have at least one organization assigned, typically the  <i>Default</i> 
  organization. If the Ad Hoc instance only has the  Default  organization
  defined, the Available/Assigned Organization control will not be shown.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<p class=MsoNormal><i><u><span style=''>Adding
a Role Using  Save As </span></u></i></p>


<p class=MsoNormal><span style=''>The
administrator can create a role by modifying an existing role and then saving
it with a new name by clicking on the <b>Save As</b> button. The <b>'Save As'</b>
option also copies the data object access right settings from the existing role
to the new role.</span></p>


<h5 align=left style='text-align:left'><i><span style='font-weight:normal'>Modifying
Roles</span></i></h5>


<p class=MsoNormal><span style=''>System
administrators can change the Role name, description, and the assigned
permissions, organizations, databases and users. </span></p>


<p class=MsoNormal><span style=''>To change the
Role information, click the <b>Modify Role</b> action for the role.  The <i>Modify</i>
<i>Role</i> page will be displayed.</span></p>


<p class=MsoNormal><span style=''>Modify the <i>Role</i>,
<i>Description</i> or assignments and click the <b>Save</b> button to store the
information.</span></p>


<p class=MsoNormal><span style=''>To save the
edited information by a new <i>Role </i>name, click on the <b>Save As</b>
button, enter the new name and click on the <b>OK</b> button to save the
information.</span></p>

<p class=MsoHeader>&nbsp;</p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>The <i>Selected
  Role</i> drop-down list may be used to navigate through roles and avoid using
  the <b>Back to Roles</b> button and selecting a role.</span></p>
  </td>
 </tr>
</table>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>The <i>System
  Admin </i>role must have an assigned user named <i>Admin</i></span></p>
  </td>
 </tr>
</table>




<h5 align=left style='text-align:left'><a name="_Ref169668422"><i><span
style='font-weight:normal'>Setting a Role's Data Object Access Rights</span></i></a></h5>


<p class=MsoNormal><span style=''>System
administrators can limit access to data objects for each role created in Ad Hoc.
Access rights can be granted at the column-level or data object-level. Modify
access rights to a data object by specifying access levels for each column of
the object.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>The <b>System
  Admin </b>role always has <b>Full </b>access rights to all data objects and
  columns within each data object.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>The <i>Object
Access Rights</i> page allows the system administrator to specify access rights
to specific data objects.<span style='color:red'> </span>By default, all roles
associated with the current reporting database have <i>Full </i>access to all
objects and columns in the current reporting database.</span></p>


<p class=MsoNormal><span style=''>The three
levels of access for data objects are:</span></p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><span style=''>Full - all
     columns are accessible</span></li>
 <li class=MsoNormal><span style=''>Limited -
     some columns are accessible</span></li>
 <li class=MsoNormal><span style=''>None - no
     columns are accessible</span></li>
</ul>


<p class=MsoNormal><span style=''>The two
levels of access for individual columns are:</span></p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><span style=''>Full -
     column available for reporting</span></li>
 <li class=MsoNormal><span style=''>None - no
     access</span></li>
</ul>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>To change the
access to columns, click on the <b>Set Data Object Access Rights</b> action for
the role. The <i>Object Access Rights</i> page will be displayed for the
selected role:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image034.jpg"></span></p>


<p class=MsoNormal><span style=''>The <i>Database</i>
dropdown list allows the System Administrator to select the pool of objects for
the database.</span></p>


<p class=MsoNormal><span style=''>The <i>Data Object</i><b>
</b>column lists all data objects. The <i>Type</i> column displays the object
type and the <i>Access</i><b> </b>column displays the access type for each data
object.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>The default
  access type for each data object and all columns is <i>Full.</i></span></p>
  </td>
 </tr>
</table>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>Column
  permissions for the <i>System Admin </i>role cannot be modified for any data
  object.</span></p>
  </td>
 </tr>
</table>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>Click on the <img
border=0   src="System_Admin_Guide_files/image007.gif"> icon
to <b>Modify Column Access Rights</b> and display the <b>Column Access Rights</b>
dialog.</span></p>


<p class=MsoNormal><span style='font-size:10.0pt;'><img
border=0   src="System_Admin_Guide_files/image035.jpg"></span></p>



<p class=MsoNormal><span style=''>Using the
checkboxes, select the appropriate columns and click on the <b>Set to Full</b>
or <b>Set to None</b> buttons to change the access for the columns for the
respective role and data object.</span></p>


<p class=MsoNormal><span style=''>Click on the <b>Save</b>
button to store the configuration.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#FFFF99;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><i><span style=''>Hint:</span></i></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i><span style=''>Quickly
  switch between roles by choosing the role from the drop-down menu provided. Select
  one or more checkboxes and click <b>Set to Full </b>or <b>Set to None </b>to
  make fast access changes to objects in the metadata database. Click <b>Copy
  Permissions </b>and then choose a role to quickly duplicate permissions
  between user roles.</span></i></p>
  </td>
 </tr>
</table>


</body>
</html>
