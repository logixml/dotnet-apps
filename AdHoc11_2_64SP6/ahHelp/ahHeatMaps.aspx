<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h3 class="title"></h3>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image161.jpg"><img border=0  
src="Report_Design_Guide_files/image162.jpg"></p>


<p class=MsoNormal>Heat maps are useful for giving business users quick views
of large amounts of data to find trends and anomalies at-a-glance. It would be
very difficult to view and comprehend information about 2,000 items in a pie
chart; a heat map, however, makes this possible. Heat maps can show
relationships among hundreds or thousands of items in hierarchies with
rectangular spaces divided into regions. Each region is divided again to
correspond to each level in the hierarchy. Business users easily interact with
these hierarchical, colorful regions to get more information.</p>


<p class=MsoNormal>Heat maps are especially useful when an organization has
numerous facts to analyze, such as many sales regions, many manufacturing
plants or hundreds of product lines and wants to monitor the complex activities
among those many products, projects or salespeople.</p>


<p class=MsoNormal>Heat maps are comprised of multiple <i>cells </i>that have a
varying size and color. Each cell has a specific label so that users can
determine what each cell represents. The size, color and label for each cell
are determined by values from three different points of data. In order to fully
populate a heat map, select three distinct data columns for the label, cell
size and cell color.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image163.jpg"></p>

<p class=MsoNormal><span style='font-size:10.0pt'>A heat map displaying the
number of orders and revenue received from each product.</span></p>


<p class=MsoNormal>A cell is created for each distinct product name in the <i>ProductName
</i>column. The size of the cell is determined by the corresponding value from
the <i>Revenue </i>column; larger values produce larger cells. The color of the
cell is determined by the value of the <i>Order Count </i>column.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>



<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>Numeric data columns must be selected for the <i>Cell Size</i><b>
  </b>and <i>Cell Color</i><b> </b>columns.</p>
  </td>
 </tr>
</table>


<p class=MsoNormal>When a Heat Map chart is selected for a report, the Report
Builder will create an associated Heat Map Settings tab with the various
attributes that control the display of the heat map.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image164.jpg"></p>


<p class=MsoNormal>The <i>Title</i> attribute value will be displayed
left-justified above the rendered heat map.</p>


<p class=MsoNormal>The <i>Label Column</i> frame contains the attributes
controlling the column to be used as the primary label for each cell of the
heat map and format of the value.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image165.jpg"></p>


<p class=MsoNormal>The <i>Cell Color Column</i> frame contains the attributes
controlling the column to be used to determine the cell color, the format of
the data values, and any aggregation of the data values. Aggregation options
include Sum, Average, Standard Deviation, Count, Count Distinct, Maximum, and
Minimum.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image166.jpg"></p>



<p class=MsoNormal>The <i>Cell Size Column</i> frame contains the attributes
controlling the column to be used to determine the cell size, the format of the
data values, and any aggregation of the data values. Aggregation options
include Sum, Average, Standard Deviation, Count, Count Distinct, Maximum, and
Minimum.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image167.jpg"></p>


<p class=MsoNormal>The <i>Color Slider</i> frame contains the color
distribution range for the heat map cells.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image168.jpg"></p>


<p class=MsoNormal>The <i>Allow Resizing</i> attribute will place drag handles
on the edges of the displayed heat map, allowing the heat map image to be
resized. </p>



<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
