<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h3 class="title"></h3>


<p class=MsoNormal>Most Ad Hoc reports rely on a single data source. All of the
display elements (display tables, crosstabs, and various charts) in the report
would display data from that source. Logically this makes sense because
analytical reports are typically used to make a point, tell a story, or reveal
information about the data.</p>


<p class=MsoNormal>Ad Hoc is initially configured to accept data from a single
data source; however, if the System Administrator discovers that reports need
to reflect data from disparate data sources they can configure Ad Hoc to allow
the end user to identify multiple data sources for a report.</p>


<p class=MsoNormal>If the System Administrator enables the  Multiple Data
Sources  option, the <i>Select Data</i> dialog changes for all users. </p>


<p class=MsoNormal>To create a new data source, click on the <b>Modify Data
Source</b> button. The <i>Select Data</i> dialog is presented.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image231.jpg"></p>


<p class=MsoNormal>The dropdown list of data sources will appear at the bottom
of the dialog along with a <b>Save as New</b> button. Select the data objects
for the new data source from the data objects tree and click on the <i>Save as
New</i> button. Enter a name for the data source in the <i>Save as New </i>dialog
and click on the <b>OK</b> button to confirm it.</p>


<p class=MsoNormal><a name="_Toc297114557"><b>Note: Every data-oriented display
element in the report needs a data source. As they are added to the report
definition, the assumption is that the currently selected data source is to be
used.</b></a></p>


<b><span style='font-size:13.0pt;'><br
clear=all style='page-break-before:always'>
</span></b>

</body>
</html>
