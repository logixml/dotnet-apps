<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h3 class="title"></h3>


<p class=MsoNormal>Dashboards are created by adding and configuring different
panels. A panel is built with one of the following content types:</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><b>Pre-defined reports</b> - Canned reports that provide a
     quick view of a user's most:</li>
 <ul style='margin-top:0in' type=circle>
  <li class=MsoNormal><i>Frequently Viewed Reports</i></li>
  <li class=MsoNormal><i>Recently View Reports</i></li>
 </ul>
 <li class=MsoNormal><b>Favorites</b> - A user specified list of reports
     contained with a folder accessible from the My Personal Reports page.</li>
 <li class=MsoNormal><b>URL - </b>Any valid URL the user has access to.</li>
 <li class=MsoNormal><b>Reports</b> - A user specified report accessible from
     the My Personal Reports or Shared Reports pages.</li>
</ul>


<p class=MsoNormal>Panels may be organized into columns in the dashboard. A
column may contain none to many panels but as a minimum, a dashboard must
contain at least one panel to be saved.</p>


<p class=MsoNormal><b><span style='font-size:10.0pt'><img border=0 
 src="Report_Design_Guide_files/image227.jpg"></span></b><span
style='font-size:10.0pt'>The Dashboard Builder Panel Interface</span></p>


<p class=MsoNormal><span style='font-size:10.0pt'><img border=0 
 src="Report_Design_Guide_files/image228.jpg"></span></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The Dashboard Panel
definition dialog</span></p>


<p class=MsoNormal><b>To add a dashboard panel:</b></p>


<p class=MsoNormal>An unlimited number of panels can be added to a dashboard.
The panels can be configured to appear in the dashboard layout using panel
settings and the panel drag feature controls provided.</p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>Click the <b>Add </b>button to view the Panel Settings
     panel.</li>
 <li class=MsoNormal>Type a <i>Name</i> and <i>Description</i> for the panel in
     the fields provided.</li>
 <li class=MsoNormal>Change the <i>Initial Column</i> value based on where the
     panel should appear in the dashboard. The default value is 1.</li>
 <li class=MsoNormal>Change the <i>Initial Display</i> setting based on: </li>
</ol>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span>Yes - display the panel at initial viewing of the dashboard
(default).</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span>No - do not display the panel at initial viewing of the
dashboard.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>If <i>Initial Display</i> = <i>No,</i> then the user has
  the option to add the panel via the <b>Change Dashboard</b> option within the
  dashboard viewer.</p>
  </td>
 </tr>
</table>


<ol style='margin-top:0in' start=5 type=1>
 <li class=MsoNormal>Adjust the panel <i>Height</i>, Minimum Height and Minimum
     Width if necessary. </li>
 <li class=MsoNormal>Select the <i>Panel Content</i> type (default is <i>Report</i>).</li>
 <li class=MsoNormal>Specify the report, folder or URL in the field provided.</li>
 <ol style='margin-top:0in' start=1 type=a>
  <li class=MsoNormal>If <i>Pre-defined reports</i>, then select one of the
      listed reports and click <b>OK</b>.</li>
  <li class=MsoNormal>If <i>Favorites</i>, then locate and select a <i>Personal
      Reports</i> folder and click <b>OK</b>.</li>
  <li class=MsoNormal>If <i>URL</i>, then specify a URL address. <span
      style='color:black'>Click <b>Test URL</b> to confirm that the URL can be
      viewed.</span></li>
  <li class=MsoNormal>If <i>Reports</i>, then locate and select a report
      located in the <i>Personal Reports</i> or <i>Shared Reports</i>.</li>
 </ol>
 <li class=MsoNormal>Click <b>Save Panel </b>and return to the Dashboard
     Builder interface.</li>
 <li class=MsoNormal>If the dashboard had not been previously saved, then type
     a name for the dashboard in the field provided.</li>
 <li class=MsoNormal>Rearrange panels as necessary with the up/down arrows</li>
 <li class=MsoNormal>Click <b>Save </b>to store the dashboard.</li>
</ol>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#FFFF99;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial>Hint:</p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i>The dashboard may be previewed at any time by clicking <b>Preview
  Dashboard,</b> as long as the Panel Settings panel is not being viewed.</i></p>
  </td>
 </tr>
</table>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><b>To modify a dashboard panel:</b></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>Hover the mouse over the <img border=0  
     src="Report_Design_Guide_files/image229.jpg"> icon for the report and
     select <i>Modify Dashboard Panel</i> from the dropdown list. </li>
 <li class=MsoNormal>Modify the panel as desired.</li>
 <li class=MsoNormal>Click <b>Save Panel </b>and return to the Dashboard
     Builder interface.</li>
 <li class=MsoNormal>If the dashboard had not been previously saved, then type
     a name for the dashboard in the field provided.</li>
 <li class=MsoNormal>Click <b>Save</b> to store the dashboard definition<b>.</b></li>
</ol>

<p class=MsoNormal> </p>

<p class=MsoNormal><b>To delete one or more dashboard panels:</b></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>Select the desired panel(s) by enabling its respective
     checkbox(es).</li>
 <li class=MsoNormal>Click the <b>Delete </b>button.</li>
 <li class=MsoNormal>Click <b>OK </b>to confirm the removal.</li>
 <li class=MsoNormal>If the dashboard had not been previously saved, then type
     a name for the dashboard in the field provided.</li>
 <li class=MsoNormal>Click <b>Save</b> to store the dashboard definition<b>.</b></li>
</ol>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial>Note:</p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i>At least one dashboard panel must exist in order to
  save the dashboard.</i></p>
  </td>
 </tr>
</table>


<b><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></b>


<p class=MsoNormal><b>To modify a Dashboard's Settings:</b></p>


<p class=MsoNormal>Click on the <img border=0  
src="Report_Design_Guide_files/image225.jpg"> icon for the Dashboard Settings
to expand the page and allow general dashboard appearance to be customized. </p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image230.jpg"></p>


<p class=MsoNormal>The <i>Dashboard Settings</i> panel, offers the ability to
configure a dashboard's general appearance and provide an optional description.
A panel's appearance is controlled by the selected Dashboard <i>Style</i>,
which gives the dashboard a specific &quot;look and feel&quot;. The available
dashboard styles are:</p>


</div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection17>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>(None)</li>
 <li class=MsoNormal>Classic</li>
 <li class=MsoNormal>Gray</li>
 <li class=MsoNormal>Harmony</li>
 <li class=MsoNormal>LemonLime</li>
 <li class=MsoNormal>Light</li>
 <li class=MsoNormal>Clarity</li>
 <li class=MsoNormal style='margin-right:-19.5pt'>ProfessionalBlue</li>
 <li class=MsoNormal style='margin-right:-28.5pt'>ProfessionalGreen</li>
 <li class=MsoNormal>Silver</li>
 <li class=MsoNormal>SimpleBlue</li>
 <li class=MsoNormal>Transit</li>
 <li class=MsoNormal>Mocha</li>
 <li class=MsoNormal>Nature</li>
 <li class=MsoNormal>Ocean</li>
 <li class=MsoNormal>Professional</li>
 <li class=MsoNormal>Black Pearl</li>
 <li class=MsoNormal>RedWine</li>
 <li class=MsoNormal>Technical</li>
 <li class=MsoNormal>Tropical</li>
</ul>

</div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection18>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Notes:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>The system
  administrator may add additional <i>Dashboard Styles</i> to this list.</p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>The <i>Dashboard</i>
  <i>Style</i> will not control the appearance of any of the reports viewed in
  a panel.</p>
  </td>
 </tr>
</table>


<p class=MsoNormal>As necessary, specify or modify <i>the Description,
Dashboard Expiration Date</i> and <i>Header</i> content.</p>


<p class=MsoNormal>The <i>Dashboard Body</i> attributes control the initial
functional behavior of the dashboard. </p>


<p class=MsoNormal>The <i>Free-Form Layout</i> option presents the panels in a
non-columnar layout and allows the user to reposition the panels anywhere on
the page.</p>


<p class=MsoNormal>The <i>Dashboard Adjustable</i> attribute determines whether
panel positions may be adjusted by the end user.</p>


<p class=MsoNormal>The <i>Dashboard Tabs</i> attribute determines whether the
end user can rearrange the panels into different browser tabs.</p>


<p class=MsoNormal>The <i>Dashboard Columns</i> attribute sets the lattice
framework for the panels. This attribute is disabled if the Free-Form Layout
option is enabled.</p>


<p class=MsoNormal>Click the <img border=0  
src="Report_Design_Guide_files/image226.jpg"> icon for the Dashboard Settings
collapse/hide the Dashboard Settings panel.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>The description appears under the dashboard's name in the Reports
  list in the <i>Personal Reports</i> and <i>Shared Reports</i> areas.</p>
  </td>
 </tr>
</table>

<span style='font-size:18.0pt;'><br clear=all
style='page-break-before:always'>
</span>



</body>
</html>
