<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PagingControlCustomizable.ascx.vb" Inherits="LogiAdHoc.PagingControlCustomizable" %>
<div id="pager" runat="server">
    <table cellpadding="0" cellspacing="0" id="pagerTable" runat="server">
        <tr>
            <td class="pagerBtn">
                <asp:LinkButton ID="lnkFrst" runat="server" Text="First" OnClientClick="javascript:noMessage=true;" />
                <asp:LinkButton ID="lnkPrev" runat="server" Text="Previous" OnClientClick="javascript:noMessage=true;" />
            </td>
            <td id="count">
                <asp:Panel ID="pnlCount" runat="server">
                    <asp:Label ID="lblPage" runat="server" Text="Page" AssociatedControlID="TxtPageNum" meta:resourcekey="lblPageResource1" />
                    <asp:TextBox ID="TxtPageNum" runat="server" Width="32px" AutoPostBack="true"></asp:TextBox>
                    <asp:Label ID="lblOf" runat="server" Text="of" meta:resourcekey="lblOfResource1" />
                    <asp:Label ID="LblPageCount" runat="server" Width="16px"></asp:Label>
                </asp:Panel>
            </td>
            <td class="pagerBtn">
                <asp:LinkButton ID="lnkNext" runat="server" Text="Next" OnClientClick="javascript:noMessage=true;" />
                <asp:LinkButton ID="lnkLast" runat="server" Text="Last" OnClientClick="javascript:noMessage=true;" />
            </td>
        </tr>
    </table>
</div>
