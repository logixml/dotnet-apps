<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="MobileSearch.ascx.vb" Inherits="LogiAdHoc.MobileSearch" %>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch" CssClass="formSearch">

            <asp:TextBox ID="SearchText" runat="server" CssClass="searchText" meta:resourcekey="SearchTextResource1"></asp:TextBox>
            <asp:ImageButton  ID="btnSearch" ImageUrl="~/ahImages/iconFind.gif" runat="server" OnClick="btnSearch_Click" CausesValidation="false" />
            <asp:ImageButton  ID="btnClear" ImageUrl="~/ahImages/iClearMobile.png" runat="server" OnClick="btnClear_Click" CausesValidation="false" />
            
            <asp:Panel ID="pnlMessage" runat="server">
                <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2" Text="No records found."></asp:Localize>
            </asp:Panel>

</asp:Panel>
