<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PasswordControl.ascx.vb" Inherits="LogiAdHoc.ahControls_PasswordControl" %>

<asp:Panel ID="PasswordPanel" runat="server" meta:resourcekey="PasswordPanelResource1">

    <div id="divPasswordRestrictions" runat="server">
        <asp:Literal ID="lblPasswordRestrictionsInfo" runat="server" meta:resourcekey="lblPasswordRestrictionsInfoResource1"
            Text="New password must confirm to the following rules:" >
        </asp:Literal>
        <asp:BulletedList ID="lstPwdRestrictions" runat="server" >
        </asp:BulletedList>
    </div>
    
    <table>
        <tr>
            <td>
                <asp:Label ID="lblOldPassword" runat="server" AssociatedControlID="oldpassword" Text="Old Password:" meta:resourcekey="lblOldPasswordResource1" />
            </td>
            <td align="left">
                <input id="oldpassword" type="password" maxlength="100" runat="server"><asp:RequiredFieldValidator ID="rtvOldPassword" runat="server" ValidationGroup="Password"
                    ErrorMessage="Please enter your old password." ControlToValidate="oldpassword"
                    EnableClientScript="False" meta:resourcekey="rtvOldPasswordResource1" Text="*"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvOldPassword" runat="server" ValidationGroup="Password"
                    ErrorMessage="The old password is incorrect." ControlToValidate="oldpassword"
                    EnableClientScript="False" OnServerValidate="ValidatePassword" meta:resourcekey="cvOldPasswordResource1" Text="*"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblNewPassword" runat="server" AssociatedControlID="newpassword" Text="New Password:" meta:resourcekey="lblNewPasswordResource1" />
            </td>
            <td align="left">
                <input id="newpassword" type="password" maxlength="100" runat="server">
                <asp:RequiredFieldValidator ID="rtvNewPassword" runat="server" ValidationGroup="Password"
                    ErrorMessage="Please enter a new password." ControlToValidate="newpassword" meta:resourcekey="rtvNewPasswordResource1" Text="*"></asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvNewPassword" runat="server" ValidationGroup="Password"
                    ErrorMessage="The password cannot contain special characters." ControlToValidate="newpassword"
                    EnableClientScript="False" OnServerValidate="ValidateNewPassword" meta:resourcekey="cvNewPasswordResource1" Text="*"></asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblRetypePassword" runat="server" AssociatedControlID="retypepassword" Text="Retype Password:" meta:resourcekey="lblRetypePasswordResource1" />
            </td>
            <td align="left">
                <input id="retypepassword" type="password" maxlength="100" runat="server"><asp:RequiredFieldValidator ID="rtvRetypePassword" runat="server" ValidationGroup="Password"
                    ErrorMessage="Confirm your new password by re-entering it." ControlToValidate="retypepassword"
                    EnableClientScript="False" meta:resourcekey="rtvRetypePasswordResource1" Text="*"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="ctfPassword" runat="server" ValidationGroup="Password"
                    ErrorMessage="The two password fields must match exactly." ControlToValidate="newpassword"
                    ControlToCompare="retypepassword" EnableClientScript="False" meta:resourcekey="ctfPasswordResource1" Text="*"></asp:CompareValidator>
            </td>
        </tr>
    </table>
    <asp:CheckBox ID="chkForceChangePassword" runat="server" meta:resourcekey="chkForceChangePasswordResource1"
        Text="User must change password at next logon." />
    <br />
    <!-- Buttons -->
    <table>
        <tr>
            <td colspan="2">
                <%--<AdHoc:Button ID="ahFVOk" runat="server" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" ValidationGroup="Password" OnClick="SavePassword_OnClick" />--%>
                <AdHoc:LogiButton ID="SavePassword" OnClick="SavePassword_OnClick" runat="server"
                    ValidationGroup="Password" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" />
                <AdHoc:LogiButton ID="CancelPassword" OnClick="CancelPassword_OnClick" runat="server"
                    ValidationGroup="Password" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" meta:resourcekey="CancelPasswordResource1" />
            </td>
        </tr>
    </table>
    <asp:ValidationSummary ID="vsummary1" ValidationGroup="Password" runat="server" meta:resourcekey="vsummary1Resource1" />
</asp:Panel>
