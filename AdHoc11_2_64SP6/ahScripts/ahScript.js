﻿// JScript File

function setDivSize() {
    /* 
        divMain height must be:
        full viewable area height - menu height - submenu height - navtrail height - divButtons height - divLegend height
    */
    
    var em = document.getElementById("menu");
    var m = 0
    if (em != null) {
        m = em.offsetHeight; 
    }
    
    var es = document.getElementById("submenu");
    var s = 0
    if (es != null) {
        s = es.offsetHeight; 
    }
    
    var en = document.getElementById("navtrail");
    var n = 0
    if (en != null) {
        n = en.offsetHeight; 
    }
    
    var eb = document.getElementById("divButtons");
    var b = 0
    if (eb != null) {
        b = eb.offsetHeight; 
    }
    
    var el = document.getElementById("divLegend")
    var l = 0
    if (el != null) {
        l = el.offsetHeight; 
    }
    
    var eh = document.getElementById("divHelp")
    var h = 0
    if (eh != null) {
        h = eh.offsetHeight; 
    }
    
    var myHeight = 0;
    if( typeof( window.innerWidth ) == 'number' ) {
        //Non-IE
        myHeight = window.innerHeight;
    } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
        //IE 6+ in 'standards compliant mode'
        myHeight = document.documentElement.clientHeight;
    } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
        //IE 4 compatible
        myHeight = document.body.clientHeight;
    }

    var d = myHeight - m - s - n - b - l - h - 60;
    
    if (d < 0) { d = 0; }
    
    document.getElementById("divMain").style.height = d;    
}
