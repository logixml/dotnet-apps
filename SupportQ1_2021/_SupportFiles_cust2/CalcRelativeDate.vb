Public Function MakeInt(InValue as String) as String
on error goto ErrorInt
	if IsNumeric(InValue)
	  Return CInt(InValue)
	else
ErrorInt:
	  Return CInt(0)
	End If
End Function


Public Function CalcRelativeDate(DefaultDateSpec as String) as String
Dim ReturnDate as Date
  ReturnDate = now()
  Return CStr(Year(ReturnDate)) + "-" + CStr(Month(ReturnDate)) + "-" + CStr(Day(ReturnDate ))
End Function

Public Function IsValidDate(DateStr as String) as boolean
  return IsDate(DateStr)
End Function