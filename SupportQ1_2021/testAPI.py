from flask import Flask, jsonify, request, redirect
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

class Test(Resource):
    def post(self):
        return {"response": [
            {"test": "TESTING"},
            {"test": "\u2003"},
            {"test": "\u2211"},
            {"test": "\ud83d"},
            {"test": "\udc0d"},
            {"test": "\u0018"},# THIS ONE
            {"test": "\r"},
            {"test": "\u00c2"},
            {"test": "\u00a3"},
            #{"test": "\u2003\u2211\ud83d\udc0d\u0018T\r\u00c2\u00a3S'T(|]><[\\xG %s %% \"+N\\\\&\\M\u00c8.?\nTAB\t12}34{56\ud835\udff389,:;#END"},
        ]}

api.add_resource(Test, '/json/')#

if __name__ == '__main__':
    app.run(debug=True)