function padStart(strIn, strPad, ttlLen) {
	var strLen = strIn.length;
	if (strLen >= ttlLen) return strIn;
	var padding = Array(ttlLen-strLen+1).join(strPad);
	return padding + strIn;	
}