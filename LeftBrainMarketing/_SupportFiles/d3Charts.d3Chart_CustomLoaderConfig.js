// loader settings
var optsLoader = {
    lines: 12, // The number of lines to draw
	length: 0, // The length of each line
	width: 4.5, // The line thickness
	radius: 11, // The radius of the inner circle
	scale: 1.10, // Scales overall size of the spinner
	corners: 1, // Corner roundness (0..1)
	color: '#8a8a8a', // CSS color or array of colors
	fadeColor: 'transparent', // CSS color or array of colors
	opacity: 0.05, // Opacity of the lines
	rotate: 0, // The rotation offset
	direction: 1, // 1: clockwise, -1: counterclockwise
	speed: 1, // Rounds per second
	trail: 60, // Afterglow percentage
	fps: 20, // Frames per second when using setTimeout() as a fallback in IE 9
	zIndex: 2e9, // The z-index (defaults to 2000000000)
	className: 'spinner', // The CSS class to assign to the spinner
	top: '50%', // Top position relative to parent
	left: '50%', // Left position relative to parent
	position: 'absolute' // Element positioning
};



		
/*
function createPDFAllHospKPIs()
{
	createPDFOverallRatingOverallRating();
	createPDFTestRate();
	createPDFPositivityRate();
	createPDFIdRate();
	createPDFTurnaround();
	createPDFTestRateSplit();
}

function createWordHospExportKPIs()
{
 
	clickOverallRate();
	clickTestRate();
	clickPositivityRate();
	clickIdRate();
	clickTurnaround();
	clickTestRateSplit();
}


function createPDFAllLabKPIs()
{
	createPDFPositivityRate();
	createPDFTestRateSplit();
}

function createWordLabExportKPIs()
{ 
	clickPositivityRate();
	clickTestRateSplit();
}

*/
//Export functionality
function createLegendCircle(color, pdfStr ) {
	
	var imgDataCreate = new Array();
	var canvasCreate = document.createElement('canvas'); // Create a Canvas element.
	var ctxCreate = canvasCreate.getContext("2d");
	var imageCreate;
	canvasCreate.width = '25';
	canvasCreate.height = '25';
	ctxCreate.beginPath();
	ctxCreate.arc(7, 17, 7, 0, 2.3 * Math.PI);
	ctxCreate.fillStyle = color;
	ctxCreate.fill();
	
	if( pdfStr === "pdf") {
		imageCreate = canvasCreate.toDataURL("image/png");
		return imageCreate;
	}
	imageCreate = canvasCreate.toDataURL("image/png").replace("image/png", "image/octet-stream");  // here is the most important part because if you dont replace you will get a DOM 18 exception.
	imgDataCreate.push(imageCreate);
	
	return imgDataCreate[0];
}


//export to word
$.fn.wordExport = function(fileName) {
	fileName = typeof fileName !== 'undefined' ? fileName : "Word-Export-Report";
	var static = {
		mhtml: {
			top: "Mime-Version: 1.0\nContent-Base: " + location.href + "\nContent-Type: Multipart/related; boundary=\"NEXT.ITEM-BOUNDARY\";type=\"text/html\"\n\n--NEXT.ITEM-BOUNDARY\nContent-Type: text/html; charset=\"utf-8\"\nContent-Location: " + location.href + "\n\n<!DOCTYPE html>\n<html>\n_html_</html>",
			head: "<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n<style>\n_styles_\n</style>\n</head>\n",
			body: "<body>_body_</body>"
		}
	};
	var options = {
		maxWidth: 624
	};
	// Clone selected element before manipulating it
	var markup = $(this).clone();
	
	// Remove hidden elements from the output
	markup.each(function() {
		var self = $(this);
		if (self.is(':hidden'))
			self.remove();
	});
	
	// Embed all images using Data URLs
	var images = Array();
	var img = markup.find('img');
	for (var i = 0; i < img.length; i++) {
		// Calculate dimensions of output image
		var w =img[i].width; //Math.min(img[i].width, options.maxWidth);
		var h = img[i].height;//210;//img[i].height * (w / img[i].width);
		// Create canvas for converting image to data URL
		var canvas = document.createElement("CANVAS");
		canvas.width = w;
		canvas.height = h;
		// Draw image to canvas
		var context = canvas.getContext('2d');
		context.drawImage(img[i], 0, 0, w, h);
		// Get data URL encoding of image
		var uri = canvas.toDataURL("image/png");
		$(img[i]).attr("src", img[i].src);
		img[i].width = w;
		img[i].height = h;
		// Save encoded image to array
		images[i] = {
			type: uri.substring(uri.indexOf(":") + 1, uri.indexOf(";")),
			encoding: uri.substring(uri.indexOf(";") + 1, uri.indexOf(",")),
			location: $(img[i]).attr("src"),
			data: uri.substring(uri.indexOf(",") + 1)
		};
	}
	
	// Prepare bottom of mhtml file with image data
	var mhtmlBottom = "\n";
	for (var i = 0; i < images.length; i++) {
		mhtmlBottom += "--NEXT.ITEM-BOUNDARY\n";
		mhtmlBottom += "Content-Location: " + images[i].location + "\n";
		mhtmlBottom += "Content-Type: " + images[i].type + "\n";
		mhtmlBottom += "Content-Transfer-Encoding: " + images[i].encoding + "\n\n";
		mhtmlBottom += images[i].data + "\n\n";
	}
	mhtmlBottom += "--NEXT.ITEM-BOUNDARY--";
	
	//TODO: load css from included stylesheet
	var styles = "";
	
	// Aggregate parts of the file together
	var fileContent = static.mhtml.top.replace("_html_", static.mhtml.head.replace("_styles_", styles) + static.mhtml.body.replace("_body_", markup.html())) + mhtmlBottom;
	
	// Create a Blob with the file contents
	var blob = new Blob([fileContent], {
		type: "application/msword;charset=utf-8"
	});
	saveAs(blob, fileName + ".doc");
};

