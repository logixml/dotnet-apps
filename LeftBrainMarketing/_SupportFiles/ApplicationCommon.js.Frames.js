function jsRefresh(ele, report, refresh) {
	console.log(report);
	var id = ele.id;
	console.log(id);
	if (refresh == "self") {
		console.log(refresh);
		var frameid = $(ele).closest('.frame').find('.framecontent').attr('id');
		console.log("frame: " + frameid);
	}
	else if (refresh == "parent") {
		console.log(refresh);
		var frameid = $(ele).closest('.frame').parentsUntil('.frame').closest('.frame').attr('id');
		console.log(frameid);
	}
	else if (refresh == "top") {
		console.log(refresh);
		var frameid = $(ele).parents('.frame').closest('.frame').attr('id');
		console.log(frameid);
	}

	rdAjaxRequestWithFormVars('rdAjaxCommand=RefreshElement&rdRefreshElementID='+ frameid +'&rdReport=' + report,'false','',true,null,null,null,true);

}
function isEmpty(val){
    return (val === undefined || val == null || val.length <= 0);
}

/*
$('#Frame_@Shared.UniqueID~ .expand').click(function() {
	var currentframe = $(this).closest('.frame');
	ExpandContent(currentframe,'');
});

*/
function ExpandContent(currentframe,expandcontentvalue) {

	$('.clonedframe').remove();
	var newframe = currentframe.clone().addClass('clonedframe');
	var topSelected = currentframe.offset().top - $(window).scrollTop() - 5,
		leftSelected = currentframe.offset().left  - 5,
		widthSelected = currentframe.width() + 5,
		heightSelected = currentframe.height() + 5,
		windowWidth = $(window).width(),
		windowHeight = $(window).height(),
		newWidth = '96vw',
		newHeight = windowHeight * .96,
		thisparent = currentframe.parent(),
		frameheaderheight = $(currentframe).find('.frameheader').height(),
		framecontentheight = $(currentframe).find('.ExpandKeepContent-True').height(),
		framefooterheight = $(currentframe).find('.framefooter').height(),
		newcontentheight = newHeight - frameheaderheight - framefooterheight - framecontentheight, 
		ScrollTop = $(window).scrollTop(),
		kpiheight = $(currentframe).find('.kpi').height();
	
	$(newframe).css({position: 'fixed', top: topSelected, left: leftSelected, width: widthSelected, height: heightSelected, 'z-index': '99999', 'overflow-y': 'hidden','overflow-x': 'hidden',  'font-size': '12px'});
	$(newframe).find('#colResize .expand').addClass('contract').removeClass('expand')
	$(newframe).find('#colResize').removeClass('ThemeHidden');
//	$(newframe).find('#colFrameHeaderContent.UniqueID~').hide();
	$(newframe).find('#colFrameHeaderContent').hide();
	$(newframe).appendTo("body");			
	$('#divOverlay').addClass('active');
	$('body').css({'overflow': 'hidden'});
	$(newframe).find('#expand').hide();
	$(newframe).find('.kpi').css({'position': 'relative', 'height': kpiheight});
	$(newframe).animate({top: '2vh', left: '2vw', width: '96vw',  height: newHeight}, 500 );
		
	$(newframe).animate({height: '96vh' });
	var oldcontentheight = $(newframe).find('.framecontent').height();
	
	setTimeout(function(){ 
		$('.clonedframe .ExpandKeepContent-False *:not(.kpi)').fadeOut();
	}, 200);
	
	setTimeout (function(){
		var expandcontentiframe = $(newframe).find('.expandcontent iframe');
		if (expandcontentvalue != '') {
			$(newframe).find('.FrameTitle').text($(newframe).find('.FrameTitle').text() + ' - ' +expandcontentvalue);
		}
		$(expandcontentiframe).attr('src', $(expandcontentiframe).attr('data-hiddensource') + '&ExpandContentValue='+expandcontentvalue);
	}, 200);
	
	setTimeout(function(){ 
		$(newframe).find('.expandcontent').css({'position': 'relative'});

		$(newframe).find('.expandcontent').fadeIn(1000);
		var contentheight = $(newframe).find('.framecontent').height() - oldcontentheight;
		$(newframe).find('.framecontent').css({height: '100%'});	
	}, 200);
	
	$(newframe).find('.contract').click(function() {
		$(newframe).find('.expandcontent').fadeOut(200);
		setTimeout(function(){ 		
			$('#divOverlay').removeClass('active');
			$('body').css({'overflow': 'auto','padding-right': '0px'});
			$(window).scrollTop(ScrollTop);
			$(newframe).find('*').not('#KPI_Icon').css({'overflow': 'hidden'});
			$(newframe).animate({ top: topSelected, left: leftSelected, width: widthSelected, height: heightSelected }, 500 );
			$(newframe).fadeOut(200);

			//$(newframe).remove();
		}, 300);
	});

}
