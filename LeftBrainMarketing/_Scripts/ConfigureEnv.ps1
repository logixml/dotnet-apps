﻿param (
    [string]$environment
)

$xdoc = new-object System.Xml.XmlDocument
$file = resolve-path("..\_Definitions\_Settings.lgx")
$xdoc.load($file)

if($environment -eq "DEV")
{

    $xdoc.SelectSingleNode("//Path").SetAttribute('AppPath', 'http://10.0.1.10/Pulse')
	$xdoc.SelectSingleNode("//General").SetAttribute('rdDebuggerStyle', 'DebuggerLinks')

    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServer', '10.0.1.10')
    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServerDatabase', 'Pulse_Dev')
    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServerPassword', 'leftbrain99')
    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServerUser', 'LogiDev')

    $xdoc.SelectSingleNode("//Security").SetAttribute('AuthenticationClientAddresses', '10.0.1.10')
    $xdoc.SelectSingleNode("//Security").SetAttribute('LogonFailPage', 'http://10.0.1.10/AZIMFINZI.Dev/LeftBrain.Security/')
    $xdoc.SelectSingleNode("//Security").SetAttribute('SecurityEnabled', 'False')
   
    $xdoc.SelectSingleNode("//Constants").SetAttribute('cRunMode', 'DEV')
    $xdoc.SelectSingleNode("//Constants").SetAttribute('useLoginURL', 'http://10.0.1.10/Pulse')
    $xdoc.SelectSingleNode("//Constants").SetAttribute('useUserManagement', 'http://10.0.1.10/Pulse/LeftBrain.Security/Account/ManageUsers.aspx')
    $xdoc.SelectSingleNode("//Constants").SetAttribute('useUserProfileMgmt', 'http://10.0.1.10/Pulse/LeftBrain.Security/Account/Manage')
    $xdoc.SelectSingleNode("//Constants").SetAttribute('UserManagementHelpPage', 'http://10.0.1.10/Pulse/TechnicalNotes')
	
    Write-Output $xdoc.OuterXml
    $xdoc.save($file)
}

ElseIf($environment -eq "DEVLOCAL")
{

    $xdoc.SelectSingleNode("//Path").SetAttribute('AppPath', 'http://localhost/Pulse')
	$xdoc.SelectSingleNode("//General").SetAttribute('rdDebuggerStyle', 'DebuggerLinks')

    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServer', '10.0.1.10')
    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServerDatabase', 'Pulse_Dev')
    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServerPassword', 'leftbrain99')
    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServerUser', 'LogiDev')

    $xdoc.SelectSingleNode("//Security").SetAttribute('AuthenticationClientAddresses', '10.0.1.10')
    $xdoc.SelectSingleNode("//Security").SetAttribute('LogonFailPage', 'http://localhost/AZIMFINZI.Dev/LeftBrain.Security/')
    $xdoc.SelectSingleNode("//Security").SetAttribute('SecurityEnabled', 'False')
   
    $xdoc.SelectSingleNode("//Constants").SetAttribute('cRunMode', 'DEV')
    $xdoc.SelectSingleNode("//Constants").SetAttribute('useLoginURL', 'http://localhost/Pulse')
    $xdoc.SelectSingleNode("//Constants").SetAttribute('useUserManagement', 'http://localhost/Pulse/LeftBrain.Security/Account/ManageUsers.aspx')
    $xdoc.SelectSingleNode("//Constants").SetAttribute('useUserProfileMgmt', 'http://localhost/Pulse/LeftBrain.Security/Account/Manage')
    $xdoc.SelectSingleNode("//Constants").SetAttribute('UserManagementHelpPage', 'http://localhost/Pulse/TechnicalNotes')
	
    Write-Output $xdoc.OuterXml
    $xdoc.save($file)
}
ElseIf($environment -eq "PROD")
{

    $xdoc.SelectSingleNode("//Path").SetAttribute('AppPath', 'http://pulsepd.leftbrain.ca')
	$xdoc.SelectSingleNode("//General").SetAttribute('rdDebuggerStyle', 'NoDetail')
    $xdoc.SelectSingleNode("//General").SetAttribute('LicenseFileLocation', 'c:\LogiLicence')
	
    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServer', 'SRV-SQL16')
    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServerDatabase', 'Pulse.Prod')
    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServerUser', 'LogiDev')
    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServerPassword', 'leftbrain99')

    $xdoc.SelectSingleNode("//Security").SetAttribute('AuthenticationClientAddresses', '192.168.11.65,99.228.152.179,216.151.184.150')
    $xdoc.SelectSingleNode("//Security").SetAttribute('LogonFailPage', 'http://pulse.leftbrain.ca/Account/Login.aspx?logout=1/')
    $xdoc.SelectSingleNode("//Security").SetAttribute('SecurityEnabled', 'True')
	$xdoc.SelectSingleNode("//Security").SetAttribute('SecureKeySessionless', 'True')

    $xdoc.SelectSingleNode("//Constants").SetAttribute('cRunMode', 'PROD')
    $xdoc.SelectSingleNode("//Constants").SetAttribute('userLoginURL', 'http://pulse.leftbrain.ca/Account/Login.aspx?logout=1')
    $xdoc.SelectSingleNode("//Constants").SetAttribute('userManagement', 'http://pulsepd.leftbrain.ca/LeftBrain.Security/Account/ManageUsers.aspx')
    $xdoc.SelectSingleNode("//Constants").SetAttribute('userProfileMgmt', 'http://pulsepd.leftbrain.ca/LeftBrain.Security/Account/Manage')

	
	
    Write-Output $xdoc.OuterXml
    $xdoc.save($file)
}


ElseIf($environment -eq "UAT")
{

    $xdoc.SelectSingleNode("//Path").SetAttribute('AppPath', 'http://pulsepduat.leftbrain.ca')
	$xdoc.SelectSingleNode("//General").SetAttribute('rdDebuggerStyle', 'NoDetail')
    $xdoc.SelectSingleNode("//General").SetAttribute('LicenseFileLocation', 'c:\LogiLicence')
	
    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServer', 'SRV-SQL16')
    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServerDatabase', 'Pulse.UAT')
    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServerUser', 'LogiDev')
    $xdoc.SelectSingleNode("//Connection[@ID='ConnPulseDB']").SetAttribute('SqlServerPassword', 'leftbrain99')

    $xdoc.SelectSingleNode("//Security").SetAttribute('AuthenticationClientAddresses', '192.168.11.65,99.228.152.179,216.151.184.150')
    $xdoc.SelectSingleNode("//Security").SetAttribute('LogonFailPage', 'http://pulseuat.leftbrain.ca/Account/Login.aspx?logout=1/')
    $xdoc.SelectSingleNode("//Security").SetAttribute('SecurityEnabled', 'True')
	$xdoc.SelectSingleNode("//Security").SetAttribute('SecureKeySessionless', 'True')

    $xdoc.SelectSingleNode("//Constants").SetAttribute('cRunMode', 'UAT')
    $xdoc.SelectSingleNode("//Constants").SetAttribute('userLoginURL', 'http://pulseuat.leftbrain.ca/Account/Login.aspx?logout=1')
    $xdoc.SelectSingleNode("//Constants").SetAttribute('userManagement', 'http://pulsepduat.leftbrain.ca/LeftBrain.Security/Account/ManageUsers.aspx')
    $xdoc.SelectSingleNode("//Constants").SetAttribute('userProfileMgmt', 'http://pulsepduat.leftbrain.ca/LeftBrain.Security/Account/Manage')

	
	
    Write-Output $xdoc.OuterXml
    $xdoc.save($file)
}