// *******************************************************
// START: THIS PART REMAINS THE SAME, DON'T MODIFY
// *******************************************************
// Create the main myMSALObj instance
// configuration parameters are located at authConfig.js
const myMSALObj = new msal.PublicClientApplication(msalConfig);     // Instantiate new instance of myMSALObj

let accessToken;
let username = "";

console.log("graphapi authRedirect.js Setting handleResponse as the function to handle responses");
myMSALObj.handleRedirectPromise()
    .then(handleResponse)                   // When an Async myMSALObj Promise is filled, handleResponse (in authRedirect.js) processes it
    .catch(err => {console.error(err);})
    ;

function handleResponse(resp) {                         // Handle Async Promises for all myMSALObj Promises
    console.log("graphapi authRedirect.js handleResponse resp");
    console.log("graphapi authRedirect.js Received a Promise response from myMSALObj.  Response was:")
    console.log(resp);

    if (resp !== null) {                    // We have received a response returned with the Promise
        username = resp.account.username;   // Capture the username for later reference
        console.log("graphapi username received in handleResponse: " + username);
        console.log("graphapi resp.account passed to loginCallbackGraph in Index.html: " + resp.account);
        loginCallbackGraph(resp.account);        // Already logged in, so call loginCallBack in Index.html with account info
    } else {
        /**
         * See here for more info on account retrieval: 
         * https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-common/docs/Accounts.md
         */
        const currentAccounts = myMSALObj.getAllAccounts();
        if (currentAccounts === null) {
            return;
        } else if (currentAccounts.length > 1) {
            // Add choose account code here
            console.warn("Multiple accounts detected.");
        } else if (currentAccounts.length === 1) {
            username = currentAccounts[0].username;
            loginCallbackGraph(currentAccounts[0]);
            }
        }
    }

// User Interface routines tied to sign in/out buttons
function signIn() {                             // Request login based on scopes defined in authConfig.js
    console.log("graphapi authRedirect.js signIn");
    myMSALObj.loginRedirect(loginRequest);
    }

function signOut() {
    const logoutRequest = {account: myMSALObj.getAccountByUsername(username)};  // Get info on account to be logged out
    myMSALObj.logout(logoutRequest);                                            // Request logout
    }

function getTokenRedirect(request) {            // Called by MS-GRAPH calls to verify that you have a valid token
    console.log("graphapi authRedirect.js getTokenRedirect request");
    console.log(request);
    /**
     * See here for more info on account retrieval: 
     * https://github.com/AzureAD/microsoft-authentication-library-for-js/blob/dev/lib/msal-common/docs/Accounts.md
     */
    request.account = myMSALObj.getAccountByUsername(username);
    console.log("graphapi getTokenRedirect request.account: " + request.account);

    return myMSALObj.acquireTokenSilent(request)
        .catch(error => {
            console.warn("silent token acquisition fails. acquiring token using redirect");
            if (error instanceof msal.InteractionRequiredAuthError) {
                // fallback to interaction when silent call fails
                return myMSALObj.acquireTokenRedirect(request);
                } 
            else {console.warn(error);}
            });
    }

// *******************************************************
// END: THE ABOVE PART REMAINS THE SAME, DON'T MODIFY
// *******************************************************


// *******************************************************
// GRAPH API CALL SPECIFIC CONTENT
// *******************************************************
// This is the scope definition for graph api call to read user basic data.
// If you wanted to access user mail content as well, you'd add new scope to the array.
const tokenRequest = {
    scopes: ["User.Read"],
    forceRefresh: false // Set this to "true" to skip a cached token and go to the server to get a new token
    };


// THIS IS TO GET USER INFORMATION FROM MS-GRAPH (from INDEX.HTML "Click here to call MS Graph API to get logged in user information")
function seeProfile() {
    console.log("graphapi authRedirect.js seeProfile tokenRequest");
    console.log(tokenRequest);

    getTokenRedirect(tokenRequest)      // Calls function authRedirect_getTokenRedirect with scopes and forceRefresh to check for valid Token
        // .then(response => {
        //     console.log("authRedirect.js seeProfile returned_from_getTokenRedirect response.accessToken");
        //     console.log(response.accessToken);
        //     })
        .then(response => {callMSGraph(response.accessToken, myInfoReceivedCallback);}) //callMSGraph function (see below) with callback to myInfoReceivedCallBack (index.html)
        .catch(error => {console.error(error);});
    }

// Helper function to call MS Graph API endpoint 
// using authorization bearer token scheme
// (callback passed "myInfoReceivedCallback" as name of callback routine)
function callMSGraph(token, callback) {
    console.log("graphapi authRedirect.js callMSGraph token");
    console.log(token);
    console.log("graphapi authRedirect.js callMSGraph callback");
    console.log(callback);

    // Set up variables to be used in call to MS-GRAPH API via Fetch
    var endpoint = "https://graph.microsoft.com/v1.0/me";           // MS-GRAPH endpoint we are calling
    var endpoint = "https://graph.microsoft.com/v1.0/groups";       // MS-GRAPH endpoint we are calling
    const headers = new Headers();
    const bearer = `Bearer ${token}`;
    headers.append("Authorization", bearer);                    // Embed token into Header to send to MS-Graph
    const options = {method: "GET",headers: headers};           // Will do FETCH with GET and headers

    console.log('graphapi request made to Graph API at: ' + new Date().toString());
    console.log("graphapi Ready for fetch:");
    console.log("graphapi endpoint: " + endpoint);
    console.log("graphapi Options: " + options.method, options.headers);

    fetch(endpoint, options)
        .then(response => response.json())
        .then(response => callback(response, endpoint))
        .catch(error => console.log(error))
        ;
    }