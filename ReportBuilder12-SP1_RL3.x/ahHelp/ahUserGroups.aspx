<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221752"></a><a name="_Toc457221116">Organizations</a></h2>


<p class=MsoNormal><span style=''>Ad Hoc gives
the System Administrator the ability to establish multiple  organizations ,
which are collections of user communities. Every Ad Hoc user must be a member
of one organization; unless otherwise assigned they are in the  Default  organization.</span></p>


<p class=MsoNormal><span style=''>Organizations
generally do not share user communities, reports, or data.</span></p>


<p class=MsoNormal><span style=''>The ability
to define multiple organizations must be enabled via the Management Console. Refer
to the <i>Management Console Usage Guide</i> for details.</span></p>


<p class=MsoNormal><span style=''>Select <b>Organizations</b>
from the <i>User Configuration</i> drop-down list<b> </b>to display the <i>Organizations</i>
configuration page:</span></p>



<p class=MsoNormal><span style=''><img
border=0   id="Picture 20"
src="System_Admin_Guide_files/image025.jpg"></span></p>



<p class=MsoNormal><span style='font-size:18.0pt;Wingdings;
color:red'>I</span><span style='font-size:18.0pt;;
color:red'> </span><span style=''>The Default
organization is created with every new installation of the application. System
administrators can rename it, but it cannot be deleted. When attempting to
delete the Default organization, all members are deleted, except for the System
Administrator.</span></p>


<p class=MsoNormal><span style=''>Click <b>Add</b>
to display an empty <i>Organization</i> page.</span></p>


<p class=MsoNormal><span style=''>Click <b>Delete</b>
to remove selected organizations. Organizations are selected by checking their checkboxes.</span></p>


<p class=MsoNormal><span style=''>The  </span><span
style=''><img border=0  
id="Picture 21" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon indicates that more than one
action can be performed on the organization. Hover your mouse cursor over the  </span><span
style=''><img border=0  
id="Picture 22" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon to display the available
actions.</span></p>


<p class=MsoNormal><span style=''>The available
actions on the <i>Organizations</i> page are <b>Modify Organization, Delete
Organization</b>, and <b>Set Session Parameters</b>. The <b>Set Session
Parameters</b> action will only be displayed if a session parameter has
previously been created for the application.</span></p>

<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<p class=MsoNormal><b><span style=''>Adding an
Organization</span></b></p>


<p class=MsoNormal><span style=''>To add an
organization, click <b>Add</b> and the <i>Organization</i> page will be
displayed:</span></p>



<p class=MsoNormal><span style=''><img
border=0   id="Picture 23"
src="System_Admin_Guide_files/image027.jpg"></span></p>



<p class=MsoNormal><span style=''>An <i>Organization
Name</i> is required and must be unique. The <i>Description</i> of the organization
is optional. </span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the new organization information. </span></p>


<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<p class=MsoNormal><b><span style=''>Adding an
Organization Using  Save As </span></b></p>


<p class=MsoNormal><span style=''>The System Administrator
can also create an organization by modifying an existing organization and then
saving it with a new name, using the <b>Save As</b> button. This technique also
copies the session parameters and values, if any, from the existing
organization to the new organization.</span></p>


<p class=MsoHeader>&nbsp;</p>

<h5 align=left style='text-align:left'><span style='text-decoration:none'>Modifying
Organizations</span></h5>


<p class=MsoNormal><span style=''>System Administrators
can change the name, description, and theme of an organization at any time. Each
organization must have a unique name. </span></p>


<p class=MsoNormal><span style=''>To change an organization s
information, click <b>Modify Organization</b>. The <i>Modify</i> <i>Organization</i>
page will be displayed:</span></p>


<p class=MsoNormal><span style=''><img
border=0   id="Picture 24"
src="System_Admin_Guide_files/image028.jpg"></span></p>



<p class=MsoNormal><span style=''><br>
Modify the <i>Organization Name</i> and/or <i>Description </i>and click <b>Save</b>.</span></p>


<p class=MsoNormal><span style=''>To save the
edited information using a new <i>Organization Name</i>, click <b>Save As</b>,
enter the new name, and click <b>OK</b>.</span></p>

<p class=MsoHeader>&nbsp;</p>

<p class=MsoHeader>The Selected Organization drop-down list can be used to
navigate through organizations (thereby avoiding having to use the Back to
Organizations button and then selecting a different organization).</p>

<p class=MsoHeader>&nbsp;</p>

<p class=MsoHeader>&nbsp;</p>

<h5 align=left style='text-align:left'><a name="_Ref169938361"><span
style='text-decoration:none'>Setting an Organization s Session Parameters</span></a></h5>


<p class=MsoNormal><span style=''>System
administrators can create multiple session variables called <i>session parameters</i>
that can be used for display purposes or to filter data within a report or data
object. Session parameters and values are initially defined with application
scope. The session parameter values can be overridden for each organization. </span></p>


<p class=MsoNormal><span style=''>To change the
session parameter values for an organization, click the <b>Set Session Parameters</b>
action for the organization. The <i>Organization Session Parameters</i> page
will be displayed:</span></p>


<p class=MsoNormal><img border=0   id="Picture 25"
src="System_Admin_Guide_files/image029.jpg"></p>



<p class=MsoNormal><span style=''>If Session
Parameters have not been defined, the option to override the session parameter
values for organizations will not be available.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #FFFFCC 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#FFFFCC'>
  <p class=HintCxSpFirst><b>Hint:</b></p>
  <p class=HintCxSpLast>Refer to the <i>Session Parameters</i> chapter for help
  creating parameters and setting default values.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<p class=MsoHeader>&nbsp;</p>

<p class=MsoNormal><span style=''>The <i>Selected
Organization</i> identifies the organization that was the action target from
the previous page. The drop-down list allows the Administrator to select other
organizations and review or adjust their session parameters without having to
return to the Organizations page.</span></p>


<p class=MsoNormal><span style=''>The grid
presents the session parameters that can be set for the organization. Click the
<i>Parameter Name </i>or the <i>Follow Default</i> column header to sort the
contents of the grid.</span></p>


<p class=MsoNormal><span style=''>The <b>Restore
Defaults</b> button provides a mechanism to reset all of the checked session parameters
back to the values shown in the <i>Default Value</i> column.</span></p>


<p class=MsoNormal><span style=''>The <i>Follow
Default</i> checkbox specifies whether the parameter value should adopt the <i>Default
Value</i>. This also allows the parameter value to be set  permanently ,
meaning that changes to the <i>Default Value</i> will have no impact on the
parameter value for the organization, if the <i>Follow Default</i> checkbox is
unchecked.</span></p>


<p class=MsoNormal><span style=''>The grayed <i>Parameter
Value</i> text boxes are not disabled. Their gray color is a visual cue that
the value is the same as the default value and is expected to follow the default
value. That means that if the default value changes, the organization will automatically
pick up the new value.</span></p>


<p class=MsoNormal><span style=''>The <i>Parameter
Value</i> can be changed by either entering it directly into the text box or by
selecting the<b> Modify</b> action and providing a new value. The parameter
value may also be changed by selecting the <b>Restore Default</b> action.</span></p>


<p class=MsoNormal><span style=''>If any <i>Parameter
Value</i> is set it will be considered an override value and the <i>Follow
Default</i> checkbox will automatically be unchecked. </span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the session parameter values for the organization.</span></p>



<p class=MsoNormal><span style=''>An action
that is available for each session parameter is <b>Set By User.</b> Clicking
this action will present a page that allows the session parameter values to be
set for all users in the organization:</span></p>



<p class=MsoNormal><span style=''><img
border=0   id="Picture 26"
src="System_Admin_Guide_files/image030.jpg"></span></p>



<p class=MsoNormal><span style=''>The <i>Selected
Session Parameter</i> value initially identifies the parameter that was
specified when linking to this page. The drop-down list allows the
Administrator to review and modify other session parameter values for the group
of users.</span></p>


<p class=MsoNormal><span style=''>The <i>Default
Value</i> is the value that will be used for all users in the list unless the
value is specifically overridden.</span></p>


<p class=MsoNormal><span style=''>The <i>Type</i>
value specifies one of the five session parameter data types; <i>date</i>, <i>number</i>,
<i>numeric list</i>, <i>text</i>, and <i>textual list</i>.</span></p>


<p class=MsoNormal><span style=''>The <i>Role</i>
drop-down list can be used to filter the list of users. Initially the drop-down
indicates  All  and lists all users in the Organization.</span></p>


<p class=MsoNormal><span style=''>Click <b>Restore
Default</b> to reset the session parameter value for all selected users back to
the displayed Default Value. Users can be selected by checking their checkbox.
All users can be selected by clicking the checkbox in the list header.</span></p>


<p class=MsoNormal><span style=''>Click <b>Set
Value</b> to open a dialog box to acquire a new value and then apply the value
to all of the selected users.</span></p>


<p class=MsoNormal><span style=''>The list of
users can be sorted by clicking the <i>User</i> or <i>Follow Default</i> column
header.</span></p>


<p class=MsoNormal><span style=''>The <i>Follow
Default</i> checkbox specifies whether the parameter value should adopt the <i>Default
Value</i>. This also allows the parameter value to be set  permanently , meaning
that changes to the <i>Default Value</i> will have no impact on the parameter
value for the user, if the <i>Follow Default</i> checkbox is unchecked.</span></p>


<p class=MsoNormal><span style=''>The grayed <i>Parameter
Value</i> text boxes are not disabled. The gray color is a visual cue that the
value is the same as the default value and is expected to follow the default
value. That means that if the default value changes, the user will
automatically pick up the new value.</span></p>


<p class=MsoNormal><span style=''>The <i>Parameter
Value</i> can be changed by either entering it directly into the text box or by
selecting the<b> Modify</b> action and providing a new value. The parameter
value can also be changed by selecting the <b>Restore Default</b> action.</span></p>


<p class=MsoNormal><span style=''>If any <i>Parameter
Value</i> is set, it will be considered an override value and the <i>Follow
Default</i> checkbox will be automatically unchecked. </span></p>


<p class=MsoNormal><span style=''>To change the
parameter value, either enter a new value in the <i>Parameter Value</i> text
box or hover your mouse cursor over the <i>Actions</i> </span><span
style=''><img border=0  
id="Picture 27" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon and select <b>Modify</b> from
the list of actions. </span></p>


<p class=MsoNormal><span style=''>The <b>Modify</b>
action will display a dialog box relevant to the type of the session parameter.
 Date  session parameters will have a date picker control and  List  session
parameters will present a list of values in the dialog box.</span></p>


<p class=MsoNormal><span style=''>From the<i>
Actions</i> icon, the session parameter value for the related user can be reset
to the default value for the Organization by clicking <b>Restore Default</b>.</span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the session parameter values for the User.</span></p>


<span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span>



</body>
</html>
