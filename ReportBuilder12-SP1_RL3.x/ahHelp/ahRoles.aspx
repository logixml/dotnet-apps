<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221753"></a><a name="_Toc457221117">Roles</a></h2>


<p class=MsoNormal><span style=''>Roles
determine what a user is authorized to do within Ad Hoc. Access to features,
databases, objects, and columns are all controlled by the rights given to the
user through their assigned roles.</span></p>


<p class=MsoNormal><span style=''>A <i>role</i><b>
</b>is comprised of one or more packages of rights, called <i>permissions</i>
or <i>permission packages</i>. Roles are associated with organizations and then
assigned to individual users within the organization. </span></p>


<p class=MsoNormal><span style=''>Select <b>Roles</b>
from the <i>User Configuration</i> drop-down list<b> </b>to display the <i>Roles</i>
configuration page:</span></p>


<p class=MsoNormal><span style=''><img
border=0   id="Picture 28"
src="System_Admin_Guide_files/image031.jpg"></span></p>



<p class=MsoNormal><span style=''>Click <b>Add</b>
to display an empty <i>Role</i> page.</span></p>


<p class=MsoNormal><span style=''>Click <b>Delete</b>
to remove selected roles. Roles are selected by checking their checkbox.</span></p>


<p class=MsoNormal><span style=''>If a user is
assigned multiple roles he will lose any role that the System Administrator
deletes. If a user is assigned only one role and it s deleted, he no longer has
access to the Ad Hoc instance. A user may not delete a role that is currently
assigned to their own User account.</span></p>


<p class=MsoNormal><span style=''>The  </span><span
style=''><img border=0  
id="Picture 29" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon indicates that more than one
action can be performed on the role. Hover your mouse cursor over the  </span><span
style=''><img border=0  
id="Picture 30" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon to display the available
actions.</span></p>


<p class=MsoNormal><span style=''>The available
actions on the <i>Roles</i> page are <b>Modify Role, Delete Role</b>, and <b>Set
Data Object Access Rights</b>. </span></p>



<p class=MsoNormal><b><span style=''>Adding a
Role</span></b></p>


<p class=MsoNormal><span style=''>To add a role,
click <b>Add</b> and the <i>Role</i> page will be displayed:</span></p>



<p class=MsoNormal><img border=0   id="Picture 349"
src="System_Admin_Guide_files/image032.jpg"></p>

<p class=MsoNormal><span style=''>The <i>Role</i>
(name) value is required and must be unique. The <i>Description</i> of the role
is optional. </span></p>

<p class=MsoNormal><span style=''>Assign <i>Permissions</i>,
<i>Organizations</i>, <i>Databases</i>, and <i>Users</i> from their respective <i>Available</i>
lists.</span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the new Role information. </span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Every role must have at least one organization
  assigned, typically the  Default  organization. If the Ad Hoc instance only
  has the  Default  organization defined, the Available/Assigned Organization
  control will not be shown.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<p class=MsoNormal><b><span style=''>Adding a
Role Using  Save As </span></b></p>


<p class=MsoNormal><span style=''>The System Administrator
can also create a role by modifying an existing role and then saving it with a
new name, by using the <b>Save As</b> button. This technique copies the data
object access right settings from the existing role to the new role.</span></p>



<h5 align=left style='text-align:left'><span style='text-decoration:none'>Modifying
Roles</span></h5>


<p class=MsoNormal><span style=''>System Administrators
can change the <i>Role</i> name, description, and its assigned permissions,
organizations, databases, and users. </span></p>


<p class=MsoNormal><span style=''>To change the
Role information, click its <b>Modify Role</b> action. The <i>Modify</i> <i>Role</i>
page will be displayed.</span></p>


<p class=MsoNormal><span style=''>Modify the <i>Role</i>,
<i>Description</i>, or assignments and click <b>Save</b> to save the information.</span></p>


<p class=MsoNormal><span style=''>To save the
edited information using a new <i>Role </i>name, click <b>Save As</b>, enter
the new name, and click <b>OK</b> to save the information.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>The Selected Role drop-down list can be used to
  navigate through roles and avoid using the Back to Roles button and selecting
  a role.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>The <i>System Admin </i>role must have an assigned
  user named <i>Admin.</i></p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>




<h5 align=left style='text-align:left'><a name="_Ref169668422"><span
style='text-decoration:none'>Setting a Role's Data Object Access Rights</span></a></h5>


<p class=MsoNormal><span style=''>System
administrators can control each role s access to data objects. Access rights
can be granted at the column-level or data object-level. You modify access
rights to a data object by specifying access levels for each column of the
object.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>The <i>System Admin</i> role always has <i>Full</i>
  access rights to all data objects and columns within each data object.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal><span style=''>The <i>Object
Access Rights</i> page allows the System Administrator to assign access rights
to specific data objects.<span style='color:red'> </span>By default, all roles
associated with the current reporting database have <i>Full </i>access to all
objects and columns in the current reporting database.</span></p>


<p class=MsoNormal><span style=''>The three levels
of access for data objects are:</span></p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><span style=''>Full - All
     columns are accessible.</span></li>
 <li class=MsoNormal><span style=''>Limited - Some
     columns are accessible.</span></li>
 <li class=MsoNormal><span style=''>None - No
     columns are accessible</span></li>
</ul>


<p class=MsoNormal><span style=''>The two
levels of access for individual columns are:</span></p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><span style=''>Full - Column
     available for reporting</span></li>
 <li class=MsoNormal><span style=''>None - No
     access</span></li>
</ul>



<p class=MsoNormal><span style=''>To change the
access to columns, click the <b>Set Data Object Access Rights</b> action for
the role. The <i>Object Access Rights</i> page will be displayed:</span></p>


<p class=MsoNormal><span style=''><img
border=0   id="Picture 34"
src="System_Admin_Guide_files/image033.jpg"></span></p>



<p class=MsoNormal><span style=''>The <i>Database</i>
drop-down list allows the System Administrator to select the pool of objects
for the database.</span></p>


<p class=MsoNormal><span style=''>The <i>Data Object</i><b>
</b>column lists all data objects. The <i>Type</i> column displays the object
type and the <i>Access</i><b> </b>column displays the access type for each data
object.</span></p>


<p class=MsoNormal><span style=''>The default
access type for each data object and all columns is <i>Full</i>.</span></p>


<p class=MsoNormal><span style=''>Column
permissions for the System Admin role cannot be modified for any data object.</span></p>



<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>Click the </span><span
style=''><img border=0  
id="Picture 35" src="System_Admin_Guide_files/image008.gif" alt=iconAction></span><span
style=''> icon to <b>Modify Column Access
Rights</b> and display the <b>Column Access Rights</b> dialog box:</span></p>



<p class=MsoNormal><img border=0   id="Picture 351"
src="System_Admin_Guide_files/image034.jpg"></p>



<p class=MsoNormal><span style=''>Using the
checkboxes, select the desired columns and click <b>Set to Full</b> or <b>Set
to None</b> to change the access rights for the columns for the respective role
and data object.</span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the configuration.</span></p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #FFFFCC 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#FFFFCC'>
  <p class=HintCxSpFirst><b>Hint:</b></p>
  <p class=HintCxSpLast>You can quickly switch between roles by choosing the
  role from the drop-down list provided. Check one or more checkboxes and click
  <b>Set to Full</b> or <b>Set to None</b> to make fast access changes to
  objects in the metadata database. Click <b>Copy Permissions</b> and then
  choose a role to quickly duplicate permissions between user roles.</p>
  </div>
  </td>
 </tr>
</table>

</div>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>



</body>
</html>
