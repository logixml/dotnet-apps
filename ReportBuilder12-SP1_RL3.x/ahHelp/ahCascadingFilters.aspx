<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221782"></a><a name="_Toc457221146">Cascading Filters</a></h2>


<p class=MsoNormal><span style=''>The Cascading
Filters page allows the System Administrator to create and configure cascading
filters for use when building reports with the Report Builder. The term <i>cascading
filter </i>refers to a series of dependent drop-down lists where the value selected
in the first list affects the values displayed in the second list. Cascading
filters have no default values but instead rely on user input at runtime.</span></p>


<p class=MsoNormal><span style=''>Select <b>Cascading
Filters</b> from the <i>Report Configuration</i> drop-down list<b> </b>to
display the <i>Cascading Filters</i> configuration page.</span></p>



<p class=MsoNormal><span style=''><img
border=0   id="Picture 111"
src="System_Admin_Guide_files/image078.jpg"></span></p>


<p class=MsoNormal><span style=''><br>
The <b>Database</b> drop-down list acts as a filter for the Cascading Filter
list. Only Cascading Filters related to the selected database will be
displayed. If only one reporting database has been configured for the Ad Hoc
instance, the <b>Database</b> filter list will not be shown.</span></p>


<p class=MsoNormal><span style=''>Click <b>Add</b>
to display an empty <i>Cascading Filter</i> page.</span></p>


<p class=MsoNormal><span style=''>Click <b>Delete</b>
to remove the selected cascading filters. Cascading filters are selected by checking
their checkboxes.</span></p>


<p class=MsoNormal><span style=''>Two actions
are available for a Cascading Filter: <b>Modify Filter </b>and<b> Delete Filter.
</b>Hover your mouse cursor over the </span><span style=''><img
border=0   id="Picture 112"
src="System_Admin_Guide_files/image026.jpg"></span><span style='
"Arial","sans-serif"'> icon to show them.</span></p>


<p class=MsoNormal><span style=''>Each
cascading filter is comprised of one or more <i>filter items</i>. One filter
item<i> </i>equals one drop-down list. Administrators must configure the
following four attributes for each filter item:</span></p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><i><span style=''>Data
     Object</span></i><span style=''> - The
     data object used to populate the drop-down list.</span></li>
 <li class=MsoNormal><i><span style=''>Filter
     Column</span></i><b><span style=''> </span></b><span
     style=''>- The column that is filtered by
     the value selected from the previous filter item.</span></li>
 <li class=MsoNormal><i><span style=''>Value
     Column</span></i><b><span style=''> </span></b><span
     style=''>- Tthe column that provides the
     value for the next filter item.</span></li>
 <li class=MsoNormal><i><span style=''>Display
     Column</span></i><b><span style=''> </span></b><span
     style=''>- The column that provides the
     values for the drop-down list.</span></li>
</ul>


<p class=MsoNormal><span style=''>Cascading
filters are used as parameters in reports. From the Report Builder, end-users
must select a column to filter and then choose <b>In cascading list </b>as the
operator. A list of all the cascading filters present is displayed and the end-user
must choose one.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst>Note:</p>
  <p class=NotesCxSpLast>The first filter item always contains a blank value
  for the Filter Column.<br>
  <br>
  The  In cascading list  and  Not in cascading list  options are only
  displayed to the end-user creating a report when the column selected to be
  filtered matches the <i>last</i> <i>Value Column</i> in the cascading filter
  definition.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<h3><a name="_Toc457221783"></a><a name="_Toc457221147">Adding a new Cascading
Filter</a></h3>


<p class=MsoNormal><span style=''>Click <b>Add</b>
to define a new cascading filter. A blank <i>Cascading Filter</i> page will be
displayed:<br>
<br>
</span></p>


<p class=MsoNormal><span style=''><img
border=0   id="Picture 113"
src="System_Admin_Guide_files/image079.jpg"></span></p>

<p class=MsoNormal><span style=''>The <i>Database
Connection</i> identifies the currently selected reporting database. </span></p>


<p class=MsoNormal><span style=''>Enter a <i>Name</i>
and <i>Description</i> (optional) for the cascading filter.</span></p>


<p class=MsoNormal><span style=''>Click <b>Add</b>
to create a filter item. If this is the first filter item, the following dialog
box will be displayed:</span></p>




<p class=MsoNormal><img border=0   id="Picture 355"
src="System_Admin_Guide_files/image080.jpg"></p>



<p class=MsoNormal><span style=''>If this is an
additional filter item, the following dialog box will be displayed:</span></p>


<p class=MsoNormal><img border=0   id="Picture 357"
src="System_Admin_Guide_files/image081.jpg"></p>


<p class=MsoNormal><span style=''>The
difference between the two dialog boxes is the <i>Filter Column</i>. Since
there are no filters applied to the first list of a cascading filter, this
option is not displayed for the first cascading filter list.</span></p>


<p class=MsoNormal><span style=''>Repeat the
process of adding filter items until the cascading filter definition is complete.</span></p>


<p class=MsoNormal><span style=''>Click <b>Delete</b>
to remove the selected filter items. Filter items are selected by checking
their checkboxes.</span></p>


<p class=MsoNormal><span style=''>The
definition of the filter item can be modified by clicking on the </span><span
style=''><img border=0  
id="Picture 116" src="System_Admin_Guide_files/image008.gif" alt=iconAction></span><span
style=''> icon.</span></p>


<p class=MsoNormal><span style=''>Click <b>Test
Filter</b> to exercise the filter items and verify that the lists are properly
populated. A preview page will be displayed similar to the following:<br>
<br>
</span></p>


<p class=MsoNormal><span style=''><img
border=0   id="Picture 117"
src="System_Admin_Guide_files/image082.jpg"></span></p>

<p class=MsoNormal><span style=''>The cascading
filter test will only exercises the filter. It doesn t accurately reflect the way
the cascading filter will <i>look</i> in a report.</span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the cascading filter definition in the metadata database.</span></p>


<p class=MsoNormal><span style=''>Here s an example
of cascading filers at work. The following cascading filter definition was the
basis for the <b>Test Filter</b> description above:</span></p>



<p class=MsoNormal><span style=''><img
border=0   id="Picture 118"
src="System_Admin_Guide_files/image083.jpg"></span></p>


<p class=MsoNormal><span style=''>The first
filter list is based on the  Country  column of the  Customers  data object.
When the cascading filter is displayed, the first drop-down list will be
populated with a list of the distinct countries found in the  Customers  data
object. The list is unfiltered. The caption for the list will be  Country , the
friendly name of that column. When the user selects one of the countries, the
data in the  Country  <i>Value Column</i> will be passed to the next filter
item.</span></p>


<p class=MsoNormal><span style=''>The second
filter list is based on the  Region  column of the  Customers  data object.
When this list is displayed, the contents will be all of the distinct regions
in the  Customers  table, filtered by the  Country  value selected from the
previous list. The caption for the list will be  Region , the friendly name for
that column. When the user selects a  Region , the data in the  Region  <i>Value
Column</i> will be passed to the next filter item.</span></p>


<p class=MsoNormal><span style=''>The last
filter item is based on the  City  of the  Customers  data object. When this
list is displayed, the contents will be a list of  Cities  filtered by the  Region 
value selected from the previous list. When the user selects a  City , the corresponding
 City  value will be used to filter the data in the report.</span></p>


<p class=MsoNormal><span style=''>The <i>In cascading
list</i> option will be displayed to the user building the report when the
column that the parameter is based on is the  City  column.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>The Value Column and Display Column can be different.<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>The cascading filter can be based on multiple,
  related data objects.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>Only the last filter item is used to filter the
  report. If the last filter Value Column was  City  and the user selected
   Springfield , all records with a  City  of  Springfield  will be selected.
  The previous filter items are not considered.</p>
  </div>
  </td>
 </tr>
</table>

</div>

</body>
</html>
