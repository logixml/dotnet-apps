<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221793"></a><a name="_Toc457221157">Archives</a></h2>


<p class=MsoNormal><span style=''>The Archives
page allows System Administrators to view the complete archive for any individual
report. Administrators can delete an individual report from the archive or
delete the entire archive. An archive  snapshot  can also be emailed.</span></p>


<p class=MsoNormal><span style=''>Select <b>Archives</b>
from the <i>Report Configuration</i> drop-down list<b> </b>to display the <i>Scheduled
Reports</i> configuration page:</span></p>


<p class=MsoNormal><img border=0  
src="System_Admin_Guide_files/image093.jpg"></p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>The <i>Archives</i> option will not be displayed
  unless archiving is enabled and at least one report for the current reporting
  database has been archived.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal><span style=''>Click a
report's name link in the <i>Name</i> column to view the report in a new
browser window.</span></p>


<p class=MsoNormal><span style=''>Click the
number link in the <i>Archive Count</i> column to view all archives associated
to the report.</span></p>


<p class=MsoNormal><span style=''>Reports with
archives can be sorted by clicking on the <i>Name</i> or <i>Owner</i> column
header.</span></p>


<p class=MsoNormal><span style=''>Click <b>Delete
Archives</b> to remove all archives for a report. Archives are selected for
deletion by checking their checkboxes. Click <b>OK</b> when prompted to confirm
the deletion.</span></p>


<p class=MsoNormal><span style=''>To view the
archives associated to a report, click the number link in the <i>Archive Count</i>
column next to the corresponding report. The Archive page will be displayed for
the report:</span></p>

<p class=MsoNormal style='margin-left:.25in'><span style=''>&nbsp;</span></p>

<p class=MsoNormal style='margin-left:.25in'><span style=''>&nbsp;</span></p>

<p class=MsoNormal><img border=0   id="Picture 1"
src="System_Admin_Guide_files/image094.jpg"></p>



<p class=MsoNormal><span style=''>Click the <i>Archived
Date</i> or <i>Owner</i> column headers to sort the archive list.</span></p>


<p class=MsoNormal><span style=''>Click the
date link from the <i>Archived Date</i> column to view the archived report in a
new browser window.</span></p>


<p class=MsoNormal><span style=''>Click the </span><span
style=''><img border=0  
id="Picture 141" src="System_Admin_Guide_files/image095.jpg"></span><span
style=''> icon to email the archive report.
Enter a recipient address and subject, then click <b>Send</b> to email the
report.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Archives can be centrally managed through the
  Management Console. Refer to the Management Console Usage Guide for details
  regarding managing the archive folders.</p>
  </div>
  </td>
 </tr>
</table>

</div>

</body>
</html>
