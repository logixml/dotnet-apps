<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945109"></a><a name="_Toc382291606">Configuring Table Columns</a></h2>


<p class=MsoNormal>The first step of table configuration is to select the
columns to be used in the display table. If the report contains more than one
display table, the configuration area will reflect the currently selected
display table.</p>


<p class=MsoNormal>To select columns for the table:</p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>Click the <i>Table Columns </i>tab.</li>
</ol>


<p class=MsoNormal><img border=0   id="Picture 80"
src="Report_Design_Guide_files/image080.jpg"></p>


<ol style='margin-top:0in' start=2 type=1>
 <li class=MsoNormal>Select one or more<i> Available Columns</i> and then click
     the <img border=0   id="Picture 81"
     src="Report_Design_Guide_files/image081.jpg"> icon to add the column(s) to
     the<i> Assigned Columns</i> list. Hold the Ctrl key down to select
     multiple columns.<br>
     <br>
 </li>
 <li class=MsoNormal>From the <i>Assigned Columns</i> list, change a column's
     initial display order by clicking either the <img border=0 
      id="Picture 82" src="Report_Design_Guide_files/image082.jpg"> or
     <img border=0   id="Picture 83"
     src="Report_Design_Guide_files/image083.jpg"> icon to move the row up or
     down. Hold the Ctrl key down to select multiple columns.<br>
     <br>
 </li>
 <li class=MsoNormal>To remove a column from the <i>Assigned Columns</i> list,
     select it and then click the <img border=0  
     id="Picture 84" src="Report_Design_Guide_files/image084.jpg"> icon. Hold
     the Ctrl key down to select multiple columns.</li>
</ol>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>At least one data column should be selected before
  continuing.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>A column description will only be available if one has
  been specified by the System Administrator.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<span style='font-size:6.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>To configure and delete columns in the table:</p>


<p class=MsoNormal>After adding them, each column's order of appearance,
display characteristics, and summary information may be defined on the <i>Column
Configuration</i> tab:</p>



<p class=MsoNormal><img border=0   id="Picture 85"
src="Report_Design_Guide_files/image085.jpg"></p>


<p class=MsoNormal>When first displayed, only the Column, Header and Sortable
columns are shown in the configuration grid. Click the <i>Show All Attributes</i>
icon to expand the grid so that it looks like the example above. The grid can
be collapsed again by clicking the <i>Show Minimum Attributes</i> icon.</p>


<p class=MsoNormal>Data columns can be selected (or deselected) by checking their
checkbox. All data columns can be toggled at once by clicking the checkbox in
the upper-left corner of the grid. Some of the following functions apply to
selected columns.</p>


<p class=MsoNormal>Grid rows can be rearranged by selecting the row and then
clicking the <img border=0   id="Picture 86"
src="Report_Design_Guide_files/image086.jpg"> or <img border=0 
height=24 id="Picture 87" src="Report_Design_Guide_files/image087.jpg"> icon to
move the row up or down. To move a group of rows, select the desired columns by
checking their respective checkboxes and then clicking either the <img
border=0   id="Picture 88"
src="Report_Design_Guide_files/image086.jpg"> or <img border=0 
height=24 id="Picture 89" src="Report_Design_Guide_files/image087.jpg"> icon to
move the set of rows up or down.</p>


<p class=MsoNormal>Data columns can be removed from the display table by
selecting them and clicking the <img border=0   id="Picture 90"
src="Report_Design_Guide_files/image088.gif" alt=remove>icon.</p>


<b><span style='font-size:10.0pt;'><br
clear=all style='page-break-before:always'>
</span></b>


<p class=MsoNormal>There are nine configurable options available in Column
Configuration:</p>


<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=280 valign=top style='width:167.8pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Header</p>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Linkable</p>
  <p class=MsoListParagraphCxSpLast style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Sortable</p>
  </td>
  <td width=280 valign=top style='width:167.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Summary</p>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Format</p>
  <p class=MsoListParagraphCxSpLast style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Width</p>
  </td>
  <td width=280 valign=top style='width:167.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Alignment</p>
  <p class=MsoListParagraphCxSpLast style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Style</p>
  </td>
 </tr>
</table>


<p class=MsoNormal>The <i>Header</i> determines the column header displayed
when the report is rendered.</p>


<p class=MsoNormal>The <i>Linkable</i> option toggles pre-defined hyperlinks
for records in the column. Each record in the <i>Linkable</i> column contains a
hyperlink to an address specified by the System Administrator. Make column
records linkable from the report by placing a check in the corresponding <i>Linkable</i>
checkbox.</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>The Linkable column will only be shown if the System
  Administrator has configured at least one of the columns as  hyperlink
  capable .</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>The <i>Sortable</i> option determines whether the column in
the rendered report can be sorted by the end-user. Sorting can be enabled or
disabled for all columns by checking the checkbox in the header of the <i>Sortable</i>
column.</p>


<p class=MsoNormal>The <i>Summary</i> option offers the ability to create table
footers containing aggregates of values for each column of data. An unlimited
number of aggregates can be created for each table column. The following
aggregate functions are supported:</p>


<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=280 valign=top style='width:167.8pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoListParagraphCxSpFirst style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Average</p>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Calculation</p>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Count</p>
  </td>
  <td width=280 valign=top style='width:167.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Count Distinct</p>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Maximum</p>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Minimum</p>
  </td>
  <td width=280 valign=top style='width:167.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoListParagraphCxSpMiddle style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Standard Deviation</p>
  <p class=MsoListParagraphCxSpLast style='text-indent:-.25in'><span
  style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  </span></span>Sum</p>
  </td>
 </tr>
</table>




<p class=MsoNormal>To manage summary values:</p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <img
border=0 width=15 height=14 id="Picture 91"
src="Report_Design_Guide_files/image089.gif" alt=sumIcon1> to display the <i>Aggregates</i>
dialog box for a specific column. <br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>Add
an Aggregate</b> and the following dialog box will be displayed:</p>

<p class=MsoNormal><br>
<br>
</p>


<p class=MsoNormal><img border=0   id="Picture 328"
src="Report_Design_Guide_files/image090.jpg"><br>
<br>
</p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Type a
name used as the internal value for the aggregate in the <i>Name</i> field.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Type a displayed
name for the new value in the <i>Label</i> field.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Choose an <i>Aggregate</i>
function from the drop-down list.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>6.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Choose a <i>Format</i>
from the drop-down list.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>7.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>OPTIONAL:
If more than one aggregate has been specified, click the <img border=0
  id="Picture 93" src="Report_Design_Guide_files/image091.gif"
alt=arrowUp> or <img border=0   id="Picture 94"
src="Report_Design_Guide_files/image092.gif" alt=arrowDown> icon to arrange the
order in which the aggregate will appear in the column. <br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>8.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Continue
adding additional aggregates or click <b>OK</b> to add the summary value(s) and
return to the <i>Column Configuration</i> interface.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>9.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Aggregates
can be removed by clicking the <img border=0   id="Picture 95"
src="Report_Design_Guide_files/image088.gif" alt=remove>icon.</p>




<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>An <img border=0   id="Picture 96"
  src="Report_Design_Guide_files/image093.gif" alt=sumIcon2> icon is displayed
  to indicate that a summary value exists for a particular column.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>One of the aggregation options is  Calculation . This option
allows the user to create a new aggregation from previously defined summary
information. When  Calculation  is selected, the following dialog box is
displayed: </p>


<p class=MsoNormal><img border=0   id="Picture 332"
src="Report_Design_Guide_files/image094.jpg"></p>


<p class=MsoNormal><br>
In the Modify Calculation dialog box, any of the previously defined summaries
may be used in the calculation as well as aggregate functions on columns and
direct constants in the definition. The default internal name for an
aggregation is AGGR<i>n</i>, where <i>n</i> is a unique number.</p>


<p class=MsoNormal><br>
The <i>Format</i> option provides data formatting options for values in each
column. The following formatting options are supported:</p>



<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=420 valign=top style='width:251.75pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>(none)</li>
   <li class=MsoNormal>General Number</li>
   <li class=MsoNormal>Currency</li>
   <li class=MsoNormal>Integer</li>
   <li class=MsoNormal>Fixed</li>
   <li class=MsoNormal>Standard</li>
   <li class=MsoNormal>Percent</li>
   <li class=MsoNormal>Scientific</li>
   <li class=MsoNormal>2- or 3-digit place holder</li>
  </ul>
  </td>
  <td width=420 valign=top style='width:251.75pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>General Date</li>
   <li class=MsoNormal>Long/Medium/Short Date</li>
   <li class=MsoNormal>Long/Medium/Short Time</li>
   <li class=MsoNormal>Yes/No</li>
   <li class=MsoNormal>True/False</li>
   <li class=MsoNormal>On/Off</li>
   <li class=MsoNormal>HTML<sup>1</sup></li>
   <li class=MsoNormal>Preserve line feed<sup>2</sup></li>
  </ul>
  </td>
 </tr>
</table>






<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>The application chooses the default format type for
  each column. Changing the format type may yield undesirable results.<br>
  <br>
  If a format is needed that isn t offered, contact your System Administrator.
  Additional formats may be provided by the System Administrator<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>1. The Format &quot;HTML&quot; can only be specified
  by the System Administrator and may not be changed to something different
  from the Column Configuration panel.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>2. The Format  Preserve line feed  allows text in a
  memo type field to display as it is stored with line feeds (carriage returns)
  observed.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>The <i>Width</i> option offers the ability to customize the
width of each column, improving the appearance of the report when it is
rendered in the web page and when exported. </p>


<p class=MsoNormal>By default, a column's <i>Width</i> value is blank to allow the
application to  automatically determine the appropriate width based on:</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>All columns in the report</li>
 <li class=MsoNormal>The context in each column</li>
 <li class=MsoNormal>The available webpage space</li>
 <li class=MsoNormal>The page size and orientation</li>
</ul>


<p class=MsoNormal>When customizing a column's width, it s important to
determine the width scale. The scale of measured can be either <i>pixels</i> or
<i>percent</i>, where: <br>
<br>
</p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><i>Pixel</i> is a single point of graphic data displayed
     on the monitor. Hundreds of pixels can be used to display a very small
     image. </li>
 <li class=MsoNormal><i>Percentage</i> is a fraction of the screen space allocated
     for each column.<br>
     <br>
 </li>
</ul>


<p class=MsoNormal>To modify a column's width:</p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>OPTIONAL: Determine the scale Type to use for the entire
     tabular report. To toggle the scale of a column, click the label to the
     right on the width field and select <i>px</i> for pixels and <i>%</i> for percentage.</li>
 <li class=MsoNormal>Enter or modify the column's Width value.</li>
</ol>







<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>When specifying a tabular report's column's widths
  in pixels, keep in mind the average monitor resolution settings of the
  end-users viewing the report.<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>When specifying a tabular report's column's widths
  in percentages, keep in mind that:</p>
  <p class=NotesCxSpMiddle>a. The sum total of the percentage values must not
  exceed 100%.</p>
  <p class=NotesCxSpLast>b. The application will use whatever percentage has
  not been allocated to columns for the columns without a value.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>




<p class=MsoNormal>The <i>Alignment</i> option adjusts the position of values
in columns. In the following example, the alignment of numerical values is
changed from Left to Center in the <i>Quantity</i> column<br>
.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse'>
 <tr style='height:28.35pt'>
  <td width=246 style='width:2.05in;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoNormal align=center style='text-align:center'><img border=0
    id="Picture 98"
  src="Report_Design_Guide_files/image095.jpg" alt="alignment_before"></p>
  </td>
  <td width=246 style='width:2.05in;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoNormal align=center style='text-align:center'><img border=0
    id="Picture 99"
  src="Report_Design_Guide_files/image096.gif" alt=arrow></p>
  </td>
  <td width=246 style='width:2.05in;padding:0in 5.4pt 0in 5.4pt;height:28.35pt'>
  <p class=MsoNormal align=center style='text-align:center'><img border=0
    id="Picture 100"
  src="Report_Design_Guide_files/image097.jpg" alt="alignment_after"></p>
  </td>
 </tr>
</table>


<p class=MsoNormal>The <i>Style</i> option<i> </i>offers the ability to apply
conditional formatting to a column's cells based on a specific value or another
column's value. Users must create the condition and specify the formatting
style. When more than one condition is specified, the application will apply
the style associated to the first condition that is satisfied. Conditions are
evaluated when the report is run.</p>


<p class=MsoNormal>Users can optionally apply the specified conditional
formatting to all data columns by checking the appropriate checkbox.</p>


<p class=MsoNormal><img border=0   id="Picture 101"
src="Report_Design_Guide_files/image098.gif" alt=condFormat></p>


<p class=MsoNormal>A conditional style takes the form of an equation similar
to:</p>


<p class=MsoFooter align=center style='text-align:center'><i>Label </i>is <i>Compared
</i>to <i>Value<br>
 </i></p>

<p class=MsoFooter align=center style='text-align:center'>Or<br>
<br>
</p>

<p class=MsoFooter align=center style='text-align:center'><i>Label</i> is <i>Compared</i>
to <i>Column</i></p>


<p class=MsoFooter>where <i>Label</i> represents a column name, <i>Compared</i>
represents a comparison operator, <i>Value</i> represents a threshold, and <i>Column</i>
represents another data column.</p>


<p class=MsoFooter>The available comparison operators are:</p>



<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=420 valign=top style='width:251.75pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>Equal to</li>
   <li class=MsoNormal>Not equal to</li>
   <li class=MsoNormal>Less than</li>
   <li class=MsoNormal>Greater than</li>
   <li class=MsoNormal>Less than or equal to</li>
   <li class=MsoNormal>Greater than or equal to</li>
   <li class=MsoNormal>Starts with <sup>1</sup></li>
  </ul>
  </td>
  <td width=420 valign=top style='width:251.75pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>Does not start with <sup>1</sup></li>
   <li class=MsoNormal>Ends with <sup>1</sup></li>
   <li class=MsoNormal>Does not end with <sup>1</sup></li>
   <li class=MsoNormal>Contains <sup>1</sup></li>
   <li class=MsoNormal>Does not end with <sup>1</sup></li>
   <li class=MsoNormal>Contains <sup>1</sup></li>
   <li class=MsoNormal>Not between</li>
  </ul>
  </td>
 </tr>
</table>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>1. These operators are only available for data type
  of type String or Text.</p>
  <p class=NotesCxSpLast>2. The operators available are dependent upon the
  column's data type.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>To add a conditional Style:</p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <img
border=0 width=15 height=15 id="Picture 102"
src="Report_Design_Guide_files/image099.gif" alt="style_icon"> or <i><img
border=0 width=15 height=15 id="Picture 103"
src="Report_Design_Guide_files/image100.gif" alt="style_icon2"> </i>to access
the Condition Styles dialog box. In the example below, two styles have been
added and the Add a Condition panel opened to show the various options in the dialog
box.</p>


<p class=MsoNormal><img border=0   id="Picture 337"
src="Report_Design_Guide_files/image101.jpg"><br>
<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>Add
a Condition</b>. In the panel are the <i>Column</i>, <i>Operator</i>, <i>Value </i>and
<i>Style </i>attributes.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Choose a <i>Column</i>
to base the conditional styling on from the drop-down list.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Choose a
comparison <i>Operator</i> from the drop-down list.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>From the <i>Value</i>
type drop-down list, choose either <i>Specific Value,</i> <i>Pre-defined Date</i>,
or <i>Other Data Column</i>.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>6.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Specify a threshold
Value. If a Value type of:<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span><i>Specific
Value </i>was selected, then type in a value or click the <img border=0
  id="Picture 105" src="Report_Design_Guide_files/image053.gif"
alt=iconFind> icon to view and select a valid value from the database.</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span><i>Pre-defined
Date </i>was selected, select a token (i.e., Today).</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>c.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span><i>Other
Data Column</i> was selected, select a column from the drop-down list.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>7.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Choose a <i>Style</i>
from the drop-down list.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>8.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>OK</b>
to add the styling condition.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>9.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Add more
parameters by clicking <b>Add a Condition</b> and repeating the steps above.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>10.<span
style='font:7.0pt "Times New Roman"'> </span>OPTIONAL: Because styles are
applied based on the first condition that is satisfied, the order may be
significant. You can move a condition up or down in the list by clicking the <img
border=0   id="Picture 106"
src="Report_Design_Guide_files/image091.gif" alt=arrowUp> or <img border=0
width=15 height=9 id="Picture 107" src="Report_Design_Guide_files/image092.gif"
alt=arrowDown> icon.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>11.<span
style='font:7.0pt "Times New Roman"'> </span>OPTIONAL: Check <b><i>Add this
style to all columns</i></b> to apply the Style to all columns of a row in the
table.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>12.<span
style='font:7.0pt "Times New Roman"'> </span>Click <b>OK</b> to save the styling
condition(s) and return to the Column Configuration interface.<br>
<br>
</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>Pre-defined dates get evaluated at the time the
  report runs.<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>If the value is a number, the value field must
  contain a valid number to complete the comparison.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpLast>The <i><img border=0  
  id="Picture 108" src="Report_Design_Guide_files/image100.gif"
  alt="style_icon2"></i>  icon indicates that a least one conditional style
  exists for that particular column.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>




<p class=MsoNormal>To modify or remove a conditional Style:</p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click the <img
border=0 width=15 height=15 id="Picture 109"
src="Report_Design_Guide_files/image100.gif" alt="style_icon2"> icon to access
the Style Details panel for a specific column.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>From the
Style Details panel:<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>If modifying
a Style, click the <img border=0   id="Picture 110"
src="Report_Design_Guide_files/image054.gif" alt=mod-icon> icon associated to a
specific Style's condition. Modify the conditions as desired and then click <b>OK</b><span
style='font-size:10.0pt'> </span>to save the condition.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>If
removing a Style, click the <img border=0   id="Picture 111"
src="Report_Design_Guide_files/image088.gif" alt=remove> icon associated to a
specific Style's condition.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>c.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>OPTIONAL: Check
or uncheck <b><i>Add this style to all columns</i></b> to apply/remove the
Style to/from all columns in the table.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>OK</b>
to save the modifications and return to the Column Configuration interface.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>

<p class=MsoFooter><b>Adding a new custom column to the data table</b></p>


<p class=MsoFooter>From the Column Configuration page, a new column can be
added to the data table by clicking <b>Add Custom Column</b>. Columns created
in this manner can reference previously defined summary information. </p>


<p class=MsoFooter>In the example below, a total column (OrderTotal) has been
created using the<i> Quantity</i> column and the <i>Saleprice</i> calculated
column.</p>

<p class=MsoFooter><br>
<br>
</p>

<p class=MsoFooter><img border=0   id="Picture 339"
src="Report_Design_Guide_files/image102.jpg"></p>


<p class=MsoFooter>The sequence of events for this example was:</p>


<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click the <b>Add
Calculated Column</b> button on the <i>Column Configuration</i> page.<br>
<br>
</p>

<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click the <i>Saleprice</i>
column from the <i>Calculate Columns</i> data object.</p>

<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click the multiplication
symbol from the list of operators.</p>

<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click the <i>Quantity</i>
column from the OrderDetails object.</p>

<p class=MsoFooter style='margin-left:.5in;text-indent:-.25in'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>OK</b>
to save the result</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b> <br>
  This type of calculated column may use reporting summary information in the
  calculated column definition.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>If a data table contains a calculated column, the
  configuration grid will display an <b>Actions</b> column. Clicking on the <img
  border=0   id="Picture 113"
  src="Report_Design_Guide_files/image103.jpg"> icon allows the column to be
  edited.<br clear=all style='page-break-before:always'>
  </p>
  </div>
  </td>
 </tr>
</table>

</div>

<h4><a name="_Toc382291607">&nbsp;</a></h4>

<h4>&nbsp;</h4>

<h4>&nbsp;</h4>

<h4>&nbsp;</h4>

</body>
</html>
