<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945103">Sorting Data</a></h2>


<p class=MsoNormal>Data returned from the database may have a default sort
order specified. This will determine the initial display order of the data for
tabular reports and the initial population of crosstab reports. However, that
sort order can be overridden when the report is executed if the column sort
option is enabled.</p>


<p class=MsoNormal>To set the initial sort order of the data, click <b>Select
Data</b> to display the <i>Select Data </i>dialog box. Click the <i>Sort</i>
tab to display:</p>


<p class=MsoNormal><img   id="Picture 311"
src="Report_Design_Guide_files/image047.jpg"></p>


<p class=MsoNormal>The sort order has to be set column-by-column. </p>



<p class=MsoNormal>Click <b>Add a Column</b> to create a sorting level:<br>
<br>
</p>


<p class=MsoNormal><img   id="Picture 312"
src="Report_Design_Guide_files/image048.jpg"></p>

<p class=MsoNormal>Select a column from the <i>Column</i> drop-down list and select
the <i>Direction</i> (Ascending or Descending). Repeat the process for each
columns to be used to set the sort sequence of the data.</p>


<p class=MsoNormal>To remove a column from the sort sequence, click the <img
  id="Picture 44" src="Report_Design_Guide_files/image049.jpg"> icon
for that column.</p>


<p class=MsoNormal>If the <i>Column</i> order in the list needs to be adjusted,
rearrange it by clicking the  <img   id="Picture 45"
src="Report_Design_Guide_files/image050.gif" alt=dragHandle>  drag handle and
dragging the column s row up or down.</p>



</body>
</html>
