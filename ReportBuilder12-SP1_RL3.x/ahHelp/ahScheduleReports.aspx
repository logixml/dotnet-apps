<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945135"></a><a name="_Toc382291630">Scheduling Reports</a></h2>


<p class=MsoNormal>The scheduling process is flexible and easy, and offers the
ability to deliver reports via email as attachments in a variety or formats or
as a link.</p>


<p class=MsoNormal>Scheduling reports is typically a two-step process; <br>
<br>
</p>

<p class=MsoNormal style='text-indent:.5in'>1) Specifiy when the report is
scheduled to run.</p>

<p class=MsoNormal style='text-indent:.5in'>2) Identify those who should
receive it (subscribers). </p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #FFFFCC 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#FFFFCC'>
  <p class=HintCxSpFirst><b>Hint:</b></p>
  <p class=HintCxSpLast>If the option to schedule reports is not displayed, contact
  the system administrator. The ability to schedule reports is controlled by
  the role(s) assigned to the user.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<p class=MsoNormal>To schedule a report, hover your mouse cursor over the <b>More</b>
button and select <i>Schedule</i> from its drop-down list. </p>


<p class=MsoNormal>If the report has no schedules associated with it, the
Schedules page will appear as:</p>



<p class=MsoNormal><img border=0   id="Picture 256"
src="Report_Design_Guide_files/image200.jpg"></p>



<p class=MsoNormal>If a schedule already exists for the report, the <i>Schedules</i>
page will be displayed:</p>


<p class=MsoNormal><img border=0   id="Picture 257"
src="Report_Design_Guide_files/image201.jpg"></p>


<p class=MsoNormal><br>
Click <b>Add</b> to create a new schedule for the report.</p>


<p class=MsoNormal>Click <b>Delete</b> to remove selected schedules for the
report. Schedules can be selected by checking their checkbox.</p>


<p class=MsoNormal>The Schedules list can be sorted by clicking the <i>Frequency</i>
or <i>Schedule</i> column headers.</p>


<p class=MsoNormal>The three actions available for a schedule are <i>Modify
Schedule</i>, <i>Change Subscription,</i> and <i>Run Schedule.</i> The <i>Run
Schedule</i> option will not be displayed until subscribers have been added to
the schedule or if the report has expired.</p>


<p class=MsoNormal>Hover your mouse cursor over the <img border=0 
 id="Picture 258" src="Report_Design_Guide_files/image202.jpg"> icon
and select the appropriate action from the list. </p>



<p class=MsoNormal>Some or all of the following options are available when
scheduling a new report:</p>


<p class=MsoNormal><img border=0   id="Picture 259"
src="Report_Design_Guide_files/image203.jpg"></p>


<p class=MsoNormal><br>
The <i>Output Format</i> determines how the report is delivered in the email.
HTML reports are embedded in the email and PDF, Word, Excel and CSV reports are
sent as attachments.</p>


<p class=MsoNormal>Reports can be added to the archive whenever the scheduler
delivers the report. If the report is added to the archive, the email contains
a link to the directory containing the archives. The report is not embedded or
sent as an attachment but rather saved to the archive directory.</p>


<p class=MsoNormal>The <i>Add to Archive</i> checkbox is only visible when
archiving is enabled by the System Administrator. If the <i>Add to Archive</i>
checkbox is checked, the <i>Output Format</i> will be set to the default
archive format specified by the System Administrator and the <i>Output Format</i>
list will be disabled. </p>


<p class=MsoNormal>The following options are displayed in the <i>Schedule Task</i>
list (with the default set to <i>Daily</i>):</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Once</li>
 <li class=MsoNormal>Daily</li>
 <li class=MsoNormal>Weekly</li>
 <li class=MsoNormal>Monthly</li>
</ul>


<p class=MsoNormal>As these options are selected, the following gray dialog boxes
will either be hidden or will display options related to the scheduling option
selected. If  Once  is selected, it s hidden. </p>



<p class=MsoNormal><img border=0   id="Picture 260"
src="Report_Design_Guide_files/image204.gif" alt=schedDaily></p>


<p class=MsoNormal><img border=0   id="Picture 261"
src="Report_Design_Guide_files/image205.gif" alt=schedWeekly></p>


<p class=MsoNormal><span style='font-size:10.0pt'><img border=0 
 id="Picture 262" src="Report_Design_Guide_files/image206.gif"
alt=schedMonthly></span></p>



<p class=MsoNormal>The <i>Start Time</i> specifies the time when the scheduler
will run the report. The hour and minute options are displayed as drop-down
lists.</p>


<p class=MsoNormal>The <i>Start Date</i> specifies the initial date when the
scheduler will run the report. The date may be manually keyed in or populated
from the calendar control by clicking on the <img border=0  
id="Picture 263" src="Report_Design_Guide_files/image207.gif"
alt=calendar-icon> icon. </p>


<p class=MsoNormal>The <i>End Date</i> checkbox allows the user to let the
report run indefinitely (if left unchecked). If checked, the option to specify
a date to terminate report execution is displayed. Specification of the <i>End
Date</i> is identical to the process for the <i>Start Date</i>. </p>


<p class=MsoNormal>The <i>Repeat Task</i> option allows a report to be run
repeatedly, beginning at the <i>Start Time</i> and <i>Start Date</i> specified
earlier. If the <i>Repeat Task</i> option is checked, the <i>Interval</i> and <i>Duration</i>
for running the report may be specified. The <i>Interval</i> determines the
frequency and the <i>Duration</i> determines  for how long . Both values must
be greater than 0, and the <i>Interval</i> value must be less than the <i>Duration</i>
value. </p>


<p class=MsoNormal>The initial scheduling page example shown was for a report
that did not require any user input. The following example is for a report that
expects user input when it s run:</p>


<p class=MsoNormal><img border=0   id="Picture 264"
src="Report_Design_Guide_files/image208.jpg"></p>



<p class=MsoNormal>Since a scheduled report is run  unattended , parameter
values that would normally be supplied by the user (called  Ask  parameters)
must be set at the time scheduling is established. The scheduling page is
adjusted to allow the specification of the parameter values to be used when the
scheduled report is run. Notice at the bottom of the schedule page a section
labeled <i>Report Input Parameters</i> is displayed.</p>


<p class=MsoNormal>Reports may have multiple parameters defined. Every
parameter requiring user input must have values set. To set the values for a
parameter, click the  <img border=0   id="Picture 265"
src="Report_Design_Guide_files/image071.gif" alt=iconAction> icon and a <i>Parameter
Details</i> dialog box will be displayed.</p>


<p class=MsoNormal><img border=0   id="Picture 39"
src="Report_Design_Guide_files/image209.jpg"></p>


<p class=MsoNormal>Clicking the  helper  icons <img border=0 
 id="Picture 267" src="Report_Design_Guide_files/image210.gif"
alt=view-icon> and  <img border=0   id="Picture 268"
src="Report_Design_Guide_files/image207.gif" alt=calendar-icon>   (when the
parameter is date-based) may assist in selecting the values. Click <b>Save
Parameter</b> to save the values to be used for the associated parameter when
the report is run.</p>


<p class=MsoNormal>When the scheduling options have been completed, click <b>Save</b>
at the bottom of the page to save the information and create the scheduled
task.</p>


<p class=MsoNormal>If the <i>Subscribed Users</i> list is not displayed in the
schedule page, then no users have elected to receive the report.</p>



<p class=MsoNormal><img border=0   id="Picture 269"
src="Report_Design_Guide_files/image211.jpg"></p>

<p class=MsoNormal><span style='font-size:10.0pt'>Subscribed users are listed
at the bottom of the schedule page.</span></p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #FFFFCC 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#FFFFCC'>
  <p class=HintCxSpFirst><b>Hint:</b></p>
  <p class=HintCxSpLast>Test that the scheduled report is delivered as expected
  by first subscribing yourself and then running the report from the Scheduled
  Reports grid. The speed of delivery depends on the size of the report and the
  local network configuration.</p>
  </div>
  </td>
 </tr>
</table>

</div>






</body>
</html>
