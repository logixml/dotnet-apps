<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221788"></a><a name="_Toc457221152">Data Formats</a></h2>


<p class=MsoNormal><span style=''>The <i>Data
Formats</i> page allows the System Administrator to manage data formats for use
when specifying default column formats, or when building reports. By default, Ad
Hoc is installed with 22 pre-defined data formats which are listed in the <i>Data
Formats</i> page in a green font.</span></p>


<p class=MsoNormal><span style=''>Data formats
are available to end-users when specifying a column format in the Report Builder.
The list of format options will be limited to those that apply to the data type
of the column. The presentation order of the data format in the drop-down list
matches the order on the Data Formats page. </span></p>


<p class=MsoNormal><span style=''>Select <b>Data
Formats</b> from the <i>Report Configuration</i> drop-down list<b> </b>to
display the <i>Data Formats</i> configuration page:</span></p>


<p class=MsoNormal><img border=0   id="Picture 359"
src="System_Admin_Guide_files/image087.jpg"></p>



<p class=MsoNormal><span style=''>Click <b>Add</b>
to display an empty <i>Data Format dialog box.</i></span></p>


<p class=MsoNormal><span style=''>Click <b>Delete</b>
to remove the selected user-defined data formats. Data Formats are selected by checking
their checkboxes. The standard data formats distributed with Ad Hoc, displayed
in green, cannot be deleted.</span></p>


<p class=MsoNormal><span style=''>A search
control may be displayed for the <i>Data Formats</i> page. Enter the search criteria
in the textbox and click <b>Find Data Formats</b>. The search feature will look
for formats based on a  contains -type test. To clear the search criteria click
the </span><img border=0   id="Picture 361"
src="System_Admin_Guide_files/image088.gif"><span style=''> icon
in the text box. The availability of the search feature is configurable, so it
may not appear.</span></p>


<p class=MsoNormal><span style=''>The
presentation order of data formats in various lists follows the order of the
data formats in the grid. The grid order can be adjusted by either using the
drag-and-drop methods available or selecting a data format with the checkbox
and clicking on the </span><span style=''><img
border=0   id="Picture 129"
src="System_Admin_Guide_files/image089.jpg"></span><span style='
"Arial","sans-serif"'> or </span><span style=''><img
border=0 width=17 height=14 id="Picture 130"
src="System_Admin_Guide_files/image090.jpg"></span><span style='
"Arial","sans-serif"'> arrows.</span></p>


<p class=MsoNormal><span style=''>The  </span><span
style=''><img border=0  
id="Picture 131" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon indicates that more than one
action can be performed on the data format. Hover your mouse cursor over the  </span><span
style=''><img border=0  
id="Picture 132" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon to display the available actions
for a data format, which are <b>Modify Data Format</b> and <b>View Dependencies</b>.</span></p>


<h3><a name="_Toc457221789"></a><a name="_Toc457221153">Adding a Data Format</a></h3>

<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<p class=MsoNormal><span style=''>To create a
user-defined data format, click <b>Add</b>. The following dialog box will be displayed:</span></p>


<p class=MsoNormal><img border=0   id="Picture 362"
src="System_Admin_Guide_files/image091.jpg"></p>


<p class=MsoNormal><span style=''>The <i>Format
Name</i> value will be displayed in the format lists available to the end-user.</span></p>


<p class=MsoNormal><span style=''>The <i>Format</i>
is the formatting character string. Refer to our </span><a
href="http://devnet.logixml.com/rdPage.aspx?rdReport=Article&amp;dnDocID=2073"><span
style=''>Formatting Data</span></a><span
style=''> document for additional details about
this string.</span></p>


<p class=MsoNormal><span style=''>Select one or
more <i>Applies to</i> options. These identify the generic data types of the
columns that the new data format might be applied to.</span></p>


<p class=MsoNormal><span style=''>Check the <i>Is
Available</i> checkbox to specify the visibility of the data format.</span></p>


<p class=MsoNormal><span style=''>The <i>Explanation</i>
is optional.</span></p>


<p class=MsoNormal><span style=''>Click <b>OK</b>
to save the data format in the metadata database. The data format will be
available after the next login. Existing sessions will be unaffected.</span></p>




<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<h3><a name="_Toc457221790"></a><a name="_Toc457221154">Modifying a Data Format</a></h3>


<p class=MsoNormal><span style=''>Hover your
mouse cursor over the  </span><span style=''><img
border=0   id="Picture 134"
src="System_Admin_Guide_files/image026.jpg"></span><span style='
"Arial","sans-serif"'> icon for a data format and select the <b>Modify Data Format</b>
option from the list. In the dialog box displayed, make the necessary changes
and click <b>OK</b> to save the revised data format in the metadata database.</span></p>


<p class=MsoNormal><span style=''>Reports using
the old data format will have to be re-saved to acquire the revised format.</span></p>


<h3><a name="_Toc457221791"></a><a name="_Toc457221155">Viewing Dependencies</a></h3>


<p class=MsoNormal><span style=''>Hover your
mouse cursor over the  </span><span style=''><img
border=0   id="Picture 135"
src="System_Admin_Guide_files/image026.jpg"></span><span style='
"Arial","sans-serif"'> icon for a presentation style and select the <b>View Dependencies</b>
option from the list.</span></p>


<p class=MsoNormal><span style=''>A report page
identifying the scope and usage of the data format will be displayed. Administrators
should exercise this option before modifying or deleting a data format in order
to determine the impact of changes.</span></p>

</body>
</html>
