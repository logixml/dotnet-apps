<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221794"></a><a name="_Toc457221158">Report Settings</a> </h2>


<p class=MsoNormal><span style=''>The <i>Report
Settings</i> page allows the System Administrator to optimize report performance
and appearance. Some settings determine the behavior of the application and
take effect immediately, while others may only work for new or re-built
reports.</span></p>


<p class=MsoNormal><span style=''>Select <b>Report
Settings</b> from the <i>Report Configuration</i> drop-down list<b> </b>to
display the <i>Report Settings</i> configuration page. </span></p>


<p class=MsoNormal><span style=''>The following
is an example of the page when the <i>Active SQL</i> feature is enabled. See
the <i>Special Application Settings</i> section of this document for more
information about this feature.</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><img border=0   id="Picture 348"
src="System_Admin_Guide_files/image096.jpg"></p>



<p class=MsoNormal><span style=''>Click the </span><span
style=''><img border=0  
id="Picture 143" src="System_Admin_Guide_files/image097.jpg"></span><span
style=''>icon to display brief help text for
each property.</span></p>


<p class=MsoNormal><i><span style=''>Design-time
Validation</span></i><span style=''>  - This
feature is built into several steps of the Report Builder to ensure that
invalid values and bad syntax do not affect reports at runtime. Design-time
validation improves usability by informing users of potential problems before
they occur. Administrators can disable this functionality to improve the performance
of the Report Builder but we do not recommend it.</span></p>


<p class=MsoNormal><i><span style=''>Apply data
format to Excel exports</span></i><span style=''>
 - This controls the formatting of columns when a report is exported to Excel.
The following values can be selected from the drop-down:<br>
<br>
</span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><b><span style=''>Always</span></b><span
     style=''>: The Excel columns will be
     formatted the same as the columns  data types.</span></li>
 <li class=MsoNormal><b><span style=''>Exclude
     Dates</span></b><span style=''>: All
     columns will be formatted with the exception of Date/Time columns</span></li>
 <li class=MsoNormal><b><span style=''>Never</span></b><span
     style=''>: All columns will be formatted
     as string   no type-specific formatting will be used.</span></li>
</ul>


<p class=MsoNormal><i><span style=''>Excel
Output Format</span></i><span style=''>  - Specifies
the output format of the Excel file created. The extension for Excel 2003 files
is </span><span style='font-size:11.0pt;'>.xls</span><span
style=''> and for Excel 2007+ files it s </span><span
style='font-size:11.0pt;'>.xlsx</span><span
style=''>.</span></p>


<p class=MsoNormal><i><span style=''>Max List
Records<sup>1</sup></span></i><span style=''>  
Specifies the maximum number of records returned when using <b>In list</b> report
parameters. We recommend keeping this number as small as possible to improve
performance and keep drop-down lists manageable. Enter <b>0 </b>(zero) to
disable this feature. The maximum value is 64,000.</span></p>


<p class=MsoNormal><i><span style=''>Default
rows per report page</span></i><span style=''>  
Specifies the default number of rows shown in the first layer of each report.
From the Report Builder, the end-user has the option of overriding the default
value in the Table Settings panel.</span></p>


<p class=MsoNormal><i><span style=''>Default rows
per subreport page</span></i><span style=''>  
Specifies the default number of rows shown in the drill-down layers of grouped
reports. From the Report Builder, the end-user has the option of overriding the
default value in the Table Settings panel.</span></p>


<p class=MsoNormal><i><span style=''>Default
printable page size</span></i><span style=''>  -
Specifies the default page size selected for printable paging in reports.</span></p>


<p class=MsoNormal><i><span style=''>Default
Template</span></i><span style=''>   Specifies
the default style sheet for each new report. This is also the style sheet that
will be used to display and verify any new Presentation Styles.</span></p>


<p class=MsoNormal><span style=''>Click <b>Save
</b>to commit any changes.</span></p>


<p class=MsoNormal><span style=''>The following
is an example of the page when the <i>Active SQL</i> feature is disabled. See
the <i>Special Application Settings</i> section of this document for more
information about this feature. Only the attributes that haven t been
previously discussed section are explained.</span></p>




<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><img border=0   id="Picture 365"
src="System_Admin_Guide_files/image098.jpg"></p>

<p class=MsoNormal><i><span style=''>Max
Records -</span></i><span style=''> Specifies
the maximum number of records returned for a report. The number specified applies
to reports and sub-reports. Database performance is improved, but users may not
see all the results in a report. Enter <b>0</b> (zero) to disable this feature.
The maximum number is 64,000.</span></p>


<p class=MsoNormal><i><span style=''>Show
message if maximum records reached</span></i><span style=''>
 - S</span><span style=''>pecifies whether or
not a warning will be displayed in the report when more rows are returned than
is allowed by the <i>Max Records</i> limit.</span></p>


<p class=MsoNormal><i><span style=''>Show
message if no records are returned</span></i><span style=''>
- S</span><span style=''>pecifies whether or
not a warning will be displayed in the report when no rows are returned.</span></p>


<p class=MsoNormal><span style=''>If the end-user
chooses to show number of records returned in a data section, the <i>Row count
message</i> will be displayed, with [row count] replaced with actual number of
records.</span></p>


<p class=MsoNormal><i><span style=''>Row count
message appearance</span></i><span style=''>
will apply the selected stylesheet class to the message.</span></p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
