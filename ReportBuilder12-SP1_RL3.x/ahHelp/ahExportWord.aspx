<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945125"></a><a name="_Toc382291621">Export to Word</a></h2>


<p class=MsoNormal><i>Export to Word</i> downloads the report into a Microsoft
Word document. A temporary name is given to the file, with the Word file
extension (.docx).</p>



<p class=MsoNormal><img border=0   id="Picture 229"
src="Report_Design_Guide_files/image182.jpg" alt=w></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The report is exported to
Microsoft Word format.</span></p>

<p class=MsoNormal>If Microsoft Word is installed, the report opens as a Word
document. Edit the report as a Word document or save the report unedited.</p>


<p class=MsoNormal>Save the report in Word format by clicking the File menu and
choosing <b>Save as</b>. Choose a name and location for the file and click <b>Save</b>.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst>Notes:</p>
  <p class=NotesCxSpMiddle>Reports containing drill-down groupings will not be
  expanded when exported.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>Due to a MS-Word limitation, if a report contains more
  than 50 columns, then only the first 50 columns will be exported.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #FFFFCC 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#FFFFCC'>
  <p class=HintCxSpFirst>Hint:</p>
  <p class=HintCxSpLast>Click View &gt; Toolbars to add standard Word toolbars
  to the browser window.</p>
  </div>
  </td>
 </tr>
</table>

</div>




</body>
</html>
