<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945127"></a><a name="_Toc382291623">Export to CSV</a></h2>



<p class=MsoNormal><i>Export to CSV</i> opens a new browser window and displays
the report in comma-separated values within a spreadsheet (comma-delimited text
file).<br>
<br>
</p>

<p class=MsoNormal><img border=0   id="Picture 232"
src="Report_Design_Guide_files/image184.gif" alt=csvReport></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The report is exported to CSV
format and viewed by the Microsoft Excel browser plug-in.</span></p>

<p class=MsoNormal>Save the report in CSV format by clicking the File menu and
choosing <b>Save as</b>. Choose the filename and location and click <b>Save</b>.
The default file type is <b>CSV </b>(comma delimited).</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Reports containing drill-down groupings will not be
  expanded when exported.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>




<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #FFFFCC 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#FFFFCC'>
  <p class=HintCxSpFirst><b>Hint:</b></p>
  <p class=HintCxSpLast>The CSV format is viewable by a variety of
  applications. It is best viewed by spreadsheet, database, and text-editing
  applications.</p>
  </div>
  </td>
 </tr>
</table>

</div>



<h4><a name="_Toc382291624">&nbsp;</a></h4>

</body>
</html>
