<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="AdminMainMenu.ascx.vb" Inherits="LogiAdHoc.ahControls_AdminMainMenu" %>
<span id="functions">
    <asp:HyperLink ID="About" runat="server"></asp:HyperLink>
    <span class="separator">|</span>
    <%--<asp:HyperLink ID="Help" runat="server"></asp:HyperLink>
    <span class="separator">|</span>--%>
    <asp:HyperLink ID="Logout" runat="server"></asp:HyperLink>
</span>
<div id="menu">
    <table id="tbMenu" runat="server">
        <tr id="trMenu" runat="server">
            <td>
                <asp:HyperLink ID="SiteLogo" runat="server" />
            </td>
            <td>
                <span id="links">
                    <asp:HyperLink ID="Administration" runat="server"></asp:HyperLink>
                </span>
            </td>
            <td width="20">&nbsp;</td>
        </tr>
    </table>
</div>
