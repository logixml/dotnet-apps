<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Relationship.aspx.vb"
    Inherits="LogiAdHoc.ahConfiguration_Relationship" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="~/ahControls/PagingControl.ascx" %>  
<%@ Register TagPrefix="AdHoc" TagName="RecompileGrid" Src="~/ahControls/RecompileReportGrid.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Relationship</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>

    <%--<script type="text/javascript">
    </script>--%>
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <asp:UpdatePanel ID="UPBct" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="Relationship" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlParentID" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
        }
        </script>

        <div class="divForm">
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
            <input type="hidden" id="ahParentID" name="ahParentID" runat="server" />                 
            <input type="hidden" id="ahRecompiledFlag" name="ahRecompiledFlag" runat="server" />
        
            <asp:UpdatePanel ID="UpdatePanel1" runat="Server" RenderMode="Inline">
                <ContentTemplate>
                    
                    <table id="tbDBConn" runat="server" class="tbTB">
                        <tr>
                            <td width="125px">
                                <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:LogiAdHoc, DatabaseConnection %>"></asp:Localize>
                            </td>
                            <td>
                                <asp:Label ID="lblDBConn" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table id="tbParent" runat="server" class="tbTB">
                        <tr>
                            <td width="125px">
                                <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource1" Text="Selected Relationship:"></asp:Localize></td>
                            <td>
                                <asp:DropDownList ID="ddlParentID" AutoPostBack="True" runat="server" meta:resourcekey="ddlParentIDResource1" />
                            </td>
                        </tr>
                    </table>
                    <table class="tbTB">
                        <tr>
                            <td width="125px">
                                <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2" Text="Relation Label:"></asp:Localize></td>
                            <td>
                                <input id="RelationName" type="text" maxlength="100" style="width: 300px" runat="server"><asp:RequiredFieldValidator
                                    ID="rtvRelationName" runat="server" ErrorMessage="Relation Label is required."
                                    ControlToValidate="RelationName" meta:resourcekey="rtvRelationNameResource1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <%--<tr>
                            <td width="125">
                                <label for="ddlDatabase1">
                                    <asp:Localize ID="Localize7" runat="server" meta:resourcekey="LiteralResource3" Text="Main Database:"></asp:Localize></label></td>
                            <td>
                                <asp:DropDownList ID="ddlDatabase1" AutoPostBack="True" OnSelectedIndexChanged="ddlDatabase1_SelectedIndexChanged"
                                    runat="server" meta:resourcekey="Object1Resource1" />
                            </td>
                        </tr>--%>
                        <tr>
                            <td width="125">
                                <label for="Object1">
                                    <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3" Text="Main Data Object:"></asp:Localize></label></td>
                            <td>
                                <asp:DropDownList ID="Object1" AutoPostBack="True" OnSelectedIndexChanged="Object1_SelectedIndexChanged"
                                    runat="server" meta:resourcekey="Object1Resource1" />
                            </td>
                        </tr>
                        <tr>
                            <td width="125">
                                <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource5" Text="Join Type:"></asp:Localize></td>
                            <td>
                                <asp:DropDownList ID="Relation" runat="server" meta:resourcekey="RelationResource1">
                                    <asp:ListItem Value="Inner Join" meta:resourcekey="ListItemResource1">Inner Join</asp:ListItem>
                                    <asp:ListItem Value="Left Outer Join" meta:resourcekey="ListItemResource2">Left Outer Join</asp:ListItem>
                                    <asp:ListItem Value="Right Outer Join" meta:resourcekey="ListItemResource3">Right Outer Join</asp:ListItem>
                                </asp:DropDownList>
                                <asp:CustomValidator ID="cvValidRelation" runat="server" EnableClientScript="False"
                                    ErrorMessage="A similar relationship already exists." ControlToValidate="RelationName"
                                    OnServerValidate="IsRelationValid" meta:resourcekey="cvValidRelationResource1">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <%--<tr>
                            <td width="125">
                                <asp:Localize ID="Localize13" runat="server" meta:resourcekey="LiteralResource6" Text="Joined Database:"></asp:Localize></td>
                            <td>
                                <asp:DropDownList ID="ddlDatabase2" AutoPostBack="True" OnSelectedIndexChanged="ddlDatabase2_SelectedIndexChanged"
                                    runat="server" meta:resourcekey="Object2Resource1" />
                            </td>
                        </tr>--%>
                        <tr>
                            <td width="125">
                                <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource6" Text="Joined Data Object:"></asp:Localize></td>
                            <td>
                                <asp:DropDownList ID="Object2" AutoPostBack="True" OnSelectedIndexChanged="Object2_SelectedIndexChanged"
                                    runat="server" meta:resourcekey="Object2Resource1" />
                            </td>
                        </tr>
                        <tr>
                            <td width="125">
                                <label for="AutoReverse">
                                    <asp:Localize ID="Localize8" runat="server" meta:resourcekey="LiteralResource8" Text="Auto Reverse:"></asp:Localize></label></td>
                            <td>
                                <asp:CheckBox ID="AutoReverse" runat="server" />
                            </td>
                        </tr>
<%--                        <tr id="trAutomaticJoin" runat="server">
                            <td width="125">
                                <label for="Automatic">
                                    <asp:Localize ID="Localize8" runat="server" meta:resourcekey="LiteralResource8" Text="Automatic:"></asp:Localize></label></td>
                            <td>
                                <asp:CheckBox ID="Automatic" runat="server" meta:resourcekey="AutomaticResource1" />
                            </td>
                        </tr>
--%>                        
                        <tr>
                            <td width="125px">
                                <asp:Localize ID="Localize9" runat="server" meta:resourcekey="LiteralResource9" Text="Joined Data Object Modified Label:"></asp:Localize></td>
                            <td>
                                <input id="ObjectLabel2" type="text" maxlength="100" style="width: 300px" runat="server" />
                                <asp:CustomValidator ID="cvJoinedObjectLabel" runat="server" ControlToValidate="ObjectLabel2"
                                    EnableClientScript="False" ErrorMessage="Joined Data Object Modified Label cannot contain some special characters."
                                    meta:resourcekey="cvJoinedObjectLabelResource1" OnServerValidate="IsObjectLabelValid">*</asp:CustomValidator>
                            </td>
                            <td>
                                <img id="help1" runat="server" src="../ahImages/iconHelp.gif" />
                                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" TargetControlID="help1"
                                    PopupControlID="pnlHelp1" Position="Bottom" />
                            </td>
                        </tr>
                        <tr>
                            <td width="125px">
                                <asp:Localize ID="Localize10" runat="server" meta:resourcekey="LiteralResource10"
                                    Text="Main Data Object Modified Label:"></asp:Localize></td>
                            <td>
                                <input id="ObjectLabel1" type="text" maxlength="100" style="width: 300px" runat="server" />
                                <asp:CustomValidator ID="cvMainObjectLabel" runat="server" ControlToValidate="ObjectLabel1"
                                    EnableClientScript="False" ErrorMessage="Main Data Object Modified Label cannot contain some special characters."
                                    meta:resourcekey="cvMainObjectLabelResource1" OnServerValidate="IsObjectLabelValid">*</asp:CustomValidator>
                            </td>
                            <td>
                                <img id="help2" runat="server" src="../ahImages/iconHelp.gif" />
                                <ajaxToolkit:PopupControlExtender ID="PopupControlExtender2" runat="server" TargetControlID="help2"
                                    PopupControlID="pnlHelp2" Position="Bottom" />
                            </td>
                        </tr>
                    </table>
                    <br />
                    
                    <br />
                    <table><tr><td>
                    <div id="data_main">
                        <div id="activities">
                            <AdHoc:LogiButton ID="btnNewRelation" OnClick="NewRelation" CausesValidation="false"
                                Text="<%$ Resources:LogiAdHoc, AddWithSpaces %>" runat="server" />
                            <AdHoc:LogiButton ID="btnRemoveRelation" OnClick="RemoveRelation" CausesValidation="false"
                                Text="<%$ Resources:LogiAdHoc, Delete %>" runat="server" />
                        </div>
                        <asp:GridView ID="grdMain" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            CssClass="gridDetail" DataKeyNames="JoinRelationID" OnRowDataBound="OnItemDataBoundHandler">
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                    <HeaderStyle Width="30px" />
                                    <HeaderTemplate>
                                        <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                        <asp:CheckBox ID="CheckAll" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" />
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                    <ItemTemplate>
                                        <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                        <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>"></asp:CheckBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField HeaderText="Main Data Object Column" DataField="Column1" meta:resourcekey="BoundFieldResource1" />
                                <asp:BoundField HeaderText="Joined Data Object Column" DataField="Column2" meta:resourcekey="BoundFieldResource2" />--%>
                                <asp:TemplateField HeaderText="Main Data Object Column" meta:resourcekey="BoundFieldResource1">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="lstColumn1" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Joined Data Object Column" meta:resourcekey="BoundFieldResource2">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="lstColumn2" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="Modify" ImageUrl="../ahImages/modify.gif" AlternateText="Modify Relationship"
                                        runat="server" OnCommand="ModifyRelationship" CausesValidation="false" meta:resourcekey="ModifyResource1" />
                                </ItemTemplate><ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </asp:TemplateField>--%>
                            </Columns>
                            <PagerTemplate>
                                <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                            </PagerTemplate>
                            <PagerStyle HorizontalAlign="Center" />
                            <HeaderStyle CssClass="gridheader" />
                            <RowStyle CssClass="gridrow" />
                            <AlternatingRowStyle CssClass="gridalternaterow" />
                        </asp:GridView>  
                    </div>   
                    </td></tr></table>        
                    <div id="divWarning" runat="server" class="warning">
                        <asp:Localize ID="lcModifyRelationshipWarning" runat="server" Text="<%$ Resources:LogiAdHoc, ModifyRelationshipWarning %>" />
                    </div>   
                    <div id="divButtons" class="divButtons">
                        <AdHoc:LogiButton ID="btnSave" runat="server" OnClick="SaveRelation"
                            ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" Text="<%$ Resources:LogiAdHoc, Save %>"
                            UseSubmitBehavior="false" />
                        <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="CancelRelation"
                            ToolTip="Click to cancel your unsaved changes and return to the previous page."
                            Text="Back to Relationships" CausesValidation="False" meta:resourcekey="btnCancelResource1" />
                        <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1" />
                    </div>
                    <%--<asp:Panel ID="pnlRelationDetails" CssClass="detailpanel" runat="server" Width="280px">
                        <h2 id="hdr" runat="server">
                            <asp:Localize ID="Localize14" runat="server" meta:resourcekey="LiteralResource14" Text="Related Columns"></asp:Localize></h3>
                        </h2>
                        <table width="100%">
                            <tr>
                                <td width="90">
                                    <asp:Localize ID="Localize7" runat="server" meta:resourcekey="LiteralResource4" Text="Main Data Object Column:"></asp:Localize></td>
                                <td>
                                    <asp:DropDownList ID="lstColumn1" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Localize ID="Localize13" runat="server" meta:resourcekey="LiteralResource7" Text="Joined Data Object Column:"></asp:Localize></td>
                                <td>
                                    <asp:DropDownList ID="lstColumn2" runat="server" />
                                </td>
                            </tr>
                        </table>
                        <p>
                        </p>
                        <!-- Buttons -->
                        <table>
                            <tr>
                                <td colspan="2">
                                    <asp:Button ID="btnSaveRelation" OnClick="pnlSaveRelation_OnClick" runat="server" CssClass="command"
                                        Text="Save Relation" CausesValidation="False" meta:resourcekey="btnSaveRelationResource1" />
                                    <asp:Button ID="btnCancelRelation" OnClick="pnlCancelRelation_OnClick" runat="server" CssClass="command"
                                        Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" />
                                </td>
                            </tr>
                        </table>
                        <ul class="validation_error" id="ErrorList" runat="server">
                            <li>
                                <asp:Label ID="lblValidationMessage" runat="server"></asp:Label>
                            </li>
                        </ul>
                    </asp:Panel>--%>
                    
                    
                    <asp:Button runat="server" ID="Button2" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopupRecompile" BehaviorID="ahModalPopupBehaviorRecompile"
                        TargetControlID="Button2" PopupControlID="ahPopupRecompile" BackgroundCssClass="modalBackground"
                        DropShadow="false" PopupDragHandleControlID="pnlDragHandleRecompile" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopupRecompile" style="display:none; width:750;">
                        <asp:Panel ID="pnlDragHandleRecompile" runat="server" Style="cursor: hand;">
                            <div class="modalPopupHandle">
                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:LogiAdHoc, ParameterDetails %>"></asp:Localize>
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="imgClosePopupRecompile" runat="server" 
                                            OnClick="imgClosePopupRecompile_Click" CausesValidation="false"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                        </asp:Panel>
                        <div class="modalDiv">
                            <asp:UpdatePanel ID="upPopupRecompile2" runat="server">
                                <ContentTemplate>
                                    The following reports will be affected by this change, would you like to continue?
                                    <br /><br />
                                    
                                    <AdHoc:RecompileGrid runat="server" ID="grdRecompile" />
                                    <br /><br />
                                    
                                    
                                    <AdHoc:LogiButton ID="btnRecompile" runat="server" CausesValidation="False"
                                        UseSubmitBehavior="false" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" OnClientClick="javascript: SetRebuilt();"
                                        Text="Recompile Selected" OnClick="btnRecompile_Click" />
                                    <AdHoc:LogiButton ID="btnCancelRecompile" runat="server" 
                                        ToolTip="Cancel" Text="Cancel"
                                        CausesValidation="False" OnClick="btnCancelRecompile_Click" />
                                
                                    
                                    <AdHoc:LogiButton ID="btnCloseRecompile" runat="server" Visible="false"
                                        ToolTip="close" Text="Close"
                                        CausesValidation="False" OnClick="btnCancelRecompile_Click" />
                                
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                    
                    
                    <div id="divSaveAnimation">
                        <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                            TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                            DropShadow="False">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                            <table><tr><td>
                            <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                            </td>
                            <td valign="middle">
                            <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                            </td></tr></table>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <%--<asp:AsyncPostBackTrigger ControlID="ddlParentID" EventName="SelectedIndexChanged" />
                    <asp:PostBackTrigger ControlID="btnCancel" />                                             --%>
                </Triggers>
            </asp:UpdatePanel>
            <asp:Panel ID="pnlHelp1" runat="server" CssClass="popupControl" Width="300px">
                <asp:Localize ID="Localize11" runat="server" meta:resourcekey="LiteralResource11">
                You may choose to show a modified label for your <b>Joined Data Object</b> 
                when it is presented as a result of this relationship. If this is the case, 
                please enter your desired label below.</asp:Localize>
            </asp:Panel>
            <asp:Panel ID="pnlHelp2" runat="server" CssClass="popupControl" Width="300px">
                <asp:Localize ID="Localize12" runat="server" meta:resourcekey="LiteralResource12">
                You may choose to show a modified label for your <b>Main Data Object</b> 
                when it is presented as a result of this relationship <b>in reverse</b>. 
                If this is the case, please enter your desired label below.</asp:Localize>
            </asp:Panel>
            <%--<div id="divLegend" class="legend" runat="server">
                <asp:Localize ID="locLegend" runat="server" Text="<%$ Resources:LogiAdHoc, LegendLabel %>"></asp:Localize>
                <dl>
                <dt>
                    <img id="Img1" src="../ahImages/modify.gif" alt="Modify Icon" title="Click this icon to modify the selected relationship." runat="server" meta:resourcekey="LegendImage1"/></dt>
                <dd>
                    <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource1" Text="Modify Relationship"></asp:Localize>&nbsp;</dd></dl>
            </div>--%>
        </div>
    </form>
</body>
</html>
