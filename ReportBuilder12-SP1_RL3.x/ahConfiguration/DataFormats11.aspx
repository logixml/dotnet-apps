<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DataFormats11.aspx.vb" EnableViewState="false" Inherits="LogiAdHoc.DataFormats11" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>

<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>


<%@ Import Namespace="ahService" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Data Formats</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    
    <script src="../ahScripts/jQuery/lib/jquery-1.4.3.min.js" type="text/javascript"></script>
	<script src="../ahScripts/jQuery/js/DataFormats.js" type="text/javascript"></script>

</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server" method="post">
        <div id="submenu">
            <AdHoc:NavMenu ID="subnav" runat="server" />
        </div>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" ParentLevels="0" Key="DataFormats" />
        <asp:ScriptManager ID="ScriptManager1" runat="server" ScriptMode="debug" EnablePartialRendering="true" />


                <input type="hidden" id="EditRowNumber" name="EditRowNumber" value="0" /> 
                <input type="hidden" id="ahDirty" name="ahDirty" value="" runat="server" />                               
                <input type="hidden" id="ihDisableReorder" runat="server" value="0" />
                <input type="hidden" id="MaxSort" name="MaxSort" value="0" />
                <input type="hidden" id="ErrorMessageOverflow" value="<%= HttpContext.GetGlobalResourceObject("Errors","Err_Explanation255") %>" />
                <input type="hidden" id="ConfirmMessageDelete" value="<%= HttpContext.GetGlobalResourceObject("LogiAdHoc","DataFormats_DeleteConfirm") %>" />
                <input type="hidden" id="ConfirmMessageSelectDelete" value="<%= HttpContext.GetGlobalResourceObject("LogiAdHoc","DataFormats_DeleteSelect") %>" />
                                
                <table class="limiting">
                    <tr>
                        <td>
                            <table class="gridForm">
                                <tr>
                                    <td>
                                    
                                    <div id="data_main">
                                        
                                        <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="conditional">
                                            <ContentTemplate> 
                                    
                                        <div id="activities">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr width="100%">
                                                    <td align="left" valign="top">
                                                    
                                                        <span class="outerbox">
                                                            <input id="deleteButton" type="button" value="<%= HttpContext.GetGlobalResourceObject("LogiAdHoc","Delete") %>" onclick="RemoveRow();" class="innerbutton" />
                                                        </span>
                                                        
                                                        <span class="outerbox">
                                                            <input type="button" value="Add" onclick="AddRow();" class="innerbutton" />
                                                        </span>
                                                        
                                                    </td>
                                                    <td align="right" valign="top">
                                                        <AdHoc:Search ID="srch" runat="server" Title="Find Data Formats" meta:resourcekey="AdHocSearch" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        
                                        <div id="divDisabledReorder" runat="server">
                                            <p class="info">
                                                <asp:Localize ID="Localize14" runat="server" meta:resourcekey="LiteralResource14"
                                                    Text="Reordering has been disabled due to some rows being hidden as a result of search." />
                                            </p>
                                        </div>
                                        <br /><br />
                                        
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        
                                        <asp:UpdatePanel runat="server" ID="UpdatePanel3" UpdateMode="conditional">
                                            <ContentTemplate> 
                                         <table>
                                            <tr>
                                                <td>                                
                                                    <AdHoc:AHGrid runat="server" ID="jqGrid" ShowPager="true" GridID="DF" WidthStr="750" HeightStr="450" ShowBorder="false" 
                                                        EnableMultiSelect="true"  
                                                        EnableSearch="true"
                                                        EnableSelectAll="true"
                                                        EnableColumnReorder="true" 
                                                        EnableDragDropRows="true" 
                                                        EnableRowSelect="true" />
                                                    <input type="hidden" id="selDF" value="" runat="server" />
                                                    <input type="hidden" id="asgDF" value="" runat="server" />
                                                    <input type="hidden" id="orgDF" value="" runat="server" />
                                                </td>                                                                
                                                <td width="30">
                                                    <div id="divColumnMove" runat="server">
                                                        <%--<asp:ImageButton ID="imgFormatMoveup" runat="server" AlternateText="Move Formats Up"
                                                            CausesValidation="False" CssClass="mslbutton" ImageUrl="../ahImages/arrowUp.gif"
                                                            meta:resourcekey="imgMoveupResource1" OnCommand="FormatMoveUp" ToolTip="Move Formats Up" />
                                                        <asp:ImageButton ID="imgFormatMovedown" runat="server" AlternateText="Move Formats Down"
                                                            CausesValidation="False" CssClass="mslbutton" ImageUrl="../ahImages/arrowDown.gif"
                                                            meta:resourcekey="imgMovedownResource1" OnCommand="FormatMoveDown" ToolTip="Move Formats Down" />--%>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>

                                    <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="conditional">
                                        <ContentTemplate>  

                                    <div id="hovermenu" style="display: none; position: absolute; width: 150px; height: 50px; background-color:#f9f9f9; z-index:9999;">
                                        
                                        <div style="text-align: left; white-space: nowrap; background-color:#f9f9f9; border:1px solid #BCBCBC; color:#484848;" id="pnlActionsMenu">
                                            <div id="divModify" class="hoverMenuActionLink">
                                                <a href="#" id="hovermodify" onclick="Open(''); return false;" class="hoverMenuActionLink">Modify Data Format</a>
                                            </div>
                                            <div id="divDependencies" class="hoverMenuActionLink">
                                                <a href="" id="hoverlink">View Dependencies</a>
                                            </div>
                                        </div>
                                        
                                    </div>
                                
                                    <div id="divLegend" class="legend" runat="server">
                                        <span>
                                            <asp:Localize ID="locLegend" runat="server" Text="<%$ Resources:LogiAdHoc, LegendLabel %>"></asp:Localize>
                                        </span>
                                        <dl id="dlApplicationFormat" runat="server">
                                            <dt>
                                                <img id="Img2" runat="server" alt="Application Format" border="0" height="13" meta:resourcekey="LegendImage2"
                                                    src="../ahImages/spacer.gif" style="background-color: #008800;" title="Application Format"
                                                    width="13" />
                                            </dt>
                                            <dd style="color: #008800;">
                                                <asp:Localize ID="Localize10" runat="server" meta:resourcekey="LiteralResource10"
                                                    Text="Application Format"></asp:Localize>&nbsp;
                                            </dd>
                                        </dl>
                                        <dl id="dlUserDefinedFormat" runat="server">
                                            <dt>
                                                <img id="Img1" runat="server" alt="User-defined Format" border="0" height="13" meta:resourcekey="LegendImage3"
                                                    src="../ahImages/spacer.gif" style="background-color: Black;" title="User-defined Format"
                                                    width="13" />
                                            </dt>
                                            <dd style="color: Black;">
                                                <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource11" Text="User-defined Format"></asp:Localize>&nbsp;
                                            </dd>
                                        </dl>
                                    </div>
    
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
    
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            
        
            
        <asp:UpdatePanel runat="server" ID="updateTest" UpdateMode="conditional">

            <ContentTemplate>    
                <asp:Button runat="server" ID="Button1" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                    TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                    DropShadow="false" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                </ajaxToolkit:ModalPopupExtender>        
                <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 400;">
                    <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                    <div class="modalPopupHandle">
                        <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                <asp:Localize ID="PopupHeader" runat="server" meta:resourcekey="LiteralResource1" Text="Data Format Details"></asp:Localize>
                            </td>
                            <td style="width: 20px;">
                                <asp:ImageButton ID="imgClosePopup" runat="server" 
                                    OnClick="imgClosePopup_Click" CausesValidation="false"
                                    SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                    AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                        </td></tr></table>
                    </div>
                    </asp:Panel>
                                <div class="modalDiv">
                                <asp:UpdatePanel ID="upPopup" runat="server" UpdateMode="conditional">
                                    <ContentTemplate>
                                        <%--<asp:Panel ID="ParamDetails" CssClass="detailpanel" runat="server" meta:resourcekey="ParamDetailsResource1">--%>
                                        <input type="hidden" id="NewFormatID" runat="server" />
                                        <table class="tbTB">
                                            <tr>
                                                <td>
                                                    <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2" Text="Format Name:"></asp:Localize>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFormatName" MaxLength="50" runat="server" />
                                                    <asp:RequiredFieldValidator ID="rtvFormatName" runat="server" ErrorMessage="Format Name is required."
                                                        ControlToValidate="txtFormatName" ValidationGroup="DataFormat" meta:resourcekey="rtvFormatNameResource1">*</asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvValidName" runat="server" ErrorMessage="This name already exists."
                                                        ControlToValidate="txtFormatName" ValidationGroup="DataFormat" OnServerValidate="IsNameValid" meta:resourcekey="cvValidNameResource1">*</asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3" Text="Format:"></asp:Localize>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtFormat" MaxLength="255" runat="server" />
                                                    <asp:RequiredFieldValidator ID="rfvFormat" runat="server" ErrorMessage="Format definition is required."
                                                        ControlToValidate="txtFormat" ValidationGroup="DataFormat" meta:resourcekey="rtvFormatResource1">*</asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvValidFormat" runat="server" ControlToValidate="txtFormat"
                                                        ErrorMessage="This Format already exists." meta:resourcekey="cvValidFormatNameResource1"
                                                        OnServerValidate="IsFormatValid" ValidationGroup="DataFormat">*</asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource4" Text="Applies to:"></asp:Localize>
                                                </td>
                                                <td>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chkNumeric" runat="server" Text="<%$ Resources:LogiAdHoc, DataFormat_Numeric %>" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chkDateTime" runat="server" Text="<%$ Resources:LogiAdHoc, DataFormat_DateTime %>" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chkBoolean" runat="server" Text="<%$ Resources:LogiAdHoc, DataFormat_Boolean %>" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chkText" runat="server" Text="<%$ Resources:LogiAdHoc, DataFormat_Text %>" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:CustomValidator ID="cvAppliesTo" runat="server" ErrorMessage="This format must be applied to at least one data type."
                                                        ControlToValidate="txtFormat" ValidationGroup="DataFormat" OnServerValidate="IsAppliesToValid" meta:resourcekey="cvAppliesToResource1">*</asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource12" Text="Is Available:"></asp:Localize>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkIsAvailable" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource5" Text="Explanation:"></asp:Localize>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtExplanation" TextMode="MultiLine" Columns="45" Rows="3" runat="server" />
                                                    <asp:CustomValidator ID="cvMaxLength" runat="server" ErrorMessage="<%$ Resources:Errors, Err_Explanation255 %>"
                                                        ControlToValidate="txtExplanation" ValidationGroup="DataFormat" OnServerValidate="ReachedMaxLength">*</asp:CustomValidator>
                                                </td>
                                            </tr>
                                            <%--<tr>
                                                <td>
                                                    <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource6" Text="Example before:"></asp:Localize>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtExampleBefore" MaxLength="50" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Localize ID="Localize7" runat="server" meta:resourcekey="LiteralResource7" Text="Example after:"></asp:Localize>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtExampleAfter" MaxLength="50" runat="server" />
                                                </td>
                                            </tr>--%>
                                            <%--<tr>
                                                <td>
                                                    <asp:Localize ID="Localize8" runat="server" meta:resourcekey="LiteralResource8" Text="Available:"></asp:Localize>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkIsAvailable" runat="server" />
                                                </td>
                                            </tr>--%>
                                        </table>
                                        <br />
                                        <!-- Buttons -->
                                        <table>
                                            <tr>
                                                <td colspan="2">
                                                    <AdHoc:LogiButton ID="btnSaveFormat" OnClick="SaveFormat_OnClick" runat="server"
                                                        Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" ValidationGroup="DataFormat" UseSubmitBehavior="false" />
                                                    <AdHoc:LogiButton ID="btnCancelFormat" OnClick="CancelFormat_OnClick"
                                                        runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="False" />
                                                    <asp:ValidationSummary ID="vsummary" ValidationGroup="DataFormat" runat="server"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                </div>
                </asp:Panel>
                <div id="divSaveAnimation">
                    <asp:Button runat="server" ID="btnSaveAnimation" Style="display:none;" />
                    <asp:Button ID="Button2" runat="server" Text="Dummy" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                        TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                        DropShadow="False">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                        <table><tr><td>
                        <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                        </td>
                        <td valign="middle">
                        <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                        </td></tr></table>
                    </asp:Panel>
                </div>                
            </ContentTemplate>
        
        </asp:UpdatePanel>
        
        <script type="text/javascript">
            function InitializeGrids()
            {
                InitPageGrid("DF", gridDF, dataDF, dataViewDF, columnsDF, optionsDF, checkboxSelectorDF);
                if (jQuery.browser.msie)
                {
                    PrepareMenu();
                }
            }
        </script>
    </form> 
</body>
</html>
