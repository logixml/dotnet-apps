<%@ Page Language="VB" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Application Error</title>
    <link rel="stylesheet" type="text/css" href="Login.css">
</head>
<body background="ahImages/homePattern.gif">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
  <tr align="center" valign="middle"> 
    <td> 
      <table width="420" height="250" border="0" align="center" cellpadding="3" cellspacing="0">
        <tr bgcolor="#254974" align="center" valign="middle"> 
          <td> 
            <table width="100%" height="230" border="0" cellpadding="0" cellspacing="0">
              <tr bgcolor="#4A719C"> 
                <td align="center" valign="middle" bgcolor="#FFFFFF">
				 <table width="90%" height="230" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="center" valign="middle">
				      <font color="#CC3300">
				      <h1><asp:Literal ID="ltSorry" runat="server" meta:resourcekey="SorryResource1" Text="Sorry!"></asp:Literal></h1>
				      <p><asp:Literal ID="ltError" runat="server" meta:resourcekey="ErrorResource1" Text="There was an error in the application."></asp:Literal></p>
				      <p><asp:Literal ID="ltClick" runat="server" meta:resourcekey="ClickResource1" Text="You may click the 'Back' button on your browser to continue working."></asp:Literal></p>
                      </font>
					</td>
                  </tr>
                </table>
               </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
     <br>
    </td>
  </tr>
</table>
</body>
</html>
