<SynchronizationPackage SrcInstanceGUID="a32dcfaa-22f2-42d1-bc6f-dcca1007068a" SrcConnectionID="1" SrcGroupID="1" SrcVersion="11.2.64.1" IsSimpleView="False">
  <SelectedElements>
    <Element Type="Role" SelectAll="False" OverwriteExisting="True" OverwriteExistingDependencies="True">
      <Role ElementID="5" />
      <Role ElementID="3" />
      <Role ElementID="2" />
      <Role ElementID="4" />
    </Element>
  </SelectedElements>
  <Tasks>
    <Task Type="Role">
      <SynchronizationOptions OverwriteExisting="True" OverwriteExistingDependencies="True" />
      <Role ElementID="5" RoleName="Report Builder Readonly User" Description="Can view Global and Shared reports">
        <RolePermissions>
          <RolePermission PermissionID="100" Permission="View Global Reports" />
          <RolePermission PermissionID="6" Permission="View Shared Reports" />
        </RolePermissions>
        <Dependencies>
          <Dependency Type="Permission" ID="100" Permission="View Global Reports" />
          <Dependency Type="Permission" ID="6" Permission="View Shared Reports" />
        </Dependencies>
      </Role>
      <Role ElementID="3" RoleName="Report Builder End User" Description="Can Manage Personal Reports, View shared and Global Reports">
        <RolePermissions>
          <RolePermission PermissionID="2" Permission="End User" />
          <RolePermission PermissionID="3" Permission="Manage My Personal Reports" />
          <RolePermission PermissionID="100" Permission="View Global Reports" />
          <RolePermission PermissionID="4" Permission="View My Personal Reports" />
          <RolePermission PermissionID="6" Permission="View Shared Reports" />
        </RolePermissions>
        <Dependencies>
          <Dependency Type="Permission" ID="2" Permission="End User" />
          <Dependency Type="Permission" ID="3" Permission="Manage My Personal Reports" />
          <Dependency Type="Permission" ID="100" Permission="View Global Reports" />
          <Dependency Type="Permission" ID="4" Permission="View My Personal Reports" />
          <Dependency Type="Permission" ID="6" Permission="View Shared Reports" />
        </Dependencies>
      </Role>
      <Role ElementID="2" RoleName="Report Builder Power User" Description="Can manage my personal Reports, Shared Reports, schedule and archive Reports.">
        <RolePermissions>
          <RolePermission PermissionID="12" Permission="Administer Organization" />
          <RolePermission PermissionID="2" Permission="End User" />
          <RolePermission PermissionID="3" Permission="Manage My Personal Reports" />
          <RolePermission PermissionID="5" Permission="Manage Shared Reports" />
          <RolePermission PermissionID="13" Permission="Power End User" />
          <RolePermission PermissionID="8" Permission="Schedule and Archive Reports" />
          <RolePermission PermissionID="100" Permission="View Global Reports" />
          <RolePermission PermissionID="4" Permission="View My Personal Reports" />
          <RolePermission PermissionID="6" Permission="View Shared Reports" />
        </RolePermissions>
        <Dependencies>
          <Dependency Type="Permission" ID="12" Permission="Administer Organization" />
          <Dependency Type="Permission" ID="2" Permission="End User" />
          <Dependency Type="Permission" ID="3" Permission="Manage My Personal Reports" />
          <Dependency Type="Permission" ID="5" Permission="Manage Shared Reports" />
          <Dependency Type="Permission" ID="13" Permission="Power End User" />
          <Dependency Type="Permission" ID="8" Permission="Schedule and Archive Reports" />
          <Dependency Type="Permission" ID="100" Permission="View Global Reports" />
          <Dependency Type="Permission" ID="4" Permission="View My Personal Reports" />
          <Dependency Type="Permission" ID="6" Permission="View Shared Reports" />
        </Dependencies>
      </Role>
      <Role ElementID="4" RoleName="Report Builder Administrator" Description="Can manage Personal, Shared, All and Global Reports, Schedule and Archive Reports">
        <RolePermissions>
          <RolePermission PermissionID="12" Permission="Administer Organization" />
          <RolePermission PermissionID="2" Permission="End User" />
          <RolePermission PermissionID="7" Permission="Manage All Personal Reports" />
          <RolePermission PermissionID="3" Permission="Manage My Personal Reports" />
          <RolePermission PermissionID="5" Permission="Manage Shared Reports" />
          <RolePermission PermissionID="13" Permission="Power End User" />
          <RolePermission PermissionID="8" Permission="Schedule and Archive Reports" />
          <RolePermission PermissionID="100" Permission="View Global Reports" />
          <RolePermission PermissionID="4" Permission="View My Personal Reports" />
          <RolePermission PermissionID="6" Permission="View Shared Reports" />
        </RolePermissions>
        <Dependencies>
          <Dependency Type="Permission" ID="12" Permission="Administer Organization" />
          <Dependency Type="Permission" ID="2" Permission="End User" />
          <Dependency Type="Permission" ID="7" Permission="Manage All Personal Reports" />
          <Dependency Type="Permission" ID="3" Permission="Manage My Personal Reports" />
          <Dependency Type="Permission" ID="5" Permission="Manage Shared Reports" />
          <Dependency Type="Permission" ID="13" Permission="Power End User" />
          <Dependency Type="Permission" ID="8" Permission="Schedule and Archive Reports" />
          <Dependency Type="Permission" ID="100" Permission="View Global Reports" />
          <Dependency Type="Permission" ID="4" Permission="View My Personal Reports" />
          <Dependency Type="Permission" ID="6" Permission="View Shared Reports" />
        </Dependencies>
      </Role>
    </Task>
    <Task Type="Permission">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" />
      <Permission ElementID="100" Permission="View Global Reports" Description="Allow viewing Global Reports">
        <PermissionRights>
          <Right RightID="36" RightName="RM_VLR" />
        </PermissionRights>
      </Permission>
      <Permission ElementID="6" Permission="View Shared Reports" Description="Allows viewing of shared reports. Copy is allowed to any destination where &quot;report creation&quot; right exists.">
        <PermissionRights>
          <Right RightID="70" RightName="RM_MUP" />
          <Right RightID="63" RightName="RM_VSD" />
          <Right RightID="2" RightName="RM_VSR" />
        </PermissionRights>
      </Permission>
      <Permission ElementID="2" Permission="End User" Description="View, create, modify, rename and delete personal reports and folders; archive personal reports; create reports in tabular, bar chart and pie chart presentation formats">
        <PermissionRights>
          <Right RightID="30" RightName="RM_AR" />
          <Right RightID="65" RightName="RM_MPD" />
          <Right RightID="7" RightName="RM_MPF" />
          <Right RightID="4" RightName="RM_MPR" />
          <Right RightID="17" RightName="RM_DPA" />
          <Right RightID="27" RightName="C_MSR" />
          <Right RightID="70" RightName="RM_MUP" />
          <Right RightID="14" RightName="RM_RPR" />
          <Right RightID="49" RightName="RB_BCT" />
          <Right RightID="34" RightName="RB_CLC" />
          <Right RightID="47" RightName="RB_CTB" />
          <Right RightID="83" RightName="RB_CMC" />
          <Right RightID="52" RightName="RB_HMP" />
          <Right RightID="33" RightName="RB_IDV" />
          <Right RightID="71" RightName="RB_IMG" />
          <Right RightID="50" RightName="RB_LCT" />
          <Right RightID="48" RightName="RB_PCT" />
          <Right RightID="51" RightName="RB_SCT" />
          <Right RightID="46" RightName="RB_SML" />
          <Right RightID="77" RightName="RB_ECA" />
          <Right RightID="45" RightName="RB_CSV" />
          <Right RightID="42" RightName="RB_XLS" />
          <Right RightID="43" RightName="RB_WRD" />
          <Right RightID="44" RightName="RB_PDF" />
          <Right RightID="41" RightName="RB_XML" />
          <Right RightID="76" RightName="RM_VD" />
          <Right RightID="11" RightName="RM_SPR" />
          <Right RightID="75" RightName="RM_SBSR" />
          <Right RightID="62" RightName="RM_VPD" />
          <Right RightID="63" RightName="RM_VSD" />
          <Right RightID="10" RightName="RM_SRCN" />
          <Right RightID="1" RightName="RM_VPR" />
          <Right RightID="2" RightName="RM_VSR" />
        </PermissionRights>
      </Permission>
      <Permission ElementID="3" Permission="Manage My Personal Reports" Description="Allows view, create, change, rename, and delete personal reports as well as personal folders. Copy is allowed to any destination where &quot;report creation&quot; right exists.">
        <PermissionRights>
          <Right RightID="65" RightName="RM_MPD" />
          <Right RightID="7" RightName="RM_MPF" />
          <Right RightID="4" RightName="RM_MPR" />
          <Right RightID="17" RightName="RM_DPA" />
          <Right RightID="70" RightName="RM_MUP" />
          <Right RightID="14" RightName="RM_RPR" />
          <Right RightID="62" RightName="RM_VPD" />
          <Right RightID="1" RightName="RM_VPR" />
        </PermissionRights>
      </Permission>
      <Permission ElementID="4" Permission="View My Personal Reports" Description="Allows viewing of personal reports. Copy is allowed to any destination where &quot;report creation&quot; right exists.">
        <PermissionRights>
          <Right RightID="70" RightName="RM_MUP" />
          <Right RightID="14" RightName="RM_RPR" />
          <Right RightID="62" RightName="RM_VPD" />
          <Right RightID="1" RightName="RM_VPR" />
        </PermissionRights>
      </Permission>
      <Permission ElementID="12" Permission="Administer Organization" Description="Allow admin rights in report management area plus user management in configuration for the organization.">
        <PermissionRights>
          <Right RightID="30" RightName="RM_AR" />
          <Right RightID="66" RightName="RM_MSD" />
          <Right RightID="5" RightName="RM_MSR" />
          <Right RightID="65" RightName="RM_MPD" />
          <Right RightID="7" RightName="RM_MPF" />
          <Right RightID="8" RightName="RM_MSF" />
          <Right RightID="68" RightName="RM_MOSD" />
          <Right RightID="69" RightName="RM_MOSF" />
          <Right RightID="54" RightName="RM_MOSR" />
          <Right RightID="4" RightName="RM_MPR" />
          <Right RightID="17" RightName="RM_DPA" />
          <Right RightID="18" RightName="RM_DSA" />
          <Right RightID="19" RightName="RM_DOA" />
          <Right RightID="28" RightName="C_MAR" />
          <Right RightID="27" RightName="C_MSR" />
          <Right RightID="70" RightName="RM_MUP" />
          <Right RightID="20" RightName="C_MU" />
          <Right RightID="67" RightName="RM_MOD" />
          <Right RightID="9" RightName="RM_MOF" />
          <Right RightID="6" RightName="RM_MOR" />
          <Right RightID="16" RightName="RM_ROR" />
          <Right RightID="14" RightName="RM_RPR" />
          <Right RightID="15" RightName="RM_RSR" />
          <Right RightID="49" RightName="RB_BCT" />
          <Right RightID="34" RightName="RB_CLC" />
          <Right RightID="47" RightName="RB_CTB" />
          <Right RightID="52" RightName="RB_HMP" />
          <Right RightID="33" RightName="RB_IDV" />
          <Right RightID="71" RightName="RB_IMG" />
          <Right RightID="50" RightName="RB_LCT" />
          <Right RightID="48" RightName="RB_PCT" />
          <Right RightID="51" RightName="RB_SCT" />
          <Right RightID="46" RightName="RB_SML" />
          <Right RightID="45" RightName="RB_CSV" />
          <Right RightID="42" RightName="RB_XLS" />
          <Right RightID="43" RightName="RB_WRD" />
          <Right RightID="44" RightName="RB_PDF" />
          <Right RightID="41" RightName="RB_XML" />
          <Right RightID="61" RightName="RB_CCS" />
          <Right RightID="73" RightName="RB_IUL" />
          <Right RightID="72" RightName="RB_ICL" />
          <Right RightID="76" RightName="RM_VD" />
          <Right RightID="13" RightName="RM_SOR" />
          <Right RightID="11" RightName="RM_SPR" />
          <Right RightID="12" RightName="RM_SSR" />
          <Right RightID="75" RightName="RM_SBSR" />
          <Right RightID="31" RightName="RM_VAR" />
          <Right RightID="64" RightName="RM_VOD" />
          <Right RightID="62" RightName="RM_VPD" />
          <Right RightID="63" RightName="RM_VSD" />
          <Right RightID="10" RightName="RM_SRCN" />
          <Right RightID="3" RightName="RM_VOR" />
          <Right RightID="1" RightName="RM_VPR" />
          <Right RightID="2" RightName="RM_VSR" />
        </PermissionRights>
      </Permission>
      <Permission ElementID="5" Permission="Manage Shared Reports" Description="Allows view, create, change, rename, and delete shared reports as well as personal folders. Copy is allowed to any destination where &quot;report creation&quot; right exists.">
        <PermissionRights>
          <Right RightID="66" RightName="RM_MSD" />
          <Right RightID="5" RightName="RM_MSR" />
          <Right RightID="8" RightName="RM_MSF" />
          <Right RightID="68" RightName="RM_MOSD" />
          <Right RightID="69" RightName="RM_MOSF" />
          <Right RightID="54" RightName="RM_MOSR" />
          <Right RightID="18" RightName="RM_DSA" />
          <Right RightID="70" RightName="RM_MUP" />
          <Right RightID="15" RightName="RM_RSR" />
          <Right RightID="63" RightName="RM_VSD" />
          <Right RightID="2" RightName="RM_VSR" />
        </PermissionRights>
      </Permission>
      <Permission ElementID="13" Permission="Power End User" Description="View, create, modify, rename and delete personal reports and folders; archive personal reports; create reports in tabular, bar chart and pie chart presentation formats">
        <PermissionRights>
          <Right RightID="66" RightName="RM_MSD" />
          <Right RightID="5" RightName="RM_MSR" />
          <Right RightID="65" RightName="RM_MPD" />
          <Right RightID="38" RightName="RM_MLF" />
          <Right RightID="7" RightName="RM_MPF" />
          <Right RightID="8" RightName="RM_MSF" />
          <Right RightID="68" RightName="RM_MOSD" />
          <Right RightID="79" RightName="RM_MOGF" />
          <Right RightID="69" RightName="RM_MOSF" />
          <Right RightID="80" RightName="RM_MOGR" />
          <Right RightID="54" RightName="RM_MOSR" />
          <Right RightID="37" RightName="RM_MLR" />
          <Right RightID="4" RightName="RM_MPR" />
          <Right RightID="17" RightName="RM_DPA" />
          <Right RightID="18" RightName="RM_DSA" />
          <Right RightID="19" RightName="RM_DOA" />
          <Right RightID="27" RightName="C_MSR" />
          <Right RightID="70" RightName="RM_MUP" />
          <Right RightID="67" RightName="RM_MOD" />
          <Right RightID="9" RightName="RM_MOF" />
          <Right RightID="6" RightName="RM_MOR" />
          <Right RightID="16" RightName="RM_ROR" />
          <Right RightID="39" RightName="RM_RLR" />
          <Right RightID="14" RightName="RM_RPR" />
          <Right RightID="15" RightName="RM_RSR" />
          <Right RightID="49" RightName="RB_BCT" />
          <Right RightID="34" RightName="RB_CLC" />
          <Right RightID="47" RightName="RB_CTB" />
          <Right RightID="83" RightName="RB_CMC" />
          <Right RightID="52" RightName="RB_HMP" />
          <Right RightID="33" RightName="RB_IDV" />
          <Right RightID="71" RightName="RB_IMG" />
          <Right RightID="50" RightName="RB_LCT" />
          <Right RightID="48" RightName="RB_PCT" />
          <Right RightID="51" RightName="RB_SCT" />
          <Right RightID="46" RightName="RB_SML" />
          <Right RightID="77" RightName="RB_ECA" />
          <Right RightID="45" RightName="RB_CSV" />
          <Right RightID="42" RightName="RB_XLS" />
          <Right RightID="43" RightName="RB_WRD" />
          <Right RightID="44" RightName="RB_PDF" />
          <Right RightID="41" RightName="RB_XML" />
          <Right RightID="61" RightName="RB_CCS" />
          <Right RightID="73" RightName="RB_IUL" />
          <Right RightID="72" RightName="RB_ICL" />
          <Right RightID="76" RightName="RM_VD" />
          <Right RightID="75" RightName="RM_SBSR" />
          <Right RightID="62" RightName="RM_VPD" />
          <Right RightID="63" RightName="RM_VSD" />
          <Right RightID="10" RightName="RM_SRCN" />
          <Right RightID="1" RightName="RM_VPR" />
          <Right RightID="2" RightName="RM_VSR" />
        </PermissionRights>
      </Permission>
      <Permission ElementID="8" Permission="Schedule and Archive Reports" Description="Allows scheduling and archiving of any reports that are available to view as well as viewing of archived reports">
        <PermissionRights>
          <Right RightID="30" RightName="RM_AR" />
          <Right RightID="13" RightName="RM_SOR" />
          <Right RightID="11" RightName="RM_SPR" />
          <Right RightID="12" RightName="RM_SSR" />
          <Right RightID="75" RightName="RM_SBSR" />
          <Right RightID="31" RightName="RM_VAR" />
        </PermissionRights>
      </Permission>
      <Permission ElementID="7" Permission="Manage All Personal Reports" Description="Allows view, create, change, rename, and delete all personal reports and folders. Copy is allowed to any destination where &quot;report creation&quot; right exists.">
        <PermissionRights>
          <Right RightID="19" RightName="RM_DOA" />
          <Right RightID="70" RightName="RM_MUP" />
          <Right RightID="67" RightName="RM_MOD" />
          <Right RightID="9" RightName="RM_MOF" />
          <Right RightID="6" RightName="RM_MOR" />
          <Right RightID="16" RightName="RM_ROR" />
          <Right RightID="64" RightName="RM_VOD" />
          <Right RightID="3" RightName="RM_VOR" />
        </PermissionRights>
      </Permission>
    </Task>
  </Tasks>
  <AppSettings CopyConnectionString="False" CopyGeneralSettings="False" CopyUIFeatures="False" CopyReportingOptions="False" CopyReportingFeatures="False" CopySecuritySettings="False" MultipleOrganizations="False" UniqueReportFolderNames="False" CopySchedulerServiceSettings="False" CopySchedulingConfiguration="False" CopySMTPServerSettings="False" CopyEmailConfiguration="False" CopyArchiveConfiguration="False" CopyArchiveSettings="False" CopyEventLogDBConnection="False" CopyEventLogConfiguration="False">
    <Database ElementID="1" MaxReportRecords="9999999" MaxListRecords="50000" DesignTimeValidation="Off" DefaultTemplate="Custom-Blue" />
    <Organization ElementID="1" GroupName="Default" Description="" DefaultTheme="Rights" />
  </AppSettings>
</SynchronizationPackage>