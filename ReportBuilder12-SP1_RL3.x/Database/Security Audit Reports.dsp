<SynchronizationPackage SrcInstanceGUID="647128f6-d29c-4984-a7d2-d7f09f3aa467" SrcConnectionID="2" SrcGroupID="1" SrcVersion="12.1.43.1" IsSimpleView="False">
  <SelectedElements>
    <Element Type="Folder" SelectAll="False" OverwriteExisting="True" OverwriteExistingDependencies="False" IncludeAccessRights="False" PreserveOwner="1" PreserveFolder="1">
      <Folder ElementID="707" />
    </Element>
    <Element Type="Report" SelectAll="False" OverwriteExisting="True" OverwriteExistingDependencies="False" IncludeSchedules="False" IncludeSubscribers="False" IncludeArchives="False" PreserveOwner="1" PreserveFolder="1">
      <Report ElementID="6180" />
      <Report ElementID="6175" />
      <Report ElementID="6174" />
      <Report ElementID="6179" />
    </Element>
  </SelectedElements>
  <Tasks>
    <Task Type="Folder">
      <SynchronizationOptions OverwriteExisting="True" OverwriteExistingDependencies="False" IncludeAccessRights="False" PreserveOwner="1" PreserveFolder="1" />
      <Folder ElementID="707" FolderName="Security Audit Reports" Description="Security Audit Repots for Users, Groups and Permissions" FolderType="0" Owner="rms_support" ParentFolderID="0" AllRolesAccess="1" />
    </Task>
    <Task Type="Report">
      <SynchronizationOptions OverwriteExisting="True" OverwriteExistingDependencies="False" IncludeSchedules="False" IncludeArchives="False" PreserveOwner="1" PreserveFolder="1" />
      <Report ElementID="6180" ReportName="Permissions Audit by Group" Description="" PhysicalName="" ExpirationDate="" FolderType="0" ReportGUID="ahReport6180_032e5ffe-b04b-47fc-bcc2-a9c69e60e99a" Owner="rms_support" ParentFolderID="707" Dashboard="0" Mobile="0">
        <ReportFolder FolderType="0" LastFolderID="707">
          <Folder FolderID="707" FolderName="Security Audit Reports" />
        </ReportFolder>
        <Definition>
          <AdhocMetadata DefaultActiveItemID="2" ActiveStepIndex="1">
            <DataStructures>
              <DataStructure ID="1" Distinct="True" CategoryID="22">
                <DataSource ObjectID="336" ObjectKey="336" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_AUDIT" Object1DatabaseID="" Object1Schema="" Object1Name="" Column1Name="" Object2DatabaseID="" Object2Schema="" Object2Name="" Column2Name="" Relation="" RelationID="0" RelationDirection="0" DatabaseID="2">
                  <DataSource ObjectID="262" ObjectKey="336_380_262_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_GROUP" Object1DatabaseID="2" Object1Schema="RLANLTCS" Object1Name="MV_RB_SEC_PERMISSIONS_AUDIT" Column1Name="PERMISSION_PK" Object2DatabaseID="2" Object2Schema="RLANLTCS" Object2Name="MV_RB_SEC_PERMISSIONS_BY_GROUP" Column2Name="PERMISSION_PK" Relation="Left Outer Join" RelationID="380" RelationDirection="0" DatabaseID="2" />
                </DataSource>
                <Objects>
                  <Object ObjectID="336" ObjectKey="336" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_AUDIT" ObjectIdentifier="336_0_0">
                    <Columns>
                      <Column ColumnID="6115" ColumnName="Event Type" />
                      <Column ColumnID="6116" ColumnName="Field Name" />
                      <Column ColumnID="6117" ColumnName="Old Value" />
                      <Column ColumnID="6118" ColumnName="New Value" />
                      <Column ColumnID="6119" ColumnName="Created By" />
                      <Column ColumnID="6120" ColumnName="Created Date" />
                      <Column ColumnID="6122" ColumnName="Changed Date" />
                    </Columns>
                  </Object>
                  <Object ObjectID="262" ObjectKey="336_380_262_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_GROUP" ObjectIdentifier="262_380_0">
                    <Columns>
                      <Column ColumnID="4637" ColumnName="Group Name" />
                      <Column ColumnID="4638" ColumnName="Group Desc" />
                      <Column ColumnID="4641" ColumnName="Permission" />
                    </Columns>
                  </Object>
                </Objects>
                <OrderBy />
                <Parameter>
                  <Column ParamType="0" ID="1" ColumnID="6122" ObjectKey="336" Operator="Between" BitCmd="And" ControlType="db" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="" Level="0">
                    <Value Value="@Date.30DaysAgo~" ValueType="1" />
                    <Value Value="@Date.Tomorrow~" ValueType="1" />
                  </Column>
                  <Column ParamType="0" ID="2" ColumnID="4637" ObjectKey="336_380_262_0" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="" Level="0" />
                </Parameter>
              </DataStructure>
            </DataStructures>
            <Contents>
              <Content ItemID="1" ItemIndex="1" ContentID="1" ContentName="Header" ContentTitle="">
                <HeaderInfo ShowDate="True" ShowTime="True" ShowTitle="True" ShowDescription="False" Title="Permissions Audit by Group (Preview)" Description="" />
              </Content>
              <Content ItemID="2" ItemIndex="2" ContentID="2" ContentName="Table" ContentTitle="">
                <Data DataStructureID="1" />
                <DataTable Caption="" ShowRecordCount="False" ShowRowNumber="False" UseInteractivePaging="True" UseNumberedList="False" UseAppendRowsPaging="False" PagingLocation="0" InteractivePagingRows="15" InteractivePagingSubRows="50" SummaryRowLocationBottom="False" ShowSummaryRowOnce="True" TableWidth="-1" TableWidthScale="px">
                  <Column ColumnType="1" ColumnID="4637" ObjectKey="336_380_262_0" Header="Group Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="4638" ObjectKey="336_380_262_0" Header="Group Desc" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6115" ObjectKey="336" Header="Record Type" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6119" ObjectKey="336" Header="Created By" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6120" ObjectKey="336" Header="Created Date" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6122" ObjectKey="336" Header="Changed Date" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="4641" ObjectKey="336_380_262_0" Header="Permission" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6116" ObjectKey="336" Header="Field Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6117" ObjectKey="336" Header="Old Value" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6118" ObjectKey="336" Header="New Value" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                </DataTable>
                <XGroupBy GroupType="1" PageBreakAfterGroup="False" FlatGroupingRollUpDetails="False" ShowExpandCollapse="False" AutoExpandDrillDown="False">
                  <Layer Level="1" NoDetailRows="False" NoDrillDown="False" ColumnDrillDownGrouped="False" ColumnDrillDownAggregate="False" IncludeSummaryRow="False" SummaryRowCaption="" DrillDownColumnHeader="" DrillDownRowSuffix="" ColumnDrillDownColumnID="">
                    <Column ColumnID="4637" ObjectKey="336_380_262_0" />
                    <Column ColumnID="4638" ObjectKey="336_380_262_0" />
                    <AggregateColumns />
                  </Layer>
                </XGroupBy>
              </Content>
              <Content ItemID="3" ItemIndex="3" ContentID="11" ContentName="Exports" ContentTitle="">
                <Exports LinkSearchable="False" LinkPrintable="False" LinkExcel="True" LinkWord="False" LinkPPT="False" LinkPdf="False" LinkCSV="False" LinkXML="False" LinkArchive="False" SendMail="False" SendMailExportType="PDF" />
              </Content>
            </Contents>
            <SessionParameter />
            <General Template="Custom-Blue" PrintablePageOrientation="Portrait" ShowPrintablePageNr="ON" PrintablePageType="Letter" />
          </AdhocMetadata>
        </Definition>
        <Dependencies>
          <Dependency Type="Object" ID="336" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_AUDIT">
            <Column ID="6122" ColumnName="Changed Date" />
            <Column ID="6119" ColumnName="Created By" />
            <Column ID="6120" ColumnName="Created Date" />
            <Column ID="6115" ColumnName="Event Type" />
            <Column ID="6116" ColumnName="Field Name" />
            <Column ID="6118" ColumnName="New Value" />
            <Column ID="6117" ColumnName="Old Value" />
          </Dependency>
          <Dependency Type="Object" ID="262" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_GROUP">
            <Column ID="4638" ColumnName="Group Desc" />
            <Column ID="4637" ColumnName="Group Name" />
            <Column ID="4641" ColumnName="Permission" />
          </Dependency>
          <Dependency Type="Relation" ID="380" RelationName="Permissions Audit -&gt; Permissions by Group" Relation="Left Outer Join" ObjectID1="336" ObjectID2="262" />
          <Dependency Type="DataFormat" ID="13" FormatName="Short Date" Format="Short Date" />
        </Dependencies>
      </Report>
      <Report ElementID="6175" ReportName="User Group Audit Report" Description="" PhysicalName="" ExpirationDate="" FolderType="0" ReportGUID="ahReport6175_26ff209e-6aa1-478d-910f-a011506c867c" Owner="rms_support" ParentFolderID="707" Dashboard="0" Mobile="0">
        <ReportFolder FolderType="0" LastFolderID="707">
          <Folder FolderID="707" FolderName="Security Audit Reports" />
        </ReportFolder>
        <Definition>
          <AdhocMetadata DefaultActiveItemID="3" ActiveStepIndex="1">
            <DataStructures>
              <DataStructure ID="1" Distinct="True" CategoryID="22">
                <DataSource ObjectID="334" ObjectKey="334" ObjectSchema="RLANLTCS" ObjectName="MV_RB_USER_GROUPS_AUDIT" Object1DatabaseID="" Object1Schema="" Object1Name="" Column1Name="" Object2DatabaseID="" Object2Schema="" Object2Name="" Column2Name="" Relation="" RelationID="0" RelationDirection="0" DatabaseID="2">
                  <DataSource ObjectID="122" ObjectKey="334_377_122_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_USER_GROUPS" Object1DatabaseID="2" Object1Schema="RLANLTCS" Object1Name="MV_RB_USER_GROUPS_AUDIT" Column1Name="USER_GROUP_PK" Object2DatabaseID="2" Object2Schema="RLANLTCS" Object2Name="MV_RB_SEC_USER_GROUPS" Column2Name="USER_GROUP_PK" Relation="Left Outer Join" RelationID="377" RelationDirection="0" DatabaseID="2" />
                </DataSource>
                <Objects>
                  <Object ObjectID="334" ObjectKey="334" ObjectSchema="RLANLTCS" ObjectName="MV_RB_USER_GROUPS_AUDIT" ObjectIdentifier="334_0_0">
                    <Columns>
                      <Column ColumnID="6085" ColumnName="Group Name" />
                      <Column ColumnID="6086" ColumnName="Description" />
                      <Column ColumnID="6089" ColumnName="Updated Time" />
                      <Column ColumnID="6090" ColumnName="Record Type" />
                      <Column ColumnID="6091" ColumnName="Field Name" />
                      <Column ColumnID="6092" ColumnName="Old Value" />
                      <Column ColumnID="6093" ColumnName="New Value" />
                      <Column ColumnID="6094" ColumnName="Changed Date" />
                    </Columns>
                  </Object>
                  <Object ObjectID="122" ObjectKey="334_377_122_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_USER_GROUPS" ObjectIdentifier="122_377_0">
                    <Columns>
                      <Column ColumnID="2748" ColumnName="Updated By" />
                    </Columns>
                  </Object>
                </Objects>
                <OrderBy />
                <Parameter>
                  <Column ParamType="0" ID="1" ColumnID="6094" ObjectKey="334" Operator="Between" BitCmd="And" ControlType="db" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="" Level="0">
                    <Value Value="@Date.30DaysAgo~" ValueType="1" />
                    <Value Value="@Date.Tomorrow~" ValueType="1" />
                  </Column>
                  <Column ParamType="0" ID="2" ColumnID="6085" ObjectKey="334" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="" Level="0" />
                </Parameter>
              </DataStructure>
            </DataStructures>
            <Contents>
              <Content ItemID="1" ItemIndex="1" ContentID="1" ContentName="Header" ContentTitle="">
                <HeaderInfo ShowDate="True" ShowTime="True" ShowTitle="True" ShowDescription="False" Title="User Group Audit Report" Description="" />
              </Content>
              <Content ItemID="2" ItemIndex="2" ContentID="2" ContentName="Table" ContentTitle="">
                <Data DataStructureID="1" />
                <DataTable Caption="" ShowRecordCount="False" ShowRowNumber="False" UseInteractivePaging="False" UseNumberedList="True" UseAppendRowsPaging="False" PagingLocation="0" InteractivePagingRows="15" InteractivePagingSubRows="50" SummaryRowLocationBottom="False" ShowSummaryRowOnce="True" TableWidth="-1" TableWidthScale="px">
                  <Column ColumnType="1" ColumnID="6085" ObjectKey="334" Header="Group Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6086" ObjectKey="334" Header="Description" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6090" ObjectKey="334" Header="Record Type" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="2748" ObjectKey="334_377_122_0" Header="Updated By" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6089" ObjectKey="334" Header="Updated Time" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6094" ObjectKey="334" Header="Changed Date" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6091" ObjectKey="334" Header="Field Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6092" ObjectKey="334" Header="Old Value" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6093" ObjectKey="334" Header="New Value" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                </DataTable>
                <XGroupBy GroupType="1" PageBreakAfterGroup="False" FlatGroupingRollUpDetails="False" ShowExpandCollapse="False" AutoExpandDrillDown="False">
                  <Layer Level="1" NoDetailRows="False" NoDrillDown="False" ColumnDrillDownGrouped="False" ColumnDrillDownAggregate="False" IncludeSummaryRow="False" SummaryRowCaption="" DrillDownColumnHeader="" DrillDownRowSuffix="" ColumnDrillDownColumnID="">
                    <Column ColumnID="6085" ObjectKey="334" />
                    <Column ColumnID="6086" ObjectKey="334" />
                    <AggregateColumns />
                  </Layer>
                </XGroupBy>
              </Content>
              <Content ItemID="3" ItemIndex="3" ContentID="11" ContentName="Exports" ContentTitle="">
                <Exports LinkSearchable="False" LinkPrintable="False" LinkExcel="True" LinkWord="False" LinkPPT="False" LinkPdf="False" LinkCSV="False" LinkXML="False" LinkArchive="False" SendMail="False" SendMailExportType="PDF" />
              </Content>
            </Contents>
            <SessionParameter />
            <General Template="Custom-Blue" PrintablePageOrientation="Portrait" ShowPrintablePageNr="ON" PrintablePageType="Letter" />
          </AdhocMetadata>
        </Definition>
        <Dependencies>
          <Dependency Type="Object" ID="122" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_USER_GROUPS">
            <Column ID="2748" ColumnName="Updated By" />
          </Dependency>
          <Dependency Type="Object" ID="334" ObjectSchema="RLANLTCS" ObjectName="MV_RB_USER_GROUPS_AUDIT">
            <Column ID="6094" ColumnName="Changed Date" />
            <Column ID="6086" ColumnName="Description" />
            <Column ID="6091" ColumnName="Field Name" />
            <Column ID="6085" ColumnName="Group Name" />
            <Column ID="6093" ColumnName="New Value" />
            <Column ID="6092" ColumnName="Old Value" />
            <Column ID="6090" ColumnName="Record Type" />
            <Column ID="6089" ColumnName="Updated Time" />
          </Dependency>
          <Dependency Type="Relation" ID="377" RelationName="User Group Audit -&gt; User Group" Relation="Left Outer Join" ObjectID1="334" ObjectID2="122" />
          <Dependency Type="DataFormat" ID="13" FormatName="Short Date" Format="Short Date" />
        </Dependencies>
      </Report>
      <Report ElementID="6174" ReportName="User Audit Report" Description="" PhysicalName="" ExpirationDate="" FolderType="0" ReportGUID="ahReport6174_0699a671-fd6f-43a2-adbb-88c6b5243d0a" Owner="rms_support" ParentFolderID="707" Dashboard="0" Mobile="0">
        <ReportFolder FolderType="0" LastFolderID="707">
          <Folder FolderID="707" FolderName="Security Audit Reports" />
        </ReportFolder>
        <Definition>
          <AdhocMetadata DefaultActiveItemID="3" ActiveStepIndex="1">
            <DataStructures>
              <DataStructure ID="1" Distinct="True" CategoryID="22">
                <DataSource ObjectID="335" ObjectKey="335" ObjectSchema="RLANLTCS" ObjectName="MV_RB_USERS_AUDIT" Object1DatabaseID="" Object1Schema="" Object1Name="" Column1Name="" Object2DatabaseID="" Object2Schema="" Object2Name="" Column2Name="" Relation="" RelationID="0" RelationDirection="0" DatabaseID="2">
                  <DataSource ObjectID="124" ObjectKey="335_376_124_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_USERS" Object1DatabaseID="2" Object1Schema="RLANLTCS" Object1Name="MV_RB_USERS_AUDIT" Column1Name="USER_PK" Object2DatabaseID="2" Object2Schema="RLANLTCS" Object2Name="MV_RB_SEC_USERS" Column2Name="USER_PK" Relation="Left Outer Join" RelationID="376" RelationDirection="0" DatabaseID="2" />
                </DataSource>
                <Objects>
                  <Object ObjectID="335" ObjectKey="335" ObjectSchema="RLANLTCS" ObjectName="MV_RB_USERS_AUDIT" ObjectIdentifier="335_0_0">
                    <Columns>
                      <Column ColumnID="6098" ColumnName="First Name" />
                      <Column ColumnID="6099" ColumnName="Last Name" />
                      <Column ColumnID="6100" ColumnName="Updated By" />
                      <Column ColumnID="6101" ColumnName="Updated Date" />
                      <Column ColumnID="6102" ColumnName="Updated Time" />
                      <Column ColumnID="6103" ColumnName="Record Type" />
                      <Column ColumnID="6104" ColumnName="Field Name" />
                      <Column ColumnID="6105" ColumnName="Old Value" />
                      <Column ColumnID="6106" ColumnName="New Value" />
                      <Column ColumnID="6107" ColumnName="Changed Date" />
                    </Columns>
                  </Object>
                  <Object ObjectID="124" ObjectKey="335_376_124_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_USERS" ObjectIdentifier="124_376_0">
                    <Columns>
                      <Column ColumnID="2760" ColumnName="User Login" />
                      <Column ColumnID="6019" ColumnName="External Login" />
                      <Column ColumnID="2773" ColumnName="Active" />
                    </Columns>
                  </Object>
                </Objects>
                <OrderBy>
                  <Column ColumnID="6101" ObjectKey="335" Direction="Descending" />
                </OrderBy>
                <Parameter>
                  <Column ParamType="0" ID="3" ColumnID="6107" ObjectKey="335" Operator="Between" BitCmd="And" ControlType="db" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="" Level="0">
                    <Value Value="@Date.30DaysAgo~" ValueType="1" />
                    <Value Value="@Date.Tomorrow~" ValueType="1" />
                  </Column>
                  <Column ParamType="0" ID="1" ColumnID="2773" ObjectKey="335_376_124_0" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="" Level="0">
                    <Value Value="Yes" ValueType="3" />
                  </Column>
                  <Column ParamType="0" ID="2" ColumnID="2760" ObjectKey="335_376_124_0" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="" Level="0" />
                </Parameter>
              </DataStructure>
            </DataStructures>
            <Contents>
              <Content ItemID="1" ItemIndex="1" ContentID="1" ContentName="Header" ContentTitle="">
                <HeaderInfo ShowDate="True" ShowTime="True" ShowTitle="True" ShowDescription="False" Title="User Audit Report" Description="" />
              </Content>
              <Content ItemID="2" ItemIndex="2" ContentID="2" ContentName="Table" ContentTitle="">
                <Data DataStructureID="1" />
                <DataTable Caption="" ShowRecordCount="False" ShowRowNumber="False" UseInteractivePaging="False" UseNumberedList="True" UseAppendRowsPaging="False" PagingLocation="0" InteractivePagingRows="15" InteractivePagingSubRows="50" SummaryRowLocationBottom="False" ShowSummaryRowOnce="True" TableWidth="-1" TableWidthScale="px">
                  <Column ColumnType="1" ColumnID="2760" ObjectKey="335_376_124_0" Header="User Login" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6019" ObjectKey="335_376_124_0" Header="External Login" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6098" ObjectKey="335" Header="First Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6099" ObjectKey="335" Header="Last Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6103" ObjectKey="335" Header="Record Type" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6100" ObjectKey="335" Header="Updated By" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6102" ObjectKey="335" Header="Updated Time" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6107" ObjectKey="335" Header="Changed Date" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6104" ObjectKey="335" Header="Field Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6105" ObjectKey="335" Header="Old Value" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6106" ObjectKey="335" Header="New Value" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                </DataTable>
                <XGroupBy GroupType="1" PageBreakAfterGroup="False" FlatGroupingRollUpDetails="False" ShowExpandCollapse="False" AutoExpandDrillDown="False">
                  <Layer Level="1" NoDetailRows="False" NoDrillDown="False" ColumnDrillDownGrouped="False" ColumnDrillDownAggregate="False" IncludeSummaryRow="False" SummaryRowCaption="" DrillDownColumnHeader="" DrillDownRowSuffix="" ColumnDrillDownColumnID="">
                    <Column ColumnID="2760" ObjectKey="335_376_124_0" />
                    <Column ColumnID="6019" ObjectKey="335_376_124_0" />
                    <Column ColumnID="6098" ObjectKey="335" />
                    <Column ColumnID="6099" ObjectKey="335" />
                    <AggregateColumns />
                  </Layer>
                </XGroupBy>
              </Content>
              <Content ItemID="3" ItemIndex="3" ContentID="11" ContentName="Exports" ContentTitle="">
                <Exports LinkSearchable="False" LinkPrintable="False" LinkExcel="True" LinkWord="False" LinkPPT="False" LinkPdf="False" LinkCSV="False" LinkXML="False" LinkArchive="False" SendMail="False" SendMailExportType="PDF" />
              </Content>
            </Contents>
            <SessionParameter />
            <General Template="Custom-Blue" PrintablePageOrientation="Portrait" ShowPrintablePageNr="ON" PrintablePageType="Letter" />
          </AdhocMetadata>
        </Definition>
        <Dependencies>
          <Dependency Type="Object" ID="124" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_USERS">
            <Column ID="2773" ColumnName="Active" />
            <Column ID="6019" ColumnName="External Login" />
            <Column ID="2760" ColumnName="User Login" />
          </Dependency>
          <Dependency Type="Object" ID="335" ObjectSchema="RLANLTCS" ObjectName="MV_RB_USERS_AUDIT">
            <Column ID="6107" ColumnName="Changed Date" />
            <Column ID="6104" ColumnName="Field Name" />
            <Column ID="6098" ColumnName="First Name" />
            <Column ID="6099" ColumnName="Last Name" />
            <Column ID="6106" ColumnName="New Value" />
            <Column ID="6105" ColumnName="Old Value" />
            <Column ID="6103" ColumnName="Record Type" />
            <Column ID="6100" ColumnName="Updated By" />
            <Column ID="6101" ColumnName="Updated Date" />
            <Column ID="6102" ColumnName="Updated Time" />
          </Dependency>
          <Dependency Type="Relation" ID="376" RelationName="Users Audit -&gt; Users" Relation="Left Outer Join" ObjectID1="335" ObjectID2="124" />
          <Dependency Type="DataFormat" ID="13" FormatName="Short Date" Format="Short Date" />
        </Dependencies>
      </Report>
      <Report ElementID="6179" ReportName="Permissions Audit by User" Description="" PhysicalName="" ExpirationDate="" FolderType="0" ReportGUID="ahReport6179_a784bf71-62e2-4d59-8b8b-e10557cfca84" Owner="rms_support" ParentFolderID="707" Dashboard="0" Mobile="0">
        <ReportFolder FolderType="0" LastFolderID="707">
          <Folder FolderID="707" FolderName="Security Audit Reports" />
        </ReportFolder>
        <Definition>
          <AdhocMetadata DefaultActiveItemID="2" ActiveStepIndex="1">
            <DataStructures>
              <DataStructure ID="1" Distinct="True" CategoryID="22">
                <DataSource ObjectID="336" ObjectKey="336" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_AUDIT" Object1DatabaseID="" Object1Schema="" Object1Name="" Column1Name="" Object2DatabaseID="" Object2Schema="" Object2Name="" Column2Name="" Relation="" RelationID="0" RelationDirection="0" DatabaseID="2">
                  <DataSource ObjectID="263" ObjectKey="336_378_263_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_USERS" Object1DatabaseID="2" Object1Schema="RLANLTCS" Object1Name="MV_RB_SEC_PERMISSIONS_AUDIT" Column1Name="PERMISSION_PK" Object2DatabaseID="2" Object2Schema="RLANLTCS" Object2Name="MV_RB_SEC_PERMISSIONS_BY_USERS" Column2Name="PERMISSION_PK" Relation="Left Outer Join" RelationID="378" RelationDirection="0" DatabaseID="2" />
                </DataSource>
                <Objects>
                  <Object ObjectID="336" ObjectKey="336" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_AUDIT" ObjectIdentifier="336_0_0">
                    <Columns>
                      <Column ColumnID="6115" ColumnName="Event Type" />
                      <Column ColumnID="6116" ColumnName="Field Name" />
                      <Column ColumnID="6117" ColumnName="Old Value" />
                      <Column ColumnID="6118" ColumnName="New Value" />
                      <Column ColumnID="6119" ColumnName="Created By" />
                      <Column ColumnID="6120" ColumnName="Created Date" />
                      <Column ColumnID="6122" ColumnName="Changed Date" />
                    </Columns>
                  </Object>
                  <Object ObjectID="263" ObjectKey="336_378_263_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_USERS" ObjectIdentifier="263_378_0">
                    <Columns>
                      <Column ColumnID="4648" ColumnName="User Login" />
                      <Column ColumnID="4655" ColumnName="Permission" />
                    </Columns>
                  </Object>
                </Objects>
                <OrderBy />
                <Parameter>
                  <Column ParamType="0" ID="1" ColumnID="6122" ObjectKey="336" Operator="Between" BitCmd="And" ControlType="db" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="" Level="0">
                    <Value Value="@Date.30DaysAgo~" ValueType="1" />
                    <Value Value="@Date.Tomorrow~" ValueType="1" />
                  </Column>
                  <Column ParamType="0" ID="2" ColumnID="4648" ObjectKey="336_378_263_0" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="" Level="0" />
                </Parameter>
              </DataStructure>
            </DataStructures>
            <Contents>
              <Content ItemID="1" ItemIndex="1" ContentID="1" ContentName="Header" ContentTitle="">
                <HeaderInfo ShowDate="True" ShowTime="True" ShowTitle="True" ShowDescription="False" Title="Permissions Audit by User (Preview)" Description="" />
              </Content>
              <Content ItemID="2" ItemIndex="2" ContentID="2" ContentName="Table" ContentTitle="">
                <Data DataStructureID="1" />
                <DataTable Caption="" ShowRecordCount="False" ShowRowNumber="False" UseInteractivePaging="True" UseNumberedList="False" UseAppendRowsPaging="False" PagingLocation="0" InteractivePagingRows="15" InteractivePagingSubRows="50" SummaryRowLocationBottom="False" ShowSummaryRowOnce="True" TableWidth="-1" TableWidthScale="px">
                  <Column ColumnType="1" ColumnID="4648" ObjectKey="336_378_263_0" Header="User Login" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6115" ObjectKey="336" Header="Record Type" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="4655" ObjectKey="336_378_263_0" Header="Permission" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6116" ObjectKey="336" Header="Field Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6117" ObjectKey="336" Header="Old Value" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6118" ObjectKey="336" Header="New Value" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6119" ObjectKey="336" Header="Created By" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6122" ObjectKey="336" Header="Changed Date" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="6120" ObjectKey="336" Header="Created Date" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                </DataTable>
                <XGroupBy GroupType="1" PageBreakAfterGroup="False" FlatGroupingRollUpDetails="False" ShowExpandCollapse="False" AutoExpandDrillDown="False">
                  <Layer Level="1" NoDetailRows="False" NoDrillDown="False" ColumnDrillDownGrouped="False" ColumnDrillDownAggregate="False" IncludeSummaryRow="False" SummaryRowCaption="" DrillDownColumnHeader="" DrillDownRowSuffix="" ColumnDrillDownColumnID="">
                    <Column ColumnID="4648" ObjectKey="336_378_263_0" />
                    <AggregateColumns />
                  </Layer>
                </XGroupBy>
              </Content>
              <Content ItemID="3" ItemIndex="3" ContentID="11" ContentName="Exports" ContentTitle="">
                <Exports LinkSearchable="False" LinkPrintable="False" LinkExcel="True" LinkWord="False" LinkPPT="False" LinkPdf="False" LinkCSV="False" LinkXML="False" LinkArchive="False" SendMail="False" SendMailExportType="PDF" />
              </Content>
            </Contents>
            <SessionParameter />
            <General Template="Custom-Blue" PrintablePageOrientation="Portrait" ShowPrintablePageNr="ON" PrintablePageType="Letter" />
          </AdhocMetadata>
        </Definition>
        <Dependencies>
          <Dependency Type="Object" ID="336" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_AUDIT">
            <Column ID="6122" ColumnName="Changed Date" />
            <Column ID="6119" ColumnName="Created By" />
            <Column ID="6120" ColumnName="Created Date" />
            <Column ID="6115" ColumnName="Event Type" />
            <Column ID="6116" ColumnName="Field Name" />
            <Column ID="6118" ColumnName="New Value" />
            <Column ID="6117" ColumnName="Old Value" />
          </Dependency>
          <Dependency Type="Object" ID="263" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_USERS">
            <Column ID="4655" ColumnName="Permission" />
            <Column ID="4648" ColumnName="User Login" />
          </Dependency>
          <Dependency Type="Relation" ID="378" RelationName="Permissions Audit -&gt; Permissions By Users" Relation="Left Outer Join" ObjectID1="336" ObjectID2="263" />
          <Dependency Type="DataFormat" ID="13" FormatName="Short Date" Format="Short Date" />
        </Dependencies>
      </Report>
    </Task>
    <Task Type="Object">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" IncludeLinks="False" IncludeAccessRights="False" PreserveCategories="1" DeleteUnmatched="False" />
      <Object ElementID="336" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_AUDIT" Description="Permissions Audit" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="6126" ColumnName="APPLICATION_OBJECT_PK" Description="APPLICATION_OBJECT_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="6113" ColumnName="PERMISSION_PK" Description="PERMISSION_PK" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="6115" ColumnName="Event Type" Description="Record Type" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="50" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6116" ColumnName="Field Name" Description="Field Name" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="34" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6117" ColumnName="Old Value" Description="Old Value" ColumnType="D" OrdinalPosition="5" ColumnOrder="6" DataType="200" CharacterMaxLen="43" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6118" ColumnName="New Value" Description="New Value" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="200" CharacterMaxLen="43" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6119" ColumnName="Created By" Description="Created By" ColumnType="D" OrdinalPosition="7" ColumnOrder="8" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6120" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="8" ColumnOrder="9" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6121" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="9" ColumnOrder="10" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6122" ColumnName="Changed Date" Description="Changed Date" ColumnType="D" OrdinalPosition="10" ColumnOrder="11" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="22" CategoryName="Security" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="262" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_GROUP" Description="Permissions by Group" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="4637" ColumnName="Group Name" Description="Group Name" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4638" ColumnName="Group Desc" Description="Group Desc" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4639" ColumnName="Users" Description="Users" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4640" ColumnName="Channels" Description="Outlet" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4641" ColumnName="Permission" Description="Permission" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4642" ColumnName="View" Description="View" ColumnType="D" OrdinalPosition="5" ColumnOrder="6" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4643" ColumnName="Update" Description="Update" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4644" ColumnName="Delete" Description="Delete" ColumnType="D" OrdinalPosition="7" ColumnOrder="8" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4645" ColumnName="New" Description="New" ColumnType="D" OrdinalPosition="8" ColumnOrder="9" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4646" ColumnName="Filter" Description="Filter" ColumnType="D" OrdinalPosition="9" ColumnOrder="10" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4647" ColumnName="Full name" Description="Full name" ColumnType="D" OrdinalPosition="10" ColumnOrder="11" DataType="200" CharacterMaxLen="1000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6129" ColumnName="PERMISSION_PK" Description="PERMISSION_PK" ColumnType="D" OrdinalPosition="11" ColumnOrder="12" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="22" CategoryName="Security" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="122" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_USER_GROUPS" Description="User Groups" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2743" ColumnName="USER_PK" Description="USER_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2744" ColumnName="USER_GROUP_PK" Description="USER_GROUP_PK" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2745" ColumnName="Group Name" Description="Group Name" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2746" ColumnName="Description" Description="Description" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2747" ColumnName="Created By" Description="Created By" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2748" ColumnName="Updated By" Description="Updated By" ColumnType="D" OrdinalPosition="5" ColumnOrder="6" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2749" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2750" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="7" ColumnOrder="8" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="22" CategoryName="Security" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="334" ObjectSchema="RLANLTCS" ObjectName="MV_RB_USER_GROUPS_AUDIT" Description="User Group Audit" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="6083" ColumnName="USER_GROUP_PK" Description="USER_GROUP_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="6084" ColumnName="User Group ID" Description="User Group ID" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6085" ColumnName="Group Name" Description="Group Name" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="500" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6086" ColumnName="Description" Description="Description" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6087" ColumnName="Updated By" Description="Updated By" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="501" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6088" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="5" ColumnOrder="6" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6089" ColumnName="Updated Time" Description="Updated Time" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="200" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6090" ColumnName="Record Type" Description="Record Type" ColumnType="D" OrdinalPosition="7" ColumnOrder="8" DataType="200" CharacterMaxLen="50" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6091" ColumnName="Field Name" Description="Field Name" ColumnType="D" OrdinalPosition="8" ColumnOrder="9" DataType="129" CharacterMaxLen="50" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6092" ColumnName="Old Value" Description="Old Value" ColumnType="D" OrdinalPosition="9" ColumnOrder="10" DataType="200" CharacterMaxLen="32000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6093" ColumnName="New Value" Description="New Value" ColumnType="D" OrdinalPosition="10" ColumnOrder="11" DataType="200" CharacterMaxLen="32000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6094" ColumnName="Changed Date" Description="Changed Date" ColumnType="D" OrdinalPosition="11" ColumnOrder="12" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6095" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="12" ColumnOrder="13" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="22" CategoryName="Security" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="124" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_USERS" Description="Users" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2759" ColumnName="USER_PK" Description="USER_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2760" ColumnName="User Login" Description="User Login" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2761" ColumnName="Group Name" Description="Group Name" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6019" ColumnName="External Login" Description="External Login" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2762" ColumnName="Group Desc" Description="Group Desc" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2763" ColumnName="First Name" Description="First Name" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2764" ColumnName="Last Name" Description="Last Name" ColumnType="D" OrdinalPosition="5" ColumnOrder="6" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2765" ColumnName="Street" Description="Street" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2766" ColumnName="City" Description="City" ColumnType="D" OrdinalPosition="7" ColumnOrder="8" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2767" ColumnName="Zip" Description="Zip" ColumnType="D" OrdinalPosition="8" ColumnOrder="9" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2768" ColumnName="State" Description="State" ColumnType="D" OrdinalPosition="9" ColumnOrder="10" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2769" ColumnName="Country" Description="Country" ColumnType="D" OrdinalPosition="10" ColumnOrder="11" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2770" ColumnName="Phone" Description="Phone" ColumnType="D" OrdinalPosition="11" ColumnOrder="12" DataType="200" CharacterMaxLen="50" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2771" ColumnName="Email" Description="Email" ColumnType="D" OrdinalPosition="12" ColumnOrder="13" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2772" ColumnName="Comments" Description="Comments" ColumnType="D" OrdinalPosition="13" ColumnOrder="14" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2773" ColumnName="Active" Description="Active" ColumnType="D" OrdinalPosition="14" ColumnOrder="15" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2774" ColumnName="SOX Approver" Description="SOX Approver" ColumnType="D" OrdinalPosition="15" ColumnOrder="16" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2775" ColumnName="Sales Group Name" Description="Sales Group Name" ColumnType="D" OrdinalPosition="16" ColumnOrder="17" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2776" ColumnName="Created By" Description="Created By" ColumnType="D" OrdinalPosition="17" ColumnOrder="18" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2801" ColumnName="Password Change Date" Description="Password Change Date" ColumnType="D" OrdinalPosition="17" ColumnOrder="19" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2777" ColumnName="Updated By" Description="Updated By" ColumnType="D" OrdinalPosition="18" ColumnOrder="20" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2778" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="19" ColumnOrder="21" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2779" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="20" ColumnOrder="22" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="5844" ColumnName="Last Login" Description="Last Login" ColumnType="D" OrdinalPosition="22" ColumnOrder="23" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Long Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="22" CategoryName="Security" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="335" ObjectSchema="RLANLTCS" ObjectName="MV_RB_USERS_AUDIT" Description="User Audit" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="6096" ColumnName="USER_PK" Description="USER_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="6097" ColumnName="User ID" Description="User ID" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6098" ColumnName="First Name" Description="First Name" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="1000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6099" ColumnName="Last Name" Description="Last Name" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="1000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6100" ColumnName="Updated By" Description="Updated By" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="1000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6101" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="5" ColumnOrder="6" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6102" ColumnName="Updated Time" Description="Updated Time" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="200" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6103" ColumnName="Record Type" Description="Record Type" ColumnType="D" OrdinalPosition="7" ColumnOrder="8" DataType="200" CharacterMaxLen="50" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6104" ColumnName="Field Name" Description="Field Name" ColumnType="D" OrdinalPosition="8" ColumnOrder="9" DataType="129" CharacterMaxLen="500" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6105" ColumnName="Old Value" Description="Old Value" ColumnType="D" OrdinalPosition="9" ColumnOrder="10" DataType="200" CharacterMaxLen="32000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6106" ColumnName="New Value" Description="New Value" ColumnType="D" OrdinalPosition="10" ColumnOrder="11" DataType="200" CharacterMaxLen="32000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6107" ColumnName="Changed Date" Description="Changed Date" ColumnType="D" OrdinalPosition="11" ColumnOrder="12" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6108" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="12" ColumnOrder="13" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="22" CategoryName="Security" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="263" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_USERS" Description="Permissions by Users" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="4648" ColumnName="User Login" Description="User Login" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4649" ColumnName="First Name" Description="First Name" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4650" ColumnName="Last Name" Description="Last Name" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4651" ColumnName="Email ID" Description="Email ID" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4652" ColumnName="SOX Approver" Description="SOX Approver" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4653" ColumnName="Active" Description="Active" ColumnType="D" OrdinalPosition="5" ColumnOrder="6" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4654" ColumnName="Group Name" Description="Group Name" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4655" ColumnName="Permission" Description="Permission" ColumnType="D" OrdinalPosition="7" ColumnOrder="8" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4656" ColumnName="View" Description="View" ColumnType="D" OrdinalPosition="8" ColumnOrder="9" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4657" ColumnName="Update" Description="Update" ColumnType="D" OrdinalPosition="9" ColumnOrder="10" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4658" ColumnName="Delete" Description="Delete" ColumnType="D" OrdinalPosition="10" ColumnOrder="11" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4659" ColumnName="New" Description="New" ColumnType="D" OrdinalPosition="11" ColumnOrder="12" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4660" ColumnName="Filter" Description="Filter" ColumnType="D" OrdinalPosition="12" ColumnOrder="13" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4661" ColumnName="Full Name" Description="Full Name" ColumnType="D" OrdinalPosition="13" ColumnOrder="14" DataType="200" CharacterMaxLen="1000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="6128" ColumnName="PERMISSION_PK" Description="PERMISSION_PK" ColumnType="D" OrdinalPosition="14" ColumnOrder="15" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="22" CategoryName="Security" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
    </Task>
    <Task Type="Relation">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" />
      <Relation ElementID="380" RelationName="Permissions Audit -&gt; Permissions by Group" Description="" Relation="Left Outer Join" ObjectID1="336" ObjectID2="262" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="1">
        <RelationDetails>
          <RelationColumn ColumnID1="6113" ColumnName1="PERMISSION_PK" ColumnID2="6129" ColumnName2="PERMISSION_PK" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="336" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_AUDIT">
            <Column ID="6113" ColumnName="PERMISSION_PK" />
          </Dependency>
          <Dependency Type="Object" ID="262" ObjectName="MV_RB_SEC_PERMISSIONS_BY_GROUP">
            <Column ID="6129" ColumnName="PERMISSION_PK" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="377" RelationName="User Group Audit -&gt; User Group" Description="" Relation="Left Outer Join" ObjectID1="334" ObjectID2="122" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="1">
        <RelationDetails>
          <RelationColumn ColumnID1="6083" ColumnName1="USER_GROUP_PK" ColumnID2="2744" ColumnName2="USER_GROUP_PK" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="334" ObjectSchema="RLANLTCS" ObjectName="MV_RB_USER_GROUPS_AUDIT">
            <Column ID="6083" ColumnName="USER_GROUP_PK" />
          </Dependency>
          <Dependency Type="Object" ID="122" ObjectName="MV_RB_SEC_USER_GROUPS">
            <Column ID="2744" ColumnName="USER_GROUP_PK" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="376" RelationName="Users Audit -&gt; Users" Description="" Relation="Left Outer Join" ObjectID1="335" ObjectID2="124" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="1">
        <RelationDetails>
          <RelationColumn ColumnID1="6096" ColumnName1="USER_PK" ColumnID2="2759" ColumnName2="USER_PK" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="335" ObjectSchema="RLANLTCS" ObjectName="MV_RB_USERS_AUDIT">
            <Column ID="6096" ColumnName="USER_PK" />
          </Dependency>
          <Dependency Type="Object" ID="124" ObjectName="MV_RB_SEC_USERS">
            <Column ID="2759" ColumnName="USER_PK" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="378" RelationName="Permissions Audit -&gt; Permissions By Users" Description="" Relation="Left Outer Join" ObjectID1="336" ObjectID2="263" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="1">
        <RelationDetails>
          <RelationColumn ColumnID1="6113" ColumnName1="PERMISSION_PK" ColumnID2="6128" ColumnName2="PERMISSION_PK" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="336" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_AUDIT">
            <Column ID="6113" ColumnName="PERMISSION_PK" />
          </Dependency>
          <Dependency Type="Object" ID="263" ObjectName="MV_RB_SEC_PERMISSIONS_BY_USERS">
            <Column ID="6128" ColumnName="PERMISSION_PK" />
          </Dependency>
        </Dependencies>
      </Relation>
    </Task>
    <Task Type="DataFormat">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" />
      <DataFormat ElementID="13" FormatKey="ShortDate" FormatName="Short Date" Format="Short Date" Internal="1" AppliesTo="10" Explanation="" IsAvailable="1" SortOrder="13" />
    </Task>
  </Tasks>
  <AppSettings CopyConnectionString="False" CopyGeneralSettings="False" CopyUIFeatures="False" CopyReportingOptions="False" CopyReportingFeatures="False" CopySecuritySettings="False" MultipleOrganizations="False" UniqueReportFolderNames="False" CopySchedulerServiceSettings="False" CopySchedulingConfiguration="False" CopySMTPServerSettings="False" CopyEmailConfiguration="False" CopyArchiveConfiguration="False" CopyArchiveSettings="False" CopyEventLogDBConnection="False" CopyEventLogConfiguration="False">
    <Database ElementID="2" MaxReportRecords="500000" MaxListRecords="999" DesignTimeValidation="Off" DefaultTemplate="Custom-Blue" />
    <Organization ElementID="1" GroupName="Default" Description="" DefaultTheme="Rights" />
  </AppSettings>
</SynchronizationPackage>