<SynchronizationPackage SrcInstanceGUID="eeb395a0-32bf-458c-9625-cab5dad2df46" SrcConnectionID="1" SrcGroupID="1" SrcVersion="11.2.64.1" IsSimpleView="False">
  <SelectedElements>
    <Element Type="Folder" SelectAll="False" OverwriteExisting="True" OverwriteExistingDependencies="False" IncludeAccessRights="False" PreserveOwner="1" PreserveFolder="1">
      <Folder ElementID="1" />
    </Element>
    <Element Type="Report" SelectAll="False" OverwriteExisting="True" OverwriteExistingDependencies="False" IncludeSchedules="False" IncludeSubscribers="False" IncludeArchives="False" PreserveOwner="1" PreserveFolder="1">
      <Report ElementID="1" />
      <Report ElementID="2" />
      <Report ElementID="3" />
    </Element>
  </SelectedElements>
  <Tasks>
    <Task Type="Folder">
      <SynchronizationOptions OverwriteExisting="True" OverwriteExistingDependencies="False" IncludeAccessRights="False" PreserveOwner="1" PreserveFolder="1" />
      <Folder ElementID="1" FolderName="Security Reports" Description="" FolderType="1" Owner="Admin" ParentFolderID="0" AllRolesAccess="1" />
    </Task>
    <Task Type="Report">
      <SynchronizationOptions OverwriteExisting="True" OverwriteExistingDependencies="False" IncludeSchedules="False" IncludeArchives="False" PreserveOwner="1" PreserveFolder="1" />
      <Report ElementID="1" ReportName="User List Report" Description="" PhysicalName="" ExpirationDate="" FolderType="0" ReportGUID="ahReport190533_c16ac6da-f603-4ca4-8295-7749323332e6" Owner="Admin" ParentFolderID="6" Dashboard="0" Mobile="0">
        <ReportFolder FolderType="0" LastFolderID="6">
          <Folder FolderID="6" FolderName="Security Reports" />
        </ReportFolder>
        <Definition>
          <AdhocMetadata DefaultActiveItemID="2" ActiveStepIndex="2">
            <DataStructures>
              <DataStructure ID="1" Distinct="False" CategoryID="0">
                <DataSource ObjectID="176" ObjectKey="176" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_CHANNELS_BY_USER" Object1DatabaseID="" Object1Schema="" Object1Name="" Column1Name="" Object2DatabaseID="" Object2Schema="" Object2Name="" Column2Name="" Relation="" RelationID="0" RelationDirection="0" DatabaseID="1" />
                <Objects>
                  <Object ObjectID="176" ObjectKey="176" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_CHANNELS_BY_USER" ObjectIdentifier="176_0_0">
                    <Columns>
                      <Column ColumnID="3067" ColumnName="User Login" />
                      <Column ColumnID="3068" ColumnName="First Name" />
                      <Column ColumnID="3069" ColumnName="Last Name" />
                      <Column ColumnID="3072" ColumnName="Active" />
                      <Column ColumnID="3073" ColumnName="Group Name" />
                      <Column ColumnID="3074" ColumnName="Group Desc" />
                      <Column ColumnID="3076" ColumnName="Group Outlets" />
                      <Column ColumnID="4166" ColumnName="Last Login" />
                    </Columns>
                  </Object>
                  <Object ObjectID="-1" ObjectKey="-1" ObjectSchema="" ObjectName="MyCalc" ObjectIdentifier="-1_0_0">
                    <Columns>
                      <Column ColumnID="1" ColumnName="Search Criteria" Formula="decode( «176».«3072» ,'Yes','Active','Inactive')" DataType="200" />
                      <Column ColumnID="2" ColumnName="Last Login Time" Formula="nvl(to_char(«176».«4166» ,'HH:MI::SS AM'),'-')" DataType="200" />
                      <Column ColumnID="3" ColumnName="Last login New" Formula="nvl(to_char(«176».«4166»,'mm/dd/yyyy'),'-')" DataType="200" />
                    </Columns>
                  </Object>
                </Objects>
                <OrderBy />
                <Parameter>
                  <Column ParamType="0" ID="1" ColumnID="1" ObjectKey="-1" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="Status: " Level="0">
                    <Value Value="Active" ValueType="3" />
                  </Column>
                </Parameter>
              </DataStructure>
            </DataStructures>
            <Contents>
              <Content ItemID="1" ItemIndex="1" ContentID="1" ContentName="Header" ContentTitle="">
                <HeaderInfo ShowDate="True" ShowTime="True" ShowTitle="True" ShowDescription="False" Title="User List Report (Preview)" Description="" />
              </Content>
              <Content ItemID="2" ItemIndex="2" ContentID="2" ContentName="Table" ContentTitle="">
                <Data DataStructureID="1" />
                <DataTable Caption="" ShowRecordCount="False" ShowRowNumber="False" UseInteractivePaging="True" UseNumberedList="False" UseAppendRowsPaging="False" PagingLocation="0" InteractivePagingRows="15" InteractivePagingSubRows="50" SummaryRowLocationBottom="False" ShowSummaryRowOnce="True" TableWidth="-1" TableWidthScale="px">
                  <Column ColumnType="1" ColumnID="3073" ObjectKey="176" Header="Group Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="3074" ObjectKey="176" Header="Group Desc" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="3067" ObjectKey="176" Header="User Login" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="3068" ObjectKey="176" Header="First Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="3069" ObjectKey="176" Header="Last Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1" ObjectKey="-1" Header="Status" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="3076" ObjectKey="176" Header="Group Outlets" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="3" ObjectKey="-1" Header="Last login" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="2" ObjectKey="-1" Header="Last Login Time" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                </DataTable>
                <XGroupBy GroupType="1" PageBreakAfterGroup="False" FlatGroupingRollUpDetails="False" ShowExpandCollapse="False" AutoExpandDrillDown="False">
                  <Layer Level="1" NoDetailRows="False" NoDrillDown="False" ColumnDrillDownGrouped="False" ColumnDrillDownAggregate="False" IncludeSummaryRow="False" SummaryRowCaption="" DrillDownColumnHeader="" DrillDownRowSuffix="" ColumnDrillDownColumnID="">
                    <Column ColumnID="3067" ObjectKey="176" />
                    <Column ColumnID="3068" ObjectKey="176" />
                    <Column ColumnID="3069" ObjectKey="176" />
                    <Column ColumnID="1" ObjectKey="-1" />
                    <Column ColumnID="3" ObjectKey="-1" />
                    <Column ColumnID="2" ObjectKey="-1" />
                    <AggregateColumns />
                  </Layer>
                </XGroupBy>
              </Content>
              <Content ItemID="3" ItemIndex="3" ContentID="11" ContentName="Exports" ContentTitle="">
                <Exports LinkSearchable="False" LinkPrintable="False" LinkExcel="True" LinkWord="False" LinkPPT="False" LinkPdf="True" LinkCSV="False" LinkXML="False" LinkArchive="False" SendMail="False" SendMailExportType="PDF" />
              </Content>
            </Contents>
            <SessionParameter />
            <General Template="Custom-Blue" PrintablePageOrientation="Portrait" ShowPrintablePageNr="ON" PrintablePageType="Letter" />
          </AdhocMetadata>
        </Definition>
        <Dependencies>
          <Dependency Type="Object" ID="176" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_CHANNELS_BY_USER">
            <Column ID="3072" ColumnName="Active" />
            <Column ID="3068" ColumnName="First Name" />
            <Column ID="3074" ColumnName="Group Desc" />
            <Column ID="3073" ColumnName="Group Name" />
            <Column ID="3076" ColumnName="Group Outlets" />
            <Column ID="4166" ColumnName="Last Login" />
            <Column ID="3069" ColumnName="Last Name" />
            <Column ID="3067" ColumnName="User Login" />
          </Dependency>
          <Dependency Type="DataFormat" ID="13" FormatName="Short Date" Format="Short Date" />
        </Dependencies>
      </Report>
      <Report ElementID="2" ReportName="Security Group Report" Description="Permission By Group" PhysicalName="" ExpirationDate="" FolderType="0" ReportGUID="ahReport190534_d9fc715d-a855-4998-9b5f-cda1da2d14fc" Owner="Admin" ParentFolderID="6" Dashboard="0" Mobile="0">
        <ReportFolder FolderType="0" LastFolderID="6">
          <Folder FolderID="6" FolderName="Security Reports" />
        </ReportFolder>
        <Definition>
          <AdhocMetadata DefaultActiveItemID="2" ActiveStepIndex="1">
            <DataStructures>
              <DataStructure ID="1" Distinct="False" CategoryID="248">
                <DataSource ObjectID="81" ObjectKey="81" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_GROUP" Object1DatabaseID="" Object1Schema="" Object1Name="" Column1Name="" Object2DatabaseID="" Object2Schema="" Object2Name="" Column2Name="" Relation="" RelationID="0" RelationDirection="0" DatabaseID="1" />
                <Objects>
                  <Object ObjectID="81" ObjectKey="81" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_GROUP" ObjectIdentifier="81_0_0">
                    <Columns>
                      <Column ColumnID="1541" ColumnName="Group Name" />
                      <Column ColumnID="1542" ColumnName="Group Desc" />
                      <Column ColumnID="1543" ColumnName="Users" />
                      <Column ColumnID="1546" ColumnName="View" />
                      <Column ColumnID="1547" ColumnName="Update" />
                      <Column ColumnID="1548" ColumnName="Delete" />
                      <Column ColumnID="1549" ColumnName="New" />
                      <Column ColumnID="1550" ColumnName="Filter" />
                      <Column ColumnID="1551" ColumnName="Full name" />
                    </Columns>
                  </Object>
                </Objects>
                <OrderBy />
                <Parameter>
                  <Column ParamType="0" ID="1" ColumnID="1541" ObjectKey="81" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="Group Name: " Level="0" />
                </Parameter>
              </DataStructure>
            </DataStructures>
            <Contents>
              <Content ItemID="1" ItemIndex="1" ContentID="1" ContentName="Header" ContentTitle="">
                <HeaderInfo ShowDate="True" ShowTime="True" ShowTitle="True" ShowDescription="False" Title="Permission By Group (Preview)" Description="" />
              </Content>
              <Content ItemID="2" ItemIndex="2" ContentID="2" ContentName="Table" ContentTitle="">
                <Data DataStructureID="1" />
                <DataTable Caption="" ShowRecordCount="False" ShowRowNumber="False" UseInteractivePaging="True" UseNumberedList="False" UseAppendRowsPaging="False" PagingLocation="0" InteractivePagingRows="15" InteractivePagingSubRows="50" SummaryRowLocationBottom="False" ShowSummaryRowOnce="True" TableWidth="-1" TableWidthScale="px">
                  <Column ColumnType="1" ColumnID="1551" ObjectKey="81" Header="Permissions" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1546" ObjectKey="81" Header="View" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1547" ObjectKey="81" Header="Update" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1548" ObjectKey="81" Header="Delete" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1549" ObjectKey="81" Header="New" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1541" ObjectKey="81" Header="Group Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1542" ObjectKey="81" Header="Group Desc" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1543" ObjectKey="81" Header="User Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1550" ObjectKey="81" Header="Filter" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                </DataTable>
                <XGroupBy GroupType="1" PageBreakAfterGroup="False" FlatGroupingRollUpDetails="False" ShowExpandCollapse="True" AutoExpandDrillDown="False">
                  <Layer Level="1" NoDetailRows="False" NoDrillDown="False" ColumnDrillDownGrouped="False" ColumnDrillDownAggregate="False" IncludeSummaryRow="False" SummaryRowCaption="" DrillDownColumnHeader="" DrillDownRowSuffix="" ColumnDrillDownColumnID="">
                    <Column ColumnID="1541" ObjectKey="81" />
                    <Column ColumnID="1542" ObjectKey="81" />
                    <Column ColumnID="1543" ObjectKey="81" />
                    <AggregateColumns />
                  </Layer>
                </XGroupBy>
              </Content>
              <Content ItemID="3" ItemIndex="3" ContentID="11" ContentName="Exports" ContentTitle="">
                <Exports LinkSearchable="False" LinkPrintable="False" LinkExcel="True" LinkWord="False" LinkPPT="False" LinkPdf="True" LinkCSV="False" LinkXML="False" LinkArchive="False" SendMail="False" SendMailExportType="PDF" />
              </Content>
            </Contents>
            <SessionParameter />
            <General Template="Custom-Blue" PrintablePageOrientation="Portrait" ShowPrintablePageNr="ON" PrintablePageType="Letter" />
          </AdhocMetadata>
        </Definition>
        <Dependencies>
          <Dependency Type="Object" ID="81" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_GROUP">
            <Column ID="1548" ColumnName="Delete" />
            <Column ID="1550" ColumnName="Filter" />
            <Column ID="1551" ColumnName="Full name" />
            <Column ID="1542" ColumnName="Group Desc" />
            <Column ID="1541" ColumnName="Group Name" />
            <Column ID="1549" ColumnName="New" />
            <Column ID="1547" ColumnName="Update" />
            <Column ID="1543" ColumnName="Users" />
            <Column ID="1546" ColumnName="View" />
          </Dependency>
        </Dependencies>
      </Report>
      <Report ElementID="3" ReportName="Permission By Users" Description="Permission By Users" PhysicalName="" ExpirationDate="" FolderType="0" ReportGUID="ahReport190535_2b1da372-0435-40ad-875b-68d3732de39b" Owner="Admin" ParentFolderID="6" Dashboard="0" Mobile="0">
        <ReportFolder FolderType="0" LastFolderID="6">
          <Folder FolderID="6" FolderName="Security Reports" />
        </ReportFolder>
        <Definition>
          <AdhocMetadata DefaultActiveItemID="3" ActiveStepIndex="1">
            <DataStructures>
              <DataStructure ID="1" Distinct="False" CategoryID="248">
                <DataSource ObjectID="82" ObjectKey="82" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_USERS" Object1DatabaseID="" Object1Schema="" Object1Name="" Column1Name="" Object2DatabaseID="" Object2Schema="" Object2Name="" Column2Name="" Relation="" RelationID="0" RelationDirection="0" DatabaseID="1" />
                <Objects>
                  <Object ObjectID="82" ObjectKey="82" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_USERS" ObjectIdentifier="82_0_0">
                    <Columns>
                      <Column ColumnID="1552" ColumnName="User Login" />
                      <Column ColumnID="1553" ColumnName="First Name" />
                      <Column ColumnID="1554" ColumnName="Last Name" />
                      <Column ColumnID="1555" ColumnName="Email ID" />
                      <Column ColumnID="1556" ColumnName="SOX Approver" />
                      <Column ColumnID="1557" ColumnName="Active" />
                      <Column ColumnID="1558" ColumnName="Group Name" />
                      <Column ColumnID="1560" ColumnName="View" />
                      <Column ColumnID="1561" ColumnName="Update" />
                      <Column ColumnID="1562" ColumnName="Delete" />
                      <Column ColumnID="1563" ColumnName="New" />
                      <Column ColumnID="1564" ColumnName="Filter" />
                      <Column ColumnID="1565" ColumnName="Full Name" />
                    </Columns>
                  </Object>
                  <Object ObjectID="-1" ObjectKey="-1" ObjectSchema="" ObjectName="MyCalc" ObjectIdentifier="-1_0_0">
                    <Columns>
                      <Column ColumnID="1" ColumnName="Users" Formula="«82».«1553» ||' '|| «82».«1554»" DataType="200" />
                      <Column ColumnID="2" ColumnName="Active Users" Formula="decode(«82».«1557»,'Yes','Active','Inactive')" DataType="200" />
                    </Columns>
                  </Object>
                </Objects>
                <OrderBy />
                <Parameter>
                  <Column ParamType="0" ID="1" ColumnID="1" ObjectKey="-1" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="Users: " Level="0" />
                  <Column ParamType="0" ID="2" ColumnID="2" ObjectKey="-1" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="Status: " Level="0" />
                </Parameter>
              </DataStructure>
            </DataStructures>
            <Contents>
              <Content ItemID="1" ItemIndex="1" ContentID="1" ContentName="Header" ContentTitle="">
                <HeaderInfo ShowDate="True" ShowTime="True" ShowTitle="True" ShowDescription="False" Title="Permission By Users (Preview)" Description="" />
              </Content>
              <Content ItemID="2" ItemIndex="2" ContentID="2" ContentName="Table" ContentTitle="">
                <Data DataStructureID="1" />
                <DataTable Caption="" ShowRecordCount="False" ShowRowNumber="False" UseInteractivePaging="True" UseNumberedList="False" UseAppendRowsPaging="False" PagingLocation="0" InteractivePagingRows="15" InteractivePagingSubRows="50" SummaryRowLocationBottom="False" ShowSummaryRowOnce="True" TableWidth="-1" TableWidthScale="px">
                  <Column ColumnType="1" ColumnID="1565" ObjectKey="82" Header="Permissions" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1560" ObjectKey="82" Header="View" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1561" ObjectKey="82" Header="Update" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1562" ObjectKey="82" Header="Delete" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1563" ObjectKey="82" Header="New" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1552" ObjectKey="82" Header="User ID" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1553" ObjectKey="82" Header="First Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1554" ObjectKey="82" Header="Last Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1555" ObjectKey="82" Header="Email ID" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1556" ObjectKey="82" Header="SOX Approver" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1558" ObjectKey="82" Header="Group Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1564" ObjectKey="82" Header="Filter" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="2" ObjectKey="-1" Header="Status" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                </DataTable>
                <XGroupBy GroupType="1" PageBreakAfterGroup="False" FlatGroupingRollUpDetails="False" ShowExpandCollapse="False" AutoExpandDrillDown="False">
                  <Layer Level="1" NoDetailRows="False" NoDrillDown="False" ColumnDrillDownGrouped="False" ColumnDrillDownAggregate="False" IncludeSummaryRow="False" SummaryRowCaption="" DrillDownColumnHeader="" DrillDownRowSuffix="" ColumnDrillDownColumnID="">
                    <Column ColumnID="1552" ObjectKey="82" />
                    <Column ColumnID="1553" ObjectKey="82" />
                    <Column ColumnID="1554" ObjectKey="82" />
                    <Column ColumnID="1555" ObjectKey="82" />
                    <Column ColumnID="1556" ObjectKey="82" />
                    <Column ColumnID="2" ObjectKey="-1" />
                    <Column ColumnID="1558" ObjectKey="82" />
                    <AggregateColumns />
                  </Layer>
                </XGroupBy>
              </Content>
              <Content ItemID="3" ItemIndex="3" ContentID="11" ContentName="Exports" ContentTitle="">
                <Exports LinkSearchable="False" LinkPrintable="False" LinkExcel="True" LinkWord="False" LinkPPT="False" LinkPdf="True" LinkCSV="False" LinkXML="False" LinkArchive="False" SendMail="False" SendMailExportType="PDF" />
              </Content>
            </Contents>
            <SessionParameter />
            <General Template="Custom-Blue" PrintablePageOrientation="Portrait" ShowPrintablePageNr="ON" PrintablePageType="Letter" />
          </AdhocMetadata>
        </Definition>
        <Dependencies>
          <Dependency Type="Object" ID="82" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_USERS">
            <Column ID="1557" ColumnName="Active" />
            <Column ID="1562" ColumnName="Delete" />
            <Column ID="1555" ColumnName="Email ID" />
            <Column ID="1564" ColumnName="Filter" />
            <Column ID="1553" ColumnName="First Name" />
            <Column ID="1565" ColumnName="Full Name" />
            <Column ID="1558" ColumnName="Group Name" />
            <Column ID="1554" ColumnName="Last Name" />
            <Column ID="1563" ColumnName="New" />
            <Column ID="1556" ColumnName="SOX Approver" />
            <Column ID="1561" ColumnName="Update" />
            <Column ID="1552" ColumnName="User Login" />
            <Column ID="1560" ColumnName="View" />
          </Dependency>
        </Dependencies>
      </Report>
    </Task>
    <Task Type="Object">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" IncludeLinks="False" IncludeAccessRights="False" PreserveCategories="1" />
      <Object ElementID="176" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_CHANNELS_BY_USER" Description="Channels By User" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="3067" ColumnName="User Login" Description="User Login" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4203" ColumnName="USER_PK" Description="USER_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="3068" ColumnName="First Name" Description="First Name" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3069" ColumnName="Last Name" Description="Last Name" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3070" ColumnName="Email" Description="Email" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3071" ColumnName="SOX Approver" Description="SOX Approver" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3072" ColumnName="Active" Description="Active" ColumnType="D" OrdinalPosition="5" ColumnOrder="6" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3073" ColumnName="Group Name" Description="Group Name" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3074" ColumnName="Group Desc" Description="Group Desc" ColumnType="D" OrdinalPosition="7" ColumnOrder="8" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3076" ColumnName="Group Outlets" Description="Group Outlets" ColumnType="D" OrdinalPosition="8" ColumnOrder="9" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3077" ColumnName="User Outlets" Description="User Outlets" ColumnType="D" OrdinalPosition="9" ColumnOrder="10" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4204" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="11" ColumnOrder="12" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4205" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="12" ColumnOrder="13" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="4166" ColumnName="Last Login" Description="Last Login" ColumnType="D" OrdinalPosition="13" ColumnOrder="14" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="81" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_GROUP" Description="Permissions by Group" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="1541" ColumnName="Group Name" Description="Group Name" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1542" ColumnName="Group Desc" Description="Group Desc" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1543" ColumnName="Users" Description="Users" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1544" ColumnName="Channels" Description="Channels" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1545" ColumnName="Permission" Description="Permission" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1546" ColumnName="View" Description="View" ColumnType="D" OrdinalPosition="5" ColumnOrder="6" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1547" ColumnName="Update" Description="Update" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1548" ColumnName="Delete" Description="Delete" ColumnType="D" OrdinalPosition="7" ColumnOrder="8" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1549" ColumnName="New" Description="New" ColumnType="D" OrdinalPosition="8" ColumnOrder="9" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1550" ColumnName="Filter" Description="Filter" ColumnType="D" OrdinalPosition="9" ColumnOrder="10" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1551" ColumnName="Full name" Description="Full name" ColumnType="D" OrdinalPosition="10" ColumnOrder="11" DataType="200" CharacterMaxLen="1000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="5" CategoryName="Security" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="82" ObjectSchema="RLANLTCS" ObjectName="MV_RB_SEC_PERMISSIONS_BY_USERS" Description="Permissions by Users" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="1552" ColumnName="User Login" Description="User Login" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1553" ColumnName="First Name" Description="First Name" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1554" ColumnName="Last Name" Description="Last Name" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1555" ColumnName="Email ID" Description="Email ID" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1556" ColumnName="SOX Approver" Description="SOX Approver" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1557" ColumnName="Active" Description="Active" ColumnType="D" OrdinalPosition="5" ColumnOrder="6" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1558" ColumnName="Group Name" Description="Group Name" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1559" ColumnName="Permission" Description="Permission" ColumnType="D" OrdinalPosition="7" ColumnOrder="8" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1560" ColumnName="View" Description="View" ColumnType="D" OrdinalPosition="8" ColumnOrder="9" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1561" ColumnName="Update" Description="Update" ColumnType="D" OrdinalPosition="9" ColumnOrder="10" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1562" ColumnName="Delete" Description="Delete" ColumnType="D" OrdinalPosition="10" ColumnOrder="11" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1563" ColumnName="New" Description="New" ColumnType="D" OrdinalPosition="11" ColumnOrder="12" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1564" ColumnName="Filter" Description="Filter" ColumnType="D" OrdinalPosition="12" ColumnOrder="13" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1565" ColumnName="Full Name" Description="Full Name" ColumnType="D" OrdinalPosition="13" ColumnOrder="14" DataType="200" CharacterMaxLen="1000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="5" CategoryName="Security" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
    </Task>
    <Task Type="DataFormat">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" />
      <DataFormat ElementID="13" FormatKey="ShortDate" FormatName="Short Date" Format="Short Date" Internal="1" AppliesTo="10" Explanation="" IsAvailable="1" SortOrder="13" />
    </Task>
  </Tasks>
  <AppSettings CopyConnectionString="False" CopyGeneralSettings="False" CopyUIFeatures="False" CopyReportingOptions="False" CopyReportingFeatures="False" CopySecuritySettings="False" MultipleOrganizations="False" UniqueReportFolderNames="False" CopySchedulerServiceSettings="False" CopySchedulingConfiguration="False" CopySMTPServerSettings="False" CopyEmailConfiguration="False" CopyArchiveConfiguration="False" CopyArchiveSettings="False" CopyEventLogDBConnection="False" CopyEventLogConfiguration="False">
    <Database ElementID="1" MaxReportRecords="64000" MaxListRecords="10000" DesignTimeValidation="Off" DefaultTemplate="Custom-Blue" />
    <Organization ElementID="1" GroupName="Default" Description="" DefaultTheme="Rights" />
  </AppSettings>
</SynchronizationPackage>