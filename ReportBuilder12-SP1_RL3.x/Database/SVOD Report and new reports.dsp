<SynchronizationPackage SrcInstanceGUID="647128f6-d29c-4984-a7d2-d7f09f3aa467" SrcConnectionID="1" SrcGroupID="1" SrcVersion="12.1.43.1" IsSimpleView="True">
  <SelectedElements>
    <Element Type="Report" SelectAll="False" OverwriteExisting="True" OverwriteExistingDependencies="True" IncludeSchedules="False" IncludeSubscribers="False" IncludeArchives="False" PreserveOwner="1" PreserveFolder="1">
      <Report ElementID="135" />
      <Report ElementID="318" />
      <Report ElementID="319" />
    </Element>
  </SelectedElements>
  <Tasks>
    <Task Type="Report">
      <SynchronizationOptions OverwriteExisting="True" OverwriteExistingDependencies="True" IncludeSchedules="False" IncludeArchives="False" PreserveOwner="1" PreserveFolder="1" />
      <Report ElementID="135" ReportName="Strategic Rights Report SVOD" Description="" PhysicalName="" ExpirationDate="" FolderType="0" ReportGUID="ahReport109343_0f72d36a-bb05-4fbf-a9dd-b723dec14f12" Owner="Admin" ParentFolderID="0" Dashboard="0" Mobile="0">
        <Definition>
          <AdhocMetadata DefaultActiveItemID="1" ActiveStepIndex="2">
            <DataStructures>
              <DataStructure ID="1" Distinct="True" CategoryID="0">
                <DataSource ObjectID="71" ObjectKey="71" ObjectSchema="RLANLTCS" ObjectName="MV_RB_RPT_STRATEGIC_RIGHT_SVOD" Object1DatabaseID="" Object1Schema="" Object1Name="" Column1Name="" Object2DatabaseID="" Object2Schema="" Object2Name="" Column2Name="" Relation="" RelationID="0" RelationDirection="0" DatabaseID="1">
                  <DataSource ObjectID="94" ObjectKey="71_74_94_0" ObjectSchema="" ObjectName="VV_RB_SVOD_BO_TREE" Object1DatabaseID="1" Object1Schema="RLANLTCS" Object1Name="MV_RB_RPT_STRATEGIC_RIGHT_SVOD" Column1Name="ID_BUSINESS_OUTLET_TM" Object2DatabaseID="1" Object2Schema="" Object2Name="VV_RB_SVOD_BO_TREE" Column2Name="ID_BUSINESS_OUTLET_TM" Relation="Inner Join" RelationID="74" RelationDirection="0" DatabaseID="1" />
                  <DataSource ObjectID="96" ObjectKey="71_78_96_0" ObjectSchema="" ObjectName="VV_RB_SVOD_MOVIES_TYPE" Object1DatabaseID="1" Object1Schema="RLANLTCS" Object1Name="MV_RB_RPT_STRATEGIC_RIGHT_SVOD" Column1Name="ID_ASSET" Object2DatabaseID="1" Object2Schema="" Object2Name="VV_RB_SVOD_MOVIES_TYPE" Column2Name="ID_ASSET" Relation="Inner Join" RelationID="78" RelationDirection="0" DatabaseID="1" />
                </DataSource>
                <Objects>
                  <Object ObjectID="71" ObjectKey="71" ObjectSchema="RLANLTCS" ObjectName="MV_RB_RPT_STRATEGIC_RIGHT_SVOD" ObjectIdentifier="71_0_0">
                    <Columns>
                      <Column ColumnID="1129" ColumnName="Title" />
                      <Column ColumnID="1130" ColumnName="YOP" />
                      <Column ColumnID="1131" ColumnName="Studio" />
                      <Column ColumnID="1132" ColumnName="Linear LSD" />
                      <Column ColumnID="1133" ColumnName="Linear LSD Est" />
                      <Column ColumnID="1134" ColumnName="Linear LED" />
                      <Column ColumnID="1135" ColumnName="Linear LED Est" />
                      <Column ColumnID="1136" ColumnName="Sky Go LSD" />
                      <Column ColumnID="1137" ColumnName="Sky Go LSD Est" />
                      <Column ColumnID="1138" ColumnName="Sky Go LED" />
                      <Column ColumnID="1139" ColumnName="Sky Go LED Est" />
                      <Column ColumnID="1140" ColumnName="Restriction" />
                      <Column ColumnID="1141" ColumnName="SVOD LSD Restr" />
                      <Column ColumnID="1142" ColumnName="SVOD LSD Est Restr" />
                      <Column ColumnID="1143" ColumnName="SVOD LED Restr" />
                      <Column ColumnID="1144" ColumnName="SVOD LED Est Restr" />
                      <Column ColumnID="1145" ColumnName="Anytime+ LSD" />
                      <Column ColumnID="1146" ColumnName="Anytime+ LSD Est" />
                      <Column ColumnID="1147" ColumnName="Anytime+ LED" />
                      <Column ColumnID="1148" ColumnName="Anytime+ LED Est" />
                      <Column ColumnID="1149" ColumnName="Linear to PC Streaming" />
                      <Column ColumnID="1150" ColumnName="Mobile" />
                      <Column ColumnID="1151" ColumnName="Now TV SS" />
                      <Column ColumnID="1152" ColumnName="Now TV You View" />
                      <Column ColumnID="1153" ColumnName="Now TV Mobile" />
                      <Column ColumnID="1154" ColumnName="Sky Go Extra" />
                      <Column ColumnID="1155" ColumnName="Deal Code" />
                      <Column ColumnID="1156" ColumnName="Deal Name" />
                      <Column ColumnID="1158" ColumnName="Date Updated" />
                      <Column ColumnID="1161" ColumnName="SVOD LSD" />
                      <Column ColumnID="1163" ColumnName="SVOD LED" />
                      <Column ColumnID="1164" ColumnName="ID_ASSET" />
                      <Column ColumnID="1706" ColumnName="TBA" />
                      <Column ColumnID="1707" ColumnName="Title Grade" />
                      <Column ColumnID="2529" ColumnName="Genre" />
                      <Column ColumnID="1708" ColumnName="Scheduled Start Date" />
                      <Column ColumnID="2530" ColumnName="Duration" />
                      <Column ColumnID="1709" ColumnName="Scheduled End Date" />
                      <Column ColumnID="3054" ColumnName="SDR" />
                      <Column ColumnID="3055" ColumnName="HDR" />
                      <Column ColumnID="1710" ColumnName="Premier" />
                      <Column ColumnID="2565" ColumnName="Certification" />
                    </Columns>
                  </Object>
                  <Object ObjectID="94" ObjectKey="71_74_94_0" ObjectSchema="" ObjectName="VV_RB_SVOD_BO_TREE" ObjectIdentifier="94_74_0">
                    <Columns>
                      <Column ColumnID="1657" ColumnName="Business Outlet" />
                    </Columns>
                  </Object>
                  <Object ObjectID="96" ObjectKey="71_78_96_0" ObjectSchema="" ObjectName="VV_RB_SVOD_MOVIES_TYPE" ObjectIdentifier="96_78_0">
                    <Columns>
                      <Column ColumnID="1712" ColumnName="Type" />
                    </Columns>
                  </Object>
                  <Object ObjectID="-1" ObjectKey="-1" ObjectSchema="" ObjectName="MyCalc" ObjectIdentifier="-1_0_0">
                    <Columns>
                      <Column ColumnID="1" ColumnName="Param Studio" Formula="(case when &#xA;    upper( «71».«1131» )  &lt;&gt;  '20TH CENTURY FOX INT''L TV'&#xA;and upper( «71».«1131» ) &lt;&gt; 'COLUMBIA TRI-STAR'&#xA;and upper( «71».«1131» ) &lt;&gt; 'DISNEY/TOUCHSTONE'&#xA;and upper( «71».«1131» ) &lt;&gt; 'DREAMWORKS SKG'&#xA;and upper( «71».«1131» ) &lt;&gt; 'ENTERTAINMENT FILM DIST.'&#xA;and upper( «71».«1131» ) &lt;&gt; 'MGM TELEVISION (EUROPE) LTD'&#xA;and upper( «71».«1131» ) &lt;&gt; 'MIRAMAX FILMS'&#xA;and upper( «71».«1131» ) &lt;&gt; 'PARAMOUNT PICTURES INTL.'&#xA;and upper( «71».«1131» ) &lt;&gt; 'SONY PICTURES INTERNATIONAL TV'&#xA;and upper( «71».«1131» ) &lt;&gt; 'SPYGLASS ENTERTAINMENT'&#xA;and upper( «71».«1131» ) &lt;&gt; 'UNIVERSAL STUDIOS' &#xA;and upper( «71».«1131» ) &lt;&gt; 'UNIVERSAL STUDIOS LIMITED' &#xA;and upper( «71».«1131» ) &lt;&gt; 'WARNER BROTHERS INT TV' &#xA;    then 'All Indy'&#xA; else  «71».«1131» &#xA;end)" DataType="200" />
                      <Column ColumnID="2" ColumnName="SVOD Only" Formula="decode(«71».«1132» ,null,'Yes','No')" DataType="200" />
                      <Column ColumnID="3" ColumnName="Type:" Formula="fnc_ra_svod_tvod_type('@Request.RP1_18~' , '@Request.RP1_2~' , '@Request.RP1_3~',   nvl( «71».«1136» ,  «71».«1145»  ) , nvl( «71».«1138» ,  «71».«1147»  ) ,   «71».«1708» ,  «71».«1709» , «71».«1710»   )" DataType="200" />
                      <Column ColumnID="4" ColumnName="Release Date" Formula="case when «71».«1708» is not null  then to_char( «71».«1708» ,'dd/mm/yyyy') || ' - ' || to_char( «71».«1709» ,'dd/mm/yyyy') else null end" DataType="200" />
                      <Column ColumnID="5" ColumnName="Released" Formula="case when «71».«1708» is not null  then 'Yes' else null end" DataType="200" />
                      <Column ColumnID="6" ColumnName="SVOD Movies" Formula="'SVOD Movies'" DataType="200" />
                      <Column ColumnID="7" ColumnName="TBA New" Formula="decode(«71».«1706» ,'Yes','Checked','Unchecked')" DataType="200" />
                    </Columns>
                  </Object>
                </Objects>
                <OrderBy />
                <Parameter>
                  <Column ParamType="0" ID="16" ColumnID="1" ObjectKey="-1" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="Studio: " Level="0">
                    <Value Value="All Indy" ValueType="3" />
                  </Column>
                  <Column ParamType="0" ID="17" ColumnID="1657" ObjectKey="71_74_94_0" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="0" OfferAllOption="False" Ask="True" Caption="Channels: " Level="0">
                    <Value Value="003............Sky Movie Channels" ValueType="3" />
                  </Column>
                  <Column ParamType="0" ID="18" ColumnID="1712" ObjectKey="71_78_96_0" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="Type: " Level="0">
                    <Value Value="New" ValueType="3" />
                  </Column>
                  <Column ParamType="0" ID="19" ColumnID="1707" ObjectKey="71" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="0" OfferAllOption="False" Ask="True" Caption="Grade: " Level="0">
                    <Value Value="Grade A" ValueType="3" />
                  </Column>
                  <Column ParamType="0" ID="20" ColumnID="7" ObjectKey="-1" Operator="Equal to" BitCmd="And" ControlType="dd" ParamDisplayMode="1" OfferAllOption="True" Ask="True" Caption="Exclude TBA: " Level="0">
                    <Value Value="(All)" ValueType="0" />
                  </Column>
                  <Column ParamType="0" ID="2" ColumnID="1163" ObjectKey="71" Operator="Greater than or equal to" BitCmd="And" ControlType="db" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="SVOD LSD" Level="0">
                    <Value Value="@Date.ThisMonthStart~" ValueType="1" />
                  </Column>
                  <Column ParamType="0" ID="3" ColumnID="1161" ObjectKey="71" Operator="Less than or equal to" BitCmd="And" ControlType="db" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="SVOD LED" Level="0">
                    <Value Value="@Date.ThisMonthEnd~" ValueType="1" />
                  </Column>
                  <Column ParamType="0" ID="21" ColumnID="3" ObjectKey="-1" Operator="Is not null" BitCmd="And" Ask="False" Caption="" Level="0" />
                </Parameter>
              </DataStructure>
            </DataStructures>
            <Contents>
              <Content ItemID="1" ItemIndex="1" ContentID="2" ContentName="Table" ContentTitle="">
                <Data DataStructureID="1" />
                <DataTable Caption="Strategic Rights Report SVOD" ShowRecordCount="False" ShowRowNumber="False" UseInteractivePaging="True" UseNumberedList="False" UseAppendRowsPaging="False" PagingLocation="0" InteractivePagingRows="50" InteractivePagingSubRows="50" SummaryRowLocationBottom="True" ShowSummaryRowOnce="True" TableWidth="-1" TableWidthScale="px">
                  <Column ColumnType="1" ColumnID="1131" ObjectKey="71" Header="Studio" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="325" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1129" ObjectKey="71" Header="Title" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1707" ObjectKey="71" Header="Grade" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="100" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1130" ObjectKey="71" Header="YOP" Format="General Number" Alignment="Right" HasSort="False" IsLinked="False" Width="30" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="3" ObjectKey="-1" Header="Type:" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="90" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="2" ObjectKey="-1" Header="SVOD Only" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="70" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="5" ObjectKey="-1" Header="Released" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="70" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="4" ObjectKey="-1" Header="Release Date" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="180" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1132" ObjectKey="71" Header="Linear LSD" Format="Short Date" Alignment="Right" HasSort="False" IsLinked="False" Width="90" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1133" ObjectKey="71" Header="Est" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="30" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1134" ObjectKey="71" Header="Linear LED" Format="Short Date" Alignment="Right" HasSort="False" IsLinked="False" Width="90" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1135" ObjectKey="71" Header="Est" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="30" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1136" ObjectKey="71" Header="Sky Go LSD" Format="Short Date" Alignment="Right" HasSort="False" IsLinked="False" Width="90" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1137" ObjectKey="71" Header="Est" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="30" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1138" ObjectKey="71" Header="Sky Go LED" Format="Short Date" Alignment="Right" HasSort="False" IsLinked="False" Width="90" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1139" ObjectKey="71" Header="Est" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="30" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1140" ObjectKey="71" Header="Restriction" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="150" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1141" ObjectKey="71" Header="Start Date" Format="Short Date" Alignment="Right" HasSort="False" IsLinked="False" Width="90" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1142" ObjectKey="71" Header="Est" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="30" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1143" ObjectKey="71" Header="End Date" Format="Short Date" Alignment="Right" HasSort="False" IsLinked="False" Width="90" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1144" ObjectKey="71" Header="Est" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="30" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1145" ObjectKey="71" Header="Anytime+  LSD" Format="Short Date" Alignment="Right" HasSort="False" IsLinked="False" Width="90" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1146" ObjectKey="71" Header="Est" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="30" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1147" ObjectKey="71" Header="Anytime+  LED" Format="Short Date" Alignment="Right" HasSort="False" IsLinked="False" Width="90" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1148" ObjectKey="71" Header="Est" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="30" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1149" ObjectKey="71" Header="Linear to PC Streaming" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="55" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1150" ObjectKey="71" Header="Mobile" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="55" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1151" ObjectKey="71" Header="Now TVSS" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="55" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1152" ObjectKey="71" Header="Now TV You View" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="55" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1153" ObjectKey="71" Header="Now TV Mobile" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="55" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1154" ObjectKey="71" Header="Sky Go Extra" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="55" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1155" ObjectKey="71" Header="Deal Code" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="90" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1156" ObjectKey="71" Header="Deal Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1158" ObjectKey="71" Header="Date Updated" Format="Short Date" Alignment="Right" HasSort="False" IsLinked="False" Width="75" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="2529" ObjectKey="71" Header="Genre" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="70" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="2530" ObjectKey="71" Header="Duration" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="70" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="3054" ObjectKey="71" Header="SDR" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="30" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="3055" ObjectKey="71" Header="HDR" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="30" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="2565" ObjectKey="71" Header="Certification" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="70" Scale="px" VisualizationType="None" ShowData="1" />
                </DataTable>
                <XGroupBy GroupType="0" PageBreakAfterGroup="False" FlatGroupingRollUpDetails="False" ShowExpandCollapse="False" AutoExpandDrillDown="False" />
              </Content>
              <Content ItemID="2" ItemIndex="2" ContentID="3" ContentName="Crosstab" ContentTitle="">
                <Data DataStructureID="1" />
                <Crosstab Caption="" UseInteractivePaging="True" UseNumberedList="False" UseAppendRowsPaging="False" PagingLocation="0" InteractivePagingRows="15" ShowExpandCollapse="False" AutoExpandDrillDown="False" CrosstabColumnID="6" CrosstabObjectKey="-1" CrosstabColumnHeader="Calculated Columns.SVOD Movies" CTHeaderTimePeriod="">
                  <LabelColumn LabelColumnID="3" LabelObjectKey="-1" LabelHeader="Type" LabelFormat="" SortLabelColumn="False" LabelAlignment="Left" IsLinked="False" />
                  <ValueColumn ValueColumnUniqueID="1" ValueColumnID="1164" ValueFormat="General Number" SortValueColumn="False" ValueAlignment="Right" ValueObjectKey="71" ValueFunction="Count" ValueHeader="ID_ASSET Count" IncludeSummaryRow="False" IncludeSummaryColumn="False" SummaryRowCaption="" SummaryColHeader="" SummaryRowFunction="Sum" SummaryColFunction="Sum" SummaryRowFormat="General Number" SummaryColFormat="General Number" />
                </Crosstab>
              </Content>
              <Content ItemID="3" ItemIndex="3" ContentID="11" ContentName="Exports" ContentTitle="">
                <Exports LinkSearchable="False" LinkPrintable="False" LinkExcel="True" LinkWord="False" LinkPPT="False" LinkPdf="False" LinkCSV="False" LinkXML="False" LinkArchive="False" SendMail="False" SendMailExportType="PDF" />
              </Content>
            </Contents>
            <SessionParameter />
            <General Template="Custom-Blue" PrintablePageOrientation="Landscape" ShowPrintablePageNr="ON" PrintablePageType="A4" />
          </AdhocMetadata>
        </Definition>
        <Dependencies>
          <Dependency Type="Object" ID="71" ObjectSchema="RLANLTCS" ObjectName="MV_RB_RPT_STRATEGIC_RIGHT_SVOD">
            <Column ID="1147" ColumnName="Anytime+ LED" />
            <Column ID="1148" ColumnName="Anytime+ LED Est" />
            <Column ID="1145" ColumnName="Anytime+ LSD" />
            <Column ID="1146" ColumnName="Anytime+ LSD Est" />
            <Column ID="2565" ColumnName="Certification" />
            <Column ID="1158" ColumnName="Date Updated" />
            <Column ID="1155" ColumnName="Deal Code" />
            <Column ID="1156" ColumnName="Deal Name" />
            <Column ID="2530" ColumnName="Duration" />
            <Column ID="2529" ColumnName="Genre" />
            <Column ID="3055" ColumnName="HDR" />
            <Column ID="1164" ColumnName="ID_ASSET" />
            <Column ID="1134" ColumnName="Linear LED" />
            <Column ID="1135" ColumnName="Linear LED Est" />
            <Column ID="1132" ColumnName="Linear LSD" />
            <Column ID="1133" ColumnName="Linear LSD Est" />
            <Column ID="1149" ColumnName="Linear to PC Streaming" />
            <Column ID="1150" ColumnName="Mobile" />
            <Column ID="1153" ColumnName="Now TV Mobile" />
            <Column ID="1151" ColumnName="Now TV SS" />
            <Column ID="1152" ColumnName="Now TV You View" />
            <Column ID="1710" ColumnName="Premier" />
            <Column ID="1140" ColumnName="Restriction" />
            <Column ID="1709" ColumnName="Scheduled End Date" />
            <Column ID="1708" ColumnName="Scheduled Start Date" />
            <Column ID="3054" ColumnName="SDR" />
            <Column ID="1154" ColumnName="Sky Go Extra" />
            <Column ID="1138" ColumnName="Sky Go LED" />
            <Column ID="1139" ColumnName="Sky Go LED Est" />
            <Column ID="1136" ColumnName="Sky Go LSD" />
            <Column ID="1137" ColumnName="Sky Go LSD Est" />
            <Column ID="1131" ColumnName="Studio" />
            <Column ID="1163" ColumnName="SVOD LED" />
            <Column ID="1144" ColumnName="SVOD LED Est Restr" />
            <Column ID="1143" ColumnName="SVOD LED Restr" />
            <Column ID="1161" ColumnName="SVOD LSD" />
            <Column ID="1142" ColumnName="SVOD LSD Est Restr" />
            <Column ID="1141" ColumnName="SVOD LSD Restr" />
            <Column ID="1706" ColumnName="TBA" />
            <Column ID="1129" ColumnName="Title" />
            <Column ID="1707" ColumnName="Title Grade" />
            <Column ID="1130" ColumnName="YOP" />
          </Dependency>
          <Dependency Type="Object" ID="94" ObjectSchema="" ObjectName="VV_RB_SVOD_BO_TREE">
            <Column ID="1657" ColumnName="Business Outlet" />
          </Dependency>
          <Dependency Type="Object" ID="96" ObjectSchema="" ObjectName="VV_RB_SVOD_MOVIES_TYPE">
            <Column ID="1712" ColumnName="Type" />
          </Dependency>
          <Dependency Type="Relation" ID="74" RelationName="Strategic Rights SVOD -&gt; SVOD Business Outlet Tree" Relation="Inner Join" ObjectID1="71" ObjectID2="94" />
          <Dependency Type="Relation" ID="78" RelationName="Strategic Rights SVOD -&gt; SVOD Movies Type" Relation="Inner Join" ObjectID1="71" ObjectID2="96" />
          <Dependency Type="DataFormat" ID="1" FormatName="General Number" Format="General Number" />
          <Dependency Type="DataFormat" ID="13" FormatName="Short Date" Format="Short Date" />
        </Dependencies>
      </Report>
      <Report ElementID="318" ReportName="Ratul RLUP 95 With Audit" Description="" PhysicalName="" ExpirationDate="" FolderType="0" ReportGUID="ahReport318_334c5eda-a563-4fd4-ad42-26506e434188" Owner="Admin" ParentFolderID="0" Dashboard="0" Mobile="0">
        <Definition>
          <AdhocMetadata DefaultActiveItemID="1" ActiveStepIndex="1">
            <DataStructures>
              <DataStructure ID="1" Distinct="True" CategoryID="6">
                <DataSource ObjectID="16" ObjectKey="16" ObjectSchema="" ObjectName="VV_RB_Deals" Object1DatabaseID="" Object1Schema="" Object1Name="" Column1Name="" Object2DatabaseID="" Object2Schema="" Object2Name="" Column2Name="" Relation="" RelationID="0" RelationDirection="0" DatabaseID="1">
                  <DataSource ObjectID="13" ObjectKey="16_66_13_1" ObjectSchema="" ObjectName="VV_RB_Deal_Navigator" Object1DatabaseID="1" Object1Schema="" Object1Name="VV_RB_Deal_Navigator" Column1Name="DEAL_PK" Object2DatabaseID="1" Object2Schema="" Object2Name="VV_RB_Deals" Column2Name="DEAL_PK" Relation="Inner Join" RelationID="66" RelationDirection="1" DatabaseID="1">
                    <DataSource ObjectID="22" ObjectKey="16_66_13_1_64_22_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS" Object1DatabaseID="1" Object1Schema="" Object1Name="VV_RB_Deal_Navigator" Column1Name="DEAL_ASSET_DETAIL_PK" Object2DatabaseID="1" Object2Schema="RLANLTCS" Object2Name="MV_RB_DEAL_MEDIA_RIGHTS" Column2Name="DEAL_ASSET_DETAIL_PK" Relation="Left Outer Join" RelationID="64" RelationDirection="0" DatabaseID="1">
                      <DataSource ObjectID="19" ObjectKey="16_66_13_1_64_22_0_136_19_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_EXHIBITION_WINDOWS" Object1DatabaseID="1" Object1Schema="RLANLTCS" Object1Name="MV_RB_DEAL_MEDIA_RIGHTS" Column1Name="DEAL_ASSET_DETAIL_PK" Object2DatabaseID="1" Object2Schema="RLANLTCS" Object2Name="MV_RB_DEAL_EXHIBITION_WINDOWS" Column2Name="DEAL_ASSET_DETAIL_PK" Relation="Left Outer Join" RelationID="136" RelationDirection="0" DatabaseID="1">
                        <JoinColumns>
                          <JoinOn Column1Name="Row No" Column2Name="Media Row No" />
                        </JoinColumns>
                      </DataSource>
                    </DataSource>
                    <DataSource ObjectID="67" ObjectKey="16_66_13_1_67_67_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ASSETS" Object1DatabaseID="1" Object1Schema="" Object1Name="VV_RB_Deal_Navigator" Column1Name="ASSET_PK" Object2DatabaseID="1" Object2Schema="RLANLTCS" Object2Name="MV_RB_ASSETS" Column2Name="ASSET_PK" Relation="Right Outer Join" RelationID="67" RelationDirection="0" DatabaseID="1" />
                  </DataSource>
                  <DataSource ObjectID="23" ObjectKey="16_137_23_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS_AUDIT" Object1DatabaseID="1" Object1Schema="" Object1Name="VV_RB_Deals" Column1Name="DEAL_PK" Object2DatabaseID="1" Object2Schema="RLANLTCS" Object2Name="MV_RB_DEAL_MEDIA_RIGHTS_AUDIT" Column2Name="DEAL_PK" Relation="Left Outer Join" RelationID="137" RelationDirection="0" DatabaseID="1" />
                </DataSource>
                <Objects>
                  <Object ObjectID="16" ObjectKey="16" ObjectSchema="" ObjectName="VV_RB_Deals" ObjectIdentifier="16_0_0">
                    <Columns>
                      <Column ColumnID="197" ColumnName="Deal Description" />
                      <Column ColumnID="198" ColumnName="Deal Name" />
                      <Column ColumnID="209" ColumnName="Agreement Type" />
                      <Column ColumnID="205" ColumnName="Profit Center" />
                      <Column ColumnID="1670" ColumnName="Primary Party" />
                      <Column ColumnID="3043" ColumnName="Non Outlet Party" />
                    </Columns>
                  </Object>
                  <Object ObjectID="22" ObjectKey="16_66_13_1_64_22_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS" ObjectIdentifier="22_64_0">
                    <Columns>
                      <Column ColumnID="392" ColumnName="Row No" />
                      <Column ColumnID="354" ColumnName="Applies To" />
                      <Column ColumnID="365" ColumnName="Term From" />
                      <Column ColumnID="368" ColumnName="Estimated Term From" />
                      <Column ColumnID="366" ColumnName="Term To" />
                      <Column ColumnID="374" ColumnName="Estimated Term To" />
                      <Column ColumnID="369" ColumnName="Type" />
                    </Columns>
                  </Object>
                  <Object ObjectID="19" ObjectKey="16_66_13_1_64_22_0_136_19_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_EXHIBITION_WINDOWS" ObjectIdentifier="19_136_0">
                    <Columns>
                      <Column ColumnID="282" ColumnName="Start Date" />
                      <Column ColumnID="284" ColumnName="End Date" />
                    </Columns>
                  </Object>
                  <Object ObjectID="67" ObjectKey="16_66_13_1_67_67_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ASSETS" ObjectIdentifier="67_67_0">
                    <Columns>
                      <Column ColumnID="1043" ColumnName="Asset ID" />
                      <Column ColumnID="1045" ColumnName="Asset Title" />
                      <Column ColumnID="1044" ColumnName="Asset Type" />
                    </Columns>
                  </Object>
                  <Object ObjectID="23" ObjectKey="16_137_23_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS_AUDIT" ObjectIdentifier="23_137_0">
                    <Columns>
                      <Column ColumnID="2590" ColumnName="Created Date" />
                    </Columns>
                  </Object>
                </Objects>
                <OrderBy />
                <Parameter>
                  <Column ParamType="0" ID="1" ColumnID="365" ObjectKey="16_66_13_1_64_22_0" Operator="Not equal to" BitCmd="And" Ask="False" Caption="" Level="0">
                    <Value Value="16_66_13_1_64_22_0_136_19_0.282" ValueType="2">
                      <Column ColumnID="282" ObjectKey="16_66_13_1_64_22_0_136_19_0" ColumnName="Start Date" />
                    </Value>
                  </Column>
                  <Column ParamType="0" ID="3" ColumnID="366" ObjectKey="16_66_13_1_64_22_0" Operator="Not equal to" BitCmd="Or" Ask="False" Caption="" Level="1">
                    <Value Value="16_66_13_1_64_22_0_136_19_0.284" ValueType="2">
                      <Column ColumnID="284" ObjectKey="16_66_13_1_64_22_0_136_19_0" ColumnName="End Date" />
                    </Value>
                  </Column>
                  <Column ParamType="0" ID="5" ColumnID="369" ObjectKey="16_66_13_1_64_22_0" Operator="Equal to" BitCmd="And" Ask="False" Caption="" Level="0">
                    <Value Value="Master" ValueType="0" />
                  </Column>
                  <Column ParamType="0" ID="6" ColumnID="1670" ObjectKey="16" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="" Level="0" />
                  <Column ParamType="0" ID="7" ColumnID="2590" ObjectKey="16_137_23_0" Operator="Greater than or equal to" BitCmd="And" ControlType="db" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="" Level="0">
                    <Value Value="" ValueType="0" />
                  </Column>
                </Parameter>
              </DataStructure>
            </DataStructures>
            <Contents>
              <Content ItemID="1" ItemIndex="1" ContentID="2" ContentName="Table" ContentTitle="">
                <Data DataStructureID="1" />
                <DataTable Caption="" ShowRecordCount="False" ShowRowNumber="False" UseInteractivePaging="True" UseNumberedList="False" UseAppendRowsPaging="False" PagingLocation="0" InteractivePagingRows="100" InteractivePagingSubRows="50" SummaryRowLocationBottom="False" ShowSummaryRowOnce="True" TableWidth="-1" TableWidthScale="px">
                  <Column ColumnType="1" ColumnID="197" ObjectKey="16" Header="Deal Code" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="198" ObjectKey="16" Header="Deal Name" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="209" ObjectKey="16" Header="Deal Type" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="205" ObjectKey="16" Header="Profit Center" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="3043" ObjectKey="16" Header="Party" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1043" ObjectKey="16_66_13_1_67_67_0" Header="Title ID" Format="General Number" Alignment="Right" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1045" ObjectKey="16_66_13_1_67_67_0" Header="Title Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1044" ObjectKey="16_66_13_1_67_67_0" Header="Title Type" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="392" ObjectKey="16_66_13_1_64_22_0" Header="Row No" Format="General Number" Alignment="Right" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="354" ObjectKey="16_66_13_1_64_22_0" Header="Applies To" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="368" ObjectKey="16_66_13_1_64_22_0" Header="Estimated Term From" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="365" ObjectKey="16_66_13_1_64_22_0" Header="Term From" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="374" ObjectKey="16_66_13_1_64_22_0" Header="Estimated Term To" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="366" ObjectKey="16_66_13_1_64_22_0" Header="Term To" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="282" ObjectKey="16_66_13_1_64_22_0_136_19_0" Header="Start Date" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="284" ObjectKey="16_66_13_1_64_22_0_136_19_0" Header="End Date" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                </DataTable>
                <XGroupBy GroupType="0" PageBreakAfterGroup="False" FlatGroupingRollUpDetails="False" ShowExpandCollapse="False" AutoExpandDrillDown="False" />
              </Content>
              <Content ItemID="2" ItemIndex="2" ContentID="11" ContentName="Exports" ContentTitle="">
                <Exports LinkSearchable="False" LinkPrintable="False" LinkExcel="True" LinkWord="True" LinkPPT="False" LinkPdf="True" LinkCSV="False" LinkXML="False" LinkArchive="False" SendMail="False" SendMailExportType="PDF" />
              </Content>
            </Contents>
            <SessionParameter />
            <General Template="Custom-Blue" PrintablePageOrientation="Portrait" ShowPrintablePageNr="ON" PrintablePageType="Letter" />
          </AdhocMetadata>
        </Definition>
        <Dependencies>
          <Dependency Type="Object" ID="67" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ASSETS">
            <Column ID="1043" ColumnName="Asset ID" />
            <Column ID="1045" ColumnName="Asset Title" />
            <Column ID="1044" ColumnName="Asset Type" />
          </Dependency>
          <Dependency Type="Object" ID="19" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_EXHIBITION_WINDOWS">
            <Column ID="284" ColumnName="End Date" />
            <Column ID="282" ColumnName="Start Date" />
          </Dependency>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="354" ColumnName="Applies To" />
            <Column ID="368" ColumnName="Estimated Term From" />
            <Column ID="374" ColumnName="Estimated Term To" />
            <Column ID="392" ColumnName="Row No" />
            <Column ID="365" ColumnName="Term From" />
            <Column ID="366" ColumnName="Term To" />
            <Column ID="369" ColumnName="Type" />
          </Dependency>
          <Dependency Type="Object" ID="23" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS_AUDIT">
            <Column ID="2590" ColumnName="Created Date" />
          </Dependency>
          <Dependency Type="Object" ID="16" ObjectSchema="" ObjectName="VV_RB_Deals">
            <Column ID="209" ColumnName="Agreement Type" />
            <Column ID="197" ColumnName="Deal Description" />
            <Column ID="198" ColumnName="Deal Name" />
            <Column ID="3043" ColumnName="Non Outlet Party" />
            <Column ID="1670" ColumnName="Primary Party" />
            <Column ID="205" ColumnName="Profit Center" />
          </Dependency>
          <Dependency Type="Relation" ID="67" RelationName="Assets -&gt; Deal Navigator" Relation="Right Outer Join" ObjectID1="13" ObjectID2="67" />
          <Dependency Type="Relation" ID="66" RelationName="Deal Navigator -&gt; Deals" Relation="Inner Join" ObjectID1="13" ObjectID2="16" />
          <Dependency Type="Relation" ID="64" RelationName="Deal Navigator -&gt; Media Rights" Relation="Left Outer Join" ObjectID1="13" ObjectID2="22" />
          <Dependency Type="Relation" ID="137" RelationName="Deals -&gt; Media Rights Audit" Relation="Left Outer Join" ObjectID1="16" ObjectID2="23" />
          <Dependency Type="Relation" ID="136" RelationName="Media Rights -&gt; Exhibitions" Relation="Left Outer Join" ObjectID1="22" ObjectID2="19" />
          <Dependency Type="DataFormat" ID="1" FormatName="General Number" Format="General Number" />
          <Dependency Type="DataFormat" ID="13" FormatName="Short Date" Format="Short Date" />
          <Dependency Type="SessionParameter" ID="1" ParameterName="spUserId" />
        </Dependencies>
      </Report>
      <Report ElementID="319" ReportName="Ratul RLUP 95 without Audit" Description="" PhysicalName="" ExpirationDate="" FolderType="0" ReportGUID="ahReport319_dd8956d4-d3e2-489d-bd5f-5b647ead5222" Owner="Admin" ParentFolderID="0" Dashboard="0" Mobile="0">
        <Definition>
          <AdhocMetadata DefaultActiveItemID="1" ActiveStepIndex="1">
            <DataStructures>
              <DataStructure ID="1" Distinct="True" CategoryID="6">
                <DataSource ObjectID="16" ObjectKey="16" ObjectSchema="" ObjectName="VV_RB_Deals" Object1DatabaseID="" Object1Schema="" Object1Name="" Column1Name="" Object2DatabaseID="" Object2Schema="" Object2Name="" Column2Name="" Relation="" RelationID="0" RelationDirection="0" DatabaseID="1">
                  <DataSource ObjectID="13" ObjectKey="16_66_13_1" ObjectSchema="" ObjectName="VV_RB_Deal_Navigator" Object1DatabaseID="1" Object1Schema="" Object1Name="VV_RB_Deal_Navigator" Column1Name="DEAL_PK" Object2DatabaseID="1" Object2Schema="" Object2Name="VV_RB_Deals" Column2Name="DEAL_PK" Relation="Inner Join" RelationID="66" RelationDirection="1" DatabaseID="1">
                    <DataSource ObjectID="22" ObjectKey="16_66_13_1_64_22_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS" Object1DatabaseID="1" Object1Schema="" Object1Name="VV_RB_Deal_Navigator" Column1Name="DEAL_ASSET_DETAIL_PK" Object2DatabaseID="1" Object2Schema="RLANLTCS" Object2Name="MV_RB_DEAL_MEDIA_RIGHTS" Column2Name="DEAL_ASSET_DETAIL_PK" Relation="Left Outer Join" RelationID="64" RelationDirection="0" DatabaseID="1">
                      <DataSource ObjectID="19" ObjectKey="16_66_13_1_64_22_0_136_19_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_EXHIBITION_WINDOWS" Object1DatabaseID="1" Object1Schema="RLANLTCS" Object1Name="MV_RB_DEAL_MEDIA_RIGHTS" Column1Name="DEAL_ASSET_DETAIL_PK" Object2DatabaseID="1" Object2Schema="RLANLTCS" Object2Name="MV_RB_DEAL_EXHIBITION_WINDOWS" Column2Name="DEAL_ASSET_DETAIL_PK" Relation="Left Outer Join" RelationID="136" RelationDirection="0" DatabaseID="1">
                        <JoinColumns>
                          <JoinOn Column1Name="Row No" Column2Name="Media Row No" />
                        </JoinColumns>
                      </DataSource>
                    </DataSource>
                    <DataSource ObjectID="67" ObjectKey="16_66_13_1_67_67_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ASSETS" Object1DatabaseID="1" Object1Schema="" Object1Name="VV_RB_Deal_Navigator" Column1Name="ASSET_PK" Object2DatabaseID="1" Object2Schema="RLANLTCS" Object2Name="MV_RB_ASSETS" Column2Name="ASSET_PK" Relation="Right Outer Join" RelationID="67" RelationDirection="0" DatabaseID="1" />
                  </DataSource>
                </DataSource>
                <Objects>
                  <Object ObjectID="16" ObjectKey="16" ObjectSchema="" ObjectName="VV_RB_Deals" ObjectIdentifier="16_0_0">
                    <Columns>
                      <Column ColumnID="197" ColumnName="Deal Description" />
                      <Column ColumnID="198" ColumnName="Deal Name" />
                      <Column ColumnID="209" ColumnName="Agreement Type" />
                      <Column ColumnID="205" ColumnName="Profit Center" />
                      <Column ColumnID="1670" ColumnName="Primary Party" />
                      <Column ColumnID="3043" ColumnName="Non Outlet Party" />
                    </Columns>
                  </Object>
                  <Object ObjectID="22" ObjectKey="16_66_13_1_64_22_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS" ObjectIdentifier="22_64_0">
                    <Columns>
                      <Column ColumnID="392" ColumnName="Row No" />
                      <Column ColumnID="354" ColumnName="Applies To" />
                      <Column ColumnID="365" ColumnName="Term From" />
                      <Column ColumnID="368" ColumnName="Estimated Term From" />
                      <Column ColumnID="366" ColumnName="Term To" />
                      <Column ColumnID="374" ColumnName="Estimated Term To" />
                      <Column ColumnID="369" ColumnName="Type" />
                    </Columns>
                  </Object>
                  <Object ObjectID="19" ObjectKey="16_66_13_1_64_22_0_136_19_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_EXHIBITION_WINDOWS" ObjectIdentifier="19_136_0">
                    <Columns>
                      <Column ColumnID="282" ColumnName="Start Date" />
                      <Column ColumnID="284" ColumnName="End Date" />
                    </Columns>
                  </Object>
                  <Object ObjectID="67" ObjectKey="16_66_13_1_67_67_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ASSETS" ObjectIdentifier="67_67_0">
                    <Columns>
                      <Column ColumnID="1043" ColumnName="Asset ID" />
                      <Column ColumnID="1045" ColumnName="Asset Title" />
                      <Column ColumnID="1044" ColumnName="Asset Type" />
                    </Columns>
                  </Object>
                </Objects>
                <OrderBy />
                <Parameter>
                  <Column ParamType="0" ID="1" ColumnID="365" ObjectKey="16_66_13_1_64_22_0" Operator="Not equal to" BitCmd="And" Ask="False" Caption="" Level="0">
                    <Value Value="16_66_13_1_64_22_0_136_19_0.282" ValueType="2">
                      <Column ColumnID="282" ObjectKey="16_66_13_1_64_22_0_136_19_0" ColumnName="Start Date" />
                    </Value>
                  </Column>
                  <Column ParamType="0" ID="3" ColumnID="366" ObjectKey="16_66_13_1_64_22_0" Operator="Not equal to" BitCmd="Or" Ask="False" Caption="" Level="1">
                    <Value Value="16_66_13_1_64_22_0_136_19_0.284" ValueType="2">
                      <Column ColumnID="284" ObjectKey="16_66_13_1_64_22_0_136_19_0" ColumnName="End Date" />
                    </Value>
                  </Column>
                  <Column ParamType="0" ID="5" ColumnID="369" ObjectKey="16_66_13_1_64_22_0" Operator="Equal to" BitCmd="And" Ask="False" Caption="" Level="0">
                    <Value Value="Master" ValueType="0" />
                  </Column>
                  <Column ParamType="0" ID="6" ColumnID="1670" ObjectKey="16" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="" Level="0" />
                </Parameter>
              </DataStructure>
            </DataStructures>
            <Contents>
              <Content ItemID="1" ItemIndex="1" ContentID="2" ContentName="Table" ContentTitle="">
                <Data DataStructureID="1" />
                <DataTable Caption="" ShowRecordCount="False" ShowRowNumber="False" UseInteractivePaging="True" UseNumberedList="False" UseAppendRowsPaging="False" PagingLocation="0" InteractivePagingRows="100" InteractivePagingSubRows="50" SummaryRowLocationBottom="False" ShowSummaryRowOnce="True" TableWidth="-1" TableWidthScale="px">
                  <Column ColumnType="1" ColumnID="197" ObjectKey="16" Header="Deal Code" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="198" ObjectKey="16" Header="Deal Name" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="209" ObjectKey="16" Header="Deal Type" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="205" ObjectKey="16" Header="Profit Center" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="3043" ObjectKey="16" Header="Party" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1043" ObjectKey="16_66_13_1_67_67_0" Header="Title ID" Format="General Number" Alignment="Right" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1045" ObjectKey="16_66_13_1_67_67_0" Header="Title Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="1044" ObjectKey="16_66_13_1_67_67_0" Header="Title Type" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="392" ObjectKey="16_66_13_1_64_22_0" Header="Row No" Format="General Number" Alignment="Right" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="354" ObjectKey="16_66_13_1_64_22_0" Header="Applies To" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="368" ObjectKey="16_66_13_1_64_22_0" Header="Estimated Term From" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="365" ObjectKey="16_66_13_1_64_22_0" Header="Term From" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="374" ObjectKey="16_66_13_1_64_22_0" Header="Estimated Term To" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="366" ObjectKey="16_66_13_1_64_22_0" Header="Term To" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="282" ObjectKey="16_66_13_1_64_22_0_136_19_0" Header="Start Date" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="284" ObjectKey="16_66_13_1_64_22_0_136_19_0" Header="End Date" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                </DataTable>
                <XGroupBy GroupType="0" PageBreakAfterGroup="False" FlatGroupingRollUpDetails="False" ShowExpandCollapse="False" AutoExpandDrillDown="False" />
              </Content>
              <Content ItemID="2" ItemIndex="2" ContentID="11" ContentName="Exports" ContentTitle="">
                <Exports LinkSearchable="False" LinkPrintable="False" LinkExcel="True" LinkWord="True" LinkPPT="False" LinkPdf="True" LinkCSV="False" LinkXML="False" LinkArchive="False" SendMail="False" SendMailExportType="PDF" />
              </Content>
            </Contents>
            <SessionParameter />
            <General Template="Custom-Blue" PrintablePageOrientation="Portrait" ShowPrintablePageNr="ON" PrintablePageType="Letter" />
          </AdhocMetadata>
        </Definition>
        <Dependencies>
          <Dependency Type="Object" ID="67" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ASSETS">
            <Column ID="1043" ColumnName="Asset ID" />
            <Column ID="1045" ColumnName="Asset Title" />
            <Column ID="1044" ColumnName="Asset Type" />
          </Dependency>
          <Dependency Type="Object" ID="19" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_EXHIBITION_WINDOWS">
            <Column ID="284" ColumnName="End Date" />
            <Column ID="282" ColumnName="Start Date" />
          </Dependency>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="354" ColumnName="Applies To" />
            <Column ID="368" ColumnName="Estimated Term From" />
            <Column ID="374" ColumnName="Estimated Term To" />
            <Column ID="392" ColumnName="Row No" />
            <Column ID="365" ColumnName="Term From" />
            <Column ID="366" ColumnName="Term To" />
            <Column ID="369" ColumnName="Type" />
          </Dependency>
          <Dependency Type="Object" ID="16" ObjectSchema="" ObjectName="VV_RB_Deals">
            <Column ID="209" ColumnName="Agreement Type" />
            <Column ID="197" ColumnName="Deal Description" />
            <Column ID="198" ColumnName="Deal Name" />
            <Column ID="3043" ColumnName="Non Outlet Party" />
            <Column ID="1670" ColumnName="Primary Party" />
            <Column ID="205" ColumnName="Profit Center" />
          </Dependency>
          <Dependency Type="Relation" ID="67" RelationName="Assets -&gt; Deal Navigator" Relation="Right Outer Join" ObjectID1="13" ObjectID2="67" />
          <Dependency Type="Relation" ID="66" RelationName="Deal Navigator -&gt; Deals" Relation="Inner Join" ObjectID1="13" ObjectID2="16" />
          <Dependency Type="Relation" ID="64" RelationName="Deal Navigator -&gt; Media Rights" Relation="Left Outer Join" ObjectID1="13" ObjectID2="22" />
          <Dependency Type="Relation" ID="136" RelationName="Media Rights -&gt; Exhibitions" Relation="Left Outer Join" ObjectID1="22" ObjectID2="19" />
          <Dependency Type="DataFormat" ID="1" FormatName="General Number" Format="General Number" />
          <Dependency Type="DataFormat" ID="13" FormatName="Short Date" Format="Short Date" />
          <Dependency Type="SessionParameter" ID="1" ParameterName="spUserId" />
        </Dependencies>
      </Report>
    </Task>
    <Task Type="Object">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" IncludeLinks="False" IncludeAccessRights="False" PreserveCategories="1" DeleteUnmatched="False" />
      <Object ElementID="71" ObjectSchema="RLANLTCS" ObjectName="MV_RB_RPT_STRATEGIC_RIGHT_SVOD" Description="Strategic Rights SVOD" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="1129" ColumnName="Title" Description="Title" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1130" ColumnName="YOP" Description="YOP" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1131" ColumnName="Studio" Description="Studio" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="500" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1132" ColumnName="Linear LSD" Description="Linear LSD" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1133" ColumnName="Linear LSD Est" Description="Linear LSD Est" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="129" CharacterMaxLen="1" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1134" ColumnName="Linear LED" Description="Linear LED" ColumnType="D" OrdinalPosition="5" ColumnOrder="6" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1135" ColumnName="Linear LED Est" Description="Linear LED Est" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="129" CharacterMaxLen="1" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1136" ColumnName="Sky Go LSD" Description="Sky Go LSD" ColumnType="D" OrdinalPosition="7" ColumnOrder="8" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1137" ColumnName="Sky Go LSD Est" Description="Sky Go LSD Est" ColumnType="D" OrdinalPosition="8" ColumnOrder="9" DataType="129" CharacterMaxLen="1" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1138" ColumnName="Sky Go LED" Description="Sky Go LED" ColumnType="D" OrdinalPosition="9" ColumnOrder="10" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1139" ColumnName="Sky Go LED Est" Description="Sky Go LED Est" ColumnType="D" OrdinalPosition="10" ColumnOrder="11" DataType="129" CharacterMaxLen="1" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1140" ColumnName="Restriction" Description="Restriction" ColumnType="D" OrdinalPosition="11" ColumnOrder="12" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1141" ColumnName="SVOD LSD Restr" Description="SVOD LSD Restr" ColumnType="D" OrdinalPosition="12" ColumnOrder="13" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1142" ColumnName="SVOD LSD Est Restr" Description="SVOD LSD Est Restr" ColumnType="D" OrdinalPosition="13" ColumnOrder="14" DataType="129" CharacterMaxLen="1" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1143" ColumnName="SVOD LED Restr" Description="SVOD LED Restr" ColumnType="D" OrdinalPosition="14" ColumnOrder="15" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1144" ColumnName="SVOD LED Est Restr" Description="SVOD LED Est Restr" ColumnType="D" OrdinalPosition="15" ColumnOrder="16" DataType="129" CharacterMaxLen="1" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1145" ColumnName="Anytime+ LSD" Description="Anytime  LSD" ColumnType="D" OrdinalPosition="16" ColumnOrder="17" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1146" ColumnName="Anytime+ LSD Est" Description="Anytime  LSD Est" ColumnType="D" OrdinalPosition="17" ColumnOrder="18" DataType="129" CharacterMaxLen="1" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1147" ColumnName="Anytime+ LED" Description="Anytime  LED" ColumnType="D" OrdinalPosition="18" ColumnOrder="19" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1148" ColumnName="Anytime+ LED Est" Description="Anytime  LED Est" ColumnType="D" OrdinalPosition="19" ColumnOrder="20" DataType="129" CharacterMaxLen="1" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1149" ColumnName="Linear to PC Streaming" Description="Linear to PC Streaming" ColumnType="D" OrdinalPosition="20" ColumnOrder="21" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1150" ColumnName="Mobile" Description="Mobile" ColumnType="D" OrdinalPosition="21" ColumnOrder="22" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1151" ColumnName="Now TV SS" Description="Now TV SS" ColumnType="D" OrdinalPosition="22" ColumnOrder="23" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1152" ColumnName="Now TV You View" Description="Now TV You View" ColumnType="D" OrdinalPosition="23" ColumnOrder="24" DataType="129" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1153" ColumnName="Now TV Mobile" Description="Now TV Mobile" ColumnType="D" OrdinalPosition="24" ColumnOrder="25" DataType="129" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1154" ColumnName="Sky Go Extra" Description="Sky Go Extra" ColumnType="D" OrdinalPosition="25" ColumnOrder="26" DataType="129" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1155" ColumnName="Deal Code" Description="Deal Code" ColumnType="D" OrdinalPosition="26" ColumnOrder="27" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1156" ColumnName="Deal Name" Description="Deal Name" ColumnType="D" OrdinalPosition="27" ColumnOrder="28" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1157" ColumnName="Contract Data Entry Status" Description="Contract Data Entry Status" ColumnType="D" OrdinalPosition="28" ColumnOrder="29" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1158" ColumnName="Date Updated" Description="Date Updated" ColumnType="D" OrdinalPosition="29" ColumnOrder="30" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1159" ColumnName="ID_APPLIESTO_GROUP_SVOD" Description="ID_APPLIESTO_GROUP_SVOD" ColumnType="D" OrdinalPosition="29" ColumnOrder="31" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1160" ColumnName="ID_APPLIESTO_GROUP_PAY" Description="ID_APPLIESTO_GROUP_PAY" ColumnType="D" OrdinalPosition="30" ColumnOrder="32" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1165" ColumnName="ID_BUSINESS_OUTLET_TM" Description="ID_BUSINESS_OUTLET_TM" ColumnType="D" OrdinalPosition="40" ColumnOrder="33" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1161" ColumnName="SVOD LSD" Description="SVOD LSD" ColumnType="D" OrdinalPosition="32" ColumnOrder="33" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1167" ColumnName="Asset Type" Description="Title Type" ColumnType="D" OrdinalPosition="42" ColumnOrder="34" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1162" ColumnName="ID_DEAL" Description="ID_DEAL" ColumnType="D" OrdinalPosition="31" ColumnOrder="34" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1163" ColumnName="SVOD LED" Description="SVOD LED" ColumnType="D" OrdinalPosition="33" ColumnOrder="35" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1704" ColumnName="Master Media Type" Description="Master Media Type" ColumnType="D" OrdinalPosition="34" ColumnOrder="35" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1705" ColumnName="RESTR_ENDUSER_RIGHTS" Description="RESTR_ENDUSER_RIGHTS" ColumnType="D" OrdinalPosition="35" ColumnOrder="36" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1164" ColumnName="ID_ASSET" Description="ID_ASSET" ColumnType="D" OrdinalPosition="32" ColumnOrder="36" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1706" ColumnName="TBA" Description="TBA" ColumnType="D" OrdinalPosition="41" ColumnOrder="42" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1707" ColumnName="Title Grade" Description="Title Grade" ColumnType="D" OrdinalPosition="42" ColumnOrder="43" DataType="200" CharacterMaxLen="2000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2529" ColumnName="Genre" Description="Genre" ColumnType="D" OrdinalPosition="43" ColumnOrder="44" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1708" ColumnName="Scheduled Start Date" Description="Scheduled Start Date" ColumnType="D" OrdinalPosition="43" ColumnOrder="44" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3054" ColumnName="SDR" Description="SDR" ColumnType="D" OrdinalPosition="44" ColumnOrder="45" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2530" ColumnName="Duration" Description="Duration" ColumnType="D" OrdinalPosition="44" ColumnOrder="45" DataType="200" CharacterMaxLen="8" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1709" ColumnName="Scheduled End Date" Description="Scheduled End Date" ColumnType="D" OrdinalPosition="44" ColumnOrder="45" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3055" ColumnName="HDR" Description="HDR" ColumnType="D" OrdinalPosition="45" ColumnOrder="46" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1710" ColumnName="Premier" Description="Premier" ColumnType="D" OrdinalPosition="45" ColumnOrder="46" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2565" ColumnName="Certification" Description="Certification" ColumnType="D" OrdinalPosition="46" ColumnOrder="47" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="4" CategoryName="Custom Entities" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="94" ObjectSchema="" ObjectName="VV_RB_SVOD_BO_TREE" Description="SVOD Businee Outlet Tree" Type="VV" Definition="select * from mv_rb_items_bus_outlet t where upper(&quot;Parent&quot;) in ('SKY CINEMA CHANNELS','SKY 3D CHANNELS') &#xA;or &quot;Business Outlet&quot; like '%.SKY CINEMA CHANNELS' or &quot;Business Outlet&quot; like '%.SKY 3D CHANNELS'" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="1657" ColumnName="Business Outlet" Description="Business Outlet" ColumnType="V" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Business Outlet" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2725" ColumnName="BUSINESS_OUTLET_TM_ITEM_PK" Description="BUSINESS_OUTLET_TM_ITEM_PK" ColumnType="V" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="BUSINESS_OUTLET_TM_ITEM_PK" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1658" ColumnName="Parent" Description="Parent" ColumnType="V" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Parent" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1659" ColumnName="ID_BUSINESS_OUTLET_TM" Description="ID_BUSINESS_OUTLET_TM" ColumnType="V" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="ID_BUSINESS_OUTLET_TM" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2726" ColumnName="Business Outlet Items" Description="Business Outlet Items" ColumnType="V" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Business Outlet Items" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2536" ColumnName="Parent Level 2" Description="Parent Level 2" ColumnType="V" OrdinalPosition="5" ColumnOrder="6" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Parent Level 2" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2539" ColumnName="ID_BUSINESS_OUTLET_ITEM" Description="ID_BUSINESS_OUTLET_ITEM" ColumnType="V" OrdinalPosition="6" ColumnOrder="7" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="ID_BUSINESS_OUTLET_ITEM" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2537" ColumnName="Parent Level 3" Description="Parent Level 3" ColumnType="V" OrdinalPosition="6" ColumnOrder="7" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Parent Level 3" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2538" ColumnName="Business Outlet Flat" Description="Business Outlet Flat" ColumnType="V" OrdinalPosition="7" ColumnOrder="8" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Business Outlet Flat" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2727" ColumnName="Created Date" Description="Created Date" ColumnType="V" OrdinalPosition="9" ColumnOrder="10" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="0" Definition="Created Date" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2728" ColumnName="Updated Date" Description="Updated Date" ColumnType="V" OrdinalPosition="10" ColumnOrder="11" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="0" Definition="Updated Date" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="4" CategoryName="Custom Entities" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="96" ObjectSchema="" ObjectName="VV_RB_SVOD_MOVIES_TYPE" Description="SVOD Movies Type" Type="VV" Definition="select id_asset,'New' &quot;Type&quot; from mv_rb_rpt_strategic_right_svod&#xD;&#xA;union &#xD;&#xA;select id_asset,'Unscheduled' &quot;Type&quot; from mv_rb_rpt_strategic_right_svod &#xD;&#xA;union&#xD;&#xA;select id_asset,'Expiring' &quot;Type&quot; from mv_rb_rpt_strategic_right_svod &#xD;&#xA;union&#xD;&#xA;select id_asset,'Continuing' &quot;Type&quot; from mv_rb_rpt_strategic_right_svod&#xD;&#xA;union&#xD;&#xA;select id_asset,'Premier' &quot;Type&quot; from mv_rb_rpt_strategic_right_svod" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="1711" ColumnName="ID_ASSET" Description="ID_ASSET" ColumnType="V" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="ID_ASSET" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1712" ColumnName="Type" Description="Type" ColumnType="V" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Type" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="67" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ASSETS" Description="Titles" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="1090" ColumnName="Airing Order" Description="Airing Order" ColumnType="D" OrdinalPosition="48" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1676" ColumnName="Asset SubType" Description="Title Sub Type" ColumnType="D" OrdinalPosition="22" ColumnOrder="2" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1675" ColumnName="Asset Type Concat" Description="Title Type Concat" ColumnType="D" OrdinalPosition="3" ColumnOrder="3" DataType="200" CharacterMaxLen="513" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1042" ColumnName="ASSET_PK" Description="ASSET_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="4" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1060" ColumnName="MPAA Rating" Description="Certification" ColumnType="D" OrdinalPosition="18" ColumnOrder="5" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1072" ColumnName="Close Captioned" Description="Close Captioned" ColumnType="D" OrdinalPosition="31" ColumnOrder="6" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1092" ColumnName="Contributors" Description="Contributors" ColumnType="D" OrdinalPosition="49" ColumnOrder="7" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1686" ColumnName="Contributors Flat" Description="Contributors Flat" ColumnType="D" OrdinalPosition="55" ColumnOrder="8" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1078" ColumnName="Copyright Year" Description="Copyright Year" ColumnType="D" OrdinalPosition="37" ColumnOrder="9" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1091" ColumnName="Created By" Description="Created By" ColumnType="D" OrdinalPosition="49" ColumnOrder="10" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1093" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="50" ColumnOrder="11" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1079" ColumnName="Credits In (hh:mm:ss:tt)" Description="Credits In (hh:mm:ss:tt)" ColumnType="D" OrdinalPosition="38" ColumnOrder="12" DataType="200" CharacterMaxLen="48" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Time" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1080" ColumnName="Credits Out (hh:mm:ss:tt)" Description="Credits Out (hh:mm:ss:tt)" ColumnType="D" OrdinalPosition="39" ColumnOrder="13" DataType="200" CharacterMaxLen="48" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Time" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2767" ColumnName="Seasons" Description="Seasons" ColumnType="D" OrdinalPosition="12" ColumnOrder="14" DataType="200" CharacterMaxLen="457" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1063" ColumnName="Delivery Format" Description="Delivery Format" ColumnType="D" OrdinalPosition="22" ColumnOrder="15" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1081" ColumnName="Disclaimer" Description="Disclaimer" ColumnType="D" OrdinalPosition="40" ColumnOrder="16" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1073" ColumnName="Dubbed" Description="Dubbed" ColumnType="D" OrdinalPosition="32" ColumnOrder="17" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1052" ColumnName="Duration (hh:mm:ss)" Description="Duration (hh:mm:ss)" ColumnType="D" OrdinalPosition="10" ColumnOrder="18" DataType="200" CharacterMaxLen="8" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1068" ColumnName="Duration (Secs)" Description="Duration (Secs)" ColumnType="D" OrdinalPosition="27" ColumnOrder="19" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1053" ColumnName="Short Name" Description="EPG Name" ColumnType="D" OrdinalPosition="11" ColumnOrder="20" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1067" ColumnName="Asset Description" Description="Episode Notes" ColumnType="D" OrdinalPosition="26" ColumnOrder="21" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1069" ColumnName="Episode Number" Description="Episode Number" ColumnType="D" OrdinalPosition="28" ColumnOrder="22" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1680" ColumnName="Genre" Description="EPG Genre Flat" ColumnType="D" OrdinalPosition="59" ColumnOrder="23" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1688" ColumnName="Genre Flat" Description="EPG Sub Genre" ColumnType="D" OrdinalPosition="60" ColumnOrder="24" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1083" ColumnName="Guild Association" Description="Guild Association" ColumnType="D" OrdinalPosition="42" ColumnOrder="25" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1065" ColumnName="HD Percentage" Description="HD Percentage" ColumnType="D" OrdinalPosition="24" ColumnOrder="26" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1066" ColumnName="HD Type" Description="HD Type" ColumnType="D" OrdinalPosition="25" ColumnOrder="27" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1055" ColumnName="Inactive Reason" Description="Inactive Reason" ColumnType="D" OrdinalPosition="13" ColumnOrder="28" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1057" ColumnName="Internal" Description="Internal" ColumnType="D" OrdinalPosition="15" ColumnOrder="29" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1046" ColumnName="IP Name" Description="IP Name" ColumnType="D" OrdinalPosition="4" ColumnOrder="30" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1679" ColumnName="Keyword" Description="Keyword" ColumnType="D" OrdinalPosition="58" ColumnOrder="31" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1071" ColumnName="Language" Description="Language" ColumnType="D" OrdinalPosition="30" ColumnOrder="32" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1689" ColumnName="Library Title" Description="Library Title" ColumnType="D" OrdinalPosition="73" ColumnOrder="33" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1064" ColumnName="Master Audio" Description="Master Audio" ColumnType="D" OrdinalPosition="23" ColumnOrder="34" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1690" ColumnName="Movie ID" Description="Movie ID" ColumnType="D" OrdinalPosition="7" ColumnOrder="35" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1077" ColumnName="Nielsen No" Description="Nielsen No" ColumnType="D" OrdinalPosition="36" ColumnOrder="36" DataType="202" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1076" ColumnName="# of Episodes" Description="No of Episodes" ColumnType="D" OrdinalPosition="35" ColumnOrder="37" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1058" ColumnName="Non-Airable" Description="Non-Airable" ColumnType="D" OrdinalPosition="16" ColumnOrder="38" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1691" ColumnName="Notes Concat" Description="Notes Concat" ColumnType="D" OrdinalPosition="61" ColumnOrder="39" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1692" ColumnName="PPS Usage Report" Description="PPS Usage Report" ColumnType="D" OrdinalPosition="20" ColumnOrder="40" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1084" ColumnName="Production No" Description="Production No" ColumnType="D" OrdinalPosition="43" ColumnOrder="41" DataType="202" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1047" ColumnName="Project ID" Description="Project ID" ColumnType="D" OrdinalPosition="5" ColumnOrder="42" DataType="200" CharacterMaxLen="200" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1075" ColumnName="Rating Warning" Description="Rating Warning" ColumnType="D" OrdinalPosition="34" ColumnOrder="43" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1695" ColumnName="Restricted Notes" Description="Restricted Notes" ColumnType="D" OrdinalPosition="30" ColumnOrder="44" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1087" ColumnName="Season Closer" Description="Season Closer" ColumnType="D" OrdinalPosition="45" ColumnOrder="45" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1086" ColumnName="Season Opener" Description="Season Opener" ColumnType="D" OrdinalPosition="44" ColumnOrder="46" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1089" ColumnName="Series Closer" Description="Series Closer" ColumnType="D" OrdinalPosition="47" ColumnOrder="47" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1698" ColumnName="Series ID" Description="Series ID" ColumnType="D" OrdinalPosition="10" ColumnOrder="48" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1088" ColumnName="Series Opener" Description="Series Opener" ColumnType="D" OrdinalPosition="46" ColumnOrder="49" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1048" ColumnName="Series Title" Description="Series Title" ColumnType="D" OrdinalPosition="6" ColumnOrder="50" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1062" ColumnName="Shooting Format" Description="Shooting Format" ColumnType="D" OrdinalPosition="21" ColumnOrder="51" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1677" ColumnName="Source Asset Identifier" Description="Source ID" ColumnType="D" OrdinalPosition="47" ColumnOrder="52" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1051" ColumnName="Source Business Unit" Description="Source Business Unit" ColumnType="D" OrdinalPosition="9" ColumnOrder="53" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1699" ColumnName="Source ID" Description="Source ID" ColumnType="D" OrdinalPosition="8" ColumnOrder="54" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1085" ColumnName="Source System" Description="Source System" ColumnType="D" OrdinalPosition="43" ColumnOrder="55" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1074" ColumnName="Subtitled" Description="Subtitled" ColumnType="D" OrdinalPosition="33" ColumnOrder="56" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1700" ColumnName="Supplier Air Date" Description="Supplier Air Date" ColumnType="D" OrdinalPosition="72" ColumnOrder="57" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1082" ColumnName="Syndication #" Description="Syndication #" ColumnType="D" OrdinalPosition="41" ColumnOrder="58" DataType="200" CharacterMaxLen="40" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1678" ColumnName="Synopsis" Description="Synopsis" ColumnType="D" OrdinalPosition="57" ColumnOrder="59" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1701" ColumnName="Synopsis Concat" Description="Synopsis Concat" ColumnType="D" OrdinalPosition="66" ColumnOrder="60" DataType="200" CharacterMaxLen="7600" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1681" ColumnName="TBA" Description="TBA" ColumnType="D" OrdinalPosition="67" ColumnOrder="61" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1061" ColumnName="Asset Format" Description="Title Category" ColumnType="D" OrdinalPosition="20" ColumnOrder="62" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1702" ColumnName="Asset Hierarchy" Description="Title Hierarchy" ColumnType="D" OrdinalPosition="70" ColumnOrder="63" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1043" ColumnName="Asset ID" Description="Title ID" ColumnType="D" OrdinalPosition="1" ColumnOrder="64" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1045" ColumnName="Asset Title" Description="Title Name" ColumnType="D" OrdinalPosition="3" ColumnOrder="65" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1056" ColumnName="Asset Source" Description="Title Source" ColumnType="D" OrdinalPosition="14" ColumnOrder="66" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1054" ColumnName="Asset Status" Description="Title Status" ColumnType="D" OrdinalPosition="12" ColumnOrder="67" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1044" ColumnName="Asset Type" Description="Title Type" ColumnType="D" OrdinalPosition="2" ColumnOrder="68" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1094" ColumnName="Updated By" Description="Updated By" ColumnType="D" OrdinalPosition="51" ColumnOrder="69" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1095" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="52" ColumnOrder="70" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1049" ColumnName="Vendor" Description="Vendor" ColumnType="D" OrdinalPosition="7" ColumnOrder="71" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1050" ColumnName="Vendor 2" Description="Vendor 2" ColumnType="D" OrdinalPosition="8" ColumnOrder="72" DataType="200" CharacterMaxLen="500" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1070" ColumnName="Release Year" Description="Year of Production" ColumnType="D" OrdinalPosition="29" ColumnOrder="73" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2768" ColumnName="Highlight" Description="Highlight" ColumnType="D" OrdinalPosition="135" ColumnOrder="136" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="3" CategoryName="Title Maintenance" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="19" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_EXHIBITION_WINDOWS" Description="Exhibitions" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="272" ColumnName="DEAL_PK" Description="DEAL_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="273" ColumnName="EXHIBITION_PK" Description="EXHIBITION_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="274" ColumnName="Applies To" Description="Applies To" ColumnType="D" OrdinalPosition="1" ColumnOrder="3" DataType="200" CharacterMaxLen="1004" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="275" ColumnName="ASSET_PK" Description="ASSET_PK" ColumnType="D" OrdinalPosition="1" ColumnOrder="4" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="276" ColumnName="EXHIBITION_WINDOW_PK" Description="EXHIBITION_WINDOW_PK" ColumnType="D" OrdinalPosition="1" ColumnOrder="5" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="277" ColumnName="Unlimited" Description="Unlimited" ColumnType="D" OrdinalPosition="3" ColumnOrder="6" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="278" ColumnName="Exhib. Days" Description="Exhib Days" ColumnType="D" OrdinalPosition="4" ColumnOrder="7" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="279" ColumnName="DEAL_ASSET_DETAIL_PK" Description="DEAL_ASSET_DETAIL_PK" ColumnType="D" OrdinalPosition="5" ColumnOrder="8" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2571" ColumnName="BTTERM_GROUP_PK" Description="BTTERM_GROUP_PK" ColumnType="D" OrdinalPosition="7" ColumnOrder="8" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="280" ColumnName="Reserved Exhibitions" Description="Reserved Exhibitions" ColumnType="D" OrdinalPosition="5" ColumnOrder="9" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1664" ColumnName="Window Exhib. Days" Description="Window Exhib  Days" ColumnType="D" OrdinalPosition="8" ColumnOrder="10" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="281" ColumnName="Preview Plays" Description="Preview Plays" ColumnType="D" OrdinalPosition="6" ColumnOrder="11" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="282" ColumnName="Start Date" Description="Start Date" ColumnType="D" OrdinalPosition="9" ColumnOrder="12" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="283" ColumnName="Pooled" Description="Pooled" ColumnType="D" OrdinalPosition="7" ColumnOrder="13" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="284" ColumnName="End Date" Description="End Date" ColumnType="D" OrdinalPosition="10" ColumnOrder="14" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="285" ColumnName="Exhib.Day Scope" Description="Exhib Day Scope" ColumnType="D" OrdinalPosition="8" ColumnOrder="15" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="286" ColumnName="Notes" Description="Notes" ColumnType="D" OrdinalPosition="11" ColumnOrder="16" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="287" ColumnName="Restricted Notes" Description="Restricted Notes" ColumnType="D" OrdinalPosition="12" ColumnOrder="17" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="288" ColumnName="Window Name" Description="Window Name" ColumnType="D" OrdinalPosition="13" ColumnOrder="18" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="289" ColumnName="Rights Type" Description="Rights Type" ColumnType="D" OrdinalPosition="17" ColumnOrder="19" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="290" ColumnName="Window Type" Description="Window Type" ColumnType="D" OrdinalPosition="14" ColumnOrder="20" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="291" ColumnName="Window Start Date" Description="Window Start Date" ColumnType="D" OrdinalPosition="15" ColumnOrder="21" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2572" ColumnName="Notes Restricted" Description="Notes Restricted" ColumnType="D" OrdinalPosition="21" ColumnOrder="22" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="292" ColumnName="Window End Date" Description="Window End Date" ColumnType="D" OrdinalPosition="16" ColumnOrder="23" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2573" ColumnName="Notes Unrestricted" Description="Notes Unrestricted" ColumnType="D" OrdinalPosition="22" ColumnOrder="24" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="294" ColumnName="Day Period" Description="Day Period" ColumnType="D" OrdinalPosition="20" ColumnOrder="25" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="295" ColumnName="Daypart" Description="Daypart" ColumnType="D" OrdinalPosition="20" ColumnOrder="26" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="293" ColumnName="Territory" Description="Territory" ColumnType="D" OrdinalPosition="18" ColumnOrder="27" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="296" ColumnName="Exhib. Network" Description="Channel" ColumnType="D" OrdinalPosition="17" ColumnOrder="28" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="297" ColumnName="Channel Cap" Description="Channel Cap" ColumnType="D" OrdinalPosition="24" ColumnOrder="29" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="298" ColumnName="Plays" Description="Plays" ColumnType="D" OrdinalPosition="21" ColumnOrder="30" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="300" ColumnName="Day Type" Description="Day Type" ColumnType="D" OrdinalPosition="19" ColumnOrder="31" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="299" ColumnName="Run Cap" Description="Run Cap" ColumnType="D" OrdinalPosition="25" ColumnOrder="32" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="301" ColumnName="Addl.Exhib. Restriction" Description="Addl Exhib Restriction" ColumnType="D" OrdinalPosition="22" ColumnOrder="33" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="302" ColumnName="Premiere" Description="Premiere" ColumnType="D" OrdinalPosition="19" ColumnOrder="34" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="303" ColumnName="Evaluation" Description="Evaluation" ColumnType="D" OrdinalPosition="23" ColumnOrder="35" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="304" ColumnName="Updated By" Description="Updated By" ColumnType="D" OrdinalPosition="24" ColumnOrder="36" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1730" ColumnName="Media Type" Description="Media Type" ColumnType="D" OrdinalPosition="34" ColumnOrder="37" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="305" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="25" ColumnOrder="38" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="306" ColumnName="Created By" Description="Created By" ColumnType="D" OrdinalPosition="26" ColumnOrder="39" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1731" ColumnName="Start Time" Description="Start Time" ColumnType="D" OrdinalPosition="35" ColumnOrder="40" DataType="200" CharacterMaxLen="8" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1732" ColumnName="End Time" Description="End Time" ColumnType="D" OrdinalPosition="36" ColumnOrder="41" DataType="200" CharacterMaxLen="8" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="307" ColumnName="Media Row" Description="Media Row" ColumnType="D" OrdinalPosition="31" ColumnOrder="42" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="308" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="27" ColumnOrder="43" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="309" ColumnName="Max Exhib" Description="Max Exhib" ColumnType="D" OrdinalPosition="36" ColumnOrder="44" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="310" ColumnName="Enduser Rights" Description="Product Offering" ColumnType="D" OrdinalPosition="34" ColumnOrder="45" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2574" ColumnName="Network Type" Description="Channel Type" ColumnType="D" OrdinalPosition="45" ColumnOrder="46" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2575" ColumnName="Estimated Window Start Date" Description="Estimated Window Start Date" ColumnType="D" OrdinalPosition="78" ColumnOrder="47" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2576" ColumnName="Estimated Window End Date" Description="Estimated Window End Date" ColumnType="D" OrdinalPosition="79" ColumnOrder="48" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2577" ColumnName="Estimated Start Date" Description="Estimated Start Date" ColumnType="D" OrdinalPosition="76" ColumnOrder="49" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2578" ColumnName="Estimated End Date" Description="Estimated End Date" ColumnType="D" OrdinalPosition="77" ColumnOrder="50" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3031" ColumnName="Window Media Row" Description="Window Media Row" ColumnType="D" OrdinalPosition="62" ColumnOrder="63" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3032" ColumnName="Media Row No" Description="Media Row No" ColumnType="D" OrdinalPosition="63" ColumnOrder="64" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2579" ColumnName="Exhib Day Type" Description="Exhib Day Type" ColumnType="D" OrdinalPosition="83" ColumnOrder="84" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2580" ColumnName="Exhib Day Period" Description="Exhib Day Period" ColumnType="D" OrdinalPosition="84" ColumnOrder="85" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2581" ColumnName="Exhib Plays" Description="Exhib Plays" ColumnType="D" OrdinalPosition="85" ColumnOrder="86" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="6" CategoryName="Rights In" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS" Description="Media Rights" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="351" ColumnName="DEAL_PK" Description="DEAL_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="352" ColumnName="ASSET_PK" Description="ASSET_PK" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1749" ColumnName="MEDIA_RIGHT_TM_PK" Description="MEDIA_RIGHT_TM_PK" ColumnType="D" OrdinalPosition="3" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="355" ColumnName="DEAL_ASSET_DETAIL_PK" Description="DEAL_ASSET_DETAIL_PK" ColumnType="D" OrdinalPosition="2" ColumnOrder="4" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2586" ColumnName="BTTERM_GROUP_PK" Description="BTTERM_GROUP_PK" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1750" ColumnName="START_TERM_EVENT_PK" Description="START_TERM_EVENT_PK" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="392" ColumnName="Row No" Description="Row No" ColumnType="D" OrdinalPosition="6" ColumnOrder="6" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="354" ColumnName="Applies To" Description="Applies To" ColumnType="D" OrdinalPosition="7" ColumnOrder="7" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="353" ColumnName="Ext. Carve-Out" Description="Ext  Carve- Out" ColumnType="D" OrdinalPosition="8" ColumnOrder="8" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="356" ColumnName="Party" Description="Party" ColumnType="D" OrdinalPosition="9" ColumnOrder="9" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="357" ColumnName="Media Type" Description="Media Type" ColumnType="D" OrdinalPosition="10" ColumnOrder="10" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1682" ColumnName="Media Type Tree" Description="Media Type Tree" ColumnType="D" OrdinalPosition="11" ColumnOrder="11" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="358" ColumnName="Venue" Description="Venue" ColumnType="D" OrdinalPosition="12" ColumnOrder="12" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="359" ColumnName="Packaging" Description="Delivery Means" ColumnType="D" OrdinalPosition="13" ColumnOrder="13" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="360" ColumnName="Touchpoints" Description="Viewing Devices" ColumnType="D" OrdinalPosition="14" ColumnOrder="14" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="363" ColumnName="Business Outlets" Description="Channels" ColumnType="D" OrdinalPosition="16" ColumnOrder="15" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="364" ColumnName="Territory" Description="Territory" ColumnType="D" OrdinalPosition="17" ColumnOrder="16" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1753" ColumnName="Language Template" Description="Language Template" ColumnType="D" OrdinalPosition="18" ColumnOrder="17" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="361" ColumnName="Language" Description="Language" ColumnType="D" OrdinalPosition="19" ColumnOrder="18" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="362" ColumnName="Perpetuity" Description="Perpetuity" ColumnType="D" OrdinalPosition="20" ColumnOrder="19" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1754" ColumnName="Perpetuity Flag" Description="Perpetuity Flag" ColumnType="D" OrdinalPosition="21" ColumnOrder="20" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="365" ColumnName="Term From" Description="Term From" ColumnType="D" OrdinalPosition="22" ColumnOrder="21" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="368" ColumnName="Estimated Term From" Description="Estimated Term From" ColumnType="D" OrdinalPosition="23" ColumnOrder="22" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="366" ColumnName="Term To" Description="Term To" ColumnType="D" OrdinalPosition="24" ColumnOrder="23" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="374" ColumnName="Estimated Term To" Description="Estimated Term To" ColumnType="D" OrdinalPosition="25" ColumnOrder="24" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="367" ColumnName="Exclusivity" Description="Exclusivity" ColumnType="D" OrdinalPosition="26" ColumnOrder="25" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1756" ColumnName="Holdback" Description="Holdback" ColumnType="D" OrdinalPosition="27" ColumnOrder="26" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="369" ColumnName="Type" Description="Type" ColumnType="D" OrdinalPosition="28" ColumnOrder="27" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="371" ColumnName="Restriction" Description="Restriction" ColumnType="D" OrdinalPosition="29" ColumnOrder="28" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="370" ColumnName="Sublicense" Description="Sublicense" ColumnType="D" OrdinalPosition="30" ColumnOrder="29" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="378" ColumnName="Notes" Description="Notes" ColumnType="D" OrdinalPosition="31" ColumnOrder="30" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="375" ColumnName="Restricted Notes" Description="Restricted Notes" ColumnType="D" OrdinalPosition="32" ColumnOrder="31" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="379" ColumnName="End User Rights" Description="Product Offerings" ColumnType="D" OrdinalPosition="33" ColumnOrder="32" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2587" ColumnName="Notes Restricted" Description="Notes Restricted" ColumnType="D" OrdinalPosition="33" ColumnOrder="33" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="372" ColumnName="Blackout" Description="Blackout" ColumnType="D" OrdinalPosition="34" ColumnOrder="34" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2588" ColumnName="Notes Unrestricted" Description="Notes Unrestricted" ColumnType="D" OrdinalPosition="34" ColumnOrder="35" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="376" ColumnName="Customer" Description="Customer" ColumnType="D" OrdinalPosition="35" ColumnOrder="36" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="373" ColumnName="Publications" Description="Publications" ColumnType="D" OrdinalPosition="36" ColumnOrder="37" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="377" ColumnName="Min Value" Description="Min Value" ColumnType="D" OrdinalPosition="37" ColumnOrder="38" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="380" ColumnName="Max Value" Description="Max Value" ColumnType="D" OrdinalPosition="38" ColumnOrder="39" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1758" ColumnName="ID_MEDIA_RIGHT_TM" Description="ID_MEDIA_RIGHT_TM" ColumnType="D" OrdinalPosition="40" ColumnOrder="40" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="381" ColumnName="Units" Description="Units" ColumnType="D" OrdinalPosition="39" ColumnOrder="41" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="382" ColumnName="Minutes" Description="Minutes" ColumnType="D" OrdinalPosition="40" ColumnOrder="42" DataType="200" CharacterMaxLen="300" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1759" ColumnName="ID_TERRITORY_TM" Description="ID_TERRITORY_TM" ColumnType="D" OrdinalPosition="41" ColumnOrder="43" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="384" ColumnName="Refresh Percent" Description="Refresh Percent" ColumnType="D" OrdinalPosition="41" ColumnOrder="44" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="386" ColumnName="Quantity" Description="Quantity" ColumnType="D" OrdinalPosition="42" ColumnOrder="45" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1760" ColumnName="ID_PKGING_RIGHTS_TM" Description="ID_PKGING_RIGHTS_TM" ColumnType="D" OrdinalPosition="42" ColumnOrder="46" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1761" ColumnName="ID_LANGUAGE_TM" Description="ID_LANGUAGE_TM" ColumnType="D" OrdinalPosition="43" ColumnOrder="47" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="391" ColumnName="Usage Period" Description="Usage Period" ColumnType="D" OrdinalPosition="45" ColumnOrder="48" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="388" ColumnName="Usage Logical Operator" Description="Usage Logical Operator" ColumnType="D" OrdinalPosition="43" ColumnOrder="49" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="390" ColumnName="Usage Time Period" Description="Usage Time Period" ColumnType="D" OrdinalPosition="44" ColumnOrder="50" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1762" ColumnName="ID_VENUE_TM" Description="ID_VENUE_TM" ColumnType="D" OrdinalPosition="46" ColumnOrder="51" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1763" ColumnName="ID_TOUCHPOINT_TM" Description="ID_TOUCHPOINT_TM" ColumnType="D" OrdinalPosition="44" ColumnOrder="52" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1764" ColumnName="ID_ENDUSER_RIGHTS_TM" Description="ID_ENDUSER_RIGHTS_TM" ColumnType="D" OrdinalPosition="45" ColumnOrder="53" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1765" ColumnName="ID_BUSINESS_OUTLET_TM" Description="ID_BUSINESS_OUTLET_TM" ColumnType="D" OrdinalPosition="47" ColumnOrder="54" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="383" ColumnName="Updated By" Description="Updated By" ColumnType="D" OrdinalPosition="54" ColumnOrder="55" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="385" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="55" ColumnOrder="56" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="387" ColumnName="Created By" Description="Created By" ColumnType="D" OrdinalPosition="56" ColumnOrder="57" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="389" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="57" ColumnOrder="58" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="6" CategoryName="Rights In" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="23" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS_AUDIT" Description="Media Rights Audit" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2589" ColumnName="Created By" Description="Created By" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="202" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="394" ColumnName="Updated By" Description="Updated By" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="202" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2590" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2591" ColumnName="Created Time" Description="Created Time" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="397" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="398" ColumnName="Updated Time" Description="Updated Time" ColumnType="D" OrdinalPosition="5" ColumnOrder="6" DataType="200" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1766" ColumnName="DEAL_PK" Description="DEAL_PK" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="399" ColumnName="Record Type" Description="Record Type" ColumnType="D" OrdinalPosition="6" ColumnOrder="8" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1767" ColumnName="Tab" Description="Tab" ColumnType="D" OrdinalPosition="6" ColumnOrder="9" DataType="129" CharacterMaxLen="12" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="400" ColumnName="Deal Name" Description="Deal Name" ColumnType="D" OrdinalPosition="7" ColumnOrder="10" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="401" ColumnName="Deal Type" Description="Deal Type" ColumnType="D" OrdinalPosition="8" ColumnOrder="11" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="403" ColumnName="Field Name" Description="Field Name" ColumnType="D" OrdinalPosition="10" ColumnOrder="12" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1768" ColumnName="Deal Code" Description="Deal Code" ColumnType="D" OrdinalPosition="11" ColumnOrder="13" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="404" ColumnName="Old Value" Description="Old Value" ColumnType="D" OrdinalPosition="11" ColumnOrder="14" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="402" ColumnName="Source Name" Description="Source Name" ColumnType="D" OrdinalPosition="9" ColumnOrder="15" DataType="129" CharacterMaxLen="9" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="405" ColumnName="New Value" Description="New Value" ColumnType="D" OrdinalPosition="12" ColumnOrder="16" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1769" ColumnName="Row No" Description="Row No" ColumnType="D" OrdinalPosition="12" ColumnOrder="17" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="406" ColumnName="Deal ID" Description="Deal ID" ColumnType="D" OrdinalPosition="13" ColumnOrder="18" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="6" CategoryName="Rights In" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="16" ObjectSchema="" ObjectName="VV_RB_Deals" Description="Deals" Type="VV" Definition="select d.*,uod.id_user_orig from mv_rb_deals d, MV_RB_USER_OUTLET_DEALS uod&#xA;where d.deal_pk=uod.deal_pk" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="195" ColumnName="DEAL_PK" Description="DEAL_PK" ColumnType="V" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="DEAL_PK" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="196" ColumnName="Deal ID" Description="Deal ID" ColumnType="V" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Deal ID" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="197" ColumnName="Deal Description" Description="Deal Code" ColumnType="V" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Description" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1668" ColumnName="Contract No" Description="Contract No" ColumnType="V" OrdinalPosition="3" ColumnOrder="4" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Contract No" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="3033" ColumnName="Master Deal ID" Description="Master Deal ID" ColumnType="V" OrdinalPosition="3" ColumnOrder="5" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Master Deal ID" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3034" ColumnName="Master Agreement" Description="Master Agreement" ColumnType="V" OrdinalPosition="4" ColumnOrder="6" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Master Agreement" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3035" ColumnName="Business Unit" Description="Business Unit" ColumnType="V" OrdinalPosition="5" ColumnOrder="7" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Business Unit" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="199" ColumnName="Deal Type" Description="Agreement Type" ColumnType="V" OrdinalPosition="4" ColumnOrder="8" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Type" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="200" ColumnName="Contract Status" Description="Contract Status" ColumnType="V" OrdinalPosition="5" ColumnOrder="9" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Contract Status" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="198" ColumnName="Deal Name" Description="Deal Name" ColumnType="V" OrdinalPosition="8" ColumnOrder="9" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="203" ColumnName="Date Executed" Description="Approval Date" ColumnType="V" OrdinalPosition="7" ColumnOrder="10" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Date Executed" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1669" ColumnName="Deal Sub Type" Description="Agreement Sub Type" ColumnType="V" OrdinalPosition="6" ColumnOrder="10" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Sub Type" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="204" ColumnName="Date Effective" Description="Date Effective" ColumnType="V" OrdinalPosition="8" ColumnOrder="11" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Date Effective" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="3036" ColumnName="Internal Term Sheet Status" Description="Internal Term Sheet Status" ColumnType="V" OrdinalPosition="7" ColumnOrder="11" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Internal Term Sheet Status" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="202" ColumnName="Contract Data Entry Status" Description="Contract Data Entry Status" ColumnType="V" OrdinalPosition="6" ColumnOrder="12" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Contract Data Entry Status" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="206" ColumnName="Contract Currency" Description="Contract Currency" ColumnType="V" OrdinalPosition="10" ColumnOrder="14" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Contract Currency" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="205" ColumnName="Profit Center" Description="Profit Center" ColumnType="V" OrdinalPosition="15" ColumnOrder="16" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Profit Center" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="209" ColumnName="Agreement Type" Description="Deal Type" ColumnType="V" OrdinalPosition="13" ColumnOrder="16" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Agreement Type" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="207" ColumnName="Budget Line" Description="Budget Line" ColumnType="V" OrdinalPosition="17" ColumnOrder="18" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Budget Line" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="208" ColumnName="Source Name" Description="Source Name" ColumnType="V" OrdinalPosition="18" ColumnOrder="19" DataType="129" CharacterMaxLen="9" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Source Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1670" ColumnName="Primary Party" Description="Primary Party" ColumnType="V" OrdinalPosition="22" ColumnOrder="23" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Primary Party" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1671" ColumnName="Distributor Party" Description="Distributor Party" ColumnType="V" OrdinalPosition="23" ColumnOrder="24" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Distributor Party" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1673" ColumnName="Primary Business Outlet" Description="Primary Business Outlet" ColumnType="V" OrdinalPosition="26" ColumnOrder="25" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Primary Business Outlet" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="3037" ColumnName="Distributor Without Internal" Description="Distributor Without Internal" ColumnType="V" OrdinalPosition="24" ColumnOrder="25" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Distributor Without Internal" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1674" ColumnName="SAP Channel ID" Description="SAP Channel ID" ColumnType="V" OrdinalPosition="27" ColumnOrder="26" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="SAP Channel ID" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1672" ColumnName="Licensor Party" Description="Licensor Party" ColumnType="V" OrdinalPosition="25" ColumnOrder="26" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Licensor Party" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="222" ColumnName="Created By" Description="Created By" ColumnType="V" OrdinalPosition="28" ColumnOrder="27" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Created By" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="224" ColumnName="Created Date" Description="Created Date" ColumnType="V" OrdinalPosition="29" ColumnOrder="28" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="0" Definition="Created Date" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="223" ColumnName="Total Asset Value" Description="Total Title Value" ColumnType="V" OrdinalPosition="26" ColumnOrder="29" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="2-digit place holder" Alignment="Right" NativeDataType="0" Definition="Total Asset Value" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="226" ColumnName="Updated By" Description="Updated By" ColumnType="V" OrdinalPosition="30" ColumnOrder="30" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Updated By" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="225" ColumnName="No of Assets" Description="No of Titles" ColumnType="V" OrdinalPosition="27" ColumnOrder="31" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="No of Assets" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="227" ColumnName="Updated Date" Description="Updated Date" ColumnType="V" OrdinalPosition="31" ColumnOrder="32" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="0" Definition="Updated Date" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3038" ColumnName="Channel Entity" Description="Channel Entity" ColumnType="V" OrdinalPosition="32" ColumnOrder="33" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Channel Entity" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="3039" ColumnName="DM5 Doc Type" Description="DM5 Doc Type" ColumnType="V" OrdinalPosition="33" ColumnOrder="34" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="DM5 Doc Type" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="3040" ColumnName="Parties" Description="Parties" ColumnType="V" OrdinalPosition="34" ColumnOrder="35" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Parties" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="3041" ColumnName="Financial Status" Description="Financial Status" ColumnType="V" OrdinalPosition="35" ColumnOrder="36" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Financial Status" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="228" ColumnName="ID_USER_ORIG" Description="ID_USER_ORIG" ColumnType="V" OrdinalPosition="30" ColumnOrder="37" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="ID_USER_ORIG" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="3042" ColumnName="Client ID" Description="Client ID" ColumnType="V" OrdinalPosition="36" ColumnOrder="38" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Client ID" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="3043" ColumnName="Non Outlet Party" Description="Non Outlet Party" ColumnType="V" OrdinalPosition="37" ColumnOrder="39" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Non Outlet Party" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3044" ColumnName="Skip Workflow" Description="Skip Workflow" ColumnType="V" OrdinalPosition="38" ColumnOrder="40" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Skip Workflow" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3045" ColumnName="All Media Rights" Description="All Media Rights" ColumnType="V" OrdinalPosition="39" ColumnOrder="41" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="All Media Rights" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3046" ColumnName="All Promotion Rights" Description="All Promotion Rights" ColumnType="V" OrdinalPosition="40" ColumnOrder="42" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="All Promotion Rights" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3047" ColumnName="No Promotion Rights" Description="No Promotion Rights" ColumnType="V" OrdinalPosition="41" ColumnOrder="43" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="No Promotion Rights" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3048" ColumnName="All Editing Rights" Description="All Editing Rights" ColumnType="V" OrdinalPosition="42" ColumnOrder="44" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="All Editing Rights" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3049" ColumnName="No Editing Rights" Description="No Editing Rights" ColumnType="V" OrdinalPosition="43" ColumnOrder="45" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="No Editing Rights" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3050" ColumnName="ProMamS Contract Status" Description="ProMamS Contract Status" ColumnType="V" OrdinalPosition="44" ColumnOrder="46" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="ProMamS Contract Status" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="3051" ColumnName="No Revenue participation" Description="No Revenue participation" ColumnType="V" OrdinalPosition="45" ColumnOrder="47" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="No Revenue participation" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3052" ColumnName="Deal ID (Main)" Description="Deal ID (Main)" ColumnType="V" OrdinalPosition="46" ColumnOrder="48" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Deal ID (Main)" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="3053" ColumnName="Deal Name (Main)" Description="Deal Name (Main)" ColumnType="V" OrdinalPosition="47" ColumnOrder="49" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Name (Main)" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="6" CategoryName="Rights In" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="13" ObjectSchema="" ObjectName="VV_RB_Deal_Navigator" Description="Deal Navigator" Type="VV" Definition="select d.*,uod.id_user_orig from mv_rb_deal_navigator d, MV_RB_USER_OUTLET_DEALS uod&#xD;&#xA;where d.deal_pk=uod.deal_pk" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="153" ColumnName="DEAL_PK" Description="DEAL_PK" ColumnType="V" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="DEAL_PK" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="154" ColumnName="Deal ID" Description="Deal ID" ColumnType="V" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Deal ID" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="155" ColumnName="Deal Name" Description="Deal Name" ColumnType="V" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="156" ColumnName="ASSET_PK" Description="ASSET_PK" ColumnType="V" OrdinalPosition="3" ColumnOrder="4" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="ASSET_PK" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="157" ColumnName="Asset ID" Description="Title ID" ColumnType="V" OrdinalPosition="4" ColumnOrder="5" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Asset ID" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="158" ColumnName="DEAL_ASSET_DETAIL_PK" Description="DEAL_ASSET_DETAIL_PK" ColumnType="V" OrdinalPosition="4" ColumnOrder="6" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="DEAL_ASSET_DETAIL_PK" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="159" ColumnName="Asset Title" Description="Title Name" ColumnType="V" OrdinalPosition="5" ColumnOrder="7" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Asset Title" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="160" ColumnName="Asset Type" Description="Title Type" ColumnType="V" OrdinalPosition="6" ColumnOrder="8" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Asset Type" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="161" ColumnName="Series Name" Description="Series Name" ColumnType="V" OrdinalPosition="7" ColumnOrder="9" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Series Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="162" ColumnName="Applies To" Description="Applies To" ColumnType="V" OrdinalPosition="8" ColumnOrder="10" DataType="200" CharacterMaxLen="513" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Applies To" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="164" ColumnName="On Or Before" Description="On Or Before" ColumnType="V" OrdinalPosition="10" ColumnOrder="11" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="On Or Before" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="166" ColumnName="Letigation Pending" Description="Do Not Air" ColumnType="V" OrdinalPosition="9" ColumnOrder="12" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Letigation Pending" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="165" ColumnName="Program Do Not Air" Description="Program Do Not Air" ColumnType="V" OrdinalPosition="11" ColumnOrder="13" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Program Do Not Air" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="163" ColumnName="Season Name" Description="Season Name" ColumnType="V" OrdinalPosition="8" ColumnOrder="14" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Season Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="167" ColumnName="Source Name" Description="Source Name" ColumnType="V" OrdinalPosition="12" ColumnOrder="15" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Source Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="168" ColumnName="Created By" Description="Created By" ColumnType="V" OrdinalPosition="13" ColumnOrder="16" DataType="200" CharacterMaxLen="513" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Created By" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="169" ColumnName="Updated By" Description="Updated By" ColumnType="V" OrdinalPosition="14" ColumnOrder="17" DataType="200" CharacterMaxLen="513" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Updated By" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="170" ColumnName="Created Date" Description="Created Date" ColumnType="V" OrdinalPosition="15" ColumnOrder="18" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="0" Definition="Created Date" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="171" ColumnName="Updated Date" Description="Updated Date" ColumnType="V" OrdinalPosition="16" ColumnOrder="19" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="0" Definition="Updated Date" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="172" ColumnName="ID_USER_ORIG" Description="ID_USER_ORIG" ColumnType="V" OrdinalPosition="17" ColumnOrder="20" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="ID_USER_ORIG" Explanation="" HideColumn="1" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="6" CategoryName="Rights In" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
    </Task>
    <Task Type="Relation">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" />
      <Relation ElementID="74" RelationName="Strategic Rights SVOD -&gt; SVOD Business Outlet Tree" Description="" Relation="Inner Join" ObjectID1="71" ObjectID2="94" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="1165" ColumnName1="ID_BUSINESS_OUTLET_TM" ColumnID2="1659" ColumnName2="ID_BUSINESS_OUTLET_TM" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="71" ObjectSchema="" ObjectName="MV_RB_RPT_STRATEGIC_RIGHT_SVOD">
            <Column ID="1165" ColumnName="ID_BUSINESS_OUTLET_TM" />
          </Dependency>
          <Dependency Type="Object" ID="94" ObjectName="VV_RB_SVOD_BO_TREE">
            <Column ID="1659" ColumnName="ID_BUSINESS_OUTLET_TM" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="78" RelationName="Strategic Rights SVOD -&gt; SVOD Movies Type" Description="" Relation="Inner Join" ObjectID1="71" ObjectID2="96" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="1164" ColumnName1="ID_ASSET" ColumnID2="1711" ColumnName2="ID_ASSET" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="71" ObjectSchema="" ObjectName="MV_RB_RPT_STRATEGIC_RIGHT_SVOD">
            <Column ID="1164" ColumnName="ID_ASSET" />
          </Dependency>
          <Dependency Type="Object" ID="96" ObjectName="VV_RB_SVOD_MOVIES_TYPE">
            <Column ID="1711" ColumnName="ID_ASSET" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="67" RelationName="Assets -&gt; Deal Navigator" Description="" Relation="Right Outer Join" ObjectID1="13" ObjectID2="67" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="1">
        <RelationDetails>
          <RelationColumn ColumnID1="156" ColumnName1="ASSET_PK" ColumnID2="1042" ColumnName2="ASSET_PK" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="13" ObjectSchema="RLANLTCS" ObjectName="VV_RB_Deal_Navigator">
            <Column ID="156" ColumnName="ASSET_PK" />
          </Dependency>
          <Dependency Type="Object" ID="67" ObjectName="MV_RB_ASSETS">
            <Column ID="1042" ColumnName="ASSET_PK" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="66" RelationName="Deal Navigator -&gt; Deals" Description="" Relation="Inner Join" ObjectID1="13" ObjectID2="16" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="1">
        <RelationDetails>
          <RelationColumn ColumnID1="153" ColumnName1="DEAL_PK" ColumnID2="195" ColumnName2="DEAL_PK" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="13" ObjectSchema="" ObjectName="VV_RB_Deal_Navigator">
            <Column ID="153" ColumnName="DEAL_PK" />
          </Dependency>
          <Dependency Type="Object" ID="16" ObjectName="VV_RB_Deals">
            <Column ID="195" ColumnName="DEAL_PK" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="64" RelationName="Deal Navigator -&gt; Media Rights" Description="" Relation="Left Outer Join" ObjectID1="13" ObjectID2="22" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="1">
        <RelationDetails>
          <RelationColumn ColumnID1="158" ColumnName1="DEAL_ASSET_DETAIL_PK" ColumnID2="355" ColumnName2="DEAL_ASSET_DETAIL_PK" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="13" ObjectSchema="RLANLTCS" ObjectName="VV_RB_Deal_Navigator">
            <Column ID="158" ColumnName="DEAL_ASSET_DETAIL_PK" />
          </Dependency>
          <Dependency Type="Object" ID="22" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="355" ColumnName="DEAL_ASSET_DETAIL_PK" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="137" RelationName="Deals -&gt; Media Rights Audit" Description="" Relation="Left Outer Join" ObjectID1="16" ObjectID2="23" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="1">
        <RelationDetails>
          <RelationColumn ColumnID1="195" ColumnName1="DEAL_PK" ColumnID2="1766" ColumnName2="DEAL_PK" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="16" ObjectSchema="RLANLTCS" ObjectName="VV_RB_Deals">
            <Column ID="195" ColumnName="DEAL_PK" />
          </Dependency>
          <Dependency Type="Object" ID="23" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS_AUDIT">
            <Column ID="1766" ColumnName="DEAL_PK" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="136" RelationName="Media Rights -&gt; Exhibitions" Description="" Relation="Left Outer Join" ObjectID1="22" ObjectID2="19" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="355" ColumnName1="DEAL_ASSET_DETAIL_PK" ColumnID2="279" ColumnName2="DEAL_ASSET_DETAIL_PK" />
          <RelationColumn ColumnID1="392" ColumnName1="Row No" ColumnID2="3032" ColumnName2="Media Row No" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="355" ColumnName="DEAL_ASSET_DETAIL_PK" />
            <Column ID="392" ColumnName="Row No" />
          </Dependency>
          <Dependency Type="Object" ID="19" ObjectName="MV_RB_DEAL_EXHIBITION_WINDOWS">
            <Column ID="279" ColumnName="DEAL_ASSET_DETAIL_PK" />
            <Column ID="3032" ColumnName="Media Row No" />
          </Dependency>
        </Dependencies>
      </Relation>
    </Task>
    <Task Type="DataFormat">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" />
      <DataFormat ElementID="1" FormatKey="GeneralNumber" FormatName="General Number" Format="General Number" Internal="1" AppliesTo="9" Explanation="" IsAvailable="1" SortOrder="1" />
      <DataFormat ElementID="13" FormatKey="ShortDate" FormatName="Short Date" Format="Short Date" Internal="1" AppliesTo="10" Explanation="" IsAvailable="1" SortOrder="13" />
    </Task>
    <Task Type="SessionParameter">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" />
      <SessionParameter ElementID="1" ParameterName="spUserId" DefaultValue="1" DataTypeCategory="1" />
    </Task>
  </Tasks>
  <AppSettings CopyConnectionString="False" CopyGeneralSettings="False" CopyUIFeatures="False" CopyReportingOptions="False" CopyReportingFeatures="False" CopySecuritySettings="False" MultipleOrganizations="False" UniqueReportFolderNames="False" CopySchedulerServiceSettings="False" CopySchedulingConfiguration="False" CopySMTPServerSettings="False" CopyEmailConfiguration="False" CopyArchiveConfiguration="False" CopyArchiveSettings="False" CopyEventLogDBConnection="False" CopyEventLogConfiguration="False">
    <Database ElementID="1" MaxReportRecords="500000" MaxListRecords="100" DesignTimeValidation="Off" DefaultTemplate="Custom-Blue" />
    <Organization ElementID="1" GroupName="Default" Description="" DefaultTheme="Rights" />
  </AppSettings>
</SynchronizationPackage>