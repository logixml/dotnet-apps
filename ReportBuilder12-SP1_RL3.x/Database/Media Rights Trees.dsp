<SynchronizationPackage SrcInstanceGUID="647128f6-d29c-4984-a7d2-d7f09f3aa467" SrcConnectionID="1" SrcGroupID="1" SrcVersion="12.1.43.1" IsSimpleView="False">
  <SelectedElements>
    <Element Type="Object" SelectAll="False" OverwriteExisting="True" OverwriteExistingDependencies="False" IncludeLinks="False" IncludeAccessRights="False" PreserveCategories="1" DeleteUnmatched="False">
      <Object ElementID="132" />
      <Object ElementID="133" />
      <Object ElementID="134" />
      <Object ElementID="135" />
      <Object ElementID="136" />
      <Object ElementID="137" />
      <Object ElementID="138" />
      <Object ElementID="139" />
      <Object ElementID="140" />
      <Object ElementID="141" />
      <Object ElementID="142" />
      <Object ElementID="143" />
      <Object ElementID="144" />
      <Object ElementID="145" />
      <Object ElementID="92" />
    </Element>
    <Element Type="Relation" SelectAll="False" OverwriteExisting="True" OverwriteExistingDependencies="False">
      <Relation ElementID="139" />
      <Relation ElementID="140" />
      <Relation ElementID="141" />
      <Relation ElementID="142" />
      <Relation ElementID="143" />
      <Relation ElementID="145" />
      <Relation ElementID="136" />
      <Relation ElementID="146" />
    </Element>
  </SelectedElements>
  <Tasks>
    <Task Type="Object">
      <SynchronizationOptions OverwriteExisting="True" OverwriteExistingDependencies="False" IncludeLinks="False" IncludeAccessRights="False" PreserveCategories="1" DeleteUnmatched="False" />
      <Object ElementID="132" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_AIRING_NETWORK" Description="Airing Network Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2451" ColumnName="AIRING_NETWORK_PK" Description="AIRING_NETWORK_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2452" ColumnName="Airing Network" Description="Airing Network" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2756" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2757" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="133" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_BUSINESS_TERMS" Description="Business Terms Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2453" ColumnName="Applies To" Description="Applies To" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2454" ColumnName="Business Category" Description="Business Category" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2455" ColumnName="Template Name" Description="Template Name" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2456" ColumnName="Notes" Description="Notes" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="134" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_DRM_RIGHTS" Description="DRM Rights Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2457" ColumnName="DRM Rights" Description="DRM Rights" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2458" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2459" ColumnName="ID_DRM_TM" Description="ID_DRM_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2510" ColumnName="Parent Level 2" Description="Parent Level 2" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2511" ColumnName="Parent Level 3" Description="Parent Level 3" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2512" ColumnName="DRM Rights Flat" Description="DRM Rights Flat" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2513" ColumnName="ID_DRM_ITEM" Description="ID_DRM_ITEM" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="135" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_EXHIB_NETWORK" Description="Exhib Network Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2460" ColumnName="EXHIBITION_WINDOW_PK" Description="EXHIBITION_WINDOW_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2461" ColumnName="Exhib Network" Description="Exhib Network" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2758" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2759" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="136" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_NETWORK" Description="Networks Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2462" ColumnName="Network" Description="Network" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2463" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2464" ColumnName="ID_NETWORK_TM" Description="ID_NETWORK_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="137" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_EDIT_RIGHTS" Description="Editing Rights Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2465" ColumnName="Editing Rights" Description="Editing Rights" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2466" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2467" ColumnName="ID_EDITING_RIGHTS_TM" Description="ID_EDITING_RIGHTS_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2939" ColumnName="Parent Level 2" Description="Parent Level 2" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2940" ColumnName="Parent Level 3" Description="Parent Level 3" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2941" ColumnName="Editing Rights Flat" Description="Editing Rights Flat" ColumnType="D" OrdinalPosition="5" ColumnOrder="6" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2942" ColumnName="ID_EDITING_RIGHTS_ITEM" Description="ID_EDITING_RIGHTS_ITEM" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2943" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="7" ColumnOrder="8" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2944" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="8" ColumnOrder="9" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="138" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_ENDUSR_RIGHTS" Description="Product Offering Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2468" ColumnName="Enduser Rights" Description="Product Offerings" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2469" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2470" ColumnName="ID_ENDUSER_RIGHTS_TM" Description="ID_ENDUSER_RIGHTS_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2514" ColumnName="Parent Level 2" Description="Parent Level 2" ColumnType="D" OrdinalPosition="2" ColumnOrder="4" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2515" ColumnName="Parent Level 3" Description="Parent Level 3" ColumnType="D" OrdinalPosition="3" ColumnOrder="5" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2516" ColumnName="Enduser Rights Flat" Description="Product Offerings Flat" ColumnType="D" OrdinalPosition="4" ColumnOrder="6" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2517" ColumnName="ID_ENDUSER_RIGHTS_ITEM" Description="ID_ENDUSER_RIGHTS_ITEM" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="139" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_LANGUAGE" Description="Language Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2471" ColumnName="Language" Description="Language" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2472" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2473" ColumnName="ID_LANGUAGE_TM" Description="ID_LANGUAGE_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2981" ColumnName="Language Items" Description="Language Items" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2982" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2983" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="140" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_MEDIA_RIGHTS" Description="Media Rights Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2474" ColumnName="Media Rights" Description="Media Rights" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2475" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2518" ColumnName="Parent Level 2" Description="Parent Level 2" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2476" ColumnName="ID_MEDIA_RIGHT_TM" Description="ID_MEDIA_RIGHT_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2519" ColumnName="Parent Level 3" Description="Parent Level 3" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2520" ColumnName="Media Rights Flat" Description="Media Rights Flat" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2521" ColumnName="ID_MEDIA_RIGHT_ITEM" Description="ID_MEDIA_RIGHT_ITEM" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2969" ColumnName="Media Rights Items" Description="Media Rights Items" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2970" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="8" ColumnOrder="9" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2971" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="9" ColumnOrder="10" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="141" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_PKGING_RIGHTS" Description="Delivery Means Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2477" ColumnName="Packaging Rights" Description="Delivery Means" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2478" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2479" ColumnName="ID_PKGING_RIGHTS_TM" Description="ID_PKGING_RIGHTS_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2979" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2980" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="142" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_PROMO_RIGHTS" Description="Promo Rights Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2480" ColumnName="Promo Rights" Description="Promo Rights" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2481" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2482" ColumnName="ID_PROMO_RIGHTS_TM" Description="ID_PROMO_RIGHTS_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2997" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2998" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="143" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_TERRITORY" Description="Territory Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2483" ColumnName="Territory" Description="Territory" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2484" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2485" ColumnName="ID_TERRITORY_TM" Description="ID_TERRITORY_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2972" ColumnName="Parent Level 2" Description="Parent Level 2" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2973" ColumnName="Parent Level 3" Description="Parent Level 3" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2974" ColumnName="Territory Flat" Description="Territory Flat" ColumnType="D" OrdinalPosition="5" ColumnOrder="6" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2975" ColumnName="Territory Items" Description="Territory Items" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2976" ColumnName="ID_TERRITORY_ITEM" Description="ID_TERRITORY_ITEM" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2977" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="7" ColumnOrder="8" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2978" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="8" ColumnOrder="9" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="144" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_TOUCHPOINT" Description="Viewing Devices Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2486" ColumnName="Touchpoint" Description="Viewing Devices" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2487" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2488" ColumnName="ID_TOUCHPOINT_TM" Description="ID_TOUCHPOINT_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2522" ColumnName="Parent Level 2" Description="Parent Level 2" ColumnType="D" OrdinalPosition="2" ColumnOrder="4" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2523" ColumnName="Parent Level 3" Description="Parent Level 3" ColumnType="D" OrdinalPosition="3" ColumnOrder="5" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2524" ColumnName="Touchpoint Flat" Description="Viewing Devices Flat" ColumnType="D" OrdinalPosition="4" ColumnOrder="6" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2525" ColumnName="ID_TOUCHPOINT_ITEM" Description="ID_TOUCHPOINT_ITEM" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="145" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_VENUE" Description="Venue Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2489" ColumnName="Venue" Description="Venue" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2490" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2491" ColumnName="ID_VENUE_TM" Description="ID_VENUE_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2984" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2985" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="92" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_BUS_OUTLET" Description="Channels Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="1652" ColumnName="Business Outlet" Description="Channels" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1654" ColumnName="ID_BUSINESS_OUTLET_TM" Description="ID_BUSINESS_OUTLET_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1653" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2506" ColumnName="Parent Level 2" Description="Parent Level 2" ColumnType="D" OrdinalPosition="2" ColumnOrder="4" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2507" ColumnName="Parent Level 3" Description="Parent Level 3" ColumnType="D" OrdinalPosition="3" ColumnOrder="5" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2508" ColumnName="Business Outlet Flat" Description="Channels Flat" ColumnType="D" OrdinalPosition="4" ColumnOrder="6" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2509" ColumnName="ID_BUSINESS_OUTLET_ITEM" Description="ID_BUSINESS_OUTLET_ITEM" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS" Description="Media Rights" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="351" ColumnName="DEAL_PK" Description="DEAL_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="352" ColumnName="ASSET_PK" Description="ASSET_PK" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1749" ColumnName="MEDIA_RIGHT_TM_PK" Description="MEDIA_RIGHT_TM_PK" ColumnType="D" OrdinalPosition="3" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="355" ColumnName="DEAL_ASSET_DETAIL_PK" Description="DEAL_ASSET_DETAIL_PK" ColumnType="D" OrdinalPosition="2" ColumnOrder="4" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2687" ColumnName="BTTERM_GROUP_PK" Description="BTTERM_GROUP_PK" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1750" ColumnName="START_TERM_EVENT_PK" Description="START_TERM_EVENT_PK" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="392" ColumnName="Row No" Description="Row No" ColumnType="D" OrdinalPosition="6" ColumnOrder="6" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="354" ColumnName="Applies To" Description="Applies To" ColumnType="D" OrdinalPosition="7" ColumnOrder="7" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="353" ColumnName="Ext. Carve-Out" Description="Ext  Carve- Out" ColumnType="D" OrdinalPosition="8" ColumnOrder="8" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="356" ColumnName="Party" Description="Party" ColumnType="D" OrdinalPosition="9" ColumnOrder="9" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="357" ColumnName="Media Type" Description="Media Type" ColumnType="D" OrdinalPosition="10" ColumnOrder="10" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1682" ColumnName="Media Type Tree" Description="Media Type Tree" ColumnType="D" OrdinalPosition="11" ColumnOrder="11" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="358" ColumnName="Venue" Description="Venue" ColumnType="D" OrdinalPosition="12" ColumnOrder="12" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="359" ColumnName="Packaging" Description="Delivery Means" ColumnType="D" OrdinalPosition="13" ColumnOrder="13" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="360" ColumnName="Touchpoints" Description="Viewing Devices" ColumnType="D" OrdinalPosition="14" ColumnOrder="14" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="363" ColumnName="Business Outlets" Description="Channels" ColumnType="D" OrdinalPosition="16" ColumnOrder="15" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="364" ColumnName="Territory" Description="Territory" ColumnType="D" OrdinalPosition="17" ColumnOrder="16" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1753" ColumnName="Language Template" Description="Language Template" ColumnType="D" OrdinalPosition="18" ColumnOrder="17" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="361" ColumnName="Language" Description="Language" ColumnType="D" OrdinalPosition="19" ColumnOrder="18" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="362" ColumnName="Perpetuity" Description="Perpetuity" ColumnType="D" OrdinalPosition="20" ColumnOrder="19" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1754" ColumnName="Perpetuity Flag" Description="Perpetuity Flag" ColumnType="D" OrdinalPosition="21" ColumnOrder="20" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="365" ColumnName="Term From" Description="Term From" ColumnType="D" OrdinalPosition="22" ColumnOrder="21" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="368" ColumnName="Estimated Term From" Description="Estimated Term From" ColumnType="D" OrdinalPosition="23" ColumnOrder="22" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="366" ColumnName="Term To" Description="Term To" ColumnType="D" OrdinalPosition="24" ColumnOrder="23" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="374" ColumnName="Estimated Term To" Description="Estimated Term To" ColumnType="D" OrdinalPosition="25" ColumnOrder="24" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="367" ColumnName="Exclusivity" Description="Exclusivity" ColumnType="D" OrdinalPosition="26" ColumnOrder="25" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1756" ColumnName="Holdback" Description="Holdback" ColumnType="D" OrdinalPosition="27" ColumnOrder="26" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="369" ColumnName="Type" Description="Type" ColumnType="D" OrdinalPosition="28" ColumnOrder="27" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="371" ColumnName="Restriction" Description="Restriction" ColumnType="D" OrdinalPosition="29" ColumnOrder="28" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="370" ColumnName="Sublicense" Description="Sublicense" ColumnType="D" OrdinalPosition="30" ColumnOrder="29" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="378" ColumnName="Notes" Description="Notes" ColumnType="D" OrdinalPosition="31" ColumnOrder="30" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="375" ColumnName="Restricted Notes" Description="Restricted Notes" ColumnType="D" OrdinalPosition="32" ColumnOrder="31" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="379" ColumnName="End User Rights" Description="Product Offerings" ColumnType="D" OrdinalPosition="33" ColumnOrder="32" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2688" ColumnName="Notes Restricted" Description="Notes Restricted" ColumnType="D" OrdinalPosition="33" ColumnOrder="33" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="372" ColumnName="Blackout" Description="Blackout" ColumnType="D" OrdinalPosition="34" ColumnOrder="34" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2689" ColumnName="Notes Unrestricted" Description="Notes Unrestricted" ColumnType="D" OrdinalPosition="34" ColumnOrder="35" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="376" ColumnName="Customer" Description="Customer" ColumnType="D" OrdinalPosition="35" ColumnOrder="36" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="373" ColumnName="Publications" Description="Publications" ColumnType="D" OrdinalPosition="36" ColumnOrder="37" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="377" ColumnName="Min Value" Description="Min Value" ColumnType="D" OrdinalPosition="37" ColumnOrder="38" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="380" ColumnName="Max Value" Description="Max Value" ColumnType="D" OrdinalPosition="38" ColumnOrder="39" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1758" ColumnName="ID_MEDIA_RIGHT_TM" Description="ID_MEDIA_RIGHT_TM" ColumnType="D" OrdinalPosition="40" ColumnOrder="40" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="381" ColumnName="Units" Description="Units" ColumnType="D" OrdinalPosition="39" ColumnOrder="41" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="382" ColumnName="Minutes" Description="Minutes" ColumnType="D" OrdinalPosition="40" ColumnOrder="42" DataType="200" CharacterMaxLen="300" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1759" ColumnName="ID_TERRITORY_TM" Description="ID_TERRITORY_TM" ColumnType="D" OrdinalPosition="41" ColumnOrder="43" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="384" ColumnName="Refresh Percent" Description="Refresh Percent" ColumnType="D" OrdinalPosition="41" ColumnOrder="44" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="386" ColumnName="Quantity" Description="Quantity" ColumnType="D" OrdinalPosition="42" ColumnOrder="45" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1760" ColumnName="ID_PKGING_RIGHTS_TM" Description="ID_PKGING_RIGHTS_TM" ColumnType="D" OrdinalPosition="42" ColumnOrder="46" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1761" ColumnName="ID_LANGUAGE_TM" Description="ID_LANGUAGE_TM" ColumnType="D" OrdinalPosition="43" ColumnOrder="47" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="391" ColumnName="Usage Period" Description="Usage Period" ColumnType="D" OrdinalPosition="45" ColumnOrder="48" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="388" ColumnName="Usage Logical Operator" Description="Usage Logical Operator" ColumnType="D" OrdinalPosition="43" ColumnOrder="49" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="390" ColumnName="Usage Time Period" Description="Usage Time Period" ColumnType="D" OrdinalPosition="44" ColumnOrder="50" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1762" ColumnName="ID_VENUE_TM" Description="ID_VENUE_TM" ColumnType="D" OrdinalPosition="46" ColumnOrder="51" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1763" ColumnName="ID_TOUCHPOINT_TM" Description="ID_TOUCHPOINT_TM" ColumnType="D" OrdinalPosition="44" ColumnOrder="52" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1764" ColumnName="ID_ENDUSER_RIGHTS_TM" Description="ID_ENDUSER_RIGHTS_TM" ColumnType="D" OrdinalPosition="45" ColumnOrder="53" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1765" ColumnName="ID_BUSINESS_OUTLET_TM" Description="ID_BUSINESS_OUTLET_TM" ColumnType="D" OrdinalPosition="47" ColumnOrder="54" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="383" ColumnName="Updated By" Description="Updated By" ColumnType="D" OrdinalPosition="54" ColumnOrder="55" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="385" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="55" ColumnOrder="56" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="387" ColumnName="Created By" Description="Created By" ColumnType="D" OrdinalPosition="56" ColumnOrder="57" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="389" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="57" ColumnOrder="58" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="6" CategoryName="Rights In" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
    </Task>
    <Task Type="Relation">
      <SynchronizationOptions OverwriteExisting="True" OverwriteExistingDependencies="False" />
      <Relation ElementID="139" RelationName="Media Rights -&gt; Media Rights Tree" Description="" Relation="Left Outer Join" ObjectID1="22" ObjectID2="140" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="1758" ColumnName1="ID_MEDIA_RIGHT_TM" ColumnID2="2476" ColumnName2="ID_MEDIA_RIGHT_TM" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="1758" ColumnName="ID_MEDIA_RIGHT_TM" />
          </Dependency>
          <Dependency Type="Object" ID="140" ObjectName="MV_RB_ITEMS_MEDIA_RIGHTS">
            <Column ID="2476" ColumnName="ID_MEDIA_RIGHT_TM" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="140" RelationName="Media Rights -&gt; Territory Tree" Description="" Relation="Left Outer Join" ObjectID1="22" ObjectID2="143" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="1759" ColumnName1="ID_TERRITORY_TM" ColumnID2="2485" ColumnName2="ID_TERRITORY_TM" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="1759" ColumnName="ID_TERRITORY_TM" />
          </Dependency>
          <Dependency Type="Object" ID="143" ObjectName="MV_RB_ITEMS_TERRITORY">
            <Column ID="2485" ColumnName="ID_TERRITORY_TM" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="141" RelationName="Media Rights -&gt; Platform Tree" Description="" Relation="Left Outer Join" ObjectID1="22" ObjectID2="141" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="1760" ColumnName1="ID_PKGING_RIGHTS_TM" ColumnID2="2479" ColumnName2="ID_PKGING_RIGHTS_TM" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="1760" ColumnName="ID_PKGING_RIGHTS_TM" />
          </Dependency>
          <Dependency Type="Object" ID="141" ObjectName="MV_RB_ITEMS_PKGING_RIGHTS">
            <Column ID="2479" ColumnName="ID_PKGING_RIGHTS_TM" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="142" RelationName="Media Rights -&gt; Language Tree" Description="" Relation="Left Outer Join" ObjectID1="22" ObjectID2="139" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="1761" ColumnName1="ID_LANGUAGE_TM" ColumnID2="2473" ColumnName2="ID_LANGUAGE_TM" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="1761" ColumnName="ID_LANGUAGE_TM" />
          </Dependency>
          <Dependency Type="Object" ID="139" ObjectName="MV_RB_ITEMS_LANGUAGE">
            <Column ID="2473" ColumnName="ID_LANGUAGE_TM" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="143" RelationName="Media Rights -&gt; Venue Tree" Description="" Relation="Left Outer Join" ObjectID1="22" ObjectID2="145" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="1762" ColumnName1="ID_VENUE_TM" ColumnID2="2491" ColumnName2="ID_VENUE_TM" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="1762" ColumnName="ID_VENUE_TM" />
          </Dependency>
          <Dependency Type="Object" ID="145" ObjectName="MV_RB_ITEMS_VENUE">
            <Column ID="2491" ColumnName="ID_VENUE_TM" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="145" RelationName="Media Rights -&gt; Enduser Rights Tree" Description="" Relation="Left Outer Join" ObjectID1="22" ObjectID2="138" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="1764" ColumnName1="ID_ENDUSER_RIGHTS_TM" ColumnID2="2470" ColumnName2="ID_ENDUSER_RIGHTS_TM" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="1764" ColumnName="ID_ENDUSER_RIGHTS_TM" />
          </Dependency>
          <Dependency Type="Object" ID="138" ObjectName="MV_RB_ITEMS_ENDUSR_RIGHTS">
            <Column ID="2470" ColumnName="ID_ENDUSER_RIGHTS_TM" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="136" RelationName="Media Rights -&gt; Touchpoint Tree" Description="" Relation="Left Outer Join" ObjectID1="22" ObjectID2="144" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="1763" ColumnName1="ID_TOUCHPOINT_TM" ColumnID2="2488" ColumnName2="ID_TOUCHPOINT_TM" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="1763" ColumnName="ID_TOUCHPOINT_TM" />
          </Dependency>
          <Dependency Type="Object" ID="144" ObjectName="MV_RB_ITEMS_TOUCHPOINT">
            <Column ID="2488" ColumnName="ID_TOUCHPOINT_TM" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="146" RelationName="Media Rights -&gt; Channel Tree" Description="" Relation="Left Outer Join" ObjectID1="22" ObjectID2="92" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="1765" ColumnName1="ID_BUSINESS_OUTLET_TM" ColumnID2="1654" ColumnName2="ID_BUSINESS_OUTLET_TM" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="1765" ColumnName="ID_BUSINESS_OUTLET_TM" />
          </Dependency>
          <Dependency Type="Object" ID="92" ObjectName="MV_RB_ITEMS_BUS_OUTLET">
            <Column ID="1654" ColumnName="ID_BUSINESS_OUTLET_TM" />
          </Dependency>
        </Dependencies>
      </Relation>
    </Task>
  </Tasks>
  <AppSettings CopyConnectionString="False" CopyGeneralSettings="False" CopyUIFeatures="False" CopyReportingOptions="False" CopyReportingFeatures="False" CopySecuritySettings="False" MultipleOrganizations="False" UniqueReportFolderNames="False" CopySchedulerServiceSettings="False" CopySchedulingConfiguration="False" CopySMTPServerSettings="False" CopyEmailConfiguration="False" CopyArchiveConfiguration="False" CopyArchiveSettings="False" CopyEventLogDBConnection="False" CopyEventLogConfiguration="False">
    <Database ElementID="1" MaxReportRecords="500000" MaxListRecords="10000" DesignTimeValidation="Off" DefaultTemplate="Custom-Blue" />
    <Organization ElementID="1" GroupName="Default" Description="" DefaultTheme="Rights" />
  </AppSettings>
</SynchronizationPackage>