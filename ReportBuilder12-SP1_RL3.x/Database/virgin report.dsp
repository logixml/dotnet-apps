<SynchronizationPackage SrcInstanceGUID="647128f6-d29c-4984-a7d2-d7f09f3aa467" SrcConnectionID="1" SrcGroupID="1" SrcVersion="12.1.43.1" IsSimpleView="True">
  <SelectedElements>
    <Element Type="Report" SelectAll="False" OverwriteExisting="True" OverwriteExistingDependencies="False" IncludeSchedules="False" IncludeSubscribers="False" IncludeArchives="False" PreserveOwner="1" PreserveFolder="1">
      <Report ElementID="310" />
    </Element>
  </SelectedElements>
  <Tasks>
    <Task Type="Report">
      <SynchronizationOptions OverwriteExisting="True" OverwriteExistingDependencies="False" IncludeSchedules="False" IncludeArchives="False" PreserveOwner="1" PreserveFolder="1" />
      <Report ElementID="310" ReportName="Sports Virgin Media Rights" Description="" PhysicalName="" ExpirationDate="" FolderType="0" ReportGUID="ahReport310_d020ee85-57b9-4583-b4c2-6fdc630c0a69" Owner="anm56" ParentFolderID="0" Dashboard="0" Mobile="0">
        <Definition>
          <AdhocMetadata DefaultActiveItemID="1" ActiveStepIndex="2">
            <DataStructures>
              <DataStructure ID="1" Distinct="True" CategoryID="6">
                <DataSource ObjectID="13" ObjectKey="13" ObjectSchema="" ObjectName="VV_RB_Deal_Navigator" Object1DatabaseID="" Object1Schema="" Object1Name="" Column1Name="" Object2DatabaseID="" Object2Schema="" Object2Name="" Column2Name="" Relation="" RelationID="0" RelationDirection="0" DatabaseID="1">
                  <DataSource ObjectID="16" ObjectKey="13_66_16_0" ObjectSchema="" ObjectName="VV_RB_Deals" Object1DatabaseID="1" Object1Schema="" Object1Name="VV_RB_Deal_Navigator" Column1Name="DEAL_PK" Object2DatabaseID="1" Object2Schema="" Object2Name="VV_RB_Deals" Column2Name="DEAL_PK" Relation="Inner Join" RelationID="66" RelationDirection="0" DatabaseID="1" />
                  <DataSource ObjectID="22" ObjectKey="13_64_22_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS" Object1DatabaseID="1" Object1Schema="" Object1Name="VV_RB_Deal_Navigator" Column1Name="DEAL_ASSET_DETAIL_PK" Object2DatabaseID="1" Object2Schema="RLANLTCS" Object2Name="MV_RB_DEAL_MEDIA_RIGHTS" Column2Name="DEAL_ASSET_DETAIL_PK" Relation="Left Outer Join" RelationID="64" RelationDirection="0" DatabaseID="1">
                    <DataSource ObjectID="140" ObjectKey="13_64_22_0_139_140_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_MEDIA_RIGHTS" Object1DatabaseID="1" Object1Schema="RLANLTCS" Object1Name="MV_RB_DEAL_MEDIA_RIGHTS" Column1Name="ID_MEDIA_RIGHT_TM" Object2DatabaseID="1" Object2Schema="RLANLTCS" Object2Name="MV_RB_ITEMS_MEDIA_RIGHTS" Column2Name="ID_MEDIA_RIGHT_TM" Relation="Left Outer Join" RelationID="139" RelationDirection="0" DatabaseID="1" />
                    <DataSource ObjectID="138" ObjectKey="13_64_22_0_145_138_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_ENDUSR_RIGHTS" Object1DatabaseID="1" Object1Schema="RLANLTCS" Object1Name="MV_RB_DEAL_MEDIA_RIGHTS" Column1Name="ID_ENDUSER_RIGHTS_TM" Object2DatabaseID="1" Object2Schema="RLANLTCS" Object2Name="MV_RB_ITEMS_ENDUSR_RIGHTS" Column2Name="ID_ENDUSER_RIGHTS_TM" Relation="Left Outer Join" RelationID="145" RelationDirection="0" DatabaseID="1" />
                  </DataSource>
                </DataSource>
                <Objects>
                  <Object ObjectID="16" ObjectKey="13_66_16_0" ObjectSchema="" ObjectName="VV_RB_Deals" ObjectIdentifier="16_66_0">
                    <Columns>
                      <Column ColumnID="197" ColumnName="Deal Description" />
                      <Column ColumnID="198" ColumnName="Deal Name" />
                      <Column ColumnID="205" ColumnName="Profit Center" />
                      <Column ColumnID="2652" ColumnName="Distributor Without Internal" />
                    </Columns>
                  </Object>
                  <Object ObjectID="22" ObjectKey="13_64_22_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS" ObjectIdentifier="22_64_0">
                    <Columns>
                      <Column ColumnID="392" ColumnName="Row No" />
                      <Column ColumnID="354" ColumnName="Applies To" />
                      <Column ColumnID="357" ColumnName="Media Type" />
                      <Column ColumnID="365" ColumnName="Term From" />
                      <Column ColumnID="366" ColumnName="Term To" />
                      <Column ColumnID="369" ColumnName="Type" />
                      <Column ColumnID="379" ColumnName="End User Rights" />
                    </Columns>
                  </Object>
                  <Object ObjectID="138" ObjectKey="13_64_22_0_145_138_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_ENDUSR_RIGHTS" ObjectIdentifier="138_145_0">
                    <Columns>
                      <Column ColumnID="2468" ColumnName="Enduser Rights" />
                    </Columns>
                  </Object>
                </Objects>
                <OrderBy />
                <Parameter>
                  <Column ParamType="0" ID="1" ColumnID="205" ObjectKey="13_66_16_0" Operator="Equal to" BitCmd="And" Ask="False" Caption="" Level="0">
                    <Value Value="Sky Sports" ValueType="0" />
                  </Column>
                  <Column ParamType="0" ID="2" ColumnID="357" ObjectKey="13_64_22_0" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="" Level="0">
                    <Value Value="SS Archive" ValueType="3" />
                    <Value Value="SS Catch Up" ValueType="3" />
                    <Value Value="SS Preview" ValueType="3" />
                    <Value Value="SS Wholesale" ValueType="3" />
                  </Column>
                  <Column ParamType="0" ID="5" ColumnID="2468" ObjectKey="13_64_22_0_145_138_0" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="" Level="0">
                    <Value Value="019............Virgin Media Limited" ValueType="3" />
                  </Column>
                  <Column ParamType="0" ID="3" ColumnID="366" ObjectKey="13_64_22_0" Operator="Greater than or equal to" BitCmd="And" Ask="False" Caption="" Level="0">
                    <Value Value="@Date.Today~" ValueType="1" />
                  </Column>
                  <Column ParamType="0" ID="4" ColumnID="197" ObjectKey="13_66_16_0" Operator="Contains" BitCmd="And" ControlType="tb" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="" Level="0">
                    <Value Value="" ValueType="0" />
                  </Column>
                </Parameter>
              </DataStructure>
            </DataStructures>
            <Contents>
              <Content ItemID="1" ItemIndex="1" ContentID="2" ContentName="Table" ContentTitle="">
                <Data DataStructureID="1" />
                <DataTable Caption="" ShowRecordCount="False" ShowRowNumber="False" UseInteractivePaging="True" UseNumberedList="False" UseAppendRowsPaging="False" PagingLocation="0" InteractivePagingRows="100" InteractivePagingSubRows="50" SummaryRowLocationBottom="False" ShowSummaryRowOnce="True" TableWidth="-1" TableWidthScale="px">
                  <Column ColumnType="1" ColumnID="197" ObjectKey="13_66_16_0" Header="Deal Code" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="198" ObjectKey="13_66_16_0" Header="Deal Name" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="205" ObjectKey="13_66_16_0" Header="Profit Center" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="2652" ObjectKey="13_66_16_0" Header="Distributor Without Internal" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="392" ObjectKey="13_64_22_0" Header="Row No" Format="General Number" Alignment="Right" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="354" ObjectKey="13_64_22_0" Header="Applies To" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="357" ObjectKey="13_64_22_0" Header="Media Type" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="379" ObjectKey="13_64_22_0" Header="Product Offerings" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="365" ObjectKey="13_64_22_0" Header="Term From" Format="Short Date" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="366" ObjectKey="13_64_22_0" Header="Term To" Format="Short Date" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="369" ObjectKey="13_64_22_0" Header="Type" Format="" Alignment="Left" HasSort="True" IsLinked="False" Width="-1" Scale="px" VisualizationType="None" ShowData="1" />
                </DataTable>
                <XGroupBy GroupType="0" PageBreakAfterGroup="False" FlatGroupingRollUpDetails="False" ShowExpandCollapse="False" AutoExpandDrillDown="False" />
              </Content>
              <Content ItemID="2" ItemIndex="2" ContentID="11" ContentName="Exports" ContentTitle="">
                <Exports LinkSearchable="False" LinkPrintable="False" LinkExcel="True" LinkWord="True" LinkPPT="False" LinkPdf="True" LinkCSV="False" LinkXML="False" LinkArchive="False" SendMail="False" SendMailExportType="PDF" />
              </Content>
            </Contents>
            <SessionParameter />
            <General Template="Custom-Blue" PrintablePageOrientation="Portrait" ShowPrintablePageNr="ON" PrintablePageType="Letter" />
          </AdhocMetadata>
        </Definition>
        <Dependencies>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="354" ColumnName="Applies To" />
            <Column ID="379" ColumnName="End User Rights" />
            <Column ID="357" ColumnName="Media Type" />
            <Column ID="392" ColumnName="Row No" />
            <Column ID="365" ColumnName="Term From" />
            <Column ID="366" ColumnName="Term To" />
            <Column ID="369" ColumnName="Type" />
          </Dependency>
          <Dependency Type="Object" ID="138" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_ENDUSR_RIGHTS">
            <Column ID="2468" ColumnName="Enduser Rights" />
          </Dependency>
          <Dependency Type="Object" ID="16" ObjectSchema="" ObjectName="VV_RB_Deals">
            <Column ID="197" ColumnName="Deal Description" />
            <Column ID="198" ColumnName="Deal Name" />
            <Column ID="2652" ColumnName="Distributor Without Internal" />
            <Column ID="205" ColumnName="Profit Center" />
          </Dependency>
          <Dependency Type="Relation" ID="66" RelationName="Deal Navigator -&gt; Deals" Relation="Inner Join" ObjectID1="13" ObjectID2="16" />
          <Dependency Type="Relation" ID="64" RelationName="Deal Navigator -&gt; Media Rights" Relation="Left Outer Join" ObjectID1="13" ObjectID2="22" />
          <Dependency Type="Relation" ID="145" RelationName="Media Rights -&gt; Enduser Rights Tree" Relation="Left Outer Join" ObjectID1="22" ObjectID2="138" />
          <Dependency Type="Relation" ID="139" RelationName="Media Rights -&gt; Media Rights Tree" Relation="Left Outer Join" ObjectID1="22" ObjectID2="140" />
          <Dependency Type="DataFormat" ID="1" FormatName="General Number" Format="General Number" />
          <Dependency Type="DataFormat" ID="13" FormatName="Short Date" Format="Short Date" />
          <Dependency Type="SessionParameter" ID="1" ParameterName="spUserId" />
        </Dependencies>
      </Report>
    </Task>
    <Task Type="Object">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" IncludeLinks="False" IncludeAccessRights="False" PreserveCategories="1" DeleteUnmatched="False" />
      <Object ElementID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS" Description="Media Rights" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="351" ColumnName="DEAL_PK" Description="DEAL_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="352" ColumnName="ASSET_PK" Description="ASSET_PK" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1749" ColumnName="MEDIA_RIGHT_TM_PK" Description="MEDIA_RIGHT_TM_PK" ColumnType="D" OrdinalPosition="3" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="355" ColumnName="DEAL_ASSET_DETAIL_PK" Description="DEAL_ASSET_DETAIL_PK" ColumnType="D" OrdinalPosition="2" ColumnOrder="4" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2687" ColumnName="BTTERM_GROUP_PK" Description="BTTERM_GROUP_PK" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1750" ColumnName="START_TERM_EVENT_PK" Description="START_TERM_EVENT_PK" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="392" ColumnName="Row No" Description="Row No" ColumnType="D" OrdinalPosition="6" ColumnOrder="6" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="354" ColumnName="Applies To" Description="Applies To" ColumnType="D" OrdinalPosition="7" ColumnOrder="7" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="353" ColumnName="Ext. Carve-Out" Description="Ext  Carve- Out" ColumnType="D" OrdinalPosition="8" ColumnOrder="8" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="356" ColumnName="Party" Description="Party" ColumnType="D" OrdinalPosition="9" ColumnOrder="9" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="357" ColumnName="Media Type" Description="Media Type" ColumnType="D" OrdinalPosition="10" ColumnOrder="10" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1682" ColumnName="Media Type Tree" Description="Media Type Tree" ColumnType="D" OrdinalPosition="11" ColumnOrder="11" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="358" ColumnName="Venue" Description="Venue" ColumnType="D" OrdinalPosition="12" ColumnOrder="12" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="359" ColumnName="Packaging" Description="Delivery Means" ColumnType="D" OrdinalPosition="13" ColumnOrder="13" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="360" ColumnName="Touchpoints" Description="Viewing Devices" ColumnType="D" OrdinalPosition="14" ColumnOrder="14" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="363" ColumnName="Business Outlets" Description="Channels" ColumnType="D" OrdinalPosition="16" ColumnOrder="15" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="364" ColumnName="Territory" Description="Territory" ColumnType="D" OrdinalPosition="17" ColumnOrder="16" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1753" ColumnName="Language Template" Description="Language Template" ColumnType="D" OrdinalPosition="18" ColumnOrder="17" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="361" ColumnName="Language" Description="Language" ColumnType="D" OrdinalPosition="19" ColumnOrder="18" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="362" ColumnName="Perpetuity" Description="Perpetuity" ColumnType="D" OrdinalPosition="20" ColumnOrder="19" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1754" ColumnName="Perpetuity Flag" Description="Perpetuity Flag" ColumnType="D" OrdinalPosition="21" ColumnOrder="20" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="365" ColumnName="Term From" Description="Term From" ColumnType="D" OrdinalPosition="22" ColumnOrder="21" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="368" ColumnName="Estimated Term From" Description="Estimated Term From" ColumnType="D" OrdinalPosition="23" ColumnOrder="22" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="366" ColumnName="Term To" Description="Term To" ColumnType="D" OrdinalPosition="24" ColumnOrder="23" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="374" ColumnName="Estimated Term To" Description="Estimated Term To" ColumnType="D" OrdinalPosition="25" ColumnOrder="24" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="367" ColumnName="Exclusivity" Description="Exclusivity" ColumnType="D" OrdinalPosition="26" ColumnOrder="25" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1756" ColumnName="Holdback" Description="Holdback" ColumnType="D" OrdinalPosition="27" ColumnOrder="26" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="369" ColumnName="Type" Description="Type" ColumnType="D" OrdinalPosition="28" ColumnOrder="27" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="371" ColumnName="Restriction" Description="Restriction" ColumnType="D" OrdinalPosition="29" ColumnOrder="28" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="370" ColumnName="Sublicense" Description="Sublicense" ColumnType="D" OrdinalPosition="30" ColumnOrder="29" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="378" ColumnName="Notes" Description="Notes" ColumnType="D" OrdinalPosition="31" ColumnOrder="30" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="375" ColumnName="Restricted Notes" Description="Restricted Notes" ColumnType="D" OrdinalPosition="32" ColumnOrder="31" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="379" ColumnName="End User Rights" Description="Product Offerings" ColumnType="D" OrdinalPosition="33" ColumnOrder="32" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2688" ColumnName="Notes Restricted" Description="Notes Restricted" ColumnType="D" OrdinalPosition="33" ColumnOrder="33" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="372" ColumnName="Blackout" Description="Blackout" ColumnType="D" OrdinalPosition="34" ColumnOrder="34" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2689" ColumnName="Notes Unrestricted" Description="Notes Unrestricted" ColumnType="D" OrdinalPosition="34" ColumnOrder="35" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="376" ColumnName="Customer" Description="Customer" ColumnType="D" OrdinalPosition="35" ColumnOrder="36" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="373" ColumnName="Publications" Description="Publications" ColumnType="D" OrdinalPosition="36" ColumnOrder="37" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="377" ColumnName="Min Value" Description="Min Value" ColumnType="D" OrdinalPosition="37" ColumnOrder="38" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="380" ColumnName="Max Value" Description="Max Value" ColumnType="D" OrdinalPosition="38" ColumnOrder="39" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1758" ColumnName="ID_MEDIA_RIGHT_TM" Description="ID_MEDIA_RIGHT_TM" ColumnType="D" OrdinalPosition="40" ColumnOrder="40" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="381" ColumnName="Units" Description="Units" ColumnType="D" OrdinalPosition="39" ColumnOrder="41" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="382" ColumnName="Minutes" Description="Minutes" ColumnType="D" OrdinalPosition="40" ColumnOrder="42" DataType="200" CharacterMaxLen="300" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1759" ColumnName="ID_TERRITORY_TM" Description="ID_TERRITORY_TM" ColumnType="D" OrdinalPosition="41" ColumnOrder="43" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="384" ColumnName="Refresh Percent" Description="Refresh Percent" ColumnType="D" OrdinalPosition="41" ColumnOrder="44" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="386" ColumnName="Quantity" Description="Quantity" ColumnType="D" OrdinalPosition="42" ColumnOrder="45" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1760" ColumnName="ID_PKGING_RIGHTS_TM" Description="ID_PKGING_RIGHTS_TM" ColumnType="D" OrdinalPosition="42" ColumnOrder="46" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1761" ColumnName="ID_LANGUAGE_TM" Description="ID_LANGUAGE_TM" ColumnType="D" OrdinalPosition="43" ColumnOrder="47" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="391" ColumnName="Usage Period" Description="Usage Period" ColumnType="D" OrdinalPosition="45" ColumnOrder="48" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="388" ColumnName="Usage Logical Operator" Description="Usage Logical Operator" ColumnType="D" OrdinalPosition="43" ColumnOrder="49" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="390" ColumnName="Usage Time Period" Description="Usage Time Period" ColumnType="D" OrdinalPosition="44" ColumnOrder="50" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1762" ColumnName="ID_VENUE_TM" Description="ID_VENUE_TM" ColumnType="D" OrdinalPosition="46" ColumnOrder="51" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1763" ColumnName="ID_TOUCHPOINT_TM" Description="ID_TOUCHPOINT_TM" ColumnType="D" OrdinalPosition="44" ColumnOrder="52" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1764" ColumnName="ID_ENDUSER_RIGHTS_TM" Description="ID_ENDUSER_RIGHTS_TM" ColumnType="D" OrdinalPosition="45" ColumnOrder="53" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1765" ColumnName="ID_BUSINESS_OUTLET_TM" Description="ID_BUSINESS_OUTLET_TM" ColumnType="D" OrdinalPosition="47" ColumnOrder="54" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="383" ColumnName="Updated By" Description="Updated By" ColumnType="D" OrdinalPosition="54" ColumnOrder="55" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="385" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="55" ColumnOrder="56" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="387" ColumnName="Created By" Description="Created By" ColumnType="D" OrdinalPosition="56" ColumnOrder="57" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="389" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="57" ColumnOrder="58" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="6" CategoryName="Rights In" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="138" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_ENDUSR_RIGHTS" Description="Product Offering Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2468" ColumnName="Enduser Rights" Description="Product Offerings" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2469" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2470" ColumnName="ID_ENDUSER_RIGHTS_TM" Description="ID_ENDUSER_RIGHTS_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2514" ColumnName="Parent Level 2" Description="Parent Level 2" ColumnType="D" OrdinalPosition="2" ColumnOrder="4" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2515" ColumnName="Parent Level 3" Description="Parent Level 3" ColumnType="D" OrdinalPosition="3" ColumnOrder="5" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2516" ColumnName="Enduser Rights Flat" Description="Product Offerings Flat" ColumnType="D" OrdinalPosition="4" ColumnOrder="6" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2517" ColumnName="ID_ENDUSER_RIGHTS_ITEM" Description="ID_ENDUSER_RIGHTS_ITEM" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="16" ObjectSchema="" ObjectName="VV_RB_Deals" Description="Deals" Type="VV" Definition="select d.*,uod.id_user_orig from mv_rb_deals d, MV_RB_USER_OUTLET_DEALS uod&#xA;where d.deal_pk=uod.deal_pk" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="195" ColumnName="DEAL_PK" Description="DEAL_PK" ColumnType="V" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="DEAL_PK" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="196" ColumnName="Deal ID" Description="Deal ID" ColumnType="V" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Deal ID" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="197" ColumnName="Deal Description" Description="Deal Code" ColumnType="V" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Description" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1668" ColumnName="Contract No" Description="Contract No" ColumnType="V" OrdinalPosition="3" ColumnOrder="4" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Contract No" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2648" ColumnName="Master Deal ID" Description="Master Deal ID" ColumnType="V" OrdinalPosition="3" ColumnOrder="4" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Master Deal ID" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2649" ColumnName="Master Agreement" Description="Master Agreement" ColumnType="V" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Master Agreement" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2650" ColumnName="Business Unit" Description="Business Unit" ColumnType="V" OrdinalPosition="5" ColumnOrder="6" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Business Unit" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2651" ColumnName="Internal Term Sheet Status" Description="Internal Term Sheet Status" ColumnType="V" OrdinalPosition="7" ColumnOrder="8" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Internal Term Sheet Status" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="199" ColumnName="Deal Type" Description="Agreement Type" ColumnType="V" OrdinalPosition="4" ColumnOrder="8" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Type" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="198" ColumnName="Deal Name" Description="Deal Name" ColumnType="V" OrdinalPosition="8" ColumnOrder="9" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="200" ColumnName="Contract Status" Description="Contract Status" ColumnType="V" OrdinalPosition="5" ColumnOrder="9" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Contract Status" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="203" ColumnName="Date Executed" Description="Approval Date" ColumnType="V" OrdinalPosition="7" ColumnOrder="10" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Date Executed" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1669" ColumnName="Deal Sub Type" Description="Agreement Sub Type" ColumnType="V" OrdinalPosition="6" ColumnOrder="10" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Sub Type" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="204" ColumnName="Date Effective" Description="Date Effective" ColumnType="V" OrdinalPosition="8" ColumnOrder="11" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Date Effective" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="202" ColumnName="Contract Data Entry Status" Description="Contract Data Entry Status" ColumnType="V" OrdinalPosition="6" ColumnOrder="12" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Contract Data Entry Status" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="206" ColumnName="Contract Currency" Description="Contract Currency" ColumnType="V" OrdinalPosition="10" ColumnOrder="14" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Contract Currency" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="209" ColumnName="Agreement Type" Description="Deal Type" ColumnType="V" OrdinalPosition="13" ColumnOrder="16" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Agreement Type" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="205" ColumnName="Profit Center" Description="Profit Center" ColumnType="V" OrdinalPosition="15" ColumnOrder="16" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Profit Center" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="210" ColumnName="Integration Action" Description="Integration Action" ColumnType="V" OrdinalPosition="14" ColumnOrder="17" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Integration Action" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="211" ColumnName="Contract Origin" Description="Contract Origin" ColumnType="V" OrdinalPosition="15" ColumnOrder="18" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Contract Origin" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="207" ColumnName="Budget Line" Description="Budget Line" ColumnType="V" OrdinalPosition="17" ColumnOrder="18" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Budget Line" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="208" ColumnName="Source Name" Description="Source Name" ColumnType="V" OrdinalPosition="18" ColumnOrder="19" DataType="129" CharacterMaxLen="9" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Source Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="212" ColumnName="Billing Frequency" Description="Billing Frequency" ColumnType="V" OrdinalPosition="16" ColumnOrder="19" DataType="200" CharacterMaxLen="1000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Billing Frequency" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="213" ColumnName="Custom Attribute1" Description="Custom Attribute1" ColumnType="V" OrdinalPosition="17" ColumnOrder="20" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Custom Attribute1" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="214" ColumnName="Custom Attribute2" Description="Custom Attribute2" ColumnType="V" OrdinalPosition="18" ColumnOrder="21" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Custom Attribute2" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="215" ColumnName="Custom Attribute3" Description="Custom Attribute3" ColumnType="V" OrdinalPosition="19" ColumnOrder="22" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Custom Attribute3" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="216" ColumnName="Custom Attribute4" Description="Custom Attribute4" ColumnType="V" OrdinalPosition="20" ColumnOrder="23" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Custom Attribute4" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1670" ColumnName="Primary Party" Description="Primary Party" ColumnType="V" OrdinalPosition="22" ColumnOrder="23" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Primary Party" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="218" ColumnName="List of Approved Users" Description="List of Approved Users" ColumnType="V" OrdinalPosition="22" ColumnOrder="24" DataType="200" CharacterMaxLen="2000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="List of Approved Users" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1671" ColumnName="Distributor Party" Description="Distributor Party" ColumnType="V" OrdinalPosition="23" ColumnOrder="24" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Distributor Party" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2652" ColumnName="Distributor Without Internal" Description="Distributor Without Internal" ColumnType="V" OrdinalPosition="24" ColumnOrder="25" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Distributor Without Internal" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="217" ColumnName="List of Pending Users" Description="List of Pending Users" ColumnType="V" OrdinalPosition="21" ColumnOrder="25" DataType="200" CharacterMaxLen="2000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="List of Pending Users" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1673" ColumnName="Primary Business Outlet" Description="Primary Business Outlet" ColumnType="V" OrdinalPosition="26" ColumnOrder="25" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Primary Business Outlet" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1674" ColumnName="SAP Channel ID" Description="SAP Channel ID" ColumnType="V" OrdinalPosition="27" ColumnOrder="26" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="SAP Channel ID" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1672" ColumnName="Licensor Party" Description="Licensor Party" ColumnType="V" OrdinalPosition="25" ColumnOrder="26" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Licensor Party" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="219" ColumnName="CDSK Number" Description="CDSK Number" ColumnType="V" OrdinalPosition="23" ColumnOrder="26" DataType="200" CharacterMaxLen="2000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="CDSK Number" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="222" ColumnName="Created By" Description="Created By" ColumnType="V" OrdinalPosition="28" ColumnOrder="27" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Created By" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="220" ColumnName="CDSK Status" Description="CDSK Status" ColumnType="V" OrdinalPosition="24" ColumnOrder="27" DataType="200" CharacterMaxLen="2000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="CDSK Status" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="221" ColumnName="CDSK Hold" Description="CDSK Hold" ColumnType="V" OrdinalPosition="25" ColumnOrder="28" DataType="200" CharacterMaxLen="2000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="CDSK Hold" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="224" ColumnName="Created Date" Description="Created Date" ColumnType="V" OrdinalPosition="29" ColumnOrder="28" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="0" Definition="Created Date" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="223" ColumnName="Total Asset Value" Description="Total Title Value" ColumnType="V" OrdinalPosition="26" ColumnOrder="29" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="2-digit place holder" Alignment="Right" NativeDataType="0" Definition="Total Asset Value" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="226" ColumnName="Updated By" Description="Updated By" ColumnType="V" OrdinalPosition="30" ColumnOrder="30" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Updated By" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="225" ColumnName="No of Assets" Description="No of Titles" ColumnType="V" OrdinalPosition="27" ColumnOrder="31" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="No of Assets" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="227" ColumnName="Updated Date" Description="Updated Date" ColumnType="V" OrdinalPosition="31" ColumnOrder="32" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="0" Definition="Updated Date" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2653" ColumnName="Channel Entity" Description="Channel Entity" ColumnType="V" OrdinalPosition="32" ColumnOrder="33" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Channel Entity" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2654" ColumnName="DM5 Doc Type" Description="DM5 Doc Type" ColumnType="V" OrdinalPosition="33" ColumnOrder="34" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="DM5 Doc Type" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2655" ColumnName="Parties" Description="Parties" ColumnType="V" OrdinalPosition="34" ColumnOrder="35" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Parties" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2656" ColumnName="Financial Status" Description="Financial Status" ColumnType="V" OrdinalPosition="35" ColumnOrder="36" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Financial Status" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="228" ColumnName="ID_USER_ORIG" Description="ID_USER_ORIG" ColumnType="V" OrdinalPosition="30" ColumnOrder="37" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="ID_USER_ORIG" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2657" ColumnName="Client ID" Description="Client ID" ColumnType="V" OrdinalPosition="36" ColumnOrder="37" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Client ID" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2658" ColumnName="Non Outlet Party" Description="Non Outlet Party" ColumnType="V" OrdinalPosition="37" ColumnOrder="38" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Non Outlet Party" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2659" ColumnName="Skip Workflow" Description="Skip Workflow" ColumnType="V" OrdinalPosition="38" ColumnOrder="39" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Skip Workflow" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2660" ColumnName="All Media Rights" Description="All Media Rights" ColumnType="V" OrdinalPosition="39" ColumnOrder="40" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="All Media Rights" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2661" ColumnName="All Promotion Rights" Description="All Promotion Rights" ColumnType="V" OrdinalPosition="40" ColumnOrder="41" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="All Promotion Rights" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2662" ColumnName="No Promotion Rights" Description="No Promotion Rights" ColumnType="V" OrdinalPosition="41" ColumnOrder="42" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="No Promotion Rights" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2663" ColumnName="All Editing Rights" Description="All Editing Rights" ColumnType="V" OrdinalPosition="42" ColumnOrder="43" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="All Editing Rights" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2664" ColumnName="No Editing Rights" Description="No Editing Rights" ColumnType="V" OrdinalPosition="43" ColumnOrder="44" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="No Editing Rights" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2665" ColumnName="ProMamS Contract Status" Description="ProMamS Contract Status" ColumnType="V" OrdinalPosition="44" ColumnOrder="45" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="ProMamS Contract Status" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2666" ColumnName="No Revenue participation" Description="No Revenue participation" ColumnType="V" OrdinalPosition="45" ColumnOrder="46" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="No Revenue participation" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2667" ColumnName="Deal ID (Main)" Description="Deal ID (Main)" ColumnType="V" OrdinalPosition="46" ColumnOrder="47" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Deal ID (Main)" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2668" ColumnName="Deal Name (Main)" Description="Deal Name (Main)" ColumnType="V" OrdinalPosition="47" ColumnOrder="48" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Name (Main)" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="6" CategoryName="Rights In" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="13" ObjectSchema="" ObjectName="VV_RB_Deal_Navigator" Description="Deal Navigator" Type="VV" Definition="select d.*,uod.id_user_orig from mv_rb_deal_navigator d, MV_RB_USER_OUTLET_DEALS uod&#xD;&#xA;where d.deal_pk=uod.deal_pk" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="153" ColumnName="DEAL_PK" Description="DEAL_PK" ColumnType="V" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="DEAL_PK" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="154" ColumnName="Deal ID" Description="Deal ID" ColumnType="V" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Deal ID" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="155" ColumnName="Deal Name" Description="Deal Name" ColumnType="V" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="156" ColumnName="ASSET_PK" Description="ASSET_PK" ColumnType="V" OrdinalPosition="3" ColumnOrder="4" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="ASSET_PK" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="157" ColumnName="Asset ID" Description="Title ID" ColumnType="V" OrdinalPosition="4" ColumnOrder="5" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Asset ID" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="158" ColumnName="DEAL_ASSET_DETAIL_PK" Description="DEAL_ASSET_DETAIL_PK" ColumnType="V" OrdinalPosition="4" ColumnOrder="6" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="DEAL_ASSET_DETAIL_PK" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="159" ColumnName="Asset Title" Description="Title Name" ColumnType="V" OrdinalPosition="5" ColumnOrder="7" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Asset Title" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="160" ColumnName="Asset Type" Description="Title Type" ColumnType="V" OrdinalPosition="6" ColumnOrder="8" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Asset Type" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="161" ColumnName="Series Name" Description="Series Name" ColumnType="V" OrdinalPosition="7" ColumnOrder="9" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Series Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="162" ColumnName="Applies To" Description="Applies To" ColumnType="V" OrdinalPosition="8" ColumnOrder="10" DataType="200" CharacterMaxLen="513" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Applies To" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="164" ColumnName="On Or Before" Description="On Or Before" ColumnType="V" OrdinalPosition="10" ColumnOrder="11" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="On Or Before" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="166" ColumnName="Letigation Pending" Description="Do Not Air" ColumnType="V" OrdinalPosition="9" ColumnOrder="12" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Letigation Pending" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="165" ColumnName="Program Do Not Air" Description="Program Do Not Air" ColumnType="V" OrdinalPosition="11" ColumnOrder="13" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Program Do Not Air" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="163" ColumnName="Season Name" Description="Season Name" ColumnType="V" OrdinalPosition="8" ColumnOrder="14" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Season Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="167" ColumnName="Source Name" Description="Source Name" ColumnType="V" OrdinalPosition="12" ColumnOrder="15" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Source Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="168" ColumnName="Created By" Description="Created By" ColumnType="V" OrdinalPosition="13" ColumnOrder="16" DataType="200" CharacterMaxLen="513" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Created By" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="169" ColumnName="Updated By" Description="Updated By" ColumnType="V" OrdinalPosition="14" ColumnOrder="17" DataType="200" CharacterMaxLen="513" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Updated By" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="170" ColumnName="Created Date" Description="Created Date" ColumnType="V" OrdinalPosition="15" ColumnOrder="18" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="0" Definition="Created Date" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="171" ColumnName="Updated Date" Description="Updated Date" ColumnType="V" OrdinalPosition="16" ColumnOrder="19" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="0" Definition="Updated Date" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="172" ColumnName="ID_USER_ORIG" Description="ID_USER_ORIG" ColumnType="V" OrdinalPosition="17" ColumnOrder="20" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="ID_USER_ORIG" Explanation="" HideColumn="1" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="6" CategoryName="Rights In" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="140" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_MEDIA_RIGHTS" Description="Media Rights Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2474" ColumnName="Media Rights" Description="Media Rights" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2475" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2518" ColumnName="Parent Level 2" Description="Parent Level 2" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2476" ColumnName="ID_MEDIA_RIGHT_TM" Description="ID_MEDIA_RIGHT_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2519" ColumnName="Parent Level 3" Description="Parent Level 3" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2520" ColumnName="Media Rights Flat" Description="Media Rights Flat" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2521" ColumnName="ID_MEDIA_RIGHT_ITEM" Description="ID_MEDIA_RIGHT_ITEM" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2969" ColumnName="Media Rights Items" Description="Media Rights Items" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2970" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="8" ColumnOrder="9" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2971" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="9" ColumnOrder="10" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
    </Task>
    <Task Type="Relation">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" />
      <Relation ElementID="66" RelationName="Deal Navigator -&gt; Deals" Description="" Relation="Inner Join" ObjectID1="13" ObjectID2="16" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="1">
        <RelationDetails>
          <RelationColumn ColumnID1="153" ColumnName1="DEAL_PK" ColumnID2="195" ColumnName2="DEAL_PK" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="13" ObjectSchema="" ObjectName="VV_RB_Deal_Navigator">
            <Column ID="153" ColumnName="DEAL_PK" />
          </Dependency>
          <Dependency Type="Object" ID="16" ObjectName="VV_RB_Deals">
            <Column ID="195" ColumnName="DEAL_PK" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="64" RelationName="Deal Navigator -&gt; Media Rights" Description="" Relation="Left Outer Join" ObjectID1="13" ObjectID2="22" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="1">
        <RelationDetails>
          <RelationColumn ColumnID1="158" ColumnName1="DEAL_ASSET_DETAIL_PK" ColumnID2="355" ColumnName2="DEAL_ASSET_DETAIL_PK" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="13" ObjectSchema="RLANLTCS" ObjectName="VV_RB_Deal_Navigator">
            <Column ID="158" ColumnName="DEAL_ASSET_DETAIL_PK" />
          </Dependency>
          <Dependency Type="Object" ID="22" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="355" ColumnName="DEAL_ASSET_DETAIL_PK" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="145" RelationName="Media Rights -&gt; Enduser Rights Tree" Description="" Relation="Left Outer Join" ObjectID1="22" ObjectID2="138" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="1764" ColumnName1="ID_ENDUSER_RIGHTS_TM" ColumnID2="2470" ColumnName2="ID_ENDUSER_RIGHTS_TM" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="1764" ColumnName="ID_ENDUSER_RIGHTS_TM" />
          </Dependency>
          <Dependency Type="Object" ID="138" ObjectName="MV_RB_ITEMS_ENDUSR_RIGHTS">
            <Column ID="2470" ColumnName="ID_ENDUSER_RIGHTS_TM" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="139" RelationName="Media Rights -&gt; Media Rights Tree" Description="" Relation="Left Outer Join" ObjectID1="22" ObjectID2="140" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="1758" ColumnName1="ID_MEDIA_RIGHT_TM" ColumnID2="2476" ColumnName2="ID_MEDIA_RIGHT_TM" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="1758" ColumnName="ID_MEDIA_RIGHT_TM" />
          </Dependency>
          <Dependency Type="Object" ID="140" ObjectName="MV_RB_ITEMS_MEDIA_RIGHTS">
            <Column ID="2476" ColumnName="ID_MEDIA_RIGHT_TM" />
          </Dependency>
        </Dependencies>
      </Relation>
    </Task>
    <Task Type="DataFormat">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" />
      <DataFormat ElementID="1" FormatKey="GeneralNumber" FormatName="General Number" Format="General Number" Internal="1" AppliesTo="9" Explanation="" IsAvailable="1" SortOrder="1" />
      <DataFormat ElementID="13" FormatKey="ShortDate" FormatName="Short Date" Format="Short Date" Internal="1" AppliesTo="10" Explanation="" IsAvailable="1" SortOrder="13" />
    </Task>
    <Task Type="SessionParameter">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" />
      <SessionParameter ElementID="1" ParameterName="spUserId" DefaultValue="1" DataTypeCategory="1" />
    </Task>
  </Tasks>
  <AppSettings CopyConnectionString="False" CopyGeneralSettings="False" CopyUIFeatures="False" CopyReportingOptions="False" CopyReportingFeatures="False" CopySecuritySettings="False" MultipleOrganizations="False" UniqueReportFolderNames="False" CopySchedulerServiceSettings="False" CopySchedulingConfiguration="False" CopySMTPServerSettings="False" CopyEmailConfiguration="False" CopyArchiveConfiguration="False" CopyArchiveSettings="False" CopyEventLogDBConnection="False" CopyEventLogConfiguration="False">
    <Database ElementID="1" MaxReportRecords="500000" MaxListRecords="10000" DesignTimeValidation="Off" DefaultTemplate="Custom-Blue" />
    <Organization ElementID="1" GroupName="Default" Description="" DefaultTheme="Rights" />
  </AppSettings>
</SynchronizationPackage>