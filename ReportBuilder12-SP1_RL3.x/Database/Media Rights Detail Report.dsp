<SynchronizationPackage SrcInstanceGUID="647128f6-d29c-4984-a7d2-d7f09f3aa467" SrcConnectionID="1" SrcGroupID="1" SrcVersion="12.1.43.1" IsSimpleView="False">
  <SelectedElements>
    <Element Type="Report" SelectAll="False" OverwriteExisting="True" OverwriteExistingDependencies="False" IncludeSchedules="False" IncludeSubscribers="False" IncludeArchives="False" PreserveOwner="1" PreserveFolder="1">
      <Report ElementID="325" />
    </Element>
  </SelectedElements>
  <Tasks>
    <Task Type="Report">
      <SynchronizationOptions OverwriteExisting="True" OverwriteExistingDependencies="False" IncludeSchedules="False" IncludeArchives="False" PreserveOwner="1" PreserveFolder="1" />
      <Report ElementID="325" ReportName="Media Rights Details Report" Description="" PhysicalName="" ExpirationDate="" FolderType="0" ReportGUID="ahReport325_446f60f1-d98f-4e6c-b729-91b1122e0bc1" Owner="anm56" ParentFolderID="0" Dashboard="0" Mobile="0">
        <Definition>
          <AdhocMetadata DefaultActiveItemID="3" ActiveStepIndex="1">
            <DataStructures>
              <DataStructure ID="1" Distinct="True" CategoryID="6">
                <DataSource ObjectID="16" ObjectKey="16" ObjectSchema="" ObjectName="VV_RB_Deals" Object1DatabaseID="" Object1Schema="" Object1Name="" Column1Name="" Object2DatabaseID="" Object2Schema="" Object2Name="" Column2Name="" Relation="" RelationID="0" RelationDirection="0" DatabaseID="1">
                  <DataSource ObjectID="13" ObjectKey="16_66_13_1" ObjectSchema="" ObjectName="VV_RB_Deal_Navigator" Object1DatabaseID="1" Object1Schema="" Object1Name="VV_RB_Deal_Navigator" Column1Name="DEAL_PK" Object2DatabaseID="1" Object2Schema="" Object2Name="VV_RB_Deals" Column2Name="DEAL_PK" Relation="Inner Join" RelationID="66" RelationDirection="1" DatabaseID="1">
                    <DataSource ObjectID="22" ObjectKey="16_66_13_1_64_22_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS" Object1DatabaseID="1" Object1Schema="" Object1Name="VV_RB_Deal_Navigator" Column1Name="DEAL_ASSET_DETAIL_PK" Object2DatabaseID="1" Object2Schema="RLANLTCS" Object2Name="MV_RB_DEAL_MEDIA_RIGHTS" Column2Name="DEAL_ASSET_DETAIL_PK" Relation="Left Outer Join" RelationID="64" RelationDirection="0" DatabaseID="1">
                      <DataSource ObjectID="140" ObjectKey="16_66_13_1_64_22_0_139_140_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_MEDIA_RIGHTS" Object1DatabaseID="1" Object1Schema="RLANLTCS" Object1Name="MV_RB_DEAL_MEDIA_RIGHTS" Column1Name="ID_MEDIA_RIGHT_TM" Object2DatabaseID="1" Object2Schema="RLANLTCS" Object2Name="MV_RB_ITEMS_MEDIA_RIGHTS" Column2Name="ID_MEDIA_RIGHT_TM" Relation="Left Outer Join" RelationID="139" RelationDirection="0" DatabaseID="1" />
                      <DataSource ObjectID="138" ObjectKey="16_66_13_1_64_22_0_145_138_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_ENDUSR_RIGHTS" Object1DatabaseID="1" Object1Schema="RLANLTCS" Object1Name="MV_RB_DEAL_MEDIA_RIGHTS" Column1Name="ID_ENDUSER_RIGHTS_TM" Object2DatabaseID="1" Object2Schema="RLANLTCS" Object2Name="MV_RB_ITEMS_ENDUSR_RIGHTS" Column2Name="ID_ENDUSER_RIGHTS_TM" Relation="Left Outer Join" RelationID="145" RelationDirection="0" DatabaseID="1" />
                      <DataSource ObjectID="92" ObjectKey="16_66_13_1_64_22_0_146_92_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_BUS_OUTLET" Object1DatabaseID="1" Object1Schema="RLANLTCS" Object1Name="MV_RB_DEAL_MEDIA_RIGHTS" Column1Name="ID_BUSINESS_OUTLET_TM" Object2DatabaseID="1" Object2Schema="RLANLTCS" Object2Name="MV_RB_ITEMS_BUS_OUTLET" Column2Name="ID_BUSINESS_OUTLET_TM" Relation="Left Outer Join" RelationID="146" RelationDirection="0" DatabaseID="1" />
                    </DataSource>
                    <DataSource ObjectID="67" ObjectKey="16_66_13_1_67_67_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ASSETS" Object1DatabaseID="1" Object1Schema="" Object1Name="VV_RB_Deal_Navigator" Column1Name="ASSET_PK" Object2DatabaseID="1" Object2Schema="RLANLTCS" Object2Name="MV_RB_ASSETS" Column2Name="ASSET_PK" Relation="Inner Join" RelationID="67" RelationDirection="0" DatabaseID="1" />
                  </DataSource>
                </DataSource>
                <Objects>
                  <Object ObjectID="16" ObjectKey="16" ObjectSchema="" ObjectName="VV_RB_Deals" ObjectIdentifier="16_0_0">
                    <Columns>
                      <Column ColumnID="196" ColumnName="Deal ID" />
                      <Column ColumnID="198" ColumnName="Deal Name" />
                    </Columns>
                  </Object>
                  <Object ObjectID="22" ObjectKey="16_66_13_1_64_22_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS" ObjectIdentifier="22_64_0">
                    <Columns>
                      <Column ColumnID="357" ColumnName="Media Type" />
                      <Column ColumnID="358" ColumnName="Venue" />
                      <Column ColumnID="359" ColumnName="Packaging" />
                      <Column ColumnID="363" ColumnName="Business Outlets" />
                      <Column ColumnID="364" ColumnName="Territory" />
                      <Column ColumnID="361" ColumnName="Language" />
                      <Column ColumnID="365" ColumnName="Term From" />
                      <Column ColumnID="366" ColumnName="Term To" />
                      <Column ColumnID="367" ColumnName="Exclusivity" />
                      <Column ColumnID="371" ColumnName="Restriction" />
                      <Column ColumnID="379" ColumnName="End User Rights" />
                      <Column ColumnID="2688" ColumnName="Notes Restricted" />
                      <Column ColumnID="2689" ColumnName="Notes Unrestricted" />
                    </Columns>
                  </Object>
                  <Object ObjectID="140" ObjectKey="16_66_13_1_64_22_0_139_140_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_MEDIA_RIGHTS" ObjectIdentifier="140_139_0">
                    <Columns>
                      <Column ColumnID="2474" ColumnName="Media Rights" />
                    </Columns>
                  </Object>
                  <Object ObjectID="138" ObjectKey="16_66_13_1_64_22_0_145_138_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_ENDUSR_RIGHTS" ObjectIdentifier="138_145_0">
                    <Columns>
                      <Column ColumnID="2468" ColumnName="Enduser Rights" />
                    </Columns>
                  </Object>
                  <Object ObjectID="92" ObjectKey="16_66_13_1_64_22_0_146_92_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_BUS_OUTLET" ObjectIdentifier="92_146_0">
                    <Columns>
                      <Column ColumnID="1652" ColumnName="Business Outlet" />
                    </Columns>
                  </Object>
                  <Object ObjectID="67" ObjectKey="16_66_13_1_67_67_0" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ASSETS" ObjectIdentifier="67_67_0">
                    <Columns>
                      <Column ColumnID="1045" ColumnName="Asset Title" />
                    </Columns>
                  </Object>
                </Objects>
                <OrderBy>
                  <Column ColumnID="198" ObjectKey="16" Direction="Ascending" />
                  <Column ColumnID="1045" ObjectKey="16_66_13_1_67_67_0" Direction="Ascending" />
                </OrderBy>
                <Parameter>
                  <Column ParamType="0" ID="3" ColumnID="1045" ObjectKey="16_66_13_1_67_67_0" Operator="Contains" BitCmd="And" ControlType="tb" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="Title Name" Level="0">
                    <Value Value="" ValueType="0" />
                  </Column>
                  <Column ParamType="0" ID="5" ColumnID="198" ObjectKey="16" Operator="Contains" BitCmd="And" ControlType="tb" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="Deal Name" Level="0">
                    <Value Value="" ValueType="0" />
                  </Column>
                  <Column ParamType="0" ID="1" ColumnID="365" ObjectKey="16_66_13_1_64_22_0" Operator="Between" BitCmd="And" ControlType="db" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="Term From" Level="0">
                    <Value Value="" ValueType="0" />
                    <Value Value="" ValueType="0" />
                  </Column>
                  <Column ParamType="0" ID="2" ColumnID="366" ObjectKey="16_66_13_1_64_22_0" Operator="Between" BitCmd="And" ControlType="db" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="Expiration Date" Level="0">
                    <Value Value="" ValueType="0" />
                    <Value Value="" ValueType="0" />
                  </Column>
                  <Column ParamType="0" ID="4" ColumnID="2474" ObjectKey="16_66_13_1_64_22_0_139_140_0" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="Media" Level="0">
                    <Value Value="001..All Media Rights" ValueType="3" />
                    <Value Value="002.......All TV" ValueType="3" />
                    <Value Value="003............Linear" ValueType="3" />
                    <Value Value="004.................Free" ValueType="3" />
                    <Value Value="005.................Basic" ValueType="3" />
                    <Value Value="006.................Pay" ValueType="3" />
                    <Value Value="007.................PPV" ValueType="3" />
                    <Value Value="008......................PPV Standard" ValueType="3" />
                    <Value Value="009......................PPV Premium Home Theatre" ValueType="3" />
                    <Value Value="010............On Demand" ValueType="3" />
                    <Value Value="011.................Subscription" ValueType="3" />
                    <Value Value="012......................Push" ValueType="3" />
                    <Value Value="013...........................Preview" ValueType="3" />
                    <Value Value="014...........................Catch Up" ValueType="3" />
                    <Value Value="015...........................Library" ValueType="3" />
                    <Value Value="016...........................Movies SVOD Window" ValueType="3" />
                    <Value Value="017......................Pull" ValueType="3" />
                    <Value Value="018...........................Preview" ValueType="3" />
                    <Value Value="019...........................Catch Up" ValueType="3" />
                    <Value Value="020...........................Library" ValueType="3" />
                    <Value Value="021...........................Movies SVOD Window" ValueType="3" />
                    <Value Value="022.................AVOD" ValueType="3" />
                    <Value Value="023......................Push" ValueType="3" />
                    <Value Value="024...........................Preview" ValueType="3" />
                    <Value Value="025...........................Catch Up" ValueType="3" />
                    <Value Value="026...........................Library" ValueType="3" />
                    <Value Value="027...........................Movies AVOD Window" ValueType="3" />
                    <Value Value="028......................Pull" ValueType="3" />
                    <Value Value="029...........................Preview" ValueType="3" />
                    <Value Value="030...........................Catch Up" ValueType="3" />
                    <Value Value="031...........................Library" ValueType="3" />
                    <Value Value="032...........................Movies AVOD Window" ValueType="3" />
                    <Value Value="033.................FVOD" ValueType="3" />
                    <Value Value="034......................Push" ValueType="3" />
                    <Value Value="035...........................Preview" ValueType="3" />
                    <Value Value="036...........................Catch Up" ValueType="3" />
                    <Value Value="037...........................Library" ValueType="3" />
                    <Value Value="038...........................Movies FVOD Window" ValueType="3" />
                    <Value Value="039......................Pull" ValueType="3" />
                    <Value Value="040...........................Preview" ValueType="3" />
                    <Value Value="041...........................Catch Up" ValueType="3" />
                    <Value Value="042...........................Library" ValueType="3" />
                    <Value Value="043...........................Movies FVOD Window" ValueType="3" />
                    <Value Value="044.................EST - Buy to Own" ValueType="3" />
                    <Value Value="045......................Push" ValueType="3" />
                    <Value Value="046...........................EST Standard" ValueType="3" />
                    <Value Value="047...........................EST DVD" ValueType="3" />
                    <Value Value="048...........................EST Blu-Ray" ValueType="3" />
                    <Value Value="049...........................EST Bonus Content" ValueType="3" />
                    <Value Value="050...........................EST Ultraviolet" ValueType="3" />
                    <Value Value="051......................Pull" ValueType="3" />
                    <Value Value="052...........................EST Standard" ValueType="3" />
                    <Value Value="053...........................EST DVD" ValueType="3" />
                    <Value Value="054...........................EST Blu-Ray" ValueType="3" />
                    <Value Value="055...........................EST Bonus Content" ValueType="3" />
                    <Value Value="056...........................EST Ultraviolet" ValueType="3" />
                    <Value Value="057.................TVOD" ValueType="3" />
                    <Value Value="058......................Push" ValueType="3" />
                    <Value Value="059...........................TVOD Standard" ValueType="3" />
                    <Value Value="060...........................TVOD Premium Home Theatre" ValueType="3" />
                    <Value Value="061......................Pull" ValueType="3" />
                    <Value Value="062...........................TVOD Standard" ValueType="3" />
                    <Value Value="063...........................TVOD Premium Home Theatre" ValueType="3" />
                    <Value Value="064.......All Radio" ValueType="3" />
                    <Value Value="065.......All Theatrical" ValueType="3" />
                    <Value Value="066............Pay" ValueType="3" />
                    <Value Value="067............Free" ValueType="3" />
                    <Value Value="068.......All Non-Theatrical" ValueType="3" />
                    <Value Value="069............Pay" ValueType="3" />
                    <Value Value="070.................Institutional" ValueType="3" />
                    <Value Value="071.................Transportation" ValueType="3" />
                    <Value Value="072.................Film Festivals" ValueType="3" />
                    <Value Value="073.................Tour" ValueType="3" />
                    <Value Value="074............Free" ValueType="3" />
                    <Value Value="075.................Institutional" ValueType="3" />
                    <Value Value="076.................Transportation" ValueType="3" />
                    <Value Value="077.................Film Festivals" ValueType="3" />
                    <Value Value="078.................Tour" ValueType="3" />
                    <Value Value="079.................Public (Non-Paid)" ValueType="3" />
                    <Value Value="080.......All Home Video" ValueType="3" />
                    <Value Value="081............Physical Sales" ValueType="3" />
                    <Value Value="082.................With Digital Copy" ValueType="3" />
                    <Value Value="083.................Cover Mount" ValueType="3" />
                    <Value Value="084......................Digital" ValueType="3" />
                    <Value Value="085......................Physical" ValueType="3" />
                    <Value Value="086.................Without Digital Copy" ValueType="3" />
                    <Value Value="087.......All Merchandising" ValueType="3" />
                    <Value Value="088............Toys" ValueType="3" />
                    <Value Value="089............Apparel" ValueType="3" />
                    <Value Value="090............Tools" ValueType="3" />
                    <Value Value="091............Housewares" ValueType="3" />
                    <Value Value="092............Music CDs" ValueType="3" />
                    <Value Value="093............Games" ValueType="3" />
                    <Value Value="094.................Paid For" ValueType="3" />
                    <Value Value="095......................Electronic Games" ValueType="3" />
                    <Value Value="096......................Board Games" ValueType="3" />
                    <Value Value="097......................Apps" ValueType="3" />
                    <Value Value="098......................Interactive Gaming" ValueType="3" />
                    <Value Value="099......................Online Games" ValueType="3" />
                    <Value Value="100.................Free" ValueType="3" />
                    <Value Value="101......................Electronic Games" ValueType="3" />
                    <Value Value="102......................Board Games" ValueType="3" />
                    <Value Value="103......................Apps" ValueType="3" />
                    <Value Value="104......................Interactive Gaming" ValueType="3" />
                    <Value Value="105......................Online Games" ValueType="3" />
                    <Value Value="106............Clips" ValueType="3" />
                    <Value Value="107.................Stills" ValueType="3" />
                    <Value Value="108.................Video with Music" ValueType="3" />
                    <Value Value="109.................Video without Music" ValueType="3" />
                    <Value Value="110............Novelties" ValueType="3" />
                    <Value Value="111.................Wallpaper" ValueType="3" />
                    <Value Value="112.................Ring Tones" ValueType="3" />
                    <Value Value="113.......Publishing" ValueType="3" />
                    <Value Value="114............Printed Publishing" ValueType="3" />
                    <Value Value="115.................Paperback" ValueType="3" />
                    <Value Value="116.................Coloring &amp; Activity Books" ValueType="3" />
                    <Value Value="117.................Hardcover" ValueType="3" />
                    <Value Value="118.................Picture Books (Mass Market)" ValueType="3" />
                    <Value Value="119.................Magazine (Retail Travel)" ValueType="3" />
                    <Value Value="120.................Stationery (Photos)" ValueType="3" />
                    <Value Value="121............ePublications" ValueType="3" />
                    <Value Value="122.................Paperback" ValueType="3" />
                    <Value Value="123.................Coloring &amp; Activity Books" ValueType="3" />
                    <Value Value="124.................Hardcover" ValueType="3" />
                    <Value Value="125.................Picture Books (Mass Market)" ValueType="3" />
                    <Value Value="126.................Magazine (Retail Travel)" ValueType="3" />
                    <Value Value="127.................Stationery (Photos)" ValueType="3" />
                    <Value Value="128.......Commentary" ValueType="3" />
                    <Value Value="129............Full Length" ValueType="3" />
                    <Value Value="130............Clips" ValueType="3" />
                    <Value Value="131.................Upto 1 Minute" ValueType="3" />
                    <Value Value="132.................Upto 2 Minute" ValueType="3" />
                    <Value Value="133.................Upto 3 Minute" ValueType="3" />
                    <Value Value="134.................Upto 4 Minute" ValueType="3" />
                    <Value Value="135.................Upto 5 Minute" ValueType="3" />
                    <Value Value="136.................Upto 6 Minute" ValueType="3" />
                    <Value Value="137.................Upto 7 Minute" ValueType="3" />
                    <Value Value="138.................Upto 8 Minute" ValueType="3" />
                    <Value Value="139.................Upto 9 Minute" ValueType="3" />
                    <Value Value="140.................Upto 10 Minute" ValueType="3" />
                    <Value Value="141.................Undefined" ValueType="3" />
                    <Value Value="142.......News" ValueType="3" />
                    <Value Value="143............Sky Sports News" ValueType="3" />
                    <Value Value="144.................Upto 1 Minute" ValueType="3" />
                    <Value Value="145.................Upto 2 Minute" ValueType="3" />
                    <Value Value="146.................Upto 3 Minute" ValueType="3" />
                    <Value Value="147.................Upto 4 Minute" ValueType="3" />
                    <Value Value="148.................Upto 5 Minute" ValueType="3" />
                    <Value Value="149.................Upto 6 Minute" ValueType="3" />
                    <Value Value="150.................Upto 7 Minute" ValueType="3" />
                    <Value Value="151.................Upto 8 Minute" ValueType="3" />
                    <Value Value="152.................Upto 9 Minute" ValueType="3" />
                    <Value Value="153.................Upto 10 Minute" ValueType="3" />
                    <Value Value="154.................Undefined" ValueType="3" />
                    <Value Value="155............Sky News" ValueType="3" />
                    <Value Value="156.................Upto 1 Minute" ValueType="3" />
                    <Value Value="157.................Upto 2 Minute" ValueType="3" />
                    <Value Value="158.................Upto 3 Minute" ValueType="3" />
                    <Value Value="159.................Upto 4 Minute" ValueType="3" />
                    <Value Value="160.................Upto 5 Minute" ValueType="3" />
                    <Value Value="161.................Upto 6 Minute" ValueType="3" />
                    <Value Value="162.................Upto 7 Minute" ValueType="3" />
                    <Value Value="163.................Upto 8 Minute" ValueType="3" />
                    <Value Value="164.................Upto 9 Minute" ValueType="3" />
                    <Value Value="165.................Upto 10 Minute" ValueType="3" />
                    <Value Value="166.................Undefined" ValueType="3" />
                    <Value Value="167.......Format Rights" ValueType="3" />
                  </Column>
                  <Column ParamType="0" ID="6" ColumnID="2468" ObjectKey="16_66_13_1_64_22_0_145_138_0" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="Product Offering" Level="0">
                    <Value Value="001..All Services" ValueType="3" />
                    <Value Value="002.......All Sky Branded Services" ValueType="3" />
                    <Value Value="003............Named Sky Branded Services" ValueType="3" />
                    <Value Value="004.................Sky Anytime" ValueType="3" />
                    <Value Value="005.................Sky Anytime +" ValueType="3" />
                    <Value Value="006.................Sky Store" ValueType="3" />
                    <Value Value="007.................Sky Player" ValueType="3" />
                    <Value Value="008.................Sky Go" ValueType="3" />
                    <Value Value="009.................Sky Apps" ValueType="3" />
                    <Value Value="010......................All Apps" ValueType="3" />
                    <Value Value="011......................Sky Sports Apps" ValueType="3" />
                    <Value Value="012.................Sky Anywhere" ValueType="3" />
                    <Value Value="013.................Now TV" ValueType="3" />
                    <Value Value="014.................Sky Go Extra" ValueType="3" />
                    <Value Value="015.......Non Branded Services" ValueType="3" />
                    <Value Value="016.......3rd Party Distributed" ValueType="3" />
                    <Value Value="017............British Telecom Plc." ValueType="3" />
                    <Value Value="018............Talk Talk Telecom Limited" ValueType="3" />
                    <Value Value="019............Virgin Media Limited" ValueType="3" />
                    <Value Value="020............UPC Communication Ireland Limited" ValueType="3" />
                    <Value Value="021............You View" ValueType="3" />
                    <Value Value="022............All Other 3rd Party Distributors" ValueType="3" />
                    <Value Value="023............Eircom" ValueType="3" />
                  </Column>
                  <Column ParamType="0" ID="7" ColumnID="1652" ObjectKey="16_66_13_1_64_22_0_146_92_0" Operator="In list" BitCmd="And" ControlType="ml" ParamDisplayMode="1" OfferAllOption="False" Ask="True" Caption="Channel" Level="0">
                    <Value Value="001..All Linear Channels" ValueType="3" />
                    <Value Value="002.......Sky Branded Channels" ValueType="3" />
                    <Value Value="003............Sky Cinema Channels" ValueType="3" />
                    <Value Value="004.................Sky Cinema Action/Adventure SD" ValueType="3" />
                    <Value Value="005.................Sky Cinema Action/Adventure HD" ValueType="3" />
                    <Value Value="006.................Sky Cinema Comedy SD" ValueType="3" />
                    <Value Value="007.................Sky Cinema Comedy HD" ValueType="3" />
                    <Value Value="008.................Sky Cinema Crime/Thriller SD" ValueType="3" />
                    <Value Value="009.................Sky Cinema Crime/Thriller HD" ValueType="3" />
                    <Value Value="010.................Sky Cinema Disney SD" ValueType="3" />
                    <Value Value="011.................Sky Cinema Disney HD" ValueType="3" />
                    <Value Value="012.................Sky Cinema Drama/Romance SD" ValueType="3" />
                    <Value Value="013.................Sky Cinema Drama/Romance HD" ValueType="3" />
                    <Value Value="014.................Sky Cinema Family SD" ValueType="3" />
                    <Value Value="015.................Sky Cinema Family HD" ValueType="3" />
                    <Value Value="016.................Sky Cinema Greats SD" ValueType="3" />
                    <Value Value="017.................Sky Cinema Greats HD" ValueType="3" />
                    <Value Value="018.................Sky Cinema Hits SD" ValueType="3" />
                    <Value Value="019.................Sky Cinema Hits HD" ValueType="3" />
                    <Value Value="020.................Sky Cinema Premiere SD" ValueType="3" />
                    <Value Value="021.................Sky Cinema Premiere SD + 1" ValueType="3" />
                    <Value Value="022.................Sky Cinema Premiere HD" ValueType="3" />
                    <Value Value="023.................Sky Cinema Sci-Fi/Horror SD" ValueType="3" />
                    <Value Value="024.................Sky Cinema Sci-Fi/Horror HD" ValueType="3" />
                    <Value Value="025.................Sky Cinema Select SD" ValueType="3" />
                    <Value Value="026.................Sky Cinema Select HD" ValueType="3" />
                    <Value Value="027............Sky Box Office &amp; PPV Channels" ValueType="3" />
                    <Value Value="028.................SBO HD channels" ValueType="3" />
                    <Value Value="029......................SBO HD 1" ValueType="3" />
                    <Value Value="030......................SBO HD 2" ValueType="3" />
                    <Value Value="031......................SBO HD 3" ValueType="3" />
                    <Value Value="032......................_PPV SD Sports Chnl 491" ValueType="3" />
                    <Value Value="033......................_PPV HD Sports Chnl 492" ValueType="3" />
                    <Value Value="034.................PPV channels" ValueType="3" />
                    <Value Value="035......................_PPV Event 01" ValueType="3" />
                    <Value Value="036......................_PPV Chnl 701" ValueType="3" />
                    <Value Value="037......................_PPV Chnl 702" ValueType="3" />
                    <Value Value="038......................_PPV Chnl 703" ValueType="3" />
                    <Value Value="039......................_PPV Chnl 704" ValueType="3" />
                    <Value Value="040......................_PPV Chnl 705" ValueType="3" />
                    <Value Value="041......................_PPV Chnl 706" ValueType="3" />
                    <Value Value="042......................_PPV Chnl 707" ValueType="3" />
                    <Value Value="043......................_PPV Chnl 708" ValueType="3" />
                    <Value Value="044......................_PPV Chnl 709" ValueType="3" />
                    <Value Value="045......................_PPV Chnl 710" ValueType="3" />
                    <Value Value="046......................_PPV Chnl 711" ValueType="3" />
                    <Value Value="047......................_PPV Chnl 712" ValueType="3" />
                    <Value Value="048......................_PPV Chnl 713" ValueType="3" />
                    <Value Value="049......................_PPV Chnl 714" ValueType="3" />
                    <Value Value="050......................_PPV Chnl 715" ValueType="3" />
                    <Value Value="051......................_PPV Chnl 716" ValueType="3" />
                    <Value Value="052......................_PPV Chnl 717" ValueType="3" />
                    <Value Value="053......................_PPV Chnl 718" ValueType="3" />
                    <Value Value="054......................_PPV Chnl 719" ValueType="3" />
                    <Value Value="055......................_PPV Chnl 720" ValueType="3" />
                    <Value Value="056......................_PPV Chnl 721" ValueType="3" />
                    <Value Value="057......................_PPV Chnl 722" ValueType="3" />
                    <Value Value="058......................_PPV Chnl 723" ValueType="3" />
                    <Value Value="059......................_PPV Chnl 724" ValueType="3" />
                    <Value Value="060......................_PPV Chnl 725" ValueType="3" />
                    <Value Value="061......................_PPV Chnl 726" ValueType="3" />
                    <Value Value="062......................_PPV Chnl 727" ValueType="3" />
                    <Value Value="063......................_PPV Chnl 728" ValueType="3" />
                    <Value Value="064......................_PPV Chnl 729" ValueType="3" />
                    <Value Value="065......................_PPV Chnl 730" ValueType="3" />
                    <Value Value="066......................_PPV Chnl 731" ValueType="3" />
                    <Value Value="067......................_PPV Chnl 732" ValueType="3" />
                    <Value Value="068......................_PPV Chnl 733" ValueType="3" />
                    <Value Value="069......................_PPV Chnl 734" ValueType="3" />
                    <Value Value="070......................_PPV Chnl 735" ValueType="3" />
                    <Value Value="071......................_PPV Chnl 736" ValueType="3" />
                    <Value Value="072......................_PPV Chnl 737 previously 740" ValueType="3" />
                    <Value Value="073......................_PPV Chnl 738 previously 741" ValueType="3" />
                    <Value Value="074......................_PPV Chnl 739 previously 742" ValueType="3" />
                    <Value Value="075......................_PPV Chnl 744 previously 739" ValueType="3" />
                    <Value Value="076......................_PPV Chnl 746 was 747/756" ValueType="3" />
                    <Value Value="077......................_PPV Chnl 747 was 748/757" ValueType="3" />
                    <Value Value="078......................_PPV Chnl 748 was 749/758" ValueType="3" />
                    <Value Value="079......................_PPV Chnl 749 was 750/759" ValueType="3" />
                    <Value Value="080......................_PPV Chnl 754" ValueType="3" />
                    <Value Value="081......................_PPV Chnl 755" ValueType="3" />
                    <Value Value="082......................_PPV Chnl 756" ValueType="3" />
                    <Value Value="083......................_PPV Chnl 757" ValueType="3" />
                    <Value Value="084......................_PPV Chnl 758" ValueType="3" />
                    <Value Value="085......................_PPV Chnl 759" ValueType="3" />
                    <Value Value="086......................_PPV Chnl 760" ValueType="3" />
                    <Value Value="087......................_PPV Chnl 761" ValueType="3" />
                    <Value Value="088............Sky Sports Channels" ValueType="3" />
                    <Value Value="089.................Sky Sports Cricket Channels" ValueType="3" />
                    <Value Value="090......................Sky Sports Cricket SD" ValueType="3" />
                    <Value Value="091......................Sky Sports Cricket SD COMM" ValueType="3" />
                    <Value Value="092......................Sky Sports Cricket SD HOTEL" ValueType="3" />
                    <Value Value="093......................Sky Sports Cricket SD PUB" ValueType="3" />
                    <Value Value="094......................Sky Sports Cricket SD ROI" ValueType="3" />
                    <Value Value="095......................Sky Sports Cricket HD" ValueType="3" />
                    <Value Value="096......................Sky Sports Cricket HD COMM" ValueType="3" />
                    <Value Value="097......................Sky Sports Cricket HD PUB" ValueType="3" />
                    <Value Value="098......................Sky Sports Cricket Contingency" ValueType="3" />
                    <Value Value="099.................Sky Sports Golf Channels" ValueType="3" />
                    <Value Value="100......................Sky Sports Golf SD" ValueType="3" />
                    <Value Value="101......................Sky Sports Golf SD COMM" ValueType="3" />
                    <Value Value="102......................Sky Sports Golf SD HOTEL" ValueType="3" />
                    <Value Value="103......................Sky Sports Golf SD PUB" ValueType="3" />
                    <Value Value="104......................Sky Sports Golf SD ROI" ValueType="3" />
                    <Value Value="105......................Sky Sports Golf SD ROI COMM" ValueType="3" />
                    <Value Value="106......................Sky Sports Golf HD" ValueType="3" />
                    <Value Value="107......................Sky Sports Golf HD COMM" ValueType="3" />
                    <Value Value="108......................Sky Sports Golf HD PUB" ValueType="3" />
                    <Value Value="109......................Sky Sports Golf Contingency" ValueType="3" />
                    <Value Value="110.................Sky Sports Premier League Channels" ValueType="3" />
                    <Value Value="111......................Sky Sports Prem Lge SD" ValueType="3" />
                    <Value Value="112......................Sky Sports Prem Lge SD COMM" ValueType="3" />
                    <Value Value="113......................Sky Sports Prem Lge SD HOTEL" ValueType="3" />
                    <Value Value="114......................Sky Sports Prem Lge SD PUB" ValueType="3" />
                    <Value Value="115......................Sky Sports Prem Lge SD ROI" ValueType="3" />
                    <Value Value="116......................Sky Sports Prem Lge SD ROI COM" ValueType="3" />
                    <Value Value="117......................Sky Sports Prem Lge HD" ValueType="3" />
                    <Value Value="118......................Sky Sports Prem Lge HD COMM" ValueType="3" />
                    <Value Value="119......................Sky Sports Prem Lge HD PUB" ValueType="3" />
                    <Value Value="120......................Sky Sports Prem Lge HD ROI" ValueType="3" />
                    <Value Value="121......................Sky Sports Prem Lge HD ROI COM" ValueType="3" />
                    <Value Value="122.................Sky Sports Football Channels" ValueType="3" />
                    <Value Value="123......................Sky Sports Football SD" ValueType="3" />
                    <Value Value="124......................Sky Sports Football SD COMM" ValueType="3" />
                    <Value Value="125......................Sky Sports Football SD HOTEL" ValueType="3" />
                    <Value Value="126......................Sky Sports Football SD PUB" ValueType="3" />
                    <Value Value="127......................Sky Sports Football SD ROI" ValueType="3" />
                    <Value Value="128......................Sky Sports Football SD ROI CO" ValueType="3" />
                    <Value Value="129......................Sky Sports Football HD" ValueType="3" />
                    <Value Value="130......................Sky Sports Football HD COMM" ValueType="3" />
                    <Value Value="131......................Sky Sports Football HD PUB" ValueType="3" />
                    <Value Value="132.................Sky Sports Main Event Channels" ValueType="3" />
                    <Value Value="133......................Sky Sports Main Event SD" ValueType="3" />
                    <Value Value="134......................Sky Sports Main Event SD COMM" ValueType="3" />
                    <Value Value="135......................Sky Sports Main Event SD HOTEL" ValueType="3" />
                    <Value Value="136......................Sky Sports Main Event SD PUB" ValueType="3" />
                    <Value Value="137......................Sky Sports Main Event SD ROI" ValueType="3" />
                    <Value Value="138......................Sky Sports Main Event SD ROI C" ValueType="3" />
                    <Value Value="139......................Sky Sports Main Event HD" ValueType="3" />
                    <Value Value="140......................Sky Sports Main Event HD ROI" ValueType="3" />
                    <Value Value="141......................Sky Sports Main Event HD COMM" ValueType="3" />
                    <Value Value="142......................Sky Sports Main Event HD PUB" ValueType="3" />
                    <Value Value="143......................Sky Sports Main Event HD ROI C" ValueType="3" />
                    <Value Value="144......................Sky Sports Main Event HD 3P" ValueType="3" />
                    <Value Value="145......................Sky Sports Main Event Contin" ValueType="3" />
                    <Value Value="146.................Sky Sports Action Channels" ValueType="3" />
                    <Value Value="147......................Sky Sports Action SD" ValueType="3" />
                    <Value Value="148......................Sky Sports Action SD COMM" ValueType="3" />
                    <Value Value="149......................Sky Sports Action SD HOTEL" ValueType="3" />
                    <Value Value="150......................Sky Sports Action SD PUB" ValueType="3" />
                    <Value Value="151......................Sky Sports Action SD ROI" ValueType="3" />
                    <Value Value="152......................Sky Sports Action HD" ValueType="3" />
                    <Value Value="153......................Sky Sports Action HD COMM" ValueType="3" />
                    <Value Value="154......................Sky Sports Action HD PUB" ValueType="3" />
                    <Value Value="155.................Sky Sports Arena Channels" ValueType="3" />
                    <Value Value="156......................Sky Sports Arena SD" ValueType="3" />
                    <Value Value="157......................Sky Sports Arena SD COMM" ValueType="3" />
                    <Value Value="158......................Sky Sports Arena SD HOTEL" ValueType="3" />
                    <Value Value="159......................Sky Sports Arena SD PUB" ValueType="3" />
                    <Value Value="160......................Sky Sports Arena SD ROI" ValueType="3" />
                    <Value Value="161......................Sky Sports Arena SD ROI COMM" ValueType="3" />
                    <Value Value="162......................Sky Sports Arena HD" ValueType="3" />
                    <Value Value="163......................Sky Sports Arena HD COMM" ValueType="3" />
                    <Value Value="164......................Sky Sports Arena HD PUB" ValueType="3" />
                    <Value Value="165.................Sky Sports F1 channels" ValueType="3" />
                    <Value Value="166......................Sky Sports F1 SD" ValueType="3" />
                    <Value Value="167......................Sky Sports F1 SD COMM" ValueType="3" />
                    <Value Value="168......................Sky Sports F1 HD" ValueType="3" />
                    <Value Value="169......................Sky Sports F1 HD COMM" ValueType="3" />
                    <Value Value="170......................Sky Sports F1 Used for Contin" ValueType="3" />
                    <Value Value="171.................Sky Sports News channels" ValueType="3" />
                    <Value Value="172......................Sky Sports News SD" ValueType="3" />
                    <Value Value="173......................Sky Sports News HD" ValueType="3" />
                    <Value Value="174......................Sky Sports News HD Int'l" ValueType="3" />
                    <Value Value="175......................Sky Sports News HD 3P" ValueType="3" />
                    <Value Value="176......................Sky Sports News SD PUB" ValueType="3" />
                    <Value Value="177......................Sky Sports News HD PUB" ValueType="3" />
                    <Value Value="178......................Sky Sports News HD ROI" ValueType="3" />
                    <Value Value="179......................Sky Sports News HD ROI 3P" ValueType="3" />
                    <Value Value="180......................Sky Sports News SD ROI" ValueType="3" />
                    <Value Value="181......................Sky Sports News OTT" ValueType="3" />
                    <Value Value="182......................Sky Sports News SD COMM" ValueType="3" />
                    <Value Value="183......................Sky Sports News HD COMM" ValueType="3" />
                    <Value Value="184......................Sky Sports News SD ROI COMM" ValueType="3" />
                    <Value Value="185.................Sky Sports Mix channels" ValueType="3" />
                    <Value Value="186......................Sky Sports Mix SD" ValueType="3" />
                    <Value Value="187......................Sky Sports Mix HD" ValueType="3" />
                    <Value Value="188......................Sky Sports Mix SD COMM" ValueType="3" />
                    <Value Value="189......................Sky Sports Mix HD COMM" ValueType="3" />
                    <Value Value="190......................Sky Sports Mix HD 3P" ValueType="3" />
                    <Value Value="191.................Sky Sports Racing channels" ValueType="3" />
                    <Value Value="192......................Sky Sports Racing HD" ValueType="3" />
                    <Value Value="193......................Sky Sports Racing SD" ValueType="3" />
                    <Value Value="194......................Sky Sports Racing SD COMM" ValueType="3" />
                    <Value Value="195......................Sky Sports Racing HD COMM" ValueType="3" />
                    <Value Value="196......................Sky Sports Racing SD ROI" ValueType="3" />
                    <Value Value="197............Sky Entertainment Channels" ValueType="3" />
                    <Value Value="198.................Sky 1 channels" ValueType="3" />
                    <Value Value="199......................Sky 1 SD" ValueType="3" />
                    <Value Value="200......................Sky 1 HD" ValueType="3" />
                    <Value Value="201......................Sky 1 HD COMM" ValueType="3" />
                    <Value Value="202......................Sky 1 SD EPG + 1" ValueType="3" />
                    <Value Value="203......................Sky 1 SD COMM" ValueType="3" />
                    <Value Value="204......................Sky 1 SD ROI" ValueType="3" />
                    <Value Value="205......................Sky 1 HD 3P" ValueType="3" />
                    <Value Value="206......................Sky 1 HD ROI" ValueType="3" />
                    <Value Value="207......................Sky 1 HD ROI 3P" ValueType="3" />
                    <Value Value="208.................Sky 2 channels" ValueType="3" />
                    <Value Value="209......................Sky 2 SD" ValueType="3" />
                    <Value Value="210......................Sky 2 HD" ValueType="3" />
                    <Value Value="211......................Sky 2 SD ROI" ValueType="3" />
                    <Value Value="212.................Sky Arts channels" ValueType="3" />
                    <Value Value="213......................Sky Arts 1 SD" ValueType="3" />
                    <Value Value="214......................Sky Arts 1 HD" ValueType="3" />
                    <Value Value="215......................Sky Arts 2 SD" ValueType="3" />
                    <Value Value="216......................Sky Arts 2 HD" ValueType="3" />
                    <Value Value="217.................Sky Living channels" ValueType="3" />
                    <Value Value="218......................Sky Living SD" ValueType="3" />
                    <Value Value="219......................Sky Living SD + 1" ValueType="3" />
                    <Value Value="220......................Sky Living SD ROI" ValueType="3" />
                    <Value Value="221......................Sky Living SD ROI + 1" ValueType="3" />
                    <Value Value="222......................Sky Living HD" ValueType="3" />
                    <Value Value="223......................Sky Living HD 3P" ValueType="3" />
                    <Value Value="224......................Sky Living HD ROI" ValueType="3" />
                    <Value Value="225.................Sky Living IT channels" ValueType="3" />
                    <Value Value="226......................Real Lives SD" ValueType="3" />
                    <Value Value="227......................Real Lives SD + 1" ValueType="3" />
                    <Value Value="228.................Sky Atlantic channels" ValueType="3" />
                    <Value Value="229......................Sky Atlantic SD" ValueType="3" />
                    <Value Value="230......................Sky Atlantic HD" ValueType="3" />
                    <Value Value="231......................Sky Atlantic SD + 1" ValueType="3" />
                    <Value Value="232......................Sky Atlantic SD ROI" ValueType="3" />
                    <Value Value="233............Sky 3D Channels" ValueType="3" />
                    <Value Value="234.................Sky 3D TV" ValueType="3" />
                    <Value Value="235.................Sky 3D TV COMM" ValueType="3" />
                    <Value Value="236............Sky News Channels" ValueType="3" />
                    <Value Value="237.................Sky News SD" ValueType="3" />
                    <Value Value="238.................Sky News SD ROI" ValueType="3" />
                    <Value Value="239.................Sky News SD Freeview" ValueType="3" />
                    <Value Value="240.................Sky News HD" ValueType="3" />
                    <Value Value="241.................Sky News Direct SD" ValueType="3" />
                    <Value Value="242.................Sky News International SD" ValueType="3" />
                    <Value Value="243.................Sky News International HD" ValueType="3" />
                    <Value Value="244.................Sky News HD ROI" ValueType="3" />
                    <Value Value="245............Sky PUSH VOD Channels (Sky Anytime)" ValueType="3" />
                    <Value Value="246.................PVOD HD" ValueType="3" />
                    <Value Value="247......................PVOD HD 1" ValueType="3" />
                    <Value Value="248......................PVOD HD 2" ValueType="3" />
                    <Value Value="249......................PVOD HD 3" ValueType="3" />
                    <Value Value="250.................PVOD SD" ValueType="3" />
                    <Value Value="251......................PVOD SD 1" ValueType="3" />
                    <Value Value="252......................PVOD SD 2" ValueType="3" />
                    <Value Value="253............Sky Interactive Channels" ValueType="3" />
                    <Value Value="254.................Sky Sports Interactive Hi channels" ValueType="3" />
                    <Value Value="255......................Sky Sports Interactive Hi 1" ValueType="3" />
                    <Value Value="256......................Sky Sports Interactive Hi 2" ValueType="3" />
                    <Value Value="257......................Sky Sports Interactive Hi 3" ValueType="3" />
                    <Value Value="258......................Sky Sports Interactive Hi 4" ValueType="3" />
                    <Value Value="259......................Sky Sports Interactive Hi 5" ValueType="3" />
                    <Value Value="260.................Sky Sports Interactive Lo channels" ValueType="3" />
                    <Value Value="261......................Sky Sports Interactive Lo 1" ValueType="3" />
                    <Value Value="262......................Sky Sports Interactive Lo 2" ValueType="3" />
                    <Value Value="263......................Sky Sports Interactive Lo 3" ValueType="3" />
                    <Value Value="264......................Sky Sports Interactive Lo 4" ValueType="3" />
                    <Value Value="265......................Sky Sports Interactive Lo 5" ValueType="3" />
                    <Value Value="266......................Sky Sports Interactive Lo 6" ValueType="3" />
                    <Value Value="267......................Sky Sports Interactive Lo 7" ValueType="3" />
                    <Value Value="268......................Sky Sports Interactive Lo 8" ValueType="3" />
                    <Value Value="269......................Sky Sports Interactive Lo 9" ValueType="3" />
                    <Value Value="270............Sky Commercial Channels" ValueType="3" />
                    <Value Value="271.................Sky Sports SD PUB" ValueType="3" />
                    <Value Value="272.................Sky Sports HD PUB" ValueType="3" />
                    <Value Value="273.................Sky Poker SD" ValueType="3" />
                    <Value Value="274............Sky Barker/Promo Channels" ValueType="3" />
                    <Value Value="275.................HD Barker channel" ValueType="3" />
                    <Value Value="276.................Multiplex Barker" ValueType="3" />
                    <Value Value="277.................NVOD Barker 1 Family Daytime" ValueType="3" />
                    <Value Value="278.................NVOD Preview channel" ValueType="3" />
                    <Value Value="279.................Project Cruise" ValueType="3" />
                    <Value Value="280.................Green Button" ValueType="3" />
                    <Value Value="281.................SDIC Sky Customer Chnl 999" ValueType="3" />
                    <Value Value="282.................SDIC Sky Welcome 998" ValueType="3" />
                    <Value Value="283.................Sky Staff HD" ValueType="3" />
                    <Value Value="284.................VCID Test channel" ValueType="3" />
                    <Value Value="285................._Sky Sports Barker" ValueType="3" />
                    <Value Value="286............Sky UHD Channels" ValueType="3" />
                    <Value Value="287.................Sky Sports Interactive UHD channels" ValueType="3" />
                    <Value Value="288......................Sky UHD 1" ValueType="3" />
                    <Value Value="289......................Sky UHD 2" ValueType="3" />
                    <Value Value="290......................Sky UHD Test" ValueType="3" />
                    <Value Value="291.......Non Branded Channels" ValueType="3" />
                    <Value Value="292............3rd Party VOD Channel" ValueType="3" />
                    <Value Value="293............Entertainment Channels" ValueType="3" />
                    <Value Value="294.................Pick TV channels" ValueType="3" />
                    <Value Value="295......................Pick TV SD" ValueType="3" />
                    <Value Value="296......................Pick TV SD + 1" ValueType="3" />
                    <Value Value="297......................Pick TV SD Freeview" ValueType="3" />
                    <Value Value="298......................Pick TV SD ROI" ValueType="3" />
                    <Value Value="299.................Challenge channels" ValueType="3" />
                    <Value Value="300......................Challenge SD" ValueType="3" />
                    <Value Value="301......................Challenge SD + 1" ValueType="3" />
                    <Value Value="302......................Challenge SD ROI" ValueType="3" />
                    <Value Value="303............3rd Party Channels (managed by Sky)" ValueType="3" />
                    <Value Value="304.................MGM HD" ValueType="3" />
                    <Value Value="305.................At the Races channels" ValueType="3" />
                    <Value Value="306......................At The Races SD" ValueType="3" />
                    <Value Value="307......................At the Races SD COMM" ValueType="3" />
                    <Value Value="308......................At the Races SD ROI" ValueType="3" />
                    <Value Value="309.................Fox News SD" ValueType="3" />
                    <Value Value="310.................Chelsea TV SD" ValueType="3" />
                    <Value Value="311............JV Channels" ValueType="3" />
                    <Value Value="312.................Biography channels" ValueType="3" />
                    <Value Value="313......................Biography SD" ValueType="3" />
                    <Value Value="314......................Biography SD + 1" ValueType="3" />
                    <Value Value="315.................C &amp; I channels" ValueType="3" />
                    <Value Value="316......................CI SD" ValueType="3" />
                    <Value Value="317......................CI SD + 1" ValueType="3" />
                    <Value Value="318......................CI HD" ValueType="3" />
                    <Value Value="319......................CI Africa" ValueType="3" />
                    <Value Value="320......................CI Europe" ValueType="3" />
                    <Value Value="321......................CI Poland" ValueType="3" />
                    <Value Value="322.................Fox channels" ValueType="3" />
                    <Value Value="323......................Fox SD" ValueType="3" />
                    <Value Value="324......................Fox SD + 1" ValueType="3" />
                    <Value Value="325......................Fox HD" ValueType="3" />
                    <Value Value="326.................History channels" ValueType="3" />
                    <Value Value="327......................History SD" ValueType="3" />
                    <Value Value="328......................History SD + 1" ValueType="3" />
                    <Value Value="329......................History HD" ValueType="3" />
                    <Value Value="330......................History Europe" ValueType="3" />
                    <Value Value="331......................History Europe HD" ValueType="3" />
                    <Value Value="332......................History Romania" ValueType="3" />
                    <Value Value="333......................History South Africa" ValueType="3" />
                    <Value Value="334......................History Poland" ValueType="3" />
                    <Value Value="335......................History Russia" ValueType="3" />
                    <Value Value="336......................H2 Poland" ValueType="3" />
                    <Value Value="337......................H2 Europe" ValueType="3" />
                    <Value Value="338......................H2 SD" ValueType="3" />
                    <Value Value="339.................NGC channels" ValueType="3" />
                    <Value Value="340......................NGC UK SD" ValueType="3" />
                    <Value Value="341......................NGC UK SD + 1" ValueType="3" />
                    <Value Value="342......................NGC UK HD" ValueType="3" />
                    <Value Value="343......................NGC Wild SD" ValueType="3" />
                    <Value Value="344......................NGC Wild HD" ValueType="3" />
                    <Value Value="345......................NGC Wild Europe" ValueType="3" />
                    <Value Value="346.................Lifetime channels" ValueType="3" />
                    <Value Value="347......................Lifetime SD" ValueType="3" />
                    <Value Value="348......................Lifetime +1" ValueType="3" />
                    <Value Value="349......................Lifetime Africa" ValueType="3" />
                    <Value Value="350......................Lifetime Europe" ValueType="3" />
                    <Value Value="351......................Lifetime Poland" ValueType="3" />
                    <Value Value="352......................Lifetime HD" ValueType="3" />
                    <Value Value="353............Adult Channels" ValueType="3" />
                    <Value Value="354.................Adult Nightly 2(XXX18 &amp; Dirty)" ValueType="3" />
                    <Value Value="355.................Adult Nightly 3 (XXX Mums)" ValueType="3" />
                    <Value Value="356.................Adult Nightly 7 (Playboy Lesb)" ValueType="3" />
                    <Value Value="357.................Adult Nightly11 (XXX Brits)" ValueType="3" />
                    <Value Value="358.................Adult Nightly12 (Girl Next Do)" ValueType="3" />
                    <Value Value="359.................Adult Nightly13 (ViewersWives)" ValueType="3" />
                    <Value Value="360.................Adult Nightly 14 (Electric Blue)" ValueType="3" />
                    <Value Value="361.................Adult Nightly 15 (XXXcess)" ValueType="3" />
                    <Value Value="362.................Adult Nightly16 (XXX Amateurs)" ValueType="3" />
                    <Value Value="363.................Adult Nightly17 (XXX GirlGirl)" ValueType="3" />
                    <Value Value="364.................Adult Nightly18 (XXX Extreme)" ValueType="3" />
                    <Value Value="365.................Adult Nightly19 (XXXBig&amp;Boun)" ValueType="3" />
                    <Value Value="366............Commercial Channels" ValueType="3" />
                    <Value Value="367.................Pub Channel" ValueType="3" />
                    <Value Value="368.......Sky Branded DR" ValueType="3" />
                    <Value Value="369............Sky Cinema DR Channels" ValueType="3" />
                    <Value Value="370.................Sky Cinema Action/Adventure SD DR" ValueType="3" />
                    <Value Value="371.................Sky Cinema Comedy SD DR" ValueType="3" />
                    <Value Value="372.................Sky Cinema Crime/Thriller SD DR" ValueType="3" />
                    <Value Value="373.................Sky Cinema Disney SD DR" ValueType="3" />
                    <Value Value="374.................Sky Cinema Drama/Romance SD DR" ValueType="3" />
                    <Value Value="375.................Sky Cinema Family SD DR" ValueType="3" />
                    <Value Value="376.................Sky Cinema Greats SD DR" ValueType="3" />
                    <Value Value="377.................Sky Cinema Premiere SD DR" ValueType="3" />
                    <Value Value="378.................Sky Cinema Premiere SD + 1 DR" ValueType="3" />
                    <Value Value="379.................Sky Cinema Sci-Fi/Horror SD DR" ValueType="3" />
                    <Value Value="380.................Sky Cinema Select SD DR" ValueType="3" />
                    <Value Value="381.................Sky Cinema Hits SD DR" ValueType="3" />
                    <Value Value="382............Sky Entertainment DR Channels" ValueType="3" />
                    <Value Value="383.................Sky 1 DR channels" ValueType="3" />
                    <Value Value="384......................Sky 1 SD DR" ValueType="3" />
                    <Value Value="385......................Sky 1 SD ROI DR" ValueType="3" />
                    <Value Value="386......................Sky 1 HD DR" ValueType="3" />
                    <Value Value="387.................Sky Arts SD DR" ValueType="3" />
                    <Value Value="388.................Sky 2 SD DR" ValueType="3" />
                    <Value Value="389.................Sky Living DR channels" ValueType="3" />
                    <Value Value="390......................Sky Living SD DR" ValueType="3" />
                    <Value Value="391......................Sky Living HD DR" ValueType="3" />
                    <Value Value="392......................Sky Living SD ROI DR" ValueType="3" />
                    <Value Value="393.................Sky Atlantic DR channels" ValueType="3" />
                    <Value Value="394......................Sky Atlantic SD DR" ValueType="3" />
                    <Value Value="395......................Sky Atlantic SD ROI DR" ValueType="3" />
                    <Value Value="396......................Sky Atlantic HD DR" ValueType="3" />
                    <Value Value="397............Sky News DR Channels" ValueType="3" />
                    <Value Value="398.................Sky News SD DR" ValueType="3" />
                    <Value Value="399.................Sky News HD DR" ValueType="3" />
                    <Value Value="400.................Sky News Direct SD DR" ValueType="3" />
                    <Value Value="401.................Sky News SD ROI DR" ValueType="3" />
                    <Value Value="402.................Sky News Freeview DR" ValueType="3" />
                    <Value Value="403.................Sky News International  DR" ValueType="3" />
                    <Value Value="404.......Non Branded DR" ValueType="3" />
                    <Value Value="405............Commerical DR Channels" ValueType="3" />
                    <Value Value="406.................Pub Channel DR" ValueType="3" />
                    <Value Value="407............Entertainment DR channels" ValueType="3" />
                    <Value Value="408.................Pick TV SD DR" ValueType="3" />
                    <Value Value="409.......Sky Branded Archive" ValueType="3" />
                    <Value Value="410............Sky Movies Archive Channels" ValueType="3" />
                    <Value Value="411.................zzSky Cinema Classics SD" ValueType="3" />
                    <Value Value="412.................zzSky Cinema Classics SD DR" ValueType="3" />
                    <Value Value="413................._zzSky Movies Active 7" ValueType="3" />
                    <Value Value="414.................zzSky Cinema Classics HD" ValueType="3" />
                    <Value Value="415................._zzSky Movies Active 8" ValueType="3" />
                    <Value Value="416................._zzSky Movies 1" ValueType="3" />
                    <Value Value="417................._zzSky Movies 1 Digital" ValueType="3" />
                    <Value Value="418................._zzSky Movies 1 Analogue" ValueType="3" />
                    <Value Value="419................._zzSky Movies 1 Freeview" ValueType="3" />
                    <Value Value="420................._zzSky Movies 2" ValueType="3" />
                    <Value Value="421................._zzSky Movies 2 Digital" ValueType="3" />
                    <Value Value="422................._zzSky Movies 2 Analogue" ValueType="3" />
                    <Value Value="423................._zzSky Movies 2 Freeview" ValueType="3" />
                    <Value Value="424................._zzSky Movies 3" ValueType="3" />
                    <Value Value="425................._zzSky Movies 4" ValueType="3" />
                    <Value Value="426................._zzSky Movies 5" ValueType="3" />
                    <Value Value="427................._zzSky Movies 6" ValueType="3" />
                    <Value Value="428................._zzSky Movies 7" ValueType="3" />
                    <Value Value="429................._zzSky Movies 8" ValueType="3" />
                    <Value Value="430................._zzSky Movies 9" ValueType="3" />
                    <Value Value="431................._zzSky Movies 10" ValueType="3" />
                    <Value Value="432................._zzOLD Sky Movies 10 SD" ValueType="3" />
                    <Value Value="433................._zzSky Cinema 1 Digital" ValueType="3" />
                    <Value Value="434................._zzSky Cinema 1 Analogue" ValueType="3" />
                    <Value Value="435................._zzSky Cinema 2" ValueType="3" />
                    <Value Value="436................._zzSky Movies HD 1 DR" ValueType="3" />
                    <Value Value="437................._zzSM Premier 2 Cable Opt (SM3" ValueType="3" />
                    <Value Value="438................._zzSM Interactive 1" ValueType="3" />
                    <Value Value="439................._zzSM Interactive 2" ValueType="3" />
                    <Value Value="440................._zzSM Interactive 3" ValueType="3" />
                    <Value Value="441................._zzSM Interactive 4" ValueType="3" />
                    <Value Value="442................._zzSM Interactive 5" ValueType="3" />
                    <Value Value="443................._zzSM Interactive 6" ValueType="3" />
                    <Value Value="444................._zzSM Interactive 7" ValueType="3" />
                    <Value Value="445................._zzSM Interactive 8" ValueType="3" />
                    <Value Value="446.................Movies Active" ValueType="3" />
                    <Value Value="447................._zzMovies default errors" ValueType="3" />
                    <Value Value="448............Sky Box Office &amp; PPV Archive Channels" ValueType="3" />
                    <Value Value="449................._zzPPV Chnl 740 previously 743" ValueType="3" />
                    <Value Value="450................._zzPPV Chnl 741 previously 744" ValueType="3" />
                    <Value Value="451................._PPV Chnl 742 previously 745" ValueType="3" />
                    <Value Value="452................._PPV Chnl 743 previously 746" ValueType="3" />
                    <Value Value="453................._PPV Chnl Unavail was 745/738" ValueType="3" />
                    <Value Value="454................._zzPPV Chnl 745 previously 746/755" ValueType="3" />
                    <Value Value="455................._PPV Chnl unavailable was 747" ValueType="3" />
                    <Value Value="456................._PPV Chnl unavailable was 748" ValueType="3" />
                    <Value Value="457................._PPV Chnl 750 was 751/760" ValueType="3" />
                    <Value Value="458................._PPV Chnl unavailable was 751" ValueType="3" />
                    <Value Value="459................._PPV Chnl 751 was 752/761" ValueType="3" />
                    <Value Value="460................._PPV Chnl unavailable was 752" ValueType="3" />
                    <Value Value="461................._PPV Chnl unavailable was 753" ValueType="3" />
                    <Value Value="462................._PPV Chnl unavailable was 754" ValueType="3" />
                    <Value Value="463................._PPV Channel 62 Digital" ValueType="3" />
                    <Value Value="464................._PPV Channel 63 Digital" ValueType="3" />
                    <Value Value="465................._PPV Channel 64 Digital" ValueType="3" />
                    <Value Value="466................._PPV Channel 65 Digital" ValueType="3" />
                    <Value Value="467................._PPV Channel 66 Digital" ValueType="3" />
                    <Value Value="468................._PPV Channel 67 Digital" ValueType="3" />
                    <Value Value="469................._PPV Channel 68 Digital" ValueType="3" />
                    <Value Value="470................._PPV Chnl unavailable was unus" ValueType="3" />
                    <Value Value="471................._zzPay per View 1" ValueType="3" />
                    <Value Value="472................._zzPay per View 2" ValueType="3" />
                    <Value Value="473................._zzPay per View 3" ValueType="3" />
                    <Value Value="474................._zzPay per View 4" ValueType="3" />
                    <Value Value="475................._zzPay per View 5" ValueType="3" />
                    <Value Value="476................._zzSky Home Box Office" ValueType="3" />
                    <Value Value="477................._zz_SBO HD 3" ValueType="3" />
                    <Value Value="478................._zzPPV Freeview 771" ValueType="3" />
                    <Value Value="479................._zzPPV Widescreen" ValueType="3" />
                    <Value Value="480................._zzPPV Widescreen 1" ValueType="3" />
                    <Value Value="481................._zzPPV Widescreen 2" ValueType="3" />
                    <Value Value="482............Sky Sports Archive Channels" ValueType="3" />
                    <Value Value="483................._zzSky Sports News Freeview" ValueType="3" />
                    <Value Value="484.................Sky Sports 1 SD ROI DR" ValueType="3" />
                    <Value Value="485.................Sky Sports DR Channels" ValueType="3" />
                    <Value Value="486......................Sky Sports 1 DR channels" ValueType="3" />
                    <Value Value="487...........................zzSky Sports 1 SD DR" ValueType="3" />
                    <Value Value="488......................Sky Sports 2 DR channels" ValueType="3" />
                    <Value Value="489...........................zzSky Sports 2 SD DR" ValueType="3" />
                    <Value Value="490......................zzSky Sports 3 SD DR" ValueType="3" />
                    <Value Value="491......................Sky Sports 4 DR channels" ValueType="3" />
                    <Value Value="492...........................zzSky Sports 4 SD DR" ValueType="3" />
                    <Value Value="493......................zzSky Sports 5 Digital DR" ValueType="3" />
                    <Value Value="494.................Sky Sports 1 Pub DR" ValueType="3" />
                    <Value Value="495.................Sky Sports 2 SD ROI DR" ValueType="3" />
                    <Value Value="496.................Sky Sports 2 Pub Fixed DR" ValueType="3" />
                    <Value Value="497.................Sky Sports 4 Pub DR" ValueType="3" />
                    <Value Value="498.................Sky Sports F1 HD DR" ValueType="3" />
                    <Value Value="499................._zzzSky Sports News HD Comm" ValueType="3" />
                    <Value Value="500................._zzzSky Sports News Comm" ValueType="3" />
                    <Value Value="501.................Sky Sports Xtra (Sky Go)" ValueType="3" />
                    <Value Value="502.................Sky Sports 1 Channels" ValueType="3" />
                    <Value Value="503......................zzSky Sports 1 SD" ValueType="3" />
                    <Value Value="504......................zzSky Sports 1 SD COMM" ValueType="3" />
                    <Value Value="505......................zzSky Sports 1 SD ROI" ValueType="3" />
                    <Value Value="506......................zzSky Sports 1 HD" ValueType="3" />
                    <Value Value="507......................zzSky Sports 1 HD ROI" ValueType="3" />
                    <Value Value="508......................zzSky Sports 1 HD COMM" ValueType="3" />
                    <Value Value="509......................zzSky Sports 1 SD HOTEL" ValueType="3" />
                    <Value Value="510......................zzSky Sports 1 GO" ValueType="3" />
                    <Value Value="511......................zzSky Sports 1 SD PUB" ValueType="3" />
                    <Value Value="512......................zzSky Sports 1 HD PUB" ValueType="3" />
                    <Value Value="513......................zzSky Sports 1 Used for Contin" ValueType="3" />
                    <Value Value="514......................zzSky Sports 1 HD 3P" ValueType="3" />
                    <Value Value="515.................Sky Sports 5 Channels" ValueType="3" />
                    <Value Value="516......................zzSky Sports 5 SD" ValueType="3" />
                    <Value Value="517......................zzSky Sports 5 GO" ValueType="3" />
                    <Value Value="518......................zzSky Sports 5 HD" ValueType="3" />
                    <Value Value="519......................zzSky Sports 5 HD PUB" ValueType="3" />
                    <Value Value="520......................zzSky Sports 5 HD COMM" ValueType="3" />
                    <Value Value="521......................zzSky Sports 5 SD COMM" ValueType="3" />
                    <Value Value="522......................zzSky Sports 5 SD ROI" ValueType="3" />
                    <Value Value="523......................zzSky Sports 5 HD ROI" ValueType="3" />
                    <Value Value="524......................zzSky Sports 5 SD PUB" ValueType="3" />
                    <Value Value="525......................zzSky Sports 5 Used for Contin" ValueType="3" />
                    <Value Value="526.................Sky Sports 4 Channels" ValueType="3" />
                    <Value Value="527......................zzSky Sports 4 SD" ValueType="3" />
                    <Value Value="528......................zzSky Sports 4 SD COMM" ValueType="3" />
                    <Value Value="529......................zzSky Sports 4 HD" ValueType="3" />
                    <Value Value="530......................zzSky Sports 4 HD COMM" ValueType="3" />
                    <Value Value="531......................zzSky Sports 4 SD ROI" ValueType="3" />
                    <Value Value="532......................zzSky Sports 4 GO" ValueType="3" />
                    <Value Value="533......................zzSky Sports 4 SD HOTEL" ValueType="3" />
                    <Value Value="534......................zzSky Sports 4 SD PUB" ValueType="3" />
                    <Value Value="535......................zzSky Sports 4 HD PUB" ValueType="3" />
                    <Value Value="536......................zzSky Sports 4 Used for Contin" ValueType="3" />
                    <Value Value="537.................Sky Sports 3 Channels" ValueType="3" />
                    <Value Value="538......................zzSky Sports 3 SD" ValueType="3" />
                    <Value Value="539......................zzSky Sports 3 SD COMM" ValueType="3" />
                    <Value Value="540......................zzSky Sports 3 HD" ValueType="3" />
                    <Value Value="541......................zzSky Sports 3 HD COMM" ValueType="3" />
                    <Value Value="542......................zzSky Sports 3 SD HOTEL" ValueType="3" />
                    <Value Value="543......................zzSky Sports 3 SD ROI" ValueType="3" />
                    <Value Value="544......................zzSky Sports 3 GO" ValueType="3" />
                    <Value Value="545......................zzSky Sports 3 SD PUB" ValueType="3" />
                    <Value Value="546......................zzSky Sports 3 HD PUB" ValueType="3" />
                    <Value Value="547......................zzSky Sports 3 Used for Contin" ValueType="3" />
                    <Value Value="548.................Sky Sports 2 Channels" ValueType="3" />
                    <Value Value="549......................zzSky Sports 2 SD" ValueType="3" />
                    <Value Value="550......................zzSky Sports 2 SD ROI" ValueType="3" />
                    <Value Value="551......................zzSky Sports 2 HD" ValueType="3" />
                    <Value Value="552......................zzSky Sports 2 HD COMM" ValueType="3" />
                    <Value Value="553......................zzSky Sports 2 SD COMM" ValueType="3" />
                    <Value Value="554......................zzSky Sports 2 SD HOTEL" ValueType="3" />
                    <Value Value="555......................zzSky Sports 2 GO" ValueType="3" />
                    <Value Value="556......................zzSky Sports 2 SD PUB" ValueType="3" />
                    <Value Value="557......................zzSky Sports 2 HD PUB" ValueType="3" />
                    <Value Value="558......................zzSky Sports 2 Used for Contin" ValueType="3" />
                    <Value Value="559.................Sky Sports News DR" ValueType="3" />
                    <Value Value="560.................Sky Sports News HD DR" ValueType="3" />
                    <Value Value="561.................Sky Sports 2 HD Pub Fixed" ValueType="3" />
                    <Value Value="562.................Sky Sports 2 Pub Fixed" ValueType="3" />
                    <Value Value="563................._zzPrem+ HD" ValueType="3" />
                    <Value Value="564................._zzPremiership + Cable" ValueType="3" />
                    <Value Value="565................._zzPremiership + Commercial" ValueType="3" />
                    <Value Value="566................._zzPremiership + DTH" ValueType="3" />
                    <Value Value="567................._zzPremiership + Interactive H" ValueType="3" />
                    <Value Value="568................._zzPremiership + Interactive L" ValueType="3" />
                    <Value Value="569................._zzSky Sport 1 Pub - Ryder" ValueType="3" />
                    <Value Value="570................._zzF1 Digital +" ValueType="3" />
                    <Value Value="571................._zzF1 Digital + Playback" ValueType="3" />
                    <Value Value="572................._zzF1 Digital + Record" ValueType="3" />
                    <Value Value="573................._zzCricket Highlights" ValueType="3" />
                    <Value Value="574................._zzSky Bet Live" ValueType="3" />
                    <Value Value="575................._zzSky Sports 1 Freeview" ValueType="3" />
                    <Value Value="576................._zzSky Sports 1 Picnic" ValueType="3" />
                    <Value Value="577................._zzSky Sports 2" ValueType="3" />
                    <Value Value="578................._zzSky Sports 2 Freeview" ValueType="3" />
                    <Value Value="579................._zzSky Sports 2 Pub" ValueType="3" />
                    <Value Value="580................._zzSky Sports Club" ValueType="3" />
                    <Value Value="581................._zzSky Sports Gold" ValueType="3" />
                    <Value Value="582................._zzSky Sports HD - 3rd Event" ValueType="3" />
                    <Value Value="583................._zzSky Sports News Comm" ValueType="3" />
                    <Value Value="584................._zzSky Sports News HD Comm" ValueType="3" />
                    <Value Value="585................._zzSky Sports News HD CommTest" ValueType="3" />
                    <Value Value="586................._zzSky Sports News Pub" ValueType="3" />
                    <Value Value="587............Sky Entertainment Archive Channels" ValueType="3" />
                    <Value Value="588................._zzSky Living Loves" ValueType="3" />
                    <Value Value="589.................Sky 1 Ghost (ROI)" ValueType="3" />
                    <Value Value="590................._zzSky One Freeview" ValueType="3" />
                    <Value Value="591................._zzSky One Interactive 1" ValueType="3" />
                    <Value Value="592................._zzSky One Interactive 2" ValueType="3" />
                    <Value Value="593................._zzSky One Interactive 3" ValueType="3" />
                    <Value Value="594................._zzSky One" ValueType="3" />
                    <Value Value="595.................Sky 1 SD 3P" ValueType="3" />
                    <Value Value="596.................Sky 1 + 1 Schedule" ValueType="3" />
                    <Value Value="597................._zzSky Arts" ValueType="3" />
                    <Value Value="598................._zzSky Arts + 1" ValueType="3" />
                    <Value Value="599................._zzSky Arts 2 HD (temp)" ValueType="3" />
                    <Value Value="600................._zzSky Arts HD" ValueType="3" />
                    <Value Value="601................._zzSky Arts HD (old)" ValueType="3" />
                    <Value Value="602................._zzSky Real Lives" ValueType="3" />
                    <Value Value="603................._zzSky Real Lives +1" ValueType="3" />
                    <Value Value="604................._zzSky Real Lives HD" ValueType="3" />
                    <Value Value="605................._zzSky Real Lives 2" ValueType="3" />
                    <Value Value="606................._zzSky Pick" ValueType="3" />
                    <Value Value="607................._zzSky Travel" ValueType="3" />
                    <Value Value="608................._zzSky Travel (Old)" ValueType="3" />
                    <Value Value="609................._zzSky Travel Freeview" ValueType="3" />
                    <Value Value="610............Sky News Archive Channels" ValueType="3" />
                    <Value Value="611................._zzSky News" ValueType="3" />
                    <Value Value="612................._zzSky News Active" ValueType="3" />
                    <Value Value="613................._zzSky News Digital +1" ValueType="3" />
                    <Value Value="614............Sky Commercial Archive Channels" ValueType="3" />
                    <Value Value="615................._zzSky Bet" ValueType="3" />
                    <Value Value="616............Sky Barker/Promo Archive Channels" ValueType="3" />
                    <Value Value="617................._zzSky Customer Chnl (Active)" ValueType="3" />
                    <Value Value="618.......Non Branded Archive" ValueType="3" />
                    <Value Value="619............Entertainment Archive Channels" ValueType="3" />
                    <Value Value="620................._zzPick TV Freeview + 1" ValueType="3" />
                    <Value Value="621.................Channel 1" ValueType="3" />
                    <Value Value="622.................Bravo" ValueType="3" />
                    <Value Value="623.................Challenge DTT" ValueType="3" />
                    <Value Value="624................._zzArtsworld HD temp" ValueType="3" />
                    <Value Value="625................._zzArtsworld SD Temp" ValueType="3" />
                    <Value Value="626................._zzDot TV" ValueType="3" />
                    <Value Value="627................._zzDot TV Digital" ValueType="3" />
                    <Value Value="628................._zzMusic 1 - The Amp" ValueType="3" />
                    <Value Value="629................._zzMusic 2 - Scuzz" ValueType="3" />
                    <Value Value="630................._zzMusic 3 - Flaunt" ValueType="3" />
                    <Value Value="631................._zzMusicXclusive" ValueType="3" />
                    <Value Value="632............3rd Party Archive Channels (managed by Sky)" ValueType="3" />
                    <Value Value="633................._zz_MGM HD" ValueType="3" />
                    <Value Value="634................._zzMUTV" ValueType="3" />
                    <Value Value="635................._zzUKTV1" ValueType="3" />
                    <Value Value="636............Joint Venture Archive Channels" ValueType="3" />
                    <Value Value="637................._zzBiography" ValueType="3" />
                    <Value Value="638................._zzBiography HD" ValueType="3" />
                    <Value Value="639................._zzBiography +1" ValueType="3" />
                    <Value Value="640................._zzFox Kids" ValueType="3" />
                    <Value Value="641................._zzFox Kids UK Digital" ValueType="3" />
                    <Value Value="642................._zzGranada Good Life" ValueType="3" />
                    <Value Value="643................._zzGranada Men &amp; Motors" ValueType="3" />
                    <Value Value="644................._zzGranada Plus" ValueType="3" />
                    <Value Value="645................._zzHistory Middle East" ValueType="3" />
                    <Value Value="646................._zzThe History Channel" ValueType="3" />
                    <Value Value="647................._zzNational Geog Poland Digita" ValueType="3" />
                    <Value Value="648................._zzNGC A One" ValueType="3" />
                    <Value Value="649................._zzNGC Ben" ValueType="3" />
                    <Value Value="650................._zzNGC Benelux HD" ValueType="3" />
                    <Value Value="651................._zzNGC Central Europe" ValueType="3" />
                    <Value Value="652................._zzNGC France" ValueType="3" />
                    <Value Value="653................._zzNGC France Playback" ValueType="3" />
                    <Value Value="654................._zzNGC France Record" ValueType="3" />
                    <Value Value="655................._zzNGC HD (slave)" ValueType="3" />
                    <Value Value="656................._zzNGC Israel" ValueType="3" />
                    <Value Value="657................._zzNGC Italy" ValueType="3" />
                    <Value Value="658................._zzNGC Oz" ValueType="3" />
                    <Value Value="659................._zzNGC Pol" ValueType="3" />
                    <Value Value="660................._zzNGC Poland HD" ValueType="3" />
                    <Value Value="661................._zzNGC Romania" ValueType="3" />
                    <Value Value="662................._zzNGC SA" ValueType="3" />
                    <Value Value="663................._zzNGC Scan" ValueType="3" />
                    <Value Value="664................._zzNGC UK" ValueType="3" />
                    <Value Value="665................._zzNickelodeon Jr.Digital" ValueType="3" />
                    <Value Value="666............Adult Archive Channels" ValueType="3" />
                    <Value Value="667.................zz18+1 Hot Babes" ValueType="3" />
                    <Value Value="668.................zz18+2 Swingers" ValueType="3" />
                    <Value Value="669.................zz18+3 Fetish" ValueType="3" />
                    <Value Value="670.................zz18+4 Wives" ValueType="3" />
                    <Value Value="671.................zz18+5 Mature" ValueType="3" />
                    <Value Value="672.................zz18+6 Sex Ed" ValueType="3" />
                    <Value Value="673.................zz18+7 Extra" ValueType="3" />
                    <Value Value="674................._zzPPV 18+Movies 1 (old)" ValueType="3" />
                    <Value Value="675.................zzPPV 18+Movies 2 (old)" ValueType="3" />
                    <Value Value="676................._zzAdult Nightly 1(Hustler TV" ValueType="3" />
                    <Value Value="677................._zzAdult Nightly 4 (100% Babes" ValueType="3" />
                    <Value Value="678................._zzAdult Nightly 5 (Girls G/ Wi" ValueType="3" />
                    <Value Value="679................._zzAdult Nightly 6 (Amateur Bab" ValueType="3" />
                    <Value Value="680................._zzAdult Nightly 8(SportxxxBab" ValueType="3" />
                    <Value Value="681................._zzAdult Nightly 9 (SportxxxGi" ValueType="3" />
                    <Value Value="682................._zzAdult Nightly10 (Sportxxxw" ValueType="3" />
                    <Value Value="683............Archive Channels other" ValueType="3" />
                    <Value Value="684................._zzSky Z" ValueType="3" />
                    <Value Value="685................._zzMedical Channel" ValueType="3" />
                    <Value Value="686................._zzCompetitive Channel" ValueType="3" />
                    <Value Value="687................._zzSky Unite" ValueType="3" />
                    <Value Value="688................._zzSky Venue" ValueType="3" />
                    <Value Value="689................._zzAutomotive Channel" ValueType="3" />
                  </Column>
                </Parameter>
              </DataStructure>
            </DataStructures>
            <Contents>
              <Content ItemID="1" ItemIndex="1" ContentID="1" ContentName="Header" ContentTitle="">
                <HeaderInfo ShowDate="True" ShowTime="True" ShowTitle="True" ShowDescription="False" Title="Media Rights Details Report" Description="" />
              </Content>
              <Content ItemID="2" ItemIndex="2" ContentID="2" ContentName="Table" ContentTitle="">
                <Data DataStructureID="1" />
                <DataTable Caption="" ShowRecordCount="False" ShowRowNumber="False" UseInteractivePaging="True" UseNumberedList="False" UseAppendRowsPaging="False" PagingLocation="0" InteractivePagingRows="999" InteractivePagingSubRows="50" SummaryRowLocationBottom="False" ShowSummaryRowOnce="True" TableWidth="-1" TableWidthScale="px">
                  <Column ColumnType="1" ColumnID="1045" ObjectKey="16_66_13_1_67_67_0" Header="Title Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="357" ObjectKey="16_66_13_1_64_22_0" Header="Media Type" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="361" ObjectKey="16_66_13_1_64_22_0" Header="Language" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="358" ObjectKey="16_66_13_1_64_22_0" Header="Venue" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="359" ObjectKey="16_66_13_1_64_22_0" Header="Delivery Means" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="379" ObjectKey="16_66_13_1_64_22_0" Header="Product Offerings" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="363" ObjectKey="16_66_13_1_64_22_0" Header="Channels" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="364" ObjectKey="16_66_13_1_64_22_0" Header="Territory" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="365" ObjectKey="16_66_13_1_64_22_0" Header="Term From" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="366" ObjectKey="16_66_13_1_64_22_0" Header="Term To" Format="Short Date" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="367" ObjectKey="16_66_13_1_64_22_0" Header="Exclusivity" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="371" ObjectKey="16_66_13_1_64_22_0" Header="Restriction" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="2688" ObjectKey="16_66_13_1_64_22_0" Header="Notes Restricted" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="2689" ObjectKey="16_66_13_1_64_22_0" Header="Notes Unrestricted" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="196" ObjectKey="16" Header="Deal ID" Format="General Number" Alignment="Right" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                  <Column ColumnType="1" ColumnID="198" ObjectKey="16" Header="Deal Name" Format="" Alignment="Left" HasSort="False" IsLinked="False" Width="-1" Scale="" VisualizationType="None" ShowData="1" />
                </DataTable>
                <XGroupBy GroupType="0" PageBreakAfterGroup="False" FlatGroupingRollUpDetails="False" ShowExpandCollapse="False" AutoExpandDrillDown="False" />
              </Content>
              <Content ItemID="3" ItemIndex="3" ContentID="11" ContentName="Exports" ContentTitle="">
                <Exports LinkSearchable="False" LinkPrintable="False" LinkExcel="True" LinkWord="False" LinkPPT="False" LinkPdf="True" LinkCSV="False" LinkXML="False" LinkArchive="False" SendMail="True" SendMailExportType="EXCEL" />
              </Content>
            </Contents>
            <SessionParameter />
            <General Template="Custom-Blue" PrintablePageOrientation="Portrait" ShowPrintablePageNr="ON" PrintablePageType="Letter" />
          </AdhocMetadata>
        </Definition>
        <Dependencies>
          <Dependency Type="Object" ID="67" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ASSETS">
            <Column ID="1045" ColumnName="Asset Title" />
          </Dependency>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="363" ColumnName="Business Outlets" />
            <Column ID="379" ColumnName="End User Rights" />
            <Column ID="367" ColumnName="Exclusivity" />
            <Column ID="361" ColumnName="Language" />
            <Column ID="357" ColumnName="Media Type" />
            <Column ID="2688" ColumnName="Notes Restricted" />
            <Column ID="2689" ColumnName="Notes Unrestricted" />
            <Column ID="359" ColumnName="Packaging" />
            <Column ID="371" ColumnName="Restriction" />
            <Column ID="365" ColumnName="Term From" />
            <Column ID="366" ColumnName="Term To" />
            <Column ID="364" ColumnName="Territory" />
            <Column ID="358" ColumnName="Venue" />
          </Dependency>
          <Dependency Type="Object" ID="92" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_BUS_OUTLET">
            <Column ID="1652" ColumnName="Business Outlet" />
          </Dependency>
          <Dependency Type="Object" ID="138" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_ENDUSR_RIGHTS">
            <Column ID="2468" ColumnName="Enduser Rights" />
          </Dependency>
          <Dependency Type="Object" ID="140" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_MEDIA_RIGHTS">
            <Column ID="2474" ColumnName="Media Rights" />
          </Dependency>
          <Dependency Type="Object" ID="16" ObjectSchema="" ObjectName="VV_RB_Deals">
            <Column ID="196" ColumnName="Deal ID" />
            <Column ID="198" ColumnName="Deal Name" />
          </Dependency>
          <Dependency Type="Relation" ID="67" RelationName="Deal Navigator -&gt; Assets" Relation="Inner Join" ObjectID1="13" ObjectID2="67" />
          <Dependency Type="Relation" ID="66" RelationName="Deal Navigator -&gt; Deals" Relation="Inner Join" ObjectID1="13" ObjectID2="16" />
          <Dependency Type="Relation" ID="64" RelationName="Deal Navigator -&gt; Media Rights" Relation="Left Outer Join" ObjectID1="13" ObjectID2="22" />
          <Dependency Type="Relation" ID="146" RelationName="Media Rights -&gt; Channel Tree" Relation="Left Outer Join" ObjectID1="22" ObjectID2="92" />
          <Dependency Type="Relation" ID="145" RelationName="Media Rights -&gt; Enduser Rights Tree" Relation="Left Outer Join" ObjectID1="22" ObjectID2="138" />
          <Dependency Type="Relation" ID="139" RelationName="Media Rights -&gt; Media Rights Tree" Relation="Left Outer Join" ObjectID1="22" ObjectID2="140" />
          <Dependency Type="DataFormat" ID="1" FormatName="General Number" Format="General Number" />
          <Dependency Type="DataFormat" ID="13" FormatName="Short Date" Format="Short Date" />
          <Dependency Type="SessionParameter" ID="1" ParameterName="spUserId" />
        </Dependencies>
      </Report>
    </Task>
    <Task Type="Object">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" IncludeLinks="False" IncludeAccessRights="False" PreserveCategories="1" DeleteUnmatched="False" />
      <Object ElementID="67" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ASSETS" Description="Titles" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="1090" ColumnName="Airing Order" Description="Airing Order" ColumnType="D" OrdinalPosition="48" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1676" ColumnName="Asset SubType" Description="Title Sub Type" ColumnType="D" OrdinalPosition="22" ColumnOrder="2" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1675" ColumnName="Asset Type Concat" Description="Title Type Concat" ColumnType="D" OrdinalPosition="3" ColumnOrder="3" DataType="200" CharacterMaxLen="513" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1042" ColumnName="ASSET_PK" Description="ASSET_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="4" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1060" ColumnName="MPAA Rating" Description="Certification" ColumnType="D" OrdinalPosition="18" ColumnOrder="5" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1072" ColumnName="Close Captioned" Description="Close Captioned" ColumnType="D" OrdinalPosition="31" ColumnOrder="6" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1092" ColumnName="Contributors" Description="Contributors" ColumnType="D" OrdinalPosition="49" ColumnOrder="7" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1686" ColumnName="Contributors Flat" Description="Contributors Flat" ColumnType="D" OrdinalPosition="55" ColumnOrder="8" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1078" ColumnName="Copyright Year" Description="Copyright Year" ColumnType="D" OrdinalPosition="37" ColumnOrder="9" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1091" ColumnName="Created By" Description="Created By" ColumnType="D" OrdinalPosition="49" ColumnOrder="10" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1093" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="50" ColumnOrder="11" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1079" ColumnName="Credits In (hh:mm:ss:tt)" Description="Credits In (hh:mm:ss:tt)" ColumnType="D" OrdinalPosition="38" ColumnOrder="12" DataType="200" CharacterMaxLen="48" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Time" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2771" ColumnName="Seasons" Description="Seasons" ColumnType="D" OrdinalPosition="12" ColumnOrder="13" DataType="200" CharacterMaxLen="457" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1080" ColumnName="Credits Out (hh:mm:ss:tt)" Description="Credits Out (hh:mm:ss:tt)" ColumnType="D" OrdinalPosition="39" ColumnOrder="13" DataType="200" CharacterMaxLen="48" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Time" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1063" ColumnName="Delivery Format" Description="Delivery Format" ColumnType="D" OrdinalPosition="22" ColumnOrder="15" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1081" ColumnName="Disclaimer" Description="Disclaimer" ColumnType="D" OrdinalPosition="40" ColumnOrder="16" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1073" ColumnName="Dubbed" Description="Dubbed" ColumnType="D" OrdinalPosition="32" ColumnOrder="17" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1052" ColumnName="Duration (hh:mm:ss)" Description="Duration (hh:mm:ss)" ColumnType="D" OrdinalPosition="10" ColumnOrder="18" DataType="200" CharacterMaxLen="8" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1068" ColumnName="Duration (Secs)" Description="Duration (Secs)" ColumnType="D" OrdinalPosition="27" ColumnOrder="19" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1053" ColumnName="Short Name" Description="EPG Name" ColumnType="D" OrdinalPosition="11" ColumnOrder="20" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1067" ColumnName="Asset Description" Description="Episode Notes" ColumnType="D" OrdinalPosition="26" ColumnOrder="21" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1069" ColumnName="Episode Number" Description="Episode Number" ColumnType="D" OrdinalPosition="28" ColumnOrder="22" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1680" ColumnName="Genre" Description="EPG Genre Flat" ColumnType="D" OrdinalPosition="59" ColumnOrder="23" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1688" ColumnName="Genre Flat" Description="EPG Sub Genre" ColumnType="D" OrdinalPosition="60" ColumnOrder="24" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1083" ColumnName="Guild Association" Description="Guild Association" ColumnType="D" OrdinalPosition="42" ColumnOrder="25" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1065" ColumnName="HD Percentage" Description="HD Percentage" ColumnType="D" OrdinalPosition="24" ColumnOrder="26" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1066" ColumnName="HD Type" Description="HD Type" ColumnType="D" OrdinalPosition="25" ColumnOrder="27" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1055" ColumnName="Inactive Reason" Description="Inactive Reason" ColumnType="D" OrdinalPosition="13" ColumnOrder="28" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1057" ColumnName="Internal" Description="Internal" ColumnType="D" OrdinalPosition="15" ColumnOrder="29" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1046" ColumnName="IP Name" Description="IP Name" ColumnType="D" OrdinalPosition="4" ColumnOrder="30" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1679" ColumnName="Keyword" Description="Keyword" ColumnType="D" OrdinalPosition="58" ColumnOrder="31" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1071" ColumnName="Language" Description="Language" ColumnType="D" OrdinalPosition="30" ColumnOrder="32" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1689" ColumnName="Library Title" Description="Library Title" ColumnType="D" OrdinalPosition="73" ColumnOrder="33" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1064" ColumnName="Master Audio" Description="Master Audio" ColumnType="D" OrdinalPosition="23" ColumnOrder="34" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1690" ColumnName="Movie ID" Description="Movie ID" ColumnType="D" OrdinalPosition="7" ColumnOrder="35" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1077" ColumnName="Nielsen No" Description="Nielsen No" ColumnType="D" OrdinalPosition="36" ColumnOrder="36" DataType="202" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1076" ColumnName="# of Episodes" Description="No of Episodes" ColumnType="D" OrdinalPosition="35" ColumnOrder="37" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1058" ColumnName="Non-Airable" Description="Non-Airable" ColumnType="D" OrdinalPosition="16" ColumnOrder="38" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1691" ColumnName="Notes Concat" Description="Notes Concat" ColumnType="D" OrdinalPosition="61" ColumnOrder="39" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1692" ColumnName="PPS Usage Report" Description="PPS Usage Report" ColumnType="D" OrdinalPosition="20" ColumnOrder="40" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1084" ColumnName="Production No" Description="Production No" ColumnType="D" OrdinalPosition="43" ColumnOrder="41" DataType="202" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1047" ColumnName="Project ID" Description="Project ID" ColumnType="D" OrdinalPosition="5" ColumnOrder="42" DataType="200" CharacterMaxLen="200" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1075" ColumnName="Rating Warning" Description="Rating Warning" ColumnType="D" OrdinalPosition="34" ColumnOrder="43" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1695" ColumnName="Restricted Notes" Description="Restricted Notes" ColumnType="D" OrdinalPosition="30" ColumnOrder="44" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1087" ColumnName="Season Closer" Description="Season Closer" ColumnType="D" OrdinalPosition="45" ColumnOrder="45" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1086" ColumnName="Season Opener" Description="Season Opener" ColumnType="D" OrdinalPosition="44" ColumnOrder="46" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1089" ColumnName="Series Closer" Description="Series Closer" ColumnType="D" OrdinalPosition="47" ColumnOrder="47" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1697" ColumnName="Season" Description="Season" ColumnType="D" OrdinalPosition="69" ColumnOrder="48" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1698" ColumnName="Series ID" Description="Series ID" ColumnType="D" OrdinalPosition="10" ColumnOrder="48" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1088" ColumnName="Series Opener" Description="Series Opener" ColumnType="D" OrdinalPosition="46" ColumnOrder="49" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1048" ColumnName="Series Title" Description="Series Title" ColumnType="D" OrdinalPosition="6" ColumnOrder="50" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1062" ColumnName="Shooting Format" Description="Shooting Format" ColumnType="D" OrdinalPosition="21" ColumnOrder="51" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1677" ColumnName="Source Asset Identifier" Description="Source ID" ColumnType="D" OrdinalPosition="47" ColumnOrder="52" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1051" ColumnName="Source Business Unit" Description="Source Business Unit" ColumnType="D" OrdinalPosition="9" ColumnOrder="53" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1699" ColumnName="Source ID" Description="Source ID" ColumnType="D" OrdinalPosition="8" ColumnOrder="54" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1085" ColumnName="Source System" Description="Source System" ColumnType="D" OrdinalPosition="43" ColumnOrder="55" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1074" ColumnName="Subtitled" Description="Subtitled" ColumnType="D" OrdinalPosition="33" ColumnOrder="56" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1700" ColumnName="Supplier Air Date" Description="Supplier Air Date" ColumnType="D" OrdinalPosition="72" ColumnOrder="57" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1082" ColumnName="Syndication #" Description="Syndication #" ColumnType="D" OrdinalPosition="41" ColumnOrder="58" DataType="200" CharacterMaxLen="40" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1678" ColumnName="Synopsis" Description="Synopsis" ColumnType="D" OrdinalPosition="57" ColumnOrder="59" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1701" ColumnName="Synopsis Concat" Description="Synopsis Concat" ColumnType="D" OrdinalPosition="66" ColumnOrder="60" DataType="200" CharacterMaxLen="7600" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1681" ColumnName="TBA" Description="TBA" ColumnType="D" OrdinalPosition="67" ColumnOrder="61" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1061" ColumnName="Asset Format" Description="Title Category" ColumnType="D" OrdinalPosition="20" ColumnOrder="62" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1702" ColumnName="Asset Hierarchy" Description="Title Hierarchy" ColumnType="D" OrdinalPosition="70" ColumnOrder="63" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1043" ColumnName="Asset ID" Description="Title ID" ColumnType="D" OrdinalPosition="1" ColumnOrder="64" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1045" ColumnName="Asset Title" Description="Title Name" ColumnType="D" OrdinalPosition="3" ColumnOrder="65" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1056" ColumnName="Asset Source" Description="Title Source" ColumnType="D" OrdinalPosition="14" ColumnOrder="66" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1054" ColumnName="Asset Status" Description="Title Status" ColumnType="D" OrdinalPosition="12" ColumnOrder="67" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1044" ColumnName="Asset Type" Description="Title Type" ColumnType="D" OrdinalPosition="2" ColumnOrder="68" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1094" ColumnName="Updated By" Description="Updated By" ColumnType="D" OrdinalPosition="51" ColumnOrder="69" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1095" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="52" ColumnOrder="70" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1049" ColumnName="Vendor" Description="Vendor" ColumnType="D" OrdinalPosition="7" ColumnOrder="71" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1050" ColumnName="Vendor 2" Description="Vendor 2" ColumnType="D" OrdinalPosition="8" ColumnOrder="72" DataType="200" CharacterMaxLen="500" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1070" ColumnName="Release Year" Description="Year of Production" ColumnType="D" OrdinalPosition="29" ColumnOrder="73" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2772" ColumnName="Highlight" Description="Highlight" ColumnType="D" OrdinalPosition="135" ColumnOrder="136" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="3" CategoryName="Title Maintenance" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS" Description="Media Rights" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="351" ColumnName="DEAL_PK" Description="DEAL_PK" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="352" ColumnName="ASSET_PK" Description="ASSET_PK" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1749" ColumnName="MEDIA_RIGHT_TM_PK" Description="MEDIA_RIGHT_TM_PK" ColumnType="D" OrdinalPosition="3" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="355" ColumnName="DEAL_ASSET_DETAIL_PK" Description="DEAL_ASSET_DETAIL_PK" ColumnType="D" OrdinalPosition="2" ColumnOrder="4" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2687" ColumnName="BTTERM_GROUP_PK" Description="BTTERM_GROUP_PK" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1750" ColumnName="START_TERM_EVENT_PK" Description="START_TERM_EVENT_PK" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="392" ColumnName="Row No" Description="Row No" ColumnType="D" OrdinalPosition="6" ColumnOrder="6" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="354" ColumnName="Applies To" Description="Applies To" ColumnType="D" OrdinalPosition="7" ColumnOrder="7" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="353" ColumnName="Ext. Carve-Out" Description="Ext  Carve- Out" ColumnType="D" OrdinalPosition="8" ColumnOrder="8" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="356" ColumnName="Party" Description="Party" ColumnType="D" OrdinalPosition="9" ColumnOrder="9" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="357" ColumnName="Media Type" Description="Media Type" ColumnType="D" OrdinalPosition="10" ColumnOrder="10" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1682" ColumnName="Media Type Tree" Description="Media Type Tree" ColumnType="D" OrdinalPosition="11" ColumnOrder="11" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="358" ColumnName="Venue" Description="Venue" ColumnType="D" OrdinalPosition="12" ColumnOrder="12" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="359" ColumnName="Packaging" Description="Delivery Means" ColumnType="D" OrdinalPosition="13" ColumnOrder="13" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="360" ColumnName="Touchpoints" Description="Viewing Devices" ColumnType="D" OrdinalPosition="14" ColumnOrder="14" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="363" ColumnName="Business Outlets" Description="Channels" ColumnType="D" OrdinalPosition="16" ColumnOrder="15" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="364" ColumnName="Territory" Description="Territory" ColumnType="D" OrdinalPosition="17" ColumnOrder="16" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1753" ColumnName="Language Template" Description="Language Template" ColumnType="D" OrdinalPosition="18" ColumnOrder="17" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="361" ColumnName="Language" Description="Language" ColumnType="D" OrdinalPosition="19" ColumnOrder="18" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="362" ColumnName="Perpetuity" Description="Perpetuity" ColumnType="D" OrdinalPosition="20" ColumnOrder="19" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1754" ColumnName="Perpetuity Flag" Description="Perpetuity Flag" ColumnType="D" OrdinalPosition="21" ColumnOrder="20" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="365" ColumnName="Term From" Description="Term From" ColumnType="D" OrdinalPosition="22" ColumnOrder="21" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="368" ColumnName="Estimated Term From" Description="Estimated Term From" ColumnType="D" OrdinalPosition="23" ColumnOrder="22" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="366" ColumnName="Term To" Description="Term To" ColumnType="D" OrdinalPosition="24" ColumnOrder="23" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="374" ColumnName="Estimated Term To" Description="Estimated Term To" ColumnType="D" OrdinalPosition="25" ColumnOrder="24" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="367" ColumnName="Exclusivity" Description="Exclusivity" ColumnType="D" OrdinalPosition="26" ColumnOrder="25" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1756" ColumnName="Holdback" Description="Holdback" ColumnType="D" OrdinalPosition="27" ColumnOrder="26" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="369" ColumnName="Type" Description="Type" ColumnType="D" OrdinalPosition="28" ColumnOrder="27" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="371" ColumnName="Restriction" Description="Restriction" ColumnType="D" OrdinalPosition="29" ColumnOrder="28" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="370" ColumnName="Sublicense" Description="Sublicense" ColumnType="D" OrdinalPosition="30" ColumnOrder="29" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="378" ColumnName="Notes" Description="Notes" ColumnType="D" OrdinalPosition="31" ColumnOrder="30" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="375" ColumnName="Restricted Notes" Description="Restricted Notes" ColumnType="D" OrdinalPosition="32" ColumnOrder="31" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="379" ColumnName="End User Rights" Description="Product Offerings" ColumnType="D" OrdinalPosition="33" ColumnOrder="32" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2688" ColumnName="Notes Restricted" Description="Notes Restricted" ColumnType="D" OrdinalPosition="33" ColumnOrder="33" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="372" ColumnName="Blackout" Description="Blackout" ColumnType="D" OrdinalPosition="34" ColumnOrder="34" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2689" ColumnName="Notes Unrestricted" Description="Notes Unrestricted" ColumnType="D" OrdinalPosition="34" ColumnOrder="35" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="376" ColumnName="Customer" Description="Customer" ColumnType="D" OrdinalPosition="35" ColumnOrder="36" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="373" ColumnName="Publications" Description="Publications" ColumnType="D" OrdinalPosition="36" ColumnOrder="37" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="377" ColumnName="Min Value" Description="Min Value" ColumnType="D" OrdinalPosition="37" ColumnOrder="38" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="380" ColumnName="Max Value" Description="Max Value" ColumnType="D" OrdinalPosition="38" ColumnOrder="39" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1758" ColumnName="ID_MEDIA_RIGHT_TM" Description="ID_MEDIA_RIGHT_TM" ColumnType="D" OrdinalPosition="40" ColumnOrder="40" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="381" ColumnName="Units" Description="Units" ColumnType="D" OrdinalPosition="39" ColumnOrder="41" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="382" ColumnName="Minutes" Description="Minutes" ColumnType="D" OrdinalPosition="40" ColumnOrder="42" DataType="200" CharacterMaxLen="300" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1759" ColumnName="ID_TERRITORY_TM" Description="ID_TERRITORY_TM" ColumnType="D" OrdinalPosition="41" ColumnOrder="43" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="384" ColumnName="Refresh Percent" Description="Refresh Percent" ColumnType="D" OrdinalPosition="41" ColumnOrder="44" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="386" ColumnName="Quantity" Description="Quantity" ColumnType="D" OrdinalPosition="42" ColumnOrder="45" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1760" ColumnName="ID_PKGING_RIGHTS_TM" Description="ID_PKGING_RIGHTS_TM" ColumnType="D" OrdinalPosition="42" ColumnOrder="46" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1761" ColumnName="ID_LANGUAGE_TM" Description="ID_LANGUAGE_TM" ColumnType="D" OrdinalPosition="43" ColumnOrder="47" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="391" ColumnName="Usage Period" Description="Usage Period" ColumnType="D" OrdinalPosition="45" ColumnOrder="48" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="388" ColumnName="Usage Logical Operator" Description="Usage Logical Operator" ColumnType="D" OrdinalPosition="43" ColumnOrder="49" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="390" ColumnName="Usage Time Period" Description="Usage Time Period" ColumnType="D" OrdinalPosition="44" ColumnOrder="50" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1762" ColumnName="ID_VENUE_TM" Description="ID_VENUE_TM" ColumnType="D" OrdinalPosition="46" ColumnOrder="51" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1763" ColumnName="ID_TOUCHPOINT_TM" Description="ID_TOUCHPOINT_TM" ColumnType="D" OrdinalPosition="44" ColumnOrder="52" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1764" ColumnName="ID_ENDUSER_RIGHTS_TM" Description="ID_ENDUSER_RIGHTS_TM" ColumnType="D" OrdinalPosition="45" ColumnOrder="53" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1765" ColumnName="ID_BUSINESS_OUTLET_TM" Description="ID_BUSINESS_OUTLET_TM" ColumnType="D" OrdinalPosition="47" ColumnOrder="54" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="383" ColumnName="Updated By" Description="Updated By" ColumnType="D" OrdinalPosition="54" ColumnOrder="55" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="385" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="55" ColumnOrder="56" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="387" ColumnName="Created By" Description="Created By" ColumnType="D" OrdinalPosition="56" ColumnOrder="57" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="389" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="57" ColumnOrder="58" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="6" CategoryName="Rights In" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="92" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_BUS_OUTLET" Description="Channels Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="1652" ColumnName="Business Outlet" Description="Channels" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1654" ColumnName="ID_BUSINESS_OUTLET_TM" Description="ID_BUSINESS_OUTLET_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1653" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2506" ColumnName="Parent Level 2" Description="Parent Level 2" ColumnType="D" OrdinalPosition="2" ColumnOrder="4" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2507" ColumnName="Parent Level 3" Description="Parent Level 3" ColumnType="D" OrdinalPosition="3" ColumnOrder="5" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2508" ColumnName="Business Outlet Flat" Description="Channels Flat" ColumnType="D" OrdinalPosition="4" ColumnOrder="6" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2509" ColumnName="ID_BUSINESS_OUTLET_ITEM" Description="ID_BUSINESS_OUTLET_ITEM" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="138" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_ENDUSR_RIGHTS" Description="Product Offering Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2468" ColumnName="Enduser Rights" Description="Product Offerings" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2469" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2470" ColumnName="ID_ENDUSER_RIGHTS_TM" Description="ID_ENDUSER_RIGHTS_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2514" ColumnName="Parent Level 2" Description="Parent Level 2" ColumnType="D" OrdinalPosition="2" ColumnOrder="4" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2515" ColumnName="Parent Level 3" Description="Parent Level 3" ColumnType="D" OrdinalPosition="3" ColumnOrder="5" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2516" ColumnName="Enduser Rights Flat" Description="Product Offerings Flat" ColumnType="D" OrdinalPosition="4" ColumnOrder="6" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2517" ColumnName="ID_ENDUSER_RIGHTS_ITEM" Description="ID_ENDUSER_RIGHTS_ITEM" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="140" ObjectSchema="RLANLTCS" ObjectName="MV_RB_ITEMS_MEDIA_RIGHTS" Description="Media Rights Tree" Type="U" Definition="" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="2474" ColumnName="Media Rights" Description="Media Rights" ColumnType="D" OrdinalPosition="0" ColumnOrder="1" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2475" ColumnName="Parent" Description="Parent" ColumnType="D" OrdinalPosition="1" ColumnOrder="2" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2476" ColumnName="ID_MEDIA_RIGHT_TM" Description="ID_MEDIA_RIGHT_TM" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2518" ColumnName="Parent Level 2" Description="Parent Level 2" ColumnType="D" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2519" ColumnName="Parent Level 3" Description="Parent Level 3" ColumnType="D" OrdinalPosition="3" ColumnOrder="4" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2520" ColumnName="Media Rights Flat" Description="Media Rights Flat" ColumnType="D" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2521" ColumnName="ID_MEDIA_RIGHT_ITEM" Description="ID_MEDIA_RIGHT_ITEM" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="" Definition="" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2969" ColumnName="Media Rights Items" Description="Media Rights Items" ColumnType="D" OrdinalPosition="6" ColumnOrder="7" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2970" ColumnName="Created Date" Description="Created Date" ColumnType="D" OrdinalPosition="8" ColumnOrder="9" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2971" ColumnName="Updated Date" Description="Updated Date" ColumnType="D" OrdinalPosition="9" ColumnOrder="10" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="" Definition="" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories />
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="16" ObjectSchema="" ObjectName="VV_RB_Deals" Description="Deals" Type="VV" Definition="select d.*,uod.id_user_orig from mv_rb_deals d, MV_RB_USER_OUTLET_DEALS uod&#xA;where d.deal_pk=uod.deal_pk" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="195" ColumnName="DEAL_PK" Description="DEAL_PK" ColumnType="V" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="DEAL_PK" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="196" ColumnName="Deal ID" Description="Deal ID" ColumnType="V" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Deal ID" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="197" ColumnName="Deal Description" Description="Deal Code" ColumnType="V" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Description" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1668" ColumnName="Contract No" Description="Contract No" ColumnType="V" OrdinalPosition="3" ColumnOrder="4" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Contract No" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2648" ColumnName="Master Deal ID" Description="Master Deal ID" ColumnType="V" OrdinalPosition="3" ColumnOrder="4" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Master Deal ID" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2649" ColumnName="Master Agreement" Description="Master Agreement" ColumnType="V" OrdinalPosition="4" ColumnOrder="5" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Master Agreement" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2650" ColumnName="Business Unit" Description="Business Unit" ColumnType="V" OrdinalPosition="5" ColumnOrder="6" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Business Unit" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2651" ColumnName="Internal Term Sheet Status" Description="Internal Term Sheet Status" ColumnType="V" OrdinalPosition="7" ColumnOrder="8" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Internal Term Sheet Status" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="199" ColumnName="Deal Type" Description="Agreement Type" ColumnType="V" OrdinalPosition="4" ColumnOrder="8" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Type" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="198" ColumnName="Deal Name" Description="Deal Name" ColumnType="V" OrdinalPosition="8" ColumnOrder="9" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="200" ColumnName="Contract Status" Description="Contract Status" ColumnType="V" OrdinalPosition="5" ColumnOrder="9" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Contract Status" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="203" ColumnName="Date Executed" Description="Approval Date" ColumnType="V" OrdinalPosition="7" ColumnOrder="10" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Date Executed" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="1669" ColumnName="Deal Sub Type" Description="Agreement Sub Type" ColumnType="V" OrdinalPosition="6" ColumnOrder="10" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Sub Type" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="204" ColumnName="Date Effective" Description="Date Effective" ColumnType="V" OrdinalPosition="8" ColumnOrder="11" DataType="200" CharacterMaxLen="10" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Date Effective" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="202" ColumnName="Contract Data Entry Status" Description="Contract Data Entry Status" ColumnType="V" OrdinalPosition="6" ColumnOrder="12" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Contract Data Entry Status" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="206" ColumnName="Contract Currency" Description="Contract Currency" ColumnType="V" OrdinalPosition="10" ColumnOrder="14" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Contract Currency" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="209" ColumnName="Agreement Type" Description="Deal Type" ColumnType="V" OrdinalPosition="13" ColumnOrder="16" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Agreement Type" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="205" ColumnName="Profit Center" Description="Profit Center" ColumnType="V" OrdinalPosition="15" ColumnOrder="16" DataType="200" CharacterMaxLen="4000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Profit Center" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="210" ColumnName="Integration Action" Description="Integration Action" ColumnType="V" OrdinalPosition="14" ColumnOrder="17" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Integration Action" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="211" ColumnName="Contract Origin" Description="Contract Origin" ColumnType="V" OrdinalPosition="15" ColumnOrder="18" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Contract Origin" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="207" ColumnName="Budget Line" Description="Budget Line" ColumnType="V" OrdinalPosition="17" ColumnOrder="18" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Budget Line" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="208" ColumnName="Source Name" Description="Source Name" ColumnType="V" OrdinalPosition="18" ColumnOrder="19" DataType="129" CharacterMaxLen="9" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Source Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="212" ColumnName="Billing Frequency" Description="Billing Frequency" ColumnType="V" OrdinalPosition="16" ColumnOrder="19" DataType="200" CharacterMaxLen="1000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Billing Frequency" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="213" ColumnName="Custom Attribute1" Description="Custom Attribute1" ColumnType="V" OrdinalPosition="17" ColumnOrder="20" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Custom Attribute1" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="214" ColumnName="Custom Attribute2" Description="Custom Attribute2" ColumnType="V" OrdinalPosition="18" ColumnOrder="21" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Custom Attribute2" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="215" ColumnName="Custom Attribute3" Description="Custom Attribute3" ColumnType="V" OrdinalPosition="19" ColumnOrder="22" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Custom Attribute3" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="216" ColumnName="Custom Attribute4" Description="Custom Attribute4" ColumnType="V" OrdinalPosition="20" ColumnOrder="23" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Custom Attribute4" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1670" ColumnName="Primary Party" Description="Primary Party" ColumnType="V" OrdinalPosition="22" ColumnOrder="23" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Primary Party" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="218" ColumnName="List of Approved Users" Description="List of Approved Users" ColumnType="V" OrdinalPosition="22" ColumnOrder="24" DataType="200" CharacterMaxLen="2000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="List of Approved Users" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1671" ColumnName="Distributor Party" Description="Distributor Party" ColumnType="V" OrdinalPosition="23" ColumnOrder="24" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Distributor Party" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2652" ColumnName="Distributor Without Internal" Description="Distributor Without Internal" ColumnType="V" OrdinalPosition="24" ColumnOrder="25" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Distributor Without Internal" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="217" ColumnName="List of Pending Users" Description="List of Pending Users" ColumnType="V" OrdinalPosition="21" ColumnOrder="25" DataType="200" CharacterMaxLen="2000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="List of Pending Users" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1673" ColumnName="Primary Business Outlet" Description="Primary Business Outlet" ColumnType="V" OrdinalPosition="26" ColumnOrder="25" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Primary Business Outlet" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1674" ColumnName="SAP Channel ID" Description="SAP Channel ID" ColumnType="V" OrdinalPosition="27" ColumnOrder="26" DataType="200" CharacterMaxLen="100" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="SAP Channel ID" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="1672" ColumnName="Licensor Party" Description="Licensor Party" ColumnType="V" OrdinalPosition="25" ColumnOrder="26" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Licensor Party" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="219" ColumnName="CDSK Number" Description="CDSK Number" ColumnType="V" OrdinalPosition="23" ColumnOrder="26" DataType="200" CharacterMaxLen="2000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="CDSK Number" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="222" ColumnName="Created By" Description="Created By" ColumnType="V" OrdinalPosition="28" ColumnOrder="27" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Created By" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="220" ColumnName="CDSK Status" Description="CDSK Status" ColumnType="V" OrdinalPosition="24" ColumnOrder="27" DataType="200" CharacterMaxLen="2000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="CDSK Status" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="221" ColumnName="CDSK Hold" Description="CDSK Hold" ColumnType="V" OrdinalPosition="25" ColumnOrder="28" DataType="200" CharacterMaxLen="2000" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="CDSK Hold" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="224" ColumnName="Created Date" Description="Created Date" ColumnType="V" OrdinalPosition="29" ColumnOrder="28" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="0" Definition="Created Date" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="223" ColumnName="Total Asset Value" Description="Total Title Value" ColumnType="V" OrdinalPosition="26" ColumnOrder="29" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="2-digit place holder" Alignment="Right" NativeDataType="0" Definition="Total Asset Value" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="226" ColumnName="Updated By" Description="Updated By" ColumnType="V" OrdinalPosition="30" ColumnOrder="30" DataType="200" CharacterMaxLen="201" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Updated By" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="225" ColumnName="No of Assets" Description="No of Titles" ColumnType="V" OrdinalPosition="27" ColumnOrder="31" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="No of Assets" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="227" ColumnName="Updated Date" Description="Updated Date" ColumnType="V" OrdinalPosition="31" ColumnOrder="32" DataType="135" CharacterMaxLen="11" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="0" Definition="Updated Date" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2653" ColumnName="Channel Entity" Description="Channel Entity" ColumnType="V" OrdinalPosition="32" ColumnOrder="33" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Channel Entity" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2654" ColumnName="DM5 Doc Type" Description="DM5 Doc Type" ColumnType="V" OrdinalPosition="33" ColumnOrder="34" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="DM5 Doc Type" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2655" ColumnName="Parties" Description="Parties" ColumnType="V" OrdinalPosition="34" ColumnOrder="35" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Parties" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2656" ColumnName="Financial Status" Description="Financial Status" ColumnType="V" OrdinalPosition="35" ColumnOrder="36" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Financial Status" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="228" ColumnName="ID_USER_ORIG" Description="ID_USER_ORIG" ColumnType="V" OrdinalPosition="30" ColumnOrder="37" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="ID_USER_ORIG" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2657" ColumnName="Client ID" Description="Client ID" ColumnType="V" OrdinalPosition="36" ColumnOrder="37" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Client ID" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2658" ColumnName="Non Outlet Party" Description="Non Outlet Party" ColumnType="V" OrdinalPosition="37" ColumnOrder="38" DataType="200" CharacterMaxLen="32767" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Non Outlet Party" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2659" ColumnName="Skip Workflow" Description="Skip Workflow" ColumnType="V" OrdinalPosition="38" ColumnOrder="39" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Skip Workflow" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2660" ColumnName="All Media Rights" Description="All Media Rights" ColumnType="V" OrdinalPosition="39" ColumnOrder="40" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="All Media Rights" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2661" ColumnName="All Promotion Rights" Description="All Promotion Rights" ColumnType="V" OrdinalPosition="40" ColumnOrder="41" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="All Promotion Rights" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2662" ColumnName="No Promotion Rights" Description="No Promotion Rights" ColumnType="V" OrdinalPosition="41" ColumnOrder="42" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="No Promotion Rights" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2663" ColumnName="All Editing Rights" Description="All Editing Rights" ColumnType="V" OrdinalPosition="42" ColumnOrder="43" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="All Editing Rights" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2664" ColumnName="No Editing Rights" Description="No Editing Rights" ColumnType="V" OrdinalPosition="43" ColumnOrder="44" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="No Editing Rights" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2665" ColumnName="ProMamS Contract Status" Description="ProMamS Contract Status" ColumnType="V" OrdinalPosition="44" ColumnOrder="45" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="ProMamS Contract Status" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="2666" ColumnName="No Revenue participation" Description="No Revenue participation" ColumnType="V" OrdinalPosition="45" ColumnOrder="46" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="No Revenue participation" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2667" ColumnName="Deal ID (Main)" Description="Deal ID (Main)" ColumnType="V" OrdinalPosition="46" ColumnOrder="47" DataType="131" CharacterMaxLen="22" NumericPrecision="0" NumericScale="129" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Deal ID (Main)" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="2668" ColumnName="Deal Name (Main)" Description="Deal Name (Main)" ColumnType="V" OrdinalPosition="47" ColumnOrder="48" DataType="200" CharacterMaxLen="255" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Name (Main)" Explanation="" HideColumn="0" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="6" CategoryName="Rights In" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
      <Object ElementID="13" ObjectSchema="" ObjectName="VV_RB_Deal_Navigator" Description="Deal Navigator" Type="VV" Definition="select d.*,uod.id_user_orig from mv_rb_deal_navigator d, MV_RB_USER_OUTLET_DEALS uod&#xD;&#xA;where d.deal_pk=uod.deal_pk" HideObject="0" IsCatalogue="1" Explanation="">
        <Columns>
          <Column ElementID="153" ColumnName="DEAL_PK" Description="DEAL_PK" ColumnType="V" OrdinalPosition="0" ColumnOrder="1" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="DEAL_PK" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="154" ColumnName="Deal ID" Description="Deal ID" ColumnType="V" OrdinalPosition="1" ColumnOrder="2" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Deal ID" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="155" ColumnName="Deal Name" Description="Deal Name" ColumnType="V" OrdinalPosition="2" ColumnOrder="3" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Deal Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="156" ColumnName="ASSET_PK" Description="ASSET_PK" ColumnType="V" OrdinalPosition="3" ColumnOrder="4" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="ASSET_PK" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="157" ColumnName="Asset ID" Description="Title ID" ColumnType="V" OrdinalPosition="4" ColumnOrder="5" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="Asset ID" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="158" ColumnName="DEAL_ASSET_DETAIL_PK" Description="DEAL_ASSET_DETAIL_PK" ColumnType="V" OrdinalPosition="4" ColumnOrder="6" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="DEAL_ASSET_DETAIL_PK" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="159" ColumnName="Asset Title" Description="Title Name" ColumnType="V" OrdinalPosition="5" ColumnOrder="7" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Asset Title" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="160" ColumnName="Asset Type" Description="Title Type" ColumnType="V" OrdinalPosition="6" ColumnOrder="8" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Asset Type" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="161" ColumnName="Series Name" Description="Series Name" ColumnType="V" OrdinalPosition="7" ColumnOrder="9" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Series Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="162" ColumnName="Applies To" Description="Applies To" ColumnType="V" OrdinalPosition="8" ColumnOrder="10" DataType="200" CharacterMaxLen="513" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Applies To" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="164" ColumnName="On Or Before" Description="On Or Before" ColumnType="V" OrdinalPosition="10" ColumnOrder="11" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="On Or Before" Explanation="" HideColumn="1" FrameID="" />
          <Column ElementID="166" ColumnName="Letigation Pending" Description="Do Not Air" ColumnType="V" OrdinalPosition="9" ColumnOrder="12" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Letigation Pending" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="165" ColumnName="Program Do Not Air" Description="Program Do Not Air" ColumnType="V" OrdinalPosition="11" ColumnOrder="13" DataType="200" CharacterMaxLen="3" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Program Do Not Air" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="163" ColumnName="Season Name" Description="Season Name" ColumnType="V" OrdinalPosition="8" ColumnOrder="14" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Season Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="167" ColumnName="Source Name" Description="Source Name" ColumnType="V" OrdinalPosition="12" ColumnOrder="15" DataType="200" CharacterMaxLen="256" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Source Name" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="168" ColumnName="Created By" Description="Created By" ColumnType="V" OrdinalPosition="13" ColumnOrder="16" DataType="200" CharacterMaxLen="513" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Created By" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="169" ColumnName="Updated By" Description="Updated By" ColumnType="V" OrdinalPosition="14" ColumnOrder="17" DataType="200" CharacterMaxLen="513" NumericPrecision="0" NumericScale="0" DisplayFormat="" Alignment="Left" NativeDataType="0" Definition="Updated By" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="170" ColumnName="Created Date" Description="Created Date" ColumnType="V" OrdinalPosition="15" ColumnOrder="18" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="0" Definition="Created Date" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="171" ColumnName="Updated Date" Description="Updated Date" ColumnType="V" OrdinalPosition="16" ColumnOrder="19" DataType="135" CharacterMaxLen="7" NumericPrecision="0" NumericScale="0" DisplayFormat="Short Date" Alignment="Left" NativeDataType="0" Definition="Updated Date" Explanation="" HideColumn="0" FrameID="" />
          <Column ElementID="172" ColumnName="ID_USER_ORIG" Description="ID_USER_ORIG" ColumnType="V" OrdinalPosition="17" ColumnOrder="20" DataType="131" CharacterMaxLen="22" NumericPrecision="38" NumericScale="0" DisplayFormat="General Number" Alignment="Right" NativeDataType="0" Definition="ID_USER_ORIG" Explanation="" HideColumn="1" FrameID="" />
        </Columns>
        <ObjectCategories>
          <ObjectCategory CategoryID="6" CategoryName="Rights In" />
        </ObjectCategories>
        <Dependencies />
        <Attributes />
      </Object>
    </Task>
    <Task Type="Relation">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" />
      <Relation ElementID="67" RelationName="Deal Navigator -&gt; Assets" Description="" Relation="Inner Join" ObjectID1="13" ObjectID2="67" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="1">
        <RelationDetails>
          <RelationColumn ColumnID1="156" ColumnName1="ASSET_PK" ColumnID2="1042" ColumnName2="ASSET_PK" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="13" ObjectSchema="RLANLTCS" ObjectName="VV_RB_Deal_Navigator">
            <Column ID="156" ColumnName="ASSET_PK" />
          </Dependency>
          <Dependency Type="Object" ID="67" ObjectName="MV_RB_ASSETS">
            <Column ID="1042" ColumnName="ASSET_PK" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="66" RelationName="Deal Navigator -&gt; Deals" Description="" Relation="Inner Join" ObjectID1="13" ObjectID2="16" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="1">
        <RelationDetails>
          <RelationColumn ColumnID1="153" ColumnName1="DEAL_PK" ColumnID2="195" ColumnName2="DEAL_PK" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="13" ObjectSchema="" ObjectName="VV_RB_Deal_Navigator">
            <Column ID="153" ColumnName="DEAL_PK" />
          </Dependency>
          <Dependency Type="Object" ID="16" ObjectName="VV_RB_Deals">
            <Column ID="195" ColumnName="DEAL_PK" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="64" RelationName="Deal Navigator -&gt; Media Rights" Description="" Relation="Left Outer Join" ObjectID1="13" ObjectID2="22" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="1">
        <RelationDetails>
          <RelationColumn ColumnID1="158" ColumnName1="DEAL_ASSET_DETAIL_PK" ColumnID2="355" ColumnName2="DEAL_ASSET_DETAIL_PK" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="13" ObjectSchema="RLANLTCS" ObjectName="VV_RB_Deal_Navigator">
            <Column ID="158" ColumnName="DEAL_ASSET_DETAIL_PK" />
          </Dependency>
          <Dependency Type="Object" ID="22" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="355" ColumnName="DEAL_ASSET_DETAIL_PK" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="146" RelationName="Media Rights -&gt; Channel Tree" Description="" Relation="Left Outer Join" ObjectID1="22" ObjectID2="92" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="1765" ColumnName1="ID_BUSINESS_OUTLET_TM" ColumnID2="1654" ColumnName2="ID_BUSINESS_OUTLET_TM" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="1765" ColumnName="ID_BUSINESS_OUTLET_TM" />
          </Dependency>
          <Dependency Type="Object" ID="92" ObjectName="MV_RB_ITEMS_BUS_OUTLET">
            <Column ID="1654" ColumnName="ID_BUSINESS_OUTLET_TM" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="145" RelationName="Media Rights -&gt; Enduser Rights Tree" Description="" Relation="Left Outer Join" ObjectID1="22" ObjectID2="138" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="1764" ColumnName1="ID_ENDUSER_RIGHTS_TM" ColumnID2="2470" ColumnName2="ID_ENDUSER_RIGHTS_TM" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="1764" ColumnName="ID_ENDUSER_RIGHTS_TM" />
          </Dependency>
          <Dependency Type="Object" ID="138" ObjectName="MV_RB_ITEMS_ENDUSR_RIGHTS">
            <Column ID="2470" ColumnName="ID_ENDUSER_RIGHTS_TM" />
          </Dependency>
        </Dependencies>
      </Relation>
      <Relation ElementID="139" RelationName="Media Rights -&gt; Media Rights Tree" Description="" Relation="Left Outer Join" ObjectID1="22" ObjectID2="140" ObjectLabel1="" ObjectLabel2="" HideJoin="0" AutoReverse="0">
        <RelationDetails>
          <RelationColumn ColumnID1="1758" ColumnName1="ID_MEDIA_RIGHT_TM" ColumnID2="2476" ColumnName2="ID_MEDIA_RIGHT_TM" />
        </RelationDetails>
        <Dependencies>
          <Dependency Type="Object" ID="22" ObjectSchema="RLANLTCS" ObjectName="MV_RB_DEAL_MEDIA_RIGHTS">
            <Column ID="1758" ColumnName="ID_MEDIA_RIGHT_TM" />
          </Dependency>
          <Dependency Type="Object" ID="140" ObjectName="MV_RB_ITEMS_MEDIA_RIGHTS">
            <Column ID="2476" ColumnName="ID_MEDIA_RIGHT_TM" />
          </Dependency>
        </Dependencies>
      </Relation>
    </Task>
    <Task Type="DataFormat">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" />
      <DataFormat ElementID="1" FormatKey="GeneralNumber" FormatName="General Number" Format="General Number" Internal="1" AppliesTo="9" Explanation="" IsAvailable="1" SortOrder="1" />
      <DataFormat ElementID="13" FormatKey="ShortDate" FormatName="Short Date" Format="Short Date" Internal="1" AppliesTo="10" Explanation="" IsAvailable="1" SortOrder="13" />
    </Task>
    <Task Type="SessionParameter">
      <SynchronizationOptions OverwriteExisting="False" OverwriteExistingDependencies="False" />
      <SessionParameter ElementID="1" ParameterName="spUserId" DefaultValue="1" DataTypeCategory="1" />
    </Task>
  </Tasks>
  <AppSettings CopyConnectionString="False" CopyGeneralSettings="False" CopyUIFeatures="False" CopyReportingOptions="False" CopyReportingFeatures="False" CopySecuritySettings="False" MultipleOrganizations="False" UniqueReportFolderNames="False" CopySchedulerServiceSettings="False" CopySchedulingConfiguration="False" CopySMTPServerSettings="False" CopyEmailConfiguration="False" CopyArchiveConfiguration="False" CopyArchiveSettings="False" CopyEventLogDBConnection="False" CopyEventLogConfiguration="False">
    <Database ElementID="1" MaxReportRecords="500000" MaxListRecords="10000" DesignTimeValidation="Off" DefaultTemplate="Custom-Blue" />
    <Organization ElementID="1" GroupName="Default" Description="" DefaultTheme="Rights" />
  </AppSettings>
</SynchronizationPackage>