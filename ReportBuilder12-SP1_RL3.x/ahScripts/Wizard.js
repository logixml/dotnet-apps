// Wizard.js 
// Common javascript functions used in the Ad Hoc Report Builder Wizard

// borrowed from http://www.zeldman.com
// toggle visibility 
function toggle( targetId )
{
	if (document.getElementById)
	{
		target = document.getElementById( targetId );
  		if (target.style.display == "none")
  		{
  			target.style.display = "";
  		} 
  		else 
  		{
  			target.style.display = "none";
  		}
	}
}

// functions used to move items within the MSL
// borrowed from AppDev code and modified for cross-browser compatibility

// move selected options from the available to the assigned list or vice versa
// item is not moved if its value is blank or undefined
function MoveSelected(idFrom, idTo) {
	var el = document.getElementById(idFrom);
	var e2;
  
	if (el != null) {
		for (i=0; i<el.options.length; i++) {
			if (el.options[i].selected && el.options[i].value.length > 0) {
				e2 = el.options[i].cloneNode(true);
				document.getElementById(idTo).appendChild(e2);
				el.options[i] = null;
				i--;
			}
		}
		StoreList(idFrom);
		StoreList(idTo);
	}
}

// copy selected options from the vailable to the assigned list or vice versa
// item is not copied if its value is blank or undefined
function CopySelected(idFrom, idTo) {
	var el = document.getElementById(idFrom);
	var e2;
	
	if (el != null) {
		for (i=0; i<el.options.length; i++) {
			if (el.options[i].selected && el.options[i].value.length > 0) {
				e2 = el.options[i].cloneNode(true);
				document.getElementById(idTo).appendChild(e2);
			}
		}
		StoreList(idFrom);
		StoreList(idTo);
	}
	
}

// move selected options higher in the list
function MoveUp(idFrom) {
	var el = document.getElementById(idFrom)

	if (el != null) {
		if (! (el.options[0].selected)) {
			for (i=1; i<el.options.length; i++) {
				if (el.options[i].selected) {
					DOMNode_swapNode(el.options[i-1],el.options[i]);
					el.options[i-1].selected = true;
				}
			}
			StoreList(idFrom);
		}
	}
}

// move selected options lower in the list
function MoveDn(idFrom) {
	var el = document.getElementById(idFrom);
    
	if (el != null) {
		if (! (el.options[el.options.length-1].selected)) {
			for (i=el.options.length-2; i>-1; i--) {
				if (el.options[i].selected) {
					DOMNode_swapNode(el.options[i],el.options[i+1]);
				}
			}
			StoreList(idFrom);
		}
	}
}

// borrowed from http://www.xs4all.nl/~zanstra/logs/jsLog.htm (js weblog)
// performs a DOM-compliant version of MS JScript swapNode
function DOMNode_swapNode(n1,n2) 
{ 
	n1.parentNode.insertBefore(n2.parentNode.removeChild(n2),n1); 
}

// populates a hidden field with a comma-delimited list of values in a list
function StoreList(idList)
{
	var el = document.getElementById(idList);
	var elHid = document.getElementById("hidden"+idList);
	var aValues = new Array();

	if (el != null && elHid != null)
	{	
		for (i=0;i<el.options.length;i++)
		{
			aValues[i] = el.options[i].value;
		} 	
		elHid.value = aValues.toString();
	}	
}

// validate that at least one item has been assigned
// NOTE: This uses VAM functionality.
// VAM expects -1 for CannotEvaluate,
// 1 for success, and 0 (zero) for failure.
function IsColumnAssigned(cond)
{
	var el = document.getElementById(cond.IDToEval);
	if (el == null) 
	{
		return -1;
	}
	if (el.options.length > 0)
	{
		return 1;
	}
	else 
	{
		return 0;
	}
}