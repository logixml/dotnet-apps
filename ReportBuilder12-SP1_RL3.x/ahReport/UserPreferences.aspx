<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UserPreferences.aspx.vb" Inherits="LogiAdHoc.ahReport_UserPreferences" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PasswordControl" Src="~/ahControls/PasswordControl.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="SelectReportControl" Src="~/ahControls/SelectReportControl.ascx" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Preferences</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
    <script language="javascript">
    <!--
    //Since we are hiding and showing Select report controls
    //This function needs to be declared here.
    function PanelResizing()
    {
        var pnl_width = parseInt(document.getElementById('ucSelectReports_pnlTreeView').style.width);
        var pnl_height = parseInt(document.getElementById('ucSelectReports_pnlTreeView').style.height);
        document.getElementById('ucSelectReports_divTreeView').style.width = pnl_width + 'px';
        document.getElementById('ucSelectReports_divTreeView').style.height = pnl_height + 'px';
        document.getElementById('ucSelectReports_divListBox').style.width = (600 - pnl_width) + 'px';
        document.getElementById('ucSelectReports_divListBox').style.height = pnl_height + 'px';
        ScrollableListBoxRefineHeightAndWidth(document.getElementById('ucSelectReports_lstReports'), 200, 500);
    }
    function popupWin(){
        
        var lnk = document.getElementById("LinkURL")
        var sURL = lnk.value
        //Added a check for http:// as this is the default text and window.open was opening
        //the url in the same window instead of popup if the user clicks on Test URL without 
        //changing anything.
        if (sURL != null && sURL != '' && sURL!= 'http://')
            window.open(sURL); 
        else
            alert('Please enter a valid URL.')            
   }
    -->
    </script>

</head>
<body id="bod" >
    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
	<div id="submenu"><AdHoc:NavMenu id="subnav" runat="server" /></div>	
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="UserPref" ParentLevels="0" />
        
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"  />
        
        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            
            function EndRequestHandler(sender, args) {                
                noMessage=false;
                $find('ahSavePopupBehavior').hide();
                RestorePopupPosition('ahModalPopupBehavior');  
            }
            
            function BeginRequestHandler(sender, args) {            
                SavePopupPosition('ahModalPopupBehavior');                
            }
            
            function ShowApplicationPageSearch()
            {
                var divid = document.getElementById('SelectFolderTable');
                divid.style.display = "block";
            }
            
            function HideApplicationPageSearch()
            {
                var divid = document.getElementById('SelectFolderTable');
                divid.style.display = "none";
            }
        </script> 
        
        <div class="divForm">
        
        <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
        <input id="UserID" type="hidden" runat="server" />
        
            <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="Conditional" RenderMode="inline">
                <ContentTemplate>
                
                    <table cellpadding="4" cellspacing="4">
                        <tr>
                            <td width="135"align="left">
                                <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3" Text="Homepage Type"></asp:Localize>:
                            </td>
                            <td style="width:750px;" valign="top" colspan="2">
                                <asp:RadioButton ID="rbtnLinkHomepage" runat="server" GroupName="HomePageCondition"
                                    Text="Application Page" meta:resourcekey="rbtnLinkHomepageResource1" AutoPostBack="true" OnCheckedChanged="SetLinkAsHomepage" />
                                <asp:RadioButton ID="rbtnReportHomePage" runat="server" GroupName="HomePageCondition"
                                    Text="Report" meta:resourcekey="rbtnReportHomepageResource1"  AutoPostBack="true" OnCheckedChanged="SetReportAsHomepage"/>
                                <asp:RadioButton ID="rbtnURLHomePage" runat="server" GroupName="HomePageCondition"
                                    Text="URL" meta:resourcekey="rbtnURLHomepageResource1"  AutoPostBack="true" OnCheckedChanged="SetURLAsHomepage"/>
                                <asp:RadioButton ID="rbtnPreDefinedReport" runat="server" GroupName="HomePageCondition" 
                                    Text="Pre-defined Report" meta:resourcekey="LinkPreDefinedResource1" AutoPostBack="True" OnCheckedChanged="SetPredefinedReportAsHomepage" />                                              
                            </td>
                        </tr>
                        <tr>
                            <td width="135" align="left"><asp:Literal ID="lblSelectedType" runat="server" Text="Selected Report" />:</td>
                            <td style="width:620px;" valign="top">
                                
                                                <div ID="tdDivApplicationPage" runat="server" width="600" style="vertical-align:text-top;">
                                                    <asp:Literal ID="lblFolder" runat="server" Visible="false"></asp:Literal>                                                    
                                                    <input type="hidden" id="FolderID" name="FolderID" runat="server" />                                                            
                                                    
                                                    <asp:TreeView ID="tvAppPages" runat="server" ShowLines="True" EnableClientScript="False" OnSelectedNodeChanged="tvAppPages_SelectedNodeChanged" >
                                                        <NodeStyle HorizontalPadding="4px" ImageUrl="~/ahImages/iconSReport.gif" />
                                                        <SelectedNodeStyle HorizontalPadding="4px" ImageUrl="~/ahImages/iconSReport.gif" CssClass="treenodeSelected" />
                                                    </asp:TreeView>
                                                </div>
                                                
                                                
                                                <div ID="tdDivReport" runat="server" width="600" style="vertical-align:text-top;">
                                                
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <asp:Literal ID="LinkReport" runat="server" ></asp:Literal>
                                                                <asp:CustomValidator ID="cvLinkReportValidator" runat="server" ControlToValidate="txtProxy" ErrorMessage="<%$ Resources:Errors,Err_NoReport %>" EnableClientScript="False" OnServerValidate="IsLinkReportValid">*</asp:CustomValidator>                                                   
                                                            </td>
                                                            <td align="right" valign="top">
                                                                <AdHoc:LogiButton ID="btnFindReport" Width="90px" runat="server" Text="<%$ Resources:LogiAdHoc, FindReport %>" OnClick="FindReport" CausesValidation="False" />                                                        
                                                            </td>
                                                        </tr>
                                                    </table>
                                                
                                                    
                                                    <asp:Label ID="lbltxtProxy" runat="server" CssClass="NoShow" Text="<%$ Resources:LogiAdHoc, Report %>" AssociatedControlID="txtProxy" ></asp:Label>
                                                    <input id="txtProxy" class="hidden" type="text" size="3" value="0" runat="server" />
                                                     <input id="LinkReportID" type="hidden" runat="server" />
                                                    
                                                        
                                                    <div id="divReports" runat="server">
                                                        <table>
	                                                        <tr>
	                                                            <td>
				                                                    <AdHoc:SelectReportControl ID="ucSelectReports" runat="Server" />
	                                                            </td>
	                                                            <td>
	                                                                <AdHoc:LogiButton ID="btnFVOk" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, OK %>" OnClick="btnFVOk_Click" CausesValidation="False" />
	                                                                <br />
	                                                                <AdHoc:LogiButton ID="btnFVCancel" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" OnClick="btnFVCancel_Click" CausesValidation="False" />
	                                                            </td>
	                                                        </tr>
	                                                    </table>
                                                    </div>
                                                </div>
                                                
                                                
                                                <div ID="tdDivExternalLink" runat="server" width="600">
                                                   
                                                    <asp:Label ID="lblLinkURL" runat="server" CssClass="NoShow" Text="<%$ Resources:LogiAdHoc, URLTitle %>" AssociatedControlID="LinkURL" ></asp:Label>
                                                    <input id="LinkURL" type="text" maxlength="255" size="60" runat="server" />
                                                    <asp:RequiredFieldValidator ID="rtvLinkURL" runat="server" ControlToValidate="LinkURL"
                                                        ErrorMessage="<%$ Resources:Errors, Err_RequiredLinkURL %>">*</asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revLinkURL" runat="server" ControlToValidate="LinkURL"
                                                        ErrorMessage="<%$ Resources:Errors, Err_InvalidURL %>" ValidationExpression="https?://([\w-])+[\w-:\.]+(/[\w- ./?%&=]*)?" meta:resourcekey="revParamNameResource1">*</asp:RegularExpressionValidator>
                                       
                                                    <AdHoc:LogiButton ID="btnTestURL" Text="<%$ Resources:LogiAdHoc, TestURL %>" 
                                                        CausesValidation="false" runat="server" OnClientClick="popupWin()" UseSubmitBehavior="false" />
                                                   
                                                </div>
                                                
                                                
                                                <div id="tdDivPredefinedReport" runat="server" width="600" style="vertical-align:text-top;">
                                                
                                                    <table cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td align="left" valign="top">
                                                                <asp:Literal ID="lblPreDefinedReport" runat="server" ></asp:Literal>                                                    
                                                                <asp:CustomValidator ID="cvPredefinedReport" runat="server" ControlToValidate="txtPredefinedProxy"
                                                                    ErrorMessage="<%$ Resources:Errors,Err_NoReport %>" EnableClientScript="False" OnServerValidate="IsPredefinedReportValid">*</asp:CustomValidator>                                                                        
                                                            </td>
                                                            <td align="left" valign="top">
                                                                <AdHoc:LogiButton ID="btnFindPredefinedReport" Width="90px" runat="server" Text="<%$ Resources:LogiAdHoc, FindReport %>" OnClick="FindPreDefinedReport" CausesValidation="False" />
                                                            </td>
                                                        </tr>
                                                        </table>
                                                
                                                    
                                                    
                                                    <div id="divPredefinedReport" runat="server">
                                                        <table>
	                                                        <tr>
	                                                            <td>
				                                                    <asp:Label ID="lbllstPredefinedReport" runat="server" CssClass="NoShow" Text="<%$ Resources:LogiAdHoc, PreDefinedReports %>" AssociatedControlID="lstPredefinedReport" ></asp:Label>
				                                                    <asp:ListBox id="lstPredefinedReport" runat="server">
                                                                        <asp:ListItem Value="1" Text="<%$ Resources:LogiAdHoc, stdRptFrequent %>" />
                                                                        <asp:ListItem Value="2" Text="<%$ Resources:LogiAdHoc, stdRptRecent %>" />
                                                                    </asp:ListBox>
	                                                            </td>
	                                                            <td>
	                                                                <AdHoc:LogiButton ID="btnPDROK" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, OK %>" OnClick="btnPDRVOk_Click" CausesValidation="False" />
	                                                                <br />
	                                                                <AdHoc:LogiButton ID="btnPDRCancel" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" OnClick="btnPDRCancel_Click" CausesValidation="False" />
	                                                            </td>
	                                                        </tr>
	                                                    </table>
                                                    </div>
                                                    
                                                    <input id="PredefinedReportID" type="hidden" runat="server" />
                                                    <asp:Label ID="lbltxtPredefinedProxy" runat="server" CssClass="NoShow" Text="<%$ Resources:LogiAdHoc, PreDefinedReports %>" AssociatedControlID="txtPredefinedProxy" ></asp:Label>
                                                    <input id="txtPredefinedProxy" class="hidden" type="text" size="3" value="0" runat="server" />
                                                </div>
                                            
                                                 
                            </td>
                            <td align="left" valign="top" style="padding-top: 35px; width:250px;">                                            
                                <table id="SelectFolderTable">
                                    <tr id="trSelectFolder" runat="server">
                                        <td>                                            
                                            <AdHoc:LogiButton ID="btnPickFolder" runat="server" OnClick="PickFolder" ToolTip="Click to pick a different folder as homepage."
                                                Text="Change Folder" meta:resourcekey="btnPickFolderResource1" />                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button runat="server" ID="Button1" Style="display: none" />
                                            <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                                                TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                                                DropShadow="False" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                                            </ajaxToolkit:ModalPopupExtender>
                                            
                                            <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 506;">
                                                <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                                                    <div class="modalPopupHandle" style="width: 500px;">
                                                        <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                                                <asp:Localize ID="PopupHeader" runat="server" meta:resourcekey="PickFolderResource1"
                                                                    Text="Pick a folder"></asp:Localize>
                                                            </td>
                                                            <td style="width: 20px;">
                                                                <asp:ImageButton ID="imgClosePopup" runat="server" 
                                                                    OnClick="imgClosePopup_Click" CausesValidation="false"
                                                                    SkinID="imgbClose" ImageUrl="../ahImages/remove.gif"
                                                                    AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                                        </td></tr></table>
                                                    </div>
                                                </asp:Panel>
                                                <div class="modalDiv">
                                                <asp:UpdatePanel ID="upPopup" runat="server" UpdateMode="conditional">
                                                    <ContentTemplate>
                                                        <%--<asp:Panel ID="pnlSelectFolders" CssClass="detailpanel" runat="server">--%>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <div style="width:400px; height: 250px; overflow:auto;">
                                                                    <asp:TreeView ID="tvFolders" runat="server" ShowLines="True" EnableClientScript="False" >
                                                                        <NodeStyle HorizontalPadding="4px" ImageUrl="~/ahImages/iconSFolder.gif" />
                                                                        <SelectedNodeStyle HorizontalPadding="4px" ImageUrl="~/ahImages/iconSFolderOpen.gif" CssClass="treenodeSelected" />
                                                                    </asp:TreeView>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <AdHoc:LogiButton ID="btnPickFolder_OK" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, OK %>" OnClick="btnPickFolderOk_Click" CausesValidation="False" />
                                                                    <br />
                                                                    <AdHoc:LogiButton ID="btnPickFolder_Cancel" Width="50px" runat="server" Text="<%$ Resources:LogiAdHoc, Cancel %>" OnClick="btnPickFolderCancel_Click" CausesValidation="False" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                </div>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>                                    
                            </td>
                        </tr>
                        <tr><td colspan="3">&nbsp;</td></tr>
                        <tr>
                            <td width="135">
                                <asp:Label ID="Label4" runat="server" AssociatedControlID="chkSaveSearch" meta:resourcekey="LiteralResource4" Text="Retain search strings"></asp:Label>:
                            </td>
                            <td colspan="2">
                                <asp:CheckBox ID="chkSaveSearch" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td width="135">
                                <asp:Label ID="Label1" runat="server" AssociatedControlID="chkSaveSort" meta:resourcekey="LiteralResource5" Text="Retain sort preferences"></asp:Label>:
                            </td>
                            <td colspan="2">
                                <asp:CheckBox ID="chkSaveSort" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <div id="divButtons" class="divButtons">
                        <AdHoc:LogiButton ID="Save" runat="server" OnClick="SaveUser" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip %>"
                            Text="<%$ Resources:LogiAdHoc, Save %>" UseSubmitBehavior="false" />
                        <AdHoc:LogiButton ID="btnRestore" runat="server" OnClick="RestoreUser" ToolTip="Click to restore default settings."
                            Text="Restore Original Settings" CausesValidation="False" meta:resourcekey="btnRestoreResource1" />
                        <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1" />
                    </div>
                    <br />
                    <div id="divSaveAnimation">
                        <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                            TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                            DropShadow="False">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                            <table><tr><td>
                            <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                            </td>
                            <td valign="middle">
                            <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                            </td></tr></table>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
