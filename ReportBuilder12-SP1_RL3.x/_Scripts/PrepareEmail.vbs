
Function GetEmail()

	dim FileData
	dim fso, ts
	Const ForReading = 1

	Set fso = CreateObject("Scripting.FileSystemObject")
	Set ts = fso.OpenTextFile("@Input.Filename~", ForReading)
	FileData = ts.ReadAll

	set ts = nothing
	set fso = nothing

	'FileData = Replace(FileData, "~ReportName", "@Request.ReportName~")
	'FileData = Replace(FileData, "~ReportName", "@Procedure.GetReportName.ReportName~")
	FileData = Replace(FileData, "~ReportName", "@Input.DynReportName~")
	FileData = Replace(FileData, "~ReportLink", "@Constants.DB-" & "@Input.DBID~" & ".ArchiveWebPath~/ahReport@Request.ReportID~/@Session.GUIDExt~/@Session.GUID~.@Input.FileType~")

	'Subject = Replace("@Input.Subject~", "~ReportName", "@Request.ReportName~")
	GetEmail = FileData
End Function

Function GetSubject()
	dim Subject
	'Subject = Replace("@Input.Subject~", "~ReportName", "@Request.ReportName~")
	'Subject = Replace("@Input.Subject~", "~ReportName", "@Procedure.GetReportName.ReportName~")
	Subject = Replace("@Input.Subject~", "~ReportName", "@Input.DynReportName~")
	GetSubject = Subject
End Function
