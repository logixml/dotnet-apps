function showDeveloperSupplemental(policyEffectiveDate, natureOfBusiness){
	var isDeveloperNob = natureOfBusiness.toLowerCase().indexOf('developer') !== -1;
	if(!policyEffectiveDate) return isDeveloperNob;
	
	var dateSegments = policyEffectiveDate.split('-');
	effDate = new Date(parseInt(dateSegments[0]),parseInt(dateSegments[1]) - 1,parseInt(dateSegments[2]));
	var devSupplEffDate = new Date(2018,03,18); //js months are 0-indexed so 3 = april
	return (effDate >= devSupplEffDate && isDeveloperNob);
}

function responsiveYesNoColumnWidth(question){
	return question.IsYesNo == "True" ? 2 : 0;
}

function expandStatus(status){
	switch(status.toString().toUpperCase()){
        case "I":
            return "Issued";
        case "Q":
            return "New";
        case "U":
            return "Pending Underwriter";
        case "A":
            return "Underwriter Approved";
        case "M":
            return "Pending More Info";
        case "D":
            return "Declined";
        case "K":
            return "Possible Decline";
        case "P":
            return "Presented To Insured";
        case "E":
            return "Edit";
        case "X":
            return "Underwriter Edit";
        case "C":
            return "Closed";
        case "*":
            return "Any";
        case "9":
            return "Pending Cancel";
        case "G":
            return "Pending Endorsement";
        case "W":
            return "Submitted Endorsement";
        case "O":
            return "Endorsement Pending More Info";
        case "S":
            return "Pre-Certified";
        case "H":
            return "Cancelled";
        case "R":
            return "Pending Renewal";
        case "Z":
            return "Expired Quote";
        case "0":
            return "Unobtainable Audit"; 
		default:
			return "Unexpected status: " + status;
	}
}

function formatAnswer(answer){
	switch(answer.toString().toUpperCase()){
		case "TRUE":
			return "YES";
		case "FALSE":
			return "NO";
		default:
			return answer;
	}

}