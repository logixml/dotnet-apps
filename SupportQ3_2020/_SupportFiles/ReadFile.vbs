' Sample: This script reads a file and returns it's contents in a variable.

dim FileData     

Sub ReadFile()

	dim fso, ts
	Const ForReading = 1

	Set fso = CreateObject("Scripting.FileSystemObject")
	Set ts = fso.OpenTextFile("@Input.Filename~", ForReading)
	FileData = ts.ReadAll

	set ts = nothing
	set fso = nothing

End Sub