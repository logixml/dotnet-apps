function valInList(val) {
	var items = new Array('Directory', 'Department', 'CallType', 'Site', 'CalendarPeriod', 'Inventory', 'ChargeCode', 'InventoryType', 'InvoicePoint', 'SupplierAccount', 'SupplierInvoice', 'ChargeCategory');
	var str = "";
	for (var i = 0; i < items.length; i++) {
		if (items[i] === val) {
			return true;
		}
	}
	return false;
}