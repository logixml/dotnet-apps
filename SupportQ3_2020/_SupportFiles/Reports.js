function valInVueList(val) {
	var items = new Array('Directory', 'Department', 'CallType', 'Site', 'CalendarPeriod', 'Inventory', 'ChargeCode', 'InventoryType', 'InvoicePoint', 'SupplierAccount', 'SupplierInvoice', 'ChargeCategory');
	for (var i = 0; i < items.length; i++) {
		if (items[i] === val) {
			return true;
		}
	}
	return false;
}

function valInLogiList(val) {
	var items = new Array('ChargeClass', 'InventoryClass', 'InventoryClassTelephony', 'Summary_or_Detailed','TickBox1','AnyText','DepartmentLevel','TopN','NumbersOnly','CustomReportLabel','SmallDropDown','AnyTextArea');
	for (var i = 0; i < items.length; i++) {
		if (items[i] === val) {
			return true;
		}
	}
	return false;
}