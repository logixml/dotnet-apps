﻿<?xml version="1.0" encoding="utf-8"?>
<Process
	ID="SampleTasks"
	>
	<Remark>
		<Note
			Note="Updated for Logi Info v12.5"
		/>
	</Remark>
	<DefaultRequestParams
		localBeginDate="=DateAdd(&quot;d&quot;, 0, &quot;@Date.Yesterday~&quot;)"
		localEndDate="=DateAdd(&quot;d&quot;, -7, &quot;@Date.Yesterday~&quot;)"
		ReportFileName="Test_Report"
	/>
	<Task
		ID="taskSendEmbedded"
		>
		<Procedure
			ConnectionID="connEmail"
			EmailSubject="Logi DevNet Sample Output with Embedded Report"
			FromEmailAddress="DevNet@LogiAnalytics.com"
			FromEmailName="Logi Analytics"
			ID="procSendHTMLReport"
			ToEmailAddress="@Request.inpEmailAddress~"
			Type="SendHtmlReport"
			>
			<Target
				Report="SampleReport"
				ReportShowModes="ShowDisclaimer"
				Type="Report"
			/>
			<IfError
				ID="procIfSendError1"
				>
				<Response
					ID="respReport"
					Type="Report"
					>
					<Target
						Report="Default"
						Type="Report"
					/>
					<Note
						Note="Send the error message back to calling definition"
					/>
					<LinkParams
						StatusMsg="Error sending email: @Procedure.procSendHTMLReport.ErrorMessage~"
					/>
				</Response>
			</IfError>
		</Procedure>
		<Response
			ID="respReport"
			Type="Report"
			>
			<Target
				Report="Default"
				Type="Report"
			/>
			<LinkParams
				inpEmailAddress="@Request.inpEmailAddress~"
				StatusMsg="Status: Email was sent successfully."
			/>
		</Response>
	</Task>
	<Task
		ID="taskSendAsAttachment"
		>
		<Note
			Note="Attachment is stored in rdDownload folder, where it will be automatically cleaned up to save disk space. To keep the data instead, see the commented task below."
		/>
		<Note
			Note="---"
		/>
		<Note
			Note="If requested, run the report and save it as an HTML file to the temp folder"
		/>
		<Procedure
			Expression="&quot;@Request.ReportFormat~&quot; = &quot;HTML&quot;"
			ID="procIfFormatHTML"
			Type="If"
			>
			<Note
				Note="Use a GUID to create attachment filename"
			/>
			<Procedure
				ID="procMakeFilename"
				Type="SetSessionVars"
				>
				<SessionParams
					ReportFilename="@Function.GUID~.html"
				/>
			</Procedure>
			<Procedure
				Filename="@Function.AppPhysicalPath~\rdDownload\@Session.ReportFilename~"
				ID="procSaveAsHTMLFile"
				Type="SaveHTML"
				>
				<Target
					Report="SampleReport"
					ReportShowModes="NoDisclaimer"
					Type="Report"
				/>
			</Procedure>
		</Procedure>
		<Note
			Note="If requested, run the report and save it as a PDF file to the temp folder"
		/>
		<Procedure
			Expression="&quot;@Request.ReportFormat~&quot; = &quot;PDF&quot;"
			ID="procElseIfFormatPDF"
			Type="If"
			>
			<Note
				Note="Use a GUID to create attachment filename"
			/>
			<Procedure
				ID="procMakeFilename"
				Type="SetSessionVars"
				>
				<SessionParams
					ReportFilename="@Function.GUID~.pdf"
				/>
			</Procedure>
			<Procedure
				Filename="@Function.AppPhysicalPath~\rdDownload\@Session.ReportFilename~"
				ID="procSaveAsPDFFile"
				Type="ExportToPDF"
				>
				<Target
					Report="SampleReport"
					ReportShowModes="NoDisclaimer"
					Type="PDF"
				/>
			</Procedure>
		</Procedure>
		<Note
			Note="Send the email message with attached report file"
		/>
		<Procedure
			ConnectionID="connSMTP"
			EmailBody="&lt;p&gt;The DevNet &lt;b&gt;Process: Send Email&lt;/b&gt; Sample Application sent you this message.&lt;/p&gt;

&lt;p&gt;@Function.DateTime~&lt;/p&gt;

&lt;p&gt;Have a nice day!&lt;/p&gt;
"
			EmailBodyType="HTML"
			EmailSubject="Logi DevNet Sample Report with @Request.ReportFormat~ Attachment"
			FromEmailAddress="noreply@knowledgeanywhere.com"
			FromEmailName="Logi Analytics"
			ID="procSendEmail"
			ToEmailAddress="@Request.inpEmailAddress~"
			Type="SendMail"
			>
			<Attachment
				DisplayName="=(&apos;@Request.ReportFileName~&apos; + DateAdd(&quot;d&quot;, 0, &quot;@Date.Yesterday~&quot;) + &apos;_&apos; + DateAdd(&quot;d&quot;, -7, &quot;@Date.Yesterday~&quot;))"
				Filename="@Function.AppPhysicalPath~\rdDownload\@Session.ReportFilename~"
				ID="attReport"
				>
				<Note
					Note="Note we&apos;re not attached any supporting files (.css, .js, etc.) so the attached report"
				/>
				<Note
					Note="will look extremely basic and plain when viewed by the email recipient"
				/>
			</Attachment>
			<IfError
				ID="procIfSendError2"
				>
				<Response
					ID="respReport"
					Type="Report"
					>
					<Target
						Report="Default"
						Type="Report"
					/>
					<Note
						Note="Send the error message back to calling definition"
					/>
					<LinkParams
						StatusMsg="Error sending email: @Procedure.procSendEmail.ErrorMessage~"
					/>
				</Response>
			</IfError>
		</Procedure>
		<Response
			ID="respReport"
			Type="Report"
			>
			<Target
				Report="Default"
				Type="Report"
			/>
			<LinkParams
				inpEmailAddress="@Request.inpEmailAddress~"
				StatusMsg="Status: Email was sent successfully."
			/>
		</Response>
	</Task>
	<Remark>
		<Task
			ID="taskSendAsAttachment"
			>
			<Note
				Note="To keep attachments permanently, in UserData folder"
			/>
			<Note
				Note="---"
			/>
			<Note
				Note="If it doesn&apos;t exist, create a temporary folder for the attachment file"
			/>
			<Procedure
				Folder="@Function.AppPhysicalPath~\UserData"
				ID="procCreateTempFolder"
				Type="CreateFolder"
				>
				<IfError
					ID="procIfFileError"
					>
					<Response
						ID="respReport"
						Type="Report"
						>
						<Target
							Report="Default"
							Type="Report"
						/>
						<LinkParams
							StatusMsg="Error creating UserData folder: @Procedure.procCreateTempFolder.ErrorMessage~"
						/>
					</Response>
				</IfError>
			</Procedure>
			<Note
				Note="If requested, run the report and save it as an HTML file to the temp folder"
			/>
			<Procedure
				Expression="&quot;@Request.ReportFormat~&quot; = &quot;HTML&quot;"
				ID="procIfFormatHTML"
				Type="If"
				>
				<Note
					Note="Use a GUID to create attachment filename"
				/>
				<Procedure
					ID="procMakeFilename"
					Type="SetSessionVars"
					>
					<SessionParams
						ReportFilename="@Function.GUID~.html"
					/>
				</Procedure>
				<Procedure
					Filename="@Function.AppPhysicalPath~\UserData\@Session.ReportFilename~"
					ID="procSaveAsHTMLFile"
					Type="SaveHTML"
					>
					<Target
						Report="SampleReport"
						ReportShowModes="NoDisclaimer"
						Type="Report"
					/>
				</Procedure>
			</Procedure>
			<Note
				Note="If requested, run the report and save it as a PDF file to the temp folder"
			/>
			<Procedure
				Expression="&quot;@Request.ReportFormat~&quot; = &quot;PDF&quot;"
				ID="procElseIfFormatPDF"
				Type="If"
				>
				<Note
					Note="Use a GUID to create attachment filename"
				/>
				<Procedure
					ID="procMakeFilename"
					Type="SetSessionVars"
					>
					<SessionParams
						ReportFilename="@Function.GUID~.pdf"
					/>
				</Procedure>
				<Procedure
					Filename="@Function.AppPhysicalPath~\UserData\@Session.ReportFilename~"
					ID="procSaveAsPDFFile"
					Type="ExportToPDF"
					>
					<Target
						Report="SampleReport"
						ReportShowModes="NoDisclaimer"
						Type="PDF"
					/>
				</Procedure>
			</Procedure>
			<Note
				Note="Send the email message with attached report file"
			/>
			<Procedure
				ConnectionID="connSMTP"
				EmailBody="&lt;p&gt;The DevNet &lt;b&gt;Process: Send Email&lt;/b&gt; Sample Application sent you this message.&lt;/p&gt;

&lt;p&gt;@Function.DateTime~&lt;/p&gt;

&lt;p&gt;Have a nice day!&lt;/p&gt;
"
				EmailBodyType="HTML"
				EmailSubject="Logi DevNet Sample Report with @Request.ReportFormat~ Attachment"
				FromEmailAddress="noreply@knowledgeanywhere.com"
				FromEmailName="Logi Analytics"
				ID="procSendEmail"
				ToEmailAddress="@Request.inpEmailAddress~"
				Type="SendMail"
				>
				<Attachment
					DisplayName="=(&apos;@Request.ReportFileName~&apos; + &apos;@Request.localBeginDate~&apos; + &apos;_&apos; + &apos;@Request.localEndDate~&apos;)"
					Filename="@Function.AppPhysicalPath~\UserData\@Session.ReportFilename~"
					ID="attReport"
					>
					<Note
						Note="Note we&apos;re not attached any supporting files (.css, .js, etc.) so the attached report"
					/>
					<Note
						Note="will look extremely basic and plain when viewed by the email recipient"
					/>
				</Attachment>
				<IfError
					ID="procIfSendError2"
					>
					<Response
						ID="respReport"
						Type="Report"
						>
						<Target
							Report="Default"
							Type="Report"
						/>
						<Note
							Note="Send the error message back to calling definition"
						/>
						<LinkParams
							StatusMsg="Error sending email: @Procedure.procSendEmail.ErrorMessage~"
						/>
					</Response>
				</IfError>
			</Procedure>
			<Response
				ID="respReport"
				Type="Report"
				>
				<Target
					Report="Default"
					Type="Report"
				/>
				<LinkParams
					inpEmailAddress="@Request.inpEmailAddress~"
					StatusMsg="Status: Email was sent successfully."
				/>
			</Response>
		</Task>
	</Remark>
	<ideTestParams
		inpEmailAddress="sample@logi.com"
		rdTaskID="taskSendAsAttachment"
		ReportFileName=""
		ReportFormat="PDF"
	/>
</Process>
