function getFlagHTML(language) {
	return '<i class="flag  ' + language.toLowerCase() + '" data-qtip="' + language + '"></i>\xa0';
};

function renderSingleFlag(language) {
    var singleFlag = getFlagHTML(language, !0);
    return singleFlag;
};

function renderMultipleFlags(targetLanguages) {
    var languagesArray = targetLanguages.split(','); 
	var targetFlags = '';
	for (var i = 0; i < languagesArray.length; i++)
	{
		targetFlags = targetFlags + renderSingleFlag(languagesArray[i]) + ' ';
	}
    return targetFlags;
};

function flagRenderer(sourceLanguage, targetLanguages) {
	if(sourceLanguage == "" && targetLanguages == ""){
		return "";
	}
	var sourceFlag = renderSingleFlag(sourceLanguage);
    var signBetween = ' <i class="fa fa-caret-right"></i>\xa0';
    var targetFlags = renderMultipleFlags(targetLanguages);
    return sourceFlag + signBetween + targetFlags;
};