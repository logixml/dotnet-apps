<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PickFolder.ascx.vb" Inherits="LogiAdHoc.PickFolder" %>

<div id="divPFCtrl">
    <table class="tbTB">
        <tr id="trUserGroup" runat="server">
            <td id="Td1" runat="server" width="100" >
                <asp:Label ID="Label3" runat="server" Text="<%$ Resources:LogiAdHoc, UserGroup %>" AssociatedControlID="ddlUserGroup"></asp:Label>:
            </td>
            <td id="Td2" runat="server">
                <asp:DropDownList ID="ddlUserGroup" OnSelectedIndexChanged="UserGroup_SelectedIndexChanged" AutoPostBack="True" runat="server">
                </asp:DropDownList></td>
        </tr>
        <tr id="FolderRow" runat="server">
            <td id="Td3" runat="server" width="100">
                <asp:Label ID="Label1" runat="server" Text="<%$ Resources:LogiAdHoc, DestFolderType %>" AssociatedControlID="FolderTypeID"></asp:Label>:
            </td>
            <td id="Td4" runat="server">
                <asp:DropDownList ID="FolderTypeID" AutoPostBack="True" OnSelectedIndexChanged="FolderType_SelectedIndexChanged"
                    runat="server">
                    <asp:ListItem Value="0" Text="<%$ Resources:LogiAdHoc, Menu_SharedReports %>" />
                    <asp:ListItem Value="1" Text="<%$ Resources:LogiAdHoc, Menu_MyReports %>" />
                    <asp:ListItem Value="2" Text="<%$ Resources:LogiAdHoc, OtherUser %>" />
                    <asp:ListItem Value="3" Text="<%$ Resources:LogiAdHoc, Menu_GlobalReports %>" />
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="UserRow" runat="server">
            <td id="Td5" runat="server" width="100">
                <asp:Label ID="Label2" runat="server" Text="<%$ Resources:LogiAdHoc, NewOwner %>" AssociatedControlID="OwnerUserID"></asp:Label>:
            </td>
            <td id="Td6" runat="server">
                <asp:DropDownList ID="OwnerUserID" AutoPostBack="True" OnSelectedIndexChanged="Owner_SelectedIndexChanged"
                    runat="server" />
            </td>
        </tr>
    </table>
    <table width="100%" >    
        <tr id="ParentRow" runat="server">
            <td id="Td7" valign="top" runat="server" width="100">
                <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:LogiAdHoc, Folder %>"></asp:Localize>:
            </td>
            <td id="Td8" runat="server">
                <div style="height: 160px; overflow: auto">
                <asp:Panel ID="pnlFoldersTreeView" runat="server" CssClass="treediv" >
                    <asp:TreeView ID="tvFolders" runat="server" ShowLines="True" EnableClientScript="false">
                        <NodeStyle HorizontalPadding="4px" ImageUrl="~/ahImages/iconSFolder.gif" />
                        <SelectedNodeStyle HorizontalPadding="4px" ImageUrl="~/ahImages/iconSFolderOpen.gif"
                            CssClass="treenodeSelected" />
                    </asp:TreeView>
                </asp:Panel>
                </div>
            </td>
        </tr>
    </table>
</div>