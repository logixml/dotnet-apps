<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="TitleBar.ascx.vb" Inherits="LogiAdHoc.TitleBar" %>
<table width="100%" id="tblTitle" class="tblTitle">
<tr width="100%">
<td align="left">
    <span id="spnTitle" runat="server" class="clsTitle"></span>
    <span id="spnPath" runat="server" class="clsPath"></span>
    <span id="spnName" runat="server" class="clsName"></span>
</td>
<td align="right">
    <div id="help" class="clsHelp">
        <asp:ImageButton ID="imgbHelp" runat="Server" ImageUrl="~/ahImages/iconHelpText.gif" SkinID="imgbHelp" />
        <div id="pnlHelp" runat="server" class="hiPopupMenu" visible="false"></div>
        <ajaxToolkit:HoverMenuExtender ID="HoverMenuExtender1" runat="Server"
            PopupControlID="pnlHelp"
            PopupPosition="Left" 
            TargetControlID="imgbHelp"
            PopDelay="25" />
    </div>
</td>
</tr>
</table>
