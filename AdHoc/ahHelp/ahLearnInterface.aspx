<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945097"></a><a name="_Toc382291592">Learning the Interface</a></h2>


<p class=MsoNormal><i>Menu Bar</i></p>


<p class=MsoNormal>There are five main links available in Ad Hoc in the menu
bar at the top of the page:</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><b>Reports</b> - Modify, share and run reports.</li>
 <li class=MsoNormal><b>Profile </b>- Update your user account information and
     preferences.</li>
 <li class=MsoNormal><b>About </b>  Get summarized information about Ad Hoc.</li>
 <li class=MsoNormal><b>Help</b> - Get help using Ad Hoc.</li>
 <li class=MsoNormal><b>Logout</b> - Exit Ad Hoc.</li>
</ul>


<p class=MsoNormal>Here s what they look like:</p>



<p class=MsoNormal><img   id="Picture 3"
src="Report_Design_Guide_files/image010.jpg"></p>




<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Unless specifically stated otherwise, use of the word  report 
  or  reports  in descriptions of Ad Hoc functionality implies both reports and
  dashboards.</p>
  </div>
  </td>
 </tr>
</table>

</div>



<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<p class=MsoNormal><i>Reports Page</i></p>


<p class=MsoNormal>The <i>Reports</i> page is the default main page in Ad Hoc. For
end-users, reports are divided into <i>Personal Reports</i> and <i>Shared
Reports</i>. The <i>Personal Reports</i> tab will display a list of reports,
dashboards and folders specific to the logged in user. The <i>Shared Reports</i>
tab will similarly display a list of the reports shared by all users in the
organization.</p>


<p class=MsoNormal>Initially, the Reports page will not have any reports or
folders to list or manage and the page will appear as:</p>


<p class=MsoNormal><img   id="Picture 8"
src="Report_Design_Guide_files/image011.jpg"><br>
<br>
</p>


<p class=MsoNormal>On the page are a <b>Personal Reports</b> link to this page,
the <b>Shared Reports</b> link to the page listing the reports accessible to other
members of the organization, and the <b>New Report, New Dashboard </b>and <b>New
Folder</b> buttons to create reports, dashboards and folders respectively.</p>


<p class=MsoNormal>Once reports or folders are added to the application, the
Reports page will look something like:<br>
<br>
</p>


<p class=MsoNormal><img   id="Picture 5"
src="Report_Design_Guide_files/image012.jpg"></p>



<p class=MsoNormal><a name="_GoBack"></a>Click the <img  
id="Picture 6" src="Report_Design_Guide_files/image013.jpg"> icon to show basic
orientation help for the reports list page.</p>


<p class=MsoNormal>If more than one reporting database is available for the end-user,
a Database drop-down list will be displayed. The Database drop-down list acts
as a filter for the list of reports. Only reports based on the selected
database will be shown in the list. The database list does not filter the folders.</p>


<p class=MsoNormal>The Report Builder uses the currently selected database as
the source for all report data. Consequently, if the currently selected
database is changed, any reports that contain data from another database are
not visible.</p>


<p class=MsoNormal>In the above example is a folder named  Customer Reports 
and a report named  Categories .<br>
<br>
</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>If the Database drop-down menu is not visible, then
  the user only has access to one database. Contact the system administrator if
  additional access is needed.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpLast>The  (All)  option will only be displayed if the
  System Administrator has enabled the  Multiple Connection  option. When the
   (All)  option is selected, the report list is not filtered. In addition, the
   Modify Data Source  dialog box will present all of the data objects for all of
  the reporting databases to which the user has access.</p>
  </div>
  </td>
 </tr>
</table>

</div>



<p class=MsoNormal>The following actions are available:</p>


<p class=MsoNormal><b>New Report</b>   Click to begin the process of creating a
report.</p>


<p class=MsoNormal><b>New Folder</b>   Click to show the dialog box to create a
folder.</p>


<p class=MsoNormal><b>Delete</b>   Click to remove selected items from the list.</p>


<p class=MsoNormal><b>Copy</b>   Click to replicate the selected items</p>


<p class=MsoNormal><b>Move</b>   Click to move the selected items to another
location.<br>
<br>
</p>

<p class=MsoNormal><b>Find Reports</b>   Click to do a  contains  search of the
<i>Name</i> column for the associated text. Click the <img  
id="Picture 7" src="Report_Design_Guide_files/image014.jpg"> icon in the
textbox to clear the text and refresh the list. This option is configurable and
may be hidden.</p>



<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst>Notes:</p>
  <p class=NotesCxSpMiddle>The <i>Find Reports</i> criteria will be retained
  for all subsequent visits to the page until it has been cleared out.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>Depending on a user's <i>Preference</i> settings, the Find
  Reports criteria may be retained after logging out or terminating a browser
  session.</p>
  </div>
  </td>
 </tr>
</table>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal><u>Select All</u>   Check the checkbox in the list header to
select or deselect all items in the list.</p>


<p class=MsoNormal><u>Sort</u>   Sort the list by clicking the <i>Name</i> or <i>Last
Modified</i> header. Clicking a second time will reverse the sort order.</p>


<p class=MsoNormal><u>Select Items</u>   Click the checkbox within a row to
select or deselect the item in the list.</p>


<p class=MsoNormal><u>Run Reports </u>  Click the report link in the list to
execute the item.</p>


<p class=MsoNormal><u>Folder </u>  click the folder link in the list to
navigate to a folder.</p>



<p class=MsoNormal><b>Edit</b>   Click to edit a report or to reset the name or
description of a folder.</p>


<p class=MsoNormal><b>More &gt;</b> -  This indicates that there are multiple
actions that can be performed on the item. Hover the mouse over the icon to
display a drop-down list of available actions. Their availability is dependent
upon the configuration and the item type; however, the list may include <i>Modify,
Rename, Copy, Move, Schedule, Archive,</i> or <i>View Dependencies</i>.</p>


<p class=MsoNormal>The following icons are used to signify a report's status or
type:</p>


<p class=MsoNormal><img  
src="Report_Design_Guide_files/image015.jpg">       Ad Hoc Report</p>

<p class=MsoNormal><img   id="Picture 10"
src="Report_Design_Guide_files/image016.gif" alt=iconScheduledReport>       Scheduled
Ad Hoc Report</p>

<p class=MsoNormal><img   id="Picture 11"
src="Report_Design_Guide_files/image017.jpg">      Ad Hoc Dashboard</p>

<p class=MsoNormal><img   id="Picture 12"
src="Report_Design_Guide_files/image018.jpg">      Ad Hoc Mobile Report</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
