<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945126"></a><a name="_Toc382291622">Export to PDF</a></h2>


<p class=MsoNormal><i>Export to PDF</i> opens the report in the current browser
window as a PDF document viewable with Adobe Reader or in most modern browsers.<br>
<br>
</p>

<p class=MsoNormal><img border=0   id="Picture 231"
src="Report_Design_Guide_files/image183.jpg" alt=p></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The report is exported to
Adobe Portable Document Format.</span></p>

<p class=MsoNormal>The Adobe toolbar is displayed across the top of the report,
providing many typical PDF functions. Users can save, print and search the
entire document.</p>


<p class=MsoNormal>Save the report in PDF format by clicking <b>Save a copy</b>
from the Adobe toolbar. Choose the name and location and click <b>Save</b>. The
default file type is <b>Adobe PDF Files (*.pdf)</b>.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Reports containing drill-down groupings will not be
  expanded when exported. In order to view PDF reports from the browser, the
  Adobe Acrobat Reader plug-in must be installed. See the system administrator
  for additional help.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>




</body>
</html>
