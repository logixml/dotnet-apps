<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221773"></a><a name="_Toc457221137">Virtual Views</a></h2>


<p class=MsoNormal><span style=''>The Virtual
Views page allows the System Administrator to create customized views from objects
in the source database. End-users can include virtual views as a data source
when building reports. Virtual views are saved to the application metadata
database, leaving the source database unmodified.</span></p>


<p class=MsoNormal><span style=''>Select <b>Virtual
Views</b> from the <i>Database Configuration</i> drop-down list<b> </b>to
display the <i>Virtual Views</i> configuration page.</span></p>



<p class=MsoNormal><span style=''><img
border=0   id="Picture 99"
src="System_Admin_Guide_files/image072.jpg"></span></p>

<p class=MsoNormal><span style=''>The <b>Database</b>
drop-down list acts as a filter for the Virtual View list. Only Virtual Views
related to the selected database will be displayed. If only one reporting database
has been configured for the Ad Hoc instance, the <b>Database</b> filter will
not be shown.</span></p>


<p class=MsoNormal><span style=''>Click <b>Add</b>
to display an empty <i>Virtual View</i> page.</span></p>


<p class=MsoNormal><span style=''>Click <b>Delete</b>
to remove the selected virtual views. Virtual views are selected by clicking on
their checkboxes.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Removing a virtual view marks any reports that
  depended on that virtual view as broken. Delete virtual views with caution.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<p class=MsoNormal><span style=''>Two actions
are available for a Virtual View: <b>Modify Virtual View </b>and<b> Delete
Virtual View. </b>Hover your mouse cursor over the </span><span
style=''><img border=0  
id="Picture 100" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon to show them.</span></p>


<h3><a name="_Toc457221774"></a><a name="_Toc457221138">Adding a Virtual View</a></h3>


<p class=MsoNormal><span style=''>A virtual
view is essentially a pre-defined SQL Select statement. Typically virtual views
are used to relate multiple tables, perform data conversions, and reduce the
number of data objects that the end-user sees.</span></p>


<p class=MsoNormal><span style=''>To create a
virtual view, click the <b>Add</b> button. The following page will be
displayed:<br>
<br>
</span></p>


<p class=MsoNormal><img border=0   id="Picture 350"
src="System_Admin_Guide_files/image073.jpg"></p>

<p class=MsoNormal><span style=''>The <i>Database
Connection</i> identifies the currently selected reporting database. </span></p>


<p class=MsoNormal><span style=''>Enter a
unique <i>Virtual View Name</i>, <i>Friendly Name</i>, and <i>Definition</i>.
The <i>Definition</i> is a SQL Select statement that conforms to the syntax
rules for a SQL sub-query. Virtual views are implemented in the report
definitions as  </span><span style='font-size:11.0pt;'>SELECT
* FROM (<i>Definition)</i></span><span style=''> .</span></p>


<p class=MsoNormal><span style=''>Click <b>Test
Definition</b> to verify the <i>Definition</i> syntax using the reporting
database. If the test completed successfully, the page will display the schema
information returned by the query. For example:<br>
<br>
<br>
</span></p>

<p class=MsoNormal><span style=''><img
border=0   id="Picture 102"
src="System_Admin_Guide_files/image074.jpg"></span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the virtual view definition in the metadata database.</span></p>



<h3><a name="_Toc457221775"></a><a name="_Toc457221139">Creating Virtual Views
Using 'Save As'</a></h3>


<p class=MsoNormal><span style=''>The
administrator can create a new Virtual View by modifying an existing one, clicking
<b>Save As</b>, entering a new <i>Virtual View Name</i> and <i>Friendly Name</i>,
and clicking <b>OK</b>.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #FFFFCC 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#FFFFCC'>
  <p class=HintCxSpFirst><b>Hint:</b></p>
  <p class=HintCxSpLast>Always remember to test the virtual view definition to
  ensure that the correct data columns are returned. Modifying the sub-query
  later may break reports that depend on the virtual view for data.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<h3><a name="_Toc457221776"></a><a name="_Toc457221140">Modifying Virtual Views</a></h3>


<p class=MsoNormal><span style=''>A virtual
view can be modified by clicking on the </span><span style=''><img
border=0   id="Picture 103"
src="System_Admin_Guide_files/image008.gif" alt=iconAction></span><span
style=''> icon in the list of Virtual Views.
The <i>Friendly Name</i> and the <i>Definition</i> can be modified.</span></p>


<p class=MsoNormal><span style=''>Click <b>Test
Definition</b> to verify the new information. The <i>Virtual View Columns </i>grid
will be updated and identify the differences between the original <i>Definition</i>
and the new <i>Definition</i>. For example:</span></p>



<p class=MsoNormal><span style=''><img
border=0   id="Picture 104"
src="System_Admin_Guide_files/image075.jpg"></span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the modified information in the metadata database.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>Modifying a virtual view's sub-query may break
  reports that depend on the virtual view for data. Modify virtual views with
  caution.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>Use only SQL sub-query statements when creating
  virtual views; statements such as ORDER BY are <i>not</i> supported. For
  security purposes, the following SQL statements are always prohibited:
  EXECUTE, INSERT, UPDATE, and DELETE.</p>
  </div>
  </td>
 </tr>
</table>

</div>




</body>
</html>
