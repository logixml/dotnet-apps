<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945120"></a><a name="_Toc382291616">Adding Labels</a></h2>


<p class=MsoNormal>The Label element offers the ability to add custom messages
and text in the report. An unlimited number of labels can be added above or
below other report elements. Use labels to add text to the report that would
otherwise be too long for a table or chart caption.</p>




<p class=MsoNormal><img border=0   id="Picture 209"
src="Report_Design_Guide_files/image168.jpg" alt=c></p>


<p class=MsoNormal><span style='font-size:10.0pt'>A bar chart with a red label
beneath it.</span></p>



<p class=MsoNormal>Click the Label icon in the ADD frame to add this element to
the report definition. Alternatively, use the drag-and-drop method to place the
label in the Report Layout panel. A corresponding <i>Label Information</i> tab
will be created.</p>



<p class=MsoNormal><img border=0   id="Picture 211"
src="Report_Design_Guide_files/image169.jpg"></p>


<p class=MsoNormal>Type the text for the <i>Label</i> in the field provided.</p>


<p class=MsoNormal>Choose a <i>Label Type</i> from the drop-down list. The <i>Simple</i>
option always centers the text and <i>Full Width </i>uses the full available
width of the report.</p>


<p class=MsoNormal>OPTIONAL: Choose a text-style orientation from the <i>Style</i>
drop-down menu (e.g., red, green, Align Text Left, etc.).</p>




</body>
</html>
