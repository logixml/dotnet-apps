<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221758"></a><a name="_Toc457221122">Categories</a></h2>

<h4><i><span style='font-weight:normal;text-decoration:none'>Categories</span></i><span
style='font-weight:normal;text-decoration:none'> are a convenient mechanism for
grouping logically-related data objects, making them easier to find. <i>Categories</i>
are implemented as a filter in the Report Builder data selection dialog boxes. </span></h4>

<h4><span style='font-weight:normal;text-decoration:none'>Essentially the
System Administrator creates one or more categories and assigns data objects to
the category. Data objects may appear in more than one category since many data
objects are common to different reporting needs (e.g. the States data object can
be required to create reports on the Sales data as well as the Human Resources
data).</span></h4>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>The System Administrator may also create the
  Categories by using the Management Console. Refer to the<i> Management
  Console Usage Guide</i> for details.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal><span style=''>Select <b>Categories</b>
from the <i>Database Configuration</i> drop-down list<b> </b>to display the <i>Categories</i>
configuration page.<br>
<br>
</span></p>


<p class=MsoNormal><span style=''><img
border=0   id="Picture 49"
src="System_Admin_Guide_files/image043.jpg"></span></p>

<p class=MsoNormal><span style=''><br>
<br>
</span></p>

<p class=MsoNormal><span style=''>The <b>Database</b>
drop-down list acts as a filter for the Category list. Only Categories related
to the selected database will be displayed. If only <i>one</i> reporting
database has been configured for this Ad Hoc instance, the <b>Database</b>
filter will not be shown at all.</span></p>


<p class=MsoNormal><span style=''>Click <b>Add</b>
to display an empty <i>Category</i> page.</span></p>


<p class=MsoNormal><span style=''>Click <b>Delete</b>
to remove the selected category. Categories are selected by checking their checkboxes.</span></p>


<p class=MsoNormal><span style=''>Two actions
are available for a Category: <b>Modify Category </b>and<b> Delete Category. </b>Hover
your mouse cursor over the </span><span style=''><img
border=0   id="Picture 50"
src="System_Admin_Guide_files/image026.jpg"></span><span style='
"Arial","sans-serif"'> icon to show the available actions.</span></p>

<h4><span style='text-decoration:none'>&nbsp;</span></h4>

<p class=MsoNormal><b><span style=''>Adding a
Category</span></b></p>


<p class=MsoNormal><span style=''>To add a
category, click the <b>Add</b> button and the category dialog box will be displayed:<br>
<br>
</span></p>


<p class=MsoNormal><img border=0   id="Picture 31"
src="System_Admin_Guide_files/image044.jpg"></p>


<p class=MsoNormal><span style=''>A <i>Category
Name</i> is required and must be unique. The <i>Description </i>is optional. Assign
data objects to the category from the list of Available Objects.</span></p>


<p class=MsoNormal><span style=''>Click <b>OK</b>
to save the new Category information. </span></p>


<h5 align=left style='text-align:left'><i><span style='font-weight:normal'><span
 style='text-decoration:none'>&nbsp;</span></span></i></h5>

<i><u><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></u></i>

<h5 align=left style='text-align:left'><i><span style='font-weight:normal'><span
 style='text-decoration:none'>&nbsp;</span></span></i></h5>

<h5 align=left style='text-align:left'><span style='text-decoration:none'>Modifying
Categories</span></h5>


<p class=MsoNormal><span style=''>To change category
information, click the <b>Modify Category</b> action for the category.  The <i>Category
Details</i> dialog box will be displayed:<br>
<br>
</span></p>


<p class=MsoNormal><img border=0   id="Picture 32"
src="System_Admin_Guide_files/image045.jpg"></p>


<p class=MsoNormal><span style=''>Modify the appropriate
information and click <b>OK</b> to save the information.</span></p>


<span style='font-size:14.0pt;'><br clear=all
style='page-break-before:always'>
</span>

</body>
</html>
