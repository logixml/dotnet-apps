<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221759"></a><a name="_Toc457221123">Data Objects</a></h2>


<p class=MsoNormal><span style=''>Data Objects
are core to the Ad Hoc reporting architecture. A data object is simply a
collection of related columns whose definition resides in the metadata
database.</span></p>


<p class=MsoNormal><span style=''>Most data
objects are created by importing all or part of a schema from a reporting database
using the Management Console. The database table and view definitions are
ported into the Ad Hoc metadata database. </span></p>


<p class=MsoNormal><span style=''>Data objects
may also be created by defining virtual views and catalogs within the Ad Hoc interface.
A virtual view is a SQL SELECT statement that identifies the columns, tables,
relationships, and data transformations necessary for reporting. A catalog is a
pre-defined set of related data objects and columns. Virtual views and catalogs
only exist in the metadata database.</span></p>


<p class=MsoNormal><span style=''>The concept of
Data Object definitions existing in the metadata database is what allows the
System Administrator to customize the end-user experience and reduce the
database-specific technical knowledge required to produce complex reports. User-friendly
names, pre-defined relationships, user-defined columns, and data object-level
filters are all designed to benefit the end-user.</span></p>


<p class=MsoNormal><span style=''>The Data Objects
page allows the System Administrator to customize data objects in the application.
An object s label, column-formatting options, column descriptions, access
rights, fixed parameters, user-defined columns, and relationships can all be
modified.</span></p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>Select <b>Data
Objects</b> from the <i>Database Configuration</i> drop-down list<b> </b>to
display the <i>Data Objects</i> configuration page:</span></p>

<p class=MsoNormal><span style=''><br>
<br>
</span></p>

<p class=MsoNormal><b><span style='font-size:10.0pt;'><img
border=0   id="Picture 53"
src="System_Admin_Guide_files/image046.jpg"></span></b></p>


<p class=MsoNormal><span style=''><br>
Information on the page can be sorted by clicking the <i>Data Object, Friendly
Name,</i> or <i>Type</i> column headings.</span></p>


<p class=MsoNormal><span style=''>The  </span><span
style=''><img border=0  
id="Picture 54" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon indicates that more than one
action can be performed on the data object. Hover your mouse cursor over the  </span><span
style=''><img border=0  
id="Picture 55" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon to display the available
actions.</span></p>


<p class=MsoNormal><span style=''>The possible
actions for a data object are:</span></p>


<p class=MsoNormal><b><span style=''>Modify
Data Object</span></b><span style=''>   Change the
object s friendly name, description, visibility to the end-user, display order,
default format, default alignment, and hidden status. In addition, you can view
the dependencies for each column of the object. User-defined columns may also
be defined.</span></p>


<p class=MsoNormal><b><span style=''>Set
Relations</span></b><span style=''>   Define
relationships between the selected object and other data objects and view the
relationship s dependencies.</span></p>


<p class=MsoNormal><b><span style=''>Set Data
Object Access Rights</span></b><span style=''>
  Modify the data object access rights for each defined role associated with
the current reporting database.</span></p>


<p class=MsoNormal><b><span style=''>Set
Parameters</span></b><span style=''>   Specify
one or more &quot;fixed parameters&quot; for the data object. Fixed parameters
are filters applied to the data object in every occurrence of its use. </span></p>


<p class=MsoNormal><span style=''>Data objects
based on stored procedures may have input parameters. Input parameters are
established when the procedure is imported into the metadata database and may
only be removed by removing the data object. Input parameters can be edited,
implying that the value can be supplied by the end-user when a report that is
based on the procedure is executed.</span></p>


<p class=MsoNormal><b><span style=''>Set Links</span></b><span
style=''>   Configure links from columns to a
URL or report.</span></p>


<p style='line-height:120%'><b><span style=''>View
Dependencies </span></b><span style=''>  Displays
information about the content or usage of an item. This information can be very
helpful to System Administrators who need to know usage information. </span></p>



<h3><a name="_Toc457221760"></a><a name="_Toc457221124">Modifying Data Objects</a></h3>


<p class=MsoNormal><span style=''>The Modify
Data Object page allows the System Administrator to:</span></p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><span style=''>Change the
     friendly name, description, and visibility of the data object</span></li>
 <li class=MsoNormal><span style=''>Change the
     friendly name, description, default format, default alignment, and
     visibility of columns</span></li>
 <li class=MsoNormal><span style=''>Change the
     presentation order of the columns in the Report Builder</span></li>
 <li class=MsoNormal><span style=''>Manage
     user-defined columns</span></li>
 <li class=MsoNormal><span style=''>View the
     dependencies related to a column</span></li>
</ul>


<p class=MsoNormal><span style=''>Hover your
mouse cursor over the  </span><span style=''><img
border=0   id="Picture 56"
src="System_Admin_Guide_files/image026.jpg"></span><span style='
"Arial","sans-serif"'> icon to display the available actions and click the <b>Modify
Data Object</b> option. The following page will be displayed:</span></p>


<p class=MsoNormal><span style=''><img
border=0   id="Picture 57"
src="System_Admin_Guide_files/image047.jpg"></span></p>


<p class=MsoNormal><span style=''><br>
The data object s <i>Friendly Name</i> and <i>Description</i> specified here are
displayed in the Report Builder when the user is selecting the data for a
report.</span></p>


<p class=MsoNormal><span style=''>The <i>Hide
Data Object</i> checkbox specifies whether the data object is visible when the
user is selecting data for a report. System administrators can avail themselves
of other mechanisms to provide this data, such as Catalogs or Virtual Views,
and may not want users accessing the data object directly.</span></p>


<p class=MsoNormal><span style=''>A column s <i>Friendly
Name</i> and <i>Description</i> are displayed in the Report Builder when the
user is selecting data for the report. The <i>Default Format</i> and the <i>Default
Alignment</i> drop-down lists set its initial format and alignment in the
Report Builder. The <i>Hidden</i> checkbox determines whether the column is
visible through the Report Builder selection process. </span></p>


<p class=MsoNormal><span style=''>The <i>Use as
Primary Object</i> checkbox specifies whether the data object is displayed in
the initial list of data objects on the <b>Modify Data Source</b> dialog box in
the Ad Hoc Report Builder. This allows the System Administrator to exercise
more control over the initial list of data objects displayed to the user. A
typical use-case would be to remove lookup tables from the initial list of
objects.</span></p>


<p class=MsoNormal><span style=''>Columns are displayed
to the end-user in the Report Builder in the same order displayed in the <i>Modify
Data Objects</i> page. The columns can be displayed alphabetically by <i>Friendly
Name</i> by clicking the <i>Friendly Name</i> column header. They can also be
rearranged by using the drag-and-drop capability.<br>
<br>
</span></p>

<p class=MsoNormal><span style=''>Click <b>Save</b>
to save any changes.</span></p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>Drag-and-drop capability exists throughout the Ad
  Hoc interface. Mouse down on the handle <br>
  (<img border=0   id="Picture 58"
  src="System_Admin_Guide_files/image048.jpg">) at the left of each row in the
  grid, drag the row to the target location and release the mouse.<br>
  <br>
  Choose appropriate values for the Default Format to avoid undesirable results
  when generating reports.<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>If <i>HTML</i> is selected as the Default Format,
  then an end-user will not be able to change the format of the column in the
  Report Builder to something else.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>The <i>Preserve line feed</i> format allows text in a
  memo type field to be displayed as it is stored with line feeds (a.k.a.,
  carriage returns) intact. This is the default format for data types of Long
  Text (e.g., LongVarWChar).</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<h3><a name="_Toc457221761"></a><a name="_Toc457221125">Managing User-Defined
Columns</a></h3>


<p class=MsoNormal><span style=''>The System
Administrator can extend the list of columns for a data object by creating
ser-defined columns. The definition of a user-defined column must conform to
the SQL SELECT syntax for a column of the reporting DBMS. </span></p>


<p class=MsoNormal><span style=''>Typical uses
of user-defined columns include string formatting and concatenation, CASE
statements, and in-row calculations.</span></p>


<p class=MsoNormal><span style=''>To create a
user-defined column, click the <b>Add a User-defined Column</b> button. The
following dialog box will be displayed:</span></p>


<p class=MsoNormal><img border=0   id="Picture 36"
src="System_Admin_Guide_files/image049.jpg"></p>


<p class=MsoNormal><span style=''>Enter a
unique <i>Column Name</i> and the <i>Definition </i>in the controls provided.</span></p>


<p class=MsoNormal><span style=''>The <i>Definition</i>
must be a valid SQL statement. If d</span><span style=''>esign-time
validation is ON, this statement will be validated against the database to
assure it s syntactically correct.</span></p>


<p class=MsoNormal><span style=''>If the column
includes a calculation that uses an existing column, use the <i>Columns</i> tree
to select it and insert it in the <i>Definition</i>.</span></p>


<p class=MsoNormal><span style=''>Clicking one
of the <i>Operators</i> buttons will insert that operator into the <i>Definition</i>
at the last cursor position.</span></p>


<p class=MsoNormal><span style=''>Select the <i>Data
Type</i> of the expected result of the <i>Definition</i>.</span></p>


<p class=MsoNormal><span style=''>Click <b>OK</b>
to validate the <i>Definition</i> and save the result.</span></p>


<p class=MsoNormal><span style=''>Once a user-defined
column has been created, it can be changed or removed by hovering your mouse
cursor over the  </span><span style=''><img
border=0   id="Picture 60"
src="System_Admin_Guide_files/image026.jpg"></span><span style='
"Arial","sans-serif"'> icon for the user-defined column to display the
available actions and clicking either the <b>Modify Column</b> or <b>Remove
Column</b> option. These actions are <i>not</i> an option for the original
columns of the data object.</span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
on the <i>Modify Data Objects</i> page to commit the user-defined column definition
to the metadata database.</span></p>


<i><u><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></u></i>

<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<h3><a name="_Toc457221762"></a><a name="_Toc457221126">Viewing Dependencies</a></h3>


<p class=MsoNormal><span style=''>To view a
column s dependencies from the Modify Data Objects page, hover your mouse cursor
over the  </span><span style=''><img border=0
  id="Picture 61" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon to display the available actions
and click the <b>View Dependencies</b> option. The following page will be displayed:<br>
<br>
<br>
</span></p>

<p class=MsoNormal><img border=0   id="Picture 41"
src="System_Admin_Guide_files/image050.jpg"></p>


<p class=MsoNormal><i><span style=''>The
content of the page has been adjusted in order to get the image to fit on the
page.</span></i><span style=''> </span></p>


<p class=MsoNormal><span style=''>The View
Dependencies presents a report detailing where the column is used throughout
the Ad Hoc instance. This report can help System Administrators determine the
potential scope and impact of changes to a column definition.</span></p>



<h3><a name="_Toc457221763"></a><a name="_Toc457221127">Adding a Data Object
Relationship</a></h3>


<p class=MsoNormal><span style=''>The System
Administrator can define relationships between data objects. When a relationship
exists between two data objects, selection of one of the data objects will
present the end-user with the opportunity to select the related data object in
the Report Builder.</span></p>


<p class=MsoNormal><span style=''>Refer to the <i>Relationships</i>
section of this document for detailed information about creating and managing
relationships.</span></p>


<p class=MsoNormal><span style=''>To define
relationships for a data object from the Data Objects page, hover your mouse
cursor over the  </span><span style=''><img
border=0   id="Picture 64"
src="System_Admin_Guide_files/image026.jpg"></span><span style='
"Arial","sans-serif"'> icon to display the available actions and click the <b>Set
Relations</b> option. The following page will be displayed:<br>
<br>
</span></p>


<p class=MsoNormal><span style=''><img
border=0   id="Picture 65"
src="System_Admin_Guide_files/image051.jpg"></span></p>


<p class=MsoNormal><span style=''><br>
A list of relationships between the current data object and other data objects
in the database will be displayed.</span></p>


<p class=MsoNormal><span style=''>Click <b>Add </b>to
create a new relationship.</span></p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<h3><a name="_Toc457221764"></a><a name="_Toc457221128">Setting Data Object
Access Rights</a></h3>


<p class=MsoNormal><span style=''>Data Object access
rights can be set for the data object or for specific columns in the data object
for each Role associated with the current reporting database. Data Object access
rights determine which data objects and columns are restricted from use by a
Role. By default, all data objects and columns are accessible by all roles
associated with the current reporting database.</span></p>


<p class=MsoNormal><span style=''>For detailed
help setting access rights, refer to <i>Setting Data Object Access Rights</i>
in the <i>Roles</i> section of this document.</span></p>


<p class=MsoNormal><span style=''>To set the
access rights for a data object from the Data Objects page, hover your mouse cursor
over the  </span><span style=''><img border=0
  id="Picture 66" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon to display the available actions
and click the <b>Set Data Object Access Rights</b> option. The following page
will be displayed:</span></p>



<p class=MsoNormal><span style=''><img
border=0   id="Picture 67"
src="System_Admin_Guide_files/image052.jpg"></span><span style='
"Arial","sans-serif"'><br>
<br>
</span></p>


<p class=MsoNormal><span style=''>Access rights
for the object specified in the <i>Selected Data Object</i> list can be set to Full
or None for a role by selecting the role with the checkboxes and clicking <b>Set
to Full</b> or <b>Set to None</b>.</span></p>


<p class=MsoNormal><span style=''>Alternatively,
click the </span><span style=''><img border=0
  id="Picture 68" src="System_Admin_Guide_files/image008.gif"
alt=iconAction></span><span style=''> icon to
display the <b>Column Access Rights</b> dialog box and set the access at the
column level.</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<h3><a name="_Toc457221765"></a><a name="_Toc457221129"></a><a
name="_Ref169938485">Specifying Fixed Parameters</a></h3>


<p class=MsoNormal><span style=''>Permanent
filters can be applied to a data object. These filters are called  fixed parameters 
and are transparent to the end-user. Additional parameters can be applied as
the report is built and executed, but the end-user cannot modify a fixed parameter
on the data object. </span></p>


<p class=MsoNormal><span style=''>To define and
manage fixed parameters for a data object, hover your mouse cursor over the  </span><span
style=''><img border=0  
id="Picture 69" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon to display the available actions
and click the <b>Set Parameters</b> option. The following page will be displayed:</span></p>



<p class=MsoNormal><span style=''><img
border=0   id="Picture 70"
src="System_Admin_Guide_files/image053.jpg"></span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>A fixed parameter
takes the form of an equation similar to:</span></p>


<p class=MsoFooter align=center style='text-align:center'><i>Label </i>is <i>Compared
</i>to <i>Value</i></p>


<p class=MsoFooter>where <i>Label</i> represents a column name, <i>Compared </i>represents
a comparison operator, and <i>Value</i> represents a threshold.</p>


<p class=MsoFooter>The available comparison operators are:</p>


<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=420 valign=top style='width:251.75pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal><span style=''>Equal to</span></li>
   <li class=MsoNormal><span style=''>Not equal
       to</span></li>
   <li class=MsoNormal><span style=''>Less than</span></li>
   <li class=MsoNormal><span style=''>Greater
       than</span></li>
   <li class=MsoNormal><span style=''>Less than
       or equal to</span></li>
   <li class=MsoNormal><span style=''>Greater
       than or equal to</span></li>
   <li class=MsoNormal><span style=''>Starts
       with <sup>1</sup></span></li>
   <li class=MsoNormal><span style=''>Does not
       start with <sup>1</sup></span></li>
   <li class=MsoNormal><span style=''>Ends with
       <sup>1</sup></span></li>
   <li class=MsoNormal><span style=''>Does not
       end with <sup>1</sup></span></li>
  </ul>
  </td>
  <td width=420 valign=top style='width:251.75pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal><span style=''>Contains <sup>1</sup></span></li>
   <li class=MsoNormal><span style=''>Does not
       contain <sup>1</sup></span></li>
   <li class=MsoNormal><span style=''>Is null</span></li>
   <li class=MsoNormal><span style=''>Is not
       null</span></li>
   <li class=MsoNormal><span style=''>Between</span></li>
   <li class=MsoNormal><span style=''>Not
       between</span></li>
   <li class=MsoNormal><span style=''>In list</span></li>
   <li class=MsoNormal><span style=''>Not In
       List</span></li>
   <li class=MsoNormal><span style=''>Like <sup>1</sup></span></li>
   <li class=MsoNormal><span style=''>Not Like <sup>1</sup></span></li>
  </ul>
  </td>
 </tr>
</table>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>1. These operators are only available for data type
  of type String or Text.</p>
  <p class=NotesCxSpLast>2. The operators available are dependent upon the
  column's data type. For example, a numeric data type would not include
  operators such as true/false.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<h3><a name="_Toc457221766"></a><a name="_Toc457221130">Managing Fixed
Parameters</a></h3>


<p class=MsoNormal><span style=''>Click the <b>Add</b>
button to define a fixed parameter. The following dialog box will be displayed:</span></p>


<p class=MsoNormal><img border=0   id="Picture 33"
src="System_Admin_Guide_files/image054.jpg"></p>

<p class=MsoNormal><span style=''>If fixed
parameters have been previously defined, the option to specify an And/Or
condition will also be displayed.</span></p>


<p class=MsoNormal><span style=''>Select the <i>Column</i>
from the drop-down list, an <i>Operator</i> from the drop-down list, and either
provide a <i>Value</i> manually or use the magnifying glass to select a <i>Value</i>
from the reporting database.</span></p>


<p class=MsoNormal><span style=''>Click <b>OK</b>
to save the fixed parameter information temporarily.</span></p>


<p class=MsoNormal><span style=''>After adding
all of the fixed parameters, click <b>Save</b> on the Parameters page to save
the information in the metadata database.</span></p>


<p class=MsoNormal><span style=''>To edit or
remove a fixed parameter, hover your mouse cursor over the  </span><span
style=''><img border=0  
id="Picture 72" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon to display the available actions
and click the <b>Edit Parameter</b> or<b> Remove Parameter</b> option. </span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>1. If the <i>In list</i> or <i>Not in list</i>
  operator is selected, then more than one value can be specified. If manually entering
  each value, follow each entry by pressing the Enter key.</p>
  <p class=NotesCxSpMiddle>2. If the value is a number, the Value field must
  contain a valid number to build the report.</p>
  <p class=NotesCxSpLast>3. The Parameter functionality does not support
  conditions for Time data types. Date/Time data types are supported but their
  time portion will be ignored.<br clear=all style='page-break-before:always'>
  </p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #FFFFCC 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#FFFFCC'>
  <p class=HintCxSpFirst><b>Hint:</b></p>
  <p class=HintCxSpLast>When adding multiple parameters, a logical operator
  (And or Or) becomes available for selection at the beginning of the next
  parameter. Use this operator to set the cumulative conditions for the
  parameters.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<p class=MsoNormal><span style=''>If the report
contains two or more parameters, the </span><span style=''><img
border=0   id="Picture 73"
src="System_Admin_Guide_files/image055.gif" alt=adf-icon></span><span
style=''> icon appears for each additional parameter.
The directional pad gives you the ability to create <i>levels </i>for each
parameter. Control the order of evaluation for multiple parameters using the
directional pad.</span></p>


<p class=MsoNormal><span style=''>Advanced data
filtering<i> </i>makes it possible to define groups of parameters that work
together to filter undesirable data. Multiple parameters can be defined to control
the order of evaluation. Filter data to control what users see at runtime.</span></p>


<p class=MsoNormal><span style=''>Fixed
parameters on data objects give administrators the ability to control the content
seen by end-users. Filter extraneous data by defining one or more parameters
that are evaluated at runtime. The directional pad control (</span><span
style=''><img border=0  
id="Picture 74" src="System_Admin_Guide_files/image055.gif"
alt=paramControlPad></span><span style=''>)
enables administrators the ability to control the order of evaluation.</span></p>


<p class=MsoNormal><span style=''>The
individual arrows of the control perform the following functions:</span></p>


<p class=MsoNormal style='text-indent:.5in'><span style=''><img
border=0   id="Picture 75"
src="System_Admin_Guide_files/image056.gif" alt=paramUp></span><span
style=''> Shifts a parameter one position
higher in the list (retains indentation)</span></p>

<p class=MsoNormal style='text-indent:.5in'><span style=''><img
border=0   id="Picture 76"
src="System_Admin_Guide_files/image057.gif" alt=paramDown></span><span
style=''> Shifts a parameter one position lower
in the list (retains indentation)</span></p>

<p class=MsoNormal style='text-indent:.5in'><span style=''><img
border=0   id="Picture 77"
src="System_Admin_Guide_files/image058.gif" alt=paramLeft></span><span
style=''> Indents a parameter one position left</span></p>

<p class=MsoNormal style='text-indent:.5in'><img border=0  
id="Picture 78" src="System_Admin_Guide_files/image059.gif" alt=paramRight><span
style=''> Indents a parameter one position
right</span></p>

<p class=MsoNormal style='text-indent:.5in'><span style=''>&nbsp;</span></p>

<p class=MsoNormal><span style=''>Parameters
indented furthest to the <i>right</i> are evaluated first.</span></p>



<h3><a name="_Toc457221767"></a><a name="_Toc457221131">Using Session
Parameters</a></h3>


<p class=MsoNormal><span style=''>If session
parameters have been defined for the Ad Hoc instance, the Fixed Parameters Details
dialog box may appear slightly differently:</span></p>



<p class=MsoNormal><img border=0   id="Picture 39"
src="System_Admin_Guide_files/image060.jpg"></p>


<p class=MsoNormal><span style=''>Notice that
there is a <i>Value</i> source drop-down list in the example above. This drop-down
list is shown when there can be session parameters used as part of the filter.
The source is typically a  Specific Value ; however, if session parameters have
been defined,  Session Parameter  can be selected as the <i>Value</i> source.
When  Session Parameter  is selected, a drop-down list of relevant session
parameters is displayed.</span></p>


<p class=MsoNormal><span style=''>Session
parameters are one of five types; date, number, numeric list, text or textual
list. The drop-down list of session parameters will contain the session
parameters that match the data type of the <i>Column</i>. The list is also
restricted by the <i>Operator</i> selected.</span></p>


<p class=MsoNormal><span style=''>For date <i>Columns</i>,
the date session parameters will be shown in the list of available session
parameters.</span></p>


<p class=MsoNormal><span style=''>For numeric <i>Columns</i>,
either the number or numeric list session parameters will be shown in the list
of available session parameters. If the <i>Operator</i> is set to <i>In list</i>
or <i>Not in list</i>, the numeric list session parameters will be shown,
otherwise the number session parameters will be shown.</span></p>


<p class=MsoNormal><span style=''>For text <i>Columns</i>,
either the text or textual list session parameters will be shown in the list of
available session parameters. If the <i>Operator</i> is set to <i>In list</i>
or <i>Not in list</i>, the textual list session parameters will be shown,
otherwise the text session parameters will be shown.</span></p>


<p class=MsoNormal><span style=''>In older
versions of Logi Ad Hoc, session parameters could also be used as the basis for
a fixed parameter on a data object as shown below:</span></p>



<p class=MsoNormal><img border=0   id="Picture 40"
src="System_Admin_Guide_files/image061.jpg"></p>



<p class=MsoNormal><span style=''>Notice that
the <i>Value</i> source is  Specific Value  and that a @Session token has been
supplied as the <i>Value</i>. While this technique is still supported in Logi
Ad Hoc, @Session tokens require a specific syntax and the session parameter
specified must match the session parameter name exactly, including case. There
is no type checking of the value and the SQL syntax generated may not be
appropriate in all cases.</span></p>


<p class=MsoNormal><span style=''>The preferred
method for using session parameters as the basis for a fixed parameter on a data
object is to select  Session Parameter  as the <i>Value</i> source and selecting
the appropriate session parameter from the available list.</span></p>

<h5 align=left style='text-align:left'><span style='text-decoration:none'>&nbsp;</span></h5>

<h5 align=left style='text-align:left'><i><span style='font-weight:normal'><span
 style='text-decoration:none'>&nbsp;</span></span></i></h5>

<h3><a name="_Toc457221768"></a><a name="_Toc457221132">Configuring Input
Parameters</a></h3>


<p class=MsoNormal><span style=''>Data objects
based on stored procedures and functions may require input parameters. Input
parameters are simply values that the procedure needs in order to execute
properly.</span></p>


<p class=MsoNormal><span style=''>Since the
logic associated with the usage of the input parameters is contained within the
procedure definition, no operators can be specified for a parameter. The <b>Parameter
Details</b> dialog box for an input parameter appears as:</span></p>


<p class=MsoNormal><img border=0   id="Picture 42"
src="System_Admin_Guide_files/image062.jpg"></p>


<p class=MsoNormal><span style=''>The <i>Friendly
Name</i> that is displayed to the end-user can be adjusted.</span></p>


<p class=MsoNormal><span style=''>The <i>Editable</i>
checkbox indicates that the parameter value will be obtained through an automatic
report filter. When the data object that has editable input parameters is
selected for use in a report, the <b>Modify Data Source</b> dialog box<b>
Filter</b> tab will have associated filters automatically created. See the <i>Filter</i>
description in the <i>Report Design Guide</i>.</span></p>


<p class=MsoNormal><span style=''>The <i>Value</i>
can be set as a <i>Specific Value</i> or a <i>Session Parameter</i> can be
used. To enter a specific value, select <i>Specific Value</i> from the drop-down
list and enter the value in the space provided. To use a session parameter for
the <i>Value</i>, select <i>Session Parameter</i> from the drop-down list and
select one of the session parameters from the provided list.</span></p>


<p class=MsoNormal><span style=''>There are no visible
indications of the proper format for an input parameter nor are there
mechanisms to provide a list of viable options for the value.</span></p>



<h3><a name="_Toc457221769"></a><a name="_Toc457221133"></a><a
name="_Ref169946335">Setting Links</a></h3>


<p class=MsoNormal><span style=''>System
administrators can create automatic hyperlinks for each record in a specified
column. End-users can click these links to open other web pages or drill
through to different reports. End-users also have the option of enabling or disabling
the links when building reports.</span></p>


<p class=MsoNormal><span style=''>To define and
manage links from a data object, hover your mouse cursor over the  </span><span
style=''><img border=0  
id="Picture 82" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon to display the available actions
and click the <b>Set Links</b> option. The following page will be displayed:</span></p>


<p class=MsoNormal><span style=''><img
border=0   id="Picture 83"
src="System_Admin_Guide_files/image063.jpg"></span><span style='
"Arial","sans-serif"'><br>
<br>
</span></p>


<p class=MsoNormal><span style=''>The list contains
the columns that are candidates for linking to either a linked report or a URL.
A column can only have <i>one</i> link defined.</span></p>


<p class=MsoNormal><span style=''>From the list
of columns for the data objects, hover your mouse cursor over the  </span><span
style=''><img border=0  
id="Picture 84" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon to display the available actions,
which are <b>Add Link</b> or <b>Edit Link</b> and <b>Remove Link. </b>The
following dialog box will be displayed:</span></p>


<p class=MsoNormal><img border=0   id="Picture 43"
src="System_Admin_Guide_files/image064.jpg"></p>

<p class=MsoNormal><span style=''>When
configuring a link, administrators must first determine if the link leads to an
Ad Hoc report or an external URL. </span></p>


<p class=MsoNormal><span style=''>To create an
object link to a URL, select <i>Link to Any URL</i> and enter the URL. Click <b>Test
URL</b> to verify it. </span></p>


<p class=MsoNormal><span style=''>To create
link parameters, click <b>Add a Link Parameter</b> and enter in the parameter
name. Choose the column that will be the source of the data for the parameter
from the drop-down list. </span></p>


<p class=MsoNormal><span style=''>Click <b>OK</b>
to save the information.</span></p>


<p class=MsoNormal><span style=''>To create a
link to an existing Ad Hoc report, select <i>Link to an Ad Hoc Report</i> and
the dialog box will be re-displayed:<br>
<br>
</span></p>


<p class=MsoNormal><img border=0   id="Picture 51"
src="System_Admin_Guide_files/image065.jpg"></p>


<p class=MsoNormal><span style=''>Click the </span><span
style=''><img border=0  
id="Picture 88" src="System_Admin_Guide_files/image066.gif" alt=iconGlass></span><span
style=''> icon to locate and choose a report
from the list (shown above). After selecting the report, click <b>OK</b>. </span></p>


<p class=MsoNormal><span style=''>To create
link parameters, click <b>Add a Link Parameter</b> and click the </span><span
style=''><img border=0  
id="Picture 89" src="System_Admin_Guide_files/image066.gif" alt=iconGlass></span><span
style=''> icon to select the target parameter.
Choose the column that will be the source of the data for the parameter from
the drop-down list. </span></p>

<p class=MsoNormal><span style=''>Click <b>OK</b>
to save the information.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Linked reports must have at least one parameter to
  enable drill-through functionality between two reports where the content of
  the linked report is dependent upon data from the parent report. Links to a
  static report do not require link parameters. Refer to the Linking Reports section
  of this guide for more information about linked reports.</p>
  </div>
  </td>
 </tr>
</table>

</div>


<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<p class=MsoNormal><i><span style=''>Viewing
Dependencies</span></i></p>


<p class=MsoNormal><span style=''>To view the
dependencies for a data object from the <i>Data Objects</i> page, hover your
mouse cursor over the  </span><span style=''><img
border=0   id="Picture 90"
src="System_Admin_Guide_files/image026.jpg"></span><span style='
"Arial","sans-serif"'> icon to display the available actions and click the <b>View
Dependencies</b> option. The following page will be displayed:</span></p>


<p class=MsoNormal><img border=0   id="Picture 59"
src="System_Admin_Guide_files/image067.jpg"></p>

<p class=MsoNormal><span style=''>The option to
view dependencies of a data object may help System Administrators know the
scope and impact of changes to a data object.</span></p>



</body>
</html>
