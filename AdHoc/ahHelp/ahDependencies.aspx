<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945140">View Dependencies</a></h2>


<p class=MsoNormal>Ad Hoc allows the user to view report dependencies. The
broad dependency categories are:</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Firm dependencies   Items upon which the report depends.</li>
 <li class=MsoNormal>Loose dependencies   Items that can be adjusted and still
     allow the report to run.</li>
 <li class=MsoNormal>Firm relations   Items that depend on the presence of the
     report.</li>
 <li class=MsoNormal>Loose relations   Items that may need the report to be
     present.</li>
</ul>


<p class=MsoNormal>To view the dependencies report, hover your mouse cursor
over a report s <b>More</b> button and select <i>View Dependencies</i> from its
list. </p>


<p class=MsoNormal>Here s an example of a fully collapsed view of a dependency
page:</p>


<p class=MsoNormal><img border=0   id="Picture 275"
src="Report_Design_Guide_files/image216.jpg"></p>



<p class=MsoNormal>Click the <img border=0   id="Picture 276"
src="Report_Design_Guide_files/image217.jpg" alt="expand_blue"> or the <img
border=0 width=13 height=13 id="Picture 277"
src="Report_Design_Guide_files/image218.jpg" alt="collapse_blue"> icons to
expand or collapse a report section.</p>


<p class=MsoNormal><br>
<br>
</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>



</body>
</html>
