<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h1>Dashboards</h1>



<p class=MsoNormal>Dashboards are created by adding and configuring dashboard
panels. A panel contains one of the following content types:</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><b>Pre-defined reports</b> - Canned reports that provide a
     quick view of a user's most <i>frequently-viewed </i>and most<i> recently-viewed
     </i>reports.<i><br>
     <br>
     </i></li>
 <li class=MsoNormal><b>Favorites</b> - A user-specified list of reports
     contained within a folder accessible from the My Personal Reports page.<br>
     <br>
 </li>
 <li class=MsoNormal><b>URL - </b>Any valid URL the user can access.<br>
     <br>
 </li>
 <li class=MsoNormal><b>Reports</b> - A user-specified report accessible from
     the My Personal Reports or Shared Reports pages.<br>
     <br>
 </li>
</ul>


<p class=MsoNormal>Panels can be organized into columns in the dashboard. A
column can contain one or many panels but, at a minimum, a dashboard must
contain at least one panel in order to be saved.</p>




<p class=MsoNormal><img border=0   id="Picture 15"
src="Report_Design_Guide_files/image219.jpg"></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The Dashboard Builder Panel
Interface</span></p>


<p class=MsoNormal><br>
The <b>Create New Dashboard</b> interface is shown above, with a few panels
already defined. </p>

<p class=MsoNormal><img border=0   id="Picture 18"
src="Report_Design_Guide_files/image220.jpg"></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The Dashboard Panel
definition dialog box</span></p>



<p class=MsoNormal><b>To add a dashboard panel:</b></p>


<p class=MsoNormal>An unlimited number of panels can be added to a dashboard.
The panels can be configured to appear in the dashboard layout using panel
settings and the panel drag feature controls provided.</p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>Add
</b>to view the Panel Settings dialog box.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Enter a <i>Name</i>
and <i>Description</i> for the panel in the fields provided.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Select an <i>Initial
Column</i> value to specify where the panel should initially appear in the
dashboard. The default value is 1 and the available values will depend on the
number of columns configured in the dashboard settings.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Specify
the <i>Initial Display</i> setting based on: </p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span>Yes - display the panel at initial viewing of the dashboard
(default).</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span>No - do not display the panel at initial viewing of the
dashboard. If this setting is chosen the user can add the panel later via the
Change Dashboard option within the dashboard viewer.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Specify the
panel <i>Height</i>, <i>Minimum Height</i>, and <i>Minimum Width</i>, if
necessary. If left blank, Ad Hoc will set an automatic Height.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>6.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Select the
<i>Panel Content</i> type (default is <i>Report</i>).<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>7.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Specify
the report, folder, or URL in the field provided:<br>
<br>
</p>

<p class=MsoNormal style='margin-left:58.5pt;text-indent:-.25in'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>For <i>Pre-defined
Reports</i>, select one of the listed reports and click <b>OK</b>.</p>

<p class=MsoNormal style='margin-left:58.5pt;text-indent:-.25in'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>For <i>Favorite
Reports</i>, locate and select a <i>Personal Reports</i> folder and click <b>OK</b>.</p>

<p class=MsoNormal style='margin-left:58.5pt;text-indent:-.25in'>c.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>For <i>URL</i>,
specify a complete address. <span style='color:black'>Click <b>Test URL</b> to
confirm that the URL can be viewed.</span></p>

<p class=MsoNormal style='margin-left:58.5pt;text-indent:-.25in'>d.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>For <i>Report</i>,
locate and select a report located in the <i>Personal Reports</i> or <i>Shared
Reports</i> folder.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>8.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>Save
Panel </b>to save the panel and return to the Dashboard Builder interface.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>9.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>If the
dashboard has not been previously saved, then enter a name for the dashboard in
the field provided.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>10.<span
style='font:7.0pt "Times New Roman"'> </span>Rearrange panels as necessary with
the up/down arrows<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>11.<span
style='font:7.0pt "Times New Roman"'> </span>Click <b>Save </b>to save the
dashboard definition.<br>
<br>
</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #FFFFCC 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#FFFFCC'>
  <p class=HintCxSpFirst><b>Hint:</b></p>
  <p class=HintCxSpLast>The dashboard can be previewed at any time by clicking <b>Preview
  Dashboard</b>, as long as the Panel Settings panel is not displayed.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal><b>To modify a dashboard panel:</b></p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Hover your
mouse cursor over the <img border=0   id="Picture 280"
src="Report_Design_Guide_files/image221.jpg"> icon for the panel and select <i>Modify
Dashboard Panel</i> from its drop-down list. <br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Modify the
panel as desired.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>Save
Panel </b>to save your changes and return to the Dashboard Builder interface.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>If the
dashboard has not been previously saved, then enter a name for the dashboard in
the field provided.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>Save</b>
to save the dashboard definition<b>.</b></p>



<p class=MsoNormal> </p>

<p class=MsoNormal><b>To delete one or more dashboard panels:</b></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>Check the desired panels  checkboxes.<br>
     <br>
 </li>
 <li class=MsoNormal>Click <b>Delete</b>.<br>
     <br>
 </li>
 <li class=MsoNormal>Click <b>OK </b>to confirm the removal when prompted.<br>
     <br>
 </li>
 <li class=MsoNormal>If the dashboard has not been previously saved, then enter
     a name for the dashboard in the field provided.<br>
     <br>
 </li>
 <li class=MsoNormal>Click <b>Save</b> to save the dashboard definition<b>.</b></li>
</ol>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>At least one dashboard panel must be defned in order
  to be able to save the dashboard.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>




<p class=MsoNormal><b>To modify a Dashboard's Settings:</b></p>


<p class=MsoNormal>Click the <img border=0   id="Picture 281"
src="Report_Design_Guide_files/image217.jpg" alt="expand_blue"> icon to expand
the top area of the page to show the dashboard settings, which and allow the general
dashboard appearance to be customized:</p>


<p class=MsoNormal><img border=0   id="Picture 29"
src="Report_Design_Guide_files/image222.jpg"></p>



<p class=MsoNormal>The <i>Dashboard Settings</i> area, provides the ability to
configure a dashboard's general appearance and provide an optional description.
A panel's appearance is controlled by the selected dashboard <i>Style</i>,
which gives the dashboard a specific &quot;look and feel&quot;. The available dashboard
styles are:</p>


<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=280 valign=top style='width:167.8pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>   (None)</p>
  <p class=MsoNormal>   BlackPearl</p>
  <p class=MsoNormal>   Clarity</p>
  <p class=MsoNormal>   Classic</p>
  <p class=MsoNormal>   Gray</p>
  <p class=MsoNormal>   Harmony</p>
  </td>
  <td width=280 valign=top style='width:167.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>   LemonLime</p>
  <p class=MsoNormal>   Light</p>
  <p class=MsoNormal>   Mocha</p>
  <p class=MsoNormal>   Nature</p>
  <p class=MsoNormal>   Ocean</p>
  <p class=MsoNormal>   Professional</p>
  </td>
  <td width=280 valign=top style='width:167.85pt;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>   RedWine</p>
  <p class=MsoNormal>   Silver</p>
  <p class=MsoNormal>   Technology</p>
  <p class=MsoNormal>   Transit</p>
  <p class=MsoNormal>   Tropical</p>
  </td>
 </tr>
</table>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst>Notes:</p>
  <p class=NotesCxSpMiddle>The system administrator may add additional
  Dashboard Styles to this list.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>The Dashboard Style will not control the appearance of
  any of the reports viewed in a panel.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<p class=MsoNormal>Specify or modify the<i> Description, Dashboard Expiration
Date</i> and <i>Header</i> content.</p>


<p class=MsoNormal>The <i>Dashboard Body</i> properties control the initial
functional behavior of the dashboard. </p>


<p class=MsoNormal>When checked, the <i>Free-Form Layout</i> property displays
the panels in a non-columnar layout that allows you to reposition the panels
anywhere on the page at runtime.</p>


<p class=MsoNormal>The <i>Dashboard Adjustable</i> property determines whether
panel positions can be adjusted by the end-user.</p>


<p class=MsoNormal>The <i>Dashboard Tabs</i> property determines whether the end-user
can rearrange the panels into different tabs.</p>


<p class=MsoNormal>The <i>Dashboard Columns</i> property sets the lattice
framework for the panels. This attribute is disabled if the Free-Form Layout property
is checked.</p>


<p class=MsoNormal>Click the  <img border=0   id="Picture 283"
src="Report_Design_Guide_files/image218.jpg" alt="collapse_blue">  icon to collapse/hide
the Dashboard Settings panel.</p>


<p class=MsoNormal>Click <b>Save</b> to save any changes made.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>The <i>Description</i> text appears under the
  dashboard's name in the Reports list in the Personal Reports and Shared
  Reports areas.</p>
  </div>
  </td>
 </tr>
</table>

</div>


<span style='font-size:18.0pt;'><br clear=all
style='page-break-before:always'>
</span>



</body>
</html>
