<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945137"></a><a name="_Toc382291632">Sharing Reports</a></h2>


<p class=MsoNormal>Reports can be copied to the <i>Shared Reports </i>area for
all users to view.</p>


<p class=MsoNormal>Copying reports typically involves selecting the reports to
be copied and clicking <b>Copy</b>. Single reports can be copied by clicking on
the <i>Copy</i> action from the list of available actions for a report.</p>


<p class=MsoNormal>Enter a <i>New Report Name</i> and select a <i>Destination Folder
Type</i> of  Shared Reports<i> </i>. The option to select a folder within the
Shared Reports area may be displayed.</p>


<p class=MsoNormal>Click <b>Save</b> to save the reports in the new
destination.</p>




<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>When a report is copied, the report's name will be
  prefixed with Copy of (#) in order to distinguish them from the original. The
  &quot;(#)&quot; will only occur after a report is copied more than once.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>An Expiration Date is used to designate when a time
  sensitive report is deemed obsolete. The date must be a date greater than the
  current date.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpLast>Any corresponding report archives are not copied with
  the report.</p>
  </div>
  </td>
 </tr>
</table>

</div>


<b><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></b>



</body>
</html>
