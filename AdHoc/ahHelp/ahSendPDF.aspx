<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945130">Send Report By Email</a></h2>


<p class=MsoNormal><i>Send Report by Email </i>opens a form where the email can
be composed and recipients specified:</p>



<p class=MsoNormal><img border=0   id="Picture 240"
src="Report_Design_Guide_files/image189.jpg"></p>



<p class=MsoNormal>Manually enter an email recipient(s) or click t<span
style='color:black'>he respective </span><span style='color:black'><img
border=0 width=16 height=16 id="Picture 241"
src="Report_Design_Guide_files/image053.gif" alt=iconFind></span><span
style='color:black'> icon to view and select from a list of application users.
Specify a Subject and message.</span> When done, click <b>Send</b> to send the
email the report attached. Click <b>Cancel</b> to cancel this export action.</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>The 'From' email address is defaulted to the email
  address specified in the logged in user's Profile.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>All fields except for Cc and Bcc are required.</p>
  </div>
  </td>
 </tr>
</table>

</div>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
