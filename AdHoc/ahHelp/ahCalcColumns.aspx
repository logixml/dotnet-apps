<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945106"></a><a name="_Toc382291603">Calculated Columns</a></h2>


<p class=MsoNormal><i>Calculated Columns </i>offer the ability to create new
columns for the report based on a specified formula applied to data from
existing columns.</p>


<p class=MsoNormal>In addition to the availability of data source columns,
customized columns can be created that consist of calculations performed on data
from other columns in the report. Calculations are performed with date, numeric
and non-numeric data types. </p>


<p class=MsoNormal>The operands of the formula are either constants or the
names of existing data columns included in the report. Users can create
formulas from the six provided operators, use the predefined functions or
utilize any SQL function supported by the selected report database.</p>


<p class=MsoNormal><img border=0   id="Picture 322"
src="Report_Design_Guide_files/image068.jpg"></p>


<p class=MsoNormal><br>
The goal of the calculated column process is to create and name a <i>Definition</i>
that the reporting database can interpret and return data. Consequently, the <i>Definition</i>
must conform to the reporting DBMS SQL syntax rules for a column.</p>


<p class=MsoNormal>Though they make life easier, you aren t required to use the
helpful controls to create the <i>Definition</i>. Knowledgeable users can
simply enter the definition in the text box. We highly recommend, however, that
columns be inserted into the <i>Definition</i> by clicking on the column from
the data object/column tree. The column reference will be placed at the last cursor
position in the <i>Definition</i> textbox.</p>


<p class=MsoNormal>In the upper left corner of the <i>Calculated Columns</i>
tab are the most common functions that are used in the definition of a
calculated column.</p>


<p class=MsoNormal><img border=0   id="Picture 68"
src="Report_Design_Guide_files/image069.jpg"></p>


<p class=MsoNormal>The functions are categorized according to the generic data
types of columns; date, text, and numeric data. The drop-down lists may be
viewed by hovering the mouse over the buttons. Click the function to insert the
reference into the <i>Definition</i> textbox. For the functions requiring
additional arguments or information, a dialog box will be displayed to complete
the function. For example, the <i>Text/Concatenate</i> function will display
the following dialog box:</p>



<p class=MsoNormal><img border=0   id="Picture 323"
src="Report_Design_Guide_files/image070.jpg"></p>


<p class=MsoNormal>To complete this particular dialog box, click the <i>String1</i>
text box and then click on a column. The column reference will be placed into
the text box. Only the columns relevant to the function type will be displayed
for selection. Repeat the process for <i>String2</i> and click on <b>OK</b> to
post the function into the <i>Definition</i> text box. The other function dialog
boxes behave similarly.</p>


<p class=MsoNormal>On the left side of the <i>Calculated Columns</i> tab of the
<i>Select or Modify Data Source</i> dialog box is the data objects/columns
tree. To place the column reference into the <i>Definition</i>, click the
column.</p>


<p class=MsoNormal>After a calculated column has been defined, the data
objects/columns tree will be refreshed and the calculated column will be added
to the tree along with two management actions: </p>


<p class=MsoNormal>Click the <img border=0   id="Picture 70"
src="Report_Design_Guide_files/image071.gif" alt=iconAction> icon to edit the
calculated column definition. Click the <img border=0  
id="Picture 71" src="Report_Design_Guide_files/image072.gif" alt=TinyRemove> icon
to remove the calculated column.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>Beneath the <i>Definition</i> area of the <i>Calculated
Columns</i> tab are operators that may be added to the <i>Definition</i>:</p>


<p class=MsoNormal><img border=0   id="Picture 72"
src="Report_Design_Guide_files/image073.jpg"></p>


<p class=MsoNormal>Clicking on any of the operators will place the symbol in
the <i>Definition</i> at the last cursor position. </p>


<p class=MsoNormal>The <i>Definition</i> text box is the actual work area. It can
be populated from the functions, columns, and operators available or you can
enter the calculated column definition directly by typing it into the textbox.</p>


<p class=MsoNormal>The <b>Test</b> button will verify that the calculated
column definition meets the syntax rules for the reporting DBMS. If so, a
mini-report displaying the calculated column and the underlying data from the
reporting database will be shown.</p>


<p class=MsoNormal>Ad Hoc must know the data type of the resultant calculated
column. In most cases the data type can be accurately determined from the data
type of the underlying columns or calculation. The <i>Automatic</i> option
allows Ad Hoc to use the implied data type. To provide the specific data type
or override the implied data type, either select the data type from the <i>Type</i>
drop-down list or click the <b>Determine Type</b> button.</p>


<p class=MsoNormal>Enter the <i>Name</i> of the calculated column in the text box
provided.</p>


<p class=MsoNormal>The <b>Preview Selected Data</b> button will display a
report of all of the columns, including the defined calculated column in the <i>Selected
Data Preview</i> dialog box. From this report dialog box, clicking <b>Select
and Continue</b> will save the current calculated column definition and dismiss
all of the dialog boxes.</p>


<p class=MsoNormal>The <b>Save</b> button verifies the <i>Definition</i>,
stores the <i>Name</i> and <i>Definition</i> temporarily, clears the <i>Definition</i>
textbox and updates the data object/column tree.</p>


<p class=MsoNormal>The <b>Clear</b> button erases the contents of the <i>Definition</i>
textbox.</p>


<p class=MsoNormal>The <b>New</b> button clears the contents of the <i>Definition</i>
textbox and resets the Name. If the existing <i>Definition</i> has not been
saved, a confirmation dialog box will be shown.</p>


<p class=MsoNormal>The <b>Cancel</b> button will discard any changes made in
the <i>Select or Modify Data Source</i> dialog box and dismiss the dialog box.</p>


<p class=MsoNormal>Click <b>OK</b> to save all of the changes made in the <i>Select
or Modify Data Source</i> dialog box. The newly defined calculated columns are
then available for selection in the Report Builder.</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>A calculated column must be used in the report for the
  column to be saved in the report definition. Unused calculated columns are
  automatically removed from the report definition when the report is saved.
  This includes intermediate saves of the report definition.</p>
  </div>
  </td>
 </tr>
</table>

</div>




</body>
</html>
