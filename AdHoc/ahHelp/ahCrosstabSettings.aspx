<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945115">Crosstab Settings</a></h2>


<p class=MsoNormal>The Crosstab Settings tab offers the ability to specify a
crosstab table title and add and configure interactive paging controls for
large result sets.</p>



<p class=MsoNormal>To modify a crosstab's settings:</p>


<p class=MsoNormal>Click the <i>Crosstab Settings</i> tab.</p>


<p class=MsoNormal><img border=0   id="Picture 173"
src="Report_Design_Guide_files/image140.jpg"></p>


<p class=MsoNormal>The <i>Title</i> will be displayed above the display table
in the generated report.</p>


<p class=MsoNormal>The <i>Paging Style</i> drop-down list will display the
range of paging options. The default option is  Interactive Paging . Select
 None  to remove the paging controls from a report.</p>

<p class=MsoNormal> </p>

<p class=MsoNormal>The <i>Rows Per Page</i> and <i>Rows Per Sub-Report Page</i>
options set the number of rows displayed on the display table and drill
reports, respectively. </p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
