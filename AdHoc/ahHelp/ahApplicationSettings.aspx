<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221796"></a><a name="_Toc457221160">Application Settings</a></h2>


<p class=MsoNormal><span style=''>The <i>Application
Settings</i> page allows the administrator to set general application and
Report Builder settings. All application setting modifications will take affect
with the next user login session.</span></p>


<p class=MsoNormal><span style=''>Select <b>Application
Settings</b> from the <i>Application Configuration</i> drop-down list<b> </b>to
display the <i>Application Settings</i> configuration page.</span></p>


<p class=MsoNormal><span style=''>Although it s
displayed in a single page in Ad Hoc, the <i>Application Settings</i> page shown
here is divided into separate sections for improved clarity.</span></p>

<p class=MsoNormal><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></p>

<p class=MsoNormal><span style=''>Click the </span><span
style=''><img border=0  
id="Picture 146" src="System_Admin_Guide_files/image097.jpg"></span><span
style=''> icon to display brief help text about
any attribute.</span></p>

<u><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></u>

<p class=MsoNormal><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></p>

<p class=MsoNormal><i><span style=''>General
application settings</span></i></p>



<p class=MsoNormal><span style=''><img
border=0   id="Picture 147"
src="System_Admin_Guide_files/image099.jpg"></span></p>



<p class=MsoNormal><i><span style=''>Rows per
page</span></i><span style=''>   Specifies
whether paging is performed on the grids in the user interface and how many
rows will appear per page. A value greater than <b>0</b> turns the paging
feature on and specifies the number of row per page. A value of <b>0</b> turns
paging off and all rows are displayed in one list.</span></p>


<p class=MsoNormal><i><span style=''>List
Search Options</span></i><span style=''> -
Specifies whether or not to include a search filter in pages with grids. If the
 Show only   option is selected, specify a value between 1 and 100 to display the
Find feature when the specified value is exceeded.</span></p>


<p class=MsoNormal><i><span style=''>Show
Password Entry Option</span></i><span style=''>
- Specifies a user's ability to modify their password from within the
application. If checked, users will be able to modify their passwords using the
Profile page. Customers integrating a proprietary authentication schema within
the application may want to disable password entry.</span></p>


<p class=MsoNormal><i><span style=''>Delete
archives with report - </span></i><span style=''>Specifies
whether a report's Archives are automatically deleted when the report is
deleted. If checked, the Archives for a report will be deleted if the report is
deleted. Checking this option might result in decreased performance when a
report is deleted. A backup of deleted reports and archives are located in the </span><span
style='font-size:11.0pt;'>_Definitions\_Reports\_Backup</span><span
style=''> folder.</span></p>


<p class=MsoNormal><i><span style=''>Unique
Report/Folder</span></i><span style=''> <i>name</i>
-  Specifies whether duplicate reports/folders are permitted in the instance.
If this option is checked for an instance with duplicate items, a dialog box
will be displayed to allow the duplications to be resolved.</span></p>


<p class=MsoNormal><i><span style=''>First Day
of Fiscal Year - </span></i><span style=''>Specifies
the reference value for calculation of pre-defined date tokens of a  fiscal
year  nature. Pre-defined dates are used in parameters throughout the application.</span></p>



<p class=MsoNormal><i><span style=''>Report
settings</span></i></p>


<p class=MsoNormal><b><span style=''><img
border=0   id="Picture 148"
src="System_Admin_Guide_files/image100.jpg"></span></b></p>



<p class=MsoNormal><i><span style=''>Report
Hyperlink target window - </span></i><span style=''>Specifies<i>
</i>the target window for the hyperlinks to run the reports. The valid values
can be:<br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style=';color:black'>_blank</span><span
style=''> (the default) - renders the report in
a new window without frames.</span></p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style=''>_parent</span><span
style=''> - renders the report in the immediate
frameset parent.</span></p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style=''>_self</span><span
style=''> - renders the report in the frame
with focus.</span></p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style=''>_top</span><span
style=''> - renders the report in the full
window without frames.</span></p>


<p class=MsoNormal><i><span style=''>Show a
disabled link to no-access reports - </span></i><span style=''>Specifies
whether a user will be able to see a report to which they do not have access in
report lists. If checked, reports will appear as a disabled link if the user
does not have access to any of the objects or columns used in the report. If unchecked,
the reports will instead be hidden. </span></p>


<p class=MsoNormal><i><span style=''>Show live preview
- </span></i><span style=''>Specifies the
availability of the Live Preview panel in the Report Builder.</span></p>


<p class=MsoNormal><i><span style=''>Encoding</span></i><span
style=''> - Specifies the default character
encoding for this application. The default value is</span> <span
style='font-size:11.0pt;'>UTF-8 (Unicode)</span>.</p>


<p class=MsoNormal><i><span style=''>Default
Report Expiration Date</span></i><span style=''>
- Specifies the when reports will automatically expire. The options are:<br>
<br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:1.0in;text-indent:-.25in'><span
style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style=''>None   no default
value is set.</span></p>

<p class=MsoNormal style='margin-left:1.0in;text-indent:-.25in'><span
style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style=''>Days   reports
will expire a set number of days after creation.</span></p>

<p class=MsoNormal style='margin-left:1.0in;text-indent:-.25in'><span
style='Symbol'> <span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style=''>Date   reports
will expire on a set date.</span></p>


<p class=MsoNormal><i><span style=''>Render
animated charts as</span></i><span style=''> -
Specifies the technology to be used to display animated charts: Flash or HTML5.</span></p>



<p class=MsoNormal><i><u><span style=''>Password
Management</span></u></i></p>



<p class=MsoNormal><img border=0   id="Picture 149"
src="System_Admin_Guide_files/image101.jpg"></p>


<p class=MsoNormal><i><span style=''>Require
change of password at logon</span></i><span style=''>
- Specifies if users will be required to change their password when they login
for the first time and/or after a specified number of days.</span><span
style='font-size:11.0pt;;color:#484848'> </span></p>

<p class=MsoNormal><span style='font-size:11.0pt;;
color:#484848'>&nbsp;</span></p>

<p class=MsoNormal><i><span style=''>Require
change of password every n days</span></i><span style=''>
- Specifies the number of a days before a user's password expires and they ll
be required to change their password on login. </span></p>


<p class=MsoNormal><i><span style=''>Minimum
password length</span></i><span style=''> and <i>Maximum
password length - </i>Specify the rules for the length of the password.</span></p>


<p class=MsoNormal><i><span style=''>Password
Restrictions</span></i><span style=''> -
Specifies the required content (strength) of a password.</span></p>



<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the attributes. The attributes on the <i>Application Settings</i> page
are either stored in the </span><span style='font-size:11.0pt;'>_Settings.lgx</span><span
style=''> file or in the metadata database, or
impact records in the metadata database.</span></p>

<p class=MsoNormal><span style='font-size:11.0pt;;
color:#484848'>&nbsp;</span></p>

</body>
</html>
