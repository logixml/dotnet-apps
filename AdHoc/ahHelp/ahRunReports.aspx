<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc382291627"><br></a><a name="_Toc456945131">Running Reports</a></h2>



<p class=MsoNormal>Now that you have spent time creating and polishing your
reports, let s see how to run them.</p>



<p class=MsoNormal><img border=0   id="Picture 242"
src="Report_Design_Guide_files/image190.jpg"></p>


<p class=MsoNormal>The <b>Report</b><b>s</b> button displays the main page of
the application.</p>




<p class=MsoNormal><img border=0   id="Picture 243"
src="Report_Design_Guide_files/image191.jpg"></p>


<p class=MsoNormal><br>
A list of reports is available under the <i>Personal Reports </i>and <i>Shared
Reports </i>tabs. If no reports have been created, this list is empty.</p>


<p class=MsoNormal>Click the name of the report to launch it in a new browser
window.</p>



</body>
</html>
