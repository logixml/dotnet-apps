<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945134"></a><a name="_Toc382291629">Organizing Reports</a></h2>


<p class=MsoNormal>Users can create and store reports in the instance root folder
or within an existing folder. Administrators can create folders in the Shared
Reports area, which can be accessed by all users or limited to users based on
the role(s) assigned to the user.</p>


<p class=MsoNormal><b>To create a new folder to store reports:</b></p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Navigate
to a report area (e.g., Personal Reports, Shared Reports) or folder in which
the new folder is to be created.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>New
Folder</b>.</p>

<p class=MsoNormal style='margin-left:.25in'>&nbsp;</p>

<p class=MsoNormal> <img border=0   id="Picture 33"
src="Report_Design_Guide_files/image197.jpg"></p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Enter the
name of the new folder into the Folder field<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>OPTIONAL: Enter
a description into the Description textbox.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>OPTIONAL: As
a designated administrator of a User Group and from the Shared Reports area, specify
restricted access to this folder based on a user's role. By default, all roles
have access to the folder. To limit access:<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Select the
<b>Specific Roles</b> option.</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Move roles
between the <i>Available Roles </i>and the <i>Roles With Access</i> listes by
double-clicking on the listed role or by selecting the role and then clicking
the <img border=0   id="Picture 250"
src="Report_Design_Guide_files/image113.gif" alt=arrowRight> (right) or <img
border=0 width=9 height=15 id="Picture 251"
src="Report_Design_Guide_files/image114.gif" alt=arrowLeft> (left) icon. At a
minimum, the roles currently assigned to the user creating the folder must have
access.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>6.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>Save
</b>to create the new folder.</p>



<p class=MsoNormal><b>To modify a report folder:</b></p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>Edit</b>
to modify the corresponding report folder:</p>


<p class=MsoNormal><img border=0   id="Picture 252"
src="Report_Design_Guide_files/image198.jpg"></p>



<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Modify the
folder name and the description, as desired, in the provided fields.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>OPTIONAL:
As a designated administrator of a User Group and from the Shared Reports area,
specify restricted access to this folder based on a user's role. By default,
all roles have access to the folder. To limit access:<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>a.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Select the
<b>Specific Roles</b> option.</p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'>b.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Move roles
between the <i>Available Roles </i>and the <i>Roles With Access</i> listes by
double-clicking on the listed role or by selecting the role and then clicking
the <img border=0   id="Picture 253"
src="Report_Design_Guide_files/image113.gif" alt=arrowRight> (right) or <img
border=0 width=9 height=15 id="Picture 254"
src="Report_Design_Guide_files/image114.gif" alt=arrowLeft> (left) icon. At a
minimum, the roles currently assigned to the user creating the folder must have
access.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>Save
</b>to commit the changes.</p>


<p class=MsoNormal>Changes are reflected in the <i>Name</i> column and the <i>Last
Modified</i> column is updated to the current timestamp.</p>




<p class=MsoNormal><b>To delete one or more report folders:</b></p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Select one
or more folders by checking their checkboxes.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>Delete</b>.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>OK
</b>to confirm the removal.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Alternatively,
a single folder can be removed by hovering over the <b>More</b> button, clicking
the <b>Delete</b> option, and confirming the action.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Deleting a report folder removes all reports stored
  within that folder. A backup copy of deleted reports is stored in the
  ..\_Definitions\_Reports\_Backup directory before they are removed from the
  application. Imported reports will be deleted from being visible in the
  application interface but will not be deleted from the _Reports directory.</p>
  </div>
  </td>
 </tr>
</table>

</div>



<p class=MsoNormal>After creating a new report folder, move reports out of the
root folder to make the workspace more manageable.</p>



<p class=MsoNormal><b>To move reports into a folder:</b></p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>To move a
single report, hover your mouse cursor over the <b>More</b> button and select <b>Move</b>
from the list of actions, or select the report by checking its checkbox and click
<b>Move</b>. Multiple reports may be moved by selecting them reports and
clicking <b>Move</b>.<br>
<br>
</p>


<p class=MsoNormal><img border=0   id="Picture 255"
src="Report_Design_Guide_files/image199.jpg"></p>



<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>OPTIONAL:
From the<b> </b><i>Organization</i> drop-down menu, select a user group.
Organizations are not available by default. The administrator must have
specifically enabled the Organization capability and created multiple
organizations.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>From the <i>Destination
Folder Type</i><b> </b>drop-down list, select a folder type.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>4.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>From the <b>Folder</b>
tree, select a folder to store the report in.<br>
<br>
</p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>5.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span>Click <b>Save
</b>to move the report.</p>




</body>
</html>
