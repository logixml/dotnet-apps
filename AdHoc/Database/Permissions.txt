1	System Administration	Allows every right in the application
2	End User	View, create, modify, rename and delete personal reports and folders; archive personal reports; create reports in tabular, bar chart and pie chart presentation formats
3	Manage My Personal Reports	Allows view, create, change, rename, and delete personal reports as well as personal folders. Copy is allowed to any destination where "report creation" right exists.
4	View My Personal Reports	Allows viewing of personal reports. Copy is allowed to any destination where "report creation" right exists.
5	Manage Shared Reports	Allows view, create, change, rename, and delete shared reports as well as personal folders. Copy is allowed to any destination where "report creation" right exists.
6	View Shared Reports	Allows viewing of shared reports. Copy is allowed to any destination where "report creation" right exists.
7	Manage All Personal Reports	Allows view, create, change, rename, and delete all personal reports and folders. Copy is allowed to any destination where "report creation" right exists.
8	Schedule and Archive Reports	Allows scheduling and archiving of any reports that are available to view as well as viewing of archived reports
9	Access Configuration Area	Allows access to the configuration menu and its underlying sub-menus.
12	Administer Organization	Allow admin rights in report management area plus user management in configuration for the organization.
13	Power End User	View, create, modify, rename and delete personal reports and folders; archive personal reports; create reports in tabular, bar chart and pie chart presentation formats