<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head runat="server">
<title>Error</title>
  <meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
  <meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
  <meta name="vs_defaultClientScript" content="JavaScript">
  <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link rel="stylesheet" type="text/css" href="Login.css">
</head>

<body background="ahImages/homePattern.gif">
<table width="100%" border="0" cellspacing="0" cellpadding="0" height="100%">
  <tr align="center" valign="middle"> 
    <td> 
      <table width="420" height="250" border="0" align="center" cellpadding="3" cellspacing="0">
        <tr bgcolor="#254974" align="center" valign="middle"> 
          <td> 
            <table width="100%" height="230" border="0" cellpadding="0" cellspacing="0">
              <tr bgcolor="#4A719C"> 
                <td align="center" valign="middle" bgcolor="#FFFFFF">
				 <table width="90%" height="230" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td align="center" valign="middle">
				      <font color="#CC3300"><%=Request("ahMessage")%></font>
					</td>
                  </tr>
                </table>
               </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
     <br>
    </td>
  </tr>
</table>
</body>
</html>
