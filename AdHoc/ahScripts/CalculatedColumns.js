// CalculatedColumns.js 
// Functions used to run and validate the calculated columns panel.

var lastCaretPos;

// Places the symbol at the end of the text in the formula box.
function appendSymbol( symbol )
{
	// Symbols can be only one character long.
	if (symbol.length = 1)
	{
		var target = document.getElementById( "Formula" );
		if (target != null)
		{
			//target.value += symbol;		
			insertAtCaret(target, " " + symbol + " ");
		}
	}
	else
	{
		alert("Invalid operator");
	}
}

function appendColumn()
{
	var el = document.getElementById("ColumnPicker");
	var target = document.getElementById( "Formula" );
	
	if (el != null )
	{
		for (i=0; i<el.options.length; i++) 
		{
			if (el.options[i].selected) 
			{
				// Add the column name to the formula. Enclose it in square brackets.
				//target.value += "[" + el.options[i].text + "]";
				//insertAtCaret(target, " [" + el.options[i].text + "] ");
				var txt = el.options[i].text;
				insertAtCaret(target, " �" + txt.replace(".", "�.�") + "� ");
			}
		}
	}
}

function pickStatisticalColumn( list )
{
	var el = list;
	var target = document.getElementById( "StatFormula" );
	var mName = document.getElementById( "StatName" );
		
	if (el != null )
	{
		for (i=0; i<el.options.length; i++) 
		{
			if (el.options[i].selected) 
			{
				// Add the column name to the formula. Enclose it in square brackets.
				var txt = el.options[i].text;
				target.value = " �" + txt.replace(".", "�.�") + "�";
				var pos = txt.lastIndexOf(".");
				mName.value = txt.substring(pos + 1, txt.length + 1) + getSuffix();
			}
		}
	}
}

function getSuffix() {
	var ft = document.getElementById( "FunctionType" );
	switch (ft.value) {
		case "1":
			return "Rank";
			break;
		case "2":
			return "RvrsRank";
			break;
		case "3":
			return "Percentile";
			break;
		case "4":
			return "RngTotal";
			break;
		case "5":
			return "Difference";
			break;
		default:
			return "Rank";
			break;
	}
}

function addCalculatedColumn(idTo)
{
	var oName = document.getElementById( "Name" );
	var oFormula = document.getElementById( "Formula" );
	var oTarget = document.getElementById(idTo);
	
	//TODO: Ensure that the name is unique.
	//TODO: Validate the formula.
		
	if (oName != null && oFormula != null && oTarget != null)
	{
		var oOption = document.createElement("OPTION");
		oOption.text = oName.value;
		oOption.value = oFormula.value;
		oTarget.options.add(oOption);
	}
	
	// NOTE: toggle is defined in Wizard.js.
	toggle('CalcPanel');
}

// Determines whether to enable or disable the edit calc button.
function toggleEditColumn()
{
	var oAssigned = document.getElementById( "assigned" );
	var oEditCalc = document.getElementById( "EditColumn" );
	var oEditStat = document.getElementById( "EditStatColumn" );

	// Assume selected column is not editable.
	if (oEditCalc!=null) oEditCalc.disabled = false;
	oEditStat.disabled = false;
	
	if (oAssigned.options.length > 0)
	{
		// Only the first selected item is relevant.
		var sValue = oAssigned.options[oAssigned.selectedIndex].text;
		var s = sValue.substring(0,7);
		
		if (s == 'MyCalc.')
		{
			if (oEditCalc!=null) oEditCalc.disabled = false;
		}
		else
		{
			if (oEditCalc!=null) oEditCalc.disabled = true;
		}

		if ((s == 'MyRank.') || (s == 'MyPerc.') || (s == 'MyRevR.') || (s == 'MyRngT.') || (s == 'MyDiff.'))
		{
			oEditStat.disabled = false;
		}
		else
		{
			oEditStat.disabled = true;
		}		
	}
}

// The functions below insert text at a specified position in a textarea.
// The code below only works for IE, since it relies on createTextRange().
// Inserts will occur at the end of the textarea for all other browsers.
// See http://www.faqts.com/knowledge_base/view.phtml/aid/1052/fid/130

// Adjusts the caret position.
// Used in conjunction with the [storeCaret] function.
function setCaretToEnd (el) {
  if (el.createTextRange) {
    var v = el.value;
    var r = el.createTextRange();
    r.moveStart('character', v.length);
    r.select();
  }
}

// Inserts text at the current location of a cursor in an html form element.
// Used in conjunction with the [storeCaret] function.
function insertAtCaret(el, txt) {
  if (lastCaretPos) {
	el.range = lastCaretPos;
    el.range.text = el.range.text.charAt(el.range.text.length - 1) != ' ' ? txt : txt + ' ';
    el.range.select();
  }
  else {
    insertAtEnd(el, txt);
  }
}

// Stores the current location of a cursor in an html form element.
function storeCaret() {
  var el = document.getElementById( "Formula" );
  if (el != null) {
      if (el.createTextRange)
        lastCaretPos = document.selection.createRange().duplicate();
  }
}

// Inserts text at the end of an html form element.
function insertAtEnd(el, txt) {
  el.value += txt;
  setCaretToEnd (el);
}