<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="UserProfile.aspx.vb" Inherits="LogiAdHoc.ahReport_UserProfile" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PasswordControl" Src="~/ahControls/PasswordControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>User Profile</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
</head>
<body id="bod" >
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
	<div id="submenu"><AdHoc:NavMenu id="subnav" runat="server" /></div>	
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="UserProfile" ParentLevels="0" />
            
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"  />
        
        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
            RestorePopupPosition('ahModalPopupBehavior');
        }
        function BeginRequestHandler(sender, args) {
            SavePopupPosition('ahModalPopupBehavior');
        }
        </script>
        
        <div class="divForm">
        <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
        <input id="UserID" type="hidden" runat="server">
        
            <table style="height: 146px">
                <tr>
                    <td width="125">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:LogiAdHoc, UserName %>"></asp:Localize>:
                    </td>
                    <td>
                        <asp:Label ID="username" runat="server" meta:resourcekey="usernameResource1" /></td>
                    <td rowspan="5">
                        
                    </td>
                </tr>
                <tr id="rowPassword" runat="server">
                    <td>
                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:LogiAdHoc, Password %>"></asp:Localize>:
                    </td>
                    <td>
                        <AdHoc:LogiButton ID="SetPassword" OnClick="ShowPassword" Text="Set Password"
                            runat="server" ValidationGroup="User" meta:resourcekey="SetPasswordResource1" />
                        <asp:CustomValidator ID="cvPassword" runat="server" EnableClientScript="False" ErrorMessage="You must enter a password for this user."
                            OnServerValidate="IsPasswordEntered" ValidationGroup="User" meta:resourcekey="cvPasswordResource1">*</asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                    <asp:Label ID="Label4" runat="server" AssociatedControlID="firstname" Text="<%$ Resources:LogiAdHoc, FirstName %>"></asp:Label>:
                    </td>
                    <td>
                        <input id="firstname" type="text" maxlength="20" size="20" runat="server"></td>
                </tr>
                <tr>
                    <td>
                    <asp:Label ID="Label5" runat="server" AssociatedControlID="lastname" Text="<%$ Resources:LogiAdHoc, LastName %>"></asp:Label>:
                    </td>
                    <td>
                        <input id="lastname" type="text" maxlength="20" size="20" runat="server"></td>
                </tr>
                <tr id="tdEmail" runat="server">
                    <td>
                        <asp:Label ID="Label6" runat="server" AssociatedControlID="email" Text="<%$ Resources:LogiAdHoc, EmailAddress %>"></asp:Label>:
                    </td>
                    <td>
                        <input id="email" type="text" maxlength="50" size="50" runat="server">
                        <%--<asp:RegularExpressionValidator ID="eavEmail" runat="server" ControlToValidate="email" ValidationGroup="User"
                            ErrorMessage="This is not a properly formatted email address." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" meta:resourcekey="eavEmailResource1">*</asp:RegularExpressionValidator>--%>
                        <asp:RegularExpressionValidator ID="eavEmail" runat="server" ControlToValidate="email" ValidationGroup="User"
                            ErrorMessage="This is not a properly formatted email address." ValidationExpression="((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*((\s)*[;](\s)*)*)*" meta:resourcekey="eavEmailResource1">*</asp:RegularExpressionValidator>
                        <asp:CustomValidator ID="cvUserSubscribed" runat="server" ControlToValidate="email" ValidationGroup="User" ValidateEmptyText="true"  
                            OnServerValidate="IsEmailValid" ErrorMessage="The email address cannot be blank when you are subscribed to one or more scheduled reports." meta:resourcekey="cvUserSubscribedResource1">*</asp:CustomValidator>
                    </td>
                </tr>
            </table>

            <div>
                <asp:Button runat="server" ID="Button1" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                    TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                    DropShadow="False" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width: 306;">
                    <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                    <div class="modalPopupHandle" style="width: 300px;">
                        <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                <asp:Localize ID="PopupHeader" runat="server" Text="Set Password" meta:resourcekey="PopupHeaderResource1"></asp:Localize>
                            </td>
                            <td style="width: 20px;">
                                <asp:ImageButton ID="imgClosePopup" runat="server" 
                                    OnClick="imgClosePopup_Click" CausesValidation="false"
                                    SkinID="imgbClose" ImageUrl="../ahImages/remove.gif"
                                    AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                        </td></tr></table>
                    </div>
                    </asp:Panel>
                    <div class="modalDiv">
                        <asp:UpdatePanel ID="UP2" UpdateMode="Conditional" runat="server">
                            <ContentTemplate>
                                    <AdHoc:PasswordControl ID="UserPassword" runat="server" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="SetPassword" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
            </div>
                        
        <div id="divButtons" class="divButtons">
            <AdHoc:LogiButton ID="Save" runat="server" OnClick="SaveUser" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip %>"
                Text="<%$ Resources:LogiAdHoc, Save %>" UseSubmitBehavior="false" ValidationGroup="User" />
            <asp:ValidationSummary ID="vsummary" runat="server" ValidationGroup="User" meta:resourcekey="vsummaryResource1" />
        </div>
<br />
        <div id="divSaveAnimation">
            <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                DropShadow="False">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                <table><tr><td>
                <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                </td>
                <td valign="middle">
                <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                </td></tr></table>
            </asp:Panel>
        </div>
        </div>
    </form>
</body>
</html>
