<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ReportMove.aspx.vb" Inherits="LogiAdHoc.ahReport_ReportMove"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Move Report</title>

    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>

</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
    	<div id="submenu"><AdHoc:NavMenu id="subnav" runat="server" /></div>	
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="MoveReport" />
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server" />

        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                noMessage=false;
                $find('ahSavePopupBehavior').hide();
            }
        </script>

        <div class="divForm">
            <asp:UpdatePanel ID="UP2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <input type="hidden" id="ahDirty" runat="server" />
                    <input type="hidden" id="OriginalOwner" runat="server" />
                    <input type="hidden" id="ResultOwner" runat="server" />
                    <%--<input type="hidden" id="PhysicalName" runat="server"/>--%>
                    <table>
                        <tr>
                            <td width="100px">
                                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:LogiAdHoc, ReportName %>"></asp:Localize>:</td>
                            <td>
                                <asp:Label ID="reportname" runat="server" meta:resourcekey="reportnameResource1" /></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:LogiAdHoc, CurrentOwner %>"></asp:Localize>:
                            </td>
                            <td>
                                <asp:Label ID="username" runat="server" meta:resourcekey="usernameResource1" /></td>
                        </tr>
                        <tr id="trUserGroup" runat="server">
                            <td runat="server">
                                <asp:Label ID="Label3" runat="server" AssociatedControlID="UserGroup" Text="<%$ Resources:LogiAdHoc, UserGroup %>"></asp:Label>:
                            </td>
                            <td runat="server">
                                <asp:DropDownList ID="UserGroup" OnSelectedIndexChanged="UserGroup_SelectedIndexChanged"
                                    AutoPostBack="True" runat="server">
                                </asp:DropDownList></td>
                        </tr>
                        <tr id="FolderRow" runat="server">
                            <td runat="server">
                                <asp:Label ID="Localize5" runat="server" AssociatedControlID="FolderTypeID" Text="<%$ Resources:LogiAdHoc, DestFolderType %>"></asp:Label>:
                            </td>
                            <td runat="server">
                                <asp:DropDownList ID="FolderTypeID" AutoPostBack="True" OnSelectedIndexChanged="FolderType_SelectedIndexChanged"
                                    runat="server">
                                    <asp:ListItem Value="0" Text="<%$ Resources:LogiAdHoc, Menu_SharedReports %>" />
                                    <asp:ListItem Value="1" Text="<%$ Resources:LogiAdHoc, Menu_MyReports %>" />
                                    <asp:ListItem Value="2" Text="<%$ Resources:LogiAdHoc, OtherUser %>" />
                                    <asp:ListItem Value="3" Text="<%$ Resources:LogiAdHoc, Menu_GlobalReports %>" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="UserRow" runat="server">
                            <td runat="server">
                                <asp:Label ID="Label4" runat="server" AssociatedControlID="OwnerUserID" Text="<%$ Resources:LogiAdHoc, NewOwner %>"></asp:Label>:
                            </td>
                            <td runat="server">
                                <asp:DropDownList ID="OwnerUserID" AutoPostBack="True" OnSelectedIndexChanged="Owner_SelectedIndexChanged"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr id="ParentRow" runat="server">
                            <td valign="top" runat="server">
                                <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:LogiAdHoc, Folder %>"></asp:Localize>:
                            </td>
                            <td runat="server">
                                <asp:TreeView ID="tvFolders" runat="server" ShowLines="True" EnableClientScript="false">
                                    <NodeStyle HorizontalPadding="4px" ImageUrl="~/ahImages/iconSFolder.gif" />
                                    <SelectedNodeStyle HorizontalPadding="4px" ImageUrl="~/ahImages/iconSFolderOpen.gif"
                                        CssClass="treenodeSelected" />
                                </asp:TreeView>
                            </td>
                        </tr>
                    </table>
                    <ul runat="server" id="ErrorList" class="validation_error">
                        <li>
                            <asp:Label runat="server" ID="lblValidationMessage"></asp:Label>
                        </li>
                    </ul>
                    <div id="divButtons" class="divButtons">
                        <AdHoc:LogiButton ID="Save" Text="<%$ Resources:LogiAdHoc, Save %>"
                            OnClick="Save_OnClick" ToolTip="<%$ Resources:LogiAdHoc, SaveReportListTooltip %>"
                            runat="server" CausesValidation="false" />
                        <AdHoc:LogiButton ID="Cancel" Text="<%$ Resources:LogiAdHoc, BackToReportsList %>" 
                            OnClick="Cancel_OnClick" ToolTip="<%$ Resources:LogiAdHoc, BackToReportsListTooltip %>"
                            runat="server" CausesValidation="False" />
                    </div>
                    <asp:UpdatePanel ID="UPRenameReports" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>                
                            <asp:Button runat="server" ID="Button1" style="display:none"/>
                            <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                                TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                                DropShadow="False" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                            </ajaxToolkit:ModalPopupExtender>
                            
                            <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" style="display:none;width:430;">
                                <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                                <div class="modalPopupHandle">
                                    <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                            <asp:Localize ID="PopupHeader" runat="server" Text="Rename Reports" meta:resourcekey="LiteralResource7"></asp:Localize>
                                        </td>
                                        <td style="width: 20px;">
                                            <asp:ImageButton ID="imgClosePopup" runat="server" 
                                                OnClick="imgClosePopup_Click" CausesValidation="false"
                                                SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                                AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                    </td></tr></table>
                                </div>
                                </asp:Panel>
                                <div class="modalDiv">
                                    <asp:UpdatePanel ID="upPopup" runat="server" >
                                        <ContentTemplate>
                                            <input type="hidden" id="ihSingleReport" runat="server" />
                                            <div id="divSingleReport" runat="server">
                                                <p class="info">
                                                    <asp:Localize ID="Localize4" runat="server" Text="A report with the same name already exists in the destination folder. Please enter a new name for this report."></asp:Localize>
                                                </p>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:Localize ID="Localize9" runat="server" meta:resourcekey="LiteralResource9" Text="Report:"></asp:Localize>
                                                        </td>
                                                        <td>
                                                            <input type="hidden" id="ihRenameReportID" runat="server" />
                                                            <asp:Label ID="lblOrigReportName" runat="server"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource2" Text="New Report Name:"></asp:Localize>
                                                        </td>
                                                        <td>
                                                            <input id="txtReportName" type="text" maxlength="100" size="50" runat="server" />
                                                            <asp:RequiredFieldValidator ID="rfvtxtReportName" runat="server" ErrorMessage="Report Name is required."
                                                                ControlToValidate="txtReportName" ValidationGroup="ReportName" meta:resourcekey="rfvtxtReportNameResource1">*</asp:RequiredFieldValidator>
                                                            <asp:CustomValidator ID="cvtxtReportName" runat="server" ErrorMessage="This report name already exists in the database. Please select another report name."
                                                                ControlToValidate="txtReportName" ValidationGroup="ReportName" OnServerValidate="IsReportNameUnique" EnableClientScript="false" 
                                                                meta:resourcekey="cvtxtReportNameResource1">*</asp:CustomValidator>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div id="divMultipleReports" runat="server" style="height: 300px; overflow: auto;">
                                                <p class="info">
                                                    <asp:Localize ID="Localize8" runat="server" Text="The following reports do not have unique names in the destination folder. Please enter a new name for these reports."></asp:Localize>
                                                </p>
                                                <asp:GridView ID="grdReports" runat="server" AllowPaging="False" AutoGenerateColumns="False"
                                                    CssClass="grid" OnRowDataBound="OnItemDataBoundHandler" meta:resourcekey="grdReportsResource1">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Report" meta:resourcekey="TemplateFieldResource1">
                                                            <ItemTemplate>
                                                                <input type="hidden" id="ReportID" runat="server" />
                                                                <asp:Label ID="OrigReportName" runat="server" meta:resourcekey="OrigReportNameResource1" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="New Report Name" meta:resourcekey="TemplateFieldResource2">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="NewReportName" MaxLength="255" Width="200px" runat="server" title="New Report Name" meta:resourcekey="ColumnLabelResource1" />
                                                                <asp:RequiredFieldValidator ID="rtvColumnLabel" runat="server" ControlToValidate="NewReportName"
                                                                    ErrorMessage="Report Name is required." meta:resourcekey="rtvColumnLabelResource1">*</asp:RequiredFieldValidator>
                                                                <asp:CustomValidator ID="cvReportNameUnique" ControlToValidate="NewReportName"
                                                                    ErrorMessage="Report name already exist." OnServerValidate="IsNewReportNameUnique"
                                                                    runat="server" meta:resourcekey="cvNewReportNameUniqueResource1">*</asp:CustomValidator>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle CssClass="gridheader" />
                                                    <AlternatingRowStyle CssClass="gridalternaterow" />
                                                </asp:GridView>
                                            </div>
                                            <br />
                                            <table>
                                                <tr>
                                                    <td>
                                                        <AdHoc:LogiButton ID="btnSPA_OK" runat="server" OnClick="OK_SPA" ValidationGroup="ReportName"
                                                        ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" />
                                                        <AdHoc:LogiButton ID="btnSPA_Cancel" runat="server" OnClick="Cancel_SPA"
                                                        ToolTip="<%$ Resources:LogiAdHoc, CancelTooltip1 %>" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="ReportName" runat="server" meta:resourcekey="vsummaryResource1" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <br />
                    <div id="divSaveAnimation">
                        <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                            TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                            DropShadow="False">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                            <table><tr><td>
                            <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                            </td>
                            <td valign="middle">
                            <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                            </td></tr></table>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Cancel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
