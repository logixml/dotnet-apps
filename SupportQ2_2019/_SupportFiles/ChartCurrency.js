var barChartContainer = Y.one('#ccCurrency');
	barChartContainer.on('beforeCreateChart', function(e) {
    setCurrencyFormatter(e);
});

function setCurrencyFormatter (e) {
    if (e.options.series == null) {
        return;
    }

    if (!e.options.plotOptions) {
        e.options.plotOptions = {};
    }
    if (!e.options.plotOptions.column) {
        e.options.plotOptions.column = {};
    }
    e.options.plotOptions.column.dataLabels = {
        enabled: true,
        formatter: function () {
			debugger;
            var str;
            if (this.key == "USD") {
				return '$' + (this.y).toFixed(2);
            } else if (this.key == "GBP") {
				return '£' + (this.y).toFixed(2);
			} else if (this.key == "EUR") {
				return (this.y).toFixed(2) + ' €'
			}
            return false;
        },
        crop: false,
        overflow: 'none'
    }
}