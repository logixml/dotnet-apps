
function convertEpochToXMLDate(nEpoch) {
	dEpoch = new Date(nEpoch * 1000);
	
	// Convert the Date object to XML Date format yyyy-MM-ddThh:mm:ss
	// Uses assumption of typical Epoch - based on UTC time.
	return dEpoch.getFullYear() + '-' 
	+ ("0" + (dEpoch.getMonth() + 1)).slice(-2) + '-' 
	+ ("0" + dEpoch.getDate()).slice(-2) + 'T' 
	+ ("0" + dEpoch.getHours()).slice(-2) + ':' 
	+ ("0" + dEpoch.getMinutes()).slice(-2) + ':' 
	+ ("0" + dEpoch.getSeconds()).slice(-2) + '+00:00';
}