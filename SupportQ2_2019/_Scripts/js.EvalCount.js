//This sample displays an alert box

function rowCount(dLoss, dReport, dLastClose, dFirstTrans, dLastTrans, sEval) {
	nLossDay = new Date(dLoss).getDate();
	nReportDay = new Date(dReport).getDate();
	nLastCloseDay = new Date(dLastClose).getDate();
	nFirstTransDay = new Date(dFirstTrans).getDate();
	nLastTransDay = new Date(dLastTrans).getDate();
	
	iCnt = 0;
	if ((nLossDay < 15 || nLossDay > 20) && nLossDay > 0) { 
		iCnt++;
		if(sEval.toUpperCase() === "LOSS") return iCnt;
	}
	if ((nReportDay < 15 || nReportDay > 20) && nReportDay > 0) { 
		iCnt++;
		if(sEval.toUpperCase() === "REPORT") return iCnt;
	}
	if ((nLastCloseDay < 15 || nLastCloseDay > 20) && nLastCloseDay > 0) { 
		iCnt++;
		if(sEval.toUpperCase() === "LASTCLOSE") return iCnt;
	}
	if ((nFirstTransDay < 15 || nFirstTransDay > 20) && nFirstTransDay > 0) { 
		iCnt++;
		if(sEval.toUpperCase() === "FIRSTTRANSACTION") return iCnt;
	}
	if ((nLastTransDay < 15 || nLastTransDay > 20) && nLastTransDay > 0) { 
		iCnt++;
		if(sEval.toUpperCase() === "LASTTRANSACTION") return iCnt;
	}
	
	return "";
//	return nLossDay;
}