<%@ Page Language="vb" AutoEventWireup="false" Codebehind="About.aspx.vb" Inherits="LogiAdHoc.About" Culture="auto" UICulture="auto" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>About Logi AdHoc</title>
</head>
<body class="AboutBody" onkeypress="window.close()" onclick="window.close()" >
    <form name="frmVersion">
        <table border="0" cellspacing="0" cellpadding="0" height="100" style="width: 435px" >
            <tr >
            <td><br /><br /><br /><br /></td>
            </tr>
            <tr align="center" >
                
                <td>
                    <table border="0" cellpadding="3" cellspacing="0"  style="width: 425px;">
                        <tr>
                            <td width="60%" align="left" >
                                <b><asp:Label ID="lblAdHocVersionTitle" runat="server"></asp:Label></b>
                            </td>
                            <td width="5%"></td>
                            <td width="35%" height="25" >
                                <b><asp:Label ID="lblAdHocVersion" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        <tr>
                            <td width="60%" align="left" >
                                <b><asp:Label ID="lblEngineVersionLabel" runat="server" Text="Logi Info version:" meta:resourcekey="lblEngineVersionLabelResource1"></asp:Label></b>
                            </td>
                            <td width="5%"></td>
                            <td width="35%" height="25" >
                                <b><asp:Label ID="lblEngineVersion" runat="server"></asp:Label></b>
                            </td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
             <tr >
            <td><br /></td>
            </tr>
           <tr id="trTrialMessage" runat="server" align="center" valign="middle">
                <td>
                    <asp:Label ID="lblTrialNumDaysLeft" runat="server"></asp:Label>
                </td>
            </tr>
            <tr align="center" valign="middle">
            <td><br /><br /><br />Copyright � 2000-<%=Year(Date.Now)%> Logi Analytics, Inc.</td>
            </tr>
        </table>
    </form>
</body>
</html>
