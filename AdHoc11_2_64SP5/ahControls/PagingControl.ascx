<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PagingControl.ascx.vb" Inherits="LogiAdHoc.PagingControl" %>

<div id="pager">
    <table cellpadding="0" cellspacing="0"><tr>
    <td class="pagerBtn">
        <asp:LinkButton ID="lnkFrst" runat="server" CssClass="frst" OnClientClick="javascript:noMessage=true;" />
        <asp:LinkButton ID="lnkPrev" runat="server" CssClass="prev" OnClientClick="javascript:noMessage=true;" />
    </td>
    <td id="count">
        <asp:Label ID="lblPage" runat="server" Text="Page" AssociatedControlID="TxtPageNum" meta:resourcekey="lblPageResource1" />
        <asp:TextBox ID="TxtPageNum" runat="server" Width="32px" AutoPostBack="true"></asp:TextBox>
        <asp:Label ID="lblOf" runat="server" Text="of" meta:resourcekey="lblOfResource1" />
        <asp:Label ID="LblPageCount" runat="server" Width="16px"></asp:Label>
    </td>
    <td class="pagerBtn">
        <asp:LinkButton ID="lnkNext" runat="server" CssClass="next" OnClientClick="javascript:noMessage=true;" />
        <asp:LinkButton ID="lnkLast" runat="server" CssClass="last" OnClientClick="javascript:noMessage=true;" />
    </td>
    </tr></table>
</div>
