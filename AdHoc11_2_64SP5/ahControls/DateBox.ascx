<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DateBox.ascx.vb" Inherits="LogiAdHoc.DateBox" %>
<asp:TextBox ID="DateValue" Runat="server" Width="150px" ToolTip="<%$ Resources:LogiAdHoc, Res_Date %>" />&nbsp;
<asp:CustomValidator ID="cvDate" ControlToValidate="DateValue" EnableClientScript="False"
ErrorMessage="<%$ Resources:Errors, Err_DateFormat %>" runat="server" ValidateEmptyText="True" 
OnServerValidate="IsDateValid">*</asp:CustomValidator>


<asp:HyperLink ID="Calendar" runat="server" ImageUrl="../_Images/calendar.gif" 
    ToolTip="<%$ Resources:LogiAdHoc, CalendarLinkTooltip %>" 
    Text="<%$ Resources:LogiAdHoc, CalendarLinkTooltip %>"></asp:HyperLink>
