<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Button.ascx.vb" Inherits="LogiAdHoc.Button" %>
<script language="javascript" type="text/javascript">
<!--

    function activate(o) {
        o.className="innerbox active";
    }

    function deactivate(o) {
        o.className="innerbox inactive";
    }

-->    
</script>

<div class="block outerbox">
    <div id="btn" runat="server" class="innerbox inactive" onmousedown="activate(this);" onmouseout="deactivate(this);" onmouseup="deactivate(this);">
    </div>
</div>
<asp:Button runat="server" CssClass="NoShow" EnableViewState="false" ID="btnWork" OnCommand="DoActions" />
