<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DatabaseValues.ascx.vb" Inherits="LogiAdHoc.DatabaseValues" %>
<%@ Register TagPrefix="AdHoc" TagName="ScrollableListBox" Src="~/ahControls/ScrollableListBox.ascx" %>

<asp:UpdatePanel ID="UPPickValuesFromDatabase" runat="server" UpdateMode="conditional">
    <ContentTemplate>
        <table>
            <tr>
                <td>
                    <asp:Label ID="lbllstList" runat="server" CssClass="NoShow" Text="<%$ Resources:LogiAdHoc, Values %>" AssociatedControlID="slList" ></asp:Label>
<%--                    <asp:ListBox ID="lstList" SelectionMode="Multiple" Rows="8" runat="server" />
--%>                
                    <AdHoc:ScrollableListBox ID="slList" runat="server" MultipleSelection="True" SelectHeight="202" SelectWidth="150" />
                </td>
            </tr>
            <tr id="trDateTime" runat="server" visible="false">
                <td>
                    <asp:RadioButton ID="DateOnly" GroupName="DateTimeChoice" Text="<%$ Resources:LogiAdHoc, DateOnly %>" runat="server" 
                        meta:resourcekey="DateTimeChoiceResource1" AutoPostBack="true" OnCheckedChanged="DateTime_Changed" />
                    <asp:RadioButton ID="DateAndTime" GroupName="DateTimeChoice" Text="<%$ Resources:LogiAdHoc, DateAndTime %>" runat="server" Checked="true"
                        meta:resourcekey="DateTimeChoiceResource2" AutoPostBack="true" OnCheckedChanged="DateTime_Changed" />
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>


