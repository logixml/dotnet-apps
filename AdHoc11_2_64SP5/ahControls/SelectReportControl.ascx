<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SelectReportControl.ascx.vb" Inherits="LogiAdHoc.SelectReportControl" %>

<asp:Panel ID="pnlSelectReportControl" runat="server">
    <table cellpadding="0" cellspacing="4" style="border:solid 1px #EEEEEE;">
        <tr>
            <td>
                <div ID="pnlTreeView" runat="server">
                    <div id="divTreeView" runat="server">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:TreeView ID="tvFolders" runat="server" ShowLines="True" EnableClientScript="False" >
                                    <NodeStyle HorizontalPadding="4px" ImageUrl="~/ahImages/iconSFolder.gif" />
                                    <SelectedNodeStyle HorizontalPadding="4px" ImageUrl="~/ahImages/iconSFolderOpen.gif" CssClass="treenodeSelected" />
                                </asp:TreeView>
                            </ContentTemplate>
                        </asp:UpdatePanel>                
                    </div>
                </div>
            </td>
            <td valign="top">
                <div id="divListBox" runat="server">
                    <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Label ID="lbllstReports" runat="server" CssClass="NoShow" Text="<%$ Resources:LogiAdHoc, MainMenu_Reports %>" AssociatedControlID="lstReports" ></asp:Label>
                            <asp:ListBox ID="lstReports" runat="server"></asp:ListBox>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="tvFolders" EventName="SelectedNodeChanged" />
                        </Triggers>
                    </asp:UpdatePanel>                
                </div>
            </td>
        </tr>
    </table>
    <ajaxToolkit:ResizableControlExtender ID="RCE" runat="server" TargetControlID="pnlTreeView" 
                HandleCssClass="resizeHandle" HandleOffsetX="3" HandleOffsetY="0"
                OnClientResizing="PanelResizing" />
</asp:Panel>