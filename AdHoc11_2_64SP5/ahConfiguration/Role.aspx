<%@ Page Language="vb" AutoEventWireup="false" Codebehind="Role.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_Role"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Role</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>

</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <asp:UpdatePanel ID="UPBct" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="Role" ParentLevels="1" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlParentID" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>

        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
            RestorePopupPosition('ahModalPopupBehavior');
        }
        function BeginRequestHandler(sender, args) {
            SavePopupPosition('ahModalPopupBehavior');
        }
        </script>

        <div class="divForm">
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
            <input type="hidden" id="ahParentID" name="ahParentID" runat="server" />
            <asp:CheckBox ID="fake" runat="server" AutoPostBack="True" Style="display: none;"
                meta:resourcekey="fakeResource1" />
                
            <asp:UpdatePanel ID="UP1" runat="Server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table id="tbParent" runat="server">
                        <tr>
                            <td width="125px">
                                <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource1" Text="Selected Role:"></asp:Localize></td>
                            <td>
                                <asp:DropDownList ID="ddlParentID" AutoPostBack="True" runat="server" meta:resourcekey="ddlParentIDResource1" />
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td width="125">
                                <label for="rolename">
                                    <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2" Text="Role:"></asp:Localize></label></td>
                            <td>
                                <input id="rolename" type="text" maxlength="64" size="25" runat="server" />
                                <asp:RequiredFieldValidator ID="rtvRoleName" runat="server" ErrorMessage="Role is required."
                                    ControlToValidate="rolename" ValidationGroup="Roles" meta:resourcekey="rtvRoleNameResource1">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvValidName" runat="server" ErrorMessage="This name is not valid."
                                    ControlToValidate="rolename" OnServerValidate="IsNameValid" EnableClientScript="False"
                                    meta:resourcekey="cvValidNameResource1" ValidationGroup="Roles">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="125">
                                <asp:Localize ID="Localize15" runat="server" Text="<%$ Resources:LogiAdHoc, Description %>"></asp:Localize>:</td>
                            <td>
                                <asp:TextBox ID="RoleDescription" TextMode="MultiLine" Columns="50" Rows="3" runat="server" />
                                <asp:CustomValidator ID="cvMaxLength" ValidationGroup="Roles" runat="server" ErrorMessage="<%$ Resources:Errors, Err_Description255 %>"
                                    ControlToValidate="RoleDescription" OnServerValidate="ReachedMaxLength">*</asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                    <div id="DivRights" runat="server">
                        <h2>
                            <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3" Text="Role Permissions"></asp:Localize></h2>
                        <table id="RightsMSL">
                            <tr>
                                <td colspan="2">
                                    <label id="Label3" runat="server">
                                        <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource4" Text="Available Permissions"></asp:Localize></label></td>
                                <td colspan="2">
                                    <label id="Label4" runat="server">
                                        <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource5" Text="Assigned Permissions"></asp:Localize></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UP2" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                            <select id="RAvailable" class="MSL" multiple size="15" runat="server" ondblclick="document.getElementById('ahDirty').value=1; __doPostBack('moveRright','');" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="moveRright" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="moveRleft" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <asp:ImageButton CssClass="mslbutton" ID="moveRright" OnClick="MoveRRight_Click"
                                        ImageUrl="../ahImages/arrowRight.gif" CausesValidation="False" runat="server"
                                        meta:resourcekey="moveRrightResource1" />
                                    <asp:ImageButton CssClass="mslbutton" ID="moveRleft" OnClick="MoveRLeft_Click" ImageUrl="../ahImages/arrowLeft.gif"
                                        CausesValidation="False" runat="server" meta:resourcekey="moveRleftResource1" />
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UP3" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                            <select id="RAssigned" class="MSL" multiple size="15" runat="server" ondblclick="document.getElementById('ahDirty').value=1; __doPostBack('moveRleft','');" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="moveRright" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="moveRleft" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divUserGroupRoles" runat="server">
                        <h2>
                            <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource6" Text="Role Organizations"></asp:Localize></h2>
                        <table id="UserGroupMSL">
                            <tr>
                                <td colspan="2">
                                    <label id="Label5" runat="server">
                                        <asp:Localize ID="Localize7" runat="server" meta:resourcekey="LiteralResource7" Text="Available Organizations"></asp:Localize></label></td>
                                <td colspan="2">
                                    <label id="Label6" runat="server">
                                        <asp:Localize ID="Localize8" runat="server" meta:resourcekey="LiteralResource8" Text="Assigned Organizations"></asp:Localize></label>
                                    <input type="text" id="fakeUGAssigned" runat="server" style="display: none;"/>
                                    <asp:CustomValidator ID="cvGroupSelected" runat="server" ControlToValidate="fakeUGAssigned"
                                        EnableClientScript="False" ErrorMessage="You must select at least one organization for this role."
                                        meta:resourcekey="cvGroupSelectedResource1" OnServerValidate="IsUserGroupAssigned" ValidateEmptyText="true"
                                        ValidationGroup="Roles">*</asp:CustomValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UP4" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                            <select id="UGavailable" class="MSL" multiple size="15" runat="server" ondblclick="document.getElementById('ahDirty').value=1; __doPostBack('moveUGright','');" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="moveUGright" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="moveUGleft" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <asp:ImageButton CssClass="mslbutton" ID="moveUGright" OnClick="MoveUGRight_Click"
                                        ImageUrl="../ahImages/arrowRight.gif" CausesValidation="False" runat="server"
                                        meta:resourcekey="moveUGrightResource1" />
                                    <asp:ImageButton CssClass="mslbutton" ID="moveUGleft" OnClick="MoveUGLeft_Click"
                                        ImageUrl="../ahImages/arrowLeft.gif" CausesValidation="False" runat="server"
                                        meta:resourcekey="moveUGleftResource1" />
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UP5" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                            <select id="UGassigned" class="MSL" multiple size="15" runat="server" ondblclick="document.getElementById('ahDirty').value=1; __doPostBack('moveUGleft','');" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="moveUGright" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="moveUGleft" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="databaseroles" runat="server">
                        <h2>
                            <asp:Localize ID="Localize9" runat="server" meta:resourcekey="LiteralResource9" Text="Role Databases"></asp:Localize></h2>
                        <table id="DatabaseMSL">
                            <tr>
                                <td colspan="2">
                                    <label id="Label1" runat="server">
                                        <asp:Localize ID="Localize10" runat="server" meta:resourcekey="LiteralResource10"
                                            Text="Available Databases"></asp:Localize></label></td>
                                <td colspan="2">
                                    <label id="Label2" runat="server">
                                        <asp:Localize ID="Localize11" runat="server" meta:resourcekey="LiteralResource11"
                                            Text="Assigned Databases"></asp:Localize></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UP6" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                            <select id="DBavailable" class="MSL" multiple size="15" runat="server" ondblclick="document.getElementById('ahDirty').value=1; __doPostBack('moveDBright','');" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="moveDBright" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="moveDBleft" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <asp:ImageButton CssClass="mslbutton" ID="moveDBright" OnClick="MoveDBRight_Click"
                                        ImageUrl="../ahImages/arrowRight.gif" CausesValidation="False" runat="server"
                                        meta:resourcekey="moveDBrightResource1" />
                                    <asp:ImageButton CssClass="mslbutton" ID="moveDBleft" OnClick="MoveDBLeft_Click"
                                        ImageUrl="../ahImages/arrowLeft.gif" CausesValidation="False" runat="server"
                                        meta:resourcekey="moveDBleftResource1" />
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UP7" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                            <select id="DBassigned" class="MSL" multiple size="15" runat="server" ondblclick="document.getElementById('ahDirty').value=1; __doPostBack('moveDBleft','');" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="moveDBright" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="moveDBleft" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="userroles">
                        <h2>
                            <asp:Localize ID="Localize12" runat="server" meta:resourcekey="LiteralResource12"
                                Text="Role Users"></asp:Localize></h2>
                        <table id="UserMSL">
                            <tr>
                                <td colspan="2">
                                    <label id="lblAvailUsers" runat="server">
                                        <asp:Localize ID="Localize13" runat="server" meta:resourcekey="LiteralResource13"
                                            Text="Available Users"></asp:Localize></label></td>
                                <td colspan="2">
                                    <label id="lblUserRoles" runat="server">
                                        <asp:Localize ID="Localize14" runat="server" meta:resourcekey="LiteralResource14"
                                            Text="Assigned Users"></asp:Localize></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UP8" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                            <select id="available" class="MSL" multiple size="15" runat="server" ondblclick="document.getElementById('ahDirty').value=1; __doPostBack('moveright','');" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="moveright" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="moveleft" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <asp:ImageButton CssClass="mslbutton" ID="moveright" OnClick="MoveRight_Click" ImageUrl="../ahImages/arrowRight.gif"
                                        CausesValidation="False" runat="server" meta:resourcekey="moverightResource1" />
                                    <asp:ImageButton CssClass="mslbutton" ID="moveleft" OnClick="MoveLeft_Click" ImageUrl="../ahImages/arrowLeft.gif"
                                        CausesValidation="False" runat="server" meta:resourcekey="moveleftResource1" />
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UP9" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                            <select id="assigned" class="MSL" multiple size="15" runat="server" ondblclick="document.getElementById('ahDirty').value=1; __doPostBack('moveleft','');" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="moveright" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="moveleft" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                        </table>
                    </div>
                
                    <div id="divButtons" class="divButtons">
                        <%--<asp:UpdatePanel ID="UPSaveAS" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>--%>
                                <AdHoc:LogiButton ID="btnSave" runat="server" OnClick="SaveRole" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>"
                                    Text="<%$ Resources:LogiAdHoc, Save %>" UseSubmitBehavior="false" ValidationGroup="Roles" />
                                <AdHoc:LogiButton ID="btnSaveAs" runat="server" OnClick="SavePermissionAs" ValidationGroup="Roles"
                                    ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" Text="<%$ Resources:LogiAdHoc, SaveAs %>" />
                                <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="CancelRole"
                                    ToolTip="Click to cancel your unsaved changes and return to the previous page."
                                    Text="Back to Roles" CausesValidation="False" meta:resourcekey="btnCancelResource1" />
                                <asp:Button runat="server" ID="Button1" Style="display: none" />
                                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                                    TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                                    DropShadow="False" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                                </ajaxToolkit:ModalPopupExtender>
                                <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none;
                                    width: 250;">
                                    <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                                    <div class="modalPopupHandle">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                                <asp:Localize ID="PopupHeader" runat="server" Text="Save As New Role"></asp:Localize>
                                            </td>
                                            <td style="width: 20px;">
                                                <asp:ImageButton ID="imgClosePopup" runat="server" 
                                                    OnClick="imgClosePopup_Click" CausesValidation="false"
                                                    SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                                    AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                        </td></tr></table>
                                    </div>
                                    </asp:Panel>
                                    <div class="modalDiv">
                                    <asp:UpdatePanel ID="upPopup" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <%--<asp:Panel ID="pnlSaveAs" runat="server" CssClass="detailpanel">--%>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Localize ID="Localize20" runat="server" meta:resourcekey="LiteralResource2"
                                                            Text="Role:"></asp:Localize>
                                                    </td>
                                                    <td>
                                                        <input id="txtRoleName" type="text" maxlength="25" size="25" runat="server" />
                                                        <asp:RequiredFieldValidator ID="rfvtxtRoleName" runat="server" ErrorMessage="Role is required."
                                                            ControlToValidate="txtRoleName" ValidationGroup="SaveAs" meta:resourcekey="rtvRoleNameResource1">*</asp:RequiredFieldValidator>
                                                        <asp:CustomValidator ID="cvtxtRoleName" runat="server" ErrorMessage="This role name already exists in the database. Please select another role name."
                                                            ControlToValidate="txtRoleName" ValidationGroup="SaveAs" OnServerValidate="IsSaveAsRoleNameValid"
                                                            EnableClientScript="false" meta:resourcekey="cvValidNameResource1">*</asp:CustomValidator>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <table>
                                                <tr>
                                                    <td>
                                                        <AdHoc:LogiButton ID="btnSPA_OK" runat="server" OnClick="OK_SPA" ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>"
                                                            Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" ValidationGroup="SaveAs" UseSubmitBehavior="false" />
                                                        <AdHoc:LogiButton ID="btnSPA_Cancel" runat="server" OnClick="Cancel_SPA"
                                                            ToolTip="<%$ Resources:LogiAdHoc, CancelTooltip1 %>" Text="<%$ Resources:LogiAdHoc, Cancel %>"
                                                            CausesValidation="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="SaveAs" runat="server"
                                                meta:resourcekey="vsummaryResource1" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    </div>
                                </asp:Panel>
                                <asp:ValidationSummary ID="vsummary" runat="server" ValidationGroup="Roles" meta:resourcekey="vsummaryResource1" />
                            <%--</ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnSaveAsTop" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>--%>
                    </div>
                    <br />
                    
                    <div id="divSaveAnimation">
                        <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                            TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                            DropShadow="False">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                            <table><tr><td>
                            <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                            </td>
                            <td valign="middle">
                            <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                            </td></tr></table>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <%--<asp:AsyncPostBackTrigger ControlID="ddlParentID" EventName="SelectedIndexChanged" />--%>
                    <asp:PostBackTrigger ControlID="btnCancel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
