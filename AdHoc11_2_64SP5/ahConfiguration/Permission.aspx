<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Permission.aspx.vb" Inherits="LogiAdHoc.ahConfiguration_Permission" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="ScrollableListBox" Src="~/ahControls/ScrollableListBox.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Permission</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>
    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>
</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="True"  />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>

        <asp:UpdatePanel ID="UPBct" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
        <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="Permission" ParentLevels="1" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddlParentID" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>
        
        <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
            RestorePopupPosition('ahModalPopupBehavior');
        }
        function BeginRequestHandler(sender, args) {
            SavePopupPosition('ahModalPopupBehavior');
        }
        </script>

        <div class="divForm">
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
            <input type="hidden" id="ahParentID" name="ahParentID" runat="server" />
            <asp:CheckBox ID="fake" runat="server" AutoPostBack="True" Style="display: none;" meta:resourcekey="fakeResource1" />
                            
            <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
                <ContentTemplate>
                    <table id="tbParent" runat="server" class="tbTB">
                        <tr>
                            <td width="125px">
                                <asp:Localize ID="Localize1" runat="server" meta:resourcekey="LiteralResource1" Text="Selected Permission:"></asp:Localize>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlParentID" AutoPostBack="True" runat="server" meta:resourcekey="ddlParentIDResource1" />
                            </td>
                        </tr>
                    </table>
                    <table class="tbTB">
                        <tr>
                            <td width="125px">
                                <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2" Text="Permission Name:"></asp:Localize>
                            </td>
                            <td>
                                <input id="Permission" type="text" maxlength="100" size="45" runat="server" />
                                <asp:RequiredFieldValidator ID="rtvPermission" runat="server" ErrorMessage="Permission Name is required."
                                    ControlToValidate="Permission" ValidationGroup="Permission" meta:resourcekey="rtvPermissionResource1">*</asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="cvPermissionName" runat="server" ErrorMessage="This permission name already exists in the database. Please select another permission name."
                                    ControlToValidate="Permission" ValidationGroup="Permission" OnServerValidate="IsNameValid" meta:resourcekey="cvPermissionNameResource1">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr>
                            <td><asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3" Text="Description:"></asp:Localize></td>
                            <td>
                                <asp:TextBox ID="Description" TextMode="MultiLine" Columns="45" Rows="3" runat="server" />
                                <asp:CustomValidator ID="cvMaxLength" runat="server" ErrorMessage="<%$ Resources:Errors, Err_Description255 %>"
                                    ControlToValidate="Description" ValidationGroup="Permission" OnServerValidate="ReachedMaxLength">*</asp:CustomValidator>
                            </td>
                        </tr>
                    </table>
                    <div id="DivRights" runat="server">
                        <h2>
                            <asp:Localize ID="Localize4" runat="server" meta:resourcekey="LiteralResource4" Text="Rights for this Permission"></asp:Localize></h2>
                        <table id="RightsMSL">
                            <tr>
                                <td colspan="2">
                                    <label id="Label3" runat="server">
                                        <asp:Localize ID="Localize5" runat="server" meta:resourcekey="LiteralResource5" Text="Available Rights"></asp:Localize></label></td>
                                <td colspan="2">
                                    <label id="Label4" runat="server">
                                        <asp:Localize ID="Localize6" runat="server" meta:resourcekey="LiteralResource6" Text="Assigned Rights"></asp:Localize></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                        <%--<select id="RAvailable" class="MSL" multiple size="15" runat="server" ondblclick="document.getElementById('ahDirty').value=1; __doPostBack('moveRright','');" />--%>
                                        <AdHoc:ScrollableListBox ID="RAvailable" runat="server" DblClickFunction="document.getElementById('ahDirty').value=1; __doPostBack('moveRright','');"
                                            MultipleSelection="True" SelectHeight="202" SelectWidth="300" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="moveRright" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="moveRleft" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <asp:ImageButton CssClass="mslbutton" ID="moveRright" OnClick="MoveRRight_Click"
                                        ImageUrl="../ahImages/arrowRight.gif" CausesValidation="False" runat="server" meta:resourcekey="moveRrightResource1" />
                                    <asp:ImageButton CssClass="mslbutton" ID="moveRleft" OnClick="MoveRLeft_Click" ImageUrl="../ahImages/arrowLeft.gif"
                                        CausesValidation="False" runat="server" meta:resourcekey="moveRleftResource1" />
                                </td>
                                <td>
                                    <asp:UpdatePanel ID="UP2" UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                        <%--<select id="RAssigned" class="MSL" multiple size="15" runat="server" ondblclick="document.getElementById('ahDirty').value=1; __doPostBack('moveRleft','');" />--%>
                                        <AdHoc:ScrollableListBox ID="RAssigned" runat="server" DblClickFunction="document.getElementById('ahDirty').value=1; __doPostBack('moveRleft','');"
                                            MultipleSelection="True" SelectHeight="202" SelectWidth="300" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="moveRright" EventName="Click" />
                                            <asp:AsyncPostBackTrigger ControlID="moveRleft" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                
                            </tr>
                        </table>
                    </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlParentID" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                
                <asp:UpdatePanel ID="UPSaveAS" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>                
                        <asp:Button runat="server" ID="Button1" style="display:none"/>
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                            TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                            DropShadow="False" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                        </ajaxToolkit:ModalPopupExtender>
                        
                        <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" style="display:none;width:430;">
                            <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                            <div class="modalPopupHandle">
                                <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                        <asp:Localize ID="PopupHeader" runat="server" Text="Save As New Permission" meta:resourcekey="LiteralResource7"></asp:Localize>
                                    </td>
                                    <td style="width: 20px;">
                                        <asp:ImageButton ID="imgClosePopup" runat="server" 
                                            OnClick="imgClosePopup_Click" CausesValidation="false"
                                            SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                            AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                                </td></tr></table>
                            </div>
                            </asp:Panel>
                            <div class="modalDiv">
                            <asp:UpdatePanel ID="upPopup" runat="server" >
                                <ContentTemplate>
                        <%--<asp:Panel ID="pnlSaveAs" runat="server" CssClass="detailpanel">--%>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Localize ID="Localize7" runat="server" meta:resourcekey="LiteralResource2" Text="Permission Name:"></asp:Localize>
                                    </td>
                                    <td>
                                        <input id="txtPermissionName" type="text" maxlength="100" size="50" runat="server" />
                                        <asp:RequiredFieldValidator ID="rfvtxtPermissionName" runat="server" ErrorMessage="Permission Name is required."
                                            ControlToValidate="txtPermissionName" ValidationGroup="SaveAs" meta:resourcekey="rtvPermissionResource1">*</asp:RequiredFieldValidator>
                                        <asp:CustomValidator ID="cvtxtPermissionName" runat="server" ErrorMessage="This permission name already exists in the database. Please select another permission name."
                                            ControlToValidate="txtPermissionName" ValidationGroup="SaveAs" OnServerValidate="IsSaveAsPermissionNameValid" EnableClientScript="false" meta:resourcekey="cvPermissionNameResource1">*</asp:CustomValidator>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <AdHoc:LogiButton ID="btnSPA_OK" runat="server" OnClick="OK_SPA" ValidationGroup="SaveAs"
                                        ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" Text="<%$ Resources:LogiAdHoc, OKWithSpaces %>" UseSubmitBehavior="false" />
                                        <AdHoc:LogiButton ID="btnSPA_Cancel" runat="server" OnClick="Cancel_SPA"
                                        ToolTip="<%$ Resources:LogiAdHoc, CancelTooltip1 %>" Text="<%$ Resources:LogiAdHoc, Cancel %>" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                            <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="SaveAs" runat="server" meta:resourcekey="vsummaryResource1" />
                            </ContentTemplate>
                    </asp:UpdatePanel>
                            </div>
                        </asp:Panel>
                        
                        <asp:ValidationSummary ID="vsummary" runat="server" ValidationGroup="Permission" meta:resourcekey="vsummaryResource1" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnCancel" />
                    </Triggers>
                </asp:UpdatePanel>

            <div id="divButtons" class="divButtons">
                <AdHoc:LogiButton ID="btnSave" runat="server" OnClick="SavePermission" UseSubmitBehavior="false"
                    ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" Text="<%$ Resources:LogiAdHoc, Save %>" ValidationGroup="Permission" />
                <AdHoc:LogiButton ID="btnSaveAs" runat="server" OnClick="SavePermissionAs" ValidationGroup="Permission"
                    ToolTip="<%$ Resources:LogiAdHoc, SaveTooltip1 %>" Text="<%$ Resources:LogiAdHoc, SaveAs %>" />
                <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="CancelPermission"
                    ToolTip="Click to cancel your unsaved changes and return to the previous page." Text="Back to Permissions"
                    CausesValidation="False" meta:resourcekey="btnCancelResource1" />
            </div>
<br />
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
        </div>
    </form>
</body>
</html>
