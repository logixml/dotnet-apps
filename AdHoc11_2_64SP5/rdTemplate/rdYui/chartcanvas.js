YUI.add('chartCanvas', function (Y) {
    "use strict";
    var Lang = Y.Lang,
        TRIGGER = 'rdChartCanvas';

    if (LogiXML.Ajax.AjaxTarget) {
        LogiXML.Ajax.AjaxTarget().on('reinitialize', function () { Y.LogiXML.ChartCanvas.createElements(true); });
    }

    Y.LogiXML.Node.destroyClassKeys.push(TRIGGER);

    Y.namespace('LogiXML').ChartCanvas = Y.Base.create('ChartCanvas', Y.Base, [], {

        _handlers: {},

        configNode: null,
        id: null,
        chart: null,
        reportName: null,
        renderMode: null,
        jsonUrl: null,
        chartPointer: null,
        refreshAfterResize: false,
        debugUrl: null,
        isUnderSE: null,

        initializer: function (config) {
            this._parseHTMLConfig();
            this.configNode.setData(TRIGGER, this);

            var options = this.configNode.getAttribute('data-options'),
                chartOptions = this.parseJson(options);
            if (!chartOptions) {
                return;
            }

            this._handlers.chartError = Highcharts.addEvent(this.configNode.getDOMNode(), 'error', Y.LogiXML.ChartCanvas.handleError);
            this._handlers.setSize = this.configNode.on('setSize', this.resized, this);

            this.initChart(chartOptions);
        },

        destructor: function () {
            var configNode = this.get('configNode');
            this._clearHandlers();
            this.chart.destroy();
            configNode.setData(TRIGGER, null);
        },

        _clearHandlers: function () {
            var self = this;
            Y.each(this._handlers, function (item) {
                if (item) {
                    item.detach();
                    item = null;
                }
            });
        },

        _parseHTMLConfig: function () {

            this.configNode = this.get('configNode');
            this.id = this.configNode.getAttribute('id');
            this.reportName = this.configNode.getAttribute('data-report-name');
            this.renderMode = this.configNode.getAttribute('deta-render-mode');
            this.jsonUrl = this.configNode.getAttribute('data-json-url');
            this.chartPointer = this.configNode.getAttribute('data-chart-pointer');
            this.refreshAfterResize = this.configNode.getAttribute('data-refresh-after-resize') == "True";
            this.debugUrl = this.configNode.getAttribute('data-debug-url');
            this.isUnderSE = this.configNode.getAttribute('data-under-se');
        },

        initChart: function (chartOptions) {
            //what about resizer?
            if(this.id) {
                var idForResizer = this.id.replace(/_Row[0-9]+$/g, "");
                if (Y.one('#rdResizerAttrs_' + idForResizer) && rdInitHighChartsResizer) {
                    rdInitHighChartsResizer(this.configNode.getDOMNode());
                }
            }
            
            //post processing
            if (this.renderMode != "Skeleton") {
                LogiXML.HighchartsFormatters.setFormatters(chartOptions, false);
            } else {
                this.createChart(chartOptions);
                this.chart.showLoading('<img src="rdTemplate/rdWait.gif" alt="loading..."></img>');
                this.requestChartData();
            }
        },

        createChart: function (chartOptions, fromPostProcessing) {

            if (this.chart) {
                this.chart.destroy();
            }

            //width and height by parent?
            var dataWidth = this.configNode.getAttribute('data-width'),
                dataHeight = this.configNode.getAttribute('data-height');
            if (dataWidth > 0 && dataHeight > 0) {
                chartOptions.chart.width = dataWidth;
                chartOptions.chart.height = dataHeight;
                //cleanup old size
                if (fromPostProcessing) {
                    this.configNode.removeAttribute('data-width');
                    this.configNode.removeAttribute('data-height');
                }
            }

            chartOptions.chart.renderTo = this.configNode.getDOMNode();

            if (chartOptions.series) {
                this.setActions(chartOptions.series);
            }

            if (chartOptions.tooltip) {
                chartOptions.tooltip.formatter = LogiXML.HighchartsFormatters.tooltipFormatter;
            }

            this.chart = new Highcharts.Chart(chartOptions);

            if (chartOptions.quicktips) {
                this.setQuicktipsData(chartOptions.quicktips);
            }

            if (chartOptions.autoQuicktip === false) {
                this.chart.autoQuicktip = false;
            }
        },

        requestChartData: function () {
            
            Y.io(this.jsonUrl, {
                on: {
                    success: function (tx, r) {
                        var parsedResponse = this.parseJson(r.responseText);
                        if (parsedResponse) {
                            LogiXML.HighchartsFormatters.setFormatters(parsedResponse);
                            this.createChart(parsedResponse, true);
                        }
                    },
                    failure: function (id, o, a) {
                        this.showError("ERROR " + id + " " + a);
                    }
                },
                context: this
            });
        },

        parseJson: function (jsonString) {
            var obj; 
            try {
                if (jsonString == "rdChart error") {
                    this.showError();
                    return;
                }
                obj = Y.JSON.parse(jsonString);
            }
            catch (e) {
                this.showError("JSON parse failed: " + jsonString);
                return;
            }
            return obj;
        },

        

        //createTrendLine: function (series, chartOptions) {
                
        //    var parentSeries = this.getParentSeries(chartOptions, series.parent);
        //    if (parentSeries) {
        //        var regression = this.getRegressionType(chartOptions, parentSeries);
        //        series.data = regression(this.extractXYData(parentSeries.data));
        //        series.type = "line";
        //    }

        //},

        //extractXYData: function (data) {
        //    var extrData = [];
        //    if (data && Object.prototype.toString.call(data) === '[object Array]' && data.length > 0) 
        //    {
        //        var dataType = Object.prototype.toString.call(data[0]),
        //            i = 0; length = data.length;
        //        if (dataType === '[object Array]') { //multi dim array [x, y, z]
        //            var isNum = !isNaN(data[i][0]);
        //            for (; i < length; i++) {
        //                extrData.push([isNum ? data[i][0] : i, data[i][1]]);
        //            }
        //        } else if (dataType === '[object Object]') { //{ y=0, x=0}
        //            var xExists = typeof data[i].x !== 'undefined';
        //            var isNum = !isNaN(data[i].x);
        //            for (; i < length; i++) {
        //                extrData.push([xExists && isNum ? data[i].x : i, data[i].y]);
        //            }
        //        } else { //one dim array
        //            for (; i < length; i++) {
        //                extrData.push([i, data[i]]);
        //            }
        //        }
        //        return extrData;
        //    } 
        //    return extrData;
        //},
        
        //getParentSeries: function (chartOptions, parentId) {
        //    for (var index = 0; index < chartOptions.series.length; index++) {
        //        var item = chartOptions.series[index];
        //        if (item.id === parentId)
        //            return item;
        //    }
        //    return null;
        //},
        
        //getRegressionType: function (chartOptions) {
        //    if (!chartOptions.yAxis || chartOptions.yAxis.length == 0)
        //        return LogiXML.Math.linearRegression;
        //    var chartYAxis = chartOptions.yAxis[0];
        //    return chartYAxis.type === "logarithmic" ?
        //    LogiXML.Math.expRegression :
        //    LogiXML.Math.linearRegression;
        //},

        //createPlotBandsAndLines: function (axisType, axises) {
        //    if (!axises || !Lang.isArray(axises)) {
        //        return;
        //    }

        //    var i = 0, xLength = axises.length, y = 0, yLength = 0;

        //    for (; i < xLength; i++) {
        //        if (axises[i].plotBands) {
        //            y = 0; yLength = axises[i].plotBands.length;
        //            for (; y < yLength; y++) {
        //                this.chart[axisType][i].addPlotBand(axises[i].plotBands[y]);
        //            }
        //        }
        //        if (axises[i].plotLines) {
        //            y = 0; yLength = axises[i].plotLines.length;
        //            for (; y < yLength; y++) {
        //                this.chart[axisType][i].addPlotLine(axises[i].plotLines[y]);
        //            }
        //        }
        //    }
        //},


        setQuicktipsData: function (quicktips) {
            if (!quicktips && quicktips.length == 0) {
                return;
            }

            var i = 0, length = quicktips.length;
            for (var i = 0; i < length; i++) {
                this.chart.series[quicktips[i].index].quicktip = quicktips[i];
            }
        },

        setActions: function (series) {
            var i = 0, length = series.length,
                options;
            for (; i < length; i++) {
                options = series[i];
                if (options.events) {
                    for (var event in options.events) {
                        if (Lang.isString(options.events[event])) {
                            options.events[event] = new Function('e', options.events[event]);
                        }
                    }
                }
            }
        },

        //setAxisLabelFormatter: function (axises) {
        //    if (!axises || !Lang.isArray(axises)) {
        //        return;
        //    }

        //    var i = 0, length = axises.length;

        //    for (; i < length; i++) {
        //        if (axises[i].labels && axises[i].labels.formatter) {
        //            switch (axises[i].labels.formatter) {
        //                case "labelFormatter":
        //                    axises[i].labels.formatter = Y.LogiXML.ChartCanvas.labelFormatter;
        //                    break;
        //            }
        //        }
        //    }
        //},

        resized: function (e) {
            if (this.chart && this.chart.options) {
                this.chart.setSize(e.width, e.height);
                if (e.finished) {
                    var requestUrl = null;
                    if (this.refreshAfterResize == true) {
                        requestUrl = 'rdAjaxCommand=RefreshElement&rdRefreshElementID=' + this.id + '&rdWidth=' + e.width + '&rdHeight=' + e.height + '&rdReport=' + this.reportName + '&rdResizeRequest=True&rdRequestForwarding=Form';
                    } else if (e.notify === undefined || (e.notify == true)) {
                        requestUrl = 'rdAjaxCommand=rdAjaxNotify&rdNotifyCommand=SetElementSize&rdWidth=' + e.width + '&rdHeight=' + e.height + '&rdElementId=' + this.id + '&rdReport=' + this.reportName + '&rdRequestForwarding=Form';
                    }
                    if (requestUrl !== null) {
                        if (this.isUnderSE === "True") {
                            requestUrl += "&rdUnderSuperElement=True"
                        }
                        rdAjaxRequest(requestUrl);
                    }
                }
            }
        },

        showError: function (message) {
            if (this.chart) {
                this.chart.destroy();
            }
            if (!message && this.debugUrl == "") {
                message = "Chart error";
            }
            if (message) {
                var errorContainer = Y.Node.create("<span style='color:red'></span>");
                errorContainer.setHTML(message);
                this.configNode.append(errorContainer);
            } else {
                var aLink, imgError;
                aLink = document.createElement("A")
                aLink.href = this.debugUrl
                //Make a new IMG inside of the anchor that points to the error GIF.
                imgError = document.createElement("IMG")
                imgError.src = "rdTemplate/rdChartError.gif"

                aLink.appendChild(imgError)
                this.configNode.append(aLink);
            }
        }

    }, {
        // Static Methods and properties
        NAME: 'ChartCanvas',
        ATTRS: {
            configNode: {
                value: null,
                setter: Y.one
            }
        },

        createElements: function (isAjax) {
            if (!isAjax) {
                isAjax = false;
            }

            var chart;

            Y.all('.' + TRIGGER).each(function (node) {
                chart = node.getData(TRIGGER);
                if (!chart) {
                    chart = new Y.LogiXML.ChartCanvas({
                        configNode: node,
                        isAjax: isAjax
                    });
                }
            });
        },

        handleError: function (e) {
            var chart = e.chart,
                code = e.code,
                errorText = '';
            switch (code) {
                case 10:
                    errorText = "Can't plot zero or subzero values on a logarithmic axis";
                    break;
                case 11:
                    //errorText = "Can't link axes of different type";
                    break;
                case 12:
                    errorText = "Chart expects point configuration to be numbers or arrays in turbo mode";
                    break;
                case 13:
                    errorText = "Rendering div not found";
                    break;
                case 14:
                    errorText = "String value sent to series.data, expected Number or DateTime";
                    break;
                case 15:
                    //errorText = "Chart expects data to be sorted for X-Axis";
                    break;
                case 17:
                    errorText = "The requested series type does not exist";
                    break;
                case 18:
                    errorText = "The requested axis does not exist";
                    break;
                case 19:
                    errorText = "Too many ticks";
                    break;
                default:
                    errorText = "Undefined error";
                    break;
            }
            if (errorText == '') {
                return;
            }
            var container = Y.one(chart.renderTo);
            var errorContainer = container.one(".chartError");
            if (!errorContainer) {
                errorContainer = container.append('<div class="rdChartCanvasError" style="display: inline-block; color:red" >Chart error:<ul class="chartError"></ul></div>').one(".chartError");
            }
            errorContainer.append('<li>' + errorText  + '</li>');
        }


    });

}, '1.0.0', { requires: ['base', 'event', 'node-event-simulate', 'event-synthetic', 'node-custom-destroy', 'json-parse', 'io-xdr'] });
