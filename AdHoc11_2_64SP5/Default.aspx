<%@ Page Language="VB" %>
<!DOCTYPE HTML>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Logi 11 Ad Hoc Reporting</title>
    <link rel="stylesheet" type="text/css" href="Login.css">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
</head>
<body onload="document.getElementById('username').focus()" onkeypress="if (event.keyCode==13){document.forms['frmLogin'].submit()}">
    <form name="frmLogin" method="post" target="_top" action="Gateway.aspx">
        <div id="container-box">
            <div id="brand">
                <img src="ahImages/loginBrand.png" />
                Logi 11 Ad Hoc Reporting
            </div>
            <div id="credentials">
            	<table id="labels" class="container" style="width: 290px">
            	    <tr>
            		<td><asp:Label ID="Label1" AssociatedControlID="username" Text="<%$ Resources:LogiAdHoc, UserName %>" runat="server">:</asp:Label></td>
					<td><input type="text" id="username" runat="server" name="username" size="20" maxlength="50" /></td>
                    </tr>
                    <tr>
            		<td><asp:Label ID="Label2" AssociatedControlID="password" Text="<%$ Resources:LogiAdHoc, Password %>" runat="server">:</asp:Label></td>
					<td><input type="password" id="password" runat="server" name="password" size="20" maxlength="100"/></td>
					</tr>
					<tr>
					<td></td>
					<td><input type="submit" value="Login" id="btnLogin" name="btnLogin"/></td>
					</tr>
                	<%--<input type="image" title="Login" alt="Login" src="ahImages/loginButton.gif" id="btnLogin" name="btnLogin">--%>
                </table>
                <%If Session("rdLogonFailMessage") <> "" Then%> 
                    <span class="err"><%=Session("rdLogonFailMessage")%></span>
                <%End If
                    Session("lgx_MenuID") = "MainHeader"
                %>
            </div>
        </div>
        <div id="logo">
        	<a href="http://logianalytics.com" title="Logi Analytics" target="_blank">Logi Analytics, Inc.</a>
        </div>
    </form>
</body>
</html>
