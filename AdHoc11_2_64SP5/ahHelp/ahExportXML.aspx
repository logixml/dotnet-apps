<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291624" class="title">Export to XML</a></h4>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image192.gif"></p>


<p class=MsoNormal><i>Export to XML</i> opens a new browser window and displays
all the data presented in the report in XML (Extensible Markup Language)
format.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image193.jpg"></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The report data is exported
to XML format and viewed by Internet Explorer.</span></p>


<p class=MsoNormal>Save the report data in XML format by clicking the File menu
and choosing <b>Save as</b>. Choose the filename and location, and then choose <b>XML
Files </b>from the drop-down menu. Click <b>Save </b>to create the XML file.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
