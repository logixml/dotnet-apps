<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291625" class="title">Add to Archive</a></h4>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image186.gif"></p>


<p class=MsoNormal><i>Add to Archive </i>creates a new copy of the current
report in the associated archive.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><img border=0
    src="Report_Design_Guide_files/image194.jpg"></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><img border=0
    src="Report_Design_Guide_files/image195.gif"></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><img border=0
    src="Report_Design_Guide_files/image196.jpg"></p>
  </td>
 </tr>
</table>

<p class=MsoNormal><span style='font-size:10.0pt'>A copy of the <i>Sales by
Employee w/Unit Price Greater Than or Equal to </i>report is added to the
archive</span></p>


<p class=MsoNormal>The date and timestamp are updated when the archiving footer
link is clicked. For this reason, the timestamp of the report at runtime may
not match the timestamp in the archive.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
