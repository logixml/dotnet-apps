<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2>Linking Reports</h2>


<p class=MsoNormal><span style=''>Administrators
have the ability to enable drill-through functionality by offering a hyperlink
in one report (a.k.a., source report) to launch the view of another report
(a.k.a., linked report). When the hyperlink in the source report is selected,
record-level specific data is passed from the source report to the linked
report's respective parameters.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse'>
 <tr>
  <td width=283 style='width:2.95in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style=''><img border=0  
  src="System_Admin_Guide_files/image119.jpg"></span></p>
  </td>
  <td width=48 style='width:.5in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style=''><img border=0  
  src="System_Admin_Guide_files/image120.gif"></span></p>
  </td>
  <td width=259 style='width:2.7in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal align=center style='text-align:center'><span
  style=''><img border=0  
  src="System_Admin_Guide_files/image121.gif"></span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>Linking two
existing reports is a two-part process:</span></p>

<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal><span style=''>Create the
     linked report. Any report may be the target of a link.</span></li>
 <li class=MsoNormal><span style=''>Configure a
     link on a data object's column which can be enabled in a source report.</span></li>
</ol>



<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><i><span style=''>Note:</span></i></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i><span style=''>Avoid
  using &quot;Ask&quot;<b> </b>parameters within linked reports.</span></i></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>To configure
a link on a data object's column, refer to the </span><b><span style=''>Setting Links</span></b><span
style=''> section of the System Administration
Guide for information about setting up a link parameter to a report created in
the application.</span></p>


<p class=MsoNormal><span style=''>Users
building reports have the option of disabling object links from the Report
Builder.</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
