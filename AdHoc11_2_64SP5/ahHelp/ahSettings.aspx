<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc263162791">Report Settings</a> </h2>


<p class=MsoNormal><span style=''>The <i>Report
Settings</i> page allows the administrator to optimize report performance and
appearance. Some settings determine the behavior of the application and take
effect immediately. Others, on the other hand, may only work for new or
re-built reports.</span></p>


<p class=MsoNormal><span style=''>Select <b>Report
Settings</b> from the <i>Report Configuration</i> drop-down list<b> </b>to
display the <i>Report Settings</i> configuration page.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>The
  following Report Settings image reflects the page when the Active SQL feature
  is enabled. See the Special Application Settings section of this document for
  more information on setting the Active SQL attribute in the _Settings.lgx
  file. </span></p>
  </td>
 </tr>
</table>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image103.jpg"></span></p>



<p class=MsoNormal><span style=''>Click on the <img
border=0   src="System_Admin_Guide_files/image104.jpg">icon to
display brief help with an attribute.</span></p>


<p class=MsoNormal><i><span style=''>Design-time
Validation</span></i><span style=''> is built
into several steps of the Report Builder to ensure that invalid values and bad
syntax do not affect reports at runtime. Design-time validation improves
usability by informing users of potential problems before they occur. Administrators
can disable this functionality to improve the performance of the Report Builder
but do so at their own risk.</span></p>


<p class=MsoNormal><i><span style=''>Apply data
format to excel exports</span></i><span style=''>
determines the formatting of columns when a report is exported to excel. The
following values can be selected from the dropdown</span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><b><span style=''>Always</span></b><span
     style=''>: The Excel columns will be
     formatted same as columns data type.</span></li>
 <li class=MsoNormal><b><span style=''>Exclude
     Dates</span></b><span style=''>: All the
     columns will be formatted with the exception of Date/Time columns</span></li>
 <li class=MsoNormal><b><span style=''>Never</span></b><span
     style=''>: No formatting will be applied.
     All columns will be formatted as string. </span></li>
</ul>


<p class=MsoNormal><i><span style=''>Max List
Records<sup>1</sup></span></i><span style=''>
specifies the maximum number of records returned when using <b>In List</b>
report parameters. It is recommended to keep this number as small as possible
to improve performance and keep drop-down lists manageable. Enter <b>0 </b>(zero)
to disable this feature. The maximum value is 64,000.</span></p>


<p class=MsoNormal><i><span style=''>Default
rows per report page</span></i><span style=''>
is the default number of rows shown in the first layer of each report. From the
Report Builder, the end user has the option of overriding the default value in
the Table Settings panel.</span></p>


<p class=MsoNormal><i><span style=''>Default rows
per subreport page</span></i><span style=''> is
the default number of rows shown in the drill-down layers of grouped reports.
From the Report Builder, the end user has the option of overriding the default
value in the Table Settings panel.</span></p>


<p class=MsoNormal><i><span style=''>Default
printable page size</span></i><span style=''>
is the default page size selected for printable paging in reports.</span></p>


<p class=MsoNormal><i><span style=''>Default
Template</span></i><span style=''> is the
stylesheet that will be selected by default for each new report. This is also
the stylesheet that will be used to display and verify any new Presentation
Styles.</span></p>


<p class=MsoNormal><span style=''>Click on the  <b>Save
</b>button to commit any changes.</span></p>



<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>The
  following Report Settings image reflects the page when the Active SQL feature
  is disabled. See the Special Application Settings section of this document
  for more information on setting the Active SQL attribute in the _Settings.lgx
  file.  Only the attributes that haven t been previously discussed in this Report
  Settings section are discussed.</span></p>
  </td>
 </tr>
</table>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image105.jpg"><img
border=0 width=560 height=390 src="System_Admin_Guide_files/image106.jpg"></span></p>


<p class=MsoNormal><i><span style=''>Max
Records</span></i><span style=''> specifies the
maximum number of records returned for a report. The number specified in the Max
Records field applies to reports and sub-reports. Database performance is
improved, but users may not see all the results in a report. Enter <b>0</b>
(zero) to disable this feature. The maximum number is 64,000.</span></p>


<p class=MsoNormal><i><span style=''>Show
message if maximum records reached</span></i><span style=''>
</span><span style=''>specifies whether or not
a warning will be displayed in the report when more rows are returned than is
allowed by the <i>Max Records</i> limit.</span></p>


<p class=MsoNormal><i><span style=''>Show
message if no records are returned</span></i><span style=''>
</span><span style=''>specifies whether or not
a warning will be displayed in the report when no rows are returned.</span></p>


<p class=MsoNormal><span style=''>If the end
user chooses to show number of records returned in a data section, the <i>Row
count message</i> will be displayed, with [row count] replaced with actual
number of records.</span></p>


<p class=MsoNormal><i><span style=''>Row count
message appearance</span></i><span style=''>
will apply the selected stylesheet class to the message.</span></p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
