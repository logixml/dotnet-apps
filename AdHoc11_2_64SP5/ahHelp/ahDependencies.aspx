<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291635" class="title">View Dependencies</a></h4>


<p class=MsoNormal>Ad Hoc allows the user to view the various dependencies for
the reports. The broad dependency categories are:</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Firm dependencies   items on which the report depends</li>
 <li class=MsoNormal>Loose dependencies   items that may be adjusted and still
     allow the report to run</li>
 <li class=MsoNormal>Firm relations   items that depend on the presence of the
     report</li>
 <li class=MsoNormal>Loose relations   items that may need the report to be
     present</li>
</ul>


<p class=MsoNormal>To view the dependencies report, hover the mouse over the <b>More</b>
button for the report and select <i>View Dependencies</i> from the list. </p>


<p class=MsoNormal>A fully collapsed view of a dependency page:</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image224.jpg"></p>


<p class=MsoNormal>Click on the <img border=0  
src="Report_Design_Guide_files/image225.jpg"> or the <img border=0 
height=13 src="Report_Design_Guide_files/image226.jpg"> icons to expand or
collapse a report section.</p>



</body>
</html>
