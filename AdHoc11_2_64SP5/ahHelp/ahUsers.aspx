<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc169593919"></a><a name="_Toc263162778">Users</a></h2>


<p class=MsoNormal><span style=''>A username
and password are required to access the application. Administrators assign each
individual a username and password. Users are members of a particular organization,
typically  Default , and have one or more assigned roles for access to specific
data objects and application features.</span></p>


<p class=MsoNormal><span style=''>Select <b>Users</b>
from the <i>User Configuration</i> drop-down list<b> </b>to display the <i>Users</i>
configuration page.</span></p>


<p class=MsoNormal><img border=0  
src="System_Admin_Guide_files/image036.jpg"></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#FFFF99;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><i><span style=''>Hint:</span></i></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i><span style=''>Change
  the default password for the <b>Admin </b>user after installing the
  application. The default password is &quot;password&quot;.</span></i></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>The <b>Add</b>
button will display an empty <i>User</i> page.</span></p>


<p class=MsoNormal><span style=''>The <b>Delete</b>
button will remove the selected users. Users are selected by clicking on the
applicable checkbox.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note</span></b><span
  style=''>:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>Deleting
  users from the application permanently erases any reports that user may have
  created, except for their shared reports. Any users created by the
  administrator can be edited and removed. If user's access is to be denied, it
  is recommended that their account be locked or change their password instead
  of deleting their account.</span></p>
  </td>
 </tr>
</table>



<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>Three actions
are available for a User; <b>Modify User, Delete User</b> and <b>Set Session Parameters.
</b>Hover the mousepointer over the <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon to show the available
actions.</span></p>


<p class=MsoNormal><i><u><span style=''>Adding
a User</span></u></i></p>


<p class=MsoNormal><span style=''>To add a
user, click on the <b>Add</b> button and the <i>User</i> page will be
displayed:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image037.jpg"><img
border=0 width=576 height=45 src="System_Admin_Guide_files/image038.jpg"></span></p>



<p class=MsoNormal><span style=''>The <i>Username</i>
is required and must be unique. A <i>Password</i> must be set for each user.
The <i>First Name </i>and<i> Last Name </i>are optional and are only used for
display purposes. The <i>Email Address</i> is optional as well, however, an
email address will be necessary for the delivery of scheduled reports. The <i>Locked</i>
checkbox may be used to restrict access to the Ad Hoc instance for the user. Although
a <i>User Role </i>does not have to be set when the user is created, one is
required for the user to access the Ad Hoc instance.</span></p>




<p class=MsoNormal><span style=''>The <i>Password</i>
is set by clicking the <b>Set Password</b> button and completing the following
dialog:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image039.jpg"></span></p>


<p class=MsoNormal><span style=''>After
entering the password twice, click on the <b>OK</b> button to store the
password information.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>The length,
  strength and change frequency are configurable by the System Administrator
  through the Management Console and through <i>Configuration/Application
  Configuration/Application Settings</i> in the Ad Hoc instance.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>A user must
have at least one role assigned to them to access the Ad Hoc instance. They may
have multiple roles assigned and will have the cumulative rights of all of
their assigned roles.</span></p>


<p class=MsoNormal><span style=''>Click on the <b>Save</b>
button to store the new User information. </span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<p class=MsoNormal><i><u><span style=''>Adding
a User Using  Save As </span></u></i></p>


<p class=MsoNormal><span style=''>The
administrator can create a user by modifying an existing user and then saving
it as a new user by clicking on the <b>Save As</b> button. </span></p>


<h5 align=left style='text-align:left'><i><span style='font-weight:normal'><span
 style='text-decoration:none'>&nbsp;</span></span></i></h5>

<h5 align=left style='text-align:left'><i><span style='font-weight:normal'>Modifying
Users</span></i></h5>


<p class=MsoNormal><span style=''>System
administrators can change a user s profile at any time. </span></p>


<p class=MsoNormal><span style=''>To change
user information, click the <b>Modify User</b> action for the user.  The <i>Modify</i>
<i>Users</i> page will be displayed:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image040.jpg"><img
border=0 width=469 height=33 src="System_Admin_Guide_files/image041.jpg"></span></p>


<p class=MsoNormal><span style=''>Modify the appropriate
information and click the <b>Save</b> button to store the information.</span></p>


<p class=MsoNormal><span style=''>To save the
edited information by a new <i>Username</i>, click on the <b>Save As</b>
button, enter the new name and click on the <b>OK</b> button to save the
information.</span></p>

<p class=MsoHeader>&nbsp;</p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>The <i>Selected
  User</i> drop-down list may be used to navigate through users and avoid using
  the <b>Back to Users</b> button and selecting a different user.</span></p>
  </td>
 </tr>
</table>


<b><u><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></u></b>

<h5 align=left style='text-align:left'><span style='text-decoration:none'>&nbsp;</span></h5>

<h5 align=left style='text-align:left'><i><span style='font-weight:normal'>Set
Session Parameters</span></i></h5>


<p class=MsoNormal><span style=''>To set the
session parameter values for a user, select the <b>Set Session Parameters</b>
action. The following page will be displayed:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image042.jpg"> </span></p>


<p class=MsoNormal><span style=''>The <i>Selected
User</i> dropdown list defaults to the user that was used to link to this page.
The dropdown list of users is a convenient mechanism to change the focus of the
page to a different user without having to go back to the previous page.</span></p>


<p class=MsoNormal><span style=''>The grid
presents all of the session parameters values defined for the user. Click on
the Parameter Name or Follow Default column headers to sort the contents of the
grid.</span></p>


<p class=MsoNormal><span style=''>The Restore
Defaults button will set the values of the selected session parameters to the
Default Value displayed in the grid.</span></p>


<p class=MsoNormal><span style=''>The <i>Follow
Default</i> checkbox indicates whether the parameter value should adopt the <i>Default
Value</i>. This also allows the parameter value to be set  permanently ,
meaning that changes to the <i>Default Value</i> will have no impact on the
parameter value for the user, if the <i>Follow Default</i> checkbox is
unchecked.</span></p>


<p class=MsoNormal><span style=''>The grayed <i>Parameter
Value</i> text boxes are not disabled. The gray is a visual cue that the value
is the same as the default value and is expected to follow the default value.
That mean that if the default value changes, the user will automatically pick
up the new value.</span></p>


<p class=MsoNormal><span style=''>The <i>Parameter
Value</i> may be changed by either typing directly into the text box or
selecting the<b> Modify</b> action and providing a new value. The parameter
value may also be changed by selecting the <b>Restore Default</b> action.</span></p>


<p class=MsoNormal><span style=''>If any <i>Parameter
Value</i> is set it will be considered an override value and the  <i>Follow
Default</i>  checkbox will automatically be unchecked. </span></p>


<p class=MsoNormal><span style=''>Click on the <b>Save</b>
button to store the session parameter values for the user.</span></p>

<span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span>



</body>
</html>
