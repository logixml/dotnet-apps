<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291630" class="title">Scheduling Reports</a></h4>


<p class=MsoNormal>The scheduling process is flexible and easy, offering the
ability to deliver reports via email in HTML format, attached in a document
(e.g., PDF, Word, Excel, CSV) or offering a link to the report in the archive
directory on the server.</p>


<p class=MsoNormal>Scheduling reports is typically a two step process; </p>

<p class=MsoNormal style='text-indent:.5in'>1) Specification of when the report
is scheduled to run and </p>

<p class=MsoNormal style='text-indent:.5in'>2) Identification of who should
receive it (subscribers). </p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#FFFF99;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial>Hint:</p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i>If the option to schedule reports is not presented, contact
  the system administrator. The ability to schedule reports is controlled by
  the role(s) assigned to the user.</i></p>
  </td>
 </tr>
</table>


<p class=MsoNormal>To schedule a report, hover the mouse over the <b>More</b>
button and select <i>Schedule</i> from the dropdown list. </p>


<p class=MsoNormal>If the report has no schedules associated with it, the
Schedules page will appear as:</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image208.jpg"></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>If a schedule already exists for the report, the <i>Schedules</i>
page will be presented.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image209.jpg"></p>


<p class=MsoNormal>Click on the <b>Add</b> button to create a new schedule for
the report.</p>


<p class=MsoNormal>Click on the <b>Delete</b> button to remove selected schedules
for the report. Schedules may be selected by clicking on the checkbox adjacent
to the schedule(s).</p>


<p class=MsoNormal>The Schedules list may be sorted by clicking on the <i>Frequency</i>
or <i>Schedule</i> column headers.</p>


<p class=MsoNormal>The three actions available for a schedule are <i>Modify
Schedule</i>, <i>Change Subscription</i> and <i>Run Schedule.</i> The <i>Run
Schedule</i> option will not be presented until subscribers have been added to
the schedule or if the report has expired.</p>


<p class=MsoNormal>Hover the mouse over the <img border=0  
src="Report_Design_Guide_files/image210.jpg"> icon and select the appropriate
action from the list. </p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>Some or all of the following options are available when
scheduling a new report:</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image211.jpg"></p>


<p class=MsoNormal>The <i>Output Format</i> determines how the report is
delivered in the email. HTML reports are embedded in the email and PDF, Word,
Excel and CSV reports are sent as attachments.</p>


<p class=MsoNormal>Reports can be added to the archive whenever the scheduler
delivers the report. If the report is added to the archive, the email contains
a link to the directory containing the archives. The report is not embedded or
sent as an attachment but rather saved to the archive directory.</p>


<p class=MsoNormal>The <i>Add to Archive</i> checkbox is only visible when
archiving is enabled by the System Administrator. If the <i>Add to Archive</i>
checkbox is selected, the <i>Output Format</i> will be set to the default
archive format specified by the System Administrator and the <i>Output Format</i>
list will be disabled. </p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>The following options are presented in the <i>Schedule Task</i>
list (with the default set to Daily):</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Once</li>
 <li class=MsoNormal>Daily</li>
 <li class=MsoNormal>Weekly</li>
 <li class=MsoNormal>Monthly</li>
</ul>


<p class=MsoNormal>As these options are selected, the subordinate gray dialog
will either be hidden or present options pertinent to the scheduling option
selected. If  Once  is selected, the gray dialog is hidden. Following are the
respective dialogs:</p>



<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image212.gif"></p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image213.gif"></p>


<p class=MsoNormal><span style='font-size:10.0pt'><img border=0 
 src="Report_Design_Guide_files/image214.gif"></span></p>



<p class=MsoNormal>The <i>Start Time</i> specified determines the time when the
scheduler will run the report. The hour and minute options are presented as
drop-down lists.</p>


<p class=MsoNormal>The <i>Start Date</i> specified determines the initial date
when the scheduler will run the report. The date may be manually keyed in or
populated from the calendar control by clicking on the <img border=0 
 src="Report_Design_Guide_files/image215.gif"> icon. </p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>The <i>End Date</i> checkbox allows the user to let the
report run indefinitely if unchecked. If the option is checked, the option to
specify a date to terminate report execution is presented. Specification of the
<i>End Date</i> is identical to the process for the <i>Start Date</i>. </p>


<p class=MsoNormal>The <i>Repeat Task</i> option allows a report to be run
repeatedly, beginning at the <i>Start Time</i> and <i>Start Date</i> specified
earlier. If the <i>Repeat Task</i> option is exercised, the <i>Interval</i> and
<i>Duration</i> for running the report may be specified. The <i>Interval</i> determines
the frequency and the <i>Duration</i> determines  for how long . Both values
must be greater than 0, and the <i>Interval</i> value must be less than the <i>Duration</i>
value. </p>


<p class=MsoNormal>The initial scheduling page presented was for a report that
did not require any user input. Following is a schedule page for a report that
expects user input when it is run:</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image216.jpg"></p>


<p class=MsoNormal>Since a scheduled report is run  unattended , parameter
values that would normally be supplied by the user (called  Ask  parameters)
must be set at the time of scheduling. The scheduling page is adjusted to allow
the specification of the parameter values to be used when the scheduled report
is run. Notice at the bottom of the schedule page a section labeled <i>Report
Input Parameters</i> is presented.</p>


<p class=MsoNormal>Reports may have multiple parameters defined. Every
parameter requiring user input must have values set. To set the values for a
parameter, click on the  <img border=0  
src="Report_Design_Guide_files/image068.gif"> icon and a <i>Parameter Details</i>
dialog will be presented.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image217.jpg"></p>

<p class=MsoNormal>Clicking on the  helper  icons such as the <img border=0
  src="Report_Design_Guide_files/image218.gif"> and  <img
border=0 width=16 height=16 src="Report_Design_Guide_files/image215.gif"> (when
the parameter is date-based) may assist in selecting the values. Click on the <b>Save
Parameter</b> button to save the values to be used for the associated parameter
when the report is run.</p>


<p class=MsoNormal>When the scheduling options have been completed, click on
the <b>Save</b> button at the bottom of the page to save the information and
create the scheduled task.</p>


<p class=MsoNormal>If the <i>Subscribed Users</i> list is not presented in the
schedule page, then no users have elected to receive the report.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image219.jpg"></p>

<p class=MsoNormal><span style='font-size:10.0pt'>Subscribed users are listed
at the bottom of the schedule page.</span></p>



<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#FFFF99;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><i>Hint:</i></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i>Test that the scheduled report is delivered as expected
  by first subscribing yourself and then running the report from the Scheduled
  Reports grid</i><i>. The speed of delivery depends on the size of the report
  and the local network configuration.</i></p>
  </td>
 </tr>
</table>



</body>
</html>
