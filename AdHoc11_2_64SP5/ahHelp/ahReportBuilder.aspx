<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291595" class="title">The Report Builder</a></h4>


<p class=MsoNormal>The Report Builder is a comprehensive and flexible interface
designed to build full-featured reports. </p>


<p class=MsoNormal>Click on the <b>New Report</b> button to access the Report
Builder.</p>


<p class=MsoNormal><i>Navigating the Report Builder</i></p>


<p class=MsoNormal>Following is a high level overview of the Report Builder and
the various components and options. The details of each component are covered
elsewhere in this document.</p>


<p class=MsoNormal>The core steps in the building of a report involve:</p>

<p class=MsoNormal> </p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>selecting a report template</li>
 <li class=MsoNormal>selecting a source of the data for the report, </li>
 <li class=MsoNormal>selecting the display elements, </li>
 <li class=MsoNormal>configuring the display elements, and </li>
 <li class=MsoNormal>reviewing the output.</li>
</ul>


<p class=MsoNormal>These steps will be exercised in this section just to show
the navigation options.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>After clicking the <b>New Report</b> button, a template
selection dialog will be presented.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image022.jpg"></p>


<p class=MsoNormal>Six templates are included in addition to the <i>Blank</i>
template The <i>Blank</i> template is initially selected. Click on a template
and then click on the <b>OK</b> button.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>The <i>Select Data</i> dialog will be presented to allow the
user to determine the data objects on which the report will be based.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image023.jpg"></p>


<p class=MsoNormal>The <i>Add/Remove</i> tab allows the user to select all of
the data objects that form the basis of the report. Until data objects have
been selected for the report the other tabs will be disabled.  The <i>Calculated
Columns</i> tab allows the user to create custom columns. The <i>Sort</i> tab
determines the initial sort sequence of the data returned from the reporting
database. The <i>Filter </i>tab allows the user to specify filter criteria for
the data.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><i>Notes:</i></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.5in;text-indent:-.25in'><i><span
  style='color:black'>1)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></i><i><span style='color:black'>Access to the Calculated
  Columns tab is controlled by a right/permission. The tab may not be presented
  to all users.</span></i></p>
  <p class=MsoNormal style='margin-left:.5in;text-indent:-.25in'><i><span
  style='color:black'>2)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></i><i><span style='color:black'>The Database dropdown may not
  be presented if there is only one reporting database or if Multiple
  Connections is not enabled</span></i></p>
  <p class=MsoNormal style='margin-left:.5in;text-indent:-.25in'><i><span
  style='color:black'>3)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></i><i><span style='color:black'>The Data Objects In dropdown
  list may not be presented if no object categories have been defined.</span></i></p>
  <p class=MsoNormal style='margin-left:.5in;text-indent:-.25in'><i><span
  style='color:black'>4)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></i><i><span style='color:black'>The Save as New option for the
  data source will not be presented if the Multiple Data Sources option has not
  been enabled</span></i></p>
  </td>
 </tr>
</table>


<p class=MsoNormal>The list of data objects available to the user may be
filtered by the <i>Data Objects in</i> dropdown list. These are categories of
data objects.</p>


<p class=MsoNormal>The data objects tree identifies the data objects available
to the end user. As objects are selected the tree will be refreshed to show the
related data objects. The columns of a data object can be viewed by expanding
the data object. Sample data for the object can be displayed by clicking on the
<img border=0   src="Report_Design_Guide_files/image024.jpg"> icon.</p>


<p class=MsoNormal>The <i>Information </i>panel will display any helpful
descriptions of the data object or column that the mouse is on. Information
will be presented only if the System Administrator has specified the
descriptions as part of the object/column configuration.</p>


<p class=MsoNormal>The <b>Preview Selected Data</b> button will display a
report dialog based on the data objects selected. From this dialog the
selections can be confirmed and all of the dialogs dismissed.</p>


<p class=MsoNormal>The <b>OK</b> button saves the currently selected data
objects and dismisses the dialog.</p>


<p class=MsoNormal>If the end user has access to multiple reporting databases
and the  (All)  option was selected on the Database filter, the <b>Modify Data
Source</b> dialog will allow the user to select data objects from any of the
reporting databases.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image025.jpg"></p>


<p class=MsoNormal>Notice in the above image that the Database filter is set to
 (All) . The tree of data objects includes two databases;  Northwind  and  Reporting
Metadata . The initial presentation of the tree in this scenario is normally
for all databases to be fully expanded.</p>



<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><i>Note:</i></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i><span style='color:black'>Presentation of all of the
  objects for all databases <b>does not</b> imply any relationships across
  databases. If the user selects a data object from the list, the tree will be
  refreshed with the related data objects.</span></i></p>
  </td>
 </tr>
</table>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>Once the data objects for the report have been selected, the
full Report Builder interface is available.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image026.jpg"></p>


<p class=MsoNormal>At the top of the page is a breadcrumb trail followed by a
help icon. To return to the <b>Personal Reports</b> list, click on that label
in the breadcrumb trail. Click on the <img border=0  
src="Report_Design_Guide_files/image027.jpg"> icon to receive a basic
description and help on the report wizard page.</p>


<p class=MsoNormal>Beneath the breadcrumb trail is the current name of the
report being created/edited. In this example, the report name is  New Report .
This would be the typical name of a report that has yet to be saved.</p>


<p class=MsoNormal>The <b>Save</b>, <b>Save As</b>, <b>Preview, Select Data</b>
and <b>Select Report Style</b> buttons follow.</p>


<p class=MsoNormal>The <b>Save</b> button will present a dialog initially that
allows the user to specify the report name, description, expiration date and
target folder for the report definition. Subsequent <b>Save</b> operations
require no confirmation.</p>


<p class=MsoNormal>The <b>Save As </b>button will present the same dialog as
the initial <b>Save</b> and allow the user to save the current report
definition to a new name and target.</p>


<p class=MsoNormal>The <b>Preview</b> button will launch the report in its
current state for review.</p>


<p class=MsoNormal>The <b>Select Data</b> button will display the same <b>Select
Data</b> dialog that was presented when creating the report. It will be
populated with the current settings.</p>


<p class=MsoNormal>The <b>Select Report Style</b> button will display a dialog
that allows you to select the stylesheet used to render the report. The default
stylesheet is determined by the System Administrator.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>On the left side of the display is an <b>ADD</b> frame that
contains the choices of display elements available to the user. The content of
the <b>ADD</b> frame may be different for each user depending on the user s
Role/Permissions/Rights.</p>


<p class=MsoNormal><img  
src="Report_Design_Guide_files/image028.jpg" align=left hspace=12>The display
elements may be included in the report by double-clicking on the element or
dragging the display element to the proper location in the <b>MODIFY</b> frame.</p>


<p class=MsoNormal>Only one header is permitted in a report.</p>


<p class=MsoNormal>The Table element presents data initially as a grid.</p>



<p class=MsoNormal>The Crosstab display element tabulates values and organizes
them by header and label.</p>



<p class=MsoNormal>The Chart option will present a list of charting options to
select from.</p>



<p class=MsoNormal>The Label option allows the user to annotate the report.</p>



<p class=MsoNormal>The image option allows the user to select an image to be
displayed in the report.</p>


<p class=MsoNormal>The Exports option allows the user to present a list of
export formats to the user running the report.  Typical options would be Word,
Excel, PDF, CSV and XML.</p>



<p class=MsoNormal><b>Note: The Exports option is not presented if the current
 mobile state  is set to On <img border=0  
src="Report_Design_Guide_files/image029.jpg"> . Typically the  mobile state  is
set to Off  <img border=0  
src="Report_Design_Guide_files/image030.jpg">. Exports are not supported for
reports rendered on mobile devices.</b></p>



<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>Adjacent to the <b>ADD</b> frame is the <b>MODIFY</b> frame.
The <b>MODIFY</b> frame determines the presentation order of the display
elements.</p>



<p class=MsoNormal><img  
src="Report_Design_Guide_files/image031.jpg" align=left hspace=12>In this
example the report will display a header, data table, chart, and export
options.</p>




<p class=MsoNormal>The red  X  allows you to remove a display element.</p>







<p class=MsoNormal>The <img border=0  
src="Report_Design_Guide_files/image032.jpg">icon allows you to switch the
chart type in the report. The same list of charting options will be presented
as adding a Chart to the report.</p>


<p class=MsoNormal>Display elements may be rearranged by dragging and dropping
a display element where it is needed.</p>


<p class=MsoNormal>Highlighting a display element will refresh the configuration
area to show the options related to the element.</p>



<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>

<p class=MsoNormal>Centered on the page is the tabbed <i>Configuration</i>
panel. Each tab allows the end user to configure some aspect of a display
element. Every display element will have at least one configuration tab
associated with it.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image033.jpg"></p>


<p class=MsoNormal>Each display element has a fixed set of configuration tabs.
Highlight the display element in the MODIFY frame to refresh the configuration
area with the tabs and content relevant to the highlighted element.</p>



<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>At the bottom of the page is the <i>Live Preview</i> panel
that will show the report as it being developed. As changes are made to the
configuration, the impact on the report can be viewed  live .</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image034.jpg"></p>


<p class=MsoNormal>This panel is initially collapsed. Click on the <img
border=0   src="Report_Design_Guide_files/image035.jpg"> or <img
border=0 width=18 height=19 src="Report_Design_Guide_files/image036.jpg"> buttons
to collapse or expand the panel in the current browser window. Click on the <img
border=0   src="Report_Design_Guide_files/image037.jpg"> to
display the report preview in a separate browser window.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>The <i>Live Preview</i> is refreshed with each change to
  the report configuration. The preview is a full rendering of the display
  element as it would appear in the report. Rendering the display element will
  impact the performance of the <i>Report Builder</i>, particularly for large
  volumes of data.</p>
  </td>
 </tr>
</table>



<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Notes:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>When creating
  a report, the <i>Live Preview</i> panel will not begin displaying anything
  until a Data Object has been selected.</p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>When exiting
  the <i>Report Builder</i>, the Ad Hoc will remember the state in which the <i>Live
  Preview</i> panel was last in, expanded or collapsed. Therefore, upon the
  next launching of the <i>Report Builder</i>, non-specific to any report, the Ad
  Hoc will display the <i>Live Preview</i> panel in the viewable state it was
  last in.</p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>If the <i>Live
  Preview</i> feature is not offered in the <i>Report Builder</i>, contact the
  System Administrator to enable it.</p>
  </td>
 </tr>
</table>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>After an extended period of inactivity, the web server
  will end a browser session and work may be lost. It is a good idea to save
  report modifications often to avoid this scenario.</p>
  </td>
 </tr>
</table>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>If changes to the report have not been saved, a
  confirmation dialog will be displayed when attempting to leave the page:</p>
  <p class=MsoNormal><img border=0  
  src="Report_Design_Guide_files/image038.jpg"></p>
  <p class=MsoNormal>Each browser will display some version of this
  confirmation.</p>
  </td>
 </tr>
</table>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
