<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc263162786"></a><a name="_Ref169919056">Relationships</a></h2>


<p class=MsoNormal><span style=''>The Relationships<b>
</b>page allows the system administrator to create join relationships for data
objects in the Ad Hoc instance. Join relations must be defined within the instance
because relationships that exist in the source database are not synchronized by
the Schema Wizard. Join relationships created in the Relationships<b> </b>page
are stored in the Ad Hoc metadata database; the reporting database is not
modified<i>.</i></span></p>


<p class=MsoNormal><span style=''>Relationships
impact the end user when they are selecting data objects in the Report Builder
to be included in the report. When they select the first data object, the list
of data objects is adjusted to show the selected data object and any related
data objects. Every time a data object is selected, the list of related data
objects is refreshed.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#FFFF99;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Hint:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>Administrators
  running Microsoft SQL Server, Oracle, Informix or Sybase as a reporting
  database can utilize the <b>Import Relationships</b> action in the Management
  Console to synchronize the instance with join relations from the reporting
  database. Refer to the <i>Management Console Usage Guide</i> for more
  details.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>Select <b>Relationships</b>
from the <i>Database Object Configuration</i> drop-down list<b> </b>to display
the <i>Relationships</i> configuration page.</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image082.jpg"></span></p>



<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>A list of
relationships is displayed allowing the system administrator to view
information associated with each relationship, such as the type of relation and
which objects are joined together.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i><span style=''>Main Data
  Object</span></i><span style=''> refers to
  the <b>left</b> data object and <i>Joined Data Object</i> refers to the <b>right</b>
  data object.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>The <b>Database</b>
dropdown list acts as a filter for the Relationship list. Only Relationships
related to the selected database will be displayed. If only one reporting
database has been configured for the Ad Hoc instance, the <b>Database</b>
filter will not be shown.</span></p>


<p class=MsoNormal><span style=''>The <b>Add</b>
button will display an empty <i>Relationship</i> page.</span></p>


<p class=MsoNormal><span style=''>The <b>Delete</b>
button will remove the selected relationships. Relationships are selected by
clicking on the applicable checkbox.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>Removing relationships
  marks any reports that depend on that relationship as broken. Delete
  relationships with caution. Use the <b>View Dependencies</b> option to
  determine the scope and usage of a relationship.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>The rows in
the relationship grid may be sorted by clicking on the <i>Relation Label, Main
Data Object, Relation Type,</i> or <i>Joined Data Object</i> column header.</span></p>


<p class=MsoNormal><span style=''>The  <img
border=0   src="System_Admin_Guide_files/image025.jpg"> icon
indicates that more than one action can be performed on the data object. Hover
the mouse over the  <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon to display the available
actions and click on the appropriate one.</span></p>


<p class=MsoNormal><span style=''>The available
actions for a relationship are <b>Modify Relationship, Delete Relationship</b>
and <b>View Dependencies.</b></span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>



<p class=MsoNormal><span style=''>The following
join types are available in the application:</span></p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><b><span style=''>Inner
     Join</span></b><span style=''> - Retrieve
     records when a match exists in both tables</span></li>
 <li class=MsoNormal><b><span style=''>Left
     Outer Join</span></b><span style=''> -
     Always retrieve records in the <i>left </i>table</span></li>
 <li class=MsoNormal><b><span style=''>Right
     Outer Join</span></b><span style=''> -
     Always retrieve records in the <i>right </i>table</span></li>
</ul>



<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#FFFF99;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Hint:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>Normally,
  only one relationship needs to be defined between two data objects. Although
  three relationship types are allowed, avoid creating more than one. In the
  event that more than one relationship type is created between the same two
  data object, attempt to make the relationship's label descriptive.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><i><u><span style=''>Adding
a new Relationship</span></u></i></p>


<p class=MsoNormal><span style=''>To create a
new relationship between two data objects, click on the <b>Add</b> button. A
blank <i>Relationship</i> page will be displayed.</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image083.jpg"><br
clear=all style='page-break-before:always'>
</span></p>

<p class=MsoNormal><span style=''>The <i>Database
Connection</i> identifies the currently selected reporting database. </span></p>


<p class=MsoNormal><span style=''>The specified
<i>Relation Label</i> should clearly identify the data objects and relationship
type; however that is a recommendation and not a requirement.</span></p>


<p class=MsoNormal><span style=''>The <i>Main
Data Object</i> and the <i>Joined Data Object</i> identify the two data objects
that are related. Select a data object from each of the respective lists.</span></p>


<p class=MsoNormal><span style=''>Select the <i>Join
Type</i> from the provided list.</span></p>


<p class=MsoNormal><span style=''>The <i>Auto
Reverse</i> checkbox indicates whether the relationship is  literal  or
 logical . Ad Hoc has the ability to logically relate the two data objects. If
a relationship is defined, the logical reverse of the relationship can be
assumed by enabling the checkbox.</span></p>


<p class=MsoNormal><span style=''>If the <i>Auto
Reverse</i> checkbox is unchecked, the relationship is  literal . When the user
selects the <i>Main Data Object</i> for their report the related <i>Joined Data
Object</i> will be shown; however, if the <i>Joined Data Object</i> is selected
for their report, the related <i>Main Data Object</i> is not shown. The
relationship only exists in one direction.</span></p>


<p class=MsoNormal><span style=''>The <i>Joined
Data Object Modified Label</i> and the <i>Main Data Object Modified Label</i>
are displayed in the Report Builder to help identify the data objects to the
end user. Enter a  nickname  for the data objects to be presented when the data
object available for selection in the Report Builder.</span></p>


<p class=MsoNormal><span style=''>Relationships
are also between columns in the data objects. Normally relationships are based
on a single pair of columns in the two data objects; however, compound
relationships based on multiple column pairs can be created.</span></p>


<p class=MsoNormal><span style=''>Select the
columns to be related from the drop-down lists for the <i>Main</i> and <i>Joined
Data</i> objects. For compound relationships, click on the <b>Add</b> button
and select the second pair of related columns. To remove column pairs, select
the pair of columns with the checkbox and click the <b>Delete</b> button that
becomes visible with more than one pair of related columns.</span></p>


<p class=MsoNormal><span style=''>Click on the <b>Save</b>
button to store the relationship in the metadata database.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Notes:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>If the <i>Main
  Data Object</i> drop-down menu is inactive, relationships can only be created
  from that specific object to another object.</span></p>
  </td>
 </tr>
</table>




<h5 align=left style='text-align:left'><i><span style='font-weight:normal'><span
 style='text-decoration:none'>&nbsp;</span></span></i></h5>

<h5 align=left style='text-align:left'><i><span style='font-weight:normal'>Modifying
Relationships</span></i></h5>


<p class=MsoNormal><span style=''>To modify a
relationship, hover the mouse over the  <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon to display the available
actions and click on the <b>Modify Relationship</b> action.</span></p>


<p class=MsoNormal><span style=''>The <i>Relationship</i>
page will be displayed.</span></p>


<p class=MsoNormal><span style=''>Modifications
to either the data objects or related columns will break the reports that rely
on them. It is strongly suggested that the <b>View Dependencies</b> report is
run to identify the scope and impact of potential relationship changes.</span></p>


<p class=MsoNormal><span style=''>Modifications
to the <i>Auto Reverse</i> flag may break reports.</span></p>


<p class=MsoNormal><span style=''>Modifications
to any of the labels will not impact the reports.</span></p>


<p class=MsoNormal><span style=''>Make the
necessary adjustments to the relationship and click on the <b>Save</b> button
to store the relationship in the metadata database.</span></p>



<p class=MsoNormal><i><u><span style=''>View
Dependencies</span></u></i></p>


<p class=MsoNormal><span style=''>To view the
dependencies for a relationship from the <i>Relationships</i> page, hover the
mouse over the  <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon to display the available
actions and click on the <b>View Dependencies</b> option. </span></p>


<p class=MsoNormal><span style=''>The option to
view dependencies of a relationship may help System Administrators know the
scope and impact of changes to a relationship.<br clear=all style='page-break-before:
always'>
</span></p>

</body>
</html>
