<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291598" class="title">Selecting a Data Source</a></h4>


<p class=MsoNormal>Generally the first step in the <i>Report Builder</i> is the
selection of the data source. The <i>Report Builder</i> automatically drives
the user to the <i>Select Data </i>dialog as part of creating a new report. The
same dialog is presented when the <b>Select Data</b> button is clicked.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image047.jpg"></p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>If the data objects have been categorized, a <i>Data Object
in</i> dropdown list will be presented and acts as a filter for the list of
data objects. The list of data objects may be changed by selecting a different
category from the list. All data objects may be displayed by selecting the
 (All)  option in the list.</p>


<p class=MsoNormal>If the end user has access to multiple reporting databases
and the  (All)  option was selected on the Database filter, the <b>Select Data</b>
dialog will allow the user to select data objects from any of the reporting
databases.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image048.jpg"></p>


<p class=MsoNormal>Notice in the above image that the Database filter is set to
 (All) . The tree of data objects includes two databases;  Northwind  and  Reporting
Metadata . </p>



<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><i>Note:</i></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i><span style='color:black'>Presentation of all of the
  objects for all databases <b>does not</b> imply any relationships across
  databases. If the user selects a data object from the list, the tree will be
  refreshed with the related data objects.</span></i></p>
  </td>
 </tr>
</table>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>To select a data object to be used in the report, click on
the checkbox adjacent to the data object. Every time a data object is selected
the tree of data objects is refreshed to display all of the related data
objects. Continue selecting data objects and click on the <b>OK</b> button to
save the selected items as a data source for the report.</p>


<p class=MsoNormal>The <b>Exclude duplicate rows</b> checkbox indicates that
only distinct rows should be returned from the database when the report is executed.
Rows having identical values for all selected columns will be excluded.</p>


<p class=MsoNormal>If the data source for the report needs to be adjusted,
click on the <b>Select Data</b> button, make the required changes in the <i>Select
Data</i> dialog, and click on the <b>OK</b> button to save the changes.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>Modifying a data source for the report may require
  reconfiguration of the attributes for a display element.</p>
  </td>
 </tr>
</table>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>Most reports are based on a single data source. All of the
  display elements share the same data source. By default Ad Hoc is configured
  to allow only a single data source per report; however, the administrator can
  configure Ad Hoc to allow the specification of multiple data sources for a
  report. See Chapter 13 for details on using multiple data sources in a
  report.</p>
  </td>
 </tr>
</table>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>Calculated Columns and Statistical columns are also data
  sources for a report and are reflected in the Data Source panel. Refer to
  Chapter 3 for details on Calculated Columns and Statistical Columns.</p>
  </td>
 </tr>
</table>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>If only one data object is selected, column references in
  the Report Builder will not identify the data object. If multiple data
  objects are selected, column references will be  Data Object.Column Name 
  notation.</p>
  </td>
 </tr>
</table>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
