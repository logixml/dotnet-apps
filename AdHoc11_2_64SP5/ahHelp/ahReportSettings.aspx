<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291618" class="title">Report Style</a></h4>


<p class=MsoNormal>To change the stylesheet associated with the report, click
on the Select Report Style button. The <i>Report Style</i> dialog will appear
as:</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image178.jpg"></p>


<p class=MsoNormal>Select a style from the dropdown list and click on the <b>OK</b>
button.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
