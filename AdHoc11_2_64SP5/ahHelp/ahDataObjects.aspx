<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc263162783">Data Objects</a></h2>


<p class=MsoNormal><span style=''>Data Objects
are core to the Ad Hoc reporting architecture. A data object is simply a
collection of related columns whose definition resides in the metadata
database.</span></p>


<p class=MsoNormal><span style=''>Most data
objects are created by importing all or part of a schema from a reporting database
using the Management Console. The database table and view definitions are
ported into the Ad Hoc metadata database. </span></p>


<p class=MsoNormal><span style=''>Data objects
may also be created by defining virtual views and catalogs within the Ad Hoc
interface. A virtual view is a SQL SELECT statement that identifies the
columns, tables, relationships and data transformations necessary for
reporting. A catalog is a pre-defined set of related data objects and columns.
Virtual views and catalogs only exist in the metadata database.</span></p>


<p class=MsoNormal><span style=''>The concept
of Data Object definitions existing in the metadata database is what allows the
System Administrator to customize the end user experience and reduce the
database specific technical knowledge required to produce complex reports. User
friendly names, pre-defined relationships, user-defined columns and data object
level filters are all designed to benefit the end user.</span></p>


<p class=MsoNormal><span style=''>The Data Objects
page allows the system administrator to customize data objects in the application.
An objects label, column-formatting options, column descriptions, access
rights, fixed parameters, user-defined columns and relationships may be
modified.</span></p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>Select <b>Data
Objects</b> from the <i>Database Object Configuration</i> drop-down list<b> </b>to
display the <i>Data Objects</i> configuration page.</span></p>


<p class=MsoNormal><b><span style='font-size:10.0pt;'><img
border=0   src="System_Admin_Guide_files/image049.jpg"></span></b></p>


<p class=MsoNormal><span style=''>Information
on the page may be sorted by clicking on the <i>Data Object, Friendly Name</i>
or <i>Type</i> column headings.</span></p>


<p class=MsoNormal><span style=''>The  <img
border=0   src="System_Admin_Guide_files/image025.jpg"> icon
indicates that more than one action can be performed on the data object. Hover
the mouse over the  <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon to display the available
actions and click on the appropriate one.</span></p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>The available
actions for a data object are:</span></p>


<p class=MsoNormal><b><span style=''>Modify
Data Object</span></b><span style=''>   to
change data object friendly name, description and visibility to the end user,
as well as the column display order, friendly name, description, default
format, default alignment, and hidden status as well as view the dependencies
for each column or the data object. User-defined columns may also be defined.</span></p>


<p class=MsoNormal><b><span style=''>Set
Relations</span></b><span style=''>   to define
relationships between the selected data object and other data objects and view
the relationship dependencies.</span></p>


<p class=MsoNormal><b><span style=''>Set Data
Object Access Rights</span></b><span style=''>
  to modify the data object access rights for each defined role associated with
the current reporting database.</span></p>


<p class=MsoNormal><b><span style=''>Set
Parameters</span></b><span style=''>   to
specify and manage one or more &quot;fixed parameters&quot; for the data object.
Fixed parameters are filters applied to the data object in every occurrence of
the data object's use. </span></p>


<p class=MsoNormal><span style=''>Data objects
based on stored procedures may have input parameters. Input parameters are
established when the procedure is imported into the metadata database and may
only be removed by removing the data object. Input parameters may be editable,
implying that the value may be supplied by the end user when a report that is
based on the procedure is executed.</span></p>


<p class=MsoNormal><b><span style=''>Set Links</span></b><span
style=''>   to configure links from columns to
a URL or report.</span></p>


<p style='line-height:120%'><b><span style=''>View
Dependencies</span></b><span style=''> -</span>
<span style=''>The Dependencies page presents
information about the content or usage of an item. The information is very
helpful for System Administrators that need to know usage information. </span></p>




<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<h5 align=left style='text-align:left'><i><span style='font-weight:normal'>Modifying
Data Objects</span></i></h5>


<p class=MsoNormal><span style=''>The Modify
Data Object page provides allows the System Administrator to:</span></p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><span style=''>Change the
     friendly name, description, and visibility of the data object</span></li>
 <li class=MsoNormal><span style=''>Change the
     friendly name, description, default format, default alignment and
     visibility of columns</span></li>
 <li class=MsoNormal><span style=''>Change the
     presentation order of the columns in the Report Builder</span></li>
 <li class=MsoNormal><span style=''>Manage
     user-defined columns</span></li>
 <li class=MsoNormal><span style=''>View the
     dependencies related to a column</span></li>
</ul>


<p class=MsoNormal><span style=''>Hover the
mouse over the  <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon to display the available
actions and click on the <b>Modify Data Object</b> option. The following page
will be presented:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image050.jpg"></span></p>


<p class=MsoNormal><span style=''>The <i>Friendly
Name</i> of the data object and the <i>Description</i> are presented in the
Report Builder when the user is selecting the data for a report.</span></p>


<p class=MsoNormal><span style=''>The <i>Hide
Data Object</i> checkbox determines whether the data object is visible when the
user is selecting data for a report. The system administrator may avail
themselves of other mechanisms to provide this data, such as Catalogs or
Virtual Views and may not want users accessing the data object directly.</span></p>


<p class=MsoNormal><span style=''>The <i>Friendly
Name</i> and <i>Description</i> of a column is presented in the Report Builder
when the user is selecting data for the report. The <i>Default Format</i> and
the <i>Default Alignment</i> drop-down lists set the initial format and
alignment, respectively, for a column in the Report Builder. The <i>Hidden</i>
checkbox determines whether the column is visible through the Report Builder
selection process. </span></p>


<p class=MsoNormal><span style=''>The <i>Use as
Primary Object</i> checkbox determines whether the data object is presented in
the initial list of data objects on the <b>Modify Data Source</b> dialog in the
Ad Hoc Report Builder. This allows the System Administrator to exercise more
control over the initial list of data objects presented to the user. A typical
use case would be to remove lookup tables from the initial list of objects.</span></p>


<p class=MsoNormal><span style=''>Columns are
presented to the end user in the Report Builder in the same order displayed in
the <i>Modify Data Objects</i> page. The columns may be presented
alphabetically by <i>Friendly Name</i> by clicking on the <i>Friendly Name</i>
column header. They may also be rearranged by using the drag-and-drop capability.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>Drag-and-drop
  capability exists throughout the Ad Hoc interface. Mousedown on the handle (<img
  border=0   src="System_Admin_Guide_files/image051.jpg">) at
  the left of each row in the grid, drag the row to the target location and
  release the mouse.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>Click on the <b>Save</b>
button to store the changes.</span></p>



<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Notes:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
  style=''>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span><span style=''>Choose
  appropriate values for <i>Default Format</i><b> </b>to avoid undesirable
  results when generating reports.</span></p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
  style=''>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span><span style=''>If HTML is
  selected as the <i>Default Format</i>, then an end-user will not be able to
  change the format of the column in the Report Builder to something else.</span></p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
  style=''>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span><span style=''>The 'Preserve
  line feed' format allows text in a memo type field to display as it is stored
  with line feeds (a.k.a., carriage returns) observed. This is the default format
  for data types of Long Text (e.g., LongVarWChar).</span></p>
  </td>
 </tr>
</table>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<h5 align=left style='text-align:left'><i><span style='font-weight:normal;
text-decoration:none'>Managing User-Defined Columns</span></i></h5>


<p class=MsoNormal><span style=''>The System
Administrator can extend the list of columns for a data object by creating
user-defined columns. The definition of a user-defined column must conform to
the SQL SELECT syntax of the reporting DBMS for a column. Typical uses of
user-defined columns include string formatting and concatenation, CASE
statements and in-row calculations.</span></p>


<p class=MsoNormal><span style=''>To create a
user-defined column, click on the <b>Add a User-defined Column</b> button. The
following dialog will be displayed:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image052.jpg"></span></p>


<p class=MsoNormal><span style=''>Enter a
unique <i>Column Name</i> and the <i>Definition </i>in the space provided.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>A
  user-defined column's <i>Definition</i> must be a valid SQL statement. If d</span><span
  style=''>esign-time validation is ON, this
  statement will be validated against the database to assure it is
  syntactically correct.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>If the
calculated column is based on an existing column, use the <i>Columns</i> list
to select the existing column and place it in the <i>Definition</i>.</span></p>


<p class=MsoNormal><span style=''>Clicking on
one of the <i>Operators</i> will place the operator into the <i>Definition</i>
at the last cursor position.</span></p>


<p class=MsoNormal><span style=''>Select the <i>Data
Type</i> of the expected result of the <i>Definition</i>.</span></p>


<p class=MsoNormal><span style=''>Click on the <b>OK</b>
button to validate the <i>Definition</i> and store the result.</span></p>


<p class=MsoNormal><span style=''>Once a user-defined
column has been created, it may be changed or removed by hovering the mouse
over the  <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon for the user-defined column
to display the available actions and click on either the <b>Modify Column</b>
or <b>Remove Column</b> option.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>The <b>Modify
  Column</b> or <b>Remove Column </b>actions are not an option for the original
  columns of the data object.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>Click on the <b>Save</b>
button of the <i>Modify Data Objects</i> page to commit the user-defined column
definition to the metadata database.</span></p>



<i><u><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></u></i>

<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<p class=MsoNormal><i><span style=''>View
Dependencies</span></i></p>


<p class=MsoNormal><span style=''>To view the
dependencies for a column from the Modify Data Objects page, hover the mouse
over the  <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon to display the available
actions and click on the <b>View Dependencies</b> option. The following page
will be presented:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image053.jpg"><img
border=0 width=463 height=339 src="System_Admin_Guide_files/image054.jpg"></span></p>


<p class=MsoNormal><span style=''>The content
of this page has been adjusted to fit on the page. The View Dependencies option
for a column presents a report of where the column is used throughout the Ad
Hoc instance. This report can help System Administrators determine the
potential scope and impact of changes to a column definition.<br clear=all
style='page-break-before:always'>
</span></p>


<h5 align=left style='text-align:left'><i><span style='font-weight:normal'>Adding
a Data Object Relationship</span></i></h5>


<p class=MsoNormal><span style=''>The System
Administrator may define relationships between data objects. When a
relationship exists between two data objects, selection of one of the data
objects will present the end user with the opportunity to select the related
data object in the Report Builder.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>Refer to
  the </span><b><span
  style=''>Relationships</span></b><span
  style=''> section of this guide for detailed
  information about creating and managing relationships.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>To define
relationships for a data object from the Data Objects page, hover the mouse
over the  <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon to display the available
actions and click on the <b>Set Relations</b> option. The following page will
be presented:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image055.jpg"></span></p>


<p class=MsoNormal><span style=''>A list of
relationships from the current data object to other data objects in the
database is displayed.</span></p>


<p class=MsoNormal><span style=''>Click the <b>Add
button </b>to create a relationship from the current object to another data
object in the database.</span></p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<h5 align=left style='text-align:left'><i><span style='font-weight:normal'>Setting
Data Object Access Rights</span></i></h5>


<p class=MsoNormal><span style=''>Data Object
Access Rights may be set for the data object or specific columns in the data
object for each Role associated with the current reporting database.  Data
Object Access Rights determine which data objects and columns are restricted
from use by a Role. By default, all data objects and columns are accessible by
all roles associated with the current reporting database.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><a name="OLE_LINK18"></a><a name="OLE_LINK17"><b><span
  style=''>Note:</span></b></a></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>For
  detailed help setting access rights, refer to the </span><i><span
  style=''>Setting Data Object Access Rights</span></i><span
  style=''> in the <i>Roles</i> section of this
  guide.</span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>To set the
access rights for a data object from the Data Objects page, hover the mouse
over the  <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon to display the available
actions and click on the <b>Set Data Object Access Rights</b> option. The
following page will be presented:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image056.jpg"><br>
<br>
</span></p>

<p class=MsoNormal><span style=''>Access rights
for the <i>Selected Data Object</i> may be set to full or none for a role by
selecting the role with the checkboxes and clicking on the <b>Set to Full</b>
or <b>Set to None</b> buttons, respectively.</span></p>


<p class=MsoNormal><span style=''>Alternatively,
click on the <img border=0  
src="System_Admin_Guide_files/image007.gif"> icon to <b>Modify Column Access
Rights</b> to display the <b>Column Access Rights</b> dialog and set the access
at the column level.</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<h5 align=left style='text-align:left'><a name="_Ref169938485"><i><span
style='font-weight:normal'>Specifying Fixed Parameters</span></i></a></h5>


<p class=MsoNormal><span style=''>Permanent
filters may be applied to a data object. These filters are called  fixed parameters 
and are transparent to the end user. Additional parameters may be applied as
the report is built and executed, but the end user cannot modify the fixed
parameter on the data object. </span></p>


<p class=MsoNormal><span style=''>To define and
manage fixed parameters for a data object, hover the mouse over the  <img
border=0   src="System_Admin_Guide_files/image025.jpg"> icon
to display the available actions and click on the <b>Set Parameters</b> option.
The following page will be presented:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image057.jpg"></span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>A fixed parameter
takes the form of an equation similar to:</span></p>


<p class=MsoFooter align=center style='text-align:center'><i>Label </i>is <i>Compared
</i>to <i>Value</i></p>


<p class=MsoFooter>where <i>label</i> represents a column name, <i>compared to</i>
represents a comparison operator, and <i>value</i> represents a threshold.</p>


<p class=MsoFooter>The available comparison operators are:</p>


</div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection2>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><span style=''>Equal to</span></li>
 <li class=MsoNormal><span style=''>Not equal
     to</span></li>
 <li class=MsoNormal><span style=''>Less than</span></li>
 <li class=MsoNormal><span style=''>Greater
     than</span></li>
 <li class=MsoNormal><span style=''>Less than
     or equal to</span></li>
 <li class=MsoNormal><span style=''>Greater
     than or equal to</span></li>
 <li class=MsoNormal><span style=''>Starts with
     <sup>1</sup></span></li>
 <li class=MsoNormal><span style=''>Does not
     start with <sup>1</sup></span></li>
 <li class=MsoNormal><span style=''>Ends with <sup>1</sup></span></li>
 <li class=MsoNormal><span style=''>Does not
     end with <sup>1</sup></span></li>
 <li class=MsoNormal><span style=''>Contains <sup>1</sup></span></li>
 <li class=MsoNormal><span style=''>Does not
     contain <sup>1</sup></span></li>
 <li class=MsoNormal><span style=''>Is null</span></li>
 <li class=MsoNormal><span style=''>Is not null</span></li>
 <li class=MsoNormal><span style=''>Between</span></li>
 <li class=MsoNormal><span style=''>Not between</span></li>
 <li class=MsoNormal><span style=''>In list</span></li>
 <li class=MsoNormal><span style=''>Not In List</span></li>
 <li class=MsoNormal><span style=''>Like <sup>1</sup></span></li>
 <li class=MsoNormal><span style=''>Not Like <sup>1</sup></span></li>
</ul>


</div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection3>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial>Notes:</p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><i><span
  style=''>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></i><i><span style=''>These
  operators are only available for data type of type String or Text.</span></i></p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><i><span
  style=''>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></i><i><span style=''>The
  operators available are dependent upon the column's data type. For example, a
  numeric data type would not include operators such as <b>true/false</b>.</span></i></p>
  </td>
 </tr>
</table>



<p class=MsoNormal><i><span style=''>Managing
Fixed Parameters</span></i></p>


<p class=MsoNormal><span style=''>Click on the <b>Add</b>
button to define a fixed parameter. The following dialog will be presented:</span></p>


<p class=MsoNormal><b><span style=''><img
border=0   src="System_Admin_Guide_files/image058.jpg"></span></b></p>


<p class=MsoNormal><span style=''>If fixed
parameters have been previously defined, the option to specify an And/Or
condition will be presented.</span></p>


<p class=MsoNormal><span style=''>Select the <i>Column</i>
from the drop-down list, an <i>Operator</i> from the drop-down list and either
provide a <i>Value</i> manually or use the magnifying glass to select a <i>Value</i>
from the reporting database.</span></p>


<p class=MsoNormal><span style=''>Click on the <b>OK</b>
button to store the fixed parameter information temporarily.</span></p>


<p class=MsoNormal><span style=''>After adding
all of the fixed parameters, click on the <b>Save</b> button on the Parameters
page to store the information in the metadata database.</span></p>


<p class=MsoNormal><span style=''>To edit or
remove a fixed parameter, hover the mouse over the  <img border=0 
 src="System_Admin_Guide_files/image025.jpg"> icon to display the
available actions and click on the <b>Edit Parameter</b> or<b> Remove Parameter</b>
option, respectively. <b><br clear=all style='page-break-before:always'>
</b></span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial>Notes:</p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><i><span
  style=''>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></i><i><span style=''>If the <b>In
  list </b>or <b>Not in list</b> operator is selected, then more than one value
  may be specified. If manually typing in each value, then follow each entry by
  the ENTER key.</span></i></p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><i><span
  style=''>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></i><i><span style=''>If the
  value is a number, the <b>Value </b>field must contain a valid number to
  build the report.</span></i></p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><i><span
  style=''>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;
  </span></span></i><i><span style=''>The
  Parameter functionality does not support conditions against data types of
  type Time. Date/Time data types are supported but their time portion will be
  ignored.</span></i></p>
  </td>
 </tr>
</table>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#FFFF99;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial>Hint:</p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i><span style=''>When
  adding multiple parameters, a logical operator (<b>And</b> or <b>Or</b>)
  becomes available for selection at the beginning of the next parameter. Use this
  operator to set the cumulative conditions for the parameters.</span></i></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>If the report
contains two or more parameters, the <img border=0  
src="System_Admin_Guide_files/image059.gif"> icon appears for each additional
parameter. The directional pad gives users the ability to create <i>levels </i>for
each parameter. Control the order of evaluation for multiple parameters using
the directional pad.</span></p>


<p class=MsoNormal><span style=''>Advanced data
filtering<i> </i>makes it possible to define groups of parameters that work
together to filter undesirable data. Multiple parameters can be defined to control
the order of evaluation. Filter data to control what users see at runtime.</span></p>


<p class=MsoNormal><span style=''>Fixed
parameters on data objects give administrators the ability to control the
content seen by end-users. Filter extraneous data by defining one or more
parameters that are evaluated at runtime. The directional pad control (<img
border=0   src="System_Admin_Guide_files/image059.gif">)
enables administrators the ability to control the order of evaluation.</span></p>


<p class=MsoNormal><span style=''>The
individual arrows of the control perform the following functions:</span></p>


<p class=MsoNormal style='text-indent:.5in'><span style=''><img
border=0   src="System_Admin_Guide_files/image060.gif" alt="*"> Shifts
a parameter one position higher in the list (retains indentation)</span></p>

<p class=MsoNormal style='text-indent:.5in'><span style=''><img
border=0   src="System_Admin_Guide_files/image061.gif" alt="*"> Shifts
a parameter one position lower in the list (retains indentation)</span></p>

<p class=MsoNormal style='text-indent:.5in'><span style=''><img
border=0   src="System_Admin_Guide_files/image062.gif" alt="*"> Indents
a parameter one position left</span></p>

<p class=MsoNormal style='text-indent:.5in'><span style=''><img
border=0   src="System_Admin_Guide_files/image063.gif" alt="*"> Indents
a parameter one position right</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>Parameters
  indented furthest to the right are evaluated first.</span></p>
  </td>
 </tr>
</table>


<span style='font-size:10.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><i><span style=''>Using
Session Parameters</span></i></p>


<p class=MsoNormal><span style=''>If session
parameters have been defined for the Ad Hoc instance, the fixed parameters
detail dialog may be slightly different. See the following picture:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image064.jpg"></span></p>


<p class=MsoNormal><span style=''>Notice that
there is a <i>Value</i> source dropdown. This dropdown is shown when there may
be session parameters used as part of the filter. The source is typically a
 Specific Value ; however, if session parameters have been defined,  Session
Parameter  may be selected as the <i>Value</i> source. When  Session Parameter 
is selected, a dropdown list of relevant session parameters is displayed.</span></p>


<p class=MsoNormal><span style=''>Session
parameters are one of five types; date, number, numeric list, text or textual
list. The dropdown list of session parameters will contain the session
parameters that match the data type of the <i>Column</i>. The list is also
restricted by the <i>Operator</i> selected.</span></p>


<p class=MsoNormal><span style=''>For date <i>Columns</i>,
the date session parameters will be shown in the list of available session
parameters.</span></p>


<p class=MsoNormal><span style=''>For numeric <i>Columns</i>,
either the number or numeric list session parameters will be shown in the list
of available session parameters. If the <i>Operator</i> is set to  In List  or
 Not In List , the numeric list session parameters will be shown, otherwise the
number session parameters will be shown.</span></p>


<p class=MsoNormal><span style=''>For text <i>Columns</i>,
either the text or textual list session parameters will be shown in the list of
available session parameters. If the <i>Operator</i> is set to  In List  or
 Not In List , the textual list session parameters will be shown, otherwise the
text session parameters will be shown.</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>In older
versions of Logi Ad Hoc, session parameters could also be used as the basis for
a fixed parameter on a data object as shown in the following picture:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image065.jpg"></span></p>


<p class=MsoNormal><span style=''>Notice that
the <i>Value</i> source is  Specific Value  and that an @Session token has been
supplied as the <i>Value</i>. While this technique is still supported in Logi
Ad Hoc, @Session tokens require a specific syntax and the session parameter
specified must match the session parameter name exactly, including case. There
is no type checking of the value and the SQL syntax generated may not be
appropriate in all cases.</span></p>


<p class=MsoNormal><span style=''>The preferred
method for using session parameters as the basis for a fixed parameter on a
data object is to select  Session Parameter  as the <i>Value</i> source and
selecting the appropriate session parameter from the available list.</span></p>

<b><u><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></u></b>

<h5 align=left style='text-align:left'><span style='text-decoration:none'>&nbsp;</span></h5>

<h5 align=left style='text-align:left'><i><span style='font-weight:normal'>Configuring
Input Parameters</span></i></h5>


<p class=MsoNormal><span style=''>Data objects
based on stored procedures and functions may have required input parameters.
Input parameters are simply values that the procedure needs in order to execute
properly.</span></p>


<p class=MsoNormal><span style=''>Since the
logic associated with the usage of the input parameters is contained within the
procedure definition, no operators may be specified for a parameter. The
Parameter Details dialog for an input parameter would appear as:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image066.jpg"></span></p>


<p class=MsoNormal><span style=''>The <i>Friendly
Name</i> that is displayed to the end user may be adjusted.</span></p>


<p class=MsoNormal><span style=''>The <i>Editable</i>
checkbox indicates that the parameter value will be obtained through an
automatic report filter. When the data object that has editable input
parameters is selected for use in a report, the <b>Modify Data Source</b>
dialog<b> Filter</b> tab will have associated filters automatically created.
See the <i>Filter</i> description in the <i>Report Design Guide</i>.</span></p>


<p class=MsoNormal><span style=''>The <i>Value</i>
may be set as a <i>Specific Value</i> or a <i>Session Parameter</i> may be
used. To enter a specific value, select <i>Specific Value</i> from the dropdown
list and enter the value in the space provided. To use a session parameter for
the <i>Value</i>, select <i>Session Parameter</i> from the dropdown list and
select one of the session parameters from the provided list.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>There are
  no indications of the proper format for an input parameter nor are there
  mechanisms to provide a list of viable options for the value.</span></p>
  </td>
 </tr>
</table>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<h5 align=left style='text-align:left'><a name="_Ref169946335"><i><span
style='font-weight:normal'>Setting Links</span></i></a></h5>


<p class=MsoNormal><span style=''>System
administrators can create automatic hyperlinks for each record in a specified
column. End users can access these links to open other web pages or drill
through to different reports. End users also have the option of enabling or
disabling the links when building reports.</span></p>


<p class=MsoNormal><span style=''>To define and
manage links from a data object, hover the mouse over the  <img border=0
  src="System_Admin_Guide_files/image025.jpg"> icon to display
the available actions and click on the <b>Set Links</b> option. The following
page will be presented:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image067.jpg"><br>
<br>
</span></p>


<p class=MsoNormal><span style=''>The list
contains the columns that are candidates for linking to either a linked report
or a URL.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>A column
  may only have one link defined. </span></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><span style=''>From the list
of columns for the data objects, hover the mouse over the  <img border=0
  src="System_Admin_Guide_files/image025.jpg"> icon to display
the available actions. The available actions will either be <b>Add Link</b> or <b>Edit
Link</b> and <b>Remove Link. </b>The following dialog will be displayed:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image068.jpg"></span></p>


<p class=MsoNormal><span style=''>Administrators
must first determine if the link points to an Ad Hoc report or an external URL.
</span></p>


<p class=MsoNormal><span style=''>To create an
object link to a URL, the <i>Link to Any URL</i> option must be selected. </span></p>


<p class=MsoNormal><span style=''>Enter the <i>Link
URL</i> information and click on the <b>Test URL</b> button to verify the
address. </span></p>


<p class=MsoNormal><span style=''>Click on the <b>Add
a Link Parameter</b> button and type in the parameter name. Choose the column
that will be the source of the data for the parameter from the drop-down menu. </span></p>


<p class=MsoNormal><span style=''>Click on the <b>OK</b>
button to save the information.</span></p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>To create a
link to an existing Ad Hoc report, select the <i>Link to an Ad Hoc Report</i>
option and the dialog will be re-displayed as follows (image shows the effect
of clicking on the <img border=0  
src="System_Admin_Guide_files/image069.gif"> icon):</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image070.jpg"></span></p>


<p class=MsoNormal><span style=''>Click the <img
border=0   src="System_Admin_Guide_files/image069.gif"> icon to
locate and choose a report from the list. After selecting the report, click <b>OK</b>.
</span></p>


<p class=MsoNormal><span style=''>Click on the <b>Add
a Link Parameter</b> button and click on the <img border=0  
src="System_Admin_Guide_files/image069.gif"> icon to select the target
parameter. Choose the column that will be the source of the data for the
parameter from the drop-down menu. </span></p>


<p class=MsoNormal><span style=''>Click on the <b>OK</b>
button to save the information.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><b><span style=''>Note:</span></b></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><span style=''>Linked
  reports must have at least one parameter to enable drill-through
  functionality between two reports where the content of the linked report is
  dependent upon data from the parent report. Links to a static report do not
  require link parameters. Refer to the </span><b><span
  style=''>Linking Reports</span></b><span
  style=''> section of this guide for more information
  about linked reports.</span></p>
  </td>
 </tr>
</table>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><i><u><span style=''>View
Dependencies</span></u></i></p>


<p class=MsoNormal><span style=''>To view the
dependencies for a data object from the <i>Data Objects</i> page, hover the
mouse over the  <img border=0  
src="System_Admin_Guide_files/image025.jpg"> icon to display the available
actions and click on the <b>View Dependencies</b> option. The following page
will be presented (a split view is presented here:</span></p>


<p class=MsoNormal><span style=''><img
border=0   src="System_Admin_Guide_files/image071.jpg"><img
border=0 width=576 height=435 src="System_Admin_Guide_files/image072.jpg"></span></p>


<p class=MsoNormal><span style=''>The option to
view dependencies of a data object may help System Administrators know the
scope and impact of changes to a data object.<br clear=all style='page-break-before:
always'>
</span></p>

</body>
</html>
