<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291623" class="title">Export to CSV</a></h4>


<p class=MsoNormal><img  
src="Report_Design_Guide_files/image184.gif" align=left hspace=12><br
clear=all>
</p>

<p class=MsoNormal><i>Export to CSV</i> opens a new browser window and displays
the report in comma-separated values within a spreadsheet (comma-delimited text
file).</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial>Note:</p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i>Reports containing drill-down groupings will not be
  expanded when exported.</i></p>
  </td>
 </tr>
</table>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#FFFF99;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial>Hint:</p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i>The CSV format is viewable by a variety of applications.
  It is best viewed by spreadsheet, database, and text-editing applications.</i></p>
  </td>
 </tr>
</table>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image191.gif"></p>

<p class=MsoNormal><span style='font-size:10.0pt'>The report is exported to CSV
format and viewed by the Microsoft Excel browser plug-in.</span></p>


<p class=MsoNormal>Save the report in CSV format by clicking the File menu and
choosing <b>Save as</b>. Choose the filename and location and click <b>Save</b>.
The default file type is <b>CSV </b>(comma delimited).</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
