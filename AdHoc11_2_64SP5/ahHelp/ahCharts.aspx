<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h3 class="title"></h3>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image141.jpg"></p>



<h5 align=left style='text-align:left'>Chart Settings</h5>


<p class=MsoNormal>The Report Builder charting components allow various types
of charts to be built to bolster a report. Charts provide a visual
representation of data utilizing different styles and types. The following
chart types are supported in the application:</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Pie</li>
 <li class=MsoNormal>Bar</li>
 <li class=MsoNormal>Line/Spline/Area</li>
 <li class=MsoNormal>Scatter</li>
</ul>


<p class=MsoNormal>In addition, animated versions of these chart styles are
available. Since animated charts are not exportable, they are presented in a
separate section of the interface.</p>


<p class=MsoNormal>To add a chart to a report, click on the <i>Insert</i> tab
to display the reporting elements and click on the <i><img border=0 
 src="Report_Design_Guide_files/image141.jpg"></i>option. The
following chart select panel will be displayed:</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image142.jpg"></p>


<p class=MsoNormal>Click on the desired chart type to add it to the report.</p>


<p class=MsoNormal>When a chart is added to a report definition, a tab called <i>Chart
Settings</i> is created with the appropriate attributes for the chart type. If
multiple charts are added, each chart will have an associated <i>Chart Settings</i>
tab</p>


<p class=MsoNormal>The <i>Chart Settings</i> tab will appear similar to the
following (Bar Chart example):</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image143.jpg"></p>


<p class=MsoNormal>Notice that a bar chart image was added to the Modify panel.
The <i>Chart Settings</i> tab, by default, displays the most common and minimum
attributes necessary to render a complete chart.</p>


<p class=MsoNormal>The <img border=0  
src="Report_Design_Guide_files/image144.jpg"> icon in the report layout panel
will delete the chart when clicked.</p>


<p class=MsoNormal>The <img border=0  
src="Report_Design_Guide_files/image145.jpg"> icon the report layout panel will
present the chart selection panel and allow a different chart type to be
selected. To the extent possible, the attributes of the current chart will be
replicated in the replacement chart.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>Click on the <i>Show All Attributes</i> icon to display the
advanced settings available for the chart. Using the same example the <i>Chart
Settings</i> tab will be redisplayed as:</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image146.jpg"></p>

<p class=MsoNormal>Notice the scroll bar down the right side of the <i>Chart
Settings</i> tab. Scroll down to view and set the advanced attributes for the
chart.</p>


<p class=MsoNormal>Chart attributes are grouped according to functional area.
Following are the general areas that may apply to charts:</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Label Column</li>
 <li class=MsoNormal>Label (x-axis) Column</li>
 <li class=MsoNormal>Label Scaling</li>
 <li class=MsoNormal>Data Column</li>
 <li class=MsoNormal>Data (y-axis) Column</li>
 <li class=MsoNormal>Data Scaling</li>
 <li class=MsoNormal>Legend</li>
 <li class=MsoNormal>Relevance</li>
 <li class=MsoNormal>Trend Line</li>
 <li class=MsoNormal>Crosstab Column</li>
 <li class=MsoNormal>Style</li>
 <li class=MsoNormal>Orientation</li>
 <li class=MsoNormal>Bubble</li>
</ul>

<p class=MsoNormal style='margin-left:.5in'>&nbsp;</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial>Note:</p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i>Not all charting attribute functional areas are
  available for all chart types. In addition, attributes in each functional
  area may be different based on the chart type selected and the data type of
  the selected columns.</i></p>
  </td>
 </tr>
</table>


<p class=MsoNormal>Three attributes are presented as stand-alone properties of
the chart; <i>Title, Show Data Values</i>, and <i>Allow Resizing</i>.</p>


<p class=MsoNormal>The <i>Title</i> attribute value will be displayed as a
chart title over the displayed chart.</p>


<p class=MsoNormal>The <i>Show Data Values</i> attribute will include the
numeric data values driving the chart elements in the chart display. By default
this attribute is enabled.</p>


<p class=MsoNormal>The <i>Allow Resizing</i> attribute will present resizing
bars around the displayed chart so that the end user can adjust the display.</p>



<p class=MsoNormal><i><u>Attribute Functional Areas</u></i></p>


<p class=MsoNormal><i>Label Column</i> area   The <i>Label Column </i>area
allows the user to select the column to be used for a pie chart label. Data
values will be aggregated based on this column.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image147.jpg"></p>



<p class=MsoNormal><i>Label Column (x-axis)</i> area   For X/Y charts, the
Label Column area allows the user to select the column to be displayed along
the x-axis, specify the caption for the x-axis and select the format.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image148.jpg"></p>

<p class=MsoNormal>If the Label Column for the x-axis is a date column, a drop
down list of time periods will be displayed (<i>Apply Time Period</i>). Data
will be aggregated according to the time period.</p>


<p class=MsoNormal>The options in the list include:</p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>None   display the date information on the x-axis and
     aggregate the values by date</li>
 <li class=MsoNormal>Year   display the year information on the x-axis and
     aggregate the values by year</li>
 <li class=MsoNormal>Quarter   display the first day of the quarter on the
     x-axis and aggregate the values by quarter</li>
 <li class=MsoNormal>Fiscal Quarter   display the first day of the fiscal
     quarter on the x-axis and aggregate the values by fiscal quarter</li>
 <li class=MsoNormal>Month   display the first day of the month on the x-axis
     and aggregate the values by month</li>
</ul>



<p class=MsoNormal><i>Label Scaling</i> area   the user may override the
default scaling of the x-axis by entering values for the <i>Lower</i> and<i>
Upper</i> <i>Bound</i> attributes. </p>


<p class=MsoNormal>If the column used for the x-axis content is numeric, the <i>Linear
Numeric</i> attribute is available and if enabled will provide fixed numeric intervals
along the x-axis.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image149.jpg"></p>


<p class=MsoNormal>If the column used for the x-axis content is date oriented,
the <i>Linear Time</i> attribute is available and if enabled will provide fixed
time intervals along the x-axis.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image150.jpg"></p>



<p class=MsoNormal><i>Data Column</i> area   The <i>Data Column</i> area allows
the user to select the data to be shown in the chart, select the format from
the suite of numeric formats, and select the aggregation function to be applied
to the data.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image151.jpg"></p>



<p class=MsoNormal><i>Data Column (y-axis)</i> area   For X/Y charts, the Data
Column area allows the user to select the column used for the data displayed
along the Y-axis. In addition the user can specify a caption for the Y-axis,
select a format for the data values, and select an aggregation function.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image152.jpg"></p>

</div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection14>



<p class=MsoNormal><i>Data Scaling </i>area - the user may override the default
scaling of the y-axis by entering values for the <i>Lower</i> and<i> Upper</i> <i>Bound</i>
attributes. </p>


<p class=MsoNormal><i><img border=0  
src="Report_Design_Guide_files/image153.jpg"></i></p>



<p class=MsoNormal><i>Legend</i> area   The Legend area allows the user to
indicate whether a legend should be displayed for the chart. For some chart
types the user may also select the legend position relative to the chart and
specify a legend label.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image154.jpg"></p>


<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<p class=MsoNormal><i>Relevance</i> area   a Relevance filter allows the user
to specify data thresholds to be included in the chart.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image155.jpg"></p>

<p class=MsoNormal>Enable/Disable a relevance filter by clicking on the <i>Use
Relevance Values</i> checkbox. Identify the relevance scale by clicking on
either the <i>Top N rows</i> or <i>Percentage</i> options. Enter the threshold
value in the area provided.</p>


<p class=MsoNormal>For example, only the top 75% of data in the chart could be
relevant. In this case, check the <i>Use Relevance Values</i> checkbox, enter a
value of <b>75</b> in the value field, and click the <i>Percentage</i> radio
button.</p>


<p class=MsoNormal>Alternatively, only the top 5 rows of data in the chart
could be relevant. In this case, enable the <i>Use Relevance Values</i>
feature, enter a value of <b>5</b> in the value.</p>



<p class=MsoNormal><i>Trend Line</i> area   the Trend Line area allows the user
to indicate that a trend line should be displayed on the chart.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image156.jpg"> </p>



<p class=MsoNormal><i>Crosstab Column</i> area   the <i>Crosstab Column</i>
area allows the user to identify a second column of data to be represented in
the chart.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image157.jpg"></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><i>Style</i> area   the Style attributes allow the report
designer the change the size and spacing regarding the display element. </p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image158.jpg"></p>


<p class=MsoNormal>The Size attribute drop-down list offers three standard
sizes; small, medium and large. Selecting one of the standard sizes will reset
the other size attributes. If the size attributes are over-ridden, the Size
attribute will be set to Custom.</p>



<p class=MsoNormal><i>Orientation</i> area   the Orientation attributes allow
the report designer to designate the direction of the bars in the bar charts.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image159.jpg"></p>


<p class=MsoNormal>The Bar Direction list offers horizontal and vertical
options.</p>



<p class=MsoNormal><i>Bubble</i> area   the <i>Bubble</i> area allows the user
to identify a column of data that will be used to determine the size of the
data points/bubbles in a scatter chart..</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image160.jpg"></p>

<p class=MsoNormal> </p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection15></div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection16>


</body>
</html>
