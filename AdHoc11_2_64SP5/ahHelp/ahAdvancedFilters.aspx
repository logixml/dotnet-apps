<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291601" class="title">Advanced Concepts in Data Filtering</a></h4>


<p class=MsoNormal>Advanced data filtering<i> </i>makes it possible to define
groups of parameters that work together to filter undesirable data from the
report. Users can define multiple parameters and control the order of
evaluation. Filter report data to control what users see at runtime.</p>


<p class=MsoNormal>Data filtering gives users the ability to control the
content of the report. Filter extraneous data from the report by defining one
or more parameters that are evaluated at runtime. The directional pad control (<img
border=0   src="Report_Design_Guide_files/image057.gif">)
enables users to control the order of evaluation.</p>


<p class=MsoNormal>The individual arrows of the control perform the following
functions:</p>


<p class=MsoNormal style='text-indent:.5in'><img border=0  
src="Report_Design_Guide_files/image060.gif" alt="*"> Shifts a parameter one
position higher in the list (retains indentation)</p>

<p class=MsoNormal style='text-indent:.5in'><img border=0  
src="Report_Design_Guide_files/image061.gif" alt="*"> Shifts a parameter one
position lower in the list (retains indentation)</p>

<p class=MsoNormal style='text-indent:.5in'><img border=0  
src="Report_Design_Guide_files/image062.gif" alt="*"> Indents a parameter one
position left</p>

<p class=MsoNormal style='text-indent:.5in'><img border=0  
src="Report_Design_Guide_files/image063.gif" alt="*"> Indents a parameter one
position right</p>


<p class=MsoNormal>As parameters are indented to the right, enclosing
parentheses appear to indicate the order of evaluation.</p>



<p class=MsoFooter>Users can also perform a row-level comparison with fields in
two different columns. In this case, the parameter takes the form of an
equation similar to:</p>


<p class=MsoFooter align=center style='text-align:center'><i>Label1 </i>is <i>Compared
</i>to <i>Label2</i></p>


<p class=MsoFooter>where <i>label1</i> represents the first column name, <i>compared
to</i> represents a comparison operator, and <i>label2</i> represents the
second column name.</p>


<p class=MsoNormal><b>To compare values from two different columns:</b></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>From the <i>Column</i> drop-down menu, select the desired column.
     </li>
 <li class=MsoNormal>From the <i>Operator</i> drop-down menu, select the
     desired comparison operator.</li>
 <li class=MsoNormal>From the <i>Value</i> type drop-down menu, select <i>Other
     Data Column</i>.</li>
 <li class=MsoNormal>From the <i>Value</i> field drop-down menu, select the
     desired column the comparison will be performed against.</li>
 <li class=MsoNormal>Click <b>OK </b>to add the parameter to the report.</li>
 <li class=MsoNormal>Click the <img border=0  
     src="Report_Design_Guide_files/image051.jpg"> icon adjacent to any
     parameter to delete that parameter from the report. Click the <img
     border=0   src="Report_Design_Guide_files/image056.gif"> icon
     to modify a parameter.</li>
</ol>

<p class=MsoNormal style='margin-left:.25in'>&nbsp;</p>

<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>When comparing two different columns, the Ask in Report
  checkbox is disabled.</p>
  </td>
 </tr>
</table>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><i><u>Using Session Parameters</u></i></p>


<p class=MsoNormal>If session parameters have been defined within the Ad Hoc
instance, the parameters detail dialog may be slightly different. See the
following picture:</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image064.jpg"></p>


<p class=MsoNormal>Notice that there is a <i>Value</i> source dropdown. The <i>Value</i>
source is typically a  Specific Value ; however, if session parameters have
been defined,  Session Parameter  may be selected as the <i>Value</i> source.
When  Session Parameter  is selected, a dropdown list of relevant session
parameters is displayed.</p>


<p class=MsoNormal>Session parameters are one of five types; date, number,
numeric list, text or textual list. The dropdown list of session parameters
will contain the session parameters that match the data type of the <i>Column</i>.
The list is also restricted by the <i>Operator</i> selected.</p>


<p class=MsoNormal>For date <i>Columns</i>, the date session parameters will be
shown in the list of available session parameters.</p>


<p class=MsoNormal>For numeric <i>Columns</i>, either the number or numeric
list session parameters will be shown in the list of available session
parameters. If the <i>Operator</i> is set to  In List  or  Not In List , the
numeric list session parameters will be shown, otherwise the number session
parameters will be shown.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>For text <i>Columns</i>, either the text or textual list session
parameters will be shown in the list of available session parameters. If the <i>Operator</i>
is set to  In List  or  Not In List , the textual list session parameters will
be shown, otherwise the text session parameters will be shown.</p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Note:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal>The ability to select session parameters from a list is
  dependent on whether the session parameter was defined within the Ad Hoc
  interface (see the System Administration Guide for setting session parameters
  within Ad Hoc) or defined outside of the Ad Hoc user interface. An example of
  the latter would be session parameters passed into Ad Hoc from a parent
  application.</p>
  <p class=MsoNormal>If the session parameter is defined outside of Ad Hoc, Ad
  Hoc would have no knowledge of the data type of the parameter. You may still
  refer to the session parameter by using the @Session token. For example,
  @Session.Country~ would refer to the externally defined session parameter.</p>
  <p class=MsoNormal>Usage of session parameters is discussed in the <a
  href="http://devnet.logianalytics.com/documents/adhoc/v10/1217SessionParams.pdf">Session
  Parameters Guide</a>.</p>
  </td>
 </tr>
</table>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>




</body>
</html>
