<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291615" class="title">Report Header</a></h4>


<p class=MsoNormal>By default, a report header is included in the standard
report template with the Date and Time options enabled.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image172.jpg"></p>

<p class=MsoNormal><span style='font-size:10.0pt'>Default report header</span></p>


<p class=MsoNormal>Click <img border=0  
src="Report_Design_Guide_files/image169.jpg"> from the Insert ribbon to add the
Header to a report. A Header tab will be created. A report may only have one
Header.</p>


<p class=MsoNormal><b><span style='font-size:10.0pt'><img border=0 
 src="Report_Design_Guide_files/image173.jpg"></span></b></p>

<p class=MsoNormal><span style='font-size:10.0pt'>Header Information tab with
Date and Time options enabled.</span></p>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><span style='font-style:normal'>Notes:</span></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>If a <i>Header</i>
  is added without the Date and Time options enabled, then only a report name
  will appear in the header.</p>
  <p class=MsoNormal style='margin-left:.25in'>&nbsp;</p>
  </td>
 </tr>
</table>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
