/****** AdHocLog table Create/Update    Last update: 5/31/07 ******/
Declare @Check int
Declare @sCommand varchar(200)

if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[AdHocLog]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
	begin
	CREATE TABLE [dbo].[AdHocLog] (
		[LogEntryID] [bigint] IDENTITY (1, 1) NOT NULL ,
		[ActionName] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
		[ActionTimeStamp] [datetime] NOT NULL ,
		[ActionDuration] [int] NULL ,
		[HostAddress] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
		[SessionGUID] [varchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
		[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
		[GroupName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
		[AffectedUserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
		[Affected] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
		[AppCode] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
		[ConnectionLabel] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
		[ReportID] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
		[ReportName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
		[ReportType] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
		[Query] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
		[SessionDuration] [int] NULL 
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
	end
else
	begin
/****** From update 8.0.0 ******/
	SET @Check = (Select Coalesce(Col_length('AdHocLog','AffectedUserName'),0))
	If @Check=0
		ALTER TABLE [AdHocLog] ADD 
			[AffectedUserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL

/****** From update 8.1.16 ******/
	SET @Check = (Select Coalesce(Col_length('AdHocLog','Affected'),0))
	If @Check=0
		ALTER TABLE [AdHocLog] ADD 
			[Affected] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL

	ALTER TABLE [AdHocLog] ALTER COLUMN [ReportID] [varchar] (255)

	ALTER TABLE [AdHocLog] ALTER COLUMN [ReportName] [varchar] (200)

/****** From update 9.2.17 ******/
	SET @Check = (Select Coalesce(Col_length('AdHocLog','AppCode'),0))
	If @Check=0
		ALTER TABLE [AdHocLog] ADD 
			[Appcode] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
	end

/****** From update 10.3.0 ******/	
	ALTER TABLE [AdHocLog] ALTER COLUMN [HostAddress] [varchar] (40)
GO

