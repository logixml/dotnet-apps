<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h4><a name="_Toc382291594" class="title">Setting Your Preferences</a></h4>


<p class=MsoNormal>Click <b>Profile Management </b>and then select the <i>Preferences</i>
tab to change your user preference settings.<br>
<br>
</p>


<p class=MsoNormal><img   id="Picture 14"
src="Report_Design_Guide_files/image020.jpg"><br>
<br>
</p>


<p class=MsoNormal><span style='color:black'>To update your preferences:</span></p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
style='color:black'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='color:black'>Select a Homepage Type, then:</span></p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='color:black'>a.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='color:black'>If <i>Application Page</i>, select a
web page from the application tree. If a report area is selected (e.g., Shared
Reports), then further refine your selection by:<br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:1.25in;text-indent:-1.25in'><span
style='color:black'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
style='color:black'>Click <b>Change Folder</b>.</span></p>

<p class=MsoNormal style='margin-left:1.25in;text-indent:-1.25in'><span
style='color:black'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
style='color:black'>Select a folder from the folder tree and then click <b>OK</b>.<br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='color:black'>b.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='color:black'>If <i>Report</i>, select a report by clicking
<b>Find Report</b> to view a list of reports<i> </i>and then click <b>OK</b><i>.</i><br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='color:black'>c.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span>If <i>URL</i>, then specify a URL address. <span
style='color:black'>Click <b>Test URL </b>to confirm that the URL can be
viewed.<br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='color:black'>d.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span>If <i>Pre-<span style='color:black'>defined Report</span></i><span
style='color:black'>, select a report using <b>Find Report</b> to view a list
of reports<i> </i>and then click <b>OK</b><i>.<br>
<br>
</i></span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
style='color:black'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='color:black'>If desired, check <i>Retain search
strings</i> to retain your last search criteria for any page even after a
logout.<br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
style='color:black'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='color:black'>If desired, check <i>Retain sort
preferences</i> to retain your last choices for sorting of grids in the
application.<br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
style='color:black'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='color:black'>Click <b>Save </b>to commit the changes
and return to the Profile page.<br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
style='color:black'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><i><span style='color:black'>OPTIONAL</span></i><span
style='color:black'>: Click <b>Restore Original Settings</b> to revert the
preference settings back to the default settings.</span></p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>If a Homepage is specified, the application will
  automatically display the designated Homepage immediately after logging into
  the application and when the Home link is selected.</p>
  </div>
  </td>
 </tr>
</table>

</div>


<span style='font-size:12.0pt;;color:black'><br
clear=all style='page-break-before:always'>
</span>


<h2><a name="_Toc456945099"></a><a name="_Toc382291595">The Report Builder</a></h2>


<p class=MsoNormal>The Report Builder is a comprehensive and flexible interface
designed to build full-featured reports. </p>


<p class=MsoNormal>Click <b>New Report</b> to access the Report Builder.<br>
<br>
</p>


<p class=MsoNormal><i>Navigating the Report Builder</i></p>


<p class=MsoNormal>The following is a high level overview of the Report Builder
and its components and options. The details of each component are covered
elsewhere in this document.</p>


<p class=MsoNormal>When using the Report Builder, the core steps in building a
report involve:</p>

<p class=MsoNormal> </p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Selecting a report template.</li>
 <li class=MsoNormal>Selecting a source of the data for the report.</li>
 <li class=MsoNormal>Selecting the display elements.</li>
 <li class=MsoNormal>Configuring the display elements. </li>
 <li class=MsoNormal>Reviewing the output.</li>
</ul>


<p class=MsoNormal>These steps will be presented in this section to show the
navigation options.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>After clicking the <b>New Report</b> button, a template
selection dialog box will be displayed:<br>
<br>
</p>


<p class=MsoNormal><img   id="Picture 297"
src="Report_Design_Guide_files/image021.jpg"></p>


<p class=MsoNormal>Six standard templates are included, in addition to the <i>Blank</i>
template. The <i>Blank</i> template is initially selected. Select a template
and then click <b>OK</b> to proceed.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>The <i>Select Data</i> dialog box will be displayed to allow
the user to select the data objects to be used in the report:</p>



<p class=MsoNormal><img   id="Picture 300"
src="Report_Design_Guide_files/image022.jpg"></p>


<p class=MsoNormal>The <i>Add/Remove</i> tab allows you to select all of the
data objects that form the basis of the report. Until data objects have been
selected for the report the other tabs will be disabled. </p>


<p class=MsoNormal>The <i>Calculated Columns</i> tab allows you to create
custom columns.</p>


<p class=MsoNormal>The <i>Sort</i> tab determines the initial sort sequence of
the data returned from the reporting database. </p>


<p class=MsoNormal>The <i>Filter </i>tab allows you to specify filter criteria
for the data.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>Access to the <i>Calculated Columns</i> tab is
  controlled by a right/permission. The tab may not be displayed to all users.<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>The <i>Database</i> drop-down may not be displayed
  if there is only one reporting database or if Multiple Connections has not
  been enabled.<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>The <i>Data Objects In</i> drop-down list may not be
  displayed if no object categories have been defined.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>The <i>Save as New</i> option for the data source will
  not be displayed if the Multiple Data Sources option has not been enabled.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>The list of data objects available to you may be filtered by
the <i>Data Objects in</i> drop-down list. These are categories of data
objects.</p>


<p class=MsoNormal>The data objects tree identifies the data objects available
to you. As objects are selected the tree will be refreshed to show the related
data objects. The columns of a data object can be viewed by expanding the data
object. Sample data for the object can be displayed by clicking the <img
  id="Picture 17" src="Report_Design_Guide_files/image023.jpg"> icon.</p>


<p class=MsoNormal>The <i>Information </i>panel will display any helpful
descriptions of the data object or column that the mouse in on. Information
will be displayed only if the System Administrator has specified the
descriptions as part of the object/column configuration.</p>


<p class=MsoNormal>The <b>Preview Selected Data</b> button will display a
report dialog box based on the data objects selected. From this dialog box the
selections can be confirmed and all of the dialog boxes dismissed.</p>


<p class=MsoNormal>The <b>OK</b> button saves the currently selected data
objects and dismisses the dialog box.</p>


<p class=MsoNormal>If you have access to multiple reporting databases and the
 (All)  option was selected in the Database filter, the <b>Modify Data Source</b>
dialog box will allow you to select data objects from any of the reporting
databases.</p>


<p class=MsoNormal><img   id="Picture 302"
src="Report_Design_Guide_files/image024.jpg"></p>


<p class=MsoNormal>Notice in the image above that the Database filter is set to
 (All) . The tree of data objects includes two databases;  Northwind  and  Reporting
Metadata . The initial presentation of the tree in this scenario is normally
for all databases to be fully expanded.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Presentation of all of the objects for all databases
  does not imply any relationships across databases. If the user selects a data
  object from the list, the tree will be refreshed with the related data
  objects.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>Once the data objects for the report have been selected, the
full Report Builder interface is available:</p>


<p class=MsoNormal><img   id="Picture 19"
src="Report_Design_Guide_files/image025.jpg"></p>


<p class=MsoNormal><br>
At the top of the page is a  breadcrumb trail  followed by a help icon. To
return to the <b>Personal Reports</b> list, click its label in the breadcrumb
trail. Click the <img   id="Picture 20"
src="Report_Design_Guide_files/image026.jpg"> icon to receive a basic
description of and help for the page.</p>


<p class=MsoNormal>Beneath the breadcrumb trail is the current name of the
report being created/edited. In this example, the report name is  New Report .
This is the standard name for a report that has yet to be saved.</p>


<p class=MsoNormal>The <b>Save</b> button presents a dialog box that allows the
user to specify the report name, description, expiration date and target folder
for the report definition. Subsequent <b>Save</b> operations require no
confirmation.</p>


<p class=MsoNormal>The <b>Save As </b>button presents the same dialog box and
allows you to save the current report definition to a new name and target.</p>


<p class=MsoNormal>The <b>Preview</b> button launches the report in its current
state for review.</p>


<p class=MsoNormal>The <b>Select Data</b> button displays the same <b>Select
Data</b> dialog box that was displayed when the report was created, populated with
the current settings.</p>


<p class=MsoNormal>The <b>Select Report Style</b> button displays a dialog box
that allows you to select the style sheet used to render the report. The
default style sheet is determined by the System Administrator.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>On the left side of the display is an <b>ADD</b> frame that
contains the choices of display elements available to the user. The content of
the <b>ADD</b> frame may be different for each user depending on their Role/Permissions/Rights.<br>
<br>
</p>


<p class=MsoNormal><img  
src="Report_Design_Guide_files/image027.jpg" align=left hspace=12>The display
elements in the ADD frame can be added to the report by double-clicking them or
dragging them to the proper location in the <b>MODIFY</b> frame.</p>


<p class=MsoNormal>Only one header element is permitted in a report.</p>

<p class=MsoNormal><br>
<br>
</p>

<p class=MsoNormal>The Table element initially presents data as a grid.</p>



<p class=MsoNormal>The Crosstab element tabulates values and organizes them by
header and label.</p>

<p class=MsoNormal><br>
<br>
<br>
</p>

<p class=MsoNormal>The Chart option presents a list of charting options.</p>



<p class=MsoNormal>The Label option allows you to annotate the report.</p>



<p class=MsoNormal>The image option allows you to select an image to be
displayed in the report.</p>



<p class=MsoNormal>The Exports option allows you to present a list of export
formats to the end-user running the report. Typical options include Word,
Excel, PDF, CSV, and XML.</p>





<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst>&nbsp;</p>
  <p class=NotesCxSpMiddle><b><img   id="Picture 21"
  src="Report_Design_Guide_files/image028.jpg"><img  
  id="Picture 22" src="Report_Design_Guide_files/image029.jpg"></b></p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpLast><b>Note: <br>
  </b>The Exports option is not displayed if the current  mobile state  is set
  to On. Typically the  mobile state  is set to Off. Exports are not supported
  for reports rendered on mobile devices.</p>
  </div>
  </td>
 </tr>
</table>

</div>



<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>Adjacent to the <b>ADD</b> frame is the <b>MODIFY</b> frame.
The <b>MODIFY</b> frame determines the presentation order of the display
elements.</p>



<p class=MsoNormal><img  
src="Report_Design_Guide_files/image030.jpg" align=left hspace=12>In this
example the report will display a header, a data table, a chart, and export
options.</p>




<p class=MsoNormal>The red  X  allows you to remove a display element.</p>










<p class=MsoNormal>The <img   id="Picture 23"
src="Report_Design_Guide_files/image031.jpg">icon allows you to switch the
chart type in the report. The same list of charting options will be displayed
as when adding a Chart to the report.</p>


<p class=MsoNormal>Display elements can be rearranged by dragging and dropping them
where needed.</p>


<p class=MsoNormal>Highlighting a display element will refresh the
configuration area to show the options related to the element.</p>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>





<p class=MsoNormal>Centered on the page is the tabbed <i>Configuration</i>
panel. Each tab allows the end-user to configure some aspect of a display
element. Every display element will have at least one configuration tab
associated with it.</p>


<p class=MsoNormal><img   id="Picture 24"
src="Report_Design_Guide_files/image032.jpg"></p>


<p class=MsoNormal>Each display element has a fixed set of configuration tabs.
Highlight the display element in the MODIFY frame to refresh the configuration
area with the tabs and content relevant to the highlighted element.</p>


<p class=MsoNormal>At the bottom of the page is the <i>Live Preview</i> panel which
will show the report in real-time as it s being developed. As changes are made
to the configuration, the impact on the report can be viewed  live .<br>
<br>
</p>


<p class=MsoNormal><img   id="Picture 25"
src="Report_Design_Guide_files/image033.jpg"></p>


<p class=MsoNormal>The Live Preview panel is initially collapsed. Click the  <img
  id="Picture 26" src="Report_Design_Guide_files/image034.jpg">  or
 <img   id="Picture 27"
src="Report_Design_Guide_files/image035.jpg">  buttons to collapse or expand
the panel in the current browser window. Click the  <img  
id="Picture 28" src="Report_Design_Guide_files/image036.jpg">  button to
display the report preview in a separate browser window.</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst>Notes:</p>
  <p class=NotesCxSpMiddle>The live preview is refreshed with each change to
  the report configuration. The preview is a full rendering of the display
  element as it would appear in the report. Rendering the display element will
  impact the performance of the Report Builder, particularly for large volumes
  of data.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>When creating a report, the Live Preview panel will
  not begin displaying anything until a Data Object has been selected.<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>When exiting the Report Builder, Ad Hoc will
  remember the state of the Live Preview panel, expanded or collapsed. When
  next launched, the Report Builder will display the Live Preview panel in the
  same state, regardless of the report being viewed.</p>
  <p class=NotesCxSpMiddle><br>
  If the Live Preview feature is not offered in the Report Builder, contact the
  System Administrator to enable it.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>After an extended period of inactivity, the web
  server will end a browser session and work may be lost. It is a good idea to
  save report modifications often to avoid this scenario.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>If changes to the report have not been saved, a
  confirmation dialog box will be displayed when attempting to leave the page:</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle><img   id="Picture 304"
  src="Report_Design_Guide_files/image037.jpg"></p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpLast>Each browser will display some version of this
  confirmation</p>
  </div>
  </td>
 </tr>
</table>

</div>




<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<h2><a name="_Toc382291596"></a><a name="_Toc297114512">The Dashboard Builder</a></h2>

<p class=MsoNormal>The <i>Dashboard Builder</i> is an interface designed to
build dashboards. </p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>The Dashboard Builder is <i>not</i> designed to build
  or modify reports.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>Click <b>New Dashboard</b> to access the Dashboard Builder<i>.</i></p>



<p class=MsoNormal><img   id="Picture 30"
src="Report_Design_Guide_files/image038.jpg"></p>


<p class=MsoNormal><i><br>
Navigating the Dashboard Builder</i></p>


<p class=MsoNormal>The core steps in building a dashboard are:</p>

<p class=MsoNormal> </p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Specifying a dashboard name.</li>
 <li class=MsoNormal>Configuring one or more dashboard panels.</li>
</ul>



<p class=MsoNormal>These steps will be presented in this section to show the
navigation options.</p>



<p class=MsoNormal>Across the top of the page is the  breadcrumb trail  - a
series of links to the pages and folders recently visited:<br>
<br>
</p>


<p class=MsoNormal><img   id="Picture 31"
src="Report_Design_Guide_files/image039.jpg"></p>


<p class=MsoNormal><br>
For example, clicking the Personal Reports link in the breadcrumb trail will
display the Reports page. If changes to the dashboard have not been saved, the
following confirmation dialog box will be displayed:</p>


<p class=MsoNormal><img   id="Picture 305"
src="Report_Design_Guide_files/image040.jpg"><br>
<br>
</p>


<p class=MsoNormal>Below the breadcrumb trail is dashboard management bar that
allows you to specify a <i>Dashboard Name</i>, save the dashboard, review the dashboard
using the <b>Preview Dashboard</b> button, and navigate back to the <i>Reports</i>
page with the <b>Back to Reports List </b>button.</p>


<p class=MsoNormal>Clicking the  <img   id="Picture 34"
src="Report_Design_Guide_files/image034.jpg">  or  <img  
id="Picture 35" src="Report_Design_Guide_files/image035.jpg">  buttons will
collapse or expand the Dashboard Settings panel.</p>


<p class=MsoNormal><img   id="Picture 36"
src="Report_Design_Guide_files/image041.jpg"></p>



<p class=MsoNormal>Below the dashboard management bar is the list of dashboard
panels. Initially only the <b>Add a Panel</b> button is displayed. When one or
more panels have been configured, the dashboard list will appear as:</p>


<p class=MsoNormal><img   id="Picture 37"
src="Report_Design_Guide_files/image042.jpg"></p>



<p class=MsoNormal>Click <b>Delete Panels</b> to remove any selected panels.
Panels are selected by checking its checkbox. All panels can be selected or
deselected by checking the checkbox in the list header.</p>


<p class=MsoNormal>Actions can be performed on each panel by hovering the mouse
over the <img   id="Picture 38"
src="Report_Design_Guide_files/image043.jpg">  icon to display the drop-down
list of available actions and then selecting the action. The available actions
are <i>Modify Dashboard Panel,</i> <i>Move Up,</i> and <i>Move Down</i>.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>Clicking either the <b>Add Panel</b> button or the <i>Modify
Dashboard Panel</i> action will display the <i>Panel Settings</i> dialog box.</p>



<p class=MsoNormal><img   id="Picture 306"
src="Report_Design_Guide_files/image044.jpg"></p>



<p class=MsoNormal>After entering the panel settings, click <b>Save Panel</b>
to temporarily save the panel definition and return to the list of panels.</p>


<p class=MsoNormal>The dashboard definition is permanently saved by clicking <b>Save
</b>or <b>Save As</b> in the <i>Dashboard Builder</i>. </p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>



<h1><a name="_Toc456945101"></a><a name="_Toc382291597">Data Sources</a></h1>


<p class=MsoNormal>Every report is based on a  data source , which refers to
the data objects and columns related to a reporting database and any calculated
columns and statistical columns defined for the report.</p>



<h2><a name="_Toc456945102"></a><a name="_Toc382291598">Selecting a Data Source</a></h2>


<p class=MsoNormal>Generally the first step in building a report is the
selection of the data source. The <i>Report Builder</i> automatically takes you
to the <i>Select Data </i>dialog box as part of creating a new report. The same
dialog box is displayed when <b>Select Data</b> is clicked:</p>



<p class=MsoNormal><img   id="Picture 308"
src="Report_Design_Guide_files/image045.jpg"></p>


<p class=MsoNormal><br>
If the data objects have been categorized, a <i>Data Object in</i> drop-down
list will be displayed and acts as a filter for the list of data objects. The
list of data objects can be changed by selecting a different category from the
list. All data objects can be displayed by selecting the  (All)  option in the
list.</p>

<p class=MsoNormal>If you have access to multiple reporting databases and the
 (All)  option was selected on the Database filter, the <b>Select Data</b> dialog
box allows you to select data objects from any of the reporting databases.</p>



<p class=MsoNormal><img   id="Picture 309"
src="Report_Design_Guide_files/image046.jpg"></p>


<p class=MsoNormal>Notice in the above image that the Database filter is set to
 (All) . As a result, the tree of data objects includes two databases;  Northwind 
and  Reporting Metadata . </p>

<p class=MsoNormal><br>
<br>
</p>

<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Presentation of all of the objects for all databases
  does not imply any relationships across databases. If the user selects a data
  object from the list, the tree will be refreshed with the related data
  objects.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>To select a data object to be used in the report, check the
checkbox adjacent to the data object. Every time a data object is selected, the
tree of data objects is refreshed to display all of the related data objects.
Continue selecting data objects and click <b>OK</b> when ready to save the
selected items as a data source for the report.</p>


<p class=MsoNormal>The <b>Exclude duplicate rows</b> checkbox indicates that
only distinct rows should be returned from the database when the report is
executed. Rows having identical values for all selected columns will be
excluded.</p>


<p class=MsoNormal>If the data source for the report needs to be adjusted, click
the <b>Select Data</b> button, make the required changes in the <i>Select Data</i>
dialog box, and click the <b>OK</b> button to save the changes.</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>Modifying a data source for the report may require
  reconfiguration of the attributes for a display element.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>Most reports are based on a single data source. All
  of the display elements share the same data source. By default, Ad Hoc is
  configured to allow only a single data source per report; however, the
  administrator can configure Ad Hoc to allow the specification of multiple
  data sources for a report. See the chapter about using multiple data sources
  for details.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>Calculated Columns and Statistical columns are also
  data sources for a report and are reflected in the Data Source panel. Refer to
  the next chapter for information about Calculated Columns and Statistical
  Columns.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpLast>If only one data object is selected, column references
  in the Report Builder will not identify the data object. If multiple data
  objects are selected, column references will be shown using  Data
  Object.Column Name  notation.</p>
  </div>
  </td>
 </tr>
</table>

</div>

</body>
</html>
