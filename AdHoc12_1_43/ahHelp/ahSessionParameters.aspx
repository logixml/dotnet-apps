<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221756"></a><a name="_Toc457221120">Session Parameters</a></h2>


<p class=MsoNormal><span style=''>Session
Parameters are documented in the Application Configuration chapter.</span></p>


<p class=MsoNormal><span style=''>Session
parameters are generally defined with  application  scope and are defined and
documented under the <i>Application Configuration</i> options. By default, the
link to the Session Parameter management pages is displayed in the <i>Application
Configuration</i> drop-down list.</span></p>


<p class=MsoNormal><span style=''>If the
ability to specify <i>Organizations</i> is enabled, the session parameter s values
can be applied specifically to an organization. In this case, the scope of
Session Parameters is closer to <i>User Configuration</i> and the link to the
Session Parameter management pages is displayed in the <i>User Configuration</i>
drop-down list.</span></p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<h2><a name="_Toc457221797"></a><a name="_Toc457221161">Session Parameters</a></h2>


<p class=MsoNormal><span style=''>The Session
Parameters screen allows the System Administrator to create one or more <i>session
variables. </i> The session parameters simply relate values to a name which can
be used in a @Session token. They are commonly used in data object filters, but
can be used throughout the Ad Hoc instance.</span></p>


<p class=MsoNormal><span style=''>Session
variables are part of the .NET Framework and hold a specific value for the
duration of the user session. Each session variable is initialized with a
specified value when users log in to the application. The value is either the
default value set on this page, a value that has been set for the organization,
or a value passed to the Ad Hoc instance from a parent application.</span></p>


<p class=MsoNormal><span style=''>Select <b>Session
Parameters</b> from the <i>Application Configuration</i> drop-down list<b> </b>to
display the <i>Session Parameters</i> configuration page:</span></p>



<p class=MsoNormal><img border=0   id="Picture 150"
src="System_Admin_Guide_files/image102.jpg"></p>



<p class=MsoNormal><span style=''>Click <b>Add</b>
to display an empty <i>Session Parameter Details</i> dialog box.</span></p>


<p class=MsoNormal><span style=''>Click<b> Delete</b>
to remove the selected session parameter. Session parameters are selected by checking
their checkboxes.</span></p>


<p class=MsoNormal><span style=''>Click the <i>Parameter
Name</i> column header to sort the list.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpMiddle>Number and Numeric List session parameters must have
  a default value.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>The following syntax can be used to reference a
  session parameter:</p>
  <p class=NotesCxSpLast><span style='font-size:11.0pt;'> @Session.&lt;parameter_name&gt;~</span><span
  style='font-size:11.0pt'> </span>where &lt;parameter_name&gt; must exactly
  match the session parameter name, including character case.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>




<h3><a name="_Toc457221798"></a><a name="_Toc457221162">Adding a Session
Parameter</a></h3>

<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<p class=MsoNormal><span style=''>To add a
session parameter, click the <b>Add</b> button to open the following dialog box:
</span></p>


<p class=MsoNormal><img border=0   id="Picture 366"
src="System_Admin_Guide_files/image103.jpg"></p>


<p class=MsoNormal><span style=''>The <i>Parameter
Name</i> value is the name by which the session parameter will be referenced throughout
the application.</span></p>


<p class=MsoNormal><span style=''>The <i>Type</i>
value defines one of the parameter types recognized by Logi Ad Hoc. The <i>Type</i>
will be used along with the usage context to determine which session parameters
to display to the end-user. For example,  Text  session parameters should not
be displayed as options in the Logi Ad Hoc user interface when the context
clearly calls for a numeric or date value.</span></p>


<p class=MsoNormal><span style=''>The <i>Type</i>s
include:</span></p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><span style=''>Date   The
     session parameter value is expected to be a date and the parameter will be
     available in date contexts.</span></li>
 <li class=MsoNormal><span style=''>Number   The
     session parameter value is expected to be a number and the parameter will
     be available in numeric contexts.</span></li>
 <li class=MsoNormal><span style=''>Numeric
     List   The session parameter value is expected to be a list of numbers and
     the parameter will be available for numeric  In List/Not In List  contexts.</span></li>
 <li class=MsoNormal><span style=''>Text   The
     session parameter value is expected to be a string of characters and will
     be available in string contexts.</span></li>
 <li class=MsoNormal><span style=''>Textual
     List   The session parameter is expected to be a list of string values and
     will be available for text based  In List/Not in List  contexts.</span></li>
</ul>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>When one of
the  list  types is selected, the dialog box will be adjusted to allow
specification of a list of values, as shown below:</span></p>



<p class=MsoNormal><img border=0   id="Picture 367"
src="System_Admin_Guide_files/image104.jpg"></p>



<p class=MsoNormal><span style=''>If a  Date 
type is selected, the dialog box will be adjusted and a date picker control
will be displayed as shown below:<br>
<br>
</span></p>


<p class=MsoNormal><img border=0   id="Picture 368"
src="System_Admin_Guide_files/image105.jpg"></p>


<p class=MsoNormal><span style=''>The <i>Default
Value</i> attribute, in the absence of any overrides based on Organization or
User, will be supplied to the application when the session parameter is
resolved during the execution of reports.</span></p>


<h3><a name="_Toc457221799"></a><a name="_Toc457221163">Modifying a Session
Parameter</a></h3>


<p class=MsoNormal><span style=''>The Session
Parameters page presents a list of session parameters similar to the following:</span></p>


<p class=MsoNormal><img border=0   id="Picture 154"
src="System_Admin_Guide_files/image106.jpg"></p>


<p class=MsoNormal><span style=''><br>
Session parameters can be sorted by clicking the <i>Parameter Name</i> column
header.</span></p>


<p class=MsoNormal><span style=''>Four actions
are available for each session parameter: <b>Modify Session Parameter</b>, <b>Delete
Session Parameter,</b> <b>Set by Organization</b>, and <b>Set by User</b>.</span></p>


<p class=MsoNormal><span style=''>Selecting the
<b>Modify Session Parameter</b> action will present a dialog box to capture the
new session parameter value. For text and numeric type session parameters, the dialog
box appears like this:</span></p>

<p class=MsoNormal><span style=''><br>
<br>
</span></p>

<p class=MsoNormal><img border=0   id="Picture 369"
src="System_Admin_Guide_files/image107.jpg"></p>

<p class=MsoNormal><span style=''>Edit the <i>Parameter
Name</i>, <i>Type</i>, or <i>Default Value</i> values as desired and click <b>OK</b>
to save the information.</span></p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes</b>: </p>
  <p class=NotesCxSpMiddle>Changing the <i>Default Value</i> will change the
  attribute value for all Organizations and Users that are following the
  default values. </p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>Any changes made are immediate and may affect the
  current session. It s not necessary to logout and re-establish the session to
  impact the current user. Other user sessions are not affected by the changes
  until the user s login.</p>
  <p class=NotesCxSpLast>&nbsp;</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal><span style=''>Selecting the
<b>Set by Organization</b> action will present a page similar to the following:</span></p>



<p class=MsoNormal><span style=''><img
border=0   id="Picture 156"
src="System_Admin_Guide_files/image108.jpg"></span></p>



<p class=MsoNormal><span style=''>The <i>Selected
Session Parameter</i> defaults to the session parameter that was used to
navigate to the page. It s also a convenient method of switching the session
focus for the page without having to return to the list of session parameters.</span></p>


<p class=MsoNormal><span style=''>The <i>Default
Value</i> and <i>Type</i> of the session parameter are displayed for informational
purposes.</span></p>


<p class=MsoNormal><span style=''>A list of
organizations is displayed in a grid. The grid can be sorted by clicking on
either the <i>Organization</i> or <i>Follow Default</i> column headers.</span></p>


<p class=MsoNormal><span style=''>The <b>Restore
Default</b> button will set the <i>Parameter Value</i> to the displayed <i>Default
Value</i> for all selected Organizations.</span></p>


<p class=MsoNormal><span style=''>The <b>Set
Value</b> button will present a dialog box to capture a new value and apply the
value to all of the selected Organizations.</span></p>


<p class=MsoNormal><span style=''>The <i>Follow
Default</i> checkbox specifies whether the parameter value should adopt the
Default Value. This also allows the parameter value to be set  permanently ,
meaning that changes to the <i>Default Value</i> will have no impact on the
parameter value for the organization, if the <i>Follow Default</i> checkbox is
unchecked.</span></p>


<p class=MsoNormal><span style=''>The grayed <i>Parameter
Value</i> text boxes are not disabled. Their gray color is a visual cue that
the value is the same as the default value and is expected to follow the default
value. That means that if the default value changes, the organization will automatically
pick up the new value.</span></p>


<p class=MsoNormal><span style=''>The <i>Parameter
Value</i> can be changed by either typing directly into the text box or
selecting the<b> Modify</b> action and providing a new value. The parameter
value may also be changed by selecting the <b>Restore Default</b> action.</span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the session parameter values for the Organization.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note</b>: </p>
  <p class=NotesCxSpLast>The <b>Set by Organization</b> action will not be displayed
  from the <i>Session Parameters</i> page if the Logi Ad Hoc instance has not
  been configured to allow the specification of Organizations.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal><span style=''>Selecting the
<b>Set by User</b> action will present a page similar to the following:</span></p>


<p class=MsoNormal><span style=''><img
border=0   id="Picture 157"
src="System_Admin_Guide_files/image109.jpg"></span></p>


<p class=MsoNormal><span style=''><br>
The <i>Selected Session Parameter</i> defaults to the session parameter that
was used to navigate to the page. It s also a convenient method for switching
the session focus for the page without having to return to the list of session
parameters.</span></p>


<p class=MsoNormal><span style=''>The <i>Type</i>
attribute identifies the session parameter type: date, number, numeric list,
text, or textual list.</span></p>


<p class=MsoNormal><span style=''>The <i>Organization</i>
drop-down list in the main menu (not shown) is used to filter the user list by
their respective organization. The <i>Organization</i> drop-down list is only
displayed in the page when the Logi Ad Hoc instance is configured to allow
multiple organizations and when more than one organization exists.</span></p>


<p class=MsoNormal><span style=''>The <i>Default
Value</i> reflects the current default value for the session parameter. The
actual value can be the original application scope session parameter value or
the organization scope parameter value.</span></p>


<p class=MsoNormal><span style=''>The <i>Role</i>
drop-down list allows the list of users to be filtered by role.</span></p>


<p class=MsoNormal><span style=''>A list of
users is displayed in the grid. The grid can be sorted by clicking on either
the <i>User</i> or <i>Follow Default</i> column headers.</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><span style=''>Click <b>Restore
Default</b> to set the <i>Parameter Value</i> to the displayed <i>Default Value</i>
for all selected Users.</span></p>


<p class=MsoNormal><span style=''>Click <b>Set
Value</b> to display a dialog box to capture a new value and apply the value to
all of the selected Users.</span></p>


<p class=MsoNormal><span style=''>The <i>Follow
Default</i> checkbox indicates whether the parameter value should adopt the <i>Default
Value</i>. This also allows the parameter value to be set  permanently ,
meaning that changes to the <i>Default Value</i> will have no impact on the
parameter value for the user, if the <i>Follow Default</i> checkbox is
unchecked.</span></p>


<p class=MsoNormal><span style=''>The grayed <i>Parameter
Value</i> text boxes are not disabled. Their gray color is a visual cue that
the value is the same as the default value and is expected to follow the default
value. That means that if the default value changes, the user will
automatically pick up the new value.</span></p>


<p class=MsoNormal><span style=''>The <i>Parameter
Value</i> can be changed by either typing directly into the text box or
selecting the<b> Modify</b> action and providing a new value. The parameter
value may also be changed by selecting the <b>Restore Default</b> action.</span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the session parameter values for the User.</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal style='margin-left:.5in'><span style=''>&nbsp;</span></p>

</body>
</html>
