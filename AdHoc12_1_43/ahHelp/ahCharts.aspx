<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h1><a name="_Toc456945116"></a><a name="_Toc382291612">Charts</a></h1>



<p class=MsoNormal>The Report Builder provides charting components to provide a
visual representation of data in a variety of styles. The following chart types
are supported in the application:</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Area</li>
 <li class=MsoNormal>Bar</li>
 <li class=MsoNormal>Heatmap</li>
 <li class=MsoNormal>Line</li>
 <li class=MsoNormal>Pie</li>
 <li class=MsoNormal>Scatter</li>
</ul>

<p class=MsoNormal style='margin-left:.5in'>&nbsp;</p>

<p class=MsoNormal>In addition, 3-D and animated versions of selected chart
styles are available. Since animated charts are not exportable, they are displayed
in a separate section of the interface.</p>


<p class=MsoNormal>From the Report Builder interface, click the Chart icon in
the ADD frame at the left of the interface to add the Crosstab component to the
report. Use drag-and-drop methods to place a chart into the report in a
specific location in the MODIFY panel.</p>


<p class=MsoNormal>The following palette of charts options will be displayed:</p>


<p class=MsoNormal><br>
<br>
</p>


<p class=MsoNormal><img border=0   id="Picture 350"
src="Report_Design_Guide_files/image141.jpg"></p>


<p class=MsoNormal>Click the desired chart type to add it to the report.</p>


<p class=MsoNormal>When a chart is added to a report definition, a tab called <i>Chart
Settings</i> is created with the appropriate attributes for the chart type. If
multiple charts are added, each chart will have an associated <i>Chart Settings</i>
tab</p>


<p class=MsoNormal>The <i>Chart Settings</i> tab will appear similar to the
following Bar chart example:</p>


<p class=MsoNormal><img border=0   id="Picture 177"
src="Report_Design_Guide_files/image142.jpg"></p>


<p class=MsoNormal><br>
Notice that a Bar chart image was added to the Modify panel. The <i>Chart
Settings</i> tab, by default, displays the most common and minimal attributes
necessary to render a complete chart.</p>


<p class=MsoNormal>The chart can be deleted by clicking the <img border=0
  id="Picture 178" src="Report_Design_Guide_files/image143.jpg"> icon
in the report layout panel.</p>


<p class=MsoNormal>The <img border=0   id="Picture 179"
src="Report_Design_Guide_files/image144.jpg"> icon the report layout panel will
present the chart selection panel again and allow a different chart type to be
selected. To the extent possible, the attributes of the current chart will be
replicated in the replacement chart.</p>



<p class=MsoNormal><b>Setting Chart Attributes</b></p>


<p class=MsoNormal>Clicking the <i>Show All Attributes</i> icon will display
the advanced settings available for the chart: </p>



<p class=MsoNormal><img border=0   id="Picture 180"
src="Report_Design_Guide_files/image145.jpg"></p>

<p class=MsoNormal>Notice the scroll bar down the right side of the <i>Chart
Settings</i> tab. Scroll down to view and set the advanced attributes for the
chart.</p>


<p class=MsoNormal>Chart attributes are grouped according to functional area.
Following are the general areas that may apply to charts:</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Label Column</li>
 <li class=MsoNormal>Label (x-axis) Column</li>
 <li class=MsoNormal>Label Scaling</li>
 <li class=MsoNormal>Data Column</li>
 <li class=MsoNormal>Data (y-axis) Column</li>
 <li class=MsoNormal>Data Scaling</li>
 <li class=MsoNormal>Legend</li>
 <li class=MsoNormal>Relevance</li>
 <li class=MsoNormal>Trend Line</li>
 <li class=MsoNormal>Crosstab Column</li>
 <li class=MsoNormal>Style</li>
 <li class=MsoNormal>Orientation</li>
 <li class=MsoNormal>Bubble</li>
</ul>

<p class=MsoNormal style='margin-left:.5in'>&nbsp;</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Not all charting attribute functional areas are
  available for all chart types. In addition, attributes in each functional
  area may be different based on the chart type selected and the data type of
  the selected columns.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<p class=MsoNormal>Three attributes are displayed as stand-alone properties of
the chart; <i>Title, Show Data Values</i>, and <i>Allow Resizing</i>.</p>


<p class=MsoNormal>The <i>Title</i> attribute value will be displayed as a
chart title over the displayed chart.</p>


<p class=MsoNormal>The <i>Show Data Values</i> attribute will include the
numeric data values driving the chart elements in the chart display. By default
this attribute is enabled.</p>


<p class=MsoNormal>The <i>Allow Resizing</i> attribute will display resizing handles
around the chart so that the end-user can adjust the chart size.</p>



<p class=MsoNormal><i>Functional Attribute Sections</i></p>


<p class=MsoNormal><i>Label Column </i>  This section allows the user to select
the column to be used for a Pie chart label. Data values will be aggregated
based on this column.</p>


<p class=MsoNormal><img border=0   id="Picture 181"
src="Report_Design_Guide_files/image146.jpg"></p>



<p class=MsoNormal><i>Label Column (x-axis) </i>  For X/Y charts, this section allows
the user to select the column to be displayed along the X-axis, specify the
caption for the X-axis and select the format:</p>


<p class=MsoNormal><img border=0   id="Picture 182"
src="Report_Design_Guide_files/image147.jpg"></p>


<p class=MsoNormal>If the Label Column for the X-axis is a Date column, the <i>Apply
Time Period</i> drop-down list will include time periods. Data will be
aggregated according to the selected time period.</p>


<p class=MsoNormal>The options in the list include:<br>
<br>
</p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>None   Displays the date information on the x-axis and
     aggregate the values by date.</li>
 <li class=MsoNormal>Year   Displays the year information on the x-axis and
     aggregate the values by year.</li>
 <li class=MsoNormal>Quarter   Displays the first day of the quarter on the
     x-axis and aggregate the values by quarter.</li>
 <li class=MsoNormal>Fiscal Quarter   Displays the first day of the fiscal
     quarter on the x-axis and aggregate the values by fiscal quarter.</li>
 <li class=MsoNormal>Month   Displays the first day of the month on the x-axis
     and aggregate the values by month.</li>
</ul>



<p class=MsoNormal><i>Label Scaling </i>  This section allows you to override
the default scaling of the X-axis by entering values for the <i>Lower</i> and<i>
Upper</i> <i>Bound</i> attributes. </p>


<p class=MsoNormal>If the column used for the x-axis content is numeric, the <i>Linear
Numeric</i> attribute is available and if enabled will provide fixed numeric
intervals along the X-axis.</p>


<p class=MsoNormal><img border=0   id="Picture 183"
src="Report_Design_Guide_files/image148.jpg"></p>


<p class=MsoNormal>If the column used for the X-axis content is date oriented,
the <i>Linear Time</i> attribute is available and if enabled will provide fixed
time intervals along the X-axis.</p>


<p class=MsoNormal><img border=0   id="Picture 184"
src="Report_Design_Guide_files/image149.jpg"></p>



<p class=MsoNormal><i>Data Column </i>  This section allows you to select the
data to be shown in the chart, select the format from the suite of numeric
formats, and select the aggregation function to be applied to the data:</p>


<p class=MsoNormal><img border=0   id="Picture 185"
src="Report_Design_Guide_files/image150.jpg"></p>



<p class=MsoNormal><i>Data Column (y-axis) </i>  For X/Y charts, this section allows
you to select the column used for the data displayed along the Y-axis. In
addition, you can specify a caption for the Y-axis, select a format for the
data values, and select an aggregation function.</p>


<p class=MsoNormal><img border=0   id="Picture 186"
src="Report_Design_Guide_files/image151.jpg"></p>


<p class=MsoNormal style='text-align:justify'><i>&nbsp;</i></p>

<p class=MsoNormal><i>Data Scaling </i>  This section allows the user to
override the default scaling of the Y-axis by entering values for the <i>Lower</i>
and<i> Upper</i> <i>Bound</i> attributes: </p>


<p class=MsoNormal><i><img border=0   id="Picture 187"
src="Report_Design_Guide_files/image152.jpg"></i></p>



<p class=MsoNormal><i>Legend </i>  This section allows you to indicate whether
a legend should be displayed for the chart. For some chart types you can also
select the legend position relative to the chart and specify a legend label:</p>


<p class=MsoNormal><img border=0   id="Picture 188"
src="Report_Design_Guide_files/image153.jpg"></p>



<p class=MsoNormal><i>Relevance </i>  This section provides a Relevance filter that
allows you to specify data thresholds to be included in the chart:</p>


<p class=MsoNormal><img border=0   id="Picture 189"
src="Report_Design_Guide_files/image154.jpg"></p>


<p class=MsoNormal>You can enable/disable a relevance filter by check the <i>Use
Relevance Values</i> checkbox. Specify the relevance scale by selecting either
the <i>Top N rows</i> or <i>Percentage</i> options and enter the threshold
value in the area provided.</p>


<p class=MsoNormal>For example, only the top 75% of data in the chart could be
relevant. In this case, check the <i>Use Relevance Values</i> checkbox, enter a
value of 75 in the value field, and click the <i>Percentage</i> radio button. Alternatively,
only the top 5 rows of data in the chart could be relevant. In this case, check
<i>Use Relevance Values</i>, enter a value of 5 in the value.</p>


<p class=MsoNormal><i>Trend Line </i>  This section allows you to include a
trend line on the chart:</p>


<p class=MsoNormal><img border=0   id="Picture 190"
src="Report_Design_Guide_files/image155.jpg"> </p>

<p class=MsoNormal><i>Crosstab Column </i>  This section allows you to identify
a second column of data to be redisplayed in the chart:</p>


<p class=MsoNormal><img border=0   id="Picture 191"
src="Report_Design_Guide_files/image156.jpg"></p>



<p class=MsoNormal><i>Style </i>  This section allows you to change the size
and spacing of the chart:</p>


<p class=MsoNormal><img border=0   id="Picture 192"
src="Report_Design_Guide_files/image157.jpg"></p>


<p class=MsoNormal>The <i>Size</i> attribute drop-down list offers three standard
sizes: Small, Medium, and Large. Selecting one of the standard sizes will reset
the other size attributes. If the size attributes are overridden, the Size
attribute will be set to Custom.</p>



<p class=MsoNormal><i>Orientation </i>  This section allows you to select the
orientation of the bars in the Bar charts.</p>


<p class=MsoNormal><img border=0   id="Picture 193"
src="Report_Design_Guide_files/image158.jpg"></p>


<p class=MsoNormal>The Bar Direction drop-down list offers <i>Horizontal</i>
and <i>Vertical</i> choices.</p>



<p class=MsoNormal><i>Bubble </i>  This section allows you to specify a column
of data that will be used to set the size of the data points/bubbles in a
scatter chart:</p>


<p class=MsoNormal><img border=0   id="Picture 194"
src="Report_Design_Guide_files/image159.jpg"></p>

<p class=MsoNormal> </p>




</div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection2></div>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:auto'>
</span>

<div class=WordSection3>

</body>
</html>
