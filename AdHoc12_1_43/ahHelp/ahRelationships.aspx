<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221777">Relationships</a></h2>


<p class=MsoNormal><span style=''>The Relationships<b>
</b>page allows the System Administrator to create join relationships for data
objects in the Ad Hoc instance. Join relationships must be defined within the instance
because relationships that exist in the source database are not synchronized by
the Schema Wizard. Join relationships created in the Relationships<b> </b>page
are stored in the Ad Hoc metadata database; the reporting database is not
modified<i>.</i></span></p>


<p class=MsoNormal><span style=''>Relationships
impact the end-user when they are selecting data objects in the Report Builder
to be included in the report. When they select the first data object, the list
of data objects is adjusted to show the selected data object and any related
data objects. Every time a data object is selected, the list of related data
objects is refreshed.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #FFFFCC 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#FFFFCC'>
  <p class=HintCxSpFirst>Hint:</p>
  <p class=HintCxSpLast>When using Microsoft SQL Server, Oracle, Informix, or
  Sybase as a reporting database, administrators can utilize the <i>Import
  Relationships</i> action in the Management Console to synchronize the
  instance with join relationships from the reporting database. Refer to the <i>Management
  Console Usage Guide</i> for more details.</p>
  </div>
  </td>
 </tr>
</table>

</div>


<p class=MsoNormal><span style=''>Select <b>Relationships</b>
from the <i>Database Configuration</i> drop-down list<b> </b>to display the <i>Relationships</i>
configuration page:</span></p>


<p class=MsoNormal><span style=''><img
border=0   id="Picture 105"
src="System_Admin_Guide_files/image076.jpg"></span></p>



<p class=MsoNormal><span style=''>A list of
relationships is displayed, allowing the System Administrator to view information
associated with each relationship, such as the type of relation and which objects
are joined together.</span></p>


<p class=MsoNormal><span style=''>In the grid, <i>Main
Data Object</i> refers to the <i>left</i> data object and <i>Joined Data Object</i>
refers to the <i>right</i> data object.</span></p>


<p class=MsoNormal><span style=''>The <b>Database</b>
drop-down list acts as a filter for the Relationship list. Only Relationships
related to the selected database will be displayed. If only one reporting database
has been configured for the Ad Hoc instance, the <b>Database</b> filter will
not be shown.</span></p>


<p class=MsoNormal><span style=''>Click <b>Add</b>
to display an empty <i>Relationship</i> page.</span></p>


<p class=MsoNormal><span style=''>Click <b>Delete</b>
to remove the selected relationships. Relationships are selected by checking
their checkboxes.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Removing relationships marks any reports that depend
  on that relationship as <i>broken</i>. Delete relationships with caution. Use
  the <i>View Dependencies</i> option to determine the scope and usage of a
  relationship.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<p class=MsoNormal><span style=''>The rows in
the relationship grid can be sorted by clicking on the <i>Relation Label, Main
Data Object, Relation Type,</i> or <i>Joined Data Object</i> column headers.</span></p>


<p class=MsoNormal><span style=''>The  </span><span
style=''><img border=0  
id="Picture 106" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon indicates that more than one
action can be performed on the data object. Hover your mouse cursor over the  </span><span
style=''><img border=0  
id="Picture 107" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon to display the available actions
and click the appropriate one. The available actions for a relationship are <b>Modify
Relationship</b>,<b> Delete Relationship</b>, and <b>View Dependencies.</b></span></p>


<p class=MsoNormal><span style=''>The following
join types are available in the application:</span></p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal><b><span style=''>Inner
     Join</span></b><span style=''> - Retrieve
     records when a match exists in <i>both</i> tables</span></li>
 <li class=MsoNormal><b><span style=''>Left
     Outer Join</span></b><span style=''> -
     Always retrieve records in the <i>left </i>table</span></li>
 <li class=MsoNormal><b><span style=''>Right
     Outer Join</span></b><span style=''> -
     Always retrieve records in the <i>right </i>table</span></li>
</ul>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #FFFFCC 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#FFFFCC'>
  <p class=HintCxSpFirst><b>Hint:</b></p>
  <p class=HintCxSpLast>Normally, only one relationship needs to be defined
  between two data objects. Although three relationship types are allowed,
  avoid creating more than one. In the event that more than one relationship
  type is created between the same two data object, attempt to make the relationship's
  label descriptive.</p>
  </div>
  </td>
 </tr>
</table>

</div>



<h3><a name="_Toc457221778"></a><a name="_Toc457221142">Adding a new
Relationship</a></h3>


<p class=MsoNormal><span style=''>To create a
new relationship between two data objects, click <b>Add</b>. A blank <i>Relationship</i>
page will be displayed:</span></p>


<p class=MsoNormal><span style=''><img
border=0   id="Picture 108"
src="System_Admin_Guide_files/image077.jpg"></span></p>



<p class=MsoNormal><span style=''>The <i>Database
Connection</i> identifies the currently selected reporting database. </span></p>


<p class=MsoNormal><span style=''>We recommend
that the <i>Relation Label</i> you enter clearly identify the data objects and
relationship type; however, that s not a requirement.</span></p>


<p class=MsoNormal><span style=''>The <i>Main
Data Object</i> and the <i>Joined Data Object</i> specify the two data objects that
are related. Select a data object from each of the respective lists.</span></p>


<p class=MsoNormal><span style=''>Select the <i>Join
Type</i> from the provided list.</span></p>


<p class=MsoNormal><span style=''>The <i>Auto
Reverse</i> checkbox indicates whether the relationship is  literal  or  logical .
Ad Hoc has the ability to logically relate the two data objects. If a
relationship is defined, the logical reverse of the relationship can be assumed
by checking this checkbox.</span></p>


<p class=MsoNormal><span style=''>If the <i>Auto
Reverse</i> checkbox is unchecked, the relationship is  literal . When the user
selects the <i>Main Data Object</i> for their report, the related <i>Joined
Data Object</i> will be shown. However, if the <i>Joined Data Object</i> is
selected for the report, the related <i>Main Data Object</i> is <i>not</i>
shown. The relationship only exists in one direction.</span></p>


<p class=MsoNormal><span style=''>The <i>Joined
Data Object Modified Label</i> and the <i>Main Data Object Modified Label</i>
are displayed in the Report Builder to help identify the data objects to the end-user.
Enter a  nickname  for the data objects to be displayed when the data object
available for selection in the Report Builder.</span></p>


<p class=MsoNormal><span style=''>Relationships
can also exist between columns in the data objects. Normally relationships are
based on a single pair of columns in the two data objects; however, compound
relationships based on multiple column pairs can be created.</span></p>


<p class=MsoNormal><span style=''>Select the
columns to be related from the drop-down lists for the <i>Main</i> and <i>Joined
Data</i> objects. For compound relationships, click <b>Add</b> and select the
second pair of related columns. To remove column pairs, select the pair of
columns with their checkboxes and click the <b>Delete</b> button that becomes
visible with more than one pair of related columns.</span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the relationship in the metadata database.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpLast>If the Main Data Object drop-down list is inactive,
  relationships can only be created from that specific object to another
  object.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<h5 align=left style='text-align:left'><i><span style='font-weight:normal'><span
 style='text-decoration:none'>&nbsp;</span></span></i></h5>


<h3><a name="_Toc457221779"></a><a name="_Toc457221143">Modifying Relationships</a></h3>


<p class=MsoNormal><span style=''>To modify a
relationship, hover your mouse cursor over the  </span><span style='
"Arial","sans-serif"'><img border=0   id="Picture 109"
src="System_Admin_Guide_files/image026.jpg"></span><span style='
"Arial","sans-serif"'> icon to display the available actions and click the <b>Modify
Relationship</b> action.</span></p>


<p class=MsoNormal><span style=''>The <i>Relationship</i>
page will be displayed.</span></p>


<p class=MsoNormal><span style=''>Modifications
to either the data objects or related columns will break the reports that rely
on them. We highly recommend that you run the <b>View Dependencies</b> report first
to identify the scope and impact of any relationship changes you re
considering.</span></p>


<p class=MsoNormal><span style=''>Modifications
to the <i>Auto Reverse</i> flag may break reports.</span></p>


<p class=MsoNormal><span style=''>Modifications
to any of the labels will not impact the reports.</span></p>


<p class=MsoNormal><span style=''>Make the
necessary adjustments to the relationship and click <b>Save</b> to save the modified
relationship in the metadata database.</span></p>



<h3><a name="_Toc457221780"></a><a name="_Toc457221144">View Dependencies</a></h3>


<p class=MsoNormal><span style=''>To view the
dependencies for a relationship from the <i>Relationships</i> page, hover your
mouse cursor over the  </span><span style=''><img
border=0   id="Picture 110"
src="System_Admin_Guide_files/image026.jpg"></span><span style='
"Arial","sans-serif"'> icon to display the available actions and click the <b>View
Dependencies</b> option. </span></p>


<p class=MsoNormal><span style=''>The option to
view dependencies of a relationship may help System Administrators know the
scope and impact of changes to a relationship.<br clear=all style='page-break-before:
always'>
</span></p>

</body>
</html>
