<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945098"></a><a name="_Toc382291593">Updating Your Profile</a></h2>


<p class=MsoNormal>Click <b>Profile </b>to change your profile information. Profile
information is divided into User Profile and Preferences and accessed by
clicking on the respective tabs. </p>


<p class=MsoNormal><i>User Profile</i></p>


<p class=MsoNormal>The <i>User Profile</i> covers the user specific
information. From this screen, a user's <i>Username, Password, First Name, Last
Name</i> and <i>Email Address</i> may be changed.</p>


<p class=MsoNormal><img border=0  
src="Report_Design_Guide_files/image020.jpg"></p>


<p class=MsoNormal><b>To update your profile:</b></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>Type the new information into the <i>User Profile</i>
     fields provided.</li>
 <li class=MsoNormal>If the Security Authentication method is not NT, then</li>
 <ol style='margin-top:0in' start=1 type=a>
  <li class=MsoNormal>Click the <b>Set Password </b>button.</li>
  <li class=MsoNormal>Type your old password and new password twice in the
      fields provided.</li>
  <li class=MsoNormal>From the <i>Password</i> panel, click <b>OK </b>to apply
      the changes.</li>
 </ol>
 <li class=MsoNormal>Modify other fields as desired.</li>
 <li class=MsoNormal>Click <b>Save </b>to commit the changes.</li>
</ol>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#F3F3F3;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial><a name="OLE_LINK3"></a><a name="OLE_LINK2"></a><a
  name="OLE_LINK1"><span style='font-style:normal'>Notes:</span></a></p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>1.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span><i>Username</i>
  cannot be changed.</p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>2.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>The ability to
  modify a user password from the <i>User Profile</i> screen may be disabled by
  the administrator.</p>
  <p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'>3.<span
  style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp; </span>The content of
  the <i>Email Address</i> field cannot be deleted if the user has subscribed
  to a report.</p>
  </td>
 </tr>
</table>


<table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0
 style='background:#FFFF99;border-collapse:collapse'>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=Arial>Hint:</p>
  </td>
 </tr>
 <tr>
  <td width=590 valign=top style='width:6.15in;padding:0in 5.4pt 0in 5.4pt'>
  <p class=MsoNormal><i>Enter an email address in order to be able to subscribe
  to scheduled reports.</i></p>
  </td>
 </tr>
</table>




<h4><a name="_Toc382291594">Setting Your Preferences</a></h4>


<p class=MsoNormal>Click <b>Profile Management </b>and then select the <i>Preferences</i>
tab to change your user preference settings.<br>
<br>
</p>


<p class=MsoNormal><img   id="Picture 14"
src="Report_Design_Guide_files/image020.jpg"><br>
<br>
</p>


<p class=MsoNormal><span style='color:black'>To update your preferences:</span></p>


<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
style='color:black'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='color:black'>Select a Homepage Type, then:</span></p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='color:black'>a.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='color:black'>If <i>Application Page</i>, select a
web page from the application tree. If a report area is selected (e.g., Shared
Reports), then further refine your selection by:<br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:1.25in;text-indent:-1.25in'><span
style='color:black'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
style='color:black'>Click <b>Change Folder</b>.</span></p>

<p class=MsoNormal style='margin-left:1.25in;text-indent:-1.25in'><span
style='color:black'><span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span
style='color:black'>Select a folder from the folder tree and then click <b>OK</b>.<br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='color:black'>b.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='color:black'>If <i>Report</i>, select a report by clicking
<b>Find Report</b> to view a list of reports<i> </i>and then click <b>OK</b><i>.</i><br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='color:black'>c.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span>If <i>URL</i>, then specify a URL address. <span
style='color:black'>Click <b>Test URL </b>to confirm that the URL can be
viewed.<br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:.75in;text-indent:-.25in'><span
style='color:black'>d.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span>If <i>Pre-<span style='color:black'>defined Report</span></i><span
style='color:black'>, select a report using <b>Find Report</b> to view a list
of reports<i> </i>and then click <b>OK</b><i>.<br>
<br>
</i></span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
style='color:black'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='color:black'>If desired, check <i>Retain search
strings</i> to retain your last search criteria for any page even after a
logout.<br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
style='color:black'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='color:black'>If desired, check <i>Retain sort
preferences</i> to retain your last choices for sorting of grids in the
application.<br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
style='color:black'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='color:black'>Click <b>Save </b>to commit the changes
and return to the Profile page.<br>
<br>
</span></p>

<p class=MsoNormal style='margin-left:.25in;text-indent:-.25in'><span
style='color:black'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><i><span style='color:black'>OPTIONAL</span></i><span
style='color:black'>: Click <b>Restore Original Settings</b> to revert the
preference settings back to the default settings.</span></p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>If a Homepage is specified, the application will
  automatically display the designated Homepage immediately after logging into
  the application and when the Home link is selected.</p>
  </div>
  </td>
 </tr>
</table>

</div>


<span style='font-size:12.0pt;;color:black'><br
clear=all style='page-break-before:always'>
</span>


</body>
</html>
