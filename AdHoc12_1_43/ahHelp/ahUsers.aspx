<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221754">Users</a></h2>


<p class=MsoNormal><span style=''>A username
and password are required to access the application. Administrators assign each
individual a username and password. Users are members of a particular organization,
typically  Default , and have one or more assigned roles for access to specific
data objects and application features.</span></p>


<p class=MsoNormal><span style=''>Select <b>Users</b>
from the <i>User Configuration</i> drop-down list<b> </b>to display the <i>Users</i>
configuration page:</span></p>


<p class=MsoNormal><img border=0   id="Picture 37"
src="System_Admin_Guide_files/image035.jpg"></p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #FFFFCC 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#FFFFCC'>
  <p class=HintCxSpFirst><b>Hint:</b></p>
  <p class=HintCxSpLast>Change the default password for the Admin user after
  installing the application. The default password is &quot;password&quot;.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<p class=MsoNormal><span style=''>Click <b>Add</b>
to display an empty <i>User</i> page.</span></p>


<p class=MsoNormal><span style=''>Click <b>Delete</b>
to remove the selected users. Users are selected by checking their checkboxes.</span></p>


<p class=MsoNormal><span style=''>Deleting
users from the application permanently erases any reports that user may have
created, <i>except</i> for their shared reports. Any users created by the
administrator can be edited and removed. </span></p>


<p class=MsoNormal><span style=''>To deny a
user access, we recommended that you lock their account or change their
password instead of deleting their account.</span></p>




<p class=MsoNormal><span style=''>The actions
available for a User are <b>Modify User, Delete User</b>, and <b>Set Session
Parameters. </b>Hover your mouse cursor over the </span><span style='
"Arial","sans-serif"'><img border=0   id="Picture 38"
src="System_Admin_Guide_files/image026.jpg"></span><span style='
"Arial","sans-serif"'> icon to show the available actions.</span></p>



<p class=MsoNormal><b><span style=''>Adding a
User</span></b></p>


<p class=MsoNormal><span style=''>To add a
user, click <b>Add</b> and the <i>User</i> page will be displayed:<br>
<br>
</span></p>


<p class=MsoNormal><img border=0   id="Picture 142"
src="System_Admin_Guide_files/image036.jpg"></p>

<p class=MsoNormal><span style=''>A <i>Username</i>
value is required and must be unique. </span></p>


<p class=MsoNormal><span style=''>A <i>Password</i>
must be set for each user. Click the <b>Set Password</b> button and complete
the following dialog box:<br>
<br>
</span></p>


<p class=MsoNormal><img border=0   id="Picture 352"
src="System_Admin_Guide_files/image037.jpg"></p>


<p class=MsoNormal><span style=''>After
entering the password twice, click <b>OK</b> to save it.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>The length, strength, and change frequency are
  configurable by the System Administrator through the Management Console and
  through Configuration/Application Settings in the instance.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>

<p class=MsoNormal><span style=''><br>
The <i>First Name </i>and<i> Last Name </i>values are optional and are only
used for display purposes. <br>
<br>
The <i>Email Address</i> value is optional as well, however, an email address
will be necessary for the delivery of scheduled reports. </span></p>


<p class=MsoNormal><span style=''>Check the <i>Locked</i>
checkbox to prevent this user from accessing the Ad Hoc instance.</span></p>


<p class=MsoNormal><span style=''>Although <i>User
Roles </i>do not have to be set when the user is created, at least one is
required for the user to be able to access the Ad Hoc instance. Multiple roles can
be assigned and will convey the cumulative rights of all of their assigned
roles.</span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the new User information. </span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><i><u><span style=''><span
 style='text-decoration:none'>&nbsp;</span></span></u></i></p>

<p class=MsoNormal><b><span style=''>Adding a
User Using  Save As </span></b></p>


<p class=MsoNormal><span style=''>The System Administrator
can also create a user by modifying an existing user and then saving that
record as a new user by clicking the <b>Save As</b> button. </span></p>


<h5 align=left style='text-align:left'><i><span style='font-weight:normal'><span
 style='text-decoration:none'>&nbsp;</span></span></i></h5>

<h5 align=left style='text-align:left'><span style='text-decoration:none'>Modifying
Users</span></h5>


<p class=MsoNormal><span style=''>System
administrators can change a user s profile at any time. To change user information,
click the <b>Modify User</b> action for the user. The <i>Modify</i> <i>Users</i>
page will be displayed:</span></p>



<p class=MsoNormal><img border=0   id="Picture 353"
src="System_Admin_Guide_files/image038.jpg"></p>


<p class=MsoNormal><span style=''>Modify the appropriate
information and click <b>Save</b> to save the information.</span></p>


<p class=MsoNormal><span style=''>To save the
edited information using a new <i>Username</i>, click <b>Save As</b>, enter the
new name, and click <b>OK</b> to save the information.</span></p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>The <b>Selected User</b> drop-down list can be used to
  navigate through users in order to avoid using the <b>Back to Users</b>
  button in order to select a different user.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>


<h5 align=left style='text-align:left'><span style='text-decoration:none'>&nbsp;</span></h5>

<h5 align=left style='text-align:left'><span style='text-decoration:none'>&nbsp;</span></h5>

<h5 align=left style='text-align:left'><span style='text-decoration:none'>Set
Session Parameters</span></h5>


<p class=MsoNormal><span style=''>To set
session parameter values for an individual user, select the <b>Set Session Parameters</b>
action. The following page will be displayed:</span></p>



<p class=MsoNormal><span style=''><img
border=0   id="Picture 44"
src="System_Admin_Guide_files/image039.jpg"></span><span style='
"Arial","sans-serif"'> </span></p>



<p class=MsoNormal><span style=''>The <i>Selected
User</i> drop-down list defaults to the user that linked to this page. It s a
convenient way to change the focus of the page to a different user without
having to go back to the previous page.</span></p>


<p class=MsoNormal><span style=''>The grid
presents all of the session parameters values defined for the selected user. Click
the <b>Parameter Name</b> or <b>Follow Default</b> column headers to sort the
contents of the grid.</span></p>


<p class=MsoNormal><span style=''>The <b>Restore
Defaults</b> button sets the values of the selected session parameters to the
Default Value displayed in the grid.</span></p>


<p class=MsoNormal><span style=''>Check the <i>Follow
Default</i> checkbox to specify that parameter value should adopt the <i>Default
Value</i>. This also allows the parameter value to be set  permanently , meaning
that changes to the <i>Default Value</i> will have no impact on the parameter
value for the user, if the <i>Follow Default</i> checkbox is unchecked.</span></p>


<p class=MsoNormal><span style=''>The grayed <i>Parameter
Value</i> text boxes are not disabled. The gray color is a visual cue that the
value is the same as the default value and is expected to follow the default
value. That means that, if the default value changes, the user will
automatically pick up the new value.</span></p>


<p class=MsoNormal><span style=''>The <i>Parameter
Value</i> can be changed by either entering a value directly into the text box
or selecting the<b> Modify</b> action and providing a new value. The parameter
value may also be changed by selecting the <b>Restore Default</b> action.</span></p>


<p class=MsoNormal><span style=''>If any <i>Parameter
Value</i> is set it will be considered an override value and the <i>Follow
Default</i> checkbox will automatically be unchecked. </span></p>


<p class=MsoNormal><span style=''>Click <b>Save</b>
to save the session parameter values for the user.</span></p>

<span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span>



</body>
</html>
