<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221784"></a><a name="_Toc457221148">Presentation Styles</a></h2>


<p class=MsoNormal><span style=''>The
Presentation Styles page allows System Administrators to manage Cascading Style
Sheet (CSS) classes available for use when building reports. Presentation
styles registered on this page are used for highlighting specific report labels,
captions, and data points based on a given criteria.</span></p>


<p class=MsoNormal><span style=''>Before
registering a CSS class with the Ad Hoc instance, administrators must create
the class and add it to every style sheet in the </span><span style='font-size:
11.0pt;'>_StyleSheets</span><i><span style='
"Arial","sans-serif"'> </span></i><span style=''>folder.
Adding the class to every style sheet ensures that all classes are available
regardless of the report template selected from the Report Builder.</span></p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=Notes><strong><span style=''>Note: </span></strong><strong><span
  style=';font-weight:normal'><br>
  While you re creating and managing Presentation Styles, the default
  stylesheet will be used to present the class options. The default stylesheet
  is set in the <i>Configuration</i> </span></strong><strong><span
  style='Wingdings;font-weight:normal'> </span></strong><strong><span
  style=';font-weight:normal'> <i>Report Configuration</i>
  </span></strong><strong><span style='Wingdings;font-weight:normal'> </span></strong><strong><span
  style=';font-weight:normal'> <i>Report
  Settings</i> page s Default Template attribute.</span></strong></p>
  </div>
  </td>
 </tr>
</table>

</div>



<p class=MsoNormal><span style=''>Select <b>Presentation
Styles</b> from the <i>Report Configuration</i> drop-down list<b> </b>to
display the <i>Presentation Styles</i> configuration page:</span></p>



<p class=MsoNormal><b><span style='font-size:10.0pt;'><img
border=0   id="Picture 119"
src="System_Admin_Guide_files/image084.jpg"></span></b></p>



<p class=MsoNormal><span style=''>Click <b>Add</b>
to display an empty <i>Presentation Style dialog box.</i></span></p>


<p class=MsoNormal><span style=''>Click <b>Delete</b>
to remove the selected presentation styles. Presentation Styles are selected by
checking their checkboxes.</span></p>


<p class=MsoNormal><span style=''>The  </span><span
style=''><img border=0  
id="Picture 120" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon indicates that more than one
action can be performed on the presentation style. Hover your mouse cursor over
the  </span><span style=''><img border=0
  id="Picture 121" src="System_Admin_Guide_files/image026.jpg"></span><span
style=''> icon to display the available actions:
<b>Modify Presentation Style</b> and <b>View Dependencies</b>.</span></p>



<h3><a name="_Toc457221785"></a><a name="_Toc457221149">Adding a Presentation
Style</a></h3>


<p class=MsoNormal><span style=''>To make a
presentation style available to the end-user, click <b>Add</b>. The following dialog
box will be displayed:<br>
<br>
</span></p>


<p class=MsoNormal><img border=0   id="Picture 358"
src="System_Admin_Guide_files/image085.jpg"></p>


<p class=MsoNormal><span style=''>Select the
class by clicking the </span><span style=''><img
border=0   id="Picture 123"
src="System_Admin_Guide_files/image086.gif" alt=view-icon></span><span
style=''>icon. Enter a unique <i>Friendly Name</i>
value and click <b>OK</b> to store the presentation style reference in the
metadata database. </span></p>

<p class=MsoNormal><span style=''><br>
<br>
</span></p>

<h3><a name="_Toc457221786"></a><a name="_Toc457221150">Modifying a
Presentation Style</a></h3>


<p class=MsoNormal><span style=''>Modifying a
style is necessary when the name of the class changes within the style sheet,
or when administrators want to change what users see from the Report Builder.
Modifying a style only changes its registration status in the Ad Hoc instance -
the name of the class within the style sheet is not altered.</span></p>


<p class=MsoNormal><span style=''>Hover your
mouse cursor over the  </span><span style=''><img
border=0   id="Picture 124"
src="System_Admin_Guide_files/image026.jpg"></span><span style='
"Arial","sans-serif"'> icon for a presentation style and select the <b>Modify
Presentation Style</b> option from the list. Select the<i> Class</i> by
clicking the </span><span style=''><img
border=0   id="Picture 125"
src="System_Admin_Guide_files/image086.gif" alt=view-icon></span><span
style=''>icon. Enter a new, unique <i>Friendly
Name</i> and click <b>OK</b> to save the revised presentation style reference
in the metadata database.</span></p>


<h3><a name="_Toc457221787"></a><a name="_Toc457221151">Viewing Dependencies</a></h3>


<p class=MsoNormal><span style=''>Hover your
mouse cursor over the  </span><span style=''><img
border=0   id="Picture 126"
src="System_Admin_Guide_files/image026.jpg"></span><span style='
"Arial","sans-serif"'> icon for a presentation style and select the <b>View Dependencies</b>
option from the list.</span></p>


<p class=MsoNormal><span style=''>A report page
identifying the scope and usage of the presentation style will be displayed. Administrators
should view this page before modifying or deleting a presentation style, in
order to determine the impact of the change.</span></p>




</body>
</html>
