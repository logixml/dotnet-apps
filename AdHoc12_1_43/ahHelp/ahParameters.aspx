<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945104"></a><a name="_Toc382291600">Setting Filters</a></h2>


<p class=MsoNormal>Data used in the report can be filtered by various criteria.
The criteria can be set for the report by the developer ( non-Ask  parameters)
or may be supplied by the user when the report is executed ( Ask  parameters).</p>


<p class=MsoNormal>To set a filter for the report click <b>Select Data</b> to
display the <i>Select Data</i> dialog box. Click the <i>Filter</i> tab to
display:</p>




<p class=MsoNormal><img   id="Picture 313"
src="Report_Design_Guide_files/image051.jpg"></p>



<p class=MsoNormal>To create a filter parameter, click <b>Add a Parameter</b> and
display the Parameter Details dialog box:</p>


<p class=MsoNormal><img   id="Picture 314"
src="Report_Design_Guide_files/image052.jpg"></p>


<p class=MsoNormal>A parameter takes the form of an equation, similar to:</p>


<p class=MsoFooter align=center style='text-align:center'><i>Label </i>is <i>Compared
</i>to <i>Value</i></p>


<p class=MsoFooter>where <i>Label</i> represents a column name, C<i>ompared </i>represents
a comparison operator, and <i>Value</i> represents a threshold.</p>


<p class=MsoFooter>The available comparison operators are:</p>



<table class=MsoTableGrid border=0 cellspacing=0 cellpadding=0
 style='border-collapse:collapse;border:none'>
 <tr>
  <td width=420 valign=top style='width:251.75pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>Equal to</li>
   <li class=MsoNormal>Not equal to</li>
   <li class=MsoNormal>Less than</li>
   <li class=MsoNormal>Greater than</li>
   <li class=MsoNormal>Less than or equal to</li>
   <li class=MsoNormal>Greater than or equal to</li>
   <li class=MsoNormal>Starts with <sup>1</sup></li>
   <li class=MsoNormal>Does not start with <sup>1</sup></li>
  </ul>
  </td>
  <td width=420 valign=top style='width:251.75pt;padding:0in 5.4pt 0in 5.4pt'>
  <ul style='margin-top:0in' type=disc>
   <li class=MsoNormal>Ends with <sup>1</sup></li>
   <li class=MsoNormal>Does not end with <sup>1</sup></li>
   <li class=MsoNormal>Contains <sup>1</sup></li>
   <li class=MsoNormal>Does not contain <sup>1</sup></li>
   <li class=MsoNormal>Is null</li>
   <li class=MsoNormal>Is not null</li>
   <li class=MsoNormal>Between</li>
   <li class=MsoNormal>Not between</li>
   <li class=MsoNormal>In list</li>
   <li class=MsoNormal>Not in list</li>
   <li class=MsoNormal>In cascading list</li>
  </ul>
  </td>
 </tr>
</table>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>Operator availability is dependent upon the column's
  data type:</p>
  <p class=NotesCxSpLast>These operators are only available for data of type
  String or Text.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>

<p class=MsoNormal style='margin-right:-40.5pt'>&nbsp;</p>


<p class=MsoNormal>To set a data parameter to a <i>Specific Value</i> or <i>Pre-defined
Date</i>:</p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>From the <i>Column</i> drop-down menu, select the desired column.
     <br>
     <br>
 </li>
 <li class=MsoNormal>From the <i>Operator</i> drop-down menu, select the
     desired comparison operator.<br>
     <br>
 </li>
 <li class=MsoNormal>From the <i>Value</i> type drop-down menu, select either <i>Specific
     Value </i>or <i>Pre-defined Date</i>.<br>
     <br>
 </li>
 <li class=MsoNormal>Specify a threshold value(s) in the <i>Value</i> field.
     Click the <img   id="Picture 48"
     src="Report_Design_Guide_files/image053.gif" alt=iconFind> icon to view a
     list of valid values from the database and then populate the Value field
     from the value(s) selected.<br>
     <br>
 </li>
 <li class=MsoNormal>Click <b>OK </b>to add the parameter to the report.<br>
     <br>
 </li>
 <li class=MsoNormal>Click the <img   id="Picture 49"
     src="Report_Design_Guide_files/image049.jpg"> icon adjacent to any
     parameter to delete that parameter from the report. Click the <img
       id="Picture 50"
     src="Report_Design_Guide_files/image054.gif" alt=mod-icon> icon to modify
     a parameter.<br>
     <br>
 </li>
 <li class=MsoNormal>Add more parameters by clicking the <b>Add a Parameter</b>
     button and repeating the steps above.<br>
     <br>
 </li>
</ol>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>If the <i>In list</i> or <i>Not in list</i> operator
  is selected, then more than one value may be specified. If manually typing in
  each value, then follow each entry by the [ENTER] key.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>If the main data column is of Date type, an additional
  choice of values called Pre-defined Date. Pre-defined dates are dates in the
  form of a token, such as Today, This Year End, Last Fiscal Quarter Start,
  etc. Pre-defined dates get evaluated at the time the report runs. If the
  pre-defined date token is evaluated to a column containing time, then no
  records will be returned at runtime.<br>
  <br>
  </p>
  </div>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal><br>
<br>
</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst>If the value is a number, the Value field must
  contain a valid number to build the report. If the <i>In cascading list</i>
  or <i>Not in cascading list</i> operator is selected, then a user must choose
  a cascading filter and as a result do not have the option to specify a value.
  See the  Advanced Concepts in Data Filtering  section for a detailed
  explanation of the options available for cascading filters. See the  Running
  Reports  chapter for more information about cascading filter presentation.<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>The Filter functionality does not support conditions
  against data types of type Time. Date/Time data types are supported but their
  time portion will be ignored.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpLast>Specifying a Session Parameter as a value in report
  parameters is permissible. There are implications related to scheduling and
  archiving. In addition, modification of the session parameter after the
  report is built may  break  the report which may  break  schedules.
  Subscribed users may be automatically notified when a schedule is  broken .</p>
  </div>
  </td>
 </tr>
</table>

</div>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #FFFFCC 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#FFFFCC'>
  <p class=HintCxSpFirst><b>Hint:</b></p>
  <p class=HintCxSpLast>When adding multiple parameters, a logical operator
  (And or Or) becomes available for selection at the beginning of the next
  parameter. Use this operator to set the cumulative conditions for the
  parameters.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>

<p class=MsoNormal align=center style='text-align:center'>&nbsp;</p>

<p class=MsoNormal>If the report contains two or more parameters, the <img
  id="Picture 51" src="Report_Design_Guide_files/image055.gif"
alt=adf-icon> icon appears for each additional parameter. The directional pad
gives users the ability to create <i>levels </i>for each parameter in order to control
the order of evaluation:</p>

<p class=MsoNormal><img   id="Picture 315"
src="Report_Design_Guide_files/image056.jpg"></p>


<p class=MsoNormal>In the example above, the logic is:</p>


<p class=MsoNormal style='margin-left:.5in'><span style='font-size:10.0pt;
'>(CustomerID = ANTON OR BOTTM) And Region = CA</span></p>


<p class=MsoNormal><br>
<br>
</p>

<p class=MsoNormal><b>Creating Ask Parameters</b></p>


<p class=MsoNormal>Instead of limiting the report to predefined parameters, the
values can be modified while browsing the report. Check the <i>Ask in Report</i>
checkbox to offer the end-user the option of changing the threshold value
before the report is rendered.</p>


<p class=MsoNormal>Marking an operator as an &quot;ask&quot; parameter presents
a few other options in the <i>Parameter Details</i> dialog box, as shown below:</p>


<p class=MsoNormal><img   id="Picture 316"
src="Report_Design_Guide_files/image057.jpg"></p>


<p class=MsoNormal>The parameter caption will be automatically generated based
on the column name, if left blank.</p>


<p class=MsoNormal><i>Control Type</i> values depend on both the comparison operator
and the selected column's data type. For example, a Text type column with the <i>Equal
To</i> operator presents the following choices:<br>
<br>
</p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Text</li>
 <li class=MsoNormal>Drop-down</li>
 <li class=MsoNormal>List (single select)</li>
</ul>


<p class=MsoNormal>Selecting the <i>In list</i> operator produces the following
choices:<br>
<br>
</p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>List (multi-select)</li>
 <li class=MsoNormal>Text (multi line)</li>
</ul>


<p class=MsoNormal>A Date type column with the <i>Equal To</i> operator
produces the following choices:</p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Date</li>
 <li class=MsoNormal>Drop-down</li>
 <li class=MsoNormal>List (single select)</li>
</ul>


<p class=MsoNormal>A Boolean type column with the <i>Equal To</i> operator
produces the following choices:</p>


<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Checkbox</li>
 <li class=MsoNormal>Drop-down</li>
 <li class=MsoNormal>List (single select)</li>
</ul>


<p class=MsoNormal>If more than one parameter is displayed to the user, the
option to display each parameter adjacent the previous one or on a new line is
available. Choose by marking the desired radio button for <i>Display this
parameter</i>.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>With Control Type = List (multi-select) selected,
  the initial display of the report will appear with the Ask parameter's list
  populated with all possible values from the database and with the default
  values highlighted/pre-selected.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>With Control Type = Text (multi-line) selected, the
  initial display of the report will appear with the Ask parameter's list
  populated with only the default values. The values will not be
  highlighted/pre-selected.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>With Control Type = Drop-down selected, the <i>Offer
  All Option</i> checkbox is made available. Checking this checkbox will set
  the first row of the drop-down list to  All , allowing the report viewer to
  use that option as the report parameter.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpLast>Parameters based on comparing two columns cannot be
   asked  of the user.</p>
  </div>
  </td>
 </tr>
</table>

</div>




<h4><a name="_Toc382291601">Advanced Concepts in Data Filtering</a></h4>


<p class=MsoNormal>Advanced data filtering<i> </i>makes it possible to define
groups of parameters that work together to filter undesirable data out of the
report. Users can define multiple parameters and control the order of
evaluation in order to filter report data to control what end-users see at
runtime.</p>


<p class=MsoNormal>Data filtering gives users the ability to control the
content of the report. Filtering extraneous data from the report is done by
defining one or more parameters that are evaluated at runtime. The directional
pad control (<img   id="Picture 54"
src="Report_Design_Guide_files/image055.gif" alt=paramControlPad>) enables
users to control the order of evaluation.</p>


<p class=MsoNormal>The individual arrows of the control perform the following
functions:</p>


<p class=MsoNormal style='text-indent:.5in'><img  
id="Picture 55" src="Report_Design_Guide_files/image058.gif" alt=paramUp> Shifts
a parameter one position higher in the list (retains indentation)</p>

<p class=MsoNormal style='text-indent:.5in'><img  
id="Picture 56" src="Report_Design_Guide_files/image059.gif" alt=paramDown> Shifts
a parameter one position lower in the list (retains indentation)</p>

<p class=MsoNormal style='text-indent:.5in'><img  
id="Picture 57" src="Report_Design_Guide_files/image060.gif" alt=paramLeft> Indents
a parameter one position left</p>

<p class=MsoNormal style='text-indent:.5in'><img  
id="Picture 58" src="Report_Design_Guide_files/image061.gif" alt=paramRight> Indents
a parameter one position right</p>


<p class=MsoNormal>As parameters are indented to the right, enclosing
parentheses appear to indicate the order of evaluation.</p>


<p class=MsoFooter>Users can also perform a row-level comparison with fields in
two different columns. In this case, the parameter takes the form of an
equation similar to:<br>
<br>
</p>


<p class=MsoFooter align=center style='text-align:center'><i>Label1 </i>is <i>Compared
</i>to <i>Label2</i></p>


<p class=MsoFooter><br>
where <i>Label1</i> represents the first column name, <i>Compared </i>represents
a comparison operator, and <i>Label2</i> represents the second column name.</p>



<p class=MsoNormal>To compare values from two different columns:</p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal>From the <i>Column</i> drop-down list, select the desired column.
     </li>
 <li class=MsoNormal>From the <i>Operator</i> drop-down list, select the
     desired comparison operator.</li>
 <li class=MsoNormal>From the <i>Value</i> type drop-down list, select <i>Other
     Data Column</i>.</li>
 <li class=MsoNormal>From the <i>Value</i> field drop-down list, select the
     desired column the comparison will be performed against.</li>
 <li class=MsoNormal>Click <b>OK </b>to add the parameter to the report.</li>
 <li class=MsoNormal>Click the <img   id="Picture 59"
     src="Report_Design_Guide_files/image049.jpg"> icon adjacent to any
     parameter to delete it from the report. Click the <img  
     id="Picture 60" src="Report_Design_Guide_files/image054.gif" alt=mod-icon> icon
     to modify a parameter.</li>
</ol>

<p class=MsoNormal style='margin-left:.25in'>&nbsp;</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>When comparing two different columns, the <i>Ask in
  Report</i> checkbox is disabled.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<p class=MsoNormal style='margin-left:.25in'>&nbsp;</p>

<p class=MsoNormal style='margin-left:.25in'>&nbsp;</p>


<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal><b>Using Session Parameters</b></p>


<p class=MsoNormal>If session parameters have been defined within the Ad Hoc
instance, the layout of the Parameters Detail dialog box may be slightly
different:<br>
 </p>


<p class=MsoNormal><img   id="Picture 317"
src="Report_Design_Guide_files/image062.jpg"></p>


<p class=MsoNormal>In the example above, there is a <i>Value</i> source control.
The <i>Value</i> source is typically a  Specific Value ; however, if session
parameters have been defined,  Session Parameter  may be selected as the <i>Value</i>
source. When  Session Parameter  is selected, a drop-down list of relevant
session parameters will be displayed.</p>


<p class=MsoNormal>There are five types of session parameters: date, number,
numeric list, text, and  textual list. The drop-down list of session parameters
will contain the session parameters that match the data type of the <i>Column</i>.
The list is also restricted by the <i>Operator</i> selected.</p>


<p class=MsoNormal>For Date columns, the date session parameters will be shown
in the list of available session parameters.</p>


<p class=MsoNormal>For Numeric columns, either the number or numeric list
session parameters will be shown in the list of available session parameters.
If the <i>Operator</i> is set to  In List  or  Not In List , the numeric list
session parameters will be shown; otherwise the number session parameters will
be shown.</p>


<p class=MsoNormal>For Text columns, either the text or textual list session
parameters will be shown in the list of available session parameters. If the <i>Operator</i>
is set to  In List  or  Not In List , the textual list session parameters will
be shown; otherwise the text session parameters will be shown.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>The ability to select session parameters from a list
  is dependent on whether the session parameter was defined within the Ad Hoc
  interface (see the System Administration Guide for setting session parameters
  within Ad Hoc) or defined outside of the Ad Hoc user interface. An example of
  the latter would be session parameters passed into Ad Hoc from a parent
  application.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>If the session parameter is defined outside of Ad
  Hoc, Ad Hoc would have no knowledge of the data type of the parameter. You
  may still refer to the session parameter by using the @Session token. For
  example, @Session.Country~ would refer to the externally defined session
  parameter.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpLast>Usage of session parameters is discussed in the <a
  href="http://devnet.logianalytics.com/rdPage.aspx?rdReport=Article&amp;dnDocID=1366">Session
  Parameters Guide</a>.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpLast></p>
  </div>
  </td>
 </tr>
</table>

</div>




<p class=MsoNormal><b>Using Cascading Filters</b></p>


<p class=MsoNormal>Cascading filters are a set of related drop-down lists,
where the contents of one list is automatically filtered by the user s
selection in another list. </p>


<p class=MsoNormal>In our example, we ll be using a Country   Region - City set
of cascading filters. Once the user selects the initial Country, the Region drop-down
list will only present options from the selected Country. Similarly, once the
user has selected Country and Region, the City drop-down list will only present
cities found in the Region.</p>


<p class=MsoNormal>A cascading filter must be pre-defined by the System
Administrator before it may be incorporated into a report definition.</p>


<p class=MsoNormal>To incorporate a cascading filter into a report, navigate to
the Filter tab of the <b>Select Data</b> dialog box: </p>

<p class=MsoNormal><img border=0   id="Picture 318"
src="Report_Design_Guide_files/image063.jpg"></p>



<p class=MsoNormal>Click <b>Add Parameter</b> and the following dialog box will
be displayed:</p>




<p class=MsoNormal><img border=0   id="Picture 319"
src="Report_Design_Guide_files/image064.jpg"></p>


<p class=MsoNormal>The starting point for incorporating a cascading filter into
the report is to identify a column that matches the expected final result of a
pre-defined cascading filter. In our example, the ultimate target column is
City.</p>


<p class=MsoNormal>If we select the City column then the list of operators will
be extended to include <i>In cascading list</i> and <i>Not in cascading list</i>:</p>



<p class=MsoNormal><img border=0   id="Picture 79"
src="Report_Design_Guide_files/image065.jpg"></p>



<p class=MsoNormal>If the <i>In cascading list</i> operator is selected, the dialog
box will changed to facilitate specification of which cascading filter to use
and which other columns may be impacted when the filter is displayed in the
report:</p>



<p class=MsoNormal><img border=0   id="Picture 320"
src="Report_Design_Guide_files/image066.jpg"></p>


<p class=MsoNormal>The <i>Default Values</i> attribute shows a list of
pre-defined cascading filters that might be applied to the selected target
column, in this case  City Cascade .</p>


<p class=MsoNormal>The <i>Caption</i> attribute will be the prompt used for the
City column when the report is rendered.</p>


<p class=MsoNormal>The bottom of the dialog box is used to specify whether the
values selected by the customer should be applied to the final filter for the
report. Associate the Data Object and Value Columns with a column in the
recordset by selecting a column from the drop-down list of available columns.
Optionally, provide a caption for the cascading filter level.</p>


<p class=MsoNormal style='margin-left:4.5pt'><span style='font-size:20.0pt;
Wingdings;color:red'>I</span><span style='font-size:20.0pt;
color:red'> </span>In prior versions of Ad Hoc, the only column that was used
to filter the recordset was the last value of the cascading filter. In our
example, had the City selected been  Vienna , the resultant WHERE clause would
have been<span style=''> </span><span
style='font-size:11.0pt;'>WHERE City =  Vienna </span><span
style='font-size:11.0pt'> </span>and the other elements of the cascading filter
would have been ignored. They were only used in the filtering levels of the cascading
filter itself. With v12 of Ad Hoc, all levels of a cascading filter may be
applied to the recordset.</p>


<p class=MsoNormal>Our completed filter dialog box to filter the report on all cascading
filter level values would look like:</p>



<p class=MsoNormal><img border=0   id="Picture 321"
src="Report_Design_Guide_files/image067.jpg"></p>


<p class=MsoNormal>Click <b>OK</b> to save this filter parameter.<br clear=all
style='page-break-before:always'>
</p>


</body>
</html>
