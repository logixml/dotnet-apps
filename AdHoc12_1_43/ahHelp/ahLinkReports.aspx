<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc457221802"></a><a name="_Toc457221166">Linking Reports</a></h2>


<p class=MsoNormal><span style=''>Administrators
have the ability to enable drill-through functionality by offering a hyperlink in
one report (the  source  report) that launches the view of another report (the
 linked  report). </span></p>


<p class=MsoNormal><span style=''>When the
hyperlink in the source report is selected, record-level specific data is
passed from the source report to the linked report's respective parameters.</span></p>


<p class=MsoNormal><img border=0   id="Picture 371"
src="System_Admin_Guide_files/image111.jpg"></p>


<p class=MsoNormal><span style=''>Linking two
existing reports is a two-part process:</span></p>


<ol style='margin-top:0in' start=1 type=1>
 <li class=MsoNormal><span style=''>Create the
     linked report. Any report can be the target of a link.</span></li>
 <li class=MsoNormal><span style=''>Configure a
     link on a data object's column which can be enabled in a source report.</span></li>
</ol>


<p class=MsoNormal><span style=''>We do not
recommend using  Ask pamaramters  in a linked report.</span></p>


<p class=MsoNormal><span style=''>To configure
a link on a data object column, refer to the <b>Setting Links</b> section of this
document for information about setting up a link parameter to a report created
in the application.</span></p>


<p class=MsoNormal><span style=''>Users
building reports have the option of disabling object links from the Report
Builder.</span></p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
