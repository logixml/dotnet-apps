<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945099"></a><a name="_Toc382291595">The Report Builder</a></h2>


<p class=MsoNormal>The Report Builder is a comprehensive and flexible interface
designed to build full-featured reports. </p>


<p class=MsoNormal>Click <b>New Report</b> to access the Report Builder.<br>
<br>
</p>


<p class=MsoNormal><i>Navigating the Report Builder</i></p>


<p class=MsoNormal>The following is a high level overview of the Report Builder
and its components and options. The details of each component are covered
elsewhere in this document.</p>


<p class=MsoNormal>When using the Report Builder, the core steps in building a
report involve:</p>

<p class=MsoNormal> </p>

<ul style='margin-top:0in' type=disc>
 <li class=MsoNormal>Selecting a report template.</li>
 <li class=MsoNormal>Selecting a source of the data for the report.</li>
 <li class=MsoNormal>Selecting the display elements.</li>
 <li class=MsoNormal>Configuring the display elements. </li>
 <li class=MsoNormal>Reviewing the output.</li>
</ul>


<p class=MsoNormal>These steps will be presented in this section to show the
navigation options.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>After clicking the <b>New Report</b> button, a template
selection dialog box will be displayed:<br>
<br>
</p>


<p class=MsoNormal><img   id="Picture 297"
src="Report_Design_Guide_files/image021.jpg"></p>


<p class=MsoNormal>Six standard templates are included, in addition to the <i>Blank</i>
template. The <i>Blank</i> template is initially selected. Select a template
and then click <b>OK</b> to proceed.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>The <i>Select Data</i> dialog box will be displayed to allow
the user to select the data objects to be used in the report:</p>



<p class=MsoNormal><img   id="Picture 300"
src="Report_Design_Guide_files/image022.jpg"></p>


<p class=MsoNormal>The <i>Add/Remove</i> tab allows you to select all of the
data objects that form the basis of the report. Until data objects have been
selected for the report the other tabs will be disabled. </p>


<p class=MsoNormal>The <i>Calculated Columns</i> tab allows you to create
custom columns.</p>


<p class=MsoNormal>The <i>Sort</i> tab determines the initial sort sequence of
the data returned from the reporting database. </p>


<p class=MsoNormal>The <i>Filter </i>tab allows you to specify filter criteria
for the data.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Notes:</b></p>
  <p class=NotesCxSpMiddle>Access to the <i>Calculated Columns</i> tab is
  controlled by a right/permission. The tab may not be displayed to all users.<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>The <i>Database</i> drop-down may not be displayed
  if there is only one reporting database or if Multiple Connections has not
  been enabled.<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>The <i>Data Objects In</i> drop-down list may not be
  displayed if no object categories have been defined.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>The <i>Save as New</i> option for the data source will
  not be displayed if the Multiple Data Sources option has not been enabled.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>The list of data objects available to you may be filtered by
the <i>Data Objects in</i> drop-down list. These are categories of data
objects.</p>


<p class=MsoNormal>The data objects tree identifies the data objects available
to you. As objects are selected the tree will be refreshed to show the related
data objects. The columns of a data object can be viewed by expanding the data
object. Sample data for the object can be displayed by clicking the <img
  id="Picture 17" src="Report_Design_Guide_files/image023.jpg"> icon.</p>


<p class=MsoNormal>The <i>Information </i>panel will display any helpful
descriptions of the data object or column that the mouse in on. Information
will be displayed only if the System Administrator has specified the
descriptions as part of the object/column configuration.</p>


<p class=MsoNormal>The <b>Preview Selected Data</b> button will display a
report dialog box based on the data objects selected. From this dialog box the
selections can be confirmed and all of the dialog boxes dismissed.</p>


<p class=MsoNormal>The <b>OK</b> button saves the currently selected data
objects and dismisses the dialog box.</p>


<p class=MsoNormal>If you have access to multiple reporting databases and the
 (All)  option was selected in the Database filter, the <b>Modify Data Source</b>
dialog box will allow you to select data objects from any of the reporting
databases.</p>


<p class=MsoNormal><img   id="Picture 302"
src="Report_Design_Guide_files/image024.jpg"></p>


<p class=MsoNormal>Notice in the image above that the Database filter is set to
 (All) . The tree of data objects includes two databases;  Northwind  and  Reporting
Metadata . The initial presentation of the tree in this scenario is normally
for all databases to be fully expanded.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpLast>Presentation of all of the objects for all databases
  does not imply any relationships across databases. If the user selects a data
  object from the list, the tree will be refreshed with the related data
  objects.</p>
  </div>
  </td>
 </tr>
</table>

</div>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>



<p class=MsoNormal>Once the data objects for the report have been selected, the
full Report Builder interface is available:</p>


<p class=MsoNormal><img   id="Picture 19"
src="Report_Design_Guide_files/image025.jpg"></p>


<p class=MsoNormal><br>
At the top of the page is a  breadcrumb trail  followed by a help icon. To
return to the <b>Personal Reports</b> list, click its label in the breadcrumb
trail. Click the <img   id="Picture 20"
src="Report_Design_Guide_files/image026.jpg"> icon to receive a basic
description of and help for the page.</p>


<p class=MsoNormal>Beneath the breadcrumb trail is the current name of the
report being created/edited. In this example, the report name is  New Report .
This is the standard name for a report that has yet to be saved.</p>


<p class=MsoNormal>The <b>Save</b> button presents a dialog box that allows the
user to specify the report name, description, expiration date and target folder
for the report definition. Subsequent <b>Save</b> operations require no
confirmation.</p>


<p class=MsoNormal>The <b>Save As </b>button presents the same dialog box and
allows you to save the current report definition to a new name and target.</p>


<p class=MsoNormal>The <b>Preview</b> button launches the report in its current
state for review.</p>


<p class=MsoNormal>The <b>Select Data</b> button displays the same <b>Select
Data</b> dialog box that was displayed when the report was created, populated with
the current settings.</p>


<p class=MsoNormal>The <b>Select Report Style</b> button displays a dialog box
that allows you to select the style sheet used to render the report. The
default style sheet is determined by the System Administrator.</p>

<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>On the left side of the display is an <b>ADD</b> frame that
contains the choices of display elements available to the user. The content of
the <b>ADD</b> frame may be different for each user depending on their Role/Permissions/Rights.<br>
<br>
</p>


<p class=MsoNormal><img  
src="Report_Design_Guide_files/image027.jpg" align=left hspace=12>The display
elements in the ADD frame can be added to the report by double-clicking them or
dragging them to the proper location in the <b>MODIFY</b> frame.</p>


<p class=MsoNormal>Only one header element is permitted in a report.</p>

<p class=MsoNormal><br>
<br>
</p>

<p class=MsoNormal>The Table element initially presents data as a grid.</p>



<p class=MsoNormal>The Crosstab element tabulates values and organizes them by
header and label.</p>

<p class=MsoNormal><br>
<br>
<br>
</p>

<p class=MsoNormal>The Chart option presents a list of charting options.</p>



<p class=MsoNormal>The Label option allows you to annotate the report.</p>



<p class=MsoNormal>The image option allows you to select an image to be
displayed in the report.</p>



<p class=MsoNormal>The Exports option allows you to present a list of export
formats to the end-user running the report. Typical options include Word,
Excel, PDF, CSV, and XML.</p>





<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst>&nbsp;</p>
  <p class=NotesCxSpMiddle><b><img   id="Picture 21"
  src="Report_Design_Guide_files/image028.jpg"><img  
  id="Picture 22" src="Report_Design_Guide_files/image029.jpg"></b></p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpLast><b>Note: <br>
  </b>The Exports option is not displayed if the current  mobile state  is set
  to On. Typically the  mobile state  is set to Off. Exports are not supported
  for reports rendered on mobile devices.</p>
  </div>
  </td>
 </tr>
</table>

</div>



<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


<p class=MsoNormal>Adjacent to the <b>ADD</b> frame is the <b>MODIFY</b> frame.
The <b>MODIFY</b> frame determines the presentation order of the display
elements.</p>



<p class=MsoNormal><img  
src="Report_Design_Guide_files/image030.jpg" align=left hspace=12>In this
example the report will display a header, a data table, a chart, and export
options.</p>




<p class=MsoNormal>The red  X  allows you to remove a display element.</p>










<p class=MsoNormal>The <img   id="Picture 23"
src="Report_Design_Guide_files/image031.jpg">icon allows you to switch the
chart type in the report. The same list of charting options will be displayed
as when adding a Chart to the report.</p>


<p class=MsoNormal>Display elements can be rearranged by dragging and dropping them
where needed.</p>


<p class=MsoNormal>Highlighting a display element will refresh the
configuration area to show the options related to the element.</p>

<i><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></i>





<p class=MsoNormal>Centered on the page is the tabbed <i>Configuration</i>
panel. Each tab allows the end-user to configure some aspect of a display
element. Every display element will have at least one configuration tab
associated with it.</p>


<p class=MsoNormal><img   id="Picture 24"
src="Report_Design_Guide_files/image032.jpg"></p>


<p class=MsoNormal>Each display element has a fixed set of configuration tabs.
Highlight the display element in the MODIFY frame to refresh the configuration
area with the tabs and content relevant to the highlighted element.</p>


<p class=MsoNormal>At the bottom of the page is the <i>Live Preview</i> panel which
will show the report in real-time as it s being developed. As changes are made
to the configuration, the impact on the report can be viewed  live .<br>
<br>
</p>


<p class=MsoNormal><img   id="Picture 25"
src="Report_Design_Guide_files/image033.jpg"></p>


<p class=MsoNormal>The Live Preview panel is initially collapsed. Click the  <img
  id="Picture 26" src="Report_Design_Guide_files/image034.jpg">  or
 <img   id="Picture 27"
src="Report_Design_Guide_files/image035.jpg">  buttons to collapse or expand
the panel in the current browser window. Click the  <img  
id="Picture 28" src="Report_Design_Guide_files/image036.jpg">  button to
display the report preview in a separate browser window.</p>


<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst>Notes:</p>
  <p class=NotesCxSpMiddle>The live preview is refreshed with each change to
  the report configuration. The preview is a full rendering of the display
  element as it would appear in the report. Rendering the display element will
  impact the performance of the Report Builder, particularly for large volumes
  of data.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>When creating a report, the Live Preview panel will
  not begin displaying anything until a Data Object has been selected.<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>When exiting the Report Builder, Ad Hoc will
  remember the state of the Live Preview panel, expanded or collapsed. When
  next launched, the Report Builder will display the Live Preview panel in the
  same state, regardless of the report being viewed.</p>
  <p class=NotesCxSpMiddle><br>
  If the Live Preview feature is not offered in the Report Builder, contact the
  System Administrator to enable it.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>After an extended period of inactivity, the web
  server will end a browser session and work may be lost. It is a good idea to
  save report modifications often to avoid this scenario.</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle>If changes to the report have not been saved, a
  confirmation dialog box will be displayed when attempting to leave the page:</p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpMiddle><img   id="Picture 304"
  src="Report_Design_Guide_files/image037.jpg"></p>
  <p class=NotesCxSpMiddle>&nbsp;</p>
  <p class=NotesCxSpLast>Each browser will display some version of this
  confirmation</p>
  </div>
  </td>
 </tr>
</table>

</div>




<span style='font-size:12.0pt;'><br clear=all
style='page-break-before:always'>
</span>


</body>
</html>
