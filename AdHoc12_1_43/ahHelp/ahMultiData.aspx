<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h1><a name="_Toc456945142"></a><a name="_Toc382291637">Multiple Data Sources</a></h1>



<p class=MsoNormal>Most Ad Hoc reports rely on a single data source. All of the
display elements (display tables, crosstabs, and charts) in the report display
data from that source. Logically this makes sense because analytical reports
are typically used to make a point, tell a story, or reveal information about
the data.</p>


<p class=MsoNormal>Ad Hoc is initially configured to accept data from a single
data source; however, if the System Administrator can configure Ad Hoc to allow
the end-user to select multiple data sources for a report.</p>


<p class=MsoNormal>If the System Administrator enables the  Multiple Data Sources 
option, the <i>Select Data</i> dialog box changes for all users. </p>


<p class=MsoNormal>To create a new data source, click the <b>Modify Data Source</b>
button. The <i>Select Data</i> dialog box is displayed.</p>



<p class=MsoNormal><img border=0   id="Picture 226"
src="Report_Design_Guide_files/image223.jpg"></p>


<p class=MsoNormal>The drop-down list of data sources appears at the bottom of
the dialog box along with a <b>Save as New</b> button. Select the data objects
for the new data source from the data objects tree and click the <i>Save as New</i>
button. Enter a name for the data source in the <i>Save as New </i>dialog box
and click <b>OK</b> to confirm it.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><a name="_Toc297114557"><b>Note: </b></a></p>
  <p class=NotesCxSpLast>Every data-oriented display element in the report
  needs a data source. As they are added to the report definition, the
  assumption is that the currently selected data source is to be used.</p>
  </div>
  </td>
 </tr>
</table>

</div>


<b><span style='font-size:13.0pt;'><br
clear=all style='page-break-before:always'>
</span></b>

<h3 align=right style='text-align:right'>&nbsp;</h3>

</body>
</html>
