<html>
<head>
<link rel="stylesheet" type="text/css" href="../AdHocHelp.css" />
</head>
<body style='border:0;overflow:visible;'>

<h2><a name="_Toc456945138"></a><a name="_Toc382291633">Modifying Reports</a></h2>


<p class=MsoNormal><b><br>
To modify a report:</b></p>


<p class=MsoNormal>To modify a report, click the <b>Edit</b> button associated
with the report. The Report Builder interface will be invoked.</p>



<p class=MsoNormal><b>To rename a report:</b></p>


<p class=MsoNormal>To rename a report, hover your mouse cursor over the <b>More</b>
button and select <i>Rename</i> from its list. The report properties will be
displayed:</p>



<p class=MsoNormal><b><img border=0   id="Picture 272"
src="Report_Design_Guide_files/image213.jpg"></b></p>


<p class=MsoNormal><br>
Modify the <i>Report Name, Description,</i> and <i>Expiration Date</i> as
desired.</p>


<p class=MsoNormal>Click <b>Save </b>to save the changes.</p>


<b><span style='font-size:12.0pt;'><br
clear=all style='page-break-before:always'>
</span></b>


<p class=MsoNormal><b>To delete one or more reports and its associated
archives:</b></p>


<p class=MsoNormal>Select the desired reports by checking their checkboxes. Click
 <b>Delete</b> to remove the selected reports.</p>


<p class=MsoNormal>Click <b>OK</b> when prompted to confirm the removal.</p>



<div>

<table cellspacing=0 cellpadding=0 hspace=0 vspace=0 align=left>
 <tr>
  <td valign=top align=left style='padding-top:0in;padding-right:0in;
  padding-bottom:0in;padding-left:0in'>
  <div style='border:solid #F2F2F2 2.25pt;padding:6.0pt 4.0pt 6.0pt 4.0pt;
  background:#F2F2F2'>
  <p class=NotesCxSpFirst><b>Note:</b></p>
  <p class=NotesCxSpMiddle>Deleting a report removes the report from the
  Reports list.<br>
  <br>
  </p>
  <p class=NotesCxSpMiddle>A copy of deleted reports and dashboards are placed
  in the <span style='font-size:11.0pt;'>_Definitions\_Reports\_Backup</span>
  folder. Imported reports will no longer be visible in the application
  interface but will not be deleted from the <span style='font-size:11.0pt;
  '>_Reports</span><span style='font-size:14.0pt'> </span>folder.<br>
  <br>
  </p>
  <p class=NotesCxSpLast>Archived copies of deleted report(s) will be deleted
  according to the setting specified by the System Administrator in the
  Application Settings webpage.</p>
  </div>
  </td>
 </tr>
</table>

</div>


<h4><a name="_Toc382291634">&nbsp;</a></h4>

</body>
</html>
