<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="Search.ascx.vb" Inherits="LogiAdHoc.Search" %>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch" CssClass="formSearch">
    <asp:UpdatePanel ID="UP1" runat="server" UpdateMode="Conditional" RenderMode="Inline">
        <ContentTemplate>
            <asp:TextBox ID="SearchText" runat="server" CssClass="searchText" title="Search Text" meta:resourcekey="SearchTextResource1"></asp:TextBox>
            <span ID="spanClear" style="display:none;" onclick="var input = document.getElementById('srch_SearchText'); input.value = ''; input.focus(); __doPostBack('spanClear');"></span> <!-- new clear button -->
            <asp:ImageButton ID="imgClear" runat="server" SkinID="imgClear" CssClass="btnClear" CausesValidation="false" AlternateText="Clear search phrase" ToolTip="Clear search phrase" meta:resourcekey="btnClearSrch" />
            <AdHoc:LogiButton ID="btnSearch" Text="Search" runat="server" OnClick="btnSearch_Click" CausesValidation="false" meta:resourcekey="btnSearchResource1" UseSubmitBehavior="false" Icon ="Search"/>
            <asp:Panel ID="pnlMessage" runat="server">
                <asp:Localize ID="Localize2" runat="server" meta:resourcekey="LiteralResource2" Text="No records found."></asp:Localize>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>

                