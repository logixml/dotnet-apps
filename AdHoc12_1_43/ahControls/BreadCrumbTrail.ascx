<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="BreadCrumbTrail.ascx.vb" Inherits="LogiAdHoc.BreadCrumbTrail" %>

<asp:Panel id="pnlBCTrail" runat="server" cssClass="bctrail">
    <span id="navtrail">            
        <asp:SiteMapPath ID="SiteMapPath1" runat="server" meta:resourcekey="SiteMapPath1Resource1" />
    </span>
    <span id="help" class="clsHelp">
        <asp:ImageButton ID="imgbHelp" runat="Server" Visible="false" ImageUrl="~/ahImages/iconHelpText.gif" SkinID="imgbHelp" 
            OnClientClick="return false;" AlternateText="<%$ Resources:LogiAdHoc, MainMenu_Help %>"/>
        <div id="pnlHelp" runat="server" class="hiPopupMenu" style="display:none;"></div>
<%--        <ajaxToolkit:HoverMenuExtender ID="HoverMenuExtender1" runat="Server"
            PopupControlID="pnlHelp"
            PopupPosition="Right"
            TargetControlID="imgbHelp"
            PopDelay="25" />
--%>        
        <ajaxToolkit:PopupControlExtender ID="PopupControlExtender1" runat="server" 
            TargetControlID="imgbHelp" Enabled="True" ExtenderControlID="" DynamicServicePath="" 
            PopupControlID="pnlHelp" Position="Right">
        </ajaxToolkit:PopupControlExtender>

    </span>
</asp:Panel>