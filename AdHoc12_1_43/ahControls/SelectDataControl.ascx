<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="SelectDataControl.ascx.vb" Inherits="LogiAdHoc.SelectDataControl" %>
<%@ Register Assembly="RadTreeView.Net2" Namespace="Telerik.WebControls" TagPrefix="radT" %>

<%--<%@ Register TagPrefix="cc1" Assembly="LGXAHWCL" Namespace="LGXAHWCL" %>--%>

<asp:Panel ID="pnlSelectDataControl" runat="server">
    <table cellpadding="5" cellspacing="10">
        <tr>
            <td style="background-color: White; border: solid 1px DarkGray;">
                <div> 
                    <%--<cc1:DataTreeView ID="tvObjects" runat="server" ShowCheckBoxes="All" ShowExpandCollapse="true" 
                        ShowLines="true" >
                        <NodeStyle HorizontalPadding="4px" />
                        <SelectedNodeStyle HorizontalPadding="4px" CssClass="treenodeSelected" />
                    </cc1:DataTreeView>--%>
                    <radT:RadTreeView ID="rdtvObjects" runat="server" CheckBoxes="True" Height="300px" Width="300px" RetainScrollPosition="True" AutoPostBack="TRUE">
                    </radT:RadTreeView>
                </div>
            </td>
            <td style="background-color: White; border: solid 1px DarkGray;" width="100%">
                <div id="divColumns" runat="server" style="height: 300px; overflow: auto;">
                    <asp:Label ID="lblColumns" runat="server"></asp:Label>
                </div>
            </td>
        </tr>
    </table>
</asp:Panel>