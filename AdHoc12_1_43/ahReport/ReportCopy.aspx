<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ReportCopy.aspx.vb" Inherits="LogiAdHoc.ahReport_ReportCopy"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="wizard" TagName="datebox1" Src="../ahControls/DateBox.ascx" %>
<%@ Register TagPrefix="Adhoc" TagName="PagingControl" Src="~/ahControls/PagingControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Copy Report</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>

</head>
<body id="bod">
    <Adhoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
    	<div id="submenu"><AdHoc:NavMenu id="subnav" runat="server" /></div>	
        <Adhoc:BreadCrumbTrail ID="bct" runat="server" Key="CopyReport" />
        <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server" />

        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
            function EndRequestHandler(sender, args) {
                noMessage=false;
                $find('ahSavePopupBehavior').hide();
            }
        </script>

        <div class="divForm">
            <asp:UpdatePanel ID="UP2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <input type="hidden" id="ahDirty" runat="server" />
                    <input type="hidden" id="OriginalOwner" runat="server" />
                    <input type="hidden" id="ResultOwner" runat="server" />
                    <input type="hidden" id="PhysicalName" runat="server" />
                    <table>
                        <tr id="trReportName" runat="server">
                            <td>
                                <div id="divReportNameLabel" runat="server">
                                    <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:LogiAdHoc, ReportName %>"></asp:Localize>:
                                </div>
                            </td>
                            <td>
                                <div id="divSingleReport" runat="server">
                                    <%--<asp:Label ID="ReportList" runat="server" meta:resourcekey="ReportListResource1" />--%>
                                    <input type="text" id="ReportName" size="45" maxlength="200" runat="server" title="<%$ Resources:LogiAdHoc, ReportName %>">
                                    <asp:RequiredFieldValidator ID="rtvReportName" ControlToValidate="ReportName" ErrorMessage="Report Name is required."
                                        runat="server" meta:resourcekey="rtvReportNameResource1">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="cvReportNameUnique" ControlToValidate="ReportName"
                                        ErrorMessage="Report name already exist." OnServerValidate="IsReportNameUnique"
                                        runat="server" meta:resourcekey="cvReportNameUniqueResource1">*</asp:CustomValidator>
                                </div>
                                <div id="divMultipleReports" runat="server">
                <div id="data_main">
                                    <asp:GridView ID="grdMain" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                        CssClass="grid" OnRowDataBound="OnItemDataBoundHandler" meta:resourcekey="grdMainResource1">
                                        <Columns>
                                            <asp:TemplateField meta:resourcekey="TemplateFieldResource1">
                                                <HeaderStyle Width="30px" />
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                                    <asp:CheckBox ID="CheckAll" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                                    <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>"/>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Report" meta:resourcekey="TemplateFieldResource1">
                                                <ItemTemplate>
                                                    <input type="hidden" id="ReportID" runat="server" />
                                                    <input type="hidden" id="PhysicalName" runat="server" />
                                                    <asp:Label ID="OrigReportName" runat="server" meta:resourcekey="OrigReportNameResource1" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="New Report Name" meta:resourcekey="TemplateFieldResource2">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="NewReportName" MaxLength="255" Width="200px" runat="server" title="New Report Name" meta:resourcekey="ColumnLabelResource1" />
                                                    <asp:RequiredFieldValidator ID="rtvColumnLabel" runat="server" ControlToValidate="NewReportName"
                                                        ErrorMessage="Report Name is required." meta:resourcekey="rtvColumnLabelResource1">*</asp:RequiredFieldValidator>
                                                    <asp:CustomValidator ID="cvReportNameUnique" ControlToValidate="NewReportName"
                                                        ErrorMessage="Report name already exist." OnServerValidate="IsNewReportNameUnique"
                                                        runat="server" meta:resourcekey="cvNewReportNameUniqueResource1">*</asp:CustomValidator>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description" meta:resourcekey="TemplateFieldResource3">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="grdDescription" TextMode="MultiLine" Columns="45" Rows="3" MaxLength="255"
                                                        runat="server" title="Description" meta:resourcekey="ColumnLabelResource2"/>
                                                    <asp:CustomValidator ID="cvGridMaxLength" runat="server" ErrorMessage="<%$ Resources:Errors, Err_Description255 %>"
                                                        ControlToValidate="grdDescription" OnServerValidate="ReachedMaxLength">*</asp:CustomValidator>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Expiration Date" meta:resourcekey="TemplateFieldResource4">
                                                <ItemTemplate>
                                                    <wizard:datebox1 ID="ReportExpirationDate" runat="server" Required="false" />
                                                    <asp:CustomValidator ID="cvReportExpirationDate" ControlToValidate="grdDescription"
                                                        ErrorMessage="Report Expiration Date should be greater than today." OnServerValidate="IsGridReportExpirationDateValid"
                                                        runat="server" ValidateEmptyText="true" meta:resourcekey="cvReportExpDateResource1">*</asp:CustomValidator>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle HorizontalAlign="Center" />
                                        <HeaderStyle CssClass="gridheader" />
                                        <PagerTemplate>
                                            <Adhoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                                        </PagerTemplate>
                                        <RowStyle CssClass="gridrow" />
                                        <AlternatingRowStyle CssClass="gridalternaterow" />
                                    </asp:GridView>
                                </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:LogiAdHoc, CurrentOwner %>"></asp:Localize>:
                            </td>
                            <td>
                                <asp:Label ID="username" runat="server" meta:resourcekey="usernameResource1" /></td>
                        </tr>
                        <tr id="trDescription" runat="server">
                            <td runat="server">
                                <asp:Label ID="lblDescription" runat="server" AssociatedControlID="Description" Text="<%$ Resources:LogiAdHoc, Description %>" />:
                            </td>
                            <td runat="server">
                                <asp:TextBox ID="Description" TextMode="MultiLine" Columns="45" Rows="3" runat="server" />
                                <asp:CustomValidator ID="cvMaxLength" runat="server" ErrorMessage="<%$ Resources:Errors, Err_Description255 %>"
                                    ControlToValidate="Description" OnServerValidate="ReachedMaxLength">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr id="trExpirationDate" runat="server">
                            <td width="100px">
                                <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:LogiAdHoc, ReportExpirationDate %>"></asp:Localize>:
                            </td>
                            <td>
                                <wizard:datebox1 ID="ReportExpirationDate" runat="server" Required="false" />
                                <asp:CustomValidator ID="cvReportExpirationDate" ControlToValidate="Description"
                                    ErrorMessage="Report Expiration Date should be greater than today." OnServerValidate="IsReportExpirationDateValid"
                                    runat="server" ValidateEmptyText="true" meta:resourcekey="cvReportExpDateResource1">*</asp:CustomValidator>
                            </td>
                        </tr>
                        <tr id="trUserGroup" runat="server">
                            <td runat="server">
                                <asp:Label ID="lblUserGroup" runat="server" AssociatedControlID="UserGroup" Text="<%$ Resources:LogiAdHoc, UserGroup %>" />:
                            </td>
                            <td runat="server">
                                <asp:DropDownList ID="UserGroup" OnSelectedIndexChanged="UserGroup_SelectedIndexChanged"
                                    AutoPostBack="True" runat="server">
                                </asp:DropDownList></td>
                        </tr>
                        <tr id="FolderRow" runat="server">
                            <td runat="server">
                                <asp:Label ID="Label3" runat="server" AssociatedControlID="FolderTypeID" Text="<%$ Resources:LogiAdHoc, DestFolderType %>"></asp:Label>:
                            </td>
                            <td runat="server">
                                <asp:DropDownList ID="FolderTypeID" AutoPostBack="True" OnSelectedIndexChanged="FolderType_SelectedIndexChanged"
                                    runat="server">
                                    <asp:ListItem Value="0" Text="<%$ Resources:LogiAdHoc, Menu_SharedReports %>" />
                                    <asp:ListItem Value="1" Text="<%$ Resources:LogiAdHoc, Menu_MyReports %>" />
                                    <asp:ListItem Value="2" Text="<%$ Resources:LogiAdHoc, OtherUser %>" />
                                    <asp:ListItem Value="3" Text="<%$ Resources:LogiAdHoc, Menu_GlobalReports %>" />
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="UserRow" runat="server">
                            <td runat="server">
                                <asp:Label ID="Label4" runat="server" AssociatedControlID="OwnerUserID" Text="<%$ Resources:LogiAdHoc, NewOwner %>"></asp:Label>:
                            </td>
                            <td runat="server">
                                <asp:DropDownList ID="OwnerUserID" AutoPostBack="True" OnSelectedIndexChanged="Owner_SelectedIndexChanged"
                                    runat="server" />
                            </td>
                        </tr>
                        <tr id="ParentRow" runat="server">
                            <td valign="top" runat="server">
                                <asp:Label ID="lblFolder" runat="server" Text="<%$ Resources:LogiAdHoc, Folder %>" />:
                            </td>
                            <td runat="server">
                                <asp:TreeView ID="tvFolders" runat="server" ShowLines="True" EnableClientScript="false">
                                    <NodeStyle HorizontalPadding="4px" ImageUrl="~/ahImages/iconSFolder.gif" />
                                    <SelectedNodeStyle HorizontalPadding="4px" ImageUrl="~/ahImages/iconSFolderOpen.gif"
                                        CssClass="treenodeSelected" />
                                </asp:TreeView>
                            </td>
                        </tr>
                    </table>
                    <ul runat="server" id="ErrorList" class="validation_error">
                        <li>
                            <asp:Label runat="server" ID="lblValidationMessage" meta:resourcekey="lblValidationMessageResource1"></asp:Label>
                        </li>
                    </ul>
                    <div id="divButtons" class="divButtons">
                        <AdHoc:LogiButton ID="Save" Text="<%$ Resources:LogiAdHoc, Save %>"
                            OnClick="Save_OnClick" ToolTip="<%$ Resources:LogiAdHoc, SaveReportListTooltip %>"
                            runat="server" UseSubmitBehavior="false" />
                        <AdHoc:LogiButton ID="Cancel" Text="<%$ Resources:LogiAdHoc, BackToReportsList %>"
                            CausesValidation="False" OnClick="Cancel_OnClick" ToolTip="<%$ Resources:LogiAdHoc, BackToReportsListTooltip %>"
                            runat="server" />
                        <asp:ValidationSummary ID="vsummary" runat="server" meta:resourcekey="vsummaryResource1" />
                    </div>
<br />
                    <div id="divSaveAnimation">
                        <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                        <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                            TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                            DropShadow="False">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                            <table><tr><td>
                            <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                            </td>
                            <td valign="middle">
                            <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                            </td></tr></table>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="Cancel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </form>
</body>
</html>
