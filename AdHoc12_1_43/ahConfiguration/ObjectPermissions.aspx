<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ObjectPermissions.aspx.vb"
    Inherits="LogiAdHoc.ahConfiguration_ObjectPermissions" Culture="auto" meta:resourcekey="PageResource1"
    UICulture="auto" %>

<%@ Register Assembly="System.Web.Extensions" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register TagPrefix="AdHoc" TagName="MainMenu" Src="~/ahControls/MainMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="NavMenu" Src="~/ahControls/NavMenu.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="BreadCrumbTrail" Src="~/ahControls/BreadCrumbTrail.ascx" %>
<%@ Register TagPrefix="ctrl" TagName="ColPermissions" Src="~/ahControls/ColPermissions2.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="Search" Src="~/ahControls/Search.ascx" %>
<%@ Register TagPrefix="AdHoc" TagName="PagingControl" Src="../ahControls/PagingControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Data Object Access Rights</title>
    <link rel="shortcut icon" href="../ahImages/flav.ico" />
    <script language="JavaScript" type="text/javascript" src="../ahScripts/ahChange.js"></script>

    <script type="text/javascript">
        window.onbeforeunload = AlertOnExit;
    </script>

</head>
<body id="bod">
    <AdHoc:MainMenu ID="menu" runat="server" />
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div id="submenu">
        <AdHoc:NavMenu ID="subnav" runat="server" />
    </div>
        <asp:UpdatePanel ID="UPBct" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <AdHoc:BreadCrumbTrail ID="bct" runat="server" Key="DOARights" ParentLevels="1" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ddObjectID" EventName="SelectedIndexChanged" />
            </Triggers>
        </asp:UpdatePanel>

        <script type="text/javascript">
        
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            $find('ahSavePopupBehavior').hide();
            RestorePopupPosition('ahModalPopupBehavior');
        }
            
        Type.registerNamespace('AHScripts');
        AHScripts.PositionPanel = function() {}
        AHScripts.PositionPanel.prototype = {
            posPanel: function() {
                //document.documentElement.offsetHeight : height of visible area of the document
                //document.documentElement.scrollTop    : scroll position
                var pnl =document.getElementById("pnlColPerm");
                if (pnl != null) {
                    var centerX = (document.documentElement.offsetWidth) / 2; //.clientWidth / 2;
                    var centerY = (document.documentElement.offsetHeight) / 2;
                    var l = parseInt(centerX - 250 + document.documentElement.scrollLeft) ;
                    var t = parseInt(centerY - 250 + document.documentElement.scrollTop);
                    if (l < document.documentElement.scrollLeft) l = document.documentElement.scrollLeft;
                    if (t < document.documentElement.scrollTop) t = document.documentElement.scrollTop;
                    
                    pnl.style.top = t;
                    pnl.style.left = l;
                }
            }
        }
        AHScripts.PositionPanel.registerClass('AHScripts.PositionPanel');
        
        var panelUpdated = new AHScripts.PositionPanel();
        var postbackElement;
        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(pageLoaded);
        
        function beginRequest(sender, args) {
            postbackElement = args.get_postBackElement();
            SavePopupPosition('ahModalPopupBehavior');
        }
        function pageLoaded(sender, args) {
            var pnl =document.getElementById("pnlColPerm");
            if (pnl != null) {
                panelUpdated.posPanel();
            }
        }
        </script>

<table class="limiting"><tr><td>
        <table class="gridForm"><tr><td>
            <input type="hidden" id="ahDirty" name="ahDirty" runat="server" />
            <input type="hidden" id="ahParentID" name="ahParentID" runat="server" />
            
            <table id="tbDBConn" runat="server" class="tbTB">
                <tr>
                    <td width="130px">
                        <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:LogiAdHoc, DatabaseConnection %>"></asp:Localize>
                    </td>
                    <td>
                        <asp:Label ID="lblDBConn" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
            
            <table>
                <tr>
                    <td width="130px">
                        <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:LogiAdHoc, SelectedDataObject %>"></asp:Localize></td>
                    <td>
                        <asp:DropDownList ID="ddObjectID" AutoPostBack="True" runat="server" meta:resourcekey="ddObjectIDResource1" />
                    </td>
                </tr>
            </table>
            <br />
            <asp:UpdatePanel ID="UP1" UpdateMode="Conditional" runat="Server">
                <ContentTemplate>
                <div id="data_main">
                    <div id="activities">
                        <table width="100%" cellpadding="0" cellspacing="0">
                        <tr width="100%">
                        <td align="left" valign="top">
                        <AdHoc:LogiButton ID="btnSetFull" OnClick="SetFull_OnClick" ToolTip="Click to set access level for all selected roles to Full."
                            Text="Set To Full" runat="server" meta:resourcekey="btnSetFullResource1" />
                        <AdHoc:LogiButton ID="btnSetNone" OnClick="SetNone_OnClick" ToolTip="Click to set access level for all selected roles to None."
                            Text="Set To None" runat="server" meta:resourcekey="btnSetNoneResource1" />
                        </td>
                        <td align="right" valign="top">
                            <AdHoc:Search ID="srch" runat="server" Title="Find Roles" meta:resourcekey="AdHocSearch" />
                        </td>
                        </tr>
                        </table>
                    </div>
                    <ul class="validation_error" id="ErrorList" runat="server">
                        <li>
                            <asp:Label ID="lblValidationMessage" runat="server" meta:resourcekey="lblValidationMessageResource1"></asp:Label></li>
                    </ul>
                    <asp:GridView ID="grdMain" runat="server" AutoGenerateColumns="False" OnRowDataBound="OnItemDataBoundHandler"
                        OnSorting="OnSortCommandHandler" AllowSorting="True" DataKeyNames="RoleID" CssClass="grid"
                        AllowPaging="True" meta:resourcekey="grdMainResource1">
                        <HeaderStyle CssClass="gridheader"></HeaderStyle>
                        <PagerStyle HorizontalAlign="Center" />
                        <PagerTemplate>
                            <AdHoc:PagingControl ID="pageCtrl" runat="server" OnGotoNextPage="ChangePageIndex" />
                        </PagerTemplate>
                        <RowStyle CssClass="gridrow" />
                        <AlternatingRowStyle CssClass="gridalternaterow"></AlternatingRowStyle>
                        <Columns>
                            <asp:TemplateField HeaderText="Data Object" SortExpression="ObjectName" meta:resourcekey="TemplateFieldResource1">
                                <HeaderStyle Width="30px"></HeaderStyle>
                                <HeaderTemplate>
                                    <asp:Label ID="lblChkAll" runat="server" AssociatedControlID="CheckAll" Text="<%$ Resources:LogiAdHoc, SelectDeselectAll %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox ID="CheckAll" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselectAll %>"
                                        meta:resourcekey="CheckAllResource1" />
                                </HeaderTemplate>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:Label ID="lblChk" runat="server" AssociatedControlID="chk_Select" Text="<%$ Resources:LogiAdHoc, SelectDeselect %>" CssClass="NoShow"></asp:Label>
                                    <asp:CheckBox ID="chk_Select" runat="server" ToolTip="<%$ Resources:LogiAdHoc, SelectDeselect %>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Role" SortExpression="RoleName" meta:resourcekey="TemplateFieldResource2">
                                <ItemTemplate>
                                    <input type="hidden" id="RoleID" runat="server" />
                                    <asp:Label ID="RoleName" runat="server" meta:resourcekey="RoleNameResource1" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Access" SortExpression="AccessType" meta:resourcekey="TemplateFieldResource3">
                                <ItemTemplate>
                                    <asp:Label ID="AccessType" runat="server" meta:resourcekey="AccessTypeResource1" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="<%$ Resources:LogiAdHoc, Actions %>" meta:resourcekey="TemplateFieldResource4">
                                <HeaderStyle Width="50px"></HeaderStyle>
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgModify" SkinID="imgSingleAction" AlternateText="Modify Column Access Rights" 
                                        ToolTip="Modify Column Access Rights" runat="server" OnCommand="ModifyColumnPermission" 
                                        CausesValidation="false" meta:resourcekey="ModColPermResource1" />
                                        
                                    <%--<asp:Image ID="imgActions" AlternateText="<%$ Resources:LogiAdHoc, Actions %>" runat="server" 
                                        ToolTip="<%$ Resources:LogiAdHoc, Actions %>" ImageUrl="~/ahImages/arrowStep.gif" SkinID="imgActions" />
                                    <asp:Panel ID="pnlActionsMenu" runat="server" CssClass="popupMenu" 
                                        HorizontalAlign="Left" Wrap="false" style="display:none;">
                                        <div id="divView" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkView" runat="server" OnCommand="ViewColumnPermission" 
                                                Text="View Column Access Rights" ToolTip="View Column Access Rights"
                                                meta:resourcekey="ViewResource1"></asp:LinkButton>
                                        </div>
                                        <div id="divModColPerm" runat="server" class="hoverMenuActionLink" >
                                            <asp:LinkButton ID="lnkModColPerm" runat="server" OnCommand="ModifyColumnPermission" 
                                                Text="Modify Column Access Rights" ToolTip="Modify Column Access Rights"
                                                meta:resourcekey="ModColPermResource1"></asp:LinkButton>
                                        </div>
 				                    </asp:Panel>
                                    <ajaxToolkit:HoverMenuExtender ID="hme2" runat="server"
                                        PopupControlID="pnlActionsMenu" PopupPosition="right" 
                                        TargetControlID="imgActions" PopDelay="25" />--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>

                    <asp:Button runat="server" ID="Button1" Style="display: none" />
                    <ajaxToolkit:ModalPopupExtender runat="server" ID="ahModalPopup" BehaviorID="ahModalPopupBehavior"
                        TargetControlID="Button1" PopupControlID="ahPopup" BackgroundCssClass="modalBackground"
                        DropShadow="false" PopupDragHandleControlID="pnlDragHandle" RepositionMode="None">
                    </ajaxToolkit:ModalPopupExtender>
                    <%--<asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none;
                        width: 500; height: 500px; overflow: auto;">--%>
                    <asp:Panel runat="server" CssClass="modalPopup" ID="ahPopup" Style="display: none; width:600px;">
                        <asp:Panel ID="pnlDragHandle" runat="server" Style="cursor: hand;">
                        <div class="modalPopupHandle">
                            <table cellpadding="0" cellspacing="0" style="width: 100%"><tr><td>
                                    <asp:Localize ID="PopupHeader" runat="server"></asp:Localize>
                                </td>
                                <td style="width: 20px;">
                                    <asp:ImageButton ID="imgClosePopup" runat="server" 
                                        OnClick="imgClosePopup_Click" CausesValidation="false"
                                        SkinID="imgbClose" ImageUrl="../ahImages/remove.gif" 
                                        AlternateText="<%$ Resources:LogiAdHoc, CloseWindow %>" />
                            </td></tr></table>
                        </div>
                        </asp:Panel>
                        <div class="modalDiv">
                        <asp:UpdatePanel ID="upPopup" runat="server">
                            <ContentTemplate>
                                <ctrl:ColPermissions ID="ColumnPermissions" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddObjectID" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
            <div id="NoRole" runat="server">
     		    <br />
                <asp:Localize ID="Localize3" runat="server" meta:resourcekey="LiteralResource3" Text="No roles were found for this organization."></asp:Localize>                
            </div>
            
            <br />
            <AdHoc:LogiButton ID="btnCancel" runat="server" OnClick="Cancel_OnClick"
                ToolTip="<%$ Resources:LogiAdHoc, BackToObjectsTooltip1 %>" Text="<%$ Resources:LogiAdHoc, BackToObjects %>"
                CausesValidation="False" />
            
            <div id="divSaveAnimation">
                <asp:Button runat="server" ID="btnSaveAnimation" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender runat="server" ID="ahSavePopup" BehaviorID="ahSavePopupBehavior"
                    TargetControlID="btnSaveAnimation" PopupControlID="pnlSaving" BackgroundCssClass="modalBackground"
                    DropShadow="False">
                </ajaxToolkit:ModalPopupExtender>
                <asp:Panel id="pnlSaving" CssClass="savePopup" runat="server" style="display:none;">
                    <table><tr><td>
                    <asp:Image ID="imgSave" runat="server" SkinID="imgSaving" EnableViewState="false" AlternateText="<%$ Resources:LogiAdHoc, SaveImageAltText %>" />
                    </td>
                    <td valign="middle">
                    <asp:Label ID="lblSaveText" CssClass="lblSavePopup" runat="server" EnableViewState="false" Text="<%$ Resources:LogiAdHoc, PleaseWait %>"/>
                    </td></tr></table>
                </asp:Panel>
            </div>
        </td></tr></table>
</td></tr></table>
    </form>
</body>
</html>
