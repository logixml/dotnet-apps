// this code adds meta tags and HTML5 shim script to the HTML HEAD section
var headTag = document.getElementsByTagName('head')[0];
var charsetTag = document.createElement('meta');
charsetTag.setAttribute('charset', 'utf-8');
headTag.appendChild(charsetTag);
var viewportTag = document.createElement('meta');
viewportTag.setAttribute('viewport', 'width=device-width, initial-scale=1, maximum-scale=1');
headTag.appendChild(viewportTag);
var shimScript = document.createElement('script');
shimScript.setAttribute('src', 'https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js');
shimScript.setAttribute('language', 'JAVASCRIPT');
headTag.appendChild(shimScript);