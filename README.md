# Logi .Net Applications

### Catalog of Joe Viscome's .Net Working Applications

- This repository includes all applications that were used to test customer issues or cases throughout my time at Logi (at least the ones that are still available after multiple system migrations)
- It should not be taken as a full repository, but content should be pulled from individual applications as necessary.
- Most testing was conducted against a local database server (SQL Server) that is not included, so connection elements may need to be updated accordingly.

### How do I get set up?

- Each folder contains either an Ad Hoc or Info application that can be stood up on a new system or individual reports/files can be extracted and run in a separate application.
- Since BitBucket does not currently allow individual folders to be downloaded from a repository, it may be best to download/extract files associated with individual cases or scenarios.
- All of the SupportQ\<number\>\_\<year\> are the case review applications. Inside of these, definitions are generally included in a Month and Case Number folder. These case numbers may equate to Zendesk or the old Salesforce instance, depending on age.
