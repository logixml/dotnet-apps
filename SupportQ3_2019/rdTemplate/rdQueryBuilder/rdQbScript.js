function rdQbUpdateControls(sReport, sQbId, nSelectedControlNumber) {
    var sAjaxUrl = "rdAjaxCommand=RefreshElement&rdQbRefresh=True&rdQbControlNr=" + nSelectedControlNumber + "&rdRefreshElementID=" + sQbId + '&rdReport=' + sReport;
    sAjaxUrl = sAjaxUrl + '&rdQbNewCommand=True'; //#16126.
    rdAjaxRequestWithFormVars(sAjaxUrl);
}

function rdQbToggleJoinType(sJoinImageID) {
    var eleImage = document.getElementById(sJoinImageID)
    if (eleImage.src.indexOf("JoinInner") != -1) {
        eleImage.src = eleImage.src.replace("JoinInner", "JoinLeft")
    } else {
        eleImage.src = eleImage.src.replace("JoinLeft", "JoinInner")
    }

    var eleHidden = document.getElementById(sJoinImageID.replace("imgJoinType","rdJoinType"))
    if (eleImage.src.indexOf("JoinInner") != -1) {
        eleHidden.value = "Inner"
    } else {
        eleHidden.value = "Left"
    }

    //THIS NEEDS TO BE UPDATED TO CALL rdQbUpdateControls so the join type is updated in an Ajax way.
}

