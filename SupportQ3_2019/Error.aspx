<%@ Page Language="C#" AutoEventWireup="true"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Error</title>
</head>
<body>
	<p style='font-size:1.5em;'> 
	Either you do not have permissions, an error has occurred, or you have attempted to generate a report that does not exist.  
	<br/> 
	Please contact Keyfactor support at <a href="mailto:support@keyfactor.com">support@keyfactor.com</a> for assistance with report customization or migration to the new platform. 
	<br/> 
	Note: SQL Server Reporting Services will be deprecated in a future release as we migrate to the new reporting platform. Please contact your client success representative for more information.  
	</p> 
	<br/>
	Error message:
	<p style='color: Red;'>
	<%--This will show the actual error from Logi--%>
	<% =Session["rdLastErrorMessage"] %>
	</p>
</body>
</html>

