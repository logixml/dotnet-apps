$files = Get-ChildItem .
$finalXml = "<rdBookmarks>"
foreach ($file in $files) {
    [xml]$xml = Get-Content $file    
    $finalXml += $xml.rdBookmarks.InnerXML
}
$finalXml += "</rdBookmarks>"
([xml]$finalXml).Save("$pwd\final.xml")