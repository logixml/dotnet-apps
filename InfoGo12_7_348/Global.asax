<%@ Application Codebehind="Global.asax.vb" Inherits="rdWeb.Global" %>

<script RunAt="server">
Private Sub Application_PreSendRequestHeaders(ByVal source as Object, ByVal e as EventArgs)
   HttpContext.Current.Request.Headers.Add("X-Content-Type-Options", "nosniff")
End Sub
</script>