function identifyPositionInName(name, val) {
    //Split by word
    var words = name.split(' ');            

    //Does the word exist in the array?
    for (var i = 0; i < words.length; i++) {
        if (words[i].toLowerCase().indexOf(val.toLowerCase()) == 0) {
            return i+1;
        }
    }
    //Not found, return a high number
    return 99;
}

 

function identifyPositionInName_old(name, val) {
	//Split by word
	var words = name.split(' ');
	
	//Does the word exist in the array?
	for (var i = 0; i < words.length; i++) {
		if (words[i] == val) {
			return i+1;
		}
	}
	//Not found, return a high number
	return 99;
}