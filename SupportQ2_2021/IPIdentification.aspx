<%@ Language=C# %>
<HTML>
<script runat="server" language="CS">
  void Page_Load(object sender, System.EventArgs e) {
    var watch = new System.Diagnostics.Stopwatch();
    string sIpAddresses = "";

    try {
      watch.Start();
      System.Net.IPHostEntry heserver = System.Net.Dns.GetHostEntry(System.Environment.MachineName);


      foreach(System.Net.IPAddress curAdd in heserver.AddressList) {
        sIpAddresses += curAdd.ToString() + ", ";
      }
      watch.Stop();
    } catch (System.Net.Sockets.SocketException ex) {

    }

    this.IPAddress.Text = sIpAddresses.Substring(0, sIpAddresses.Length - 2);
    this.TimeSpent.Text = watch.ElapsedMilliseconds + " ms";
  }
</script>

<body>
  <div><label>IP Addresses: </label>
    <asp:label id="IPAddress" runat="server"></asp:label>
  </div>
  <div><label>Time to Retrieve Addresses: </label>
    <asp:label id="TimeSpent" runat="server"></asp:label>
  </div>
</body>

</HTML>