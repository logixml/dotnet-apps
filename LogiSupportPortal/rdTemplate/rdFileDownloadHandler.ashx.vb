﻿Imports System.Web
Imports System.Web.Services
Imports System.IO

Public Class rdFileDownloadHandler
    Implements System.Web.IHttpHandler

    Const FileWaitTimeout As Integer = 30000 'max 30 seconds to wait for file
    Const SleepTimeWhileWait As Integer = 200

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim fileName As String = context.Request("rdFileName")
        If String.IsNullOrEmpty(fileName) Then
            Throw New Exception("rdFileName can't be empty")
        End If

        Dim downloadFolderLocation As String = rdServer.rdState.sGetPhysicalPath() + Path.DirectorySeparatorChar + "rdDownload"

        Dim attemptsIterator As Integer = 0
        Dim maxAttemptsCount As Integer = FileWaitTimeout / SleepTimeWhileWait
        While attemptsIterator < maxAttemptsCount
            Dim fi As FileInfo = New FileInfo(Path.Combine(downloadFolderLocation, fileName))
            If (fi.Exists() AndAlso Not IsFileLocked(fi)) Then
                Dim fileExtension As String = Path.GetExtension(fileName)
                Select Case fileExtension.ToLower()
                    Case ".png"
                        context.Response.ContentType = "image/png"
                    Case ".js"
                        context.Response.ContentType = "application/json"
                    Case ".json"
                        context.Response.ContentType = "application/json"
                    Case ".xml"
                        context.Response.ContentType = "application/xml"
                    Case ".svg"
                        context.Response.ContentType = "image/svg+xml"
                End Select
                context.Response.StatusCode = 200
                context.Response.BinaryWrite(File.ReadAllBytes(fi.FullName))
                Return
            End If
            attemptsIterator += 1
            System.Threading.Thread.Sleep(SleepTimeWhileWait)
        End While

        context.Response.StatusCode = 404
    End Sub

    Public Function IsFileLocked(ByVal fi As FileInfo) As Boolean
        Dim stream As FileStream = Nothing
        Try
            stream = fi.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None)
        Catch
            If Not IsNothing(stream) Then
                stream.Close()
            End If
            Return True
        Finally
            If Not IsNothing(stream) Then
                stream.Close()
            End If
        End Try

        Return False
    End Function


    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class