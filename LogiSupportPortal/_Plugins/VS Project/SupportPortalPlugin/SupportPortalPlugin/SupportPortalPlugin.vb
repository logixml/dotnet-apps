﻿Imports System.Net
Imports System.IO


Public Class Plugin

    Public Sub Download_SFAttachment(ByRef rdObjects As rdPlugin.rdServerObjects)

        ' get the parameters passed to plugin
        Dim SF_FileID As String = rdObjects.PluginParameters("FileID").ToString()
        Dim targetFileName As String = rdObjects.PluginParameters("TargetFileName").ToString()
        targetFileName = rdObjects.ReplaceTokens("@Function.AppPhysicalPath~").ToString() + "\rdDownload\" + targetFileName

        ' define the Salesforce URL for Attachment retrieval
        Dim SF_URL As String = rdObjects.ReplaceTokens("@Session.SF_Instance~") + "/services/data/v38.0/sobjects/Attachment/" + SF_FileID + "/body"

        ' development aid only
        'rdObjects.AddDebugMessage("Plugin Msg1", "From Params", "Target Filename", targetFileName)
        'rdObjects.AddDebugMessage("Plugin Msg2", "Resolved Tokens", "SF URL", SF_URL)
        'rdObjects.AddDebugMessage("Plugin Msg3", "Resolved Tokens", "SF AccessToken", rdObjects.ReplaceTokens("@Session.SF_AccessToken~"))

        Dim myWebClient As New WebClient()
        myWebClient.Headers.Add("Authorization: Bearer " + rdObjects.ReplaceTokens("@Session.SF_AccessToken~"))
        Try
            myWebClient.DownloadFile(SF_URL, targetFileName)
            ' development aid only
            ' rdObjects.AddDebugMessage("Plugin Msg4", "Data Written", "Written OK")
        Catch ex As Exception
            rdObjects.AddDebugMessage("Plugin Msg", "Data Error", ex.Message)
        End Try
    End Sub

    Public Sub Upload_SFAttachment(ByRef rdObjects As rdPlugin.rdServerObjects)

        ' get the parameters passed to plugin and tokens
        Dim ParentID As String = rdObjects.PluginParameters("ParentID").ToString()
        Dim createdByID As String = rdObjects.ReplaceTokens("@Session.SSU_ID~")
        Dim sourceFileName As String = rdObjects.PluginParameters("SourceFileName").ToString()
        Dim sourceFileNameFull As String = rdObjects.ReplaceTokens("@Function.AppPhysicalPath~").ToString() + "\rdDownload\" + sourceFileName

        ' define the Salesforce URL for Attachment upload
        Dim SF_URL As String = rdObjects.ReplaceTokens("@Session.SF_Instance~") + "/services/data/v38.0/sobjects/Attachment/"

        ' construct the JSON payload
        Dim myJson As String = " { ""ParentId"" : """ + ParentID + """, ""Name"" : """ + sourceFileName + """, ""CreatedById"" :  """ + createdByID + """, ""body"" : """
        ' read and encode the temporary file
        myJson = myJson + Convert.ToBase64String(System.IO.File.ReadAllBytes(sourceFileNameFull)) + """ }"

        ' development aid only
        'rdObjects.AddDebugMessage("Plugin Msg1", "From Params", "JSON Payload", myJson)

        Dim myWebClient As New WebClient()
        myWebClient.Headers.Add("Content-type: application/json")
        myWebClient.Headers.Add("Authorization: Bearer " + rdObjects.ReplaceTokens("@Session.SF_AccessToken~"))

        Try
            myWebClient.UploadString(SF_URL, "POST", myJson)
            ' development aid only
            ' rdObjects.AddDebugMessage("Plugin Msg2", "Data Upload", "Uploaded OK")
        Catch ex As Exception
            rdObjects.AddDebugMessage("Plugin Msg", "Data Error", ex.Message)
        End Try
    End Sub


End Class
