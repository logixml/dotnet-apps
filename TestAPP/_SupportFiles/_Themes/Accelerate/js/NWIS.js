// Expand/Collapse Filter DIV Tags.
$( function() {

    $( "#div-Filter-Toggle" ).on( "click", function() {
		
		ToggleExpander("div-toggle-content");
      
	  	//$( "#lblShow" ).toggleClass( "TextBlue ThemeBold", 1000 );
		//$( "#lblHide" ).toggleClass( "TextBlue ThemeBold", 1000 );
		//$( "#lbl-FA-Filter" ).toggleClass( "TextBlue", 1000 );

		$(document.getElementById("lblShowHideText")).text(function(i, text){
          return text === "Hide" ? "Show" : "Hide";
      })

    });
	
} );

function ToggleExpander(id){
      // close all other expansion boxes and open the new target
      $("#"+id).slideToggle("slow");
}

function validateEmail(emailAddr)
{
	var emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*\.(\w{2}|(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum|wales|nhs|uk))$/;
	if (!emailReg.test(emailAddr))
		alert("ERROR: You must enter a valid email address.");
}

// After AJAX Function used to jump to a specific bookmark on the page.
function GotoBookmark (myBookmark) {
     Y.on('domready', function() {
		if (LogiXML.Ajax.AjaxTarget) {
            LogiXML.Ajax.AjaxTarget().on('reinitialize', function(){
                     
				// Goto Bookmark.	 
				window.location.href = "#" + myBookmark;
                                                
            });
      	}
 	}); 
}


// This function reflows all charts on a page
function reflowCharts(){
	// Get all charts on the page
	var chartsArray = Highcharts.charts.filter(function(n){ return n != undefined });
	// Loop through the array
	$.each(chartsArray,function(i){
		// Update the current chart
		chartsArray[i].reflow();
	});
}

// Reflow the charts on window load
//$(window).load( function(){reflowCharts();});
// Reflow the charts on windo resize
//$(window).resize( function(){reflowCharts();});

// Hide selected Series within a Chart Canvas element.
function HideChartSeries(cc, ccSeries)
{
	// Get the chart object
	var ChartCanvas = Y.one(cc);
	
	ChartCanvas.on('afterCreateChart', function(e) {      
		if (e.chart.series == null || e.chart.series.length == 0) {
			return;
		}
		
		// Populate an array with all of the series on the chart
		var mySeries = e.chart.series;
				
		// Loop through the series and call some function or method
		for (i = 0; i < mySeries.length; i+=1) {	
			//Hide the series
			if (mySeries[i].name == ccSeries) { mySeries[i].hide(); }
 		}
	});
}

function validateNADEX(prmNadex)
{

	var NadexReg = /^[A-z]{2}[0-9]{6}$/;
	
	if (prmNadex.length > 0)
		if (!NadexReg.test(prmNadex))
			alert("ERROR: You must enter a valid NADEX Username.");
}

function ValidateWCode(prmWCode)
{
	//alert(prmWCode);
	var WCodeReg = /^W|w[0-9]{5}$/;
	
	if (prmWCode.length > 0)
		if (!WCodeReg.test(prmWCode))
			alert("ERROR: You must enter a valid Practice Code.");
}
