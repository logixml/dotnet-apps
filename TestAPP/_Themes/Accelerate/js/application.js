/*
	Application.js is used for front end styling/UI
	You can add new functions to this file which can be called using studio elements.
*/

function urlParam(param) {
    param = param.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + param + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

// Start cookie set, get, and remove functions
	function setCookie(n,v,days) {
		if (days) {
			var date = new Date();
			date.setTime(date.getTime()+(days*24*60*60*1000));
			var expires = "; expires="+date.toGMTString();
		}
		else var expires = "";
		document.cookie = n+"="+v+expires+"; path=/";
	}

	function readCookie(n) {
		var nameEQ = n + "=";
		var ca = document.cookie.split(';');
		for(var i=0;i < ca.length;i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1,c.length);
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		}
		return null;
	}

	function eraseCookie(n) {
		setCookie(n,"",-1);
	}
// End cookie set, get, and remove functions
	
function ToggleMenu(Constant_SaveSidebarState){
	
	// ToggleMenu runs when the hamburger icon is selected on the top bar
	$("#Sidebar").toggleClass("Open");
	var currentState = $("#Sidebar").hasClass("Open");
	
	$("#HeaderTray").toggleClass("Open");
	var currentState = $("#HeaderTray").hasClass("Open");
	
	if(currentState===true && Constant_SaveSidebarState==="True"){
		setCookie("getSidebarState","Open",999);
	}else{
		setCookie("getSidebarState",null,999);
	}
}

function SidebarState(Constant_SaveSidebarState,Constant_SidebarState,Constant_AllowSidebarCollapse){
	// SidebarState runs on page load. It opens the sidebar, or keeps it closed, depending on the constants & state of the sidebar.
	if($(window).width() < 992){
		// Mobile view
		$("#Sidebar").removeClass("StaticOpen").removeClass("Open");
		$("#ShowSidebarToggle").removeClass("hidden");
		if($("#Header").is(":empty") && Constant_AllowSidebarCollapse!="True" && Constant_SidebarState!="Closed"){
			//The header has not loaded - call the function every 100ms
			setTimeout(function(){
				SidebarState(Constant_SaveSidebarState,Constant_SidebarState,Constant_AllowSidebarCollapse);
			},100);
		}
	}else{
		//Standard process
		if(Constant_AllowSidebarCollapse==="True" && Constant_SidebarState==="Open" && Constant_SaveSidebarState==="False"){
			$("#Sidebar").addClass("Open");
			$("#HeaderTray").addClass("Open");
		}else if(Constant_AllowSidebarCollapse==="True" && Constant_SidebarState==="Open" && Constant_SaveSidebarState==="True"){
			if(readCookie("getSidebarState")==="Open"){
				$("#Sidebar").addClass("Open");
			}
		}else if(Constant_SidebarState!="Closed"){
			$("#Sidebar").addClass("StaticOpen");
			$("#HeaderTray").addClass("StaticOpen");
		}
	}
}

function BootstrapCheckboxGroup(){
	$(".checkbox-group").css("border","none");
	$("div.checkbox-group label").each(function(){
		$(this).unwrap().addClass("btn btn-default");
	});
	
	$("div.checkbox-group label").on("click", function () {
		if($("input",this).is(":checked")===true){
			$(this).addClass("active");
		}else{
			$(this).removeClass("active");
		}
	});
}

function reflowCharts(){
	//Get all charts on the page
	var chartsArray = Highcharts.charts.filter(function(n){ return n != undefined });
	
	//Loop through the array
	$.each(chartsArray,function(i){
		//Update the current chart
		chartsArray[i].reflow();
	});
}

function Clean_URl(){
	$("#Menu-Stage #divFinalLevelIcon>a").each(function(){
		var onclick = $(this).attr("onclick");
		onclick = onclick.replace("rdTemplate/rdAjax/rdAjax.aspx","");
		$(this).attr("onclick",onclick);			
	});	
}

function AnimateDrillDown(name,id){
	// Move and fade to the left
	$("#Menu-Stage").delay(150).animate({
		opacity: 1,
		"margin-left": "-500px"
	}, 250, function() {
		// While hidden, relocate off canvas to the right and pull back to the viewing stage.
		$("#Menu-Stage").hide().css({
			"margin-left": "500px"
		}).show().animate({
			opacity: 1,
			"margin-left": "0"
		},250);
		
		// Append this item to the breadcrumb layer
		var crumb = $("#Breadcrumbs").text();
		if($("#Breadcrumbs:contains(" + crumb + ")").length){
			$("#Breadcrumbs").append('<span onmousedown="javascript:AnimateDrillUp();ModifyBreadcrumbs(this)" onmouseup="javascript:rdAjaxRequestWithFormVars(\'rdAjaxCommand=RefreshElement&amp;rdRefreshElementID=Menu-Refresh&amp;ID='+id+'&amp;rdReport=_Layout._Sidebar\',\'false\',\'\',true,null,null,null);"> > '+name+'</span>');
			($("#Breadcrumbs").text().length!==0 && !$("#Breadcrumbs").is(":visible"))? $("#Breadcrumbs").slideToggle(): null;
		}
		Clean_URl();
	});
}

function AnimateDrillUp(){
	// Move and fade to the right
	$("#Menu-Stage").delay(150).animate({
		opacity: 1,
		"margin-left": "500px"
	}, 250, function() {
		($("#Breadcrumbs").text().length===0)? $("#Breadcrumbs").slideToggle(): null;
		// While hidden, relocate off canvas to the left and pull back to the viewing stage.
		$("#Menu-Stage").hide().css({
			"margin-left": "-500px"
		}).show().animate({
			opacity: 1,
			"margin-left": "0"
		},250);
		Clean_URl();
	});
}

function ModifyBreadcrumbs(element){
	$(element).nextAll().remove();
}

$(function(){	
	//Check to see if the window is top if not then display button
	$(window).scroll(function(){
		if ($(this).scrollTop() > 50) {
			$('#divScrollToTop').fadeIn();
		} else {
			$('#divScrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('#ScrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},150);
		return false;
	});	
	
	// init the datepicker plugin
	// Check the page below for plugin config options
	// http://eternicode.github.io/bootstrap-datepicker/
	//$(".datepicker").datepicker();
	
	// Init the BootstrapCheckboxGroup() function on page load.
	//BootstrapCheckboxGroup();	
	
	//Init the jquery minicolor tool	
	$('.colorpicker').each(function(){
		$(this).colpick({
			layout:'hex',
			submit:0,
			color: $(this).val().replace('#',''),
			onChange:function(hsb,hex,rgb,el,bySetColor) {
				if(!bySetColor) $(el).val('#'+hex);
			}
		}).keyup(function(){
			$(this).colpickSetColor('#'+this.value);
		});
	});
	
	// Remove the framework if the page is an iframe
	//if(window.self !== window.top){
		//$("body").addClass("transparent-bg iframe-report");
	    //$("#Sidebar,#Header,#Footer,#HeaderTray").remove();

    //}

    // Remove the framework if the page is an iframe 
	//if (window.self !== window.top) {
	//    var frameId = window.frameElement.id;
	//    if (frameId.includes("rdFrame-1", 0) == false) {
	//        alert('hello');
	//        $("body").addClass("transparent-bg iframe-report");
	//        $("#Sidebar,#Header,#Footer").remove();
	//    }
	//}


	//Add the framework if the page is not an iframe
	//if (window.self === window.top) {

	//	$("#Header").load(location.pathname+"?rdReport=_Layout._Header&CurrentReport="+urlParam("rdReport")+" form #navigation");
	//	$("#Sidebar").load(location.pathname+"?rdReport=_Layout._Sidebar&CurrentReport="+urlParam("rdReport")+" form #Sidebar-left");
	//	$("#Footer").load(location.pathname+"?rdReport=_Layout._Footer&CurrentReport="+urlParam("rdReport")+" form #footer-wrapper");
		
	//	/*** ADDED BY JON ***/
	//	$("#HeaderTray").load(location.pathname + "?rdReport=_Layout._HeaderTray&CurrentReport=" + urlParam("rdReport") + " form #HeaderBar");
	//	$("#Header1,#Footer1,#ContentFrame").remove();
    //}

	if (window.self !== window.top) {

	    //alert('iFrame');

	    console.log(window.location.search);

	    var frameId = window.location.search;
	    var urlParams = frameId.indexOf("rdframeid"); //new URLSearchParams(window.location.search) 

	    $("#Header").load(location.pathname + "?rdReport=_Layout._Header&CurrentReport=" + urlParam("rdReport") + " form #navigation");
	    //$("#HeaderEmbedded").load(location.pathname + "?rdReport=_Layout._HeaderEmbedded&CurrentReport=" + urlParam("rdReport") + " form #navigation");
	    $("#Sidebar").load(location.pathname + "?rdReport=_Layout._Sidebar&CurrentReport=" + urlParam("rdReport") + " form #Sidebar-left");
	    $("#Footer").load(location.pathname + "?rdReport=_Layout._Footer&CurrentReport=" + urlParam("rdReport") + " form #footer-wrapper");

	    $("#Header1,#Footer1,#ContentFrame").remove();

	}

    //Add the framework if the page is not an iframe
	if (window.self === window.top) {
	    //alert('NOT iFrame');
	    $("#Header").load(location.pathname + "?rdReport=_Layout._Header&CurrentReport=" + urlParam("rdReport") + " form #navigation");
	    $("#Sidebar").load(location.pathname + "?rdReport=_Layout._Sidebar&CurrentReport=" + urlParam("rdReport") + " form #Sidebar-left");
	    $("#Footer").load(location.pathname + "?rdReport=_Layout._Footer&CurrentReport=" + urlParam("rdReport") + " form #footer-wrapper");

	    $("#Header1, #Footer1, #ContentFrame, #HeaderEmbedded1").remove();
	}
});