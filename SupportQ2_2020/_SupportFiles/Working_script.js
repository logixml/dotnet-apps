WORKING

/// d3 actions	
var data = jdNWData2;  // Logi JsonData element ID

// set the dimensions of the canvas
var margin = {top: 20, right: 30, bottom: 45, left: 35},
    width = 600 - margin.left - margin.right,
    height = 400 - margin.top - margin.bottom;

// set the ranges
var x = d3.scale.ordinal().rangeRoundBands([0, width], .05);
var y = d3.scale.linear().range([height, 0]);

// define the axis
var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");
var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
	.ticks(10);

// add the SVG element
var svg = d3.select("#chartBar").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", 
		"translate(" + margin.left + "," + margin.top + ")");	
	
  // scale the range of the data
  x.domain(data.map(function(d) { return d.VISIT_LABEL; }));
  y.domain([0, d3.max(data, function(d) { return d.VCOUNT; })]);
	
  // add axis
  svg.append("g")
      .attr("class", "axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
      .selectAll("text")
      .style("text-anchor", "end")
      .attr("dx", "-.8em")
      .attr("dy", "-.55em")
      .attr("transform", "rotate(-45)" );

  svg.append("g")
      .attr("class", "axis")
      .call(yAxis)
      .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 5)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      

  // Add bar chart
  svg.selectAll("bar")
      .data(data)
      .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(d.VISIT_LABEL); })
      .attr("width", x.rangeBand())
      .attr("y", function(d) { return y(d.VCOUNT); })
      .attr("height", function(d) { return height - y(d.VCOUNT); });


            
