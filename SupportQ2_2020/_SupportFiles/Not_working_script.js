NOT WORKING

/// d3 actions
var data = jdNWData2;  // Logi JsonData element ID

var margin =  {top: 20, right: 10, bottom: 20, left: 40};
var marginOverview = {top: 30, right: 10, bottom: 20, left: 40};
var selectorHeight = 40;
var width = 1000 - margin.left - margin.right;
var height = 400 - margin.top - margin.bottom - selectorHeight;
var heightOverview = 80 - marginOverview.top - marginOverview.bottom;
       
var maxLength = d3.max(data.map(function(d){ return d.VISIT_LABEL.length}))
//var barWidth = maxLength * 7;
console.log(maxLength);
var barWidth = maxLength*2;
var numBars = Math.round(width/barWidth);
console.log(numBars);
var isScrollDisplayed = barWidth * data.length > width;
       

console.log(isScrollDisplayed)
  
var xscale = d3.scale.ordinal()
                .domain(data.slice(0,numBars).map(function (d) { return d.VISIT_LABEL; }))
                .rangeBands([0, width], .2);

var yscale = d3.scale.linear()
					.domain([0, d3.max(data, function (d) { return d.VCOUNT; })])
             .range([height, 0]);
			  
 //var yscale = d3.scale.linear()
           //       .domain([d3.min(data), d3.max(data)+20])
               //   .range([0, width - 100]);
			  
			  
  
var xAxis  = d3.svg.axis().scale(xscale).orient("bottom");
var yAxis  = d3.svg.axis().scale(yscale).orient("left");

console.log(xAxis);	
console.log(yAxis);
console.log(margin.bottom);
  
var svg = d3.select("body").append("svg")
						.attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom + selectorHeight);
  
var diagram = svg.append("g")
								 .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
  
diagram.append("g")
  		 .attr("class", "x axis")
         .attr("transform", "translate(0, " + height + ")")
         .call(xAxis)
		 .selectAll(".tick text")
         .call(wrap, xscale.rangeBand());
		 
  
diagram.append("g")
       .attr("class", "y axis")
       .call(yAxis);

  
var bars = diagram.append("g");
  
bars.selectAll("rect")
            .data(data.slice(0, numBars), function (d) {return d.VISIT_LABEL; })
            .enter().append("rect")
            .attr("class", "bar")
            .attr("x", function (d) { return xscale(d.VISIT_LABEL); })
            .attr("y", function (d) { return yscale(d.VCOUNT); })
            .attr("width", xscale.rangeBand())
            .attr("height", function (d) { return height - yscale(d.VCOUNT); });
			
function wrap(text, width) {
  text.each(function() {
    var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        y = text.attr("y"),
        dy = parseFloat(text.attr("dy")),
        tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
      }
    }
  });
}

  
if (isScrollDisplayed)
{
  var xOverview = d3.scale.ordinal()
                  .domain(data.map(function (d) { return d.VISIT_LABEL; }))
                  .rangeBands([0, width], .2);
  yOverview = d3.scale.linear().range([heightOverview, 0]);
  yOverview.domain(yscale.domain());

 // var subBars = diagram.selectAll('.subBar')
   //   .data(data)

  //subBars.enter().append("rect")
   //   .classed('subBar', true)
   //   .attr({
  //        height: function(d) {
  //            return heightOverview - yOverview(d.VCOUNT);
  //        },
   //       width: function(d) {
   //           return xOverview.rangeBand()
     //     },
      //    x: function(d) {
//
      //        return xOverview(d.VISIT_LABEL);
    //      },
     //     y: function(d) {
      //        return height + heightOverview + yOverview(d.VCOUNT)
      //    }
    //  })

  var displayed = d3.scale.quantize()
              .domain([0, width])
              .range(d3.range(data.length));

  diagram.append("rect")
              .attr("transform", "translate(0, " + (height + margin.bottom + 30) + ")")
              .attr("class", "mover")
              .attr("x", 0)
              .attr("y", 0)
              .attr("height", selectorHeight)
              .attr("width", Math.round(parseFloat(numBars * width)/data.length))
			  // .attr("width", 25)
              .attr("pointer-events", "all")
              .attr("cursor", "ew-resize")
              .call(d3.behavior.drag().on("drag", display));
}
function display () {
//alert("test");   
   var x = parseInt(d3.select(this).attr("x")),
        nx = x + d3.event.dx,
        w = parseInt(d3.select(this).attr("width")),
        f, nf, new_data, rects;

    if ( nx < 0 || nx + w > width ) return;

    d3.select(this).attr("x", nx);

    f = displayed(x);
    nf = displayed(nx);

    if ( f === nf ) return;

    new_data = data.slice(nf, nf + numBars);

    xscale.domain(new_data.map(function (d) { return d.VISIT_LABEL; }));
    diagram.select(".x.axis").call(xAxis)
         .selectAll(".tick text")
         .call(wrap, xscale.rangeBand());
		 

    rects = bars.selectAll("rect")
      .data(new_data, function (d) {return d.VISIT_LABEL; });

	 	rects.attr("x", function (d) { return xscale(d.VISIT_LABEL); });

// 	  rects.attr("transform", function(d) { return "translate(" + xscale(d.VISIT_LABEL) + ",0)"; })

    rects.enter().append("rect")
      .attr("class", "bar")
      .attr("x", function (d) { return xscale(d.VISIT_LABEL); })
      .attr("y", function (d) { return yscale(d.VCOUNT); })
      .attr("width", xscale.rangeBand())
      .attr("height", function (d) { return height - yscale(d.VCOUNT); });
    rects.exit().remove();
	
};



